#!/usr/bin/env python2

import sys,os,parsing,subprocess,re,math,tempfile,csv
from os.path import join
from collections import defaultdict

def toCoralConstraint(cons):
#    print "parsing: " + cons.strip()
    tree = parsing.ExprParser.parse(cons)
    coralCons = parsing.visit_tree(tree)
#    print "result: " + coralCons
    return coralCons

def processFile(outfile):
#   print "[DEBUG] processing file: " + outfile
    with open(outfile) as f:
        lines = f.readlines()
        volcomp_domains = [x for x in lines if x.startswith("V r")]
        domains = []
        for d in volcomp_domains:
            match = re.search("V r_(.+):.:(.+), (.+)",d.strip())
            iv = (float(match.group(2)),float(match.group(3)))
            domains.append(str(iv))

        constraints = [x for x in lines if "<=" in x]
#        print "[DEBUG] constraints: " + str(constraints)
        coral_input = []
        try:
            lb = float(lines[-1].split(" ")[5])
            ub = float(lines[-2].split(" ")[5])
        except: #missing data in volcomp output
            print "[DEBUG] missing upper/lower bound data - assuming 0"
            lb = 0.0
            ub = 0.0

        for cons in constraints:
            if cons.count("<=") == 2:
                toks = cons.split(" ")
                conA = " ".join(toks[0:3])
                conB = " ".join(toks[2:])
                coral_input.append(toCoralConstraint(conA))
                coral_input.append(toCoralConstraint(conB))
            else:
                coral_input.append(toCoralConstraint(cons))
        return ((lb,ub), (coral_input, domains))

def runCoral(filename):
    """run the model counter and return a dictionary with the merged results"""
    coral_out = subprocess.check_output(["./runCounter.sh",filename,"bla"])
#   print coral_out
#   print "[DEBUG] coral output: \n" + csv_report
    return csv.DictReader(coral_out.split("\n")).next()

def addTuples(a,b):
    return (a[0]+b[0],a[1]+b[1])

def writeToCoralFile(data,infile):
    cons, domains = data
    if len(cons) > 0:
        constraints = ";".join(cons)
        infile.write(constraints + "\n")
        infile.write(str(domains).replace('(',"{").replace(")","}").replace("'","") + "\n")

def processBenchmark(directory):
    pldi_bound = (0.0,0.0)
    pldi_phi_bound = (0.0,0.0)
    n_constraints = 0
    n_constraints_phi = 0
    file_handle, filename = tempfile.mkstemp()
    file_handle_phi, filename_phi = tempfile.mkstemp()
    coral_phi_results = None
#phi cons
    with os.fdopen(file_handle_phi,'w') as coral_input_file:
        for root,dirs,files in os.walk(directory):
            pldifiles = [x for x in files if x.endswith("phi-0.in.output")]
            n_constraints_phi = n_constraints_phi + len(pldifiles)
            print "[DEBUG]: processing {} files for benchmark {}".format(len(pldifiles),root)
            for f in pldifiles:
                path_bound, constraintsData = processFile(join(root,f))
                writeToCoralFile(constraintsData,coral_input_file)
                pldi_phi_bound = addTuples(pldi_phi_bound,path_bound)
    print "calling coral for benchmark {}; constraints are in file {}".format(directory,filename_phi)
    coral_results_phi = runCoral(filename_phi)
    print "finished calling coral"

#normal cons
    # with os.fdopen(file_handle,'w') as coral_input_file:
    #     for root,dirs,files in os.walk(directory):
    #         pldifiles = [x for x in files if x.endswith(".in.output") and "phi-0" not in x]
    #         n_constraints = n_constraints + len(pldifiles)
    #         print "[DEBUG]: processing {} files for benchmark {}".format(len(pldifiles),root)
    #         for f in pldifiles:
    #             path_bound, constraintsData = processFile(join(root,f))
    #             writeToCoralFile(constraintsData,coral_input_file)
    #             pldi_bound = addTuples(pldi_bound,path_bound)

    # print "calling coral (normal)"
    # coral_results = runCoral(filename)
    # print "finished calling coral"
    coral_results = defaultdict(lambda : "")
    return ((pldi_bound,coral_results, n_constraints),(pldi_phi_bound,coral_results_phi, n_constraints_phi))


def main():
    benchmark_file = sys.argv[1]
    csv_dicts = []
    csv_phi_dicts = []
    with open(benchmark_file) as infile:
        for line in infile:
            if line.startswith("#"):
                pass
            else:
                benchmark, pldi_time = line.strip().split(" ")
                (r_tuple,r_phi_tuple) = processBenchmark(benchmark)
                pldiData,coralData,nConstraints = r_tuple
                pldiDataPhi,coralDataPhi,nConstraintsPhi = r_phi_tuple
                coralData['volcompLower'] = pldiData[0]
                coralData['volcompUpper'] = pldiData[1]
                coralData['nConstraints'] = nConstraints
                coralDataPhi['volcompLower'] = pldiDataPhi[0]
                coralDataPhi['volcompUpper'] = pldiDataPhi[1]
                coralDataPhi['nConstraints'] = nConstraintsPhi
                coralData['volcompTime'] = pldi_time.strip()
                coralDataPhi['volcompTime'] = pldi_time.strip()
                coralData['name'] = benchmark.split("/")[3]
                coralDataPhi['name'] = benchmark.split("/")[3]
                csv_dicts.append(coralData)
                csv_phi_dicts.append(coralDataPhi)
    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    writer = csv.DictWriter(sys.stdout,csv_dicts[0].keys())
    writer.writeheader()
    for d in csv_dicts:
        writer.writerow(d)
    
    print "$$$$ phi data $$$$"
    writer = csv.DictWriter(sys.stdout,csv_phi_dicts[0].keys())
    writer.writeheader()
    for d in csv_phi_dicts:
        writer.writerow(d)

    

#        coral_bound = (coralData['estimateResult'] - 2 *coralData['estimateResult'] )
#        print ">> results for " + sys.argv[1]
#        print ">>     pldi: [{},{}]".format(pldiData[0],pldiData[1])
#        print ">>    coral: [{},{}] - time: {} - cons:{}".format(coralData['estimateResult'],coralData[1], nConstraints)

main()   
