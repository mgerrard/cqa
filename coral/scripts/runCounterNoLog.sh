#!/bin/bash

cd ..
echo "start: $(date)" 
##echo "$OPTIONS $1 $2"
java -cp bin:libs/commons-math-1.2.jar:libs/opt4j-2.4.jar:libs/colt.jar:libs/ssj.jar:/usr/local/Wolfram/Mathematica/9.0/SystemFiles/Links/JLink/JLink.jar:libs/guava-15.0.jar:libs/jung/jung-api-2.0.1.jar:libs/jung/collections-generic-4.01.jar:libs/jung/jung-algorithms-2.0.1.jar:libs/jung/jung-graph-impl-2.0.1.jar coral.counters.RegCoralMain $OPTIONS $1 $2 
echo "end: $(date)" 
#cat logs/countertmp | grep "\[regCoral\]" #sed -e '1,/%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%/d'
cd scripts
