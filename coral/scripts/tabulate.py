#!/usr/bin/env python2

import sys, csv

MICRO_HEADER_TEMPLATE = r"""\multicolumn{7}{|c|}{\textbf{$BNAME}}\\
\hline
\multirow{2}{*}{name} & \emph{Num.} & \emph{Num.}  &
\emph{Num.} & \emph{Analytical} &  \multicolumn{2}{c|}{\tname{}}  \\
\cline{6-7}
 & \emph{Ands} & \emph{Ops.} & \emph{Funs.} & \emph{Solution} & \emph{estimate}~\textpm{}~$\epsilon$ &  \emph{time} \\
\hline"""

MICRO_LINE_TEMPLATE = r"""\href{http://google.com}{name} & {nClauses} & {nOperations} ({nDistinctOperations}) & {nFunctions} ({nDistinctFunctions}) & {analyticResult} & {estimateResult} \pm{stdev} & {time} \\
\hline"""

VOLCOMP_LINE_TEMPLATE = r"""{name} & {nConstraints} ({nClauses}) & {nOperations} ({nDistinctOperations}) & [{volcompLower:.4f},{volcompUpper:.4f}] & {volcompTime:.1f} & [{lowerbound95:.4f},{upperbound95:.4f}] & {time:.1f} \\
\hline"""

#name,analyticResult,estimateResult,nBoxes,stdev,timenOperations,nFunctions,nClauses,nDistinctOperations,nDistinctFunctions
def prepare_micro_table(reader,benchname):
    print MICRO_HEADER_TEMPLATE.replace("$BNAME",benchname)
    for row in reader:
        print MICRO_LINE_TEMPLATE.format(**row)

def prepare_volcomp_table(reader):
    for row in reader:
#        print "##### " + str(row)
        row['volcompLower'] = float(row['volcompLower'])
        row['volcompUpper'] = float(row['volcompUpper'])
        row['upperbound95'] = float(row['upperbound95'])
        row['lowerbound95'] = float(row['lowerbound95'])
        row['volcompTime'] = float(row['volcompTime'])
        row['time'] = float(row['time'])
        print VOLCOMP_LINE_TEMPLATE.format(**row)

def main():
    table_type = sys.argv[1] #micro, volcomp
    lines = sys.stdin.readlines()
    csvreader = csv.DictReader(lines)

    if table_type == "micro":
        prepare_micro_table(csvreader,sys.argv[2])
    elif table_type == "volcomp":
        prepare_volcomp_table(csvreader)
    else:
        print "Unknown table type (choose 'micro' or 'volcomp')"
        
main()
