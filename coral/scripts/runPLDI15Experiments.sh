#!/bin/bash

BENCHLIST=(
"../inputs/dwqcoral/complex/apollo.nodeq	253"
"../inputs/dwqcoral/framingham-1	27"
"../inputs/dwqcoral/framingham-2	44"
"../inputs/dwqcoral/complex/conflict.nodeq	14"
"../inputs/dwqcoral/complex/turn.nodeq	73"
"../inputs/dwqcoral/example-cart-12	45"
"../inputs/dwqcoral/example-cart-14	48"
"../inputs/dwqcoral/example-carton-5-18	954"
"../inputs/dwqcoral/example-carton-5-20	1030"
"../inputs/dwqcoral/example-ckd-epi-simple-0	18"
"../inputs/dwqcoral/example-invPend-0	1"
##############
# "../inputs/dwqcoral/example-carton-5-0	40"
# "../inputs/dwqcoral/example-carton-5-1	37"
# "../inputs/dwqcoral/example-carton-5-2	38"
# "../inputs/dwqcoral/example-carton-5-22	1132"
# "../inputs/dwqcoral/example-carton-5-5	38"
# "../inputs/dwqcoral/example-ckd-epi-0	46"
# "../inputs/dwqcoral/example-ckd-epi-1	38"
# "../inputs/dwqcoral/example-ckd-epi-simple-1	14"
# "../inputs/dwqcoral/framingham-0	23"
# "../inputs/dwqcoral/framingham-hypten-0	15"
# "../inputs/dwqcoral/framingham-hypten-3	8"
)

SEEDFILE="../inputs/seeds-5-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-10ksteps.csv"
#REPORTFILE="../output/data-allsubjects-table2.txt"

echo ">> Table 2: running qCORAL (default,incremental) for each benchmark (10-100k)<<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,samplebudget,mode" > $DATAFILE

for MAXSAMPLES in $(seq 100000 100000 1000000)
do
	echo "=========== $MAXSAMPLES ==========="
	for BENCHLINE in "${BENCHLIST[@]}"
	do
		BENCHFILE=$(echo "$BENCHLINE" | cut -f 1)
		NPARTS=$(echo "$BENCHLINE" | cut -f 2 )

		echo $BENCHFILE
		NSAMPLES=$(python -c "a=$MAXSAMPLES;b=$NPARTS;c=a%b; print str(a/b) if c == 0 else (a/b + 1)")
#		STEP=$(python -c "print str(${NSAMPLES}/5)")
		echo "MAX SAMPLES per partition: $NSAMPLES"
#		echo "SAMPLES per improvement step (20% of previous value): $STEP"
		
		for SEED in "${SEEDS[@]}"
		do
			DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples $NSAMPLES --mcMinSamplesPerBox 100"
			if [[ $BENCHFILE == "*complex*" ]] 
			then
				DEFAULT_OPTIONS="$DEFAULT_OPTIONS --rangeLO -100 --rangeHI 100"
			fi

			FULLNAME=$(readlink -e "$BENCHFILE")
			echo -n "."
			export OPTIONS="--mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults $DEFAULT_OPTIONS"
			./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk -v budget="$MAXSAMPLES" '{printf "%s,%s,XYZ\n", $0,budget}' >> $DATAFILE

			# export OPTIONS="--mcIterativeImprovement --mcTargetVariance=1E-10 --mcSamplesPerIncrement=$STEP $DEFAULT_OPTIONS"
			# ./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,XYZ-INCR\n", $0}' >> $DATAFILE
		done	
	done
done

echo ">> csv file done. <<"
#python report.py table4 ${DATAFILE} > ${REPORTFILE}
#echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
