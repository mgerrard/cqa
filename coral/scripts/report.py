#!/usr/bin/env python2

import sys,csv,math
from collections import defaultdict


def mean(values):
    return sum(values) / float(len(values));

# Calculate standard deviation.
# Source: http://stats.stackexchange.com/questions/25956/what-formula-is-used-for-standard-deviation-in-r
def stdev(values, mean):
    agg = 0.0
    for val in values:
        agg = agg + (val - mean)**2
    div = 1.0 / (len(values) - 1)
    return math.sqrt(div * agg)

#name,analytic,estimate,stdev,time,samples,exec
def reportTable2(filename):
    # name of all benchmarks and their analytic solution
    benchmarks = set()
    # samplerate -> benchname -> list of results
    data = defaultdict(lambda: defaultdict(list))
    
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['name']
            t_name = (name, row['analytic'])
            benchmarks.add(t_name)

            nsamples = row['samples']
            estimate = row['estimate']
            data[nsamples][name].append(float(estimate))
    
    for name, analytic in benchmarks:
        line = "{} => analytic:{:.6f}, ".format(name,float(analytic))
        keys = sorted(data.keys())
        for nsamples in keys:
            estimates = data[nsamples][name]
            avg = mean(estimates)
            sample_stdev = stdev(estimates,avg)
            line = ''.join([line," {}:[avg:{:.6f},stdev:{:.6f}],".format(nsamples,avg,sample_stdev)])
        print line
    
#file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec
def reportTable3(filename):
    # name of all benchmarks and their analytic solution
    benchmarks = set()
    # samplerate -> benchname -> list of results
    data = defaultdict(lambda: defaultdict(list))
    
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['file']
            benchmarks.add(name)

            nsamples = row['samples']
            estimate = row['estimate']
            stdev = row['stdev-varestimate']
            time = row['time']
            t_stats = (float(estimate),float(stdev),float(time))
            data[nsamples][name].append(t_stats)
    
    for name in benchmarks:
        line = "{} => ".format(name)
        keys = sorted(data.keys())
        for nsamples in keys:
            t_stats = data[nsamples][name]
            estimates = [x[0] for x in t_stats]
            stdevs = [x[1] for x in t_stats]
            times = [x[2] for x in t_stats]

            avg_est = mean(estimates)
            avg_std = mean(stdevs)
            avg_time= mean(times)

            line = ''.join([line," {}:[avg:{:.6f},est-stdev:{:.6f},time:{:.6f}],".format(nsamples,avg_est,avg_std,avg_time)])
        print line


def reportTable3math(filename):
    # name of all benchmarks
    benchmarks = set()
    # benchname -> list of results
    data = defaultdict(list)
    
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['file']
            benchmarks.add(name)

            estimate = row['estimate']
            time = row['time']
            t_stats = (float(estimate),float(time))
            data[name].append(t_stats)
    
    for name in benchmarks:
        line = "{} => ".format(name)

        t_stats = data[name]
        estimates = [x[0] for x in t_stats]
        times = [x[1] for x in t_stats]
        avg_est = mean(estimates)
        avg_time= mean(times)
        
        line = ''.join([line," :[avg:{:.6f},time:{:.6f}],".format(avg_est,avg_time)])
        print line

#file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,mode
def reportTable4(filename):
    # easy way = use R and aggregate(). But we don't want to add another dependency :P
    # name of all benchmarks
    benchmarks = set()
    # name of all modes
    modes = set()
    # samplerate -> benchname -> mode -> list of results
    data = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['file']
            benchmarks.add(name)
            mode = row['mode']
            modes.add(mode)

            nsamples = row['samples']
            estimate = row['estimate']
            time = row['time']

            t_stats = (float(estimate),float(time))
            data[nsamples][name][mode].append(t_stats)
    
    for name in benchmarks:
        print "{} => ".format(name)
        keys = data.keys()
        for nsamples in sorted([int(x) for x in keys]):
            nsamples = str(nsamples)
            line = "        {}: ".format(nsamples)
            for mode in modes:
                t_stats = data[nsamples][name][mode]
                if len(t_stats) == 0:
                    line = ''.join([line," not enough data"])
                else:
                    estimates = [x[0] for x in t_stats]
                    times = [x[1] for x in t_stats]                
                    avg_est = mean(estimates)
                    avg_time= mean(times)
                    sample_stdev = stdev(estimates,avg_est)
                    line = ''.join([line," {}:[avg:{:.6f},stdev:{:.6f},time:{:.2f}],".format(mode,avg_est,sample_stdev,avg_time)])
            print line
    pass

#file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec
def reportTable4math(filename):
    # name of all benchmarks and their analytic solution
    benchmarks = set()
    # samplerate -> benchname -> list of results
    data = defaultdict(lambda: defaultdict(list))
    
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['file']
            benchmarks.add(name)

            nsamples = row['samples']
            estimate = row['estimate']
            time = row['time']
            t_stats = (float(estimate),float(time))
            data[nsamples][name].append(t_stats)
    
    for name in benchmarks:
        line = "{} => ".format(name)
        keys = sorted(data.keys())
        for nsamples in keys:
            t_stats = data[nsamples][name]
            estimates = [x[0] for x in t_stats]
            times = [x[1] for x in t_stats]
            avg_est = mean(estimates)
            avg_time= mean(times)
            sample_stdev = stdev(estimates,avg_est)

            line = ''.join([line," {}:[avg:{:.6f},stdev:{:.6f},time:{:.6f}],".format(nsamples,avg_est,sample_stdev,avg_time)])
        print line

    pass

def main():
    mode = sys.argv[1]
    filename = sys.argv[2]
    if mode == "table2":
        reportTable2(filename)
    elif mode == "table3":
        reportTable3(filename)
    elif mode == "table4":
        reportTable4(filename)
    elif mode == "table3math":
        reportTable3math(filename)
    elif mode == "table4math": 
        reportTable4math(filename)
    else:
        print "Unknown mode"

main()
