#!/usr/bin/env python 

import re

METHOD_REGEX = re.compile("(\s*)public(\s*)static(\s*)void(\s*)benchmark(\d+)(\s*)\((.*)\)(\s*){")

TEST_TEMPLATE = """
package coral.tests.samples;

import coral.tests.JPFBenchmark;

public class Sample$NUMBER {

  public static void main(String[] args) {
    JPFBenchmark.benchmark$NUMBER(METHOD_PARAMS);
  }

}"""


def run():
    #read JPFBenchmark looking for methods
    readfile = open('./src/coral/tests/JPFBenchmark.java','r')
    for line in readfile:
        match = METHOD_REGEX.match(line)
        if match != None:
            dummyParams = getDummyParams(line)
            testNumber = match.group(5) 
            template = TEST_TEMPLATE.replace("$NUMBER",testNumber)
            template = template.replace("METHOD_PARAMS",dummyParams)
            outfile = open('./src/coral/tests/samples/Sample'+testNumber+'.java','w')
            outfile.write(template)
            outfile.close
        

def getDummyParams(line):
    nParams = getNumberOfParams(line)
    params = "0"
    for i in range(nParams - 1):
        params = params + ", 0"

    return params

#Numero de parametros de um metodo = n. de virgulas + 1
def getNumberOfParams(methodSignature):
    return methodSignature.count(",") + 1
    
