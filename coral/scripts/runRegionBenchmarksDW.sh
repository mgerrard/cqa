#!/bin/bash

#Use the benchmarks with a tractable number of regions
BENCHMARKS=(
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-carton-5-0"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-carton-5-1"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-carton-5-2"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-carton-5-5"
# "/home/mateus/workspace/coral/inputs/dwqcoral/framingham-0"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-ckd-epi-simple-1"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-ckd-epi-1"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-ckd-epi-simple-0"
# "/home/mateus/workspace/coral/inputs/dwqcoral/example-ckd-epi-0"
# "/home/mateus/workspace/coral/inputs/dwqcoral/framingham-1"
# "/home/mateus/workspace/coral/inputs/dwqcoral/framingham-hypten-0"
# "/home/mateus/workspace/coral/inputs/dwqcoral/framingham-hypten-3"
# "/home/mateus/workspace/coral/inputs/dwqcoral/framingham-2"
#"/home/mateus/workspace/coral/inputs/dwqcoral/complex/turn.nodeq"
#"/home/mateus/workspace/coral/inputs/dwqcoral/complex/conflict.nodeq"
"/home/mateus/workspace/coral/inputs/dwqcoral/complex/apollo.nodeq"
)

MAXSAMPLES=1000
MINSAMPLES=100

SEEDFILE="../inputs/seeds-30-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-tabledw-discrete.csv"
REPORTFILE="../output/report-tabledw-discrete.txt"

echo ">> Discretization: running qCoral for each benchmark<<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec" > $DATAFILE

for SEED in "${SEEDS[@]}"
do
	DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"
	echo -n "."
	for BENCHFILE in "${BENCHMARKS[@]}"
	do
		echo "  >${BENCHFILE}"
		export OPTIONS="--mcPartitionAndCache --mcDiscretize"
		export OPTIONS="$OPTIONS $DEFAULT_OPTIONS"
		FULLNAME=$(readlink -e "$BENCHFILE")
		./runDWCounter.sh $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 >> $DATAFILE
	done	
done

echo ">> csv file done. <<"
cat "${DATAFILE}"
python report.py table3 ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
