import lrparsing
from lrparsing import Keyword, List, Prio, Ref, THIS, Token, Tokens

class ExprParser(lrparsing.Grammar):
    #
    # Put Tokens we don't want to re-type in a TokenRegistry.
    #
    class T(lrparsing.TokenRegistry):
        integer = Token(re="[0-9]+")
        floating = Token(re="[0-9]+\.[0-9]+")
        floating["key"] = "I a mapping!"
        ident = Token(re="[A-Za-z_][A-Za-z_0-9]*")
    #
    # Grammar rules.
    #
    expr = Ref("expr")                # Forward reference
    atom = T.ident | T.floating | T.integer | Token('(') + expr + ')'
    expr = Prio(                      # If ambiguous choose atom 1st, ...
        atom,
        Tokens("+ - ") >> THIS,      # >> means right associative
        THIS << Tokens("* / // %") << THIS,
        THIS << Tokens("+ -") << THIS,# THIS means "expr" here
        THIS << (Tokens("== <=") | Keyword("is")) << THIS)
    expr["a"] = "I am a mapping too!"
    START = expr                      # Where the grammar must start

OPS = {"+":"ADD","-":"SUB","*":"MUL","<=":"DLE"}

def visit_tree(expr,opts={}):
    global OPS
    rule = expr[0]
#    print ExprParser.repr_parse_tree(expr) + "\n"

    if rule == ExprParser.START:
        return visit_tree(expr[1],opts)
    elif rule == ExprParser.expr:
        length = len(expr)
#        print "expr: " + str(length)
#        print expr[1]
        if length == 3 and expr[1][1] == '-': #unary op
            return visit_tree(expr[2],{"neg":True})
        elif length == 2:                        #atom
            return visit_tree(expr[1],opts)
        else:                                    #binop
#            print "binop"
            operation = OPS[expr[2][1]] #get string               
            return operation + "(" + \
                visit_tree(expr[1],opts) + "," + \
                visit_tree(expr[3],opts) + ")"
    elif rule == ExprParser.atom:
        return visit_tree(expr[1],opts)
    elif rule == ExprParser.T.integer:
        minus = "-" if opts.get("neg",False) else ""
        return "DCONST(" + minus + expr[1] + ")"
    elif rule == ExprParser.T.floating:
        minus = "-" if opts.get("neg",False) else ""
        return "DCONST(" + minus + expr[1] + ")"
    elif rule == ExprParser.T.ident:
        if opts.get("neg",False):
            return "MUL(DCONST(-1),DVAR(" + expr[1] + "))"
        else:
            return "DVAR(" + expr[1] + ")"
    else:
        raise GrammarError


#parse_tree = ExprParser.parse("0.798 * r_4-0.411 * r_5 <= -0.471458")
#print(ExprParser.repr_parse_tree(parse_tree))
#print(visit_tree(parse_tree))
