#!/bin/bash

DATAFILE="../output/data-table4-allreset.csv"
REPORTFILE="../output/report-table4-allreset.txt"

echo ">> Table4: running qCoral 30 times for each subject/config pair <<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,mode" > $DATAFILE

for MAXSAMPLES in "1000" "10000" "100000"
do
	echo "=========== $MAXSAMPLES ==========="
	for BENCHFILE in "../inputs/jpf/apollo_cons_leaves_sample70" "../inputs/jpf/turnlogic_cons_leaves_sample70" "../inputs/jpf/conflict_cons_leaves_sample70" 
	do
#		if [[ $BENCHFILE != "../inputs/jpf/apollo_cons_leaves_sample70" ]]
#		then
#			SEEDFILE="../inputs/seeds-100"  #smaller experiments
#		else
			SEEDFILE="../inputs/seeds-30-cp" 
#		fi
		IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))
		
		echo -e "\n$BENCHFILE"
		for SEED in "${SEEDS[@]}"
		do
			DEFAULT_OPTIONS="--mcSeed $SEED --mcNumExecutions 1 --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox 100 --rangeLO -100 --rangeHI 100"
			FULLNAME=$(readlink -e "$BENCHFILE")
			
			################# "pure monte carlo (1 executions) - no rp"
			export OPTIONS="--mcSkipPaving --mcResetRngEachSimulation=true $DEFAULT_OPTIONS"
			./runCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral\n", $0}' >> $DATAFILE
			################ "monte carlo + realpaver (1 executions) - multiple boxes"
			export OPTIONS="--mcResetRngEachSimulation=true $DEFAULT_OPTIONS"
			./runCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral{STRAT-nomerge}\n", $0}' >> $DATAFILE
			################ "with partitioning and cache, merged boxes, cache for realpaver (1 execution, reseed every sim.)"
			export OPTIONS="--mcPartitionAndCache --mcCallIntSolPartition --mcResetRngEachSimulation=true $DEFAULT_OPTIONS"
			./runCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral{STRAT+PARTCACHE}\n", $0}' >> $DATAFILE

			echo -n "."
		done
	done	
done

echo ">> csv file done. Preparing report... <<"
python report.py table4 ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
