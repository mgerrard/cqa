#!/usr/bin/env python2

import csv,sys,math
from collections import defaultdict

TEMPLATE = "{name} {vol:.4f} & {var:.4f} & {time:.2f} [({volmean},{volstdev}),({varmean},{varstdev}),({timemean},{timestdev})]"

#sample variance (s^2 N-1): http://mathworld.wolfram.com/Variance.html
def variance(samples,average):
    diff_of_squares = [(x - average) ** 2 for x in samples]
    return sum(diff_of_squares) / float(len(samples) - 1)

def main():
    print "----------------------------------------"
    print """How this report works:
    The first entry for each subject will be printed, along with some statistics for all entries of the specific subject.
"""
    print "data order: name vol var time [(volmean,volSampleStdev) (varmean,varSampleStdev), (timemean,timeSampleStdev)]  "
    with open(sys.argv[1]) as csvfile:
        rundata = defaultdict(list)
        reader = csv.DictReader(csvfile)

        for row in reader:
            name = row['name']
            time = row['time']
            estimate = row['vol']
            var = row['var']
            
            data = (estimate,var,time)
            rundata[name].append(data)

        for name,datalist in rundata.items():
            volEstimates = [float(x[0]) for x in datalist]
            variances = [float(x[1]) for x in datalist]
#            print variances
            times = [float(x[2]) for x in datalist]

            n_samples = float(len(volEstimates))
            volmean = sum(volEstimates) / n_samples
            varmean = sum(variances) / n_samples
            timemean = sum(times) / n_samples

            #std. dev according to CLT - check http://mathworld.wolfram.com/CentralLimitTheorem.html for more info
            volstdev = math.sqrt(variance(volEstimates,volmean) / n_samples)
            varstdev = math.sqrt(variance(variances,varmean) / n_samples)
            timestdev = math.sqrt(variance(times,timemean) / n_samples)

            first = datalist[0]
            template_info = {'name':name,'vol':float(first[0]),'var':float(first[1]),'time':float(first[2]),'volmean':volmean,'varmean':varmean,'timemean':timemean,'volstdev':volstdev,'varstdev':varstdev,'timestdev':timestdev}

            print TEMPLATE.format(**template_info)
            
main()        
