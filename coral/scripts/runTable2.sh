#!/bin/bash

SEEDFILE="../inputs/seeds-30-cp" 
DATAFILE="../output/data-table2.csv"
REPORTFILE="../output/report-table2.txt"

IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

echo ">> Table2: running qCoral 30 times for each microbenchmark <<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "name,analytic,estimate,stdev,time,samples,exec" > $DATAFILE

for SAMPLES in "1000" "10000" "100000" "1000000"
do
	echo -e "\n%%%%%%%% Running for $SAMPLES samples %%%%%%%%%%%%%%"
	for SEED in "${SEEDS[@]}"
	do
		echo -n "."
		OPTIONS="--mcNumExecutions 1 --mcMaxSamples $SAMPLES --mcMinSamplesPerBox 100 --mcSeed $SEED --mcMergeBoxes"
		export OPTIONS="$OPTIONS"
		./runMicroBenchmark.sh | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 >> $DATAFILE
	done
done

echo ">> Preparing report... <<"
python report.py table2 ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
