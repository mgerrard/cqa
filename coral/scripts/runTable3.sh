#!/bin/bash

MAXSAMPLES=30000
MINSAMPLES=100

SEEDFILE="../inputs/seeds-30-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-table3.csv"
REPORTFILE="../output/report-table3.txt"

echo ">> Table3: running qCoral 30 times for each VolComp benchmark<<"

RUNVOL="false"
if [[ "$1" == "--with-vol" ]]
then
	RUNVOL="true"
	echo ">> WARNING: VOL benchmark is included on the work set. This will take a *long* time (> 13h)."
else
	echo ">> WARNING: VOL benchmark is *not* included by default. To include it, re-execute this script with '--with-vol'"
fi

echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec" > $DATAFILE

for SEED in "${SEEDS[@]}"
do
	DEFAULT_OPTIONS="--mcSeed $SEED --mcNumExecutions 1 --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"
	echo -n "."
	for BENCHFILE in ../inputs/pldi/*-* #iterate over files with an hyphen (the benchmark files)
	do
#		echo $BENCHFILE
		if [[ $RUNVOL == "false" && "$BENCHFILE" == *"example-vol-16" ]]
		then 
		 	continue # skip vol benchmark - takes too long
		fi

		export OPTIONS="--mcMergeBoxes --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcResetRngEachSimulation=true"
		export OPTIONS="$OPTIONS --USE_DOMAINS_FILE $DEFAULT_OPTIONS"
		FULLNAME=$(readlink -e "$BENCHFILE")
		./runCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 >> $DATAFILE
	done	
done

echo ">> csv file done. Preparing report... <<"
python report.py table3 ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"

