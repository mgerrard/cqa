#!/bin/bash

MAXSAMPLES=30000
MINSAMPLES=100

for SEEDFILE in "../inputs/seeds-30-1"
do
	FULLNAME_SEEDFILE=$(readlink -e "$SEEDFILE")
	echo "SEEDFILE: $SEEDFILE, MAXSAMPLES:$MAXSAMPLES"
	DEFAULT_OPTIONS="--mcSeedFile ${FULLNAME_SEEDFILE} --mcNumExecutions 1 --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"

	# for BENCHFILE in "../inputs/jpf/apollo_cons_leaves" "../inputs/jpf/turnlogic_cons_leaves" "../inputs/jpf/conflict_cons_leaves" #
	# do
	# 	echo "================"
	# 	echo $BENCHFILE
	# 	echo "with partitioning and cache, merged boxes, cache for realpaver (1 execution, 30k, resetStream, reseed every sim.)"
	# 	export OPTIONS="--mcMergeBoxes --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcResetRngEachSimulation=true"
	# 	export OPTIONS="$OPTIONS --rangeLO -100 --rangeHI 100 $DEFAULT_OPTIONS"
	# 	FULLNAME=$(readlink -e "$BENCHFILE")
	# 	./runCounter.sh $FULLNAME $FULLNAME
	# done

	for BENCHFILE in ../inputs/pldi/*-* #iterate over files with an hyphen (the benchmark files)
	do
		echo "================"
		echo $BENCHFILE
		if [[ "$BENCHFILE" != *"example-vol-16" ]]
		then 
		  	continue # skip vol benchmark - takes too long
		fi

		echo "with partitioning and cache, merged boxes, cache for realpaver (1 execution, 30k, resetStream, reseed every sim.)"
		export OPTIONS="--mcMergeBoxes --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcResetRngEachSimulation=true"
		export OPTIONS="$OPTIONS --USE_DOMAINS_FILE $DEFAULT_OPTIONS"
		FULLNAME=$(readlink -e "$BENCHFILE")
		./runCounter.sh $FULLNAME $FULLNAME
	done	
done
