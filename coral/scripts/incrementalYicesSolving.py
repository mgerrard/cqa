#!/usr/bin/env python2

import sys, tempfile, os, random, itertools
from subprocess import Popen, PIPE

SHUFFLE=False

def processFile(filename):
    with open(filename) as infile:
        def_stm  = []
        hard_stm = []
        soft_stm = []

        max_weight = 0
        both_stm = []

        for line in infile:
            if line.startswith("(define"):
                def_stm.append(line)
            elif line.startswith("(assert+"):
                last_space_pos = line.rfind(' ')
                weight = int(line[last_space_pos:-2])
                both_stm.append((line,weight))

                if max_weight < weight:
                    max_weight = weight
        
        for t in both_stm:
            if t[1] < max_weight:
                soft_stm.append(t)
            else:
                hard_stm.append(t[0])

        return def_stm,hard_stm,soft_stm

def prepareTmpFile(def_stm,hard_stm,soft_stm):
    tmpfile = tempfile.NamedTemporaryFile(delete=False)
    filename = tmpfile.name
    print "filename: " + filename
    with tmpfile as outfile:
        outfile.writelines(def_stm)
        outfile.writelines(hard_stm)
        for stm in soft_stm:
            outfile.write(stm)
            outfile.write("(check)\n")

    return filename

def runSolver(filename):
    proc = Popen(["yices",filename],bufsize=-1,stdin=PIPE,stdout=PIPE,stderr=PIPE)
    out,err = proc.communicate()
    lines = out.split("\n")
    try:
        index_unsat = lines.index("unsat")
    except ValueError:
        index_unsat = -1

    return index_unsat
        
def cleanAsserts(stms):
    result = []
    for stm in stms:
        clean_stm = stm.replace("assert+", "assert")
        last_space = clean_stm.rfind(' ')
        clean_stm = clean_stm[:last_space] + ")\n"
        result.append(clean_stm)
    return result

def main():
    filename = sys.argv[1]
    def_stm,hard_stm,soft_stm = processFile(filename)
    hard_stm = cleanAsserts(hard_stm) 
    soft_wmap = {}
    soft_stm_clean = cleanAsserts([x[0] for x in soft_stm])
    for i in range(0,len(soft_stm_clean)):
        soft_wmap[soft_stm_clean[i]] = soft_stm[i][1]

    soft_stm = soft_stm_clean
    if (SHUFFLE):
        random.shuffle(soft_stm)

    input_filename = prepareTmpFile(def_stm,hard_stm,soft_stm)
    index_unsat = runSolver(input_filename)
    print "unsat at soft clause #{}: {}".format(index_unsat,soft_stm[index_unsat].strip())
    n_soft = len(soft_stm)
    print "reduction: {} ({}/{})".format(1 - (index_unsat+1)/float(n_soft),index_unsat+1,n_soft)

    #bruteforce the remainder of the clauses
    sliced = soft_stm[:index_unsat+1]
    soft_set = set(sliced)
    
    target = ('',float('inf'),-1)
    candidates = []
    i = 0
    for stm in sliced:
        stm_weight = soft_wmap[stm]
        if stm_weight <= target[1]: #skip constraints with weight bigger than the current candidate
            tmpset = soft_set - set([stm])
            input_filename = prepareTmpFile(def_stm,hard_stm,tmpset)
            index_unsat = runSolver(input_filename)   
            print index_unsat
            if index_unsat < 0:
                if stm_weight < target[1]:
                    target = (stm,stm_weight,i)
                    candidates = []
                    print "updating target to: {}".format(target)
                else:
                    candidates.append((stm,stm_weight,i))
                    print "adding new candidate solution"
        i = i + 1

    candidates.append(target)
    for t in candidates:
        print "brute force result: target id={}, weight={}, content={}".format(t[2],t[1],t[0])

main()
