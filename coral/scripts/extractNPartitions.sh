#!/bin/bash

for BENCHFILE in $(find ../inputs/dwqcoral/ -type f)
do
	DEFAULT_OPTIONS="--mcMaxSamples 2 --mcMinSamplesPerBox 1"
	FULLNAME=$(readlink -e "$BENCHFILE")
	export OPTIONS="--mcPartitionAndCache $DEFAULT_OPTIONS"
	./runDWCounter.sh $FULLNAME $FULLNAME | grep "GREPTHIS" | cut -d ' ' -f 2 | awk -v name="$BENCHFILE" '{print name "\t" $0 }' 

done
