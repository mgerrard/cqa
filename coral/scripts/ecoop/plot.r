#!/usr/bin/env Rscript

library("ggplot2")
library("ggthemes")
library("plyr")

problem.data1 <- read.csv("e1-fulldata", header=TRUE, colClasses=c("seed"="factor","step"="factor"))
problem.data2 <- read.csv("e2-fulldata", header=TRUE, colClasses=c("seed"="factor","step"="factor"))
problem.data3 <- read.csv("e3-fulldata", header=TRUE, colClasses=c("seed"="factor","step"="factor"))
print("finished reading")

#TODO make this a loop
levels(problem.data1$file)[levels(problem.data1$file)=="framingham-hypten-0"] <- "CORONARY (6)"
levels(problem.data1$file)[levels(problem.data1$file)=="framingham-hypten-3"] <- "CORONARY (7)"
levels(problem.data1$file)[levels(problem.data1$file)=="example-cart-12"] <- "CART (4)"
levels(problem.data1$file)[levels(problem.data1$file)=="example-cart-14"] <- "CART (5)"
levels(problem.data1$file)[levels(problem.data1$file)=="framingham-1"] <- "ARTRIAL (2)"
levels(problem.data1$file)[levels(problem.data1$file)=="framingham-2"] <- "ARTRIAL (3)"
levels(problem.data1$file)[levels(problem.data1$file)=="apollo.nodeq"] <- "APOLLO (21)"

levels(problem.data2$file)[levels(problem.data2$file)=="framingham-hypten-0"] <- "CORONARY (6)"
levels(problem.data2$file)[levels(problem.data2$file)=="framingham-hypten-3"] <- "CORONARY (7)"
levels(problem.data2$file)[levels(problem.data2$file)=="example-cart-12"] <- "CART (4)"
levels(problem.data2$file)[levels(problem.data2$file)=="example-cart-14"] <- "CART (5)"
levels(problem.data2$file)[levels(problem.data2$file)=="framingham-1"] <- "ARTRIAL (2)"
levels(problem.data2$file)[levels(problem.data2$file)=="framingham-2"] <- "ARTRIAL (3)"
levels(problem.data2$file)[levels(problem.data2$file)=="apollo.nodeq"] <- "APOLLO (21)"

levels(problem.data3$file)[levels(problem.data3$file)=="framingham-hypten-0"] <- "CORONARY (6)"
levels(problem.data3$file)[levels(problem.data3$file)=="framingham-hypten-3"] <- "CORONARY (7)"
levels(problem.data3$file)[levels(problem.data3$file)=="example-cart-12"] <- "CART (4)"
levels(problem.data3$file)[levels(problem.data3$file)=="example-cart-14"] <- "CART (5)"
levels(problem.data3$file)[levels(problem.data3$file)=="framingham-1"] <- "ARTRIAL (2)"
levels(problem.data3$file)[levels(problem.data3$file)=="framingham-2"] <- "ARTRIAL (3)"
levels(problem.data3$file)[levels(problem.data3$file)=="apollo.nodeq"] <- "APOLLO (21)"


# labeller function
subject_names <- list(
 "framingham-hypten-0"="CORONARY (6)",
 "framingham-hypten-3"="CORONARY (7)",
 "example-cart-12"="CART (4)",
 "example-cart-14"="CART (5)",
 "framingham-1"="ARTRIAL (2)",
 "framingham-2"="ARTRIAL (3)",
 "apollo.nodeq"="APOLLO (21)" 
)

step_names <- list(
  "1000"="1k",
  "10000"="10k"
)

plot_labeller <- function(variable,value){
  if (variable=='file') {
    return(subject_names[value])
  } else if (variable=='step') {
    return(step_names[value])
  } else {
    return(as.character(value))
  }
}
#labeller=plot_labeller



# 1) x-axis wall clock time (1-600 seconds). For each x point we collect both the accuracy and the number of samples 

# aggregate data
problem.data1.varmean <- aggregate(problem.data1$var,list( mode = problem.data1$mode,time = problem.data1$time,
                                                          file = problem.data1$file, step = problem.data1$step),
						 								function(x) sqrt(mean(x)));

problem.data1.samplemean <- aggregate(problem.data1$sample,list( mode = problem.data1$mode,time = problem.data1$time,
                                                          file = problem.data1$file, step = problem.data1$step),
						 								function(x) mean(x));


# plot
ggplot(problem.data1.varmean, aes(x=time,y=x)) + 
  geom_line(size=1,aes(group=mode,colour=mode)) + 
#  facet_wrap(~ step + file) + 
# scale_linetype_discrete() +
  scale_y_log10() + scale_colour_colorblind() + ylab("Average Standard Deviation") + facet_grid(step ~ file) + theme(legend.position = "bottom");

ggsave(file="time-vs-precision.pdf",width=12,height=8);

ggplot(problem.data1.samplemean, aes(x=time,y=x)) + 
  geom_line(size=1,aes(group=mode,colour=mode)) + 
  facet_wrap(~ step + file) + scale_linetype_discrete() +
  scale_y_log10() + scale_colour_colorblind() + ylab("Average Number of Samples") + facet_grid(step ~ file) + theme(legend.position = "bottom");

ggsave(file="time-vs-samples.pdf",width=12,height=8);


## 2) x-axis cumulative num of samples (10k to 1kk). For each x point we collect both accuracy and wall clock time

# aggregate data
problem.data2.varmean <- aggregate(problem.data2$var,list(samples = problem.data2$samples, mode = problem.data2$mode,
					                                      file = problem.data2$file, step = problem.data2$step),function(x) sqrt(mean(x)));

problem.data2.timemean <- aggregate(problem.data2$time,list(samples = problem.data2$samples, mode = problem.data2$mode,
					                                      file = problem.data2$file, step = problem.data2$step),function(x) mean(x));
# plot
ggplot(problem.data2.varmean, aes(x=samples,y=x)) + 
  geom_line(size=1,aes(group=mode,colour=mode)) + 
  facet_wrap(~ step + file) +
  scale_y_log10() + scale_colour_colorblind() + ylab("Average Standard Deviation") + facet_grid(step ~ file)  + guides(colour=FALSE);

ggsave(file="samples-vs-precision.pdf",width=12,height=8);

ggplot(problem.data2.timemean, aes(x=samples,y=x)) + 
  geom_line(size=1,aes(group=mode,colour=mode)) + 
  facet_wrap(~ step + file) +
  scale_colour_colorblind() + ylab("Average Time Spent") + facet_grid(step ~ file) + theme(legend.position = "bottom");

ggsave(file="samples-vs-time.pdf",width=12,height=8);


## 3) table representing the wall clock time and the number of samples required for several different accuracies: 10^-3, 10^-4, 10^-5, 10^-6

# aggregate data
problem.data3.samplemean <- aggregate(problem.data3$samples,list(var = problem.data3$var, mode = problem.data3$mode,
					                                      file = problem.data3$file, step = problem.data3$step),function(x) mean(x));

problem.data3.timemean <- aggregate(problem.data3$time,list(var = problem.data3$var, mode = problem.data3$mode,
					                                      file = problem.data3$file, step = problem.data3$step),function(x) mean(x));
# plot
ggplot(problem.data3.samplemean, aes(x=var,y=x)) + 
  geom_line(size=1,aes(group=mode,colour=mode)) + 
  facet_wrap(~ step + file) +
  scale_y_log10() + scale_colour_colorblind() + ylab("Average Number of Samples") + xlab("Precision (in number of digits)")  + facet_grid(step ~ file)  + guides(colour=FALSE);

ggsave(file="precision-vs-samples.pdf",width=12,height=8);

ggplot(problem.data3.timemean, aes(x=var,y=x)) + 
  geom_line(size=1,aes(group=mode,colour=mode)) + 
  facet_wrap(~ step + file) +
  scale_colour_colorblind() + ylab("Average Time Spent") + xlab("Precision (in number of digits)")  + facet_grid(step ~ file) + theme(legend.position = "bottom");

ggsave(file="precision-vs-time.pdf",width=12,height=8);


