#!/bin/bash

OUTPUT_FOLDER="../../output/incremental"
ARG_FILE="toRun"

echo "> output folder is: ${OUTPUT_FOLDER}"
for file in "${OUTPUT_FOLDER}"/*.csv
do
	mv "$file" "${file}.old"
done

echo "> preparing file with commands to be executed ($ARG_FILE)..."
./prepare_ecoop_experiments.py > "$ARG_FILE"

echo "> starting parallel execution..."
echo "> remember to kill lingering java processes if you interrupt this script!"
parallel --gnu --joblog parlog -v --progress --colsep '\t' -j3 --header : "./run_with_timeout.sh {file} {mode} {seed} '{args}' >> {output}" < "$ARG_FILE"

echo "> done"
