#!/bin/bash

./run_qcoral.sh $1 $1 | \
	grep "\[qCORAL-iterative\] samples" | tr -s '= ' ' ' | cut -d ' ' -f 3,5,7,9 | \
	awk -v mode="$2" -v seed="$3" '{printf "%s,%s,%s,%s,%s,%s\n",$1,$2,$3,$4,mode,seed}'

