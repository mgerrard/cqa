#!/usr/bin/env python

import sys,re,csv,math

REGEX=re.compile(r"(.+?)-(\d+)-(1e-\d\d)-(\d+?).csv")

def timeExperiment(lines,name,step):
    maxtime = 0
    for row in csv.reader(lines):
        time = math.floor(float(row[3]))
        if time - maxtime >= 1:
            maxtime = time
            row[3] = str(time)
            row.append(name)
            row.append(step)
            print ",".join(row)

def sampleExperiment(lines,name,step):
    maxsamples = 50000
    for row in csv.reader(lines):
        samples = int(row[0])
        if samples > 1149999:
            break
        elif samples > maxsamples:
            row[0] = str(maxsamples)
            maxsamples = maxsamples + 50000
            row.append(name)
            row.append(step)
            print ",".join(row)


def precisionExperiment(lines,name,step):
    i = 0
    for row in csv.reader(lines):
        var = float(row[2])
        if i % 100 == 0:
            row[2] = str(var)
            row.append(name)
            row.append(step)
            print ",".join(row)
        i = i + 1

def main():
    files = sys.argv[2:]
    mode = sys.argv[1]
    print "samples,mean,var,time,mode,seed,file,step"

    for filename in files:
        with open(filename) as infile:
            #ignore first and last
            lines = infile.readlines()[1:-1]
            match = REGEX.match(filename)
            basename = match.group(1)
            basename = basename[basename.rfind("/")+1:]
            step = match.group(2)

            if mode == "e1":
                timeExperiment(lines,basename,step)
            elif mode == "e2":
                sampleExperiment(lines,basename,step)
            elif mode == "e3":
                precisionExperiment(lines,basename,step)
            else:
                print "unknown experiment: " + mode
                break


main()
