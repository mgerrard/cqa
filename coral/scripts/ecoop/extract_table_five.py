#!/usr/bin/env python2

import sys,csv,math,pickle,re
from collections import defaultdict

REGEX=re.compile(r"(.+?)-(\d+)-(1e-\d\d)-(\d+?).csv")

HEADER=r"""
\begin{figure}[t!]
  \footnotesize
\centering
\begin{tabular}{|c|c|r|r|r|r|}
\hline

\multirow{2}{*}{Assertion} &
\multirow{2}{*}{Precision} &
\multicolumn{4}{c|}{Time} \\

\cline{3-6}
& &
\texttt{Baseline} &
\texttt{\itOne{}} &
\texttt{\itThree} &
\texttt{\itTwo{}} \\

\hline
"""

FOOTER=r"""
%% START FOOTER
\end{tabular}
\caption{\label{fig:dynamic-budget-allocation} Comparison of different
  incremental approaches when fixing the goal accuracy to 1, 2, and 3
  decimal places.  In the first iteration, all variations of \tname{}
  take 50K samples, equally distributed across the constraint
  partitions. In the following iterations each technique draw 1K
  differently until reaching the goal precision.}
\end{figure}
\normalsize
"""

SUBJECT=r"""
\multicolumn{6}{c}{\textbf{$SUBJECT$}}\\
\hline
\multirow{3}{*}{$ASSERTION$} 
"""

ENTRY=r"""
&  $10^{{ {} }}$  & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\
"""

SUBJECT_FOOTER=r"""
\hline
"""


def extractTable(lines):
    i = 2
    target = math.pow(10,-1*i)
    data = []
    for row in csv.reader(lines):
        var = float(row[2])
        while var < target:
            time = float(row[3])
            estimate = float(row[1])
            mode = row[4]
            data.append((target,estimate,time,mode))
            i = i + 2
            target = math.pow(10,-1*i)            
    return data

def load_results(files):
    #file -> step -> mode -> var ->(esmean,timemean)
    results = defaultdict(lambda : defaultdict(lambda : defaultdict(lambda : defaultdict(list))))

    for filename in files:
        with open(filename) as infile:
            #ignore first and last
            lines = infile.readlines()[1:-1]
            match = REGEX.match(filename)
            basename = match.group(1)
            basename = basename[basename.rfind("/")+1:]
            step = match.group(2)

            filedata = extractTable(lines)
            for entry in filedata:
                var,est,time,mode = entry;
                results[basename][step][var][mode].append((est,time))

    for k1,m1 in results.iteritems():
        for k2,m2 in m1.iteritems():
            for k3,m3 in m2.iteritems():
                for mode,datalist in m3.iteritems():
                    size = len(datalist)
                    averaged = [sum(x)/size for x in zip(*datalist)]
                    m3[mode] = averaged

    return results

def dump_tsv(results,table_par):
    modes = ["default","dumb","sensitivity","gradient"]
    indexes = {"est":(0,"{:.6f}\t"),"time":(1,"{:.2f}\t")}
    empty_markers = {"est":"t\t","time":"+30m\t"}
    par_index = indexes[table_par][0]
    lineformat = indexes[table_par][1]

    for filename in results.keys():
        stepmap = results[filename]
        for step in stepmap.keys():
            varmap = stepmap[step]
            for var in sorted(varmap.keys(),reverse=True):
                modemap = varmap[var]

                line = "{}\t{}\t{}\t".format(filename,step,math.sqrt(var))
                for mode in modes:
                    t = modemap[mode]
                    if t:
                        line = line + lineformat.format(t[par_index])
                    else:
                        line = line + empty_markers[table_par]
                print line

def dump_table(results,table_par,stepsize):
    print HEADER
    modes = ["default","dumb","sensitivity","gradient"]    
    indexes = {"est":(0,"{:.6f}\t"),"time":(1,"{:.2f}\t")}
    empty_markers = {"est":"t","time":-123.456}
    filemap = {"framingham-hypten-0":("CORONARY",6),
               "framingham-hypten-3":("CORONARY",7),
               "example-cart-12":("CART",4),
               "example-cart-14":("CART",5),
               "framingham-1":("ARTRIAL",2),
               "framingham-2":("ARTRIAL",3),
               "apollo.nodeq":("APOLLO",21)
    }
    par_index = indexes[table_par][0]
    lineformat = indexes[table_par][1]
    for filename in sorted(results.keys()):
        name,assertion = filemap[filename]
        print SUBJECT.replace("$SUBJECT$",str(name)).replace("$ASSERTION$",str(assertion))

        stepmap = results[filename]
        varmap = stepmap[stepsize]
        for var in sorted(varmap.keys(),reverse=True):
            modemap = varmap[var]
            args = []
            args.append(int(math.log10(var)/2))
            for mode in modes:
                t = modemap[mode]
                if t:
                    args.append(t[par_index])
                else:
                    args.append(empty_markers[table_par])
            print ENTRY.format(*args)
        print SUBJECT_FOOTER

    print FOOTER





def main():
    files = sys.argv[3:]
    mode = sys.argv[1]
    table_par = sys.argv[2]

    results = load_results(files)
#    if mode == "pickle":
#        print "picking doesn't work with lambdas"
#        with open('data.pkl','rb') as infile:
#            results = pickle.load(infile)
#    else:
#        results = load_results(files)
#        with open('data.pkl','wb') as infile:
#            pickle.dump(results,infile)
 
#    print results
    if mode == "table":
        dump_table(results,table_par,"1000")
    elif mode == "tsv":
        dump_tsv(results,table_par)

main()
    
