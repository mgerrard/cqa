#!/bin/bash

DATAFILE="/home/mateus/workspace/coral/output/distaware/distaware.csv"
printf "samples,estimate,variance,time,file\n" > $DATAFILE

SUBJECTS=( \
"../../inputs/dwqcoral/complex/apollo.nodeq" \
"../../inputs/dwqcoral/complex/conflict.nodeq" \
"../../inputs/dwqcoral/complex/turn.nodeq" \
"../../inputs/dwqcoral/framingham-0" \
"../../inputs/dwqcoral/framingham-1" \
"../../inputs/dwqcoral/framingham-2" \
"../../inputs/dwqcoral/example-cart-12" \
"../../inputs/dwqcoral/example-cart-14" \
"../../inputs/dwqcoral/example-invPend-0" \
"../../inputs/dwqcoral/example-carton-5-0" \
"../../inputs/dwqcoral/example-carton-5-1" \
"../../inputs/dwqcoral/example-carton-5-2" \
"../../inputs/dwqcoral/example-carton-5-5" \
"../../inputs/dwqcoral/example-carton-5-18" \
"../../inputs/dwqcoral/example-carton-5-20" \
"../../inputs/dwqcoral/example-carton-5-22" \
"../../inputs/dwqcoral/example-ckd-epi-0" \
"../../inputs/dwqcoral/example-ckd-epi-1" \
"../../inputs/dwqcoral/example-ckd-epi-simple-0" \
"../../inputs/dwqcoral/example-ckd-epi-simple-1" \
"../../inputs/dwqcoral/framingham-hypten-0" \
"../../inputs/dwqcoral/framingham-hypten-3" \
"../../inputs/dwqcoral/exponential/apollo.nodeq" \
"../../inputs/dwqcoral/exponential/conflict.nodeq" \
"../../inputs/dwqcoral/exponential/turn.nodeq" \
"../../inputs/dwqcoral/exponential/framingham-0" \
"../../inputs/dwqcoral/exponential/framingham-1" \
"../../inputs/dwqcoral/exponential/framingham-2" \
"../../inputs/dwqcoral/exponential/example-carton-5-0" \
"../../inputs/dwqcoral/exponential/example-carton-5-1" \
"../../inputs/dwqcoral/exponential/example-carton-5-2" \
"../../inputs/dwqcoral/exponential/example-carton-5-5" \
"../../inputs/dwqcoral/exponential/example-carton-5-18" \
"../../inputs/dwqcoral/exponential/example-carton-5-20" \
"../../inputs/dwqcoral/exponential/example-carton-5-22" \
"../../inputs/dwqcoral/exponential/example-ckd-epi-0" \
"../../inputs/dwqcoral/exponential/example-ckd-epi-1" \
"../../inputs/dwqcoral/exponential/example-ckd-epi-simple-0" \
"../../inputs/dwqcoral/exponential/example-ckd-epi-simple-1" \
"../../inputs/dwqcoral/exponential/framingham-hypten-0" \
"../../inputs/dwqcoral/exponential/framingham-hypten-3" \
)

SEEDFILE="../../inputs/seeds-5-cp" 
IFS=$'\r\n' 
SEEDS=($(cat "$SEEDFILE"))

for BENCHFILE in "${SUBJECTS[@]}"
do
	echo $BENCHFILE
	for SEED in "${SEEDS[@]}"
	do
		#if maxsamples << initialPartitionBudget, we don't perform any iterative improvement steps
		DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples 1 --mcInitialPartitionBudget 100000"
		if [[ $BENCHFILE == "*nodeq*" ]] 
		then
			DEFAULT_OPTIONS="$DEFAULT_OPTIONS --rangeLO -100 --rangeHI 100"
		fi

		FULLNAME=$(readlink -e "$BENCHFILE")
		echo -n "."
		export OPTIONS="--mcIterativeImprovement --mcNonRankedIterativeImprovement --mcTargetVariance 1E-30 $DEFAULT_OPTIONS"
		./run_qcoral.sh $FULLNAME $FULLNAME | \
			grep "\[qCORAL:results\]" | tr -s '=, ' ' ' | cut -d ' ' -f 3,5,7,9 | \
			awk -v file="$FULLNAME" '{printf "%s,%s,%s,%s,%s\n",$1,$2,$3,$4,file}' \
			>> "$DATAFILE"

	done	
done


echo "> done"
