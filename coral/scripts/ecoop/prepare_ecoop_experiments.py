#!/usr/bin/env python2

import sys,itertools,os

INITIAL_SAMPLES = (50000,)
MAX_SAMPLES = (100000000000,)
STEP_SIZE = (1000,10000)
PRECISION = (1E-30,)
SEEDFILE="../../inputs/seeds-5-cp-2"
BENCHMARKS=("../../inputs/dwqcoral/complex/apollo.nodeq",
"../../inputs/dwqcoral/example-cart-12",
"../../inputs/dwqcoral/example-cart-14",
"../../inputs/dwqcoral/framingham-1",
"../../inputs/dwqcoral/framingham-2",
"../../inputs/dwqcoral/framingham-hypten-0",
"../../inputs/dwqcoral/framingham-hypten-3")
MODE=(("--mcIterativeImprovementBZZZT--mcNonRankedIterativeImprovement","default"), 
      ("--mcIterativeImprovementBZZZT--mcProportionalBoxSampleAllocationBZZZT--mcDumbRankingHeuristic","dumb"),
      ("--mcIterativeImprovementBZZZT--mcProportionalBoxSampleAllocation","sensitivity"),
      ("--mcIterativeImprovementBZZZT--mcAllocateBudgetProportionallyBZZZT--mcProportionalBoxSampleAllocation","gradient"))
TEMPLATE="--mcSeedBZZZT{seed}BZZZT--mcMaxSamplesBZZZT{maxsamples}" + \
"BZZZT--mcInitialPartitionBudget={initialbudget}BZZZT--mcTargetVariance={precision}" + \
"BZZZT--mcSamplesPerIncrement={step}BZZZT{complexrange}BZZZT{mode}"

def main():
    seeds = []
    with open(SEEDFILE) as seedfile:
        seeds = [x.strip() for x in seedfile.readlines()]

    n = 0
    print "file\tmode\targs\toutput\tseed"
    for i in itertools.product(seeds,MAX_SAMPLES,INITIAL_SAMPLES,STEP_SIZE,PRECISION,BENCHMARKS):
        par_map = {"seed":i[0],
                   "maxsamples":i[1],
                   "initialbudget":i[2],
                   "step":i[3],
                   "precision":i[4],
                   "complexrange":"--rangeLOBZZZT-100BZZZT--rangeHIBZZZT100" if "complex" in i[5] else ""
        }
        fullname = os.path.abspath(i[5])
        basename = fullname[fullname.rfind("/") + 1:]
       
        for m in MODE:
            output_file = "../../output/incremental/{}-{}-{}-{}.csv".format(basename,i[3],i[4],n)
            n = n+1
            #write csv header
            with open(output_file,'w') as csvfile:
                csvfile.write("samples,mean,var,time,mode,seed\n")
            par_map["mode"] = m[0]
            args = TEMPLATE.format(**par_map)
            print '{}\t{}\t{}\t{}\t{}'.format(fullname,m[1],args,output_file,par_map['seed'])
            
main()
