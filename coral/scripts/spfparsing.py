import lrparsing,sys
from lrparsing import Keyword, List, Prio, Ref, THIS, Token, Tokens, GrammarError, ParseError

class ExprParser(lrparsing.Grammar):
    #
    # Put Tokens we don't want to re-type in a TokenRegistry.
    #
    class T(lrparsing.TokenRegistry):
        integer = Token(re="[0-9]+")
        floating = Token(re="[0-9]+\.[0-9]+")
        floating["key"] = "I a mapping!"
        ident = Token(re="(CONST_-?[0-9]+\.?[0-9]*)|([A-Za-z_][A-Za-z_0-9]*)")

    #
    # Grammar rules.
    #
    expr = Ref("expr")                # Forward reference
    atom = T.ident | T.floating | T.integer | Token('(') + expr + ')'
    expr = Prio(                      # If ambiguous choose atom 1st, ...
        atom,
        Tokens("-") >> THIS,      # >> means right associative
        THIS << Tokens("* / // %") << THIS,
        THIS << Tokens("+ -") << THIS,# THIS means "expr" here
        THIS << Tokens("== <= >= < > !=") << THIS,
        THIS << Tokens("&&") << THIS)

    expr["a"] = "I am a mapping too!"
    START = expr                      # Where the grammar must start

OPS = {"+":"ADD","-":"SUB","*":"MUL","/":"DIV","<=":"$LE","<":"$LT",">=":"$GE",">":"$GT", "&&":";", "==":"$EQ", "!=":"$NE"}
VAR_TO_ID = {}
VAR_ID_COUNTER = 1

def handle_var(varid,dvar=True):
    global VAR_TO_ID, VAR_ID_COUNTER
    if varid in VAR_TO_ID:
        return VAR_TO_ID[varid]
    else:
        idcounter = VAR_ID_COUNTER
        VAR_ID_COUNTER = VAR_ID_COUNTER + 1
        if dvar:
            expr = "DVAR(ID_" + str(idcounter) + ")"
        else:
            expr = "IVAR(ID_" + str(idcounter) + ")"
        VAR_TO_ID[varid] = expr
        return expr

def visit_tree(expr,opts={}):
    global OPS
    rule = expr[0]
#    print ExprParser.repr_parse_tree(expr) + "\n"

    if rule == ExprParser.START:
        return visit_tree(expr[1],opts)
    elif rule == ExprParser.expr:
        length = len(expr)
#        print "expr: " + str(length)
#        print expr[1]
        if length == 3 and expr[1][1] == '-': #unary op
            return visit_tree(expr[2],{"neg":True})
        elif length == 2:                        #atom
            return visit_tree(expr[1],opts)
        else:                                    #binop
#           print "binop"
            operation = OPS[expr[2][1]] #get string
            r1 = visit_tree(expr[1],opts)
            r2 = visit_tree(expr[3],opts)

            if operation == ";":
                if r1[1] == "boolean" and r2[1] == "boolean":
                    return (r1[0] + ";" + r2[0],"boolean")
                else:
                    print r1
                    print r2
                    raise GrammarError
            else:
                if {r1[1],r2[1]} <= {"double", "int"}:
                    isBoolean = operation in ["$LE","$LT","$GE","$GT","$EQ","$NE"] #boolean ops
                    if r1[1] == r2[1]:
                        operation = operation.replace("$","I" if r1[1] == "int" else "D")
                        return (operation + "(" + r1[0] + "," + r2[0] + ")","boolean" if isBoolean else r1[1])
                    else:
                        print "mixed type found! " + r1[1] + " " + r2[1]
                        operation = operation.replace("$","D")
                        r1_cast = r1[0] if r1[1] == "double" else "ASDOUBLE(" + r1[0] + ")"
                        r2_cast = r2[0] if r2[1] == "double" else "ASDOUBLE(" + r2[0] + ")"
                        return (operation + "(" + r1_cast + "," + r2_cast + ")","boolean" if isBoolean else r1[1])
                else:
                    print r1
                    print r2
                    raise GrammarError

                    
    elif rule == ExprParser.atom:
        length = len(expr)
        if length == 2:
            return visit_tree(expr[1],opts)
        elif length == 4: #parens
            return visit_tree(expr[2],opts)
        else:
            raise GrammarError
    elif rule == ExprParser.T.integer:
        minus = "-" if opts.get("neg",False) else ""
        return ("ICONST(" + minus + expr[1] + ")","int")
    elif rule == ExprParser.T.floating:
        minus = "-" if opts.get("neg",False) else ""
        return ("DCONST(" + minus + expr[1] + ")","double")
    elif rule == lrparsing.Token("("):
        print "paren: " + str(expr)
        return ("","")    
    elif rule == ExprParser.T.ident:
        #TODO check for SYMREAL and SYMINT and CONST
        result = "nonsense"
        exprtype = "nonsense"


        if expr[1].startswith("CONST_"):
            if "." in expr[1]:
                result = "DCONST(" + expr[1][6:] + ")"
                exprtype = "double"
            else:
                result = "ICONST(" + expr[1][6:] + ")"
                exprtype = "int"
        elif expr[1].endswith("SYMREAL"):
            result = handle_var(expr[1],dvar=True)
            exprtype = "double"
        elif expr[1].endswith("SYMINT"):
            result = handle_var(expr[1],dvar=False)
            exprtype = "int"
        else:
            print "no var type found for" + str(expr[1])
            raise GrammarError
            
        if opts.get("neg",False):
            if exprtype == "int":
                return ("MUL(ICONST(-1)," + result + ")","int")
            elif exprtype == "double":
                return ("MUL(DCONST(-1)," + result + ")","double")
            else:
                print type(rule)
                print expr
                raise GrammarError
        else:
            return (result,exprtype)
    else:
        print type(rule)
        print expr
        raise GrammarError

def parse_constraints(infile,outfile):
    for line in infile:
#        print line; i = i + 1
        cons = visit_tree(ExprParser.parse(line))[0]
        outfile.write(cons + "\n")
    
def main():
    if len(sys.argv) == 1: #pipe mode
        for line in sys.stdin:
            try:
                print visit_tree(ExprParser.parse(line))[0]
            except ParseError:
                print "error while parsing from pipe: " + line 
                traceback.print_exc()
    else:
        for filename in sys.argv[1:]: #file mode
            print filename
            try:
                with open(filename) as infile:
                    with open(filename + ".coral",'w') as outfile:
                        parse_constraints(infile,outfile)
            except ParseError:
                print "error while parsing " + filename
                traceback.print_exc()
main()


    
#parse_tree = ExprParser.parse("0.798 * r_4-0.411 * r_5 <= -0.471458")
#print(ExprParser.repr_parse_tree(parse_tree))
#print(visit_tree(parse_tree))
