#!/bin/bash
echo ">> Table1: small experiment <<"

BENCHFILE="../inputs/table1"
DEFAULT_OPTIONS="--mcNumExecutions 1 --mcMaxSamples=10000 --mcMinSamplesPerBox 100 --rangeLO=-1 --rangeHI=1"
FULLNAME=$(readlink -e "$BENCHFILE")
echo $FULLNAME			
################# "pure monte carlo (1 executions) - no rp"
echo "Pure Monte Carlo:"
export OPTIONS="--normalize=false --mcSkipPaving --mcResetRngEachSimulation=false $DEFAULT_OPTIONS"
./runCounter.sh $FULLNAME $FULLNAME | grep "variance results\|volume estimate"

################ "with partitioning and cache, merged boxes, cache for realpaver (1 execution, reseed every sim.)"
echo "Monte Carlo + Stratified Sampling:"
export OPTIONS="--normalize=false --intervalSolverExtraArgs='Consistency,strong=3B;' --mcResetRngEachSimulation=false --intervalSolverMaxBoxes=10 $DEFAULT_OPTIONS"
./runCounter.sh $FULLNAME $FULLNAME | grep "variance results\|volume estimate"
