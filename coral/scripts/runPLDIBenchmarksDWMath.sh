#!/bin/bash

DATAFILE="../output/data-tabledw-pldi-math.csv"
REPORTFILE="../output/report-tabledw-pldi-math.txt"
LOGFILE="../output/log-execution-dwmath"
PARALLELFILE="parallel-pldi-math-jobs"
echo ">> Table DW: running qCoral+mathematica for each benchmark<<"

RUNVOL="false"
if [[ "$1" == "--with-vol" ]]
then
	RUNVOL="true"
	echo ">> WARNING: VOL benchmark is included on the work set. This will take a *long* time (weeks)."
else
	echo ">> WARNING: VOL benchmark is *not* included by default. To include it, re-execute this script with '--with-vol'"
fi

echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,mode" > $DATAFILE

rm ${PARALLELFILE}
mv ${DATAFILE} "${DATAFILE}.old"
for BENCHFILE in ../inputs/dwqcoral/exponential/* #iterate over files with an hyphen (the benchmark files)
do
	if ! [[ -f $BENCHFILE ]]
	then
		continue #skip directories, etc...
	fi

	if [[ $RUNVOL == "false" && "$BENCHFILE" == *"example-vol-16" ]]
	then 
		continue # skip vol benchmark - takes too long
	fi

	echo $BENCHFILE
	FULLNAME=$(readlink -e "$BENCHFILE")
	BASENAME=$(basename "$BENCHFILE")
	LOGNAME="${LOGFILE}-$BASENAME"

	# export OPTIONS="--mcUseMathematica --mcUseNProbability"
	# ./runDWCounter.sh $FULLNAME $FULLNAME &> log_pldi_math_dw
	# grep "regCoral-csvresults" log_pldi_math_dw | cut -d ' ' -f 2- | tail --lines=+2 | tr -d '\n' >> $DATAFILE
	# echo ",NProbability" >> $DATAFILE

	# export OPTIONS="--mcUseMathematica"
	# ./runDWCounter.sh $FULLNAME $FULLNAME &> log_pldi_math_sym_dw
	# grep "regCoral-csvresults" log_pldi_math_sym_dw | cut -d ' ' -f 2- | tail --lines=+2 | tr -d '\n' >> $DATAFILE
	# echo ",Probability" >> $DATAFILE
	export OPTIONS="--mcUseMathematica --mcUseNProbability"
#	echo "./runDWCounter.sh $FULLNAME $FULLNAME |  grep 'regCoral-csvresults' | cut -d ' ' -f 2- | tail --lines=+2" >> parallel-pldi-math-jobs
	./runDWCounter.sh $FULLNAME $FULLNAME &> ${LOGNAME} 
        grep 'regCoral-csvresults' ${LOGNAME} | cut -d ' ' -f 2- | tail --lines=+2 >> ${DATAFILE}
	
done

#parallel -j 1 < parallel-pldi-math-jobs > ${DATAFILE}

echo ">> csv file done.<<"
cat ${DATAFILE}
#python report.py table3 ${DATAFILE} > ${REPORTFILE}
#echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
