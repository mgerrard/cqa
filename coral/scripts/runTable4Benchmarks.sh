#!/bin/bash

SEEDFILE=../inputs/seeds-30-cp

IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

for SEED in "${SEEDS[@]}"
do
	echo "%%%%%%%%%%%%% seed=$SEED %%%%%%%%%%%%%%%"

	if [[ "$SEED" == \#* ]] 
	then 
		continue
	fi

	for BENCHFILE in "../inputs/jpf/apollo_cons_leaves_sample70" "../inputs/jpf/turnlogic_cons_leaves_sample70" "../inputs/jpf/conflict_cons_leaves_sample70"
	do
		echo "%%%%%%%%%%%%% benchfile=$BENCHFILE %%%%%%%%%%%%%"
		for MAXSAMPLES in "1000" "10000" "100000"
		do
			echo "%%%%%%%%%%%%%%% maxsamples=$MAXSAMPLES %%%%%%%%%%%%%%%"

			DEFAULT_OPTIONS="--mcSeed $SEED --mcNumExecutions 1 --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox 100 --rangeLO -100 --rangeHI 100"

			# echo "================"
			# echo "Mathematica (1 executions, 1k) - no rp"
			# export OPTIONS="--mcSkipPaving $DEFAULT_OPTIONS --mcUseMathematica"
			# FULLNAME=$(readlink -e "$BENCHFILE")
			# echo "$FULLNAME"
			# ./runCounter.sh $FULLNAME $FULLNAME

			# echo "================"
			# echo "pure monte carlo (1 executions,1k) - no rp"
			# export OPTIONS="--mcSkipPaving --mcResetRngEachSimulation=false $DEFAULT_OPTIONS"
			# FULLNAME=$(readlink -e "$BENCHFILE")
			# ./runCounter.sh $FULLNAME $FULLNAME

			echo "================"
			echo "monte carlo + realpaver (1 executions,1k) - merged boxes"
			export OPTIONS="--mcMergeBoxes --mcResetRngEachSimulation=false $DEFAULT_OPTIONS"
			FULLNAME=$(readlink -e "$BENCHFILE")
			./runCounter.sh $FULLNAME $FULLNAME

			echo "================"
			echo $BENCHFILE
			echo "with partitioning and cache, merged boxes, cache for realpaver (1 execution, 30k, resetStream, reseed every sim.)"
			export OPTIONS="--mcMergeBoxes --mcPartitionAndCache --mcCallIntSolPartition --mcResetRngEachSimulation=true"
			export OPTIONS="$OPTIONS $DEFAULT_OPTIONS"
			FULLNAME=$(readlink -e "$BENCHFILE")
			./runCounter.sh $FULLNAME $FULLNAME

		done
	done
done
