#!/bin/bash

DATAFILE="../output/data-tabledw-complex-math.csv"
REPORTFILE="../output/report-tabledw-complex-math.txt"

echo ">> Table DW: running mathematica for each benchmark<<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,mode" > $DATAFILE

echo -n "."
for BENCHFILE in ../inputs/dwqcoral/complex/*.nodeq #iterate over files with an hyphen (the benchmark files)
do
	if ! [[ -f $BENCHFILE ]]
	then
		continue #skip directories, etc...
	fi

	echo $BENCHFILE
	FULLNAME=$(readlink -e "$BENCHFILE")

	export OPTIONS="--mcUseMathematica --mcUseNProbability"
	./runDWCounter.sh $FULLNAME $FULLNAME &> log_complex_math_dw
	grep "regCoral-csvresults" log_complex_math_dw | cut -d ' ' -f 2- | tail --lines=+2 | tr -d '\n' >> $DATAFILE
	echo ",NProbability" >> $DATAFILE

	export OPTIONS="--mcUseMathematica"
	./runDWCounter.sh $FULLNAME $FULLNAME &> log_complex_math_sym_dw
	grep "regCoral-csvresults" log_complex_math_sym_dw | cut -d ' ' -f 2- | tail --lines=+2 | tr -d '\n' >> $DATAFILE
	echo ",Probability" >> $DATAFILE
done

echo ">> csv file done.<<"
cat ${DATAFILE}
#python report.py table3 ${DATAFILE} > ${REPORTFILE}
#echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
