import lrparsing,sys,traceback
from lrparsing import Keyword, List, Prio, Ref, THIS, Repeat, Token, Tokens, GrammarError, ParseError

class ExprParser(lrparsing.Grammar):
    #
    # Put Tokens we don't want to re-type in a TokenRegistry.
    #
    class T(lrparsing.TokenRegistry):
        integer = Token(re="-?[0-9]+")
        floating = Token(re="-?[0-9]+\.[0-9]+")
        ident = Token(re="[A-Za-z_][A-Za-z_0-9.]*")
    #
    # Grammar rules.
    #
    expr = Ref("expr")       # Forward reference
    value = T.floating | T.integer
    symbol = T.ident 
    expr = Prio(
        Token('(') + Keyword("assertplus")  << THIS << value  + Token(')'), #expression + weight
        Token('(') + (Tokens("=> /= = < <= > >= + - / *") | Keyword("div")) << THIS << THIS + Token(')'), #binary expression
        Token('(') + (Keyword ("and") | Keyword("or")) << Repeat(expr,min=2) + Token(')'), # and/or may have multiple
        Token('(') + Keyword("not") << THIS  + Token(')'),
        value,
        symbol
        )
    START = expr                      # Where the grammar must start


OPS = {"+":"ADD","-":"SUB","*":"MUL","/":"DIV","=":"$EQ","/=":"$NE","<":"$LT","<=":"$LE",">":"$GT",">=":"$GE","and":"BAND","or":"BOR","not":"BNOT","div":"INTDIV_"}
ID_COUNTER = 1
SYMBOL_TABLE = dict()

def get_id(symbol):
    global SYMBOL_TABLE, ID_COUNTER
    if symbol in SYMBOL_TABLE:
        return SYMBOL_TABLE[symbol]
    else:
        SYMBOL_TABLE[symbol] = ID_COUNTER
        ID_COUNTER += 1
        return ID_COUNTER - 1
    
def visit_tree(expr,opts={}):
    global OPS
    rule = expr[0]
#    print "\n" + ExprParser.repr_parse_tree(expr) 
#    print rule
    if rule == ExprParser.START:
        arg = visit_tree(expr[1],opts)
#        print arg
        return arg
    elif rule == ExprParser.expr:
        length = len(expr)
        if length == 5: #unary op
#            print "unary now!"
            arg = visit_tree(expr[3],opts)[0]
            return (''.join((OPS[expr[2][1]],"(",arg,")")),"tuples_need_to_have_more_than_a_single_element")
        elif length == 2:                        #symbol
#           print "symbol expr now!"
            return visit_tree(expr[1],opts)
        else:                                    #binop
#           print "binop now!"
            if expr[2][1] == "=>": # boolean implication: a -> b :>> not(a) or b
                return ''.join(("BOR(BNOT(",visit_tree(expr[3],opts)[0],"),",visit_tree(expr[4],opts)[0],")"))
            elif expr[2][1] == "assertplus": #constraint weight
#                print ExprParser.repr_parse_tree(expr)
                arg = visit_tree(expr[3],opts)
                if isinstance(arg,tuple):
                    arg = arg[0]
                return ''.join((expr[4][1][1],"##",arg))
            else:
                operation = OPS[expr[2][1]] #get string
                results = []
                types = set()
                coral_expr = ""

                #fold constants for rationals
                if operation == 'DIV' and expr[3][1][0] == ExprParser.value and expr[4][1][0] == ExprParser.value:
                    n1 = float(expr[3][1][1][1])
                    n2 = float(expr[4][1][1][1])
                    n3 = n1 / n2
                    coral_expr = "DCONST(" + str(n3) + ")"
                    types.add("double")
                    
                else:
                    for expr_arg in expr[3:-1]: #handle operations with variable args; skip last parenthesis
                        # print "varargs:"
                        r = visit_tree(expr_arg,opts)
                        results.append(r)
                        types.add(r[1])

                    cast_to_double = len(types) >= 2 and operation not in ("BAND", "BOR")
                    if "$" in operation and (cast_to_double or "double" in types):
                        operation = operation.replace("$","D")
                    else:
                        operation = operation.replace("$","I")

                    args = []
                    args.append(operation)
                    args.append("(")
                    args.append(cast_if_int(results[0],cast_to_double))
                    args.append(",")
                    args.append(cast_if_int(results[1],cast_to_double))
                    args.append(")")
                    
                    coral_expr = ''.join(args)
                    for coral_arg in results[2:]:
                        args = []
                        args.append(operation)
                        args.append("(")
                        args.append(coral_expr)
                        args.append(",")
                        args.append(cast_if_int(coral_arg,cast_to_double))
                        args.append(")")
                        coral_expr = ''.join(args)
                return (coral_expr, "double" if len(types) >= 2 else types.pop())

    elif rule == ExprParser.value:
        rvalue = ''.join(("ICONST(",expr[1][1],")"))
        type = "int"
        return (rvalue,type)
    # elif rule == ExprParser.T.floating:
    #     return "DCONST(" + expr[1][1] + ")"
    elif rule == ExprParser.symbol:
        rvalue = ""
        type = ""
        var_id = expr[1][1]
        if var_id.startswith("d"):
            rvalue = ''.join(("DVAR(ID_",str(get_id(var_id)),")"))
            type = "double"
        elif var_id.startswith("i"):
            rvalue = ''.join(("IVAR(ID_",str(get_id(var_id)),")"))
            type = "int"
        else:
            raise GrammarError
        return (rvalue,type)
    else:
        raise GrammarError

def cast_if_int(result,should_cast):
    type = result[1]
    if should_cast and type == "int":
        return ''.join(["ASDOUBLE(",result[0],")"])
    else:
        return result[0]

def parse_constraints(infile):
    cons = []
    i = 0
    for line in infile:
#        print line; i = i + 1
        if "assert+" in line:
            hack_line = line.replace("assert+","assertplus")
            cons.append(visit_tree(ExprParser.parse(hack_line)))
    return '\n'.join(cons)

if __name__ == "__main__":
    for filename in sys.argv[1:]:
        print filename
        try:
            with open(filename) as infile:
                coral_cons = parse_constraints(infile)
                with open(filename + ".coral",'w') as outfile:
                    outfile.write(coral_cons)
        except ParseError:
            print "error while parsing " + filename
            traceback.print_exc()

#    print SYMBOL_TABLE
            

    
#parse_tree = ExprParser.parse("0.798 * r_4-0.411 * r_5 <= -0.471458")
#print(ExprParser.repr_parse_tree(parse_tree))
#print(visit_tree(parse_tree))

# Test data:
# (assert+ (=> (= i_173.48.253 0)  (= i_179.51.253 0)) 100000)

# a = sexpression.ExprParser.parse(line)
# print sexpression.visit_tree(a)
