#!/bin/bash

BENCHLIST=(
"../inputs/dwqcoral/twonormal/framingham-1	27"
"../inputs/dwqcoral/twonormal/framingham-2	44"
"../inputs/dwqcoral/twonormal/example-cart-12	45"
"../inputs/dwqcoral/twonormal/example-cart-14	48"
"../inputs/dwqcoral/twonormal/example-carton-5-18	954"
"../inputs/dwqcoral/twonormal/example-carton-5-20	1030"
"../inputs/dwqcoral/twonormal/example-ckd-epi-simple-0	18"
"../inputs/dwqcoral/twonormal/example-invPend-0	1"
"../inputs/dwqcoral/twonormal/example-carton-5-0	40"
"../inputs/dwqcoral/twonormal/example-carton-5-1	37"
"../inputs/dwqcoral/twonormal/example-carton-5-2	38"
"../inputs/dwqcoral/twonormal/example-carton-5-22	1132"
"../inputs/dwqcoral/twonormal/example-carton-5-5	38"
"../inputs/dwqcoral/twonormal/example-ckd-epi-0	46"
"../inputs/dwqcoral/twonormal/example-ckd-epi-1	38"
"../inputs/dwqcoral/twonormal/example-ckd-epi-simple-1	14"
"../inputs/dwqcoral/twonormal/framingham-0	23"
"../inputs/dwqcoral/twonormal/framingham-hypten-0	15"
"../inputs/dwqcoral/twonormal/framingham-hypten-3	8"
)

SEEDFILE="../inputs/seeds-1-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-discretization.csv"
#REPORTFILE="../output/data-allsubjects-table2.txt"

echo ">> Table 4: running qCORAL+discretization for each benchmark (10-100k)<<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,samplebudget,mode" > $DATAFILE

MAXSAMPLES=100000 
echo "=========== $MAXSAMPLES ==========="
for BENCHLINE in "${BENCHLIST[@]}"

do
	BENCHFILE=$(echo "$BENCHLINE" | cut -f 1)
	echo $BENCHFILE
	NPARTS=$(echo "$BENCHLINE" | cut -f 2 )
	NSAMPLES=$(python -c "a=$MAXSAMPLES;b=$NPARTS;c=a%b; print str(a/b) if c == 0 else (a/b + 1)")
	echo "MAX SAMPLES per partition: $NSAMPLES"
	
	for SEED in "${SEEDS[@]}"
	do
		DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples $NSAMPLES --mcMinSamplesPerBox 0"
		FULLNAME=$(readlink -e "$BENCHFILE")
		echo -n "."
		export OPTIONS="--mcDiscretizeWithMateusApproach --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcDiscretizationRegions=3 $DEFAULT_OPTIONS"
		./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk -v budget="$MAXSAMPLES" '{printf "%s,%s,XYZ-discrete-mateus-3\n", $0,budget}' >> $DATAFILE

		export OPTIONS="--mcDiscretizeWithMateusApproach --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcDiscretizationRegions=6 $DEFAULT_OPTIONS"
		./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk -v budget="$MAXSAMPLES" '{printf "%s,%s,XYZ-discrete-mateus-6\n", $0,budget}' >> $DATAFILE

#		export OPTIONS="--mcDiscretize --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcDiscretizationRegions=3 $DEFAULT_OPTIONS"
		# ./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk -v budget="$MAXSAMPLES" '{printf "%s,%s,XYZ-discrete-anto-3\n", $0,budget}' >> $DATAFILE

		# export OPTIONS="--mcDiscretize --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults --mcDiscretizationRegions=6 $DEFAULT_OPTIONS"
		# ./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk -v budget="$MAXSAMPLES" '{printf "%s,%s,XYZ-discrete-anto-6\n", $0,budget}' >> $DATAFILE

	done	
done


echo ">> csv file done. <<"
#python report.py table4 ${DATAFILE} > ${REPORTFILE}
#echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
