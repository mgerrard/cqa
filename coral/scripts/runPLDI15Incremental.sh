#!/bin/bash

BENCHLIST=(
"../inputs/dwqcoral/complex/apollo.nodeq	253"
"../inputs/dwqcoral/framingham-1	27"
"../inputs/dwqcoral/framingham-2	44"
#"../inputs/dwqcoral/complex/conflict.nodeq	14"
#"../inputs/dwqcoral/complex/turn.nodeq	73"
"../inputs/dwqcoral/example-cart-12	45"
"../inputs/dwqcoral/example-cart-14	48"
#"../inputs/dwqcoral/example-carton-5-18	954"
#"../inputs/dwqcoral/example-carton-5-20	1030"
"../inputs/dwqcoral/example-ckd-epi-simple-0	18"
"../inputs/dwqcoral/example-invPend-0	1"
##############
# "../inputs/dwqcoral/example-carton-5-0	40"
# "../inputs/dwqcoral/example-carton-5-1	37"
# "../inputs/dwqcoral/example-carton-5-2	38"
# "../inputs/dwqcoral/example-carton-5-22	1132"
# "../inputs/dwqcoral/example-carton-5-5	38"
# "../inputs/dwqcoral/example-ckd-epi-0	46"
# "../inputs/dwqcoral/example-ckd-epi-1	38"
# "../inputs/dwqcoral/example-ckd-epi-simple-1	14"
# "../inputs/dwqcoral/framingham-0	23"
# "../inputs/dwqcoral/framingham-hypten-0	15"
# "../inputs/dwqcoral/framingham-hypten-3	8"
)

SEEDFILE="../inputs/seeds-1-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-incremental-to100k-v2.csv"

echo ">> Running qCORAL (incremental) for each benchmark (100k)<<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,samples,estimate,variance,time,mode" > $DATAFILE

for MAXSAMPLES in 1000000
do
	echo "=========== $MAXSAMPLES ==========="
	for BENCHLINE in "${BENCHLIST[@]}"
	do
		BENCHFILE=$(echo "$BENCHLINE" | cut -f 1)
		NPARTS=$(echo "$BENCHLINE" | cut -f 2 )

		echo $BENCHFILE
		NSAMPLES=$(python -c "a=$MAXSAMPLES;b=$NPARTS;c=a%b; print str(a/b) if c == 0 else (a/b + 1)")
		STEP=100 #$(python -c "print str(${NSAMPLES}/5)")
		#samples for the initial run are fixed to ~50,000
		INITIAL_SAMPLES=$(python -c "a=50000;b=$NPARTS;c=a%b; print str(a/b) if c == 0 else (a/b + 1)")
		echo "MAX SAMPLES per partition: $NSAMPLES"
		echo "INITIAL SAMPLES per partition: ${INITIAL_SAMPLES}"
		echo "SAMPLES per improvement step: $STEP"
		
		for SEED in "${SEEDS[@]}"
		do
			DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples $NSAMPLES --mcMinSamplesPerBox 0"
			if [[ $BENCHFILE == "*complex*" ]] 
			then
				DEFAULT_OPTIONS="$DEFAULT_OPTIONS --rangeLO -100 --rangeHI 100"
			fi

			FULLNAME=$(readlink -e "$BENCHFILE")
			echo -n "."

			# export OPTIONS="--mcIterativeImprovement --mcInitialPartitionBudget=${INITIAL_SAMPLES} --mcTargetVariance=1E-12 --mcSamplesPerIncrement=$STEP --mcProportionalBoxSampleAllocation $DEFAULT_OPTIONS"
			# ./runDWCounter.sh $FULLNAME $FULLNAME | grep "\[qCORAL-iterative\] samples" | tr '=' ' ' | cut -d ' ' -f 3,5,7,9 | awk -v name="$BENCHFILE" '{printf "%s,%s,%s,%s,%s,incremental-default\n",name,$1,$2,$3,$4}' >> $DATAFILE

			export OPTIONS="--mcIterativeImprovement --mcInitialPartitionBudget=${INITIAL_SAMPLES} --mcTargetVariance=1E-12 --mcSamplesPerIncrement=$STEP --mcProportionalBoxSampleAllocation --mcDerivativeVersion=2 $DEFAULT_OPTIONS"
			./runDWCounter.sh $FULLNAME $FULLNAME | grep "\[qCORAL-iterative\] samples" | tr '=' ' ' | cut -d ' ' -f 3,5,7,9 | awk -v name="$BENCHFILE" '{printf "%s,%s,%s,%s,%s,incremental-default-v2\n",name,$1,$2,$3,$4}' >> $DATAFILE


			#export OPTIONS="--mcIterativeImprovement --mcInitialPartitionBudget=${INITIAL_SAMPLES} --mcTargetVariance=1E-12 --mcSamplesPerIncrement=$STEP --mcProportionalBoxSampleAllocation --mcAllocateBudgetProportionally $DEFAULT_OPTIONS"
			#./runDWCounter.sh $FULLNAME $FULLNAME | grep "\[qCORAL-iterative\] samples" | tr '=' ' ' | cut -d ' ' -f 3,5,7,9 | awk -v name="$BENCHFILE" '{printf "%s,%s,%s,%s,%s,incremental-proportional\n",name,$1,$2,$3,$4}' >> $DATAFILE

			# export OPTIONS="--mcIterativeImprovement --mcInitialPartitionBudget=${INITIAL_SAMPLES} --mcTargetVariance=1E-12 --mcSamplesPerIncrement=10000 --mcAllocateBudgetProportionally $DEFAULT_OPTIONS"
			# ./runDWCounter.sh $FULLNAME $FULLNAME | grep "\[qCORAL-iterative\] samples" | tr '=' ' ' | cut -d ' ' -f 3,5,7 | awk -v name="$BENCHFILE" '{printf "%s,%s,%s,%s,incremental10k\n",name,$1,$2,$3}' >> $DATAFILE

			# export OPTIONS="--mcIterativeImprovement --mcInitialPartitionBudget=${INITIAL_SAMPLES} --mcTargetVariance=1E-12 --mcSamplesPerIncrement=15000 --mcAllocateBudgetProportionally $DEFAULT_OPTIONS"
			# ./runDWCounter.sh $FULLNAME $FULLNAME | grep "\[qCORAL-iterative\] samples" | tr '=' ' ' | cut -d ' ' -f 3,5,7 | awk -v name="$BENCHFILE" '{printf "%s,%s,%s,%s,incremental15k\n",name,$1,$2,$3}' >> $DATAFILE

		done	
	done
done

echo ">> csv file done. <<"

