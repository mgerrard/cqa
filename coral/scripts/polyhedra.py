#!/usr/bin/env python2
import sys

def main():
    infile = open(sys.argv[1])
    for line in infile:
        template = "DGE(ADD($1,ADD($2,ADD($3,DCONST($4)))),DCONST(0));"
        coeff = map(float,line.split(", "))
        i = 1
        for c in coeff:
            if i == 4:
                val = str(c)
            elif (c == 0):
                val = "DCONST(0)"
            elif (c == -1):
                val = "MUL(DCONST(-1),DVAR(ID_{}))".format(i)
            elif (c == 1):
                 val = "DVAR(ID_{})".format(i)
            else:
                val = "MUL(DCONST({}),DVAR(ID_{}))".format(c,i)
            
            template = template.replace("$" + str(i),val)
            i = i + 1
        print "+\"" + template + "\""

main()
