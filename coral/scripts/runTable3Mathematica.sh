#!/bin/bash

DATAFILE="../output/data-table3-math.csv"
REPORTFILE="../output/report-table3-math.txt"

echo ">> Table3: running Mathematica NIntegrate for each VolComp benchmark<<"

RUNVOL="false"
if [[ "$1" == "--with-vol" ]]
then
	RUNVOL="true"
	echo ">> WARNING: VOL benchmark is included on the work set. This will take a *long* time (no idea how long - we stopped after 15h)."
else
	echo ">> WARNING: VOL benchmark is *not* included by default. To include it, re-execute this script with '--with-vol'"
fi

echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec" > $DATAFILE

DEFAULT_OPTIONS="--mcNumExecutions 1"
echo -n "."
for BENCHFILE in ../inputs/pldi/*-* #iterate over files with an hyphen (the benchmark files)
do
	echo $BENCHFILE
	if [[ $RUNVOL == "false" && "$BENCHFILE" == *"example-vol-16" ]]
	then 
		continue # skip vol benchmark - takes too long
	fi

	export OPTIONS="--mcSkipPaving $DEFAULT_OPTIONS --mcUseMathematica --mathematicaGA"
	export OPTIONS="$OPTIONS --USE_DOMAINS_FILE"
	FULLNAME=$(readlink -e "$BENCHFILE")
	./runCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 >> $DATAFILE
done	


echo ">> csv file done. Preparing report... <<"
python report.py table3math ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
