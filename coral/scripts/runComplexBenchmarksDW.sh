#!/bin/bash

SEEDFILE="../inputs/seeds-30-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-complex-30k.csv"
REPORTFILE="../output/report-complex-30k.txt"

echo ">> Table DW: running qCoral for each complex benchmark<<"

echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,mode" > $DATAFILE

declare -A SAMPLE_MAP
SAMPLE_MAP["1000"]="500"
SAMPLE_MAP["10000"]="5000"
SAMPLE_MAP["30000"]="10000"
SAMPLE_MAP["100000"]="10000"

for MAXSAMPLES in "30000" 
do
	echo "=========== $MAXSAMPLES ==========="
	for BENCHFILE in ../inputs/dwqcoral/complex/*.nodeq
	do
		echo $BENCHFILE
		for SEED in "${SEEDS[@]}"
		do
			DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox 100 --rangeLO -100 --rangeHI 100"
			FULLNAME=$(readlink -e "$BENCHFILE")
			echo -n "."
			
#			################# "pure monte carlo (1 executions) - no rp"
#			export OPTIONS="--mcSkipPaving $DEFAULT_OPTIONS"
#			./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral\n", $0}' >> $DATAFILE
#			################ "monte carlo + realpaver (1 executions) - multiple boxes"
#			export OPTIONS="$DEFAULT_OPTIONS"
#			./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral{STRAT-nomerge}\n", $0}' >> $DATAFILE
#			################ "with partitioning and cache, merged boxes, cache for realpaver (1 execution, reseed every sim.)"
			export OPTIONS="--mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults $DEFAULT_OPTIONS"
			./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,XYZ\n", $0}' >> $DATAFILE

#			export OPTIONS="--mcIterativeImprovement --mcTargetVariance=1E-10 --mcSamplesPerIncrement=${SAMPLE_MAP[$MAXSAMPLES]} --mcDerivativeVersion=2 $DEFAULT_OPTIONS"
#			export OPTIONS="--mcIterativeImprovement --mcTargetVariance=1E-10 --mcSamplesPerIncrement=${SAMPLE_MAP[$MAXSAMPLES]} --mcDerivativeVersion=1 $DEFAULT_OPTIONS"
#			./runDWCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral{INCREMENTAL-v1}\n", $0}' >> $DATAFILE

		done	
	done
done

echo ">> csv file done. <<"
cat "${DATAFILE}"
python report.py table4 ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
