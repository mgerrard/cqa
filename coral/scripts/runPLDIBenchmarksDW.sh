#!/bin/bash

MAXSAMPLES=30000
MINSAMPLES=100

SEEDFILE="../inputs/seeds-30-cp" 
IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

DATAFILE="../output/data-tabledw-incremental-30k.csv"
REPORTFILE="../output/report-tabledw-incremental-30k.txt"

echo ">> Table DW: running qCoral for each benchmark<<"

RUNVOL="false"
if [[ "$1" == "--with-vol" ]]
then
	RUNVOL="true"
	echo ">> WARNING: VOL benchmark is included on the work set. This will take a *long* time (> 13h)."
else
	echo ">> WARNING: VOL benchmark is *not* included by default. To include it, re-execute this script with '--with-vol'"
fi

echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec,mode" > $DATAFILE

for SEED in "${SEEDS[@]}"
do
	DEFAULT_OPTIONS="--mcSeed $SEED  --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"
	echo -n "."
	for BENCHFILE in ../inputs/dwqcoral/* #iterate over files with an hyphen (the benchmark files)
	do
		if ! [[ -f $BENCHFILE ]]
		then
			continue #skip directories, etc...
		fi

		if [[ $RUNVOL == "false" && "$BENCHFILE" == *"example-vol-16" ]]
		then 
		 	continue # skip vol benchmark - takes too long
		fi
		echo $BENCHFILE

		FULLNAME=$(readlink -e "$BENCHFILE")

#		export OPTIONS="--mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults"
#		export OPTIONS="--mcIterativeImprovement --mcTargetVariance=1E-10 --mcDerivativeVersion=0"
#		export OPTIONS="$OPTIONS $DEFAULT_OPTIONS"
#		./runDWCounter.sh $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral(incremental-v0)\n", $0}' >> $DATAFILE

#		export OPTIONS="--mcIterativeImprovement --mcTargetVariance=1E-10 --mcDerivativeVersion=1"
#		export OPTIONS="$OPTIONS $DEFAULT_OPTIONS"
#		./runDWCounter.sh $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral(incremental-v1)\n", $0}' >> $DATAFILE

#		export OPTIONS="--mcDiscretize --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults"
#		export OPTIONS="$OPTIONS $DEFAULT_OPTIONS"
#		./runDWCounter.sh $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral(discretize)\n", $0}' >> $DATAFILE

		export OPTIONS="--mcDiscretizeWithMateusApproach --mcPartitionAndCache --mcCallIntSolPartition --cachePavingResults"
		export OPTIONS="$OPTIONS $DEFAULT_OPTIONS"
		./runDWCounter.sh $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 | awk '{printf "%s,qCoral(discretize-mab)\n", $0}' >> $DATAFILE

	done	
done

echo ">> csv file done. <<"
python report.py table4 ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
