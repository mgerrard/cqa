#!/usr/bin/python 
import sys
from optparse import OptionParser

class DifferentArgumentLengthsError:
    pass

def main():

    parser = OptionParser()
    parser.add_option("-n","--numberOfSets",action="store",type="int",dest="n_sets")
    parser.add_option("-t","--templateFile",action="store",dest="template_file")
    (options,args) = parser.parse_args()

    isfour = options.n_sets == 4
    replaceInFile = options.template_file != None

    if not isfour:
        a = args[0]
        b = args[1]
        c = args[2]

        if len(a) != len(b) or len(b) != len(c) or len(a) != len(c):
            raise DifferentArgumentLengthsError

        setA = dataToSet(a)
        setB = dataToSet(b)
        setC = dataToSet(c)

        if replaceInFile:
            replace3(options.template_file,setA,setB,setC)
        else:
            print3(len(a),setA,setB,setC)

    else:
        a = args[0]
        b = args[1]
        c = args[2]
        d = args[3]

        setA = dataToSet(a)
        setB = dataToSet(b)
        setC = dataToSet(c)
        setD = dataToSet(d)

        if replaceInFile:
            replace4(options.template_file,setA,setB,setC,setD)
        else:
            print4(len(a),setA,setB,setC,setD)

def dataToSet(resultLine):
    solved = []
    count = 1

    for run in resultLine:
        if run == '.':
            solved.append(count)
        count = count+1

    return set(solved)

def print3(length,setA,setB,setC):
    print "TOTAL: {0} constraints ({1} SAT)".format(length,len(setA | setB | setC))
    print "A: {0}/{1} SAT".format(len(setA),length)
    print "B: {0}/{1} SAT".format(len(setB),length)
    print "C: {0}/{1} SAT".format(len(setC),length)

    print "A inter B inter C  = {0}".format(len(setA & setB & setC))
    print "A-B-C              = {0}".format(len((setA-setB)-setC))
    print "B-A-C              = {0}".format(len((setB-setA)-setC))
    print "C-A-B              = {0}".format(len((setC-setA)-setB))
    print "(A inter C) - B    = {0}".format(len((setA & setC)-setB))
    print "(A inter B) - C    = {0}".format(len((setA & setB)-setC))
    print "(B inter C) - A    = {0}".format(len((setB & setC)-setA))

def print4(length,setA,setB,setC,setD):
    print "TOTAL: {0} constraints ({1} SAT)".format(length,len(setA | setB | setC | setD))
    print "A: {0}/{1} SAT".format(len(setA),length)
    print "B: {0}/{1} SAT".format(len(setB),length)
    print "C: {0}/{1} SAT".format(len(setC),length)
    print "D: {0}/{1} SAT".format(len(setD),length)

    print "A inter B inter C inter D  = {0}".format(len(setA & setB & setC & setD))
    print "A-B-C-D                    = {0}".format(len(((setA-setB)-setC)-setD))
    print "B-A-C-D                    = {0}".format(len(((setB-setA)-setC)-setD))
    print "C-A-B-D                    = {0}".format(len(((setC-setA)-setB)-setD))
    print "D-B-C-A                    = {0}".format(len(((setD-setB)-setC)-setA))
    print "(A inter C) - (B union D)  = {0}".format(len((setA & setC)-(setB|setD)))
    print "(A inter B) - (C union D)  = {0}".format(len((setA & setB)-(setC|setD)))
    print "(B inter C) - (A union D)  = {0}".format(len((setB & setC)-(setD|setA)))
    print "(D inter A) - (B union C)  = {0}".format(len((setA & setD)-(setB|setC)))
    print "(D inter B) - (A union C)  = {0}".format(len((setD & setB)-(setC|setA)))
    print "(D inter C) - (A union B)  = {0}".format(len((setD & setC)-(setB|setA)))
    print "(A inter B inter C) - D    = {0}".format(len((setA & setB & setC)-setD))
    print "(D inter B inter C) - A    = {0}".format(len((setD & setB & setC)-setA))
    print "(A inter D inter C) - B    = {0}".format(len((setA & setD & setC)-setB))
    print "(A inter B inter D) - C    = {0}".format(len((setA & setB & setD)-setC))

def replace3(filename,setA,setB,setC):
    
    template_file = open(filename,'r+')
    template = template_file.read()

    template = template.replace("$T0",str(len(setA | setB | setC)))
    template = template.replace("$T1",str(len(setA)))
    template = template.replace("$T2",str(len(setB)))
    template = template.replace("$T3",str(len(setC)))

    template = template.replace("$A+B+C",str(len(setA & setB & setC)))
   
    template = template.replace("$A+C",str(len((setA & setC)-(setB))))
    template = template.replace("$A+B",str(len((setA & setB)-(setC))))
    template = template.replace("$B+C",str(len((setB & setC)-(setA))))

    template = template.replace("$A",str(len(((setA-setB)-setC))))
    template = template.replace("$B",str(len(((setB-setA)-setC))))
    template = template.replace("$C",str(len(((setC-setA)-setB))))

    print template


def replace4(filename,setA,setB,setC,setD):
    
    template_file = open(filename,'r+')
    template = template_file.read()

    template = template.replace("$T0",str(len(setA | setB | setC | setD)))
    template = template.replace("$T1",str(len(setA)))
    template = template.replace("$T2",str(len(setB)))
    template = template.replace("$T3",str(len(setC)))
    template = template.replace("$T4",str(len(setD)))

    template = template.replace("$A+B+C+D",str(len(setA & setB & setC & setD)))

    template = template.replace("$A+B+C",str(len((setA & setB & setC)-setD)))
    template = template.replace("$B+C+D",str(len((setD & setB & setC)-setA)))
    template = template.replace("$A+C+D",str(len((setA & setD & setC)-setB)))
    template = template.replace("$A+B+D",str(len((setA & setB & setD)-setC)))

    template = template.replace("$A+C",str(len((setA & setC)-(setB|setD))))
    template = template.replace("$A+B",str(len((setA & setB)-(setC|setD))))
    template = template.replace("$B+C",str(len((setB & setC)-(setD|setA))))
    template = template.replace("$A+D",str(len((setA & setD)-(setB|setC))))
    template = template.replace("$B+D",str(len((setD & setB)-(setC|setA))))
    template = template.replace("$C+D",str(len((setD & setC)-(setB|setA))))

    template = template.replace("$A",str(len(((setA-setB)-setC)-setD)))
    template = template.replace("$B",str(len(((setB-setA)-setC)-setD)))
    template = template.replace("$C",str(len(((setC-setA)-setB)-setD)))
    template = template.replace("$D",str(len(((setD-setB)-setC)-setA)))

    print template

if __name__ == "__main__":
    main()
