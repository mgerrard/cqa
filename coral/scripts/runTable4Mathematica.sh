#!/bin/bash

DATAFILE="../output/data-table4-math-reset-c.csv"
REPORTFILE="../output/report-table4-math-reset-c.txt"

echo ">> Table4: running mathematica MC integration 30 times for each subject/config pair <<"
echo ">> Preparing csv file with raw data... (${DATAFILE}) <<"
echo "file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec" > $DATAFILE

for MAXSAMPLES in "1000" "10000" "100000"
do
	echo "=========== $MAXSAMPLES ==========="
	for BENCHFILE in "../inputs/jpf/conflict_cons_leaves_sample70" "../inputs/jpf/apollo_cons_leaves_sample70" "../inputs/jpf/turnlogic_cons_leaves_sample70" "../inputs/jpf/conflict_cons_leaves_sample70"
	do
		echo $BENCHFILE
#		if [[ $BENCHFILE != "../inputs/jpf/apollo_cons_leaves_sample70" ]]
#		then
			SEEDFILE="../inputs/seeds-100"  #smaller experiments
#		else
#			SEEDFILE="../inputs/seeds-30-cp" 
#		fi
		IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

		for SEED in "${SEEDS[@]}"
		do
			DEFAULT_OPTIONS="--mcSeed $SEED --mcNumExecutions 1 --mcMaxSamples $MAXSAMPLES --mcResetRngEachSimulation=true --mcMinSamplesPerBox 100 --rangeLO -100 --rangeHI 100"
			FULLNAME=$(readlink -e "$BENCHFILE")

			################# "Mathematica MC (1 execution) - no rp"
			export OPTIONS="--mcSkipPaving $DEFAULT_OPTIONS --mcUseMathematica"
			./runCounter.sh $FULLNAME $FULLNAME | grep "regCoral-csvresults" | cut -d ' ' -f 2- | tail --lines=+2 >> $DATAFILE
                	echo -n "."
		done
	done
done

echo ">> csv file done. Preparing report... <<"
python report.py table4math ${DATAFILE} > ${REPORTFILE}
echo ">> ok, done. The report can be found at ${REPORTFILE} <<"
