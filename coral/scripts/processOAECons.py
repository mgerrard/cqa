#!/usr/bin/env python2

import spfparsing,sys

def toCoralConstraint(cons):
#    print "parsing: " + cons.strip()
    tree = spfparsing.ExprParser.parse(cons)
    coralCons = spfparsing.visit_tree(tree)
#    print "result: " + coralCons
    return coralCons

def main():
    filename = sys.argv[1]
    with open(filename) as infile:
        ode_cons = infile.readlines()
        i = 0
        for con in ode_cons:
            print i
            print toCoralConstraint(con)
            i = i + 1
            

main()
