#!/usr/bin/env python2

import sys, subprocess, re

def prepareMathDomains(boxString,keys):
    extracted = re.findall("\[(.+?),(.+?)\]",boxString)
    mathd = ","
    i = 0
    for key in keys:
#        print extracted
        mathd = mathd + "{{{}, {}, {}}},".format(key,extracted[i][0],extracted[i][1])
        i = i + 1
        
    return mathd

def main():
    with open(sys.argv[1]) as infile:
        lines = infile.readlines()
        nDomains,nCons,nBoxes = [int(x) for x in lines[0].split(" ")]
        domains = {}
        boxes = []
        cons = []
        
        for i in range(1,1 + nDomains):
            line = lines[i]
            match = re.match("(.+) in \[(.+),(.+)].*",line)
            varname = match.group(1)
            bounds = (float(match.group(2)),float(match.group(3)))
            domains[varname] = bounds

        for i in range(nDomains + 1, 1 + nDomains + nCons):
            line = lines[i]
            cons.append(line)

        for i in range(1 + nDomains + nCons, 1 + nDomains + nCons + nBoxes):
            line = lines[i]
            boxes.append(line)

        for con in cons:
            for box in boxes:
#                print con + box
                mdomains = prepareMathDomains(box, sorted(domains.keys()))
                command = "Timing[NIntegrate[" + con + mdomains + 'Method->{"MonteCarlo"}]]'
                print command
                math_out = subprocess.check_output(["./runMath",command])
                print math_out

main()



