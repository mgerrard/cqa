#!/usr/bin/env python2

import csv,sys,math
from collections import defaultdict

#sample variance (s^2 N-1): http://mathworld.wolfram.com/Variance.html
def variance(samples,average):
    diff_of_squares = [(x - average) ** 2 for x in samples]
    return sum(diff_of_squares) / float(len(samples) - 1)

def main():
    with open(sys.argv[1]) as csvfile:
        rundata = defaultdict(list)
        reader = csv.DictReader(csvfile)

        for row in reader:
            name = row['name']
            time = row['time']
            estimate = row['estimateResult']
            
            data = (time,estimate)
            rundata[name].append(data)

        for name,datalist in rundata.items():
            estimates = [float(x[1]) for x in datalist]
            times = [float(x[0]) for x in datalist]

            totalTime = sum(times)
            n_samples = float(len(estimates))
            avg = sum(estimates) / n_samples
            
            #std. dev according to CLT - check http://mathworld.wolfram.com/CentralLimitTheorem.html for more info
            var = variance(estimates, avg)
            stdev = math.sqrt(var) / math.sqrt(n_samples)
#            print estimates
#            print "{}	{:.4f}+-{:.4f}	{:.2f}".format(name,avg,stdev,totalTime)
# print 95% confidence bounds using a t-student distribution with 9 degrees of freedom
            print "{}: [{:.4f},{:.4f}] avg={:.4f}, stdev={:.4f}, time={:.2f}".format(name,avg-(2.262*stdev),avg+(2.262*stdev),avg,stdev,totalTime)

            
main()        
        
    

