#!/bin/bash

BENCHLIST=../inputs/benchmarks
#BENCHLIST=../inputs/pldibenchs
#BENCHLIST=../inputs/volbench
MAXSAMPLES=1000
MINSAMPLES=100

#cat "$BENCHLIST" | parallel -j 3 -k --arg-sep ' ' --verbose python compareWithPLDI.py {}
#cat "$BENCHLIST" | parallel -j 3 -k --arg-sep ' ' --verbose python compareWithPLDI.py {}

echo "sample budget: $MAXSAMPLES"
SEEDFILE="../inputs/seeds-1" #or seeds-30 or seeds-10

IFS=$'\r\n' SEEDS=($(cat "$SEEDFILE"))

for SEED in "${SEEDS[@]}"
do

	if [[ "$SEED" == \#* ]] 
	then 
		continue
	fi

	echo "SEED: $SEED, MINSAMPLES:$MINSAMPLES"

	DEFAULT_OPTIONS="--USE_DOMAINS_FILE --mcSeed $SEED --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"

	while read -r BENCHFILE 
	do
		echo $BENCHFILE
		 if [[ "$BENCHFILE" == *"example-vol-16" ]]
		 then 
		 	continue # skip vol benchmark - takes too long
		 fi

		 echo "================"
		 echo "Mathematica (ten executions, 1k) - merged boxes"
		 export OPTIONS="--mcMergeBoxes $DEFAULT_OPTIONS --mcNumExecutions 10 --mcUseMathematica"
		 FULLNAME=$(readlink -e "$BENCHFILE")
		./runCounter.sh $FULLNAME $FULLNAME

		 echo "================"
		 echo "pure monte carlo (ten executions,1k) - merged boxes"
		 export OPTIONS="--mcSkipPaving --mcNumExecutions 10 $DEFAULT_OPTIONS"
		 FULLNAME=$(readlink -e "$BENCHFILE")
		./runCounter.sh $FULLNAME $FULLNAME

		 echo "================"
		 echo "monte carlo + realpaver (ten executions,1k) - merged boxes"
		 export OPTIONS="--mcSkipPaving --mcNumExecutions 10 $DEFAULT_OPTIONS"
		 FULLNAME=$(readlink -e "$BENCHFILE")
		./runCounter.sh $FULLNAME $FULLNAME

		#  echo "================"
		#  echo "with partitioning and cache, merged boxes, cache for realpaver"
		#  export OPTIONS="--mcMergeBoxes --mcPartitionAndCache --cachePavingResults $DEFAULT_OPTIONS --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"
		#  FULLNAME=$(readlink -e "$BENCHFILE")
		# ./runCounter.sh $FULLNAME $FULLNAME

		#  echo "================"
		#  echo "with partitioning and cache, merged boxes"
		#  export OPTIONS="--mcMergeBoxes --mcPartitionAndCache $DEFAULT_OPTIONS --mcMaxSamples $MAXSAMPLES --mcMinSamplesPerBox $MINSAMPLES"
		#  FULLNAME=$(readlink -e "$BENCHFILE")
		# ./runCounter.sh $FULLNAME $FULLNAME
	done < $BENCHLIST

done
