#!/bin/bash
cd ..
java -cp bin:libs/commons-math-1.2.jar:libs/opt4j-2.4.jar:libs/commons-math3-3.3.jar:/usr/local/Wolfram/Mathematica/9.0/SystemFiles/Links/JLink/JLink.jar:libs/guava-15.0.jar:libs/jung/jung-api-2.0.1.jar:libs/jung/collections-generic-4.01.jar:libs/jung/jung-algorithms-2.0.1.jar:libs/jung/jung-graph-impl-2.0.1.jar coral.counters.RunBenchmark $OPTIONS
cd scripts/
