#!/bin/bash

source variables
cd ..
OPTIONS="$OPTIONS --realPaverLocation=${REALPAVER}"
#-agentlib:hprof=cpu=samples,force=n 
java -cp bin:libs/commons-math-1.2.jar:libs/opt4j-2.4.jar:libs/commons-math3-3.3.jar:$JLINK:libs/guava-15.0.jar:libs/colt.jar:libs/ssj.jar:libs/jung/jung-api-2.0.1.jar:libs/jung/collections-generic-4.01.jar:libs/jung/jung-algorithms-2.0.1.jar:libs/jung/jung-graph-impl-2.0.1.jar coral.counters.RegCoralMain $OPTIONS $1 $2 
cd scripts


