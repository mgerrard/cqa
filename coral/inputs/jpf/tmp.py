#!/usr/bin/env python2
import sys

with open(sys.argv[1]) as infile:
    lines = infile.readlines()
    longest = []

    i = 0
    j = len(lines)
    for l in lines:
        line = l.strip()
#        print line
        i = i + 1
        print "[time] " + str(i/float(j))
        isLongest = True
        for l2 in lines:
            line2 = l2.strip()
            if line in line2 and line != line2:
                isLongest = False
                break
        if isLongest:
            print line
