#!/usr/bin/env python2

import sys,re

def extract_domain(domain):
    matches = re.findall(r"{(.+?),(.+?)}",domain)
    return matches

def extract_cons(cons):
    matches = re.findall(r"r_(\d+)",cons)
    ordered = sorted(set([int(x) for x in matches]))
    return ordered

def main():
    with open(sys.argv[1]) as infile:
        intervals = set()
        for line in infile:
            if "#" in line:
                domain,cons = line.split("#")
                tuples = zip(extract_cons(cons),extract_domain(domain))
                intervals = intervals | set(tuples)

        for x in sorted(intervals):
            var_id = x[0]
            lo = float(x[1][0])
            hi = float(x[1][1])
            avg = (hi + lo) / 2
            print "{} NORMAL {} {} {} {}".format(var_id,lo,hi,avg,(hi - avg) / 3)
        

main()
