package coral.tests;

import static org.junit.Assert.*;

import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import symlib.SymLiteral;
import symlib.Util;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.simplifier.DP_Eq;
import coral.simplifier.Rewrite;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.SolverKind;
import coral.util.Config;

public class RewriteTests {

  @Before
  public void setup() {
    Util.resetID();
  }

  @Test
  public void testRewriteDavid() throws ParseException{
    String test1 = "DEQ(ADD(DCONS(3.0),DVAR(ID_2)),ADD(DVAR(ID_2),DVAR(ID_1)))";
    String test2 = "DEQ(ADD(DCONS(3.0),DVAR(ID_2)),SUB(DVAR(ID_2),DVAR(ID_1)))";
    String test3 = "DEQ(ADD(DCONS(3.0),DVAR(ID_2)),MUL(DVAR(ID_2),DVAR(ID_1)))";
    String test4 = "DEQ(ADD(DCONS(3.0),DVAR(ID_2)),DIV(DVAR(ID_2),DVAR(ID_1)))";
    rewriteDavidHelper(test1);
    rewriteDavidHelper(test2);
    rewriteDavidHelper(test3);
    rewriteDavidHelper(test4);
  }
  
  private void rewriteDavidHelper(String test) throws ParseException {
    PC pc = (new Parser(test)).parsePC();
    SymLiteral[] vars = new SymLiteral[2];
    vars = pc.getVars().toArray(vars);
    
    PC rewPc = Rewrite.rew(pc);
    System.out.println(rewPc);
    Set<SymLiteral> rewVars = rewPc.getVars();
    for (SymLiteral var : vars) {
      assertTrue(rewVars.contains(var));
    }
  }
  
  @Test @Ignore("Most likely this test reproduced a bug that doesn't exist anymore")
  public void testCons93WithOptimizedSolution() { 
    benchmark93(3.479157588755342,3.039999999999917,6.021385919380437); //answer with optimizations (wrong)
  }

  @Test
  public void testCons93WithNonOptimizedSolution() { 
    benchmark93(-3.630000000000008,-4.630000000000008,0); //answer without optimizations
  }
  
  @Test  
  public void testOptmizedCons93WithOptimizedSolution() { 
    benchmark93opt(3.039999999999917,6.021385919380437); //answer with optimizations (wrong)
  }

  @Test
  public void testOptmizedCons93WithNonOptimizedSolution() { 
    benchmark93opt(-4.630000000000008,0); //answer without optimizations
  }
  
  @Test
  public void testOpt(){
    benchmark93(7.412947983271983,8.372380137073497,2.4538587436798656);
    benchmark93opt(8.372380137073497,2.4538587436798656);
  }
  
  @Test @Ignore("See the ignored test above")
  public void testOpt2(){
    benchmark93(3.479157588755342,3.039999999999917,6.021385919380437);
    benchmark93opt(3.039999999999917,6.021385919380437);
  }
  //
  
  //hard-coded Benchmark.pc93 constraint
  private static void benchmark93(double v10, double v11, double v12) {
    //AND((($V10-$V11) == ((sin_($V12)+cos_($V12))+tan_($V12))),((pow_($V10,tan_($V12))+$V11) < ($V10*atan_($V11))))
    
    //should be 0
    double eq1 = (v10 - v11) - (Math.sin(v12) + Math.cos(v12) + Math.tan(v12));
    //eq2_a should be smaller than eq2_b
    double eq2_a = (Math.pow(v10, Math.tan(v12)) + v11);
    double eq2_b = (v10 * Math.atan(v11));
    
    Assert.assertEquals("eq1=" + eq1,0.0, eq1);
    Assert.assertEquals("eq2 < 0=" + (eq2_a - eq2_b),true, (eq2_a - eq2_b) < 0);
  }
  
  //hard-coded version of optimized Benchmark.pc93 constraint
  private static void benchmark93opt( double v11, double v12) {
    //AND(((((sin_($V12)+cos_($V12))+tan_($V12))+$V11) == (((sin_($V12)+cos_($V12))+tan_($V12))+$V11)),((pow_((((sin_($V12)+cos_($V12))+tan_($V12))+$V11),tan_($V12))+$V11) < ((((sin_($V12)+cos_($V12))+tan_($V12))+$V11)*atan_($V11))))
    
    double v10 = (Math.sin(v12) + Math.cos(v12) + Math.tan(v12)) + v11; 

    //first clause of constraint is always true
    
    //eq2_a should be smaller than eq2_b
    double eq2_a = (Math.pow(v10, Math.tan(v12)) + v11);
    double eq2_b = (v10 * Math.atan(v11));
    
    Assert.assertEquals("eq2 < 0=" + (eq2_a - eq2_b),true, (eq2_a - eq2_b) < 0);
  }
  
  @Test @Ignore("Under review")
  public void testCanonicalizationSurprisingBehavior() throws Exception {
    Config.pcCanonicalization = false;
    
    PC pc = new Parser(DavidSamples.sample185).parsePC()/*.getCanonicalForm()*/;
    PC canonPC = pc.getCanonicalForm();
    Env env = Helper.solveAndPrint(SolverKind.PSO_OPT4J.get(), pc);
    Env canonEnv =  Helper.solveAndPrint(SolverKind.PSO_OPT4J.get(), canonPC);
    
    assertTrue(Result.SAT == canonEnv.getResult());
    assertTrue(Result.UNK == env.getResult()); //only the canon. version of the pc is solved...
    
    //...but both pcs are SAT.    
    assertTrue(Helper.eval(canonPC, new double[]{99.98421485142515,94.54152509687614,-100.0,-100.0,100.0,-100.0,-22.49881837462638,-38.25232583581827}));
    //switching variables: V5->V7,V6->V5,V7->V6
    assertTrue(Helper.eval(pc, new double[]{99.98421485142515,94.54152509687614,-100.0,-100.0,-100.0,-22.49881837462638,100.0,-38.25232583581827}));
  }
  
  @Test @Ignore("Rewriting with casts is under review since it relied on visitors ignoring SymCasts. See TODO's")
  public void testIntervalSolverRewrite() throws Exception {
    PC pc = new Parser("DEQ(DCONST(0.0),DIV(DVAR(ID_1),DCONST(5.0)));IEQ(MUL(ICONST(2),IVAR(ID_3)),IVAR(ID_2));IEQ(ASINT(DVAR(ID_1)),IVAR(ID_2));IEQ(ICONST(0),MUL(ICONST(2),IVAR(ID_3)));DGT(SUB(DCONST(0.0),DIV(DVAR(ID_4),DCONST(1.0))),DCONST(1.0E-12));DLT(DIV(DVAR(ID_4),DCONST(1.0)),DCONST(0.0));IEQ(ASINT(DVAR(ID_4)),IVAR(ID_3));ILT(IVAR(ID_3),ICONST(1));DEQ(DCONST(1.0E-12),SUB(DCONST(0.0),DIV(DVAR(ID_5),DCONST(1.0))));DLT(DIV(DVAR(ID_5),DCONST(1.0)),DCONST(0.0));IEQ(ASINT(DVAR(ID_5)),IVAR(ID_3));IEQ(ICONST(0),IVAR(ID_3));DEQ(DVAR(ID_6),ADD(ADD(DVAR(ID_7),DCONST(0.02)),DCONST(0.02)));IEQ(ASINT(DVAR(ID_6)),IVAR(ID_3));DEQ(DCONST(0.0),SUB(DVAR(ID_8),ADD(ADD(DVAR(ID_7),DCONST(0.02)),DCONST(0.02))));DGT(ADD(ADD(DVAR(ID_7),DCONST(0.02)),DCONST(0.02)),DCONST(0.0));IEQ(ASINT(DVAR(ID_8)),IVAR(ID_9));IEQ(ICONST(0),IVAR(ID_9));DEQ(DVAR(ID_10),ADD(ADD(DVAR(ID_7),DCONST(0.02)),DCONST(0.02)));IEQ(ASINT(DVAR(ID_10)),IVAR(ID_9));DLT(DIV(DVAR(ID_11),DCONST(1.0)),DCONST(1.0E-12));DEQ(DCONST(0.0),DIV(DVAR(ID_11),DCONST(1.0)));IEQ(ASINT(DVAR(ID_11)),IVAR(ID_12));IEQ(ICONST(0),IVAR(ID_12));DEQ(DVAR(ID_13),ADD(DVAR(ID_7),DCONST(0.02)));IEQ(ASINT(DVAR(ID_13)),IVAR(ID_12));DEQ(DCONST(0.0),SUB(DVAR(ID_14),ADD(DVAR(ID_7),DCONST(0.02))));DGT(ADD(DVAR(ID_7),DCONST(0.02)),DCONST(0.0));IEQ(ASINT(DVAR(ID_14)),IVAR(ID_15));IEQ(ICONST(0),IVAR(ID_15));DEQ(DVAR(ID_16),ADD(DVAR(ID_7),DCONST(0.02)));IEQ(ASINT(DVAR(ID_16)),IVAR(ID_15));DLT(DIV(DVAR(ID_17),DCONST(1.0)),DCONST(1.0E-12));DEQ(DCONST(0.0),DIV(DVAR(ID_17),DCONST(1.0)));IEQ(ASINT(DVAR(ID_17)),IVAR(ID_18));IEQ(ICONST(0),IVAR(ID_18));IEQ(ASINT(DVAR(ID_7)),IVAR(ID_18));DEQ(DCONST(0.0),SUB(DVAR(ID_19),DVAR(ID_7)));DEQ(DCONST(0.0),DVAR(ID_7));IEQ(ASINT(DVAR(ID_19)),IVAR(ID_20));IEQ(ICONST(0),IVAR(ID_20));IEQ(ASINT(DVAR(ID_7)),IVAR(ID_20))").parsePC();

    Config.simplifyUsingIntervalSolver = false;
    Env env = Helper.solveAndPrint(SolverKind.PSO_OPT4J.get(), pc);
    assertTrue(env.getResult() == Result.UNK);
    
    Config.simplifyUsingIntervalSolver = true;
    env = Helper.solveAndPrint(SolverKind.PSO_OPT4J.get(), pc);
    assertTrue(env.getResult() == Result.SAT);
  }
  
  @Test 
  public void testRewriteCompositeBooleanOperations() throws Exception {
    String str = "BNOT(BAND(IEQ(IVAR(ID_1),ICONST(0)),INE(IVAR(ID_2),ICONST(0))))";
    Parser parser = new Parser(str);
    PC pc = parser.parsePC();
    Object[] pair = DP_Eq.removeSimpleEqualities(pc);
    PC newPC = (PC) pair[0];
    Env sol = (Env) pair[1];
    
    assertEquals(newPC.getVars().size(),2);
    assertEquals(sol.getMapLiteralToNumber().size(),0);
  }
}
