package coral.tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import coral.util.trees.Trie;

public class TrieTests {

  @Test
  public void testTrieInsertions() {
    Trie<String,String> t = new Trie<String,String>();
    List<String> l1 = new ArrayList<String>();
    List<String> l2 = new ArrayList<String>();
    List<String> l3 = new ArrayList<String>();
    List<String> l4 = new ArrayList<String>();

    l1.add("aaaa");
    l1.add("bbb");
    l1.add("cc");
    l1.add("d");
    t.insert(l1, "im here");
    
    l2.add("aaaa");
    l2.add("bbb");
    l2.add("d");
    l2.add("cc");
    t.insert(l2, "did you find it?");
    
    l3.add("aaaa");
    l3.add("bbb");
    l3.add("cc");
    l3.add("d");
    l3.add("no one will find me");

    l4.add("aaaaaaaa");
    
    assertTrue(t.searchPrefix(l4) == null);
    
    l4.clear();
    l4.add("aaaa");
    l4.add("bbb");
    l4.add("d");

    assertTrue(t.searchPrefix(l4) == null);
    assertTrue(t.searchPrefix(l2).equals("did you find it?"));
    assertTrue(t.searchPrefix(l3).equals("im here"));
  }
  
  @Test
  public void testTrieListInsertions() {
    Trie<String,String> t = new Trie<String,String>();
    List<String> l1 = new ArrayList<String>();
    List<String> l2 = new ArrayList<String>();
    List<String> l3 = new ArrayList<String>();
    List<String> l4 = new ArrayList<String>();
    List<String> p1 = new ArrayList<String>();

    l1.add("aaaa");
    l1.add("bbb");
    l1.add("cc");
    l1.add("d");
    
    p1.add("a");
    p1.add("b");
    p1.add("c");
    p1.add("d");
    
    t.insertList(l1, p1);
    assertTrue(t.searchPrefix(l1).equals("d")); l1.remove(3);
    assertTrue(t.searchPrefix(l1).equals("c")); l1.remove(2);
    assertTrue(t.searchPrefix(l1).equals("b")); l1.remove(1);
    assertTrue(t.searchPrefix(l1).equals("a"));
    p1.clear();
    
    l2.add("aaaa");
    l2.add("bbb");
    l2.add("d");
    l2.add("cc");
    
    p1.add("e");
    p1.add("f");
    p1.add("g");
    p1.add("h");
    
    t.insertList(l2, p1);
    assertTrue(t.searchPrefix(l2).equals("h")); l2.remove(3);
    assertTrue(t.searchPrefix(l2).equals("g")); l2.remove(2);
    assertTrue(t.searchPrefix(l2).equals("f")); l2.remove(1);
    assertTrue(t.searchPrefix(l2).equals("e"));    
  }
  
}
