package coral.tests.counters;

import org.junit.Ignore;
import org.junit.Test;

import coral.counters.DistributionAwareQCoral;

public class DWCounterTest {

  @Test
  @Ignore
  public void runBuggyConstraint() throws Exception {
    String[] args = new String[] { "--mcUseMathematica", "--mcUseNProbability",
        "inputs/tests/dw-bug-01" };
    DistributionAwareQCoral.main(args);

    System.out.println("\n--------------------------------------------");

    args = new String[] { "--mcPartitionAndCache", "--mcCallIntSolPartition",
        "--cachePavingResults", "--mcMaxSamples", "100000",
        "inputs/tests/dw-bug-01" };
    DistributionAwareQCoral.main(args);

  }
}
