package coral.tests.counters;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import coral.counters.DistributionAwareQCoral;
import coral.counters.discretization.Discretization;

public class DWCounterTests {

  @Test
  //TODO automate the checking of the result/standard deviation
  public void runBug2() throws Exception {
    for (int i = 0; i < 30; i++) {
      String[] args = new String[] { "--mcSeed", "" + (123456789 + i * 37),
          "--mcMaxSamples", "30000", "--mcDiscretize", "--mcPartitionAndCache",
          "inputs/tests/dw-bug-02" };
      
      DistributionAwareQCoral.main(args);
    }
  }
  
  @Test @Ignore("Old discretization optimization code; not using it anymore")
  public void testDiscretization() {
    RealDistribution rdist = new NormalDistribution(0,0.03333333);
    double ub = 0.1;
    double lb = -0.1;
    
    Discretization opt = new Discretization();
    double[] cuts = opt.findBestAllocation(rdist, lb, ub, 6);
    
    for (double cut : cuts) {
      Assert.assertTrue(cut + " <= " + lb,cut > lb);
      Assert.assertTrue(cut + " >= " + ub, cut < ub);
    }
  }
  
  @Test
  public void testExponentialDist() {
    RealDistribution expDist = new ExponentialDistribution(0.5);
    double cdfAtZero = expDist.cumulativeProbability(0);
    double cdfAtOne = expDist.cumulativeProbability(1);
    System.out.println("CDF[ExponentialDistribution[0.5],0] = 0. commons3 = " + cdfAtZero);
    System.out.println("CDF[ExponentialDistribution[0.5],1] = 0.393469 commons3 = " + cdfAtOne);
  }
}
