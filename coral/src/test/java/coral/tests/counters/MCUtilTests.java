package coral.tests.counters;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.junit.Assert;
import org.junit.Test;

import coral.counters.MCUtil;
import coral.counters.MCUtil.DiscreteRegion;
import coral.counters.rvars.RandomVariable;
import coral.counters.rvars.RealRandomVariable;
import coral.util.Interval;

public class MCUtilTests {

  @Test
  public void testBacktrack() {
    Interval[] iv = new Interval[] { new Interval(0, 1), new Interval(2, 3), new Interval(4, 5) };
    RandomVariable rvar = new RealRandomVariable(new NormalDistribution(0,10), new Interval(-100,100), new UniformRealDistribution());
    RandomVariable [] rvars = new RandomVariable[] {rvar,rvar,rvar};
    Iterator<DiscreteRegion[]> it = MCUtil.getAllRegions(iv,rvars, 2);
    Set<DiscreteRegion[]> set = new HashSet<DiscreteRegion[]>();
    
    while(it.hasNext()) {
      DiscreteRegion[] region = it.next();
//      System.out.println(Arrays.toString(region.b));
      set.add(region);
    }
    Assert.assertEquals(8, set.size());
//    Assert.assertEquals(2, set.size());
    
  }
}
