package coral.tests.counters;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.Range;

import coral.util.BigRational;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.RealPaverCaller;
import symlib.parser.ParseException;
import symlib.parser.Parser;

public class IntegerCountingTests {

  final String s1 = "IGE(ADD(DIV(IVAR(ID_1),ICONST(2)),DIV(IVAR(ID_1),ICONST(2))),ICONST(0))";

  @Test
  public void testDoubleDivision()
      throws IOException, InterruptedException, ParseException {
    Config.intervalSolverSlackDivision = true;
    List<Interval[]> paving = new RealPaverCaller().getPaving(
        new Parser(s1).parsePC(), new Interval[] { new Interval(-100, 100) });

    Range<BigRational> x1Range = paving.get(0)[0].range;
    System.out.println(x1Range);
    assertTrue(x1Range.contains(BigRational.ZERO));
  }

  @Test
  public void simpleDivisionCheck() {
    int x = 1;
    int y = x / 2;
    int z = x / 2;
    assertEquals(y + z, 0);
  }
}