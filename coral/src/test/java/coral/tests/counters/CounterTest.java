package coral.tests.counters;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.counters.Main;
import coral.util.Config;
import coral.util.Interval;

public class CounterTest {

  @Test
  public void testRegCoral() throws ParseException {
    //monte carlo parameters
    Main.mcNumExecutions = 10;
    Main.normalize = true;
    Main.mcPartitionAndCache = true;
    Config.mcMaxSamples = 10000;
    Config.mcMergeBoxes = true;
    
    //realpaver parameters
    //Config.realPaverLocation="path/to/realpaver/binary"
    Config.rangeHI = 10;
    Config.rangeLO = -10;

    //if you need to disable realpaver, uncomment this line and comment the next
    Config.mcSkipPaving = true;
//    Main.mcCallIntSolPartition = true;
    
//    String cons = "DLT(MUL(DCONST(0.017453292519943295),DVAR(ID_1)),DCONST(0.0));"
//        + "DNE(TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2))),DCONST(0.0));"
//        + "DNE(TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2))),DCONST(0.0));"
//        + "DNE(DVAR(ID_3),DCONST(0.0));"
//        + "DNE(DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0)),DCONST(0.0));"
//        + "DLT(SQRT_(ADD(POW_(SUB(ADD(DVAR(ID_5),MUL(MUL(DCONST(1.0),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SUB(COS_(DVAR(ID_6)),COS_(ADD(DVAR(ID_6),DIV(MUL(MUL(DCONST(1.0),DIV(MUL(SUB(DCONST(0.0),MUL(DCONST(0.017453292519943295),DVAR(ID_1))),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),DVAR(ID_3))),DVAR(ID_4)),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0)))))))),MUL(MUL(DCONST(-1.0),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SUB(DCONST(1.0),COS_(MUL(DCONST(0.017453292519943295),DVAR(ID_1)))))),DCONST(2.0)),POW_(SUB(SUB(DVAR(ID_7),MUL(MUL(DCONST(1.0),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SUB(SIN_(DVAR(ID_6)),SIN_(ADD(DVAR(ID_6),DIV(MUL(MUL(DCONST(1.0),DIV(MUL(SUB(DCONST(0.0),MUL(DCONST(0.017453292519943295),DVAR(ID_1))),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),DVAR(ID_3))),DVAR(ID_4)),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0)))))))),MUL(MUL(DCONST(-1.0),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SIN_(MUL(DCONST(0.017453292519943295),DVAR(ID_1))))),DCONST(2.0)))),DCONST(999.0));"
//        + "DGT(SQRT_(ADD(POW_(SUB(ADD(DVAR(ID_5),MUL(MUL(DCONST(1.0),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SUB(COS_(DVAR(ID_6)),COS_(ADD(DVAR(ID_6),DIV(MUL(MUL(DCONST(1.0),DIV(MUL(SUB(DCONST(0.0),MUL(DCONST(0.017453292519943295),DVAR(ID_1))),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),DVAR(ID_3))),DVAR(ID_4)),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0)))))))),MUL(MUL(DCONST(-1.0),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SUB(DCONST(1.0),COS_(MUL(DCONST(0.017453292519943295),DVAR(ID_1)))))),DCONST(2.0)),POW_(SUB(SUB(DVAR(ID_7),MUL(MUL(DCONST(1.0),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SUB(SIN_(DVAR(ID_6)),SIN_(ADD(DVAR(ID_6),DIV(MUL(MUL(DCONST(1.0),DIV(MUL(SUB(DCONST(0.0),MUL(DCONST(0.017453292519943295),DVAR(ID_1))),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),DVAR(ID_3))),DVAR(ID_4)),DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0)))))))),MUL(MUL(DCONST(-1.0),DIV(DIV(POW_(DVAR(ID_3),DCONST(2.0)),TAN_(MUL(DCONST(0.017453292519943295),DVAR(ID_2)))),DCONST(68443.0))),SIN_(MUL(DCONST(0.017453292519943295),DVAR(ID_1))))),DCONST(2.0)))),DCONST(2.0))";
    String cons = "DLT(DVAR(ID_1),DCONST(5));DGT(DVAR(ID_2),DCONST(7))";
    PC pc = (new Parser(cons)).parsePC();
    Interval defaultInterval = new Interval(0,10);
    Interval[] domain = new Interval[]{defaultInterval,defaultInterval};
    
    List<PC> constraints = new ArrayList<PC>();
    List<Interval[]> domains = new ArrayList<Interval[]>();
    //the domain should have only the intervals for the variables on the respective constraint
    constraints.add(pc);
    domains.add(domain);
    
    cons = "DLT(DVAR(ID_1),DCONST(5));DLE(DVAR(ID_2),DCONST(7))";
    pc = (new Parser(cons)).parsePC();
    constraints.add(pc);
    domains.add(domain);
    
    double[] result = Main.runRegCoral(constraints, domains);
    System.out.printf("result: avg=%f, err=%f\n",result[0],result[1]);
    
//    Assert.assertEquals(0.499950, result[0], 0.001);
//    Assert.assertEquals(0.000158, result[1], 0.001);
    Assert.assertEquals(0.500, result[0], 0.01);
  }
}
