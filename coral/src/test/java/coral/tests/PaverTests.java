package coral.tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import coral.counters.refactoring.problem.PartitionProblem;
import coral.util.BigRational;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

import symlib.SymBool;
import symlib.eval.Evaluator;
import symlib.eval.compilation.JavaPcCompiler;
import symlib.eval.compilation.PcCompiler;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.RealPaverCaller;

import static coral.util.callers.RealPaverCaller.VarData;

import coral.util.visitors.PaverInput;
import coral.util.visitors.PaverVisitor;

public class PaverTests {

  @Test
  public void testPowParsing() throws ParseException {
    PC input1 = new Parser("DEQ(MUL(POW_(DCONST(10.0),DCONST(-0.5)),SUB(DVAR(ID_2),DVAR(ID_4))),DCONST(0.0))").parsePC();
    PC input2 = new Parser("DLT(SUB(MUL(MUL(POW_(DCONST(10),DCONST(4)),DVAR(ID_1)),DVAR(ID_2)),DCONST(-1.0)),DCONST(0.0))").parsePC();
    PC input3 = new Parser("DGT(SUB(MUL(MUL(POW_(DCONST(10),ADD(DCONST(4),DCONST(5))),DVAR(ID_1)),DVAR(ID_2)),DCONST(-1.0)),DCONST(0.0))").parsePC();
    PaverVisitor pv = new PaverVisitor();

    PaverInput paverInput1 = input1.getConstraints().get(0).accept(pv);
    PaverInput paverInput2 = input2.getConstraints().get(0).accept(pv);
    PaverInput paverInput3 = input3.getConstraints().get(0).accept(pv);
    System.out.println(paverInput1);
    System.out.println(paverInput2);
    System.out.println(paverInput3);

    assertTrue(paverInput1.cons.equals("((1 / sqrt(10.0)) * (x2 - x4)) = 0.0"));
    assertTrue(paverInput2.cons.equals("((((10.0 ^ 4) * x1) * x2) - -1.0) <= 0.0"));
    assertTrue(paverInput3.cons.equals("((((10.0 ^ 9) * x1) * x2) - -1.0) >= 0.0"));

    try {
      PC input4 = new Parser("DEQ(ADD(POW_(DCONST(2.71828183),MUL(DVAR(ID_1),DCONST(-1))),SUB(POW_(DCONST(2.71828183),MUL(DVAR(ID_2),DCONST(-1))),DCONST(-1.0001))),DCONST(0.0))").parsePC();
      PaverInput paverInput4 = input4.getConstraints().get(0).accept(pv);
      fail("Should have thrown an exception here.");
    } catch (UnsupportedOperationException e) {
      assertTrue(e.getMessage().equals("RealPaver does not support exponentiation with variables or functions present in the exponent"));
    }

    try {
      PC input5 = new Parser("DEQ(ADD(POW_(DCONST(2.71828183),DCONST(-1.55323)),SUB(POW_(DCONST(2.71828183),MUL(DVAR(ID_2),DCONST(-1))),DCONST(-1.0001))),DCONST(0.0))").parsePC();
      PaverInput paverInput5 = input5.getConstraints().get(0).accept(pv);
      fail("Should have thrown an exception here.");
    } catch (UnsupportedOperationException e) {
      assertTrue(e.getMessage().equals("RealPaver does not support real numbers as an exponent: 1.55323"));
    }
  }

  @Test
  public void testLogParsing() throws ParseException {
    PC input1 = new Parser("DEQ(MUL(LOG10_(DVAR(ID_3)),DIV(DCONST(1),SQRT_(SIN_(DVAR(ID_1))))),SQRT_(COS_(EXP_(DVAR(ID_2)))))").parsePC();
    PC input2 = new Parser("DEQ(MUL(LOG_(DVAR(ID_3)),DIV(DCONST(1),SQRT_(SIN_(DVAR(ID_1))))),SQRT_(COS_(EXP_(DVAR(ID_2)))))").parsePC();
    PaverVisitor pv = new PaverVisitor();

    PaverInput paverInput1 = input1.getConstraints().get(0).accept(pv);
    PaverInput paverInput2 = input2.getConstraints().get(0).accept(pv);

    assertTrue(paverInput1.cons.equals("((log(x3) / log(10)) * (1.0 / sqrt(sin(x1)))) = sqrt(cos(exp(x2)))"));
    assertTrue(paverInput2.cons.equals("(log(x3) * (1.0 / sqrt(sin(x1)))) = sqrt(cos(exp(x2)))"));
  }

  @Test
  public void testCall() throws ParseException, IOException, InterruptedException {
    PC pc = (new Parser("DEQ(DVAR(ID_1),ADD(DVAR(ID_2),DVAR(ID_4)));DGT(DVAR(ID_4),POW_(DVAR(ID_2),DVAR(ID_3)))")).parsePC();
    Map<Integer, Interval> varIdToIntervals = new RealPaverCaller().callRealPaver(pc, null, 3000, 6);
    assertTrue(varIdToIntervals.containsKey(1));
    assertTrue(varIdToIntervals.containsKey(2));
    assertTrue(varIdToIntervals.containsKey(4));
    //assertFalse(varIdToIntervals.containsKey(3));

    pc = (new Parser("DEQ(DVAR(ID_1),ADD(DVAR(ID_2),DVAR(ID_4)));DGT(DVAR(ID_4),POW_(DVAR(ID_2),DVAR(ID_3)));DEQ(DVAR(ID_1),DCONST(1.55)")).parsePC();
    varIdToIntervals = new RealPaverCaller().callRealPaver(pc, null, 3000, 6);
    assertTrue(varIdToIntervals.containsKey(1));
    //assertTrue(varIdToIntervals.get(1).hi().doubleValue() == varIdToIntervals.get(1).lo().doubleValue() && varIdToIntervals.get(1).lo().doubleValue() == 1.55 );
    assertTrue(varIdToIntervals.containsKey(2));
    assertTrue(varIdToIntervals.containsKey(4));
    //assertFalse(varIdToIntervals.containsKey(3));
  }

  @Test
  public void testGenerateInput() {
    List<String> constraint = Arrays.asList(new String[]{"x5^2 = (x2-x1)^2 + (x4-x3)^2", "min(x4, x2-x4) <= 0", "x1^5 <= x3"});
    List<VarData> data = Arrays.asList(new VarData[]{new VarData(1, false, null), new VarData(2, false, null), new VarData(3, false, null), new VarData(4, false, null), new VarData(5, false, null)});
    String input = RealPaverCaller.generateInput(constraint, 1000, 6, 1, data, false);
    assertTrue(input.contains("precision = 1.0e-6"));
    assertTrue(input.contains("Time = 1000"));
    assertTrue(input.contains("number = 1"));
    assertTrue(input.contains(constraint.get(0)));
    assertTrue(input.contains(constraint.get(1)));
    assertTrue(input.contains(constraint.get(2)));
  }

  @Test
  public void pcWithOnlyUnsupportedOperationsShouldReturnTheOriginalDomainBox() throws Exception {
    PC pc = (new Parser("DNE(DVAR(ID_1),DCONST(1000.0))")).parsePC();
    Map<Integer, Interval> varIdToIntervals = new RealPaverCaller().callSolver(pc, 2000, 3);
    Interval iv = varIdToIntervals.get(1);
    assertEquals(iv.lo().doubleValue(), Config.RANGE.getLo(), 0);
    assertEquals(iv.hi().doubleValue(), Config.RANGE.getHi(), 0);

    List<Interval[]> ivs = new RealPaverCaller().getPaving(pc, new Interval[]{new Interval(-10000, 10000)});
    iv = ivs.get(0)[0];
    assertEquals(ivs.size(), 1);
    assertEquals(iv.lo().doubleValue(), -10000, 0);
    assertEquals(iv.hi().doubleValue(), 10000, 0);
  }

  @Test
  public void testIntegerDivision() throws Exception {
    PC pc = (new Parser("IEQ(IVAR(ID_1),DIV(IVAR(ID_2),MUL(IVAR(ID_2),IVAR(ID_3))))")).parsePC();
    System.out.println(pc);
    Interval[] defaults = new Interval[]{new Interval(-100, 100), new Interval(-100, 100), new Interval(-100, 100)};
    List<Interval[]> boxes = new RealPaverCaller().getPaving(pc, defaults);
    for (Interval[] iv : boxes) {
      System.out.println(Arrays.toString(iv));
    }

  }

  @Test
  public void testIntegerDivisionTricky() throws Exception {
    PC pc = (new Parser("ILE(DIV(IVAR(ID_1),ICONST(128)),ICONST(0))")).parsePC();
    System.out.println(pc);
    Interval[] defaults = new Interval[]{new Interval(-100, 100)};
    List<Interval[]> boxes = new RealPaverCaller().getPaving(pc, defaults);
    for (Interval[] iv : boxes) {
      System.out.println(Arrays.toString(iv));
    }
  }

  @Test
  public void testIntegerDivisionTricky2() throws Exception {
    PC pc = (new Parser("ILE(DIV(DIV(IVAR(ID_1),ICONST(2)),ICONST(64)),ICONST(0))")).parsePC();
    System.out.println(pc);
    Interval[] defaults = new Interval[]{new Interval(-100, 100)};
    List<Interval[]> boxes = new RealPaverCaller().getPaving(pc, defaults);
    for (Interval[] iv : boxes) {
      System.out.println(Arrays.toString(iv));
    }
  }

  @Test
  public void testIntegerDivisionTricky3() throws Exception {
    PC pc = (new Parser("ILE(DIV(DIV(MUL(IVAR(ID_1),IVAR(ID_2)),ICONST(2)),ICONST(64)),ICONST(0))")).parsePC();
    System.out.println(pc);
    Interval[] defaults = new Interval[]{new Interval(0, 10), new Interval(0, 10)};
    List<Interval[]> boxes = new RealPaverCaller().getPaving(pc, defaults);
    for (Interval[] iv : boxes) {
      System.out.println(Arrays.toString(iv));
    }
  }

  @Test @Ignore
  public void testIntegerPaving() throws Exception {
    Path p = Paths.get("inputs/integer/modpow");
//    List<String> lines = ImmutableList.of("ILE(DIV(IVAR(ID_1),ICONST(3)),ICONST(100))");
    PcCompiler compiler = new JavaPcCompiler();

    for (String line : Files.readAllLines(p)) {
//    for (String line : lines) {
      PC pc = (new Parser(line)).parsePC();
      compiler.include(pc);
    }
    Map<PC, Evaluator> pcToCmpCons = compiler.compile();
    Map<PC, Evaluator> tmp = new TreeMap<>(Comparator.comparing(PC::toString));
    tmp.putAll(pcToCmpCons);
    pcToCmpCons = tmp;

    Interval defInterval = new Interval(-1000, 1000);
    Interval[] defaults = new Interval[]{defInterval, defInterval, defInterval};

    Boolean[] allTrue = new Boolean[defaults.length];
    Arrays.fill(allTrue,true);
    BigRational defaultVol = Interval.computeVolume(defaults,allTrue);

    Integer[] evalArgs = new Integer[defaults.length];
    Arrays.fill(evalArgs, defInterval.intLo());

    for (Map.Entry<PC, Evaluator> entry : pcToCmpCons.entrySet()) {
      System.out.println("--------------");
      PC pc = entry.getKey();
      Evaluator cmpConstraint = entry.getValue();
      System.out.println(pc);
      long start = System.nanoTime();
      List<Interval[]> boxes = new RealPaverCaller(true).getPaving(pc, defaults);
      System.out.println("paving done in: " + (System.nanoTime() - start) / Math.pow(10.0,9));

      start = System.nanoTime();
      List<Interval[]> boxesWithoutDivAndMod = new RealPaverCaller(false).getPaving(pc, defaults);
      System.out.println("paving done (no divmod) in: " + (System.nanoTime() - start) / Math.pow(10.0,9));

      boxes = Interval.filterIdenticalBoxes(boxes);
      boxesWithoutDivAndMod = Interval.filterIdenticalBoxes(boxesWithoutDivAndMod);
      BigRational pavingVol = BigRational.ZERO;
      BigRational noDivPavingVol = BigRational.ZERO;

//      System.out.println("--------------");
      for (Interval[] box : boxes) {
        pavingVol = pavingVol.plus(Interval.computeVolume(box,allTrue));
//        System.out.println(Arrays.toString(box));
      }
//      System.out.println("--------------");
      for (Interval[] box : boxesWithoutDivAndMod) {
//        System.out.println(Arrays.toString(box));
        noDivPavingVol = noDivPavingVol.plus(Interval.computeVolume(box,allTrue));
      }

      System.out.println("search space: "
          +  (pavingVol.div(defaultVol)).doubleValue());
      System.out.println("without div or mod: "
          +  (noDivPavingVol.div(defaultVol)).doubleValue());

      double total = defaultVol.doubleValue();
      long explored = 0;
//      System.out.print("explored: 0");
//      do {
//        explored++;
//        if (cmpConstraint.isSAT(evalArgs) && !isInside(evalArgs, boxes)) {
//          boxes.stream().forEach(b -> System.out.println(Arrays.toString(b)));
//          fail("sat point not inside paving: " + Arrays.toString(evalArgs));
//        }
//        if (explored % 100000000 == 0) {
//          double percentage = explored / total;
////          System.out.print("\rexplored: " + percentage + "                          ");
//          System.out.println("explored: " + percentage);
//        }
//      } while (nextArgs(evalArgs, defaults));
    }
  }

  private boolean isInside(Integer[] evalArgs, List<Interval[]> boxes) {
    outer: for (Interval[] box : boxes) {
      for (int i = 0; i < evalArgs.length; i++) {
        if (!box[i].getIntRange().get().contains(evalArgs[i])) {
          continue outer;
        }
      }
      return true;
    }
    return false;
  }

  private boolean nextArgs(Integer[] evalArgs, Interval[] defaults) {
    boolean hasNext = false;
    for (int i = 0; i < evalArgs.length; i++) {
      if (evalArgs[i] == defaults[i].intHi()) {
        evalArgs[i] = defaults[i].intLo();
      } else {
        evalArgs[i]++;
        hasNext = true;
        break;
      }
    }
    return hasNext;
  }
}
