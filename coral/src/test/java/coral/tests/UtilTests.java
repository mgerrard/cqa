package coral.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import coral.solvers.Type;
import coral.solvers.Unit;
import coral.solvers.rand.Util;

public class UtilTests {

  @Test
  public void testUtilIsRadian() {
    Unit unit = Unit.ATAN;
    assertTrue(unit.compareTo(Unit.DOUBLE) < 0);
    assertTrue(unit.compareTo(Unit.LIMITED_BOOLEAN) < 0);
    assertTrue(unit.compareTo(Unit.LIMITED_DOUBLE) < 0);
    assertTrue(unit.compareTo(Unit.LIMITED_FLOAT) < 0);
    assertTrue(unit.compareTo(Unit.LIMITED_INT) < 0);
    assertTrue(unit.compareTo(Unit.LIMITED_LONG) < 0);
    assertTrue(unit.compareTo(Unit.ASIN_ACOS) > 0);
    assertTrue(unit.compareTo(Unit.DEGREES) > 0);
    assertTrue(unit.compareTo(Unit.RADIANS) > 0);
    
    assertTrue(Util.isRadian(new Type(Unit.ASIN_ACOS, 0, 0)));
    assertTrue(Util.isRadian(new Type(Unit.DEGREES, 0, 0)));
    assertTrue(Util.isRadian(new Type(Unit.RADIANS, 0, 0)));
    assertTrue(Util.isRadian(new Type(Unit.ATAN, 0, 0)));
  }
}
