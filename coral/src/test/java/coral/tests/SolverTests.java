package coral.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import symlib.parser.Parser;
import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.util.Config;
import coral.util.Range;

public class SolverTests {

  @Test
  public void testUnsatCase() throws Exception {
    String pcstring = "DGT(DVAR(ID_1),DCONST(3));DLT(DVAR(ID_1),DCONST(-1))";
    PC pc = (new Parser(pcstring)).parsePC();
    
    Solver solver = SolverKind.PSO_OPT4J.get();
    Env env = solver.getCallable(pc).call();
    
    assertTrue(env.getResult() == Result.UNSAT);
  }
  
  @Test
  public void testEricCase() throws Exception {
    String pcstring = "BNOT(DEQ(DVAR(ID_6),DCONST(7.0)))";
    PC pc = (new Parser(pcstring)).parsePC();
    
    Solver solver = SolverKind.PSO_OPT4J.get();
    Env env = solver.getCallable(pc).call();
    
    assertTrue(env.getResult() == Result.SAT);
  }
  
  @Test
  public void testBihuanCase() throws Exception {
    boolean oldFlex = Config.flexibleRange;
    Config.flexibleRange = false;
    boolean oldIndividual = Config.insertZeroIndividual;
    Config.insertZeroIndividual = false;
    Range oldRange = Config.RANGE;
    Config.RANGE = new Range(1500, 2500);
    
    try {
      String pcstring = "DLE(DVAR(ID_1), DCONST(1000.0))";
      PC pc = (new Parser(pcstring)).parsePC();
      
      Solver solver = SolverKind.PSO_OPT4J.get();
      Env env = solver.getCallable(pc).call();
      
      assertTrue(env.getResult() == Result.UNK);
    } finally {
      Config.flexibleRange = oldFlex;
      Config.insertZeroIndividual = oldIndividual;
      Config.RANGE = oldRange;
    }
  }
}
