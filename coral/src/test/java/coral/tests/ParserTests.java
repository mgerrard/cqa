package coral.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBinaryExpression;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.util.visitors.CoralVisitor;

public class ParserTests {

  @Test
  public void testCastToDouble() throws ParseException {
    String cons1 = "DEQ(DVAR(ID_1),ASDOUBLE(IVAR(ID_2)))";
    
    PC pc = (new Parser(cons1)).parsePC();
    SymDoubleRelational bool = (SymDoubleRelational) pc.getConstraints().get(0);
    SymNumber b = ((SymAsDouble) bool.getB()).getArg();

    assertTrue(bool.getA() instanceof SymDoubleLiteral);
    assertTrue(bool.getB() instanceof SymAsDouble);
    assertTrue(b instanceof SymIntLiteral);
  }
  
  @Test
  public void testCastToInt() throws ParseException {
    String cons1 = "IEQ(IVAR(ID_1),ASINT(DVAR(ID_2)))";
    
    PC pc = (new Parser(cons1)).parsePC();
    SymIntRelational bool = (SymIntRelational) pc.getConstraints().get(0);
    SymNumber b = ((SymAsInt) bool.getB()).getArg();

    assertTrue(bool.getA() instanceof SymIntLiteral);
    assertTrue(bool.getB() instanceof SymAsInt);
    assertTrue(b instanceof SymDoubleLiteral);
  }
  
  @Test
  public void testPCtoInputLanguage () throws ParseException {
    String cons1 = "IEQ(IVAR(ID_1),ASINT(DVAR(ID_2)))";
    String cons2 = "DEQ(DVAR(ID_1),ASDOUBLE(IVAR(ID_2)))";
    
    PC pc1 = (new Parser(cons1)).parsePC();
    PC pc2 = (new Parser(cons2)).parsePC();
    
    String back1 = CoralVisitor.processPC(pc1);
    String back2 = CoralVisitor.processPC(pc2);
    
    assertEquals(back1, cons1);
    assertEquals(back2, cons2);

  }
  
  @Test 
  public void testBNOTWithCanonicalizator() throws ParseException {
    String cons = "BNOT(IGT(IVAR(ID_1),IVAR(ID_2)))";
    PC origPC = (new Parser(cons)).parsePC();
    PC canon = origPC.getCanonicalForm();
    String out = canon.toString();
    String back = CoralVisitor.processPC(canon);
    
    assertEquals(out, "!($V1 > $V2)");
    assertEquals(back, cons);
  }
  
  @Test
  public void testMAXMIN() throws ParseException {
    String cons = "DGT(MAX_(DCONST(1),DCONST(0.99)),MIN_(DCONST(1),DCONST(0.99)))";
    PC pc = (new Parser(cons)).parsePC();
    
    SymDoubleRelational bool = (SymDoubleRelational) pc.getConstraints().get(0);
    SymMathBinary left = ((SymMathBinary) bool.getA());
    SymMathBinary right = ((SymMathBinary) bool.getB());

    assertTrue(left.getArg1() instanceof SymDoubleConstant);
    assertTrue(left.getArg2() instanceof SymDoubleConstant);
    assertTrue(right.getArg1() instanceof SymDoubleConstant);
    assertTrue(right.getArg2() instanceof SymDoubleConstant);
    
    assertEquals(left.getOp(), SymMathBinary.MAX);
    assertEquals(right.getOp(), SymMathBinary.MIN);

    assertTrue(pc.eval());
  }
}
