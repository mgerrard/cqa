package coral.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import symlib.SymLiteral;
import symlib.parser.Parser;
import coral.PC;
import coral.simplifier.Rewrite;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.Type;
import coral.util.Config;
import coral.util.Range;
import coral.util.visitors.SymLiteralTypeSearcher;

public class TypeSearchTests {
  
  static Range oldRange = null;
  
  @Before 
  public void setConfigRangeTo100() {
    oldRange = Config.RANGE;
    Config.RANGE = new Range(-100,100);
  }
  
  @After
  public void restoreConfigRange() {
    Config.RANGE = oldRange;
  }
  
  @Test
  public void testIntervalInference1() throws Exception {
    Parser p = new Parser("DLT(DVAR(ID_1),DCONST(-1));DGT(DVAR(ID_1),DCONST(-1.9))");
    PC pc = p.parsePC();
    Set<SymLiteral> vars = pc.getSortedVars();
    Iterator<SymLiteral> iter = vars.iterator();
    SymLiteral id1 = iter.next();
    
    SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,Config.flexibleRange);
    Map<SymLiteral, Type> varToType = tSearcher.getUnits();
    Type t1 = varToType.get(id1);
    
    assertEquals(-1,t1.getHi());
    assertEquals(-2,t1.getLo());
  }
  
  @Test
  public void testIntervalInference2() throws Exception {
    System.out.println(Config.flexibleRange);
    Parser p = new Parser("DLT(SQRT_(EXP_(ADD(DVAR(ID_1),DVAR(ID_2)))),POW_(DVAR(ID_3),DVAR(ID_1)));" +
        "DGT(DVAR(ID_1),DCONST(0));" +
        "DGT(DVAR(ID_2),DCONST(1));" +
        "DGT(DVAR(ID_3),DCONST(1));" +
        "DLE(DVAR(ID_2),ADD(DVAR(ID_1),DCONST(2)))");
    PC pc = p.parsePC();
    Set<SymLiteral> vars = pc.getSortedVars();
    Iterator<SymLiteral> iter = vars.iterator();
    SymLiteral id1 = iter.next();
    SymLiteral id2 = iter.next();
    SymLiteral id3 = iter.next();
    
    SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,Config.flexibleRange);
    Map<SymLiteral, Type> varToType = tSearcher.getUnits();
    Type t1 = varToType.get(id1);
    Type t2 = varToType.get(id2);
    Type t3 = varToType.get(id3);
    
    assertEquals(100,t1.getHi());
    assertEquals(0,t1.getLo());
    assertEquals(100,t2.getHi());
    assertEquals(1,t2.getLo());
    assertEquals(100,t3.getHi());
    assertEquals(1,t3.getLo());
  }

  @Test
  public void testIntervalInference3() throws Exception {
    Parser p = new Parser("DLT(SQRT_(EXP_(ADD(DVAR(ID_1),DVAR(ID_2)))),POW_(DVAR(ID_3),DVAR(ID_1)));" +
        "DGT(DVAR(ID_1),DCONST(0));" +
        "DGT(DVAR(ID_2),DCONST(1000000));" +
        "DLT(DVAR(ID_3),DCONST(100000009));" +
        "DLT(DVAR(ID_3),DCONST(100000008));" +
        "DLT(DVAR(ID_4),DCONST(-500));" +
        "DLE(DVAR(ID_2),ADD(DVAR(ID_1),DCONST(2)))");
    PC pc = p.parsePC();
    Set<SymLiteral> vars = pc.getSortedVars();
    Iterator<SymLiteral> iter = vars.iterator();
    SymLiteral id1 = iter.next();
    SymLiteral id2 = iter.next();
    SymLiteral id3 = iter.next();
    SymLiteral id4 = iter.next();
    
    SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,true);
    Map<SymLiteral, Type> varToType = tSearcher.getUnits();
    Type t1 = varToType.get(id1);
    Type t2 = varToType.get(id2);
    Type t3 = varToType.get(id3);
    Type t4 = varToType.get(id4);
    
    assertEquals(100,t1.getHi());
    assertEquals(0,t1.getLo());
    assertEquals(1000200,t2.getHi());
    assertEquals(1000000,t2.getLo());
    assertEquals(100000008,t3.getHi());
    assertEquals(-100,t3.getLo());
    assertEquals(-500,t4.getHi());
    assertEquals(-700,t4.getLo());
  }
  
  @Test
  public void testIntervalInference4() throws Exception {
    Parser p = new Parser(Benchmark.pc96);
    PC pc = p.parsePC();
    Set<SymLiteral> vars = pc.getSortedVars();
    Iterator<SymLiteral> iter = vars.iterator();
    SymLiteral id1 = iter.next();
    
    SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,true);
    Map<SymLiteral, Type> varToType = tSearcher.getUnits();
    Type t1 = varToType.get(id1);
    
    assertEquals(1000,t1.getHi());
    assertEquals(-100,t1.getLo());
  }
  
  @Test @Ignore
  public void testIntervalInference4opt() throws Exception {
    Parser p = new Parser(Benchmark.pc96);
    PC pc = Rewrite.rew(p.parsePC());
     System.out.println(pc);
    Set<SymLiteral> vars = pc.getSortedVars();
    Iterator<SymLiteral> iter = vars.iterator();
    SymLiteral id1 = iter.next();
    
    SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,true);
    Map<SymLiteral, Type> varToType = tSearcher.getUnits();
    Type t1 = varToType.get(id1);
    
    assertEquals(1000,t1.getHi());
    assertEquals(999,t1.getLo());
  }
  
  @Test
  public void testNonFlexibleIntervalInference() throws Exception {
      Parser p = new Parser("DGE(DVAR(ID_1),DCONST(1200));DLE(DVAR(ID_1),DCONST(1200))");
      PC pc = p.parsePC();
      
      Set<SymLiteral> vars = pc.getSortedVars();
      Iterator<SymLiteral> iter = vars.iterator();
      SymLiteral id1 = iter.next();
      
      { // flexible range enabled
        SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,true);
        Map<SymLiteral, Type> varToType = tSearcher.getUnits();
        Type t1 = varToType.get(id1);
        
        assertEquals(1200,t1.getHi());
        assertEquals(1200,t1.getLo());
      }
      
      { //flexible range disabled
        SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,false);
        Map<SymLiteral, Type> varToType = tSearcher.getUnits();
        Type t1 = varToType.get(id1);
        
        assertEquals(100,t1.getHi());
        assertEquals(-100,t1.getLo());        
      }
  }
  
  @Test
  public void testBihuanCase() throws Exception {
    Config.RANGE = new Range(1500, 2500);
    String pcstring = "DLE(DVAR(ID_1), DCONST(1000.0))";
    PC pc = (new Parser(pcstring)).parsePC();
    
    Set<SymLiteral> vars = pc.getSortedVars();
    Iterator<SymLiteral> iter = vars.iterator();
    SymLiteral id1 = iter.next();
    
    { // flexible range enabled
      SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,true);
      Map<SymLiteral, Type> varToType = tSearcher.getUnits();
      Type t1 = varToType.get(id1);
      
      assertEquals(1000,t1.getHi());
      assertEquals(0,t1.getLo());
    }
    
    { //flexible range disabled
      SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,false);
      Map<SymLiteral, Type> varToType = tSearcher.getUnits();
      Type t1 = varToType.get(id1);
      
      assertEquals(2500,t1.getHi());
      assertEquals(1500,t1.getLo());        
    }
  }

}
