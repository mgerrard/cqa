package coral.tests;

import static org.junit.Assert.assertTrue;

import coral.PC;
import java.util.Map;
import org.junit.Test;
import symlib.eval.Evaluator;
import symlib.eval.compilation.JavaPcCompiler;
import symlib.parser.ParseException;
import symlib.parser.Parser;

public class CompilerTests {

  @Test
  public void compileCasts() throws ParseException {
    PC pc = (new Parser("IEQ(IVAR(ID_1),ASINT(DCONST(1.1)))")).parsePC();
    JavaPcCompiler compiler = new JavaPcCompiler();
    compiler.include(pc);
    Map<PC,Evaluator> evalMap = compiler.compile();

    assertTrue(evalMap.get(pc).isSAT(new Number[]{1}));
  }
}
