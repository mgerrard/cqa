package coral.tests;

// import gnu.jel.CompilationException;
// import gnu.jel.CompiledExpression;
// import gnu.jel.Evaluator;
// import gnu.jel.Library;

// import java.util.List;

// import org.junit.Test;

// import symlib.SymBool;
// import symlib.SymLiteral;
// import symlib.eval.Elem;
// import symlib.eval.ReversePolish;
// import symlib.parser.Parser;
// import coral.PC;
// import coral.counters.refactoring.CountingUtils;

public class BytecodeGenerationTests {

//   static String constraint = "DGT(DIV(DIV(SUB(DVAR(ID_1),DVAR(ID_2)),DCONST(0.055)),DCONST(2.0)),DCONST(0.0));DGT(MUL(DCONST(0.18181818181818185),SUB(ADD(MUL(DCONST(0.5),MUL(DIV(DIV(DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_1)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)),DCONST(0.055)),DCONST(2.0)),DIV(DIV(DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_1)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)),DCONST(0.055)),DCONST(2.0)))),DIV(DIV(SUB(DVAR(ID_1),DVAR(ID_2)),DCONST(0.055)),DCONST(2.0))),DCONST(0.04759988869075444))),DCONST(0.0));DLT(DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),SUB(DVAR(ID_3),DVAR(ID_4))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_5),DVAR(ID_6)))),DCONST(7.855339059327378E-4)),DCONST(2.0)),DCONST(0.0));DGT(MUL(DCONST(0.18181818181818185),SUB(ADD(MUL(DCONST(0.5),MUL(DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(7.855339059327378E-4)),DCONST(2.0)),DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(7.855339059327378E-4)),DCONST(2.0)))),SUB(DCONST(0.0),DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),SUB(DVAR(ID_3),DVAR(ID_4))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_5),DVAR(ID_6)))),DCONST(7.855339059327378E-4)),DCONST(2.0)))),DCONST(3.332757323673897))),DCONST(0.0));DLT(DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_3),DVAR(ID_4))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_5),DVAR(ID_6)))),DCONST(0.006285533905932738)),DCONST(2.0)),DCONST(0.0));DGT(MUL(DCONST(0.18181818181818185),SUB(ADD(MUL(DCONST(0.5),MUL(DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(0.006285533905932738)),DCONST(2.0)),DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(0.006285533905932738)),DCONST(2.0)))),SUB(DCONST(0.0),DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_3),DVAR(ID_4))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_5),DVAR(ID_6)))),DCONST(0.006285533905932738)),DCONST(2.0)))),DCONST(0.4165109785694488))),DCONST(0.0));DLT(DIV(DIV(SUB(DVAR(ID_7),DVAR(ID_8)),DCONST(0.055)),DCONST(2.0)),DCONST(0.0));DGT(MUL(DCONST(0.18181818181818185),SUB(ADD(MUL(DCONST(0.5),MUL(DIV(DIV(DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_7)),MUL(DCONST(-10.0),DVAR(ID_1))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_1)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0)),DCONST(0.055)),DCONST(2.0)),DIV(DIV(DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_7)),MUL(DCONST(-10.0),DVAR(ID_1))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_1)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0)),DCONST(0.055)),DCONST(2.0)))),SUB(DCONST(0.0),DIV(DIV(SUB(DVAR(ID_7),DVAR(ID_8)),DCONST(0.055)),DCONST(2.0)))),DCONST(0.04759988869075444))),DCONST(0.0));DLT(DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),SUB(DVAR(ID_9),DVAR(ID_10))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_11),DVAR(ID_12)))),DCONST(7.855339059327378E-4)),DCONST(2.0)),DCONST(0.0));DGT(MUL(DCONST(0.18181818181818185),SUB(ADD(MUL(DCONST(0.5),MUL(DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_9)),MUL(DCONST(-10.0),DVAR(ID_3))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_11)),MUL(DCONST(-10.0),DVAR(ID_5))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0)))),DCONST(7.855339059327378E-4)),DCONST(2.0)),DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_9)),MUL(DCONST(-10.0),DVAR(ID_3))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_11)),MUL(DCONST(-10.0),DVAR(ID_5))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0)))),DCONST(7.855339059327378E-4)),DCONST(2.0)))),SUB(DCONST(0.0),DIV(DIV(ADD(ADD(MUL(DCONST(-0.7071067811865476),SUB(DVAR(ID_9),DVAR(ID_10))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_11),DVAR(ID_12)))),DCONST(7.855339059327378E-4)),DCONST(2.0)))),DCONST(3.332757323673897))),DCONST(0.0));DLT(DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_9),DVAR(ID_10))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_11),DVAR(ID_12)))),DCONST(0.006285533905932738)),DCONST(2.0)),DCONST(0.0));DLT(MUL(DCONST(0.18181818181818185),SUB(ADD(MUL(DCONST(0.5),MUL(DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_9)),MUL(DCONST(-10.0),DVAR(ID_3))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_11)),MUL(DCONST(-10.0),DVAR(ID_5))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0)))),DCONST(0.006285533905932738)),DCONST(2.0)),DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_9)),MUL(DCONST(-10.0),DVAR(ID_3))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_3)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_11)),MUL(DCONST(-10.0),DVAR(ID_5))),MUL(DCONST(0.0),DIV(SUB(ADD(MUL(DCONST(10.0),DVAR(ID_5)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)))),DCONST(1.0)))),DCONST(0.006285533905932738)),DCONST(2.0)))),SUB(DCONST(0.0),DIV(DIV(ADD(ADD(MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_9),DVAR(ID_10))),DCONST(0.0)),MUL(DCONST(0.7071067811865476),SUB(DVAR(ID_11),DVAR(ID_12)))),DCONST(0.006285533905932738)),DCONST(2.0)))),DCONST(0.4165109785694488))),DCONST(0.0))";
//   static String jelConstraint = "((((x1-x2)/0.055)/2.0) > 0.0) | ((0.18181818181818185*(((0.5*(((((((10.0*x1)+-0.0)-0.0)/1.0)/0.055)/2.0)*((((((10.0*x1)+-0.0)-0.0)/1.0)/0.055)/2.0)))+(((x1-x2)/0.055)/2.0))-0.04759988869075444)) > 0.0) | ((((((-0.7071067811865476*(x3-x4))+0.0)+(0.7071067811865476*(x5-x6)))/7.855339059327378E-4)/2.0) < 0.0) | ((0.18181818181818185*(((0.5*((((((-0.7071067811865476*((((10.0*x3)+-0.0)-0.0)/1.0))+0.0)+(0.7071067811865476*((((10.0*x5)+-0.0)-0.0)/1.0)))/7.855339059327378E-4)/2.0)*(((((-0.7071067811865476*((((10.0*x3)+-0.0)-0.0)/1.0))+0.0)+(0.7071067811865476*((((10.0*x5)+-0.0)-0.0)/1.0)))/7.855339059327378E-4)/2.0)))+(0.0-(((((-0.7071067811865476*(x3-x4))+0.0)+(0.7071067811865476*(x5-x6)))/7.855339059327378E-4)/2.0)))-3.332757323673897)) > 0.0) | ((((((0.7071067811865476*(x3-x4))+0.0)+(0.7071067811865476*(x5-x6)))/0.006285533905932738)/2.0) < 0.0) | ((0.18181818181818185*(((0.5*((((((0.7071067811865476*((((10.0*x3)+-0.0)-0.0)/1.0))+0.0)+(0.7071067811865476*((((10.0*x5)+-0.0)-0.0)/1.0)))/0.006285533905932738)/2.0)*(((((0.7071067811865476*((((10.0*x3)+-0.0)-0.0)/1.0))+0.0)+(0.7071067811865476*((((10.0*x5)+-0.0)-0.0)/1.0)))/0.006285533905932738)/2.0)))+(0.0-(((((0.7071067811865476*(x3-x4))+0.0)+(0.7071067811865476*(x5-x6)))/0.006285533905932738)/2.0)))-0.4165109785694488)) > 0.0) | ((((x7-x8)/0.055)/2.0) < 0.0) | ((0.18181818181818185*(((0.5*(((((((10.0*x7)+(-10.0*x1))-(0.0*((((10.0*x1)+-0.0)-0.0)/1.0)))/1.0)/0.055)/2.0)*((((((10.0*x7)+(-10.0*x1))-(0.0*((((10.0*x1)+-0.0)-0.0)/1.0)))/1.0)/0.055)/2.0)))+(0.0-(((x7-x8)/0.055)/2.0)))-0.04759988869075444)) > 0.0) | ((((((-0.7071067811865476*(x9-x10))+0.0)+(0.7071067811865476*(x11-x12)))/7.855339059327378E-4)/2.0) < 0.0) | ((0.18181818181818185*(((0.5*((((((-0.7071067811865476*((((10.0*x9)+(-10.0*x3))-(0.0*((((10.0*x3)+-0.0)-0.0)/1.0)))/1.0))+0.0)+(0.7071067811865476*((((10.0*x11)+(-10.0*x5))-(0.0*((((10.0*x5)+-0.0)-0.0)/1.0)))/1.0)))/7.855339059327378E-4)/2.0)*(((((-0.7071067811865476*((((10.0*x9)+(-10.0*x3))-(0.0*((((10.0*x3)+-0.0)-0.0)/1.0)))/1.0))+0.0)+(0.7071067811865476*((((10.0*x11)+(-10.0*x5))-(0.0*((((10.0*x5)+-0.0)-0.0)/1.0)))/1.0)))/7.855339059327378E-4)/2.0)))+(0.0-(((((-0.7071067811865476*(x9-x10))+0.0)+(0.7071067811865476*(x11-x12)))/7.855339059327378E-4)/2.0)))-3.332757323673897)) > 0.0) | ((((((0.7071067811865476*(x9-x10))+0.0)+(0.7071067811865476*(x11-x12)))/0.006285533905932738)/2.0) < 0.0) | ((0.18181818181818185*(((0.5*((((((0.7071067811865476*((((10.0*x9)+(-10.0*x3))-(0.0*((((10.0*x3)+-0.0)-0.0)/1.0)))/1.0))+0.0)+(0.7071067811865476*((((10.0*x11)+(-10.0*x5))-(0.0*((((10.0*x5)+-0.0)-0.0)/1.0)))/1.0)))/0.006285533905932738)/2.0)*(((((0.7071067811865476*((((10.0*x9)+(-10.0*x3))-(0.0*((((10.0*x3)+-0.0)-0.0)/1.0)))/1.0))+0.0)+(0.7071067811865476*((((10.0*x11)+(-10.0*x5))-(0.0*((((10.0*x5)+-0.0)-0.0)/1.0)))/1.0)))/0.006285533905932738)/2.0)))+(0.0-(((((0.7071067811865476*(x9-x10))+0.0)+(0.7071067811865476*(x11-x12)))/0.006285533905932738)/2.0)))-0.4165109785694488)) < 0.0)";
//   static int numberEvals = 100000;

//   @Test
//   public void testEval() throws Exception {
//     PC pc = (new Parser(constraint)).parsePC();
// //    System.out.println(pc.toString());

//     long coralEvalStart = System.nanoTime();
//     List<Elem[]> l = CountingUtils.prepareRPNState(pc);
//     for (int i = 0; i < numberEvals; i++) {
//       for (SymLiteral lit : pc.getSortedVars()) {
//         lit.setCte(i);
//       }
//       /** reverse polish code **/
//       evalAll(l);
//     }
//     long coralEvalEnd = System.nanoTime();
//     System.out.println("coralEval finished in (ns)\t"
//         + (coralEvalEnd - coralEvalStart));

//     // Set up the library
//     Class[] staticLib = new Class[1];
//     try {
//       staticLib[0] = Class.forName("java.lang.Math");
//     } catch (ClassNotFoundException e) {
//       // Can't be ;)) ...... in java ... ;)
//     }

//     Class[] dynamicLib = new Class[1];
//     VariableProvider variables = new VariableProvider();
//     Object[] context = new Object[1];
//     context[0] = variables;
//     dynamicLib[0] = variables.getClass();

//     Library lib = new Library(staticLib, dynamicLib, null, null, null);
//     lib.markStateDependent("random", null);

//     long jelCompilationStart = System.nanoTime();
//     // Compile
//     CompiledExpression expr_c = null;
//     try {
//       expr_c = Evaluator.compile(jelConstraint, lib);
//     } catch (CompilationException ce) {
//       System.err.print("–––COMPILATION ERROR :");
//       System.err.println(ce.getMessage());
//       System.err.print("                       ");
//       System.err.println(jelConstraint);
//       int column = ce.getColumn(); // Column, where error was found
//       for (int i = 0; i < column + 23 - 1; i++)
//         System.err.print(' ');
//       System.err.println('^');
//     }
//     long jelCompilationEnd = System.nanoTime();
//     System.out.println("Jel compilation cost (ns)\t"
//         + (jelCompilationEnd - jelCompilationStart));

//     // eval
//     try {
//       for (int i = 0; i < numberEvals; i++) {
//         variables.x1 = variables.x2 = variables.x3 = variables.x4 
//             = variables.x5 = variables.x6 = variables.x7 = variables.x8 
//             = variables.x9 = variables.x10 = variables.x11 = variables.x12 
//             = variables.x13 = variables.x14 = variables.x15 = i;
//         expr_c.evaluate(context);
//       }
//       ;
//     } catch (Throwable e) {
//       System.err.println("Exception emerged from JEL compiled"
//           + " code (IT'S OK) :");
//       System.err.print(e);
//     }
//     ;
//     System.out.println("Jel execution cost (ns)\t"
//         + (System.nanoTime() - jelCompilationEnd));

//   }

//   public static void evalAll(List<Elem[]> l) {
//     for (Elem[] elem : l) {
//       ReversePolish rpol = new ReversePolish(elem);
//       rpol.eval();
//     }
//   }

//   public static class VariableProvider {
//     public double x1;
//     public double x2;
//     public double x3;
//     public double x4;
//     public double x5;
//     public double x6;
//     public double x7;
//     public double x8;
//     public double x9;
//     public double x10;
//     public double x11;
//     public double x12;
//     public double x13;
//     public double x14;
//     public double x15;

//     public double x1() {
//       return x1;
//     };

//     public double x2() {
//       return x2;
//     };

//     public double x3() {
//       return x3;
//     };

//     public double x4() {
//       return x4;
//     };

//     public double x5() {
//       return x5;
//     };

//     public double x6() {
//       return x6;
//     };

//     public double x7() {
//       return x7;
//     };

//     public double x8() {
//       return x8;
//     };

//     public double x9() {
//       return x9;
//     };

//     public double x10() {
//       return x10;
//     };

//     public double x11() {
//       return x11;
//     };

//     public double x12() {
//       return x12;
//     };

//     public double x13() {
//       return x13;
//     };

//     public double x14() {
//       return x14;
//     };

//     public double x15() {
//       return x15;
//     };
//   }
}
