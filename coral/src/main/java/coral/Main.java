package coral;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.tests.Helper;
import coral.util.Config;
import coral.util.options.Option;
import coral.util.options.Options;

public class Main {
  
  @Option("solver")
  public static String solver = "PSO_OPT4J";
  
  private static Options options = new Options(Main.class, Config.class);
  
  public static void load(String[] args){
    if (args != null && args.length > 0) {
      Options.loadOptions(args, options);
      Options.loadOptions(args, options);
    }
  }
  
  public static void printOptions() {
    System.out.println("parameters:");
    System.out.println("==========================================");
    System.out.println(options.settings());
    System.out.println("==========================================");
  }
  
  public static void main(String[] args) throws ParseException, InterruptedException, ExecutionException, FileNotFoundException, IOException {
    
    
    load(args);
    printOptions();
    
    SolverKind kind;
    if (solver.contains("PSO_OPT4J")) {
      kind = SolverKind.PSO_OPT4J; 
    } else if (solver.contains("GA_OPT4J")) {
      kind = SolverKind.GA_OPT4J; 
    } else if (solver.equals("RANDOM")) {
      kind = SolverKind.RANDOM; 
    } else {
      throw new RuntimeException("did not set solver");
    }
    
    if (Config.inputCONS == null) {
      System.err.println("No input constraint");
      System.err.println("format: java -jar coral.jar --inputCONS=<constraint>");
      System.exit(1);
    }
    
    
    String pcStr = Config.inputCONS;
    PC pc = (new Parser(pcStr)).parsePC();
    Solver solver = kind.get();
    
    Env env = null;
    char status; /*.=Solved, x=Unsolved, T=Timeout*/
    try {        
      env = Helper.solveAndPrint(solver, pc);
      status = env != null && env.getResult() == Result.SAT ? '.' : 'x';
    } catch (TimeoutException _) {
      status = 'T';
    }
    
    boolean isSolved = status == '.';
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(isSolved?"SOLVED":"NOT SOLVED");
    strBuffer.append(": ");
    strBuffer.append(pc);     
    strBuffer.append("\n");
    if (isSolved) {
      strBuffer.append("SOL: ");
      strBuffer.append(env);
      strBuffer.append("\n");
    }
    
    System.out.println(strBuffer);

  }

}
