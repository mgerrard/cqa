package coral;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import symlib.SymBool;
import symlib.SymLiteral;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import coral.counters.CountingProblem;
import coral.counters.estimates.EstimateResult;
import coral.counters.estimates.PCEstimateResult;
import coral.util.visitors.SymLiteralSearcher;
import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

public abstract class PartitionedCache {

  long cacheHits = 0;
  long cacheMisses = 0;
  List<Long> cacheGets = new ArrayList<Long>();
  List<Long> cachePuts = new ArrayList<Long>();

  protected PartitionResult partitionResult;

  public static class PartitionResult {
    private HashMultimap<String, SymBool> constraintsRelatedToAVar;
    private ImmutableSet<Set<String>> independentVarsClusters;

    public PartitionResult(
        HashMultimap<String, SymBool> constraintsRelatedToAVar,
        ImmutableSet<Set<String>> independentVarsClusters) {
      this.constraintsRelatedToAVar = constraintsRelatedToAVar;
      this.independentVarsClusters = independentVarsClusters;
    }

    public ImmutableSet<Set<String>> getIndependentVarsClusters() {
      return independentVarsClusters;
    }
  }

  public void loadPartition(List<PC> pcs) {
    List<SymBool> conjunctionOfAllThePCs = Lists.newLinkedList();
    for (PC pc : pcs) {
      conjunctionOfAllThePCs.addAll(pc.getConstraints());
    }
    this.partitionResult = partition(conjunctionOfAllThePCs);
  }

  public void loadProblems(List<CountingProblem> problems) {
    List<SymBool> conjunctionOfAllThePCs = Lists.newLinkedList();
    for (CountingProblem pr : problems) {
      conjunctionOfAllThePCs.addAll(pr.pc.getConstraints());
    }
    this.partitionResult = partition(conjunctionOfAllThePCs);
  }

  public void loadProblems(Iterator<SymBool> pcConjunctions) {
    this.partitionResult = partition(pcConjunctions);
  }

  /***
   * partition one conjunction of constraints
   * 
   * @param conjuncts
   * @return
   */
  private PartitionResult partition(List<SymBool> conjuncts) {
    return partition(conjuncts.iterator());
  }

  private PartitionResult partition(Iterator<SymBool> conjuncts) {
    HashMultimap<String, SymBool> constraintsRelatedToAVar = HashMultimap
        .<String, SymBool> create();
    UndirectedSparseGraph<String, Integer> dependencyGraph = new UndirectedSparseGraph<String, Integer>();
    int edgeCounter = 0;
    while (conjuncts.hasNext()) {
      SymBool constraint = conjuncts.next();
      SymLiteralSearcher vis = new SymLiteralSearcher();
      constraint.accept(vis);
      ArrayList<String> vars = new ArrayList<String>();
      for (SymLiteral v : vis.getVars()) {
        vars.add(v.toString());
      }
      // ArrayList<String> vars = new
      // ArrayList<String>(constraint.getVarsSet());
      for (int i = 0; i < vars.size(); i++) {
        constraintsRelatedToAVar.put(vars.get(i), constraint);
        // Notice j=i to add self-dependency
        for (int j = i; j < vars.size(); j++) {
          dependencyGraph.addEdge(edgeCounter++, vars.get(i), vars.get(j));
        }
      }
    }
    WeakComponentClusterer<String, Integer> clusterer = new WeakComponentClusterer<String, Integer>();
    Set<Set<String>> clusters = clusterer.transform(dependencyGraph);
    ImmutableSet<Set<String>> independentVarsClusters = ImmutableSet
        .<Set<String>> copyOf(clusters);
    return new PartitionResult(constraintsRelatedToAVar,
        independentVarsClusters);
  }

  // HEADS-UP! For some reason, the hash of the Set<SymBool> depends on
  // the value of the toString method. Use extreme caution if you change
  // the behavior of toString for something, like debugging (using flags
  // like showConcreteValues, for example).
  Map<Set<SymBool>, EstimateResult> cacheMap = new HashMap<Set<SymBool>, EstimateResult>();

  Map<Set<SymBool>, EstimateResult> internalCache = Maps.newHashMap();
  private long internalCacheMisses = 0;
  private long internalCacheHits = 0;

  public final EstimateResult getCached(Set<SymBool> problem) {
    if (!internalCache.containsKey(problem)) {
      EstimateResult result = processConstraint(problem);
      addToCache(problem, result);
      internalCacheMisses++;
    } else {
      internalCacheHits++;
    }
    return internalCache.get(problem);
  }

  public final void addToCache(Set<SymBool> problem, EstimateResult result) {
    internalCache.put(problem, result);
  }

  public final long getCacheMisses() {
    return internalCacheMisses;
  }

  public final long getCacheHits() {
    return internalCacheHits;
  }

  private final LoadingCache<Set<SymBool>, EstimateResult> cachea = CacheBuilder
      .newBuilder().maximumSize(1000000).recordStats()
      .build(new CacheLoader<Set<SymBool>, EstimateResult>() {
        public EstimateResult load(Set<SymBool> problem) {
          // System.out.println("cache miss: " + cache.stats().missCount() +
          // " hit: " + cache.stats().hitCount());
          // System.out.println("problem: " + problem);
          return processConstraint(problem);
        }
      });

  public void clearCache() {
    /*System.out.println("[regCoral] Hit rate:         "
        + cache.stats().hitRate());
    System.out.println("[regCoral] Hit count:        "
        + cache.stats().hitCount());
    System.out.println("[regCoral] Miss count:       "
        + cache.stats().missCount());
    System.out.println("[regCoral] Avg load penalty: "
        + cache.stats().averageLoadPenalty());
    System.out.println("[regCoral] Evictions:        "
        + cache.stats().evictionCount());*/
    internalCache=Maps.newHashMap();
    internalCacheHits=0l;
    internalCacheMisses=0l;
    //cache.invalidateAll();
    // cache.cleanUp();

    /*
     * cache.clear(); cacheHits = 0; cacheMisses = 0; cacheGets = new
     * ArrayList<Long>(); cachePuts = new ArrayList<Long>();
     */
  }

  /***
   * run the analysis (e.g., SAT solving, model counting, volume computation,
   * etc.) on a disjunction of related constraints provided on input
   * 
   * @param disjuncts
   * @return
   */
  // public EstimateResult analyzeSeveral(List<SymBool>... disjuncts) {
  // double result = 1;
  // for (List<SymBool> conjunct : disjuncts) {
  // double tmp = analyzeOne(conjunct);
  // result = result * tmp;
  // }
  // return result;
  // }

  public EstimateResult analyzeOne(List<SymBool> conjunct) {
    // Uncomment the following line to go back to the original implementation
    // with local partitioning for each variable
    // PartitionResult partitionResult = partition(conjunct);
    int size = partitionResult.getIndependentVarsClusters().size();
    EstimateResult[] results = new EstimateResult[size];
    List<Set<SymBool>> allProblems = new ArrayList<Set<SymBool>>();

    int i = 0;
    for (Set<String> cluster : partitionResult.getIndependentVarsClusters()) {
      // TODO what to do in this case with the variance?
      // if (estimate == 0) {
      // break;
      // }
      // HashMultimap<String, SymBool> locallyRelatedConstraints = HashMultimap
      // .<String, SymBool> create();
      // for (SymBool symBool : conjunct) {
      // SymLiteralSearcher vis = new SymLiteralSearcher();
      // symBool.accept(vis);
      // for (SymLiteral v : vis.getVars()) {
      // locallyRelatedConstraints.put(v.toString(), symBool);
      // }
      // }
      //
      // Set<SymBool> problem = new HashSet<SymBool>();
      // for (String var : cluster) {
      // problem.addAll(locallyRelatedConstraints.get(var));
      // }
      // // TODO this is inefficient, but jus to check the results
      // problem = Sets.intersection(problem, Sets.newHashSet(conjunct));
      Set<SymBool> problem = extractPartition(conjunct, cluster);
      if (problem.isEmpty()) { // none of the vars in cluster are in the
                               // conjunct
        continue;
      }

      EstimateResult cached = null;
      if (coral.counters.Main.mcDisableCache) {
        cached = processConstraint(problem);
      } else {
        long startGet = System.nanoTime();
        cached = getCached(problem);
        /*
         * long endGet = System.nanoTime(); cacheGets.add(endGet - startGet); if
         * (cached == null) { cacheMisses++; cached =
         * processConstraint(problem); // TODO: canonicalize (GREEN?) long
         * startPut = System.nanoTime(); cache.put(problem, cached); long endPut
         * = System.nanoTime(); cachePuts.add(endPut - startPut); } else {
         * cacheHits++; }
         */
        results[i] = cached;
        allProblems.add(problem);
        notifyPartitions(allProblems);
      }
      i++;
    }

    if (i != size) {
      EstimateResult[] tmp = new EstimateResult[i];
      System.arraycopy(results, 0, tmp, 0, i);
      results = tmp;
    }

    return new PCEstimateResult(results);
  }

  protected abstract void notifyPartitions(List<Set<SymBool>> allProblems);

  public abstract EstimateResult processConstraint(Set<SymBool> conjuncts);

  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    //
    // PartitionedCache mock = new PartitionedCache() {
    // Random r = new Random(0);
    // @Override
    // public double processConstraint(Set<SymBool> conjuncts) {
    // return r.nextDouble() * 10;
    // }
    // };
    //
    // // variables
    // SymDoubleLiteral x = new SymDoubleLiteral(0);
    // SymDoubleLiteral y = new SymDoubleLiteral(0);
    // SymDoubleLiteral z = new SymDoubleLiteral(0);
    // SymDoubleLiteral a = new SymDoubleLiteral(0);
    // SymDoubleLiteral b = new SymDoubleLiteral(0);
    //
    // // constraint 1
    // SymBool exp1 = Util.gt(Util.add(x, y), Util.ZEROD);
    // SymBool exp2 = Util.gt(Util.mod(y, z), Util.ZEROD);
    // SymBool exp3 = Util.gt(Util.mod(a, b), Util.ZEROD);
    // ArrayList<SymBool> pc1 = new ArrayList<SymBool>();
    // pc1.add(exp1);
    // pc1.add(exp2);
    // pc1.add(exp3);
    //
    // // constraint 2
    // SymBool exp4 = Util.gt(Util.mod(y, z), Util.ZEROD);
    // SymBool exp5 = Util.gt(Util.mod(a, b), Util.ZEROD);
    // ArrayList<SymBool> pc2 = new ArrayList<SymBool>();
    // pc2.add(exp4);
    // pc2.add(exp5);
    //
    // // computing volume
    //
    // double res = mock.analyzeSeveral(pc1, pc2);
    //
    // System.out.println(res);
    //
  }

  Map<List<SymBool>, Map<Set<String>, Set<SymBool>>> symbolicCache = new HashMap<List<SymBool>, Map<Set<String>, Set<SymBool>>>();

  int hit = 0;
  int miss = 0;

  protected Set<SymBool> extractPartition(List<SymBool> conjunct,
      Set<String> cluster) {
    Set<SymBool> problem;
    Map<Set<String>, Set<SymBool>> m1 = symbolicCache.get(conjunct);

    if (m1 == null) {
      m1 = new HashMap<Set<String>, Set<SymBool>>();
      symbolicCache.put(conjunct, m1);
      problem = null;
    } else {
      problem = m1.get(cluster);
    }

    if (problem == null) {
      miss++;
      HashMultimap<String, SymBool> locallyRelatedConstraints = HashMultimap
          .<String, SymBool> create();
      for (SymBool symBool : conjunct) {
        SymLiteralSearcher vis = new SymLiteralSearcher();
        symBool.accept(vis);
        for (SymLiteral v : vis.getVars()) {
          locallyRelatedConstraints.put(v.toString(), symBool);
        }
      }

      problem = new HashSet<SymBool>();
      for (String var : cluster) {
        problem.addAll(locallyRelatedConstraints.get(var));
      }
      problem = Sets.intersection(problem, Sets.newHashSet(conjunct));
      m1.put(cluster, problem);
    } else {
      hit++;
    }
    // System.out.println("hit: " + hit + " miss: " + miss);
    return problem;
  }

  public void printCacheReport() {
    System.out.println("[cached] cache hits  : " + cacheHits);
    System.out.println("[cached] cache misses: " + cacheMisses);
    System.out.println("[cached] access times: " + cacheGets.toString());
    System.out.println("[cached] write  times: " + cachePuts.toString());
  }

}