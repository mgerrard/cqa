package coral.simplifier;

import java.io.IOException;

import symlib.SymBool;
import symlib.Util;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.solvers.Env;
import coral.solvers.SolverKind;

public class Simplify {
  
  public static class Data {
    public Data(Env mapping, PC pc) {
      super();
      this.mapping = mapping;
      this.pc = pc;
    }
    public final Env mapping;
    public final PC pc;
  }

  private static final int MAX_ITERATIONS = 3;
  
  public static Data simplify(PC pc) {
    
    PC simplified = pc;
    Env map = null;
    
    for(int i = 0; i < MAX_ITERATIONS; i++) {
      simplified = Rewrite.rew(simplified);

      // dependent variables
      Object[] pair;
      try {
        pair = DP_Eq.removeSimpleEqualities(simplified);
        simplified = (PC) pair[0];
        Env sol = (Env) pair[1];
        map = Env.combineEnv(map, sol, SolverKind.RANDOM);
      } catch (IOException e) {
        System.out.println("Error in the rewrite...");
        e.printStackTrace();
        break;
      } catch (InterruptedException e) {
        System.out.println("Error in the rewrite...");
        e.printStackTrace();
        break;
      }
    }
    
    return new Data(map, simplified);
  }  
  
  public static void main(String[] args) throws ParseException, IOException, InterruptedException {
    String strPC = "BOR(BNOT(IEQ(IVAR(ID_1),ICONST(0))),IEQ(IVAR(ID_2),ICONST(0)));BOR(BNOT(IEQ(IVAR(ID_3),ICONST(0))),IEQ(IVAR(ID_4),ICONST(0)));BOR(BNOT(INE(IVAR(ID_5),ICONST(0))),IEQ(IVAR(ID_6),IVAR(ID_7)));BOR(BNOT(INE(IVAR(ID_8),ICONST(0))),IEQ(IVAR(ID_9),IVAR(ID_10)));BOR(BNOT(INE(IVAR(ID_11),ICONST(0))),IEQ(IVAR(ID_12),ICONST(-1)));BOR(BNOT(INE(IVAR(ID_13),ICONST(0))),IEQ(IVAR(ID_14),IVAR(ID_15)));BOR(BNOT(INE(IVAR(ID_16),ICONST(0))),IEQ(IVAR(ID_9),IVAR(ID_10)));BOR(BNOT(INE(IVAR(ID_17),ICONST(0))),IEQ(IVAR(ID_18),ICONST(0)));IEQ(IVAR(ID_19),IVAR(ID_20));IEQ(IVAR(ID_21),IVAR(ID_18));BOR(BAND(INE(IVAR(ID_22),ICONST(0)),INE(ICONST(0),IVAR(ID_23))),BAND(BNOT(INE(IVAR(ID_22),ICONST(0))),IEQ(ICONST(0),IVAR(ID_23))));IEQ(IVAR(ID_24),IVAR(ID_6));BOR(BAND(INE(IVAR(ID_25),ICONST(0)),INE(ICONST(0),IVAR(ID_5))),BAND(BNOT(INE(IVAR(ID_25),ICONST(0))),IEQ(ICONST(0),IVAR(ID_5))));IEQ(IVAR(ID_26),IVAR(ID_14));IEQ(IVAR(ID_27),IVAR(ID_28));IEQ(IVAR(ID_29),IVAR(ID_30));IEQ(IVAR(ID_15),ADD(IVAR(ID_31),ICONST(100)));IEQ(IVAR(ID_32),IVAR(ID_33));IEQ(IVAR(ID_34),IVAR(ID_35));IEQ(IVAR(ID_36),IVAR(ID_28));IEQ(IVAR(ID_37),IVAR(ID_38));IEQ(IVAR(ID_39),IVAR(ID_40));IEQ(IVAR(ID_41),IVAR(ID_28));BOR(BAND(INE(IVAR(ID_42),ICONST(0)),INE(ICONST(0),IVAR(ID_13))),BAND(BNOT(INE(IVAR(ID_42),ICONST(0))),IEQ(ICONST(0),IVAR(ID_13))));BOR(BAND(INE(IVAR(ID_43),ICONST(0)),INE(ICONST(0),IVAR(ID_11))),BAND(BNOT(INE(IVAR(ID_43),ICONST(0))),IEQ(ICONST(0),IVAR(ID_11))));IEQ(IVAR(ID_44),IVAR(ID_45));IEQ(IVAR(ID_31),IVAR(ID_45));BOR(BAND(IGT(IVAR(ID_24),IVAR(ID_41)),INE(ICONST(0),IVAR(ID_46))),BAND(BNOT(IGT(IVAR(ID_24),IVAR(ID_41))),IEQ(ICONST(0),IVAR(ID_46))));IEQ(IVAR(ID_47),IVAR(ID_48));BOR(BAND(INE(IVAR(ID_39),ICONST(0)),INE(ICONST(0),IVAR(ID_3))),BAND(BNOT(INE(IVAR(ID_39),ICONST(0))),IEQ(ICONST(0),IVAR(ID_3))));IEQ(IVAR(ID_49),IVAR(ID_32));IEQ(IVAR(ID_50),IVAR(ID_51));BOR(BAND(ILT(IVAR(ID_19),IVAR(ID_52)),INE(ICONST(0),IVAR(ID_53))),BAND(BNOT(ILT(IVAR(ID_19),IVAR(ID_52))),IEQ(ICONST(0),IVAR(ID_53))));BOR(BAND(IGE(IVAR(ID_54),ICONST(300)),INE(ICONST(0),IVAR(ID_55))),BAND(BNOT(IGE(IVAR(ID_54),ICONST(300))),IEQ(ICONST(0),IVAR(ID_55))));IEQ(IVAR(ID_20),IVAR(ID_56));IEQ(IVAR(ID_43),IVAR(ID_34));BOR(BAND(IGE(IVAR(ID_27),IVAR(ID_49)),INE(ICONST(0),IVAR(ID_57))),BAND(BNOT(IGE(IVAR(ID_27),IVAR(ID_49))),IEQ(ICONST(0),IVAR(ID_57))));BOR(BAND(INE(IVAR(ID_50),ICONST(0)),INE(ICONST(0),IVAR(ID_58))),BAND(BNOT(INE(IVAR(ID_50),ICONST(0))),IEQ(ICONST(0),IVAR(ID_58))));IEQ(IVAR(ID_28),IVAR(ID_59));BOR(BAND(INE(IVAR(ID_60),ICONST(0)),INE(ICONST(0),IVAR(ID_8))),BAND(BNOT(INE(IVAR(ID_60),ICONST(0))),IEQ(ICONST(0),IVAR(ID_8))));BOR(BAND(INE(IVAR(ID_61),ICONST(0)),INE(ICONST(0),IVAR(ID_1))),BAND(BNOT(INE(IVAR(ID_61),ICONST(0))),IEQ(ICONST(0),IVAR(ID_1))));BOR(BAND(IGT(IVAR(ID_26),IVAR(ID_36)),INE(ICONST(0),IVAR(ID_62))),BAND(BNOT(IGT(IVAR(ID_26),IVAR(ID_36))),IEQ(ICONST(0),IVAR(ID_62))));IEQ(IVAR(ID_33),ICONST(500));IEQ(IVAR(ID_22),IVAR(ID_9));BOR(BAND(INE(IVAR(ID_63),ICONST(0)),INE(ICONST(0),IVAR(ID_64))),BAND(BNOT(INE(IVAR(ID_63),ICONST(0))),IEQ(ICONST(0),IVAR(ID_64))));BOR(BAND(INE(IVAR(ID_65),ICONST(0)),INE(ICONST(0),IVAR(ID_66))),BAND(BNOT(INE(IVAR(ID_65),ICONST(0))),IEQ(ICONST(0),IVAR(ID_66))));IEQ(IVAR(ID_7),ADD(IVAR(ID_44),ICONST(100)));IEQ(IVAR(ID_54),IVAR(ID_29));BOR(BAND(INE(IVAR(ID_65),ICONST(0)),INE(ICONST(0),IVAR(ID_67))),BAND(BNOT(INE(IVAR(ID_65),ICONST(0))),IEQ(ICONST(0),IVAR(ID_67))));IEQ(IVAR(ID_52),IVAR(ID_47));IEQ(IVAR(ID_42),IVAR(ID_37));BOR(BAND(INE(IVAR(ID_68),ICONST(0)),INE(ICONST(0),IVAR(ID_69))),BAND(BNOT(INE(IVAR(ID_68),ICONST(0))),IEQ(ICONST(0),IVAR(ID_69))));IEQ(IVAR(ID_25),IVAR(ID_37));IEQ(IVAR(ID_45),IVAR(ID_70));BOR(BAND(INE(IVAR(ID_61),ICONST(0)),INE(ICONST(0),IVAR(ID_71))),BAND(BNOT(INE(IVAR(ID_61),ICONST(0))),IEQ(ICONST(0),IVAR(ID_71))));BOR(BAND(INE(IVAR(ID_72),ICONST(0)),INE(ICONST(0),IVAR(ID_73))),BAND(BNOT(INE(IVAR(ID_72),ICONST(0))),IEQ(ICONST(0),IVAR(ID_73))));IEQ(IVAR(ID_74),IVAR(ID_28));BOR(BAND(IEQ(ICONST(0),IVAR(ID_75)),INE(ICONST(0),IVAR(ID_76))),BAND(INE(ICONST(0),IVAR(ID_75)),IEQ(ICONST(0),IVAR(ID_76))));BOR(BAND(IGE(IVAR(ID_74),IVAR(ID_77)),INE(ICONST(0),IVAR(ID_75))),BAND(BNOT(IGE(IVAR(ID_74),IVAR(ID_77))),IEQ(ICONST(0),IVAR(ID_75))));BOR(BAND(INE(IVAR(ID_78),ICONST(0)),INE(ICONST(0),IVAR(ID_79))),BAND(BNOT(INE(IVAR(ID_78),ICONST(0))),IEQ(ICONST(0),IVAR(ID_79))));IEQ(IVAR(ID_77),IVAR(ID_80));IEQ(IVAR(ID_80),IVAR(ID_33));IEQ(IVAR(ID_81),IVAR(ID_20));IEQ(IVAR(ID_82),IVAR(ID_20));BOR(BAND(ILT(IVAR(ID_83),IVAR(ID_82)),INE(ICONST(0),IVAR(ID_84))),BAND(BNOT(ILT(IVAR(ID_83),IVAR(ID_82))),IEQ(ICONST(0),IVAR(ID_84))));IEQ(IVAR(ID_78),IVAR(ID_85));BOR(BAND(ILT(IVAR(ID_86),IVAR(ID_81)),INE(ICONST(0),IVAR(ID_87))),BAND(BNOT(ILT(IVAR(ID_86),IVAR(ID_81))),IEQ(ICONST(0),IVAR(ID_87))));IEQ(IVAR(ID_83),IVAR(ID_47));IEQ(IVAR(ID_86),IVAR(ID_47));IEQ(IVAR(ID_72),IVAR(ID_88));IEQ(IVAR(ID_89),IVAR(ID_47));IEQ(IVAR(ID_90),IVAR(ID_20));BOR(BAND(ILT(IVAR(ID_91),IVAR(ID_92)),INE(ICONST(0),IVAR(ID_93))),BAND(BNOT(ILT(IVAR(ID_91),IVAR(ID_92))),IEQ(ICONST(0),IVAR(ID_93))));IEQ(IVAR(ID_94),IVAR(ID_47));BOR(BAND(INE(IVAR(ID_95),ICONST(0)),INE(ICONST(0),IVAR(ID_16))),BAND(BNOT(INE(IVAR(ID_95),ICONST(0))),IEQ(ICONST(0),IVAR(ID_16))));BOR(BAND(INE(IVAR(ID_96),ICONST(0)),INE(ICONST(0),IVAR(ID_97))),BAND(BNOT(INE(IVAR(ID_96),ICONST(0))),IEQ(ICONST(0),IVAR(ID_97))));IEQ(IVAR(ID_98),IVAR(ID_99));BOR(BAND(ILT(IVAR(ID_94),IVAR(ID_100)),INE(ICONST(0),IVAR(ID_101))),BAND(BNOT(ILT(IVAR(ID_94),IVAR(ID_100))),IEQ(ICONST(0),IVAR(ID_101))));IEQ(IVAR(ID_96),IVAR(ID_102));IEQ(IVAR(ID_92),IVAR(ID_20));IEQ(IVAR(ID_100),IVAR(ID_20));BOR(BAND(ILT(IVAR(ID_90),IVAR(ID_89)),INE(ICONST(0),IVAR(ID_103))),BAND(BNOT(ILT(IVAR(ID_90),IVAR(ID_89))),IEQ(ICONST(0),IVAR(ID_103))));IEQ(IVAR(ID_95),IVAR(ID_104));BOR(BAND(INE(IVAR(ID_98),ICONST(0)),INE(ICONST(0),IVAR(ID_105))),BAND(BNOT(INE(IVAR(ID_98),ICONST(0))),IEQ(ICONST(0),IVAR(ID_105))));IEQ(IVAR(ID_91),IVAR(ID_47));IEQ(IVAR(ID_106),IVAR(ID_107));BOR(BAND(INE(IVAR(ID_108),ICONST(0)),INE(ICONST(0),IVAR(ID_17))),BAND(BNOT(INE(IVAR(ID_108),ICONST(0))),IEQ(ICONST(0),IVAR(ID_17))));IEQ(IVAR(ID_109),IVAR(ID_106));BOR(BAND(IEQ(IVAR(ID_109),ICONST(1)),INE(ICONST(0),IVAR(ID_110))),BAND(BNOT(IEQ(IVAR(ID_109),ICONST(1))),IEQ(ICONST(0),IVAR(ID_110))));IEQ(IVAR(ID_29),ICONST(911));IEQ(IVAR(ID_34),ICONST(1));IEQ(IVAR(ID_47),ICONST(4194));IEQ(IVAR(ID_20),ICONST(4667));IEQ(IVAR(ID_45),ICONST(401));IEQ(IVAR(ID_28),ICONST(399));IEQ(IVAR(ID_37),ICONST(1));IEQ(IVAR(ID_21),ICONST(0));IEQ(IVAR(ID_68),IVAR(ID_12));IEQ(IVAR(ID_63),IVAR(ID_46));IEQ(IVAR(ID_40),IVAR(ID_53));IEQ(IVAR(ID_111),IVAR(ID_4));BOR(BNOT(IEQ(IVAR(ID_64),ICONST(0))),IEQ(IVAR(ID_51),IVAR(ID_111)));BOR(BNOT(IEQ(IVAR(ID_58),ICONST(0))),IEQ(IVAR(ID_112),ICONST(0)));IEQ(IVAR(ID_65),IVAR(ID_112));IEQ(IVAR(ID_60),IVAR(ID_62));BOR(BNOT(INE(IVAR(ID_55),ICONST(0))),IEQ(IVAR(ID_113),IVAR(ID_57)));IEQ(IVAR(ID_10),IVAR(ID_113));BOR(BNOT(IEQ(IVAR(ID_23),ICONST(0))),IEQ(IVAR(ID_114),ICONST(0)));IEQ(IVAR(ID_61),IVAR(ID_114));BOR(BNOT(IEQ(IVAR(ID_66),ICONST(0))),IEQ(IVAR(ID_115),IVAR(ID_2)));BOR(BNOT(IEQ(IVAR(ID_67),ICONST(0))),IEQ(IVAR(ID_116),IVAR(ID_115)));BOR(BNOT(INE(IVAR(ID_69),ICONST(0))),IEQ(IVAR(ID_18),IVAR(ID_116)));IEQ(IVAR(ID_102),IVAR(ID_93));BOR(BNOT(IEQ(IVAR(ID_97),ICONST(0))),IEQ(IVAR(ID_117),ICONST(-1)));IEQ(IVAR(ID_118),IVAR(ID_117));IEQ(IVAR(ID_88),IVAR(ID_87));BOR(BNOT(INE(IVAR(ID_58),ICONST(0))),IEQ(IVAR(ID_112),IVAR(ID_73)));IEQ(IVAR(ID_104),IVAR(ID_101));BOR(BNOT(IEQ(IVAR(ID_55),ICONST(0))),IEQ(IVAR(ID_113),ICONST(0)));IEQ(IVAR(ID_99),IVAR(ID_103));BOR(BNOT(INE(IVAR(ID_23),ICONST(0))),IEQ(IVAR(ID_114),IVAR(ID_105)));BOR(BNOT(INE(IVAR(ID_66),ICONST(0))),IEQ(IVAR(ID_115),ICONST(1)));BOR(BNOT(IEQ(IVAR(ID_71),ICONST(0))),IEQ(IVAR(ID_116),IVAR(ID_115)));IEQ(IVAR(ID_85),IVAR(ID_84));BOR(BNOT(IEQ(IVAR(ID_79),ICONST(0))),IEQ(IVAR(ID_119),ICONST(0)));IEQ(IVAR(ID_106),ICONST(1));IEQ(IVAR(ID_108),IVAR(ID_110));BOR(BNOT(INE(IVAR(ID_71),ICONST(0))),IEQ(IVAR(ID_116),ICONST(0)));BOR(BNOT(INE(IVAR(ID_79),ICONST(0))),IEQ(IVAR(ID_119),IVAR(ID_76)));BOR(BNOT(INE(IVAR(ID_97),ICONST(0))),IEQ(IVAR(ID_117),IVAR(ID_119)));BOR(BNOT(INE(IVAR(ID_64),ICONST(0))),IEQ(IVAR(ID_51),IVAR(ID_118)))";
    PC newPC = (new Parser(strPC)).parsePC();

    // original
    System.out.print("ORIGINAL:\n" + newPC.toCompareStrings());
    System.out.println("# clauses = "+ newPC.size());
    
    // first rewrite
    newPC = Rewrite.rew(newPC);
    System.out.printf("AFTER REWRITE:\n" + newPC.toCompareStrings());
    System.out.println("# clauses = "+ newPC.size());    

    // dependent variables
    Object[] pair = DP_Eq.removeSimpleEqualities(newPC);
    newPC = (PC) pair[0];
    Env sol = (Env) pair[1];
    System.out.print("AFTER VAR REMOVAL:\n"  + newPC.toCompareStrings());
    System.out.println(sol);
    System.out.println("# clauses = "+ newPC.size());
    
    // rewrite
    newPC = Rewrite.rew(newPC);
    System.out.print("ANOTHER REWRITE:\n" + newPC.toCompareStrings());
    System.out.println("# clauses = "+ newPC.size());    
    
    // dependent variables
    pair = DP_Eq.removeSimpleEqualities(newPC);
    newPC = (PC) pair[0];
    sol = (Env) pair[1];
    System.out.print("AFTER VAR REMOVAL:\n"  + newPC.toCompareStrings());
    System.out.println(sol);
    System.out.println("# clauses = "+ newPC.size());
    
    // rewrite
    newPC = Rewrite.rew(newPC);
    System.out.print("ANOTHER REWRITE:\n" + newPC.toCompareStrings());
    System.out.println("# clauses = "+ newPC.size());    
    
    // dependent variables
    pair = DP_Eq.removeSimpleEqualities(newPC);
    newPC = (PC) pair[0];
    sol = (Env) pair[1];
    System.out.print("AFTER VAR REMOVAL:\n"  + newPC.toCompareStrings());
    System.out.println(sol);
    System.out.println("# clauses = "+ newPC.size());
    
    // rewrite
    newPC = Rewrite.rew(newPC);
    System.out.print("ANOTHER REWRITE:\n" + newPC.toCompareStrings());
    System.out.println("# clauses = "+ newPC.size());
    
    // dependent variables
    pair = DP_Eq.removeSimpleEqualities(newPC);
    newPC = (PC) pair[0];
    sol = (Env) pair[1];
    System.out.print("AFTER VAR REMOVAL:\n"  + newPC.toCompareStrings());
    System.out.println(sol);
    
    int count = 0;
    for (SymBool clause : newPC.getConstraints()) {
      if (clause == Util.TRUE) {
          count++;
      }
    }
    
    System.out.printf("# clauses = %d ...of which %d have been eliminated", newPC.size(), count);
  }
}
