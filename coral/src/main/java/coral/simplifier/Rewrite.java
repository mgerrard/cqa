package coral.simplifier;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import symlib.SymBinaryExpression;
import symlib.SymBool;
import symlib.SymBoolOperations;
import symlib.SymDouble;
import symlib.SymDoubleConstant;
import symlib.SymDoubleRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import symlib.Util;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.util.visitors.SymLiteralSearcher;
import coral.util.visitors.adaptors.TypedVisitorAdaptor;

public class Rewrite extends TypedVisitorAdaptor {

  
  /** visit SymDoubleRelational **/
  @Override
  protected SymBool visitSymDoubleRelational(SymDoubleRelational bool) {
    int op = bool.getOp();
    SymDouble a = bool.getA();
    SymDouble b = bool.getB();
    SymDouble a_ , b_;
    a_ = b_ = null;
    if (op == SymDoubleRelational.EQ) {
      
      //  REW 1: POW(E,E1) == POW(E,E2)   ==> E1 = E2
      //  REW 2: POW(E1,E) == POW(E2,E)   ==> E1 = E2      <<LOSSY>>
      //  REW 3: f(E1) == f(E2)           ==> E1 = E2      <<LOSSY>>
      //  REW 4: LOG(E) == c               ==> E = POW(2,c)
      //  REW 5: LOG_10(E) == c            ==> E = POW(10,c)
      //  REW 6: VAR1 [+,-,*,/] VAR2 == E  ==> VAR1 = E [-,+,/,*] VAR2
      //  REW 7: VAR1 + C * VAR1 = _       ==> (1+C) * VAR1 = _  
      //  symmetric: 4, 5, and 6
      
      if (a instanceof SymMathBinary &&
          b instanceof SymMathBinary &&
          ((SymMathBinary)a).getOp()==SymMathBinary.POW &&
          ((SymMathBinary)a).getOp()==((SymMathBinary)b).getOp() &&
          ((SymMathBinary)a).getArg1().equals(((SymMathBinary)b).getArg1())) {
        a_ = (SymDouble) ((SymMathBinary)a).getArg2().accept(this);
        b_ = (SymDouble) ((SymMathBinary)b).getArg2().accept(this);
      } 
      else if (a instanceof SymMathBinary &&
          b instanceof SymMathBinary &&          
          ((SymMathBinary)a).getOp()==SymMathBinary.POW &&
          ((SymMathBinary)a).getOp()==((SymMathBinary)b).getOp() &&
          ((SymMathBinary)a).getArg2().equals(((SymMathBinary)b).getArg2())) {
        a_ = (SymDouble) ((SymMathBinary)a).getArg2().accept(this);
        b_ = (SymDouble) ((SymMathBinary)b).getArg2().accept(this);
      } 
      else if (a instanceof SymMathUnary &&
          b instanceof SymMathUnary &&
          ((SymMathUnary)a).getOp()==((SymMathUnary)b).getOp()) {
        
        a_ = (SymDouble) ((SymMathUnary)a).getArg().accept(this);
        b_ = (SymDouble) ((SymMathUnary)b).getArg().accept(this);
        
      }
      else if (a instanceof SymMathUnary &&
          b instanceof SymDoubleConstant &&
          ((SymMathUnary)a).getOp()==SymMathUnary.LOG) {
        SymDouble[] result = rew_logE_cte(a, b);        
        a_ = result[0];
        b_ = result[1];
      }
      else if (b instanceof SymMathUnary &&
          a instanceof SymDoubleConstant &&
          ((SymMathUnary)b).getOp()==SymMathUnary.LOG) {
        
        SymDouble[] result = rew_logE_cte(b, a);        
        a_ = result[0];
        b_ = result[1];
      } 
      else if (a instanceof SymMathUnary &&
          b instanceof SymDoubleConstant &&
          ((SymMathUnary)a).getOp()==SymMathUnary.LOG10) {
        
        SymDouble[] result = rew_log10_cte(a, b);        
        a_ = result[0];
        b_ = result[1];
        
      }
      else if (b instanceof SymMathUnary &&
          a instanceof SymDoubleConstant &&
          ((SymMathUnary)b).getOp()==SymMathUnary.LOG10) {
        
        SymDouble[] result = rew_log10_cte(b, a);        
        a_ = result[0];
        b_ = result[1];
        
      }
      else if (a instanceof SymBinaryExpression &&
          ((SymBinaryExpression)a).getA() instanceof SymLiteral/* &&
          ((SymBinaryExpression)a).getB() instanceof SymLiteral*/) {
        
        SymDouble[] result = rew_eq_arith(a, b);
        a_ = result[0];
        b_ = result[1];

      }
      else if (b instanceof SymBinaryExpression &&
          ((SymBinaryExpression)b).getA() instanceof SymLiteral /*&&
          ((SymBinaryExpression)b).getB() instanceof SymLiteral*/) {
        
        SymDouble[] result = rew_eq_arith(b, a);
        a_ = result[0];
        b_ = result[1];
        
      }
      else if (a instanceof SymBinaryExpression &&
          ((SymBinaryExpression)a).getOp() == 0 /*ADD*/) {        
        if (((SymBinaryExpression)a).getA() instanceof SymLiteral) {
          SymLiteral lit1 = (SymLiteral)((SymBinaryExpression)a).getA();
          SymNumber num = ((SymBinaryExpression)a).getB();
          if (num instanceof SymBinaryExpression) {
            SymBinaryExpression bNum = (SymBinaryExpression)num;
            if (bNum.getOp() == 2 /*MULT*/) {
              if ((bNum).getB() instanceof SymLiteral) {
                SymLiteral lit2 = (SymLiteral)(bNum).getB();
                if (lit1 == lit2 && bNum.getA() instanceof SymDoubleConstant) {
                  // VAR1 + C * VAR1 = _ ==> (1+C) * VAR1 = _
                  double c = 1.0 + ((SymDoubleConstant)bNum.getA()).eval();
                  a_ = (SymDouble)lit1;
                  b_ = Util.div(b, Util.createConstant(c));
                }
              }
            }
          }
        }
      }
    
    }
    
    if (a_ == null){
      a_  = (SymDouble) a.accept(this);
      b_ = (SymDouble) b.accept(this);    
    }
    
    return SymDoubleRelational.create(a_, b_, op);
    
  }

  private SymDouble[] rew_log10_cte(SymDouble a, SymDouble b) {
    SymDouble[] result = new SymDouble[2];
    result[0] = (SymDouble) ((SymMathUnary)a).getArg().accept(this);
    double val = Math.pow(10,((SymDoubleConstant)b).evalNumber().doubleValue());
    result[1] = Util.createConstant(val);
    return result;
  }

  @SuppressWarnings("unused")
  private SymDouble[] rew_log2_cte(SymDouble a, SymDouble b) {
    SymDouble[] result = new SymDouble[2];
    result[0] = (SymDouble) ((SymMathUnary)a).getArg().accept(this);
    double val = Math.pow(2,((SymDoubleConstant)b).evalNumber().doubleValue());
    result[1] = Util.createConstant(val);
    return result;
  }

  private SymDouble[] rew_logE_cte(SymDouble a, SymDouble b) {
    SymDouble[] result = new SymDouble[2];
    result[0] = (SymDouble) ((SymMathUnary)a).getArg().accept(this);
    double val = Math.pow(Math.E,((SymDoubleConstant)b).evalNumber().doubleValue());
    result[1] = Util.createConstant(val);
    return result;
  }

  @Before
  public void setUp() {
    Util.resetID();
  }

  @Test
  public void testRewEqArith() {
    SymDouble lit1 = Util.createSymLiteral(0.0);
    SymDouble lit2 = Util.createSymLiteral(0.0);
    SymDouble a = Util.add(lit1, lit2);
    SymDouble b = Util.pow(lit1, Util.createConstant(2.0));
    
    SymDouble[] c = (new Rewrite()).rew_eq_arith(a, b);
    Assert.assertEquals(c[1].toString(),"(pow_($V1,2.0)-$V1)");
  }
  
  @Test
  public void testRewEqArith2() {
    SymDouble lit1 = Util.createSymLiteral(0.0);
    SymDouble lit2 = Util.createSymLiteral(0.0);
    SymDouble a = Util.add(lit1, lit2);
    SymDouble b = Util.pow(lit2, Util.createConstant(2.0));
    
    SymDouble[] c = (new Rewrite()).rew_eq_arith(a, b);
    String rew = c[1].toString();
    @SuppressWarnings("unused") // This is likely a bug, but it is testing code
    String lit1Id = lit1.toString();
    String lit2Id = lit2.toString();
    Assert.assertTrue("Rewrited constraint: " + rew ,rew.equals("(pow_("+lit2Id+",2.0)-"+lit2Id+")"));
  }

  private SymDouble[] rew_eq_arith(SymDouble e1, SymDouble e2) {
    SymDouble result1;
    SymDouble result2;

    //e1 = e2  =>  lit1 `op` EX1 = e2 
    SymLiteral lit1 = (SymLiteral) ((SymBinaryExpression)e1).getA();
    SymLiteralSearcher litSearcher = new SymLiteralSearcher();
    litSearcher.visitSymDouble(e2);
    boolean containsLit1 = litSearcher.getDoubleVars().contains(lit1);

    SymDouble ex2 = (SymDouble) e2.accept(this); //rewrite e2 
    SymBinaryExpression res = (SymBinaryExpression) e1.clone();
    SymDouble ex1 = (SymDouble) res.getB().accept(this); //rewrite right side of e1
    
    if (containsLit1) { // isolate EX1 (right side of e1)
      result1 = ex1;
      
      //create right side of the expression
      int op = res.getOp();
      if (op == 1) { /*SUB*/
          res.setA((SymDouble)lit1);
          res.setB(ex2);
      } else {
        if(op == 0) /*ADD*/
          res.setOp(1);
        else if(op == 2) /*MUL*/
          res.setOp(3);
        else if(op == 3) /*DIV*/
          res.setOp(2);
        else
          throw new RuntimeException("Something wrong here!");
        res.setA(ex2);
        res.setB((SymDouble) lit1);
      }
    } else { // isolate lit1
      result1 = (SymDouble) lit1;
      
      //create right side of the expression
      switch (res.getOp()) {
        case 0 : /*ADD*/
          res.setOp(1);
          break;
        case 1 : /*SUB*/
          res.setOp(0);
          break;
        case 2 : /*MULT*/
          res.setOp(3);
          break;
        case 3 : /*DIV*/
          res.setOp(2);
          break;
      }
      res.setA(ex2);
      res.setB(ex1);
    }
    result2 = (SymDouble) res;
    
    return new SymDouble[]{result1, result2};
  }
  
  /** visit SymDoubleRelational **/
  @Override
  protected SymBool visitSymBoolOperations(SymBoolOperations bool) {
    SymBoolOperations tmp = (SymBoolOperations) super.visitSymBoolOperations(bool);
    SymBool result = tmp;
    if (tmp.getOp()==SymBoolOperations.OR) {
      result = Util.neg(tmp.neg());
    } else if (tmp.getOp()==SymBoolOperations.AND) {
      if (tmp.getA()==Util.TRUE) {
        result = tmp.getB();
      } else if (tmp.getB()==Util.TRUE) {
        result = tmp.getA();
      } else if (tmp.getA()==Util.FALSE) {
        result = Util.FALSE;
      } else if (tmp.getB()==Util.FALSE) {
        result = Util.FALSE;
      }
    } else if (tmp.getOp()==SymBoolOperations.NEG) {
      result = tmp.getA().neg();
    }
    return result;
  }
  
  
  /** visit SymIntRelational **/
  @Override
  protected SymBool visitSymIntRelational(SymIntRelational bool) {
    SymIntRelational tmp = (SymIntRelational) super.visitSymIntRelational(bool);
    SymInt x = tmp.getA();
    SymInt y = tmp.getB();
    SymBool result = tmp;
    if (x instanceof SymIntConstant && y instanceof SymIntConstant) {
      result = tmp.evalBool() ? Util.TRUE : Util.FALSE;
    } else if (tmp.getOp()==SymIntRelational.NE && x == y) {
      result = Util.FALSE;
    } else if (tmp.getOp()==SymIntRelational.EQ) {
      if (x == y) {
        result = Util.TRUE;
      } else if (x instanceof SymIntArith && y instanceof SymIntArith) {
        SymIntArith x_ = (SymIntArith) x;
        SymIntArith y_ = (SymIntArith) y;
        if (x_.getA() instanceof SymIntLiteral && x_.getB() instanceof SymIntConstant &&
            y_.getA() instanceof SymIntConstant && y_.getB() instanceof SymIntConstant) {
          // a + C1 = C2 + C3  =>  a = C2 + C3 - C1
          if (x_.getOp() == SymIntArith.ADD && y_.getOp() == SymIntArith.ADD) {
            int num = 
                y_.getA().evalNumber().intValue()+
                y_.getB().evalNumber().intValue()-
                x_.getB().evalNumber().intValue();
            result = Util.eq(x_.getA(), Util.createConstant(num));
          }
        }
      }
      // TODO: please write other cases...
    } 
    return result;
  }
  
  public static PC rew(PC pc) {
    PC result = new PC();
    SymBool last = null;
    List<SymBool> constraints = new ArrayList<SymBool>();
    for (SymBool sb : pc.getConstraints()) {
      SymBool curr = (new Rewrite()).visitSymBool(sb);
      // result.addConstraint(curr);      
      if (last != null) {
        SymBool tmp = checkLessGreaterEqual(last, curr);
        if (tmp != null) {
          constraints.remove(constraints.size()-1);
          curr = tmp;
        }
      }
      constraints.add(curr);
      last = curr;
    }
    result.setConstraints(constraints);
    return result;
  }
  
  private static SymBool checkLessGreaterEqual(SymBool last, SymBool curr) {
    SymBool result = null;
    if (last instanceof SymIntRelational && curr instanceof SymIntRelational) {
      SymIntRelational lastR = (SymIntRelational)last;
      SymIntRelational currR = (SymIntRelational)curr;
      if ((lastR.getOp() == SymIntRelational.GE && 
          currR.getOp() == SymIntRelational.LE) ||
          (lastR.getOp() == SymIntRelational.LE && 
          currR.getOp() == SymIntRelational.GE)) {
        // a >= b , c <= d <=> a == b iff a.equals(c) && b.equals(d)
        // a <= b , c >= d <=> a == b iff a.equals(c) && b.equals(d)
        if (lastR.getA().equals(currR.getA()) && 
            lastR.getB().equals(currR.getB())) {
          result = Util.eq(lastR.getA(), lastR.getB());
        }
      }
    } else if (last instanceof SymDoubleRelational && curr instanceof SymDoubleRelational) {
      SymDoubleRelational lastR = (SymDoubleRelational)last;
      SymDoubleRelational currR = (SymDoubleRelational)curr;
      if ((lastR.getOp() == SymIntRelational.GE && 
          currR.getOp() == SymIntRelational.LE) ||
          (lastR.getOp() == SymIntRelational.LE && 
          currR.getOp() == SymIntRelational.GE)) {
        // a >= b , c <= d <=> a == b iff a.equals(c) && b.equals(d)
        // a <= b , c >= d <=> a == b iff a.equals(c) && b.equals(d)
        if (lastR.getA().equals(currR.getA()) && 
            lastR.getB().equals(currR.getB())) {
          result = Util.eq(lastR.getA(), lastR.getB());
        }
      }
    }
    return result;
  }

  public static void main(String[] args) throws ParseException {
    
//    String strPC = "DEQ(LOG_(DVAR(ID_1)),DCONST(10.0))";
//    String strPC = "DEQ(EXP_(DVAR(ID_1)),EXP_(ADD(DVAR(ID_1),DVAR(ID_2))))";
//    String strPC = "DEQ(POW_(DVAR(ID_1),DVAR(ID_2)),POW_(DVAR(ID_1),DVAR(ID_1)))";
//    String strPC = "DEQ(MUL(DVAR(ID_1),DVAR(ID_2)),DCONST(2.0))";
//    String strPC = "DEQ(POW_(DVAR(ID_2),DVAR(ID_1)),POW_(DVAR(ID_1),DVAR(ID_1)))";
//    String strPC = "DEQ(POW_(DVAR(ID_1),DCONST(2)),ADD(DVAR(ID_1),DVAR(ID_2)))";
    
//    String strPC = "DGE(DVAR(ID_1),DCONST(1));DLE(DVAR(ID_1),DCONST(1))";
//    String strPC = "IGE(IVAR(ID_1),ICONST(1));ILE(IVAR(ID_1),ICONST(1))";
//    String strPC = "DEQ(ADD(DVAR(ID_20),MUL(DCONST(0.1),DVAR(ID_20))),DIV(ADD(ADD(DVAR(ID_21),DVAR(ID_22)),DVAR(ID_23)),DCONST(3.0)))";
//    String strPC = "BOR(DGE(DVAR(ID_1),DCONST(1)),DLE(DVAR(ID_1),DCONST(1)))";
    
    
//    String strPC = "BOR(BNOT(IEQ(IVAR(ID_1),ICONST(0))),IEQ(IVAR(ID_2),ICONST(0)));BOR(BNOT(IEQ(IVAR(ID_3),ICONST(0))),IEQ(IVAR(ID_4),ICONST(0)));BOR(BNOT(INE(IVAR(ID_5),ICONST(0))),IEQ(IVAR(ID_6),IVAR(ID_7)));BOR(BNOT(INE(IVAR(ID_8),ICONST(0))),IEQ(IVAR(ID_9),IVAR(ID_10)));BOR(BNOT(INE(IVAR(ID_11),ICONST(0))),IEQ(IVAR(ID_12),ICONST(-1)));BOR(BNOT(INE(IVAR(ID_13),ICONST(0))),IEQ(IVAR(ID_14),IVAR(ID_15)));BOR(BNOT(INE(IVAR(ID_16),ICONST(0))),IEQ(IVAR(ID_9),IVAR(ID_10)));BOR(BNOT(INE(IVAR(ID_17),ICONST(0))),IEQ(IVAR(ID_18),ICONST(0)));IEQ(IVAR(ID_19),IVAR(ID_20));IEQ(IVAR(ID_21),IVAR(ID_18));BOR(BAND(INE(IVAR(ID_22),ICONST(0)),INE(ICONST(0),IVAR(ID_23))),BAND(BNOT(INE(IVAR(ID_22),ICONST(0))),IEQ(ICONST(0),IVAR(ID_23))));IEQ(IVAR(ID_24),IVAR(ID_6));BOR(BAND(INE(IVAR(ID_25),ICONST(0)),INE(ICONST(0),IVAR(ID_5))),BAND(BNOT(INE(IVAR(ID_25),ICONST(0))),IEQ(ICONST(0),IVAR(ID_5))));IEQ(IVAR(ID_26),IVAR(ID_14));IEQ(IVAR(ID_27),IVAR(ID_28));IEQ(IVAR(ID_29),IVAR(ID_30));IEQ(IVAR(ID_15),ADD(IVAR(ID_31),ICONST(100)));IEQ(IVAR(ID_32),IVAR(ID_33));IEQ(IVAR(ID_34),IVAR(ID_35));IEQ(IVAR(ID_36),IVAR(ID_28));IEQ(IVAR(ID_37),IVAR(ID_38));IEQ(IVAR(ID_39),IVAR(ID_40));IEQ(IVAR(ID_41),IVAR(ID_28));BOR(BAND(INE(IVAR(ID_42),ICONST(0)),INE(ICONST(0),IVAR(ID_13))),BAND(BNOT(INE(IVAR(ID_42),ICONST(0))),IEQ(ICONST(0),IVAR(ID_13))));BOR(BAND(INE(IVAR(ID_43),ICONST(0)),INE(ICONST(0),IVAR(ID_11))),BAND(BNOT(INE(IVAR(ID_43),ICONST(0))),IEQ(ICONST(0),IVAR(ID_11))));IEQ(IVAR(ID_44),IVAR(ID_45));IEQ(IVAR(ID_31),IVAR(ID_45));BOR(BAND(IGT(IVAR(ID_24),IVAR(ID_41)),INE(ICONST(0),IVAR(ID_46))),BAND(BNOT(IGT(IVAR(ID_24),IVAR(ID_41))),IEQ(ICONST(0),IVAR(ID_46))));IEQ(IVAR(ID_47),IVAR(ID_48));BOR(BAND(INE(IVAR(ID_39),ICONST(0)),INE(ICONST(0),IVAR(ID_3))),BAND(BNOT(INE(IVAR(ID_39),ICONST(0))),IEQ(ICONST(0),IVAR(ID_3))));IEQ(IVAR(ID_49),IVAR(ID_32));IEQ(IVAR(ID_50),IVAR(ID_51));BOR(BAND(ILT(IVAR(ID_19),IVAR(ID_52)),INE(ICONST(0),IVAR(ID_53))),BAND(BNOT(ILT(IVAR(ID_19),IVAR(ID_52))),IEQ(ICONST(0),IVAR(ID_53))));BOR(BAND(IGE(IVAR(ID_54),ICONST(300)),INE(ICONST(0),IVAR(ID_55))),BAND(BNOT(IGE(IVAR(ID_54),ICONST(300))),IEQ(ICONST(0),IVAR(ID_55))));IEQ(IVAR(ID_20),IVAR(ID_56));IEQ(IVAR(ID_43),IVAR(ID_34));BOR(BAND(IGE(IVAR(ID_27),IVAR(ID_49)),INE(ICONST(0),IVAR(ID_57))),BAND(BNOT(IGE(IVAR(ID_27),IVAR(ID_49))),IEQ(ICONST(0),IVAR(ID_57))));BOR(BAND(INE(IVAR(ID_50),ICONST(0)),INE(ICONST(0),IVAR(ID_58))),BAND(BNOT(INE(IVAR(ID_50),ICONST(0))),IEQ(ICONST(0),IVAR(ID_58))));IEQ(IVAR(ID_28),IVAR(ID_59));BOR(BAND(INE(IVAR(ID_60),ICONST(0)),INE(ICONST(0),IVAR(ID_8))),BAND(BNOT(INE(IVAR(ID_60),ICONST(0))),IEQ(ICONST(0),IVAR(ID_8))));BOR(BAND(INE(IVAR(ID_61),ICONST(0)),INE(ICONST(0),IVAR(ID_1))),BAND(BNOT(INE(IVAR(ID_61),ICONST(0))),IEQ(ICONST(0),IVAR(ID_1))));BOR(BAND(IGT(IVAR(ID_26),IVAR(ID_36)),INE(ICONST(0),IVAR(ID_62))),BAND(BNOT(IGT(IVAR(ID_26),IVAR(ID_36))),IEQ(ICONST(0),IVAR(ID_62))));IEQ(IVAR(ID_33),ICONST(500));IEQ(IVAR(ID_22),IVAR(ID_9));BOR(BAND(INE(IVAR(ID_63),ICONST(0)),INE(ICONST(0),IVAR(ID_64))),BAND(BNOT(INE(IVAR(ID_63),ICONST(0))),IEQ(ICONST(0),IVAR(ID_64))));BOR(BAND(INE(IVAR(ID_65),ICONST(0)),INE(ICONST(0),IVAR(ID_66))),BAND(BNOT(INE(IVAR(ID_65),ICONST(0))),IEQ(ICONST(0),IVAR(ID_66))));IEQ(IVAR(ID_7),ADD(IVAR(ID_44),ICONST(100)));IEQ(IVAR(ID_54),IVAR(ID_29));BOR(BAND(INE(IVAR(ID_65),ICONST(0)),INE(ICONST(0),IVAR(ID_67))),BAND(BNOT(INE(IVAR(ID_65),ICONST(0))),IEQ(ICONST(0),IVAR(ID_67))));IEQ(IVAR(ID_52),IVAR(ID_47));IEQ(IVAR(ID_42),IVAR(ID_37));BOR(BAND(INE(IVAR(ID_68),ICONST(0)),INE(ICONST(0),IVAR(ID_69))),BAND(BNOT(INE(IVAR(ID_68),ICONST(0))),IEQ(ICONST(0),IVAR(ID_69))));IEQ(IVAR(ID_25),IVAR(ID_37));IEQ(IVAR(ID_45),IVAR(ID_70));BOR(BAND(INE(IVAR(ID_61),ICONST(0)),INE(ICONST(0),IVAR(ID_71))),BAND(BNOT(INE(IVAR(ID_61),ICONST(0))),IEQ(ICONST(0),IVAR(ID_71))));BOR(BAND(INE(IVAR(ID_72),ICONST(0)),INE(ICONST(0),IVAR(ID_73))),BAND(BNOT(INE(IVAR(ID_72),ICONST(0))),IEQ(ICONST(0),IVAR(ID_73))));IEQ(IVAR(ID_74),IVAR(ID_28));BOR(BAND(IEQ(ICONST(0),IVAR(ID_75)),INE(ICONST(0),IVAR(ID_76))),BAND(INE(ICONST(0),IVAR(ID_75)),IEQ(ICONST(0),IVAR(ID_76))));BOR(BAND(IGE(IVAR(ID_74),IVAR(ID_77)),INE(ICONST(0),IVAR(ID_75))),BAND(BNOT(IGE(IVAR(ID_74),IVAR(ID_77))),IEQ(ICONST(0),IVAR(ID_75))));BOR(BAND(INE(IVAR(ID_78),ICONST(0)),INE(ICONST(0),IVAR(ID_79))),BAND(BNOT(INE(IVAR(ID_78),ICONST(0))),IEQ(ICONST(0),IVAR(ID_79))));IEQ(IVAR(ID_77),IVAR(ID_80));IEQ(IVAR(ID_80),IVAR(ID_33));IEQ(IVAR(ID_81),IVAR(ID_20));IEQ(IVAR(ID_82),IVAR(ID_20));BOR(BAND(ILT(IVAR(ID_83),IVAR(ID_82)),INE(ICONST(0),IVAR(ID_84))),BAND(BNOT(ILT(IVAR(ID_83),IVAR(ID_82))),IEQ(ICONST(0),IVAR(ID_84))));IEQ(IVAR(ID_78),IVAR(ID_85));BOR(BAND(ILT(IVAR(ID_86),IVAR(ID_81)),INE(ICONST(0),IVAR(ID_87))),BAND(BNOT(ILT(IVAR(ID_86),IVAR(ID_81))),IEQ(ICONST(0),IVAR(ID_87))));IEQ(IVAR(ID_83),IVAR(ID_47));IEQ(IVAR(ID_86),IVAR(ID_47));IEQ(IVAR(ID_72),IVAR(ID_88));IEQ(IVAR(ID_89),IVAR(ID_47));IEQ(IVAR(ID_90),IVAR(ID_20));BOR(BAND(ILT(IVAR(ID_91),IVAR(ID_92)),INE(ICONST(0),IVAR(ID_93))),BAND(BNOT(ILT(IVAR(ID_91),IVAR(ID_92))),IEQ(ICONST(0),IVAR(ID_93))));IEQ(IVAR(ID_94),IVAR(ID_47));BOR(BAND(INE(IVAR(ID_95),ICONST(0)),INE(ICONST(0),IVAR(ID_16))),BAND(BNOT(INE(IVAR(ID_95),ICONST(0))),IEQ(ICONST(0),IVAR(ID_16))));BOR(BAND(INE(IVAR(ID_96),ICONST(0)),INE(ICONST(0),IVAR(ID_97))),BAND(BNOT(INE(IVAR(ID_96),ICONST(0))),IEQ(ICONST(0),IVAR(ID_97))));IEQ(IVAR(ID_98),IVAR(ID_99));BOR(BAND(ILT(IVAR(ID_94),IVAR(ID_100)),INE(ICONST(0),IVAR(ID_101))),BAND(BNOT(ILT(IVAR(ID_94),IVAR(ID_100))),IEQ(ICONST(0),IVAR(ID_101))));IEQ(IVAR(ID_96),IVAR(ID_102));IEQ(IVAR(ID_92),IVAR(ID_20));IEQ(IVAR(ID_100),IVAR(ID_20));BOR(BAND(ILT(IVAR(ID_90),IVAR(ID_89)),INE(ICONST(0),IVAR(ID_103))),BAND(BNOT(ILT(IVAR(ID_90),IVAR(ID_89))),IEQ(ICONST(0),IVAR(ID_103))));IEQ(IVAR(ID_95),IVAR(ID_104));BOR(BAND(INE(IVAR(ID_98),ICONST(0)),INE(ICONST(0),IVAR(ID_105))),BAND(BNOT(INE(IVAR(ID_98),ICONST(0))),IEQ(ICONST(0),IVAR(ID_105))));IEQ(IVAR(ID_91),IVAR(ID_47));IEQ(IVAR(ID_106),IVAR(ID_107));BOR(BAND(INE(IVAR(ID_108),ICONST(0)),INE(ICONST(0),IVAR(ID_17))),BAND(BNOT(INE(IVAR(ID_108),ICONST(0))),IEQ(ICONST(0),IVAR(ID_17))));IEQ(IVAR(ID_109),IVAR(ID_106));BOR(BAND(IEQ(IVAR(ID_109),ICONST(1)),INE(ICONST(0),IVAR(ID_110))),BAND(BNOT(IEQ(IVAR(ID_109),ICONST(1))),IEQ(ICONST(0),IVAR(ID_110))));IEQ(IVAR(ID_29),ICONST(911));IEQ(IVAR(ID_34),ICONST(1));IEQ(IVAR(ID_47),ICONST(4194));IEQ(IVAR(ID_20),ICONST(4667));IEQ(IVAR(ID_45),ICONST(401));IEQ(IVAR(ID_28),ICONST(399));IEQ(IVAR(ID_37),ICONST(1));IEQ(IVAR(ID_21),ICONST(0));IEQ(IVAR(ID_68),IVAR(ID_12));IEQ(IVAR(ID_63),IVAR(ID_46));IEQ(IVAR(ID_40),IVAR(ID_53));IEQ(IVAR(ID_111),IVAR(ID_4));BOR(BNOT(IEQ(IVAR(ID_64),ICONST(0))),IEQ(IVAR(ID_51),IVAR(ID_111)));BOR(BNOT(IEQ(IVAR(ID_58),ICONST(0))),IEQ(IVAR(ID_112),ICONST(0)));IEQ(IVAR(ID_65),IVAR(ID_112));IEQ(IVAR(ID_60),IVAR(ID_62));BOR(BNOT(INE(IVAR(ID_55),ICONST(0))),IEQ(IVAR(ID_113),IVAR(ID_57)));IEQ(IVAR(ID_10),IVAR(ID_113));BOR(BNOT(IEQ(IVAR(ID_23),ICONST(0))),IEQ(IVAR(ID_114),ICONST(0)));IEQ(IVAR(ID_61),IVAR(ID_114));BOR(BNOT(IEQ(IVAR(ID_66),ICONST(0))),IEQ(IVAR(ID_115),IVAR(ID_2)));BOR(BNOT(IEQ(IVAR(ID_67),ICONST(0))),IEQ(IVAR(ID_116),IVAR(ID_115)));BOR(BNOT(INE(IVAR(ID_69),ICONST(0))),IEQ(IVAR(ID_18),IVAR(ID_116)));IEQ(IVAR(ID_102),IVAR(ID_93));BOR(BNOT(IEQ(IVAR(ID_97),ICONST(0))),IEQ(IVAR(ID_117),ICONST(-1)));IEQ(IVAR(ID_118),IVAR(ID_117));IEQ(IVAR(ID_88),IVAR(ID_87));BOR(BNOT(INE(IVAR(ID_58),ICONST(0))),IEQ(IVAR(ID_112),IVAR(ID_73)));IEQ(IVAR(ID_104),IVAR(ID_101));BOR(BNOT(IEQ(IVAR(ID_55),ICONST(0))),IEQ(IVAR(ID_113),ICONST(0)));IEQ(IVAR(ID_99),IVAR(ID_103));BOR(BNOT(INE(IVAR(ID_23),ICONST(0))),IEQ(IVAR(ID_114),IVAR(ID_105)));BOR(BNOT(INE(IVAR(ID_66),ICONST(0))),IEQ(IVAR(ID_115),ICONST(1)));BOR(BNOT(IEQ(IVAR(ID_71),ICONST(0))),IEQ(IVAR(ID_116),IVAR(ID_115)));IEQ(IVAR(ID_85),IVAR(ID_84));BOR(BNOT(IEQ(IVAR(ID_79),ICONST(0))),IEQ(IVAR(ID_119),ICONST(0)));IEQ(IVAR(ID_106),ICONST(1));IEQ(IVAR(ID_108),IVAR(ID_110));BOR(BNOT(INE(IVAR(ID_71),ICONST(0))),IEQ(IVAR(ID_116),ICONST(0)));BOR(BNOT(INE(IVAR(ID_79),ICONST(0))),IEQ(IVAR(ID_119),IVAR(ID_76)));BOR(BNOT(INE(IVAR(ID_97),ICONST(0))),IEQ(IVAR(ID_117),IVAR(ID_119)));BOR(BNOT(INE(IVAR(ID_64),ICONST(0))),IEQ(IVAR(ID_51),IVAR(ID_118)))";
//    String strPC = "BOR(BNOT(IEQ(IVAR(ID_1),ICONST(0))),IEQ(IVAR(ID_2),ICONST(0)))";
//    String strPC = "BNOT(BAND(BCONST(TRUE),INE(IVAR(ID_37),ICONST(0))))";
    
//    !(true & ($V47 < $V20))
    
//    String strPC = "BNOT(BAND(BCONST(TRUE),ILE(IVAR(ID_47),IVAR(ID_20))))";
    
    String strPC = "BNOT(BAND(IEQ(IVAR(ID_1),ICONST(0)),INE(IVAR(ID_2),ICONST(0))))";
    PC pc = (new Parser(strPC)).parsePC();
    System.out.println(pc);
    pc = rew(pc);
    System.out.println("**"+pc);
    
  }
 
}
