package coral.simplifier;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.Util;
import symlib.parser.Parser;
import coral.PC;
import coral.solvers.Env;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;
import coral.util.visitors.EqualitySearcher;

public class DP_Eq {

  /**
   * this method eliminates some variables from the path condition 
   * which are entirely dependent on others.  The method operates 
   * in two steps. First, it discovers assignments of the form 
   * VAR = E.  Then it replaces occurrences of VAR in the input PC
   * and replaces it with E.  
   *
   * 
   * @param pc
   * @return a pair containing the new path condition (see above) 
   * and the environment with the symbolic assignment of dependent 
   * variables
   * @throws InterruptedException 
   * @throws IOException 
   *   
   */
  public static Object[] removeSimpleEqualities(PC pc) throws IOException, InterruptedException {
    // STEP 1: identify simple equalities
    Map<SymLiteral, List<SymNumber>> symVarAssignment = new HashMap<SymLiteral, List<SymNumber>>();
    for (SymBool constraint : pc.getConstraints()) {
      EqualitySearcher literalSearcher = new EqualitySearcher(constraint);
      Map<SymLiteral, List<SymNumber>> map = literalSearcher.getDependentVarsMap();
      symVarAssignment.putAll(map);
    }
    
    // STEP 2: build symbolic environment
    Map<SymLiteral, List<SymNumber>> heuristicVarsMap = symVarAssignment;
    Env env = new Env(null, null);
    for (Map.Entry<SymLiteral, List<SymNumber>> entry: heuristicVarsMap.entrySet()) {
      env.addKeyValue(entry.getKey(), entry.getValue().get(0));
    }
    // STEP 3: build new path condition
    PC result = pc.replaceVars(env);
    
//    System.out.println("STEP3 pc:" + result);
//    System.out.println("STEP3 env:" + env);

    Map<Integer,Interval> intervals = null;
    // STEP 3.5 try to find solutions for some of the variables with an interval solver
    if(/*Config.enableIntervalBasedSolver &&*/ Config.simplifyUsingIntervalSolver) { 
        IntervalSolverCaller isolver = coral.solvers.rand.Util.getIntervalSolver(Config.intervalSolver);
        intervals = isolver.callSolver(result, Config.intervalSolverTimeout, Config.intervalSolverPrecision);
        Env toReplace = new Env(null,null);     
        
        for(Entry<Integer,Interval> entry : intervals.entrySet()) {
          Interval it = entry.getValue();
          Integer id = entry.getKey();
          if(it.hi().equals(it.lo())) {
            // TODO make this more efficient
            SymLiteral lit = null;
            for(SymLiteral varLit: pc.getVars()) {
              if(id == varLit.getId()) {
                lit = varLit;
                break;
              }
            }
            toReplace.addKeyValue(lit, Util.createConstant(it.hi().doubleValue()));
          }
        }
        
        result = result.replaceVars(toReplace);
        env = Env.combineEnv(env, toReplace, null);
    }
    
//    System.out.println("STEP3.5 pc:" + result);
//    System.out.println("STEP3.5 env:" + env);
    
    return new Object[]{result, env,intervals};
  }

  public static void main(String[] args) throws Exception {
    String str = "BAND(IGT(MUL(IVAR(ID_1),MUL(IVAR(ID_1),IVAR(ID_2))),ICONST(0)),IEQ(IVAR(ID_1),IVAR(ID_2)))";
//    String str = "DGT(EXP_(DVAR(ID_1)),DCONST(0.0))";
    Parser parser = new Parser(str);
    PC pc = parser.parsePC();
    System.out.println(pc);
    Object[] pair = removeSimpleEqualities(pc);
    PC newPC = (PC) pair[0];
    Env sol = (Env) pair[1];
    System.out.println(newPC);
    System.out.println(sol);
  }

}