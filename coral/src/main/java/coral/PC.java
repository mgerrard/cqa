package coral;

import java.util.*;

import symlib.SymBool;
import symlib.SymBoolOperations;
import symlib.SymIntLiteral;
import symlib.SymLiteral;
import symlib.SymLongLiteral;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import coral.solvers.Env;
import coral.util.visitors.PCCanonicalForm;
import coral.util.visitors.SymLiteralReplacer;
import coral.util.visitors.SymLiteralSearcher;

public class PC {

  private List<SymBool> constraints = 
    new ArrayList<SymBool>();

  public PC(List<SymBool> constraints) {
    super();
    this.constraints = constraints;
  }

  public PC() {}

  public void addConstraint(SymBool constraint) {
    if (constraint == null)
      return;
    constraints.add(constraint);
  }
  
  public void setConstraints(List<SymBool> constraints) {
    this.constraints = constraints;
  }

  /**
   *
   * @return a set with all symbolic variables that appear in 
   * the path condition
   */
  private Set<SymLiteral> cached_vars;
  public Set<SymLiteral> getVars() {
    if (cached_vars == null) {
      cached_vars = new HashSet<SymLiteral>();
      for (SymBool constraint : constraints) {
        cached_vars.addAll(new SymLiteralSearcher(constraint).getVars());
      }
    }
    return cached_vars;
  }
  
  private SortedSet<SymLiteral> cached_sorted_vars;
  public SortedSet<SymLiteral> getSortedVars() {
    if (cached_sorted_vars == null) {
      cached_sorted_vars = new TreeSet<SymLiteral>(new Comparator<SymLiteral>() {
        @Override
        public int compare(SymLiteral o1, SymLiteral o2) {
          if(o1.getId() < o2.getId()) {
            return -1;
          } else {
            return 1;
          }
        }
      });
      
      for (SymLiteral var : getVars()) {
        cached_sorted_vars.add(var);
      }
    }
    return cached_sorted_vars;
  }

  public boolean hasConstraints() {
    return !this.getConstraints().isEmpty();
  }

  public List<SymBool> getConstraints() {
    return this.constraints;
  }

  /**
   * 
   * @param env 
   * @return a new path condition with the variable in env 
   * replaced with their corresponding symbolic values
   */
  public PC replaceVars(Env env) {
    PC newPC = new PC();
    SymLiteralReplacer replacer = new SymLiteralReplacer(env);
    List<SymBool> oldConstraints = getConstraints();
    for (SymBool aConstraint : oldConstraints) {
      newPC.addConstraint(aConstraint.accept(replacer));
    }
    return newPC;
  }

  /**
   * 
   * @return true if this path condition is satisfied by the 
   * current assignment of values to symbolic variables.  This
   * assignment is characterized with the constant values 
   * attached to the variables.
   */
  public boolean eval() {
    boolean result = true;
    for (SymBool bExp : getConstraints()) {
      //TODO add support to compiled constraints
      ReversePolish rpol = GenReversePolishExpression.createReversePolish(bExp);
      result &= rpol.isSAT();
      if (!result) {
        break;
      }
    }
    return result;
  }

  @Override
  public boolean equals(Object pc) {
    if (pc == null) {
      return false;
    }

    List<SymBool> otherConstraints = ((PC) pc).getConstraints();

    if (otherConstraints.size() != this.constraints.size()) {
      return false;
    }

    boolean result = true;

    for (int i = 0; i < this.constraints.size(); i++) {
      if (this.constraints.get(i) == null) {
        return false;
      }
      if (otherConstraints.get(i) == null) {
        return false;
      }

      result = result & this.constraints.get(i).equals(otherConstraints.get(i));
    }

    return result;
  }
  
  public String toCompareStrings() {
    StringBuilder builder = new StringBuilder();
    int i = 1;
    for (SymBool constraint : constraints) {
      builder.append(" " + i++ + ": " + constraint + "\n");
    }
    return builder.toString();
  }

  public String toString() {
    if (constraints.size() == 0) {
      return "";
    } else if (constraints.size() == 1) {
      return constraints.get(0).toString();
    } else if (constraints.size() == 2) {
      return "AND(" + constraints.get(0) + "," + constraints.get(1) + ")";
    } else {

      int counter = 1;
      StringBuilder builder = new StringBuilder();

      for (SymBool constraint : constraints) {
        if (counter == 1) {
          builder.insert(0, "AND(");
          builder.append(constraint);
          builder.append(",");

          counter++;
        } else if (counter == 2) {
          builder.append(constraint);
          builder.append("),");

          counter++;
        } else {
          builder.insert(0, "AND(");
          builder.append(constraint);
          builder.append("),");
        }

      }
      return builder.substring(0, builder.length() - 1) + ")";
    }
  }
  
  public PC getCanonicalForm(){
    PC newPC = new PC();
    PCCanonicalForm visitor = new PCCanonicalForm();
    List<SymBool> oldConstraints = getConstraints();
    for (SymBool aConstraint : oldConstraints) {
      newPC.addConstraint(aConstraint.accept(visitor));
    }
    return newPC;
  }
  
  public PC splitANDs() {
    List<SymBool> newClauses = new ArrayList<SymBool>();
    for(SymBool clause : this.getConstraints()) {
      extractClauses(clause,newClauses);
    }
    
    return new PC(newClauses);
  }

  private static void extractClauses(SymBool clause, List<SymBool> newClauses) {
    if(clause instanceof SymBoolOperations) {
      SymBoolOperations bool = ((SymBoolOperations) clause);
      int op = bool.getOp();
      
      if(op == SymBoolOperations.AND) {
        extractClauses(bool.getA(), newClauses);
        extractClauses(bool.getB(), newClauses);
      } else {
        newClauses.add(clause);
      }
    } else {
      newClauses.add(clause);
    }
  }
  
  @Override
  public int hashCode() {
    return constraints.hashCode();
  }

  public int size() {
    return constraints.size();
  }
  
  public Boolean[] getIntDomainArray() {
    Boolean[] arr = new Boolean[this.getSortedVars().size()];
    int i = 0;
    for (SymLiteral var : this.getSortedVars()) {
      arr[i] = var instanceof SymIntLiteral || var instanceof SymLongLiteral;
      i++;
    }
    return arr;
  }
}