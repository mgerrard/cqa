package coral.util;

import coral.util.options.Option;
import coral.util.options.Options;

public class Config {

  public final static int NORMAL = 0;
  public final static int LARGE = 1;

  @Option("input path condition (for command-line interface)")
  public static String inputCONS;

  @Option("number of threads")
  public static final int numThreads = 5;

  @Option("set how long each solver should run")
  public static long timeout = 1000;

  @Option("set precision of generated numbers")
  public static int fractionPrecision = 10;

  @Option("lower bound of random search interval")
  public static int rangeLO = -10000;

  @Option("upper bound of random search interval")
  public static int rangeHI = 10000;
  
  @Option("allows the inferred range for a variable to be greater than [rangeLO,rangeHI]")
  public static boolean flexibleRange = true;

  public static Range RANGE = new Range(rangeLO, rangeHI);

  @Option("random number generator seed")
  public static long seed = 464655;

  //Space search size for trigonometric vars
  @Option("Size of pi fractions (radian)")
  public static int radianSize = LARGE;
  @Option("Size of pi fractions (degree)")
  public static int degreeSize = LARGE;

  @Option("Number of particles utilized by the pso solver")
  public static int nParticlesPSO = 55;  
  @Option("Number of iterations executed by the pso solver")
  public static int nIterationsPSO = 600;
  @Option("Perturbation of particles in the pso solver")
  public static double perturbationPSO = 200;
  @Option("Number of failed tentatives to solve constraint before incrementing it's weight")
  public static int maxHit = 50;
  
  @Option("Size of the population for the D.E. solver")
  public static int nPopulationDE = 55;
  @Option("Number of iterations executed by the D.E. solver")
  public static int nIterationsDE = 600;
  @Option("Scaling factor used by the D.E. solver in the generation of candidates")
  public static double scalingFactorDE = 0.35;

  @Option("Number of generations executed by the random solver")
  public static int nIterationsRANDOM = 360000;	

  @Option("Number of generations executed by the ga solver")
  public static int nIterationsGA = 400;
  @Option("Size of population for the ga solver")
  public static int nPopulationGA = 90;
  @Option("Number of parents for the ga solver")
  public static int nParentsGA = nPopulationGA / 4;
  @Option("Number of children for the ga solver")
  public static int nChildrenGA = nParentsGA;
  @Option("Rate of crossover for the ga solver")
  public static double crossoverRateGA = 0.90;
  @Option("Show concrete value of symbolic variables (on toString)")
  public static boolean showConcreteValues = false;

  @Option("equality decision procedure simplifies the constraint before calling solver ")
  public static boolean removeSimpleEqualities = true;
  @Option("infers ranges for the variables based on units") //TODO revise this text!
  public static boolean toggleValueInference = true;
  @Option("report average time spent on unsolved constraints") 
  public static boolean reportTimeUnsolved = false;
  @Option("norm constant")
  public static int normValue = 100;
  @Option("Max number of iterations for radian search")
  public static int radianSearchLimit = 150;
  @Option("Max number of individuals of previous runs inserted in new searches")
  public static int maxInsertedIndividuals = 10;
  @Option("Enable storing of individuals from previous runs")
  public static boolean enableIndividualStoring = false;
  @Option("Store just the individuals of the previous run (cache size = 1)") 
  public static boolean storeLastIndividuals = false;
  @Option("Always insert an individual with genotype = 0")
  public static boolean insertZeroIndividual = true;
  @Option("Cache previous solutions (doesn't work without partitioning for now)")
  public static boolean cacheSolutions = false;
  @Option("Partition constraints before solving")
  public static boolean partitionConstraints = false;
  @Option("stop prematurely if one partition is not SAT")
  public static boolean stopIfPartitionUnsat = false;
  @Option("random restart if nIterations without improvement > this; 0 = off")
  public static int stagnationLimit = 0;
  
  public static SolutionCache cache = new SolutionCache();
  
  @Option("Use the interval solver to create some individuals")
  public static boolean enableIntervalBasedSolver = false; 
  @Option("Precision of the interval solver (1E-$param)") //TODO fix this text
  public static int intervalSolverPrecision = 3;
  @Option("in ms")
  public static int intervalSolverTimeout = 2000;
  @Option("Max. number of boxes the solver should return")
  public static int intervalSolverMaxBoxes = 10;
  @Option("Extra arguments to the interval solver")
  public static String intervalSolverExtraArgs = "";

  @Option("Canonicalize the query rewriting it's variables. Ex: AND(((1-$V12) <= 0),($V12 == $V14)) ==> AND(((1-$V1) <= 0),($V1 == $V2))")
  public static boolean pcCanonicalization = true;
  
  @Option("which interval-based solver you want to use (choose between 'realpaver' and 'icos')")
  public static String intervalSolver = "realpaver";
  @Option("Path to realPaver executable")
  public static String realPaverLocation = "realpaver";
  @Option("path to icos-clp executable")
  public static String icosLocation = "/home/mateus/tools/icos/icos-clp";
  @Option("args to icos")
  public static String icosArgs = "G=/tmp/icoslogs";
  @Option("try to simplify the constraint using an interval-based solver")
  public static boolean simplifyUsingIntervalSolver = false;
  
  @Option("Number of generations executed by the AVM solver")
  public static int nIterationsAVM = 20000;
  @Option("Number of selection steps executed per variable by the AVM solver")
  public static int nSelectionsAVM = 10;
  
  //Model counting options
  
  @Option("Minimum number of pseudorandom sampling points to be used for each box")
  public static int mcMinSamplesPerBox = 1000;  
  @Option("Total amount of samples for a single model counter call")
  public static long mcMaxSamples = 500000  ;
//  @Option("Max. amount of boxes returned by the interval solver")
//  public static int mcMaxBoxes = 10;
  @Option("set distinct amounts of samples for each box based on volume")
  public static boolean mcDistributeSamples = false;
  @Option("don't use an interval solver before running the model counter")
  public static boolean mcSkipPaving = false;
  @Option("merge all boxes into a single one")
  public static boolean mcMergeBoxes = false;
//  @Option("seed (six longs, separated by commas")
//  public static String mcSeed = "990764872,70974053,810762905,153046394,848454615,21597730";
  @Option("seed used in the rng ")
  public static long mcSeed = 99076487270974053l;
  @Option("cache realpaver paving results")
  public static boolean cachePavingResults = false;
  @Option("Use global adaptive integration (false = monte carlo)")
  public static boolean mathematicaGA = false;
  @Option("try to find a optimum domain split when discretizing")
  public static boolean optimizeDiscretization = false;
  @Option("Uses a iterative algorithm to improve the precision of the solution. Implies in partitioning + interval solving")
  public static boolean mcIterativeImprovement = false;
  @Option("Minimum value for the variance before the iterative improvement algorithm stop")
  public static double mcTargetVariance = Math.pow(10, -16);
  @Option("Number of samples used by the incremental solver for each call")
  public static int mcSamplesPerIncrement = 10000;
  @Option("")
  public static int mcDerivativeVersion = 0;
  @Option("How many samples will be used in the first round of the incrementalAlgorithm")
  public static int mcInitialPartitionBudget = 100000;
  @Option("Allocate samples proportionally to the derivative")
  public static boolean mcAllocateBudgetProportionally = false;
  @Option("Allocate samples to boxes proportionally to the respective variance")
  public static boolean mcProportionalBoxSampleAllocation = false;
  @Option("testing...")
  public static int mcNSamplesInRandomPartition = 0;
  @Option("regions in discretization")
  public static int mcDiscretizationRegions = 6;
  @Option("Ignore ranking and improve all partitions uniformly each step")
  public static boolean mcNonRankedIterativeImprovement = false;
  //TODO not option... just a hack 
  public static boolean storeVariance = false;
  @Option("Rank partitions by their variance")
  public static boolean mcDumbRankingHeuristic = false;
  @Option("Try all possible combinations of values")
  public static boolean mcExaustiveSearch = false;
  @Option("Introduce slack variables to account for integer divisions on realpaver")
  public static boolean intervalSolverSlackDivision = false;
  @Option("Compile constraints before evaluating them")
  public static boolean compileConstraints = false;
  @Option("Use realpaver to bound solution spaces before counting")
  public static boolean mcUseRealPaver = true;


  public static void load(String[] args, Options options){
    if (args != null && args.length > 0) {
      Options.loadOptions(args, options);
      Options.loadOptions(args, options);
    }
  }
  
  public static void printOptions(Options options) {
    System.out.println("parameters:");
    System.out.println("==========================================");
    System.out.println(options.settings());
    System.out.println("==========================================");
  }
}
