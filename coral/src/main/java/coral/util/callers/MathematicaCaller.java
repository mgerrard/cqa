package coral.util.callers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.GeometricDistribution;
import org.apache.commons.math3.distribution.IntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

import symlib.SymBool;
import symlib.SymDouble;
import symlib.SymDoubleConstant;
import symlib.SymDoubleRelational;
import symlib.SymInt;
import symlib.SymIntConstant;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymMathBinary;

import com.wolfram.jlink.KernelLink;
import com.wolfram.jlink.MathLinkException;
import com.wolfram.jlink.MathLinkFactory;

import coral.PC;
import coral.counters.CountingProblem;
import coral.counters.rvars.IntegerRandomVariable;
import coral.counters.rvars.RandomVariable;
import coral.counters.rvars.RealRandomVariable;
import coral.util.Config;
import coral.util.Interval;
import coral.util.visitors.MathematicaVisitor;

public class MathematicaCaller {

  KernelLink ml = null;
  int fCounter = 1;

  public MathematicaCaller() {
    try {
      // Typical launch on Windows
      // KernelLink ml =
      // MathLinkFactory.createKernelLink("-linkmode launch -linkname 'c:\\program files\\wolfram research\\mathematica\\7.0\\mathkernel.exe'");
      // Typical launch on Unix
      ml = MathLinkFactory
          .createKernelLink("-linkmode launch -linkname 'math -mathlink'");

      // Typical launch on Mac OS X
      // ml =
      // MathLinkFactory.createKernelLink("-linkmode launch -linkname '/Applications/Mathematica.app/Contents/MacOS/MathKernel'");

      // Get rid of the initial InputNamePacket the kernel will send
      // when it is launched.
      ml.discardAnswer();
      ml.evaluateToOutputForm("SeedRandom['" + Config.mcSeed + "']", 0);
    } catch (MathLinkException e) {
      System.out.println("Fatal error opening link: " + e.getMessage());
      throw new RuntimeException(e);
    }
  }

  /**
   * The order of the boxes should be the same as the order of the variables
   * returned by PC.getSortedVars()
   * 
   * @param pc
   * @param boxes
   * @return
   */

  public double integrateAdaptativeMonteCarlo(PC pc, List<Interval[]> boxes) {
    List<String> mathCons = toMathematicaFormat(pc);

    Set<SymLiteral> vars = pc.getSortedVars();
    String function = prepareMathematicaFunction(mathCons, vars);
    System.out.println(function);
    String strResult = ml.evaluateToOutputForm(function, 0);
    System.out.println("[mathematica] " + strResult);

    // f[x_,y_] -> f[x,y]
    String functionReference = function.split(":=")[0].replace("_", "");

    double volume = 0.0;

    for (Interval[] box : boxes) {
      String command = prepareCommand(box, vars, functionReference);
      System.out.println(command);
      strResult = ml.evaluateToOutputForm(command, 0);
      System.out.println("[mathematica] " + strResult);

      if (strResult.split("E", -1).length - 1 >= 2) { // output is composed by 2
                                                      // or more numbers
        strResult = strResult.substring(0, strResult.indexOf(' '));
      }
      volume += parseMathematicaDouble(strResult);
      System.out.println(volume);
    }

    return volume;
  }

  private List<String> toMathematicaFormat(PC pc) {
    List<String> mathCons = new ArrayList<String>();
    MathematicaVisitor mv = new MathematicaVisitor();

    for (SymBool bool : pc.getConstraints()) {
      mathCons.add(bool.accept(mv));
    }
    return mathCons;
  }

  private double parseMathematicaDouble(String output) {
    double val;
    if (output.endsWith(".E")) { // hack to avoid issues with fp representation
      val = Double.parseDouble(output.substring(0, output.length() - 2));
    } else if (output.endsWith("E")) { // hack to avoid issues with fp
                                       // representation
      val = Double.parseDouble(output.substring(0, output.length() - 1));
    } else {
      val = Double.parseDouble(output);
    }
    return val;
  }

  private String prepareCommand(Interval[] box, Set<SymLiteral> vars,
      String functionReference) {
    StringBuffer sb = new StringBuffer();
    sb.append("ScientificForm[NIntegrate[").append(functionReference);

    Iterator<SymLiteral> varIt = vars.iterator();
    for (Interval iv : box) {
      String litName = "x" + varIt.next().getId();
      sb.append(", {").append(litName).append(",");

      String lo = MathematicaVisitor.toMathematicaScientificNotation(iv.lo().doubleValue());
      String hi = MathematicaVisitor.toMathematicaScientificNotation(iv.hi().doubleValue());

      sb.append(lo).append(", ");
      sb.append(hi).append("}");
    }
    if (Config.mathematicaGA) {
      // sb.append(", Method -> {GlobalAdaptive}]");
      sb.append("],NumberFormat -> (#1 <> \"E\" <> #3 &)]");
    } else {
      sb.append(", Method -> {MonteCarlo, MaxPoints -> ");
      sb.append(Config.mcMaxSamples);
      sb.append("}]");
      sb.append(",NumberFormat -> (#1 <> \"E\" <> #3 &)]");
    }
    // sb.append(" }, PrecisionGoal -> 5],NumberFormat -> (#1 <> \"E\" <> #3 &)]");
    return sb.toString();
  }

  // ex: f[z_,y_,x_] := If[x>10.5 && x<15.3, 1, 0];
  private String prepareMathematicaFunction(List<String> mathCons,
      Set<SymLiteral> vars) {
    StringBuffer sb = new StringBuffer();
    sb.append("f").append(fCounter++);
    sb.append('[');

    for (SymLiteral var : vars) {
      sb.append("x" + var.getId() + "_,");
    }
    sb.delete(sb.length() - 1, sb.length());
    sb.append("] := If[");

    for (String cons : mathCons) {
      sb.append(cons);
      sb.append(" && ");
    }

    sb.delete(sb.length() - 4, sb.length());
    sb.append(", 1.0, 0.0];");

    return sb.toString();
  }

  public void reseed(long seed) {
    ml.evaluateToOutputForm("SeedRandom['" + seed + "']", 0);
  }

  public double probability(CountingProblem cp, boolean nProb) {
    PC boundedPC = new PC(cp.pc.getConstraints());
    Map<SymLiteral, RandomVariable> lit2rvar = cp.lit2rvar;

    StringBuffer distDeclaration = new StringBuffer();
    distDeclaration.append('{');
    for (SymLiteral lit : boundedPC.getVars()) {
      RandomVariable rvar = lit2rvar.get(lit);

      // add domain constraint to pc
//      addDomainConstraint(boundedPC, lit, rvar);
      
      distDeclaration.append("x" + lit.getId() + " \\[Distributed] ");
      distDeclaration.append("TruncatedDistribution[{");
      distDeclaration.append(rvar.bounds.lo().doubleValue());
      distDeclaration.append(',');
      distDeclaration.append(rvar.bounds.hi().doubleValue());
      distDeclaration.append("},");
      distDeclaration.append(prepareMathematicaRVar(rvar));
      distDeclaration.append("],");
    }
    distDeclaration.deleteCharAt(distDeclaration.length() - 1);
    distDeclaration.append('}');

    List<String> mathematicaCons = toMathematicaFormat(boundedPC);
    String command = prepareCommand(mathematicaCons,
        distDeclaration.toString(), nProb);

    System.out.println(command);
    String strResult = ml.evaluateToOutputForm(command, 0);
    System.out.println("[mathematica] " + strResult);
    if (strResult.split("E", -1).length - 1 >= 2) { // output is composed by 2
                                                    // or more numbers
      strResult = strResult.substring(0, strResult.indexOf(' '));
    }
    double result = parseMathematicaDouble(strResult);
    return result;
  }

  private String prepareCommand(List<String> mathematicaCons, String varDec,
      boolean nProb) {
    StringBuffer command = new StringBuffer();
    if (nProb) {
      command.append("ScientificForm[NProbability[");
    } else {
      command.append("ScientificForm[N[Probability[");
    }
    
    for(String s : mathematicaCons) {
      command.append(s);
      command.append(" && ");
    }
    int l = command.length();
    command.delete(l - 4, l);
    command.append(",");
    
    command.append(varDec);
    
    if (nProb) {
//      command.append(",Method -> {\"NIntegrate\", {Method -> \"MonteCarlo\"}}],NumberFormat -> (#1 <> \"E\" <> #3 &)]");
      command.append("],NumberFormat -> (#1 <> \"E\" <> #3 &)]");
    } else {
      command.append("]],NumberFormat -> (#1 <> \"E\" <> #3 &)]");
    }

    return command.toString();
  }

  private String prepareMathematicaRVar(RandomVariable rvar) {
    if (rvar instanceof RealRandomVariable) {
      RealRandomVariable rrvar = (RealRandomVariable) rvar;
      RealDistribution rdist = rrvar.rdist;
      
      if (rdist instanceof NormalDistribution) {
        double mean = rdist.getNumericalMean();
        double sd = ((NormalDistribution) rdist).getStandardDeviation();
        return "NormalDistribution[" + mean + "," + sd + "]";
      } else if (rdist instanceof UniformRealDistribution) {
        return "UniformDistribution[{" + rrvar.bounds.lo().doubleValue() + "," + rrvar.bounds.hi().doubleValue() + "}]";
      } else if (rdist instanceof ExponentialDistribution) {
        double mean = rdist.getNumericalMean();
        return "ExponentialDistribution[" + (1/mean) + "]";
      }
    } else {
      IntegerRandomVariable irvar = (IntegerRandomVariable) rvar;
      IntegerDistribution idist = irvar.idist;
      
      if (idist instanceof BinomialDistribution) {
        double p = ((BinomialDistribution) idist).getProbabilityOfSuccess();
        int trials = ((BinomialDistribution) idist).getNumberOfTrials();
        return "BinomialDistribution[" + trials + "," + p + "]";
      } else if (idist instanceof PoissonDistribution) {
        double lambda = ((PoissonDistribution) idist).getMean(); 
        return "PoissonDistribution[" + lambda + "]";
      } else if (idist instanceof GeometricDistribution) {
        double p = ((GeometricDistribution) idist).getProbabilityOfSuccess();
        return "GeometricDistribution[" + p + "]";
      } else if (idist instanceof UniformIntegerDistribution) {
        return "DiscreteUniformDistribution[{" + (int) irvar.bounds.lo().doubleValue() + ","
            + (int) irvar.bounds.hi().doubleValue() + "}]";
      }
    }
    throw new RuntimeException("Unkown variable type - please add it ");
  }

  private void addDomainConstraint(PC boundedPC, SymLiteral lit,
      RandomVariable rvar) {
    SymBool lower;
    SymBool upper;
    if (rvar instanceof IntegerRandomVariable) {
      lower = SymIntRelational.create((SymInt) lit, new SymIntConstant(
          (int) rvar.bounds.lo().doubleValue()), SymIntRelational.GE);
      upper = SymIntRelational.create((SymInt) lit, new SymIntConstant(
          (int) rvar.bounds.hi().doubleValue()), SymIntRelational.LE);
    } else {
      lower = SymDoubleRelational.create((SymDouble) lit, new SymDoubleConstant(
          rvar.bounds.lo().doubleValue()), SymDoubleRelational.GE);
      upper = SymDoubleRelational.create((SymDouble) lit, new SymDoubleConstant(
          rvar.bounds.hi().doubleValue()), SymDoubleRelational.LE);
    }
    
    boundedPC.addConstraint(lower);
    boundedPC.addConstraint(upper);
  }
}
