package coral.util.callers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Arrays;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.solvers.rand.Util;
import coral.util.Config;
import coral.util.Interval;
import coral.util.visitors.PaverVisitor;

public class RealPaverCaller implements IntervalSolverCaller {

  boolean enableExperimentalRewriting;

  public RealPaverCaller() {
    this.enableExperimentalRewriting = false;
  }

  public RealPaverCaller(boolean enableExperimentalRewriting) {
    this.enableExperimentalRewriting = enableExperimentalRewriting;
  }

  @Override
  public Map<Integer, Interval> callSolver(PC pc, int timeout, int precision) throws IOException, InterruptedException {
    Set<SymLiteral> vars = pc.getVars();
    Map<Integer,Interval> defaultBoxMap = new HashMap<Integer,Interval>();
    for (SymLiteral var : vars) {
      defaultBoxMap.put(var.getId(), new Interval(Config.RANGE.getLo(),Config.RANGE.getHi()));
    }
    return callRealPaver(pc, defaultBoxMap, timeout, precision);                                                                                         
  }
  
  //TODO refactor this with getPaving
  public Map<Integer,Interval> callRealPaver(PC pc, Map<Integer,Interval> box, int timeout,int precision) throws IOException, InterruptedException {
    Map<Integer,Interval> varIdToInterval;
    RealPaverInput processedInfo = processConstraints(pc, box, timeout, precision, 1, false);

    String realPaverInput = processedInfo.realpaverInput;
    int nVars = processedInfo.nvars;
    
    if (processedInfo.nUsedVars == 0) { //all clauses contain unsupported operations
      varIdToInterval = box;
    } else {
      varIdToInterval = new TreeMap<Integer,Interval>();
      Process p = runRealPaver(realPaverInput);
      
      BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
      int currentVar = 0;
      String line = reader.readLine();
      while (line != null) {
        if(line.contains("OUTER BOX") || line.contains("INNER BOX")) { //solution starts now
          currentVar = 1;
        } else if (currentVar > 0 && currentVar <= nVars) {
          Interval iv;
          int varId;
          double hi,lo;
          if(line.contains("=")) {
            String[] groups = line.split("x|=");
//          System.out.println(Arrays.toString(groups));
            hi = Double.valueOf(groups[2]);
            lo = Double.valueOf(groups[2]);
            varId = Integer.valueOf(groups[1].trim());
          } else {
            String[] groups = line.split("\\[|\\]|\\,|x|in");
//          System.out.println(Arrays.toString(groups));
            hi = Double.valueOf(groups[4]);
            lo = Double.valueOf(groups[3]);
            varId = Integer.valueOf(groups[1].trim());
          }
          iv = new Interval(lo, hi);
          varIdToInterval.put(varId, iv);
          currentVar++;
        }
//      System.out.println(line);
        line = reader.readLine();
      }
      p.waitFor();
//    if(varIdToInterval.size() == 0) {
//      System.out.println("empty interval!");
//    }
    }
    
//    System.out.println("pc:" + pc);
    return varIdToInterval;
  }
  
  //TODO we assume that all calls to the same pc will use the same interval. check if this is true 
  public static Map<PC,List<Interval[]>> pavingCache = new HashMap<PC, List<Interval[]>>();
  
  @Override
  public List<Interval[]> getPaving(PC pc, Interval[] defaultInterval) throws IOException, InterruptedException {
    List<Interval[]> result;
    
    if (Config.cachePavingResults) {
      result = pavingCache.get(pc);
      if (result == null) {
        result = runPaving(pc,defaultInterval);
        pavingCache.put(pc, result);
      } else {
		  //        System.out.println("[realpaver] cache hit!");
      }
    } else {
       result = runPaving(pc,defaultInterval);
    }
    
	  return result;
  }

  private List<Interval[]> runPaving(PC pc, Interval[] defaultInterval)
      throws IOException, InterruptedException {
    //      System.out.println("blabla");
    int timeout = Config.intervalSolverTimeout;
    int precision = Config.intervalSolverPrecision;
    int nBoxes = Config.intervalSolverMaxBoxes;
    List<Interval[]> boxes = new ArrayList<Interval[]>(Config.intervalSolverMaxBoxes);
    
    RealPaverInput processedInfo = processConstraints(pc, defaultInterval, timeout, precision, nBoxes, true);
    String realPaverInput = processedInfo.realpaverInput;
    int nVars = processedInfo.nvars;

    // If all variables don't appear in the pc, return a "sparse array". Remember to filter the
    // useful values before using it. 
    int biggestId = 0;
    for (SymLiteral lit : pc.getSortedVars()) { //FIXME there should be a smarter way to do this
      if (biggestId < lit.getId()) {
        biggestId = lit.getId();
      }
    }
    
    if (processedInfo.nUsedVars == 0) { //all clauses contain unsupported operations; return default box
      System.out.println("[realpaver] all clauses contain unsupported operations. returning default box...");
      boxes.add(defaultInterval);
    } else {
      long start = System.nanoTime();
      Process p = runRealPaver(realPaverInput);
  
      BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
      int currentVar = -1;
      String line = reader.readLine();
      Interval[] box = null;
  
      while (line != null) {
  		//      System.out.println("blabla");
        if(line.contains("OUTER BOX") || line.contains("INNER BOX")) { //solution starts now
          currentVar = 0;
  //        box = new Interval[nVars];
          box = new Interval[biggestId];
        } else if (currentVar >= 0 && currentVar < nVars) {
          Interval iv;
          int varId;
          double hi,lo;
          if(line.contains("=")) {
            String[] groups = line.split("x|=");
  //          System.out.println(Arrays.toString(groups));
            hi = Double.valueOf(groups[2]);
            lo = Double.valueOf(groups[2]);
            varId = Integer.valueOf(groups[1].trim());
          } else {
            String[] groups = line.split("\\[|\\]|\\,|x|in");
  		  //          System.out.println(Arrays.toString(groups));
            hi = Double.valueOf(groups[4]);
            lo = Double.valueOf(groups[3]);
            varId = Integer.valueOf(groups[1].trim());
          }
          iv = new Interval(lo, hi);
          box[varId-1] = iv;
          currentVar++;
        } 
        
        if (currentVar == nVars) {
          boxes.add(box);
          currentVar = -1;
          box = null;
        }
        
  //      System.out.println(line);
        line = reader.readLine();
      }
      long end = System.nanoTime();
//      System.out.printf("[realpaver] execution+processing took %f miliseconds\n", (end - start)/1000000.0);
    }

    return boxes;
  }

  private static Process runRealPaver(String realPaverInput)
      throws IOException, InterruptedException {
    String currentPID = ManagementFactory.getRuntimeMXBean().getName();
    String filename = "/tmp/realpaverinput" + currentPID; 
    FileWriter fw = new FileWriter(filename);
    BufferedWriter bw = new BufferedWriter(fw);
    bw.write(realPaverInput); 
    bw.close();
    Process p = Runtime.getRuntime().exec(Config.realPaverLocation + " " + filename);
	//	System.out.println(p);
	//	System.out.println(Config.realPaverLocation + " " + filename);
    return p;
  }
  
  public static String generateInput(List<String> clauses, int timeout, int precision, int nboxes, List<VarData> varsData, boolean paving) {
    
    StringBuffer sb = new StringBuffer();
    //OPTIONS
    sb.append(Config.intervalSolverExtraArgs + "\n");
    sb.append("Time = ");
    sb.append(timeout);
    sb.append(";\n");
    sb.append("Branch \n");
    sb.append("precision = 1.0e-");
    sb.append(precision);
    sb.append(",\n number = ");
    sb.append(nboxes);
    sb.append(", \n mode = " + (paving ? "paving":"points") + "; \n Variables \n");
    
    for(VarData data : varsData) {
      if (data.isSlack) {
        sb.append("int $");
        sb.append(data.id);
        sb.append(" in ]-oo,+oo[,\n");
      } else {
        if (data.isInt) {
          sb.append("int ");
        }
        sb.append(data.id);
        sb.append(" in [");
        
        if(data.iv != null) {
          Interval iv = data.iv;
          sb.append(iv.lo().doubleValue());
          sb.append(',');
          sb.append(iv.hi().doubleValue());
        } else {
          sb.append(Config.RANGE.getLo());
          sb.append(',');
          sb.append(Config.RANGE.getHi());
        }
        sb.append("],\n");
      }
    }
    
    int pos = sb.length();
    sb.deleteCharAt(pos - 1); //remove last comma
    sb.deleteCharAt(pos - 2);
    sb.append("; \n Constraints \n");
    
    for(String clause : clauses) {
      sb.append(clause);
      sb.append(",\n");
    }
    
    pos = sb.length();
    sb.deleteCharAt(pos - 1); //remove last comma
    sb.deleteCharAt(pos - 2);
    
    sb.append(';');
    
    return sb.toString();
  }
  
  private RealPaverInput processConstraints(PC pc, Interval[] defaultInterval,
      int timeout, int precision, int nBoxes, boolean paving) {
    Map<Integer,Interval> box = new HashMap<Integer, Interval>();
    int i = 0;
    for (SymLiteral lit : pc.getSortedVars()) {
      box.put(lit.getId(), defaultInterval[i]);
      i++;
    }
    return processConstraints(pc, box, timeout, precision, nBoxes, paving);
  }
  
  private static class RealPaverInput {
    final int nvars;
    final int nUsedVars;
    final String realpaverInput;
    public RealPaverInput(int nvars, int nUsedVars, String realpaverInput) {
      this.nvars = nvars;
      this.nUsedVars = nUsedVars;
      this.realpaverInput = realpaverInput;
    }
  }
  
  public static class VarData {
    final String id;
    final boolean isInt;
    final Interval iv;
    final boolean isSlack;

    public VarData(int id, boolean isInt, Interval iv) {
      this("x" + id,isInt,iv,false);
    }
    
    public VarData(String id, boolean isInt, Interval iv, boolean isSlack) {
      this.id = id;
      this.isInt = isInt;
      this.iv = iv;
      this.isSlack = isSlack;
    }
  }
  
  //TODO remove this map and merge with the previous method
  private RealPaverInput processConstraints(PC pc, Map<Integer,Interval> box,  int timeout, int precision, int nboxes, boolean paving) {
    List<SymBool> coralCons = pc.splitANDs().getConstraints();
    List<SymBool> allowedCons = new ArrayList<SymBool>(coralCons.size());
    List<String> realPaverCons = new ArrayList<>(coralCons.size());
    PaverVisitor translator = new PaverVisitor(enableExperimentalRewriting);
        
    for(SymBool bool : coralCons) {
        String clause = translator.translate(bool);
        if (!clause.isEmpty()) {
          allowedCons.add(bool);
          realPaverCons.add(clause);
        }
    }
    List<String> tmp = new ArrayList<>();
    tmp.addAll(translator.getSlackCons());
    tmp.addAll(realPaverCons);
    realPaverCons = tmp;

    PC realPaverPC = new PC(allowedCons); //create a new PC without the clauses that contains unsupported operations
    Set<SymLiteral> realPaverVars = realPaverPC.getVars();
    List<VarData> varsData = Util.extractVarData(pc.getVars(),box);
    int nVars = varsData.size();
    for (PaverVisitor.PaverSlackVariable slackVar : translator.getSlackVars()) {
      VarData slackVarData = new VarData(slackVar.name,true, Interval.FULL_INT,true);
      varsData.add(slackVarData);
    }
    
    String realPaverInput = generateInput(realPaverCons, timeout, precision, nboxes, varsData, paving);
    return new RealPaverInput(nVars, realPaverVars.size(), realPaverInput);
  }
  
  public static void main(String args[]) throws ParseException, IOException, InterruptedException {
    PC pc = (new Parser("DEQ(DVAR(ID_1),ADD(DVAR(ID_2),DVAR(ID_4)));DGT(DVAR(ID_4),ADD(DVAR(ID_2),DVAR(ID_3)))")).parsePC();
//    Map<Integer,Interval> map = callRealPaver(pc,null, 3000, 3);
    List<Interval[]> boxes;
    boxes = new RealPaverCaller().getPaving(pc,null);
    for (Interval[] box : boxes) {
      System.out.println(Arrays.toString(box));
    }
    //Benchmark./*pc61*/pc81)
  }
}
