package coral.util.callers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import coral.PC;
import coral.util.Interval;

public interface IntervalSolverCaller {

  //TODO cache results; this method is being called from 2 different points of the program 
  public Map<Integer, Interval> callSolver(PC pc, int timeout, int precision) throws IOException, InterruptedException;

  public List<Interval[]> getPaving(PC pc, Interval[] defaultInterval) throws IOException, InterruptedException;
  
}
