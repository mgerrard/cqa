package coral.util;

public class Range {
  
  private int lo;
  private int hi;

  public Range(int lo, int hi) {
    if(lo > hi) {
      throw new RuntimeException("lo value has to be less than hi value.");
    }
    
    this.lo = lo;
    this.hi = hi;
  }
  
  public int getLo() {
    return lo;
  }
  public void setLo(int lo) {
    this.lo = lo;
  }
  public int getHi() {
    return hi;
  }
  public void setHi(int hi) {
    this.hi = hi;
  }
  
  public boolean isNumberInRange(int number) {
    return (number >= lo) && (number <= hi);
  }
  
  public boolean isNumberInRange(double number) {
    return (number >= lo) && (number <= hi);
  }
}