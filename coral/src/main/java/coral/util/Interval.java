package coral.util;

import java.util.*;

import com.google.common.base.Optional;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

public class Interval {

  public static final Interval NULL = new Interval(0.0, 0.0);
  public static final Interval FULL_INT = new Interval(Integer.MIN_VALUE, Integer.MAX_VALUE);

  public final Range<BigRational> range;

  private final Optional<Range<Integer>> intRange;

  public Interval(BigRational lo, BigRational hi) {
    range = Range.closed(lo, hi);
    intRange = toIntRange(range);
  }

  public Interval(double lo, double hi) {
    range = Range.closed(BigRational.valueOf(lo), BigRational.valueOf(hi));
    intRange = toIntRange(range);
  }

  public Interval(int lo, int hi) {
    range = Range.closed(BigRational.valueOf(lo), BigRational.valueOf(hi));
    intRange = toIntRange(range);
  }

  public BigRational getRationalLength() {
    return range.upperEndpoint().sub(range.lowerEndpoint());
  }

  public double getLength() {
    return range.upperEndpoint().sub(range.lowerEndpoint()).doubleValue();
  }

  public int getIntLength() {
    if (intRange.isPresent()) {
      return intRange.get().upperEndpoint() - intRange.get().lowerEndpoint();
    } else {
      return 0;
    }
  }

  public BigRational lo() {
    return range.lowerEndpoint();
  }

  public BigRational hi() {
    return range.upperEndpoint();
  }

  public int intLo() {
    if (intRange.isPresent()) {
      return intRange.get().lowerEndpoint();
    } else {
      throw new RuntimeException("No integers in this interval");
    }
  }

  public int intHi() {
    if (intRange.isPresent()) {
      return intRange.get().upperEndpoint() - 1; //upper endpoint is always open
    } else {
      throw new RuntimeException("No integers in this interval");
    }
  }

  private Double doubleHi = null;
  private Double doubleLo = null;

  public double doubleHi() {
    if (doubleHi == null) {
      doubleHi = range.upperEndpoint().doubleValue();
    }
    return doubleHi;
  }

  public double doubleLo() {
    if (doubleLo == null) {
      doubleLo = range.lowerEndpoint().doubleValue();
    }
    return doubleLo;
  }

  public static BigRational computeVolume(Interval[] ivs, Boolean[] isInt) {
    BigRational total = BigRational.ONE;
    for (int i = 0; i < ivs.length; i++) {
      if (ivs[i] != Interval.NULL) {
        boolean useIntLength = isInt[i];
        if (useIntLength) {
          total = total.mul(ivs[i].getIntLength());
        } else {
          total = total.mul(ivs[i].getRationalLength());
        }
      }
    }
    return total;
  }

  public static BigRational computeVolume(Collection<Interval> ivs, Collection<Boolean> isInt) {
    if (ivs.size() == 0) {
      return BigRational.ZERO;
    }

    BigRational total = BigRational.ONE;
    Iterator<Boolean> isIntIterator = isInt.iterator();
    for (Interval iv : ivs) {
      if (iv != Interval.NULL) {
        boolean useIntLength = isIntIterator.next();
        if (useIntLength) {
          total = total.mul(iv.getIntLength());
        } else {
          total = total.mul(iv.getRationalLength());
        }
      }
    }
    return total;
  }

  public static Interval[] mergeIntervals(List<Interval[]> boxes) {
    BigRational[] lows = new BigRational[boxes.get(0).length];
    BigRational[] highs = new BigRational[boxes.get(0).length];
    java.util.Arrays.fill(lows, BigRational.valueOf(Double.MAX_VALUE));
    java.util.Arrays.fill(highs, BigRational.valueOf(Double.MIN_VALUE));

    for (Interval[] ivs : boxes) {
      for (int i = 0; i < ivs.length; i++) {
        Interval iv = ivs[i];
        if (iv != null) {
          lows[i] = iv.lo().min(lows[i]);
          highs[i] = iv.hi().max(highs[i]);
        }
      }
    }
    Interval[] singleBox = new Interval[lows.length];
    for (int i = 0; i < lows.length; i++) {
      singleBox[i] = new Interval(lows[i], highs[i]);
    }
    return singleBox;
  }

  public Interval[] split(int nregions) {
    BigRational lo = range.lowerEndpoint();
    BigRational hi = range.upperEndpoint();
    BigRational splitSize = (hi.sub(lo).div(nregions));
    Interval[] splits = new Interval[nregions];
    for (int i = 0; i < nregions - 1; i++) {
      splits[i] = new Interval(lo.plus(splitSize.mul(i)), lo.plus(splitSize.mul(i + 1)));
    }
    splits[nregions - 1] = new Interval(lo.plus(splitSize.mul(nregions - 1)), hi);
    return splits;
  }

  public Optional<Range<Integer>> toIntRange(Range<BigRational> arg0) {
    // bounds for integer interval: ceiling(lo),floor(hi)
    int lo = arg0.lowerEndpoint().ceiling().intValue();
    int hi = arg0.upperEndpoint().floor().intValue();

    if (lo > hi) { //return open range of length 0
      return Optional.absent();
    } else {
      return Optional.of(Range.<Integer>closed(lo, hi).canonical(DiscreteDomain.integers()));
    }
  }

  public static List<Interval[]> filterIdenticalBoxes(Collection<Interval[]> boxes) {
    List<Interval[]> filtered = new ArrayList<>();
    Set<Long> hashes = new TreeSet<>();
    for (Interval[] box : boxes) {
      Hasher hasher = Hashing.goodFastHash(64).newHasher();
      for (Interval iv : box) {
        if (iv == null) {
          hasher.putInt(-1);
        } else {
          hasher.putInt(iv.hashCode());
        }
      }
      HashCode hash = hasher.hash();
      if (hashes.add(hash.asLong())) {
        filtered.add(box);
      }
    }
    //System.out.println("Filtered " + (boxes.size() - filtered.size()) + " identical boxes");
    return ImmutableList.copyOf(filtered);
  }

  public String toString() {
    return range.lowerEndpoint().doubleValue() + " : " + range.upperEndpoint().doubleValue();// + " // " + intRange;
  }

  public Optional<Range<Integer>> getIntRange() {
    return intRange;
  }

  public int hashCode() {
    return range.hashCode();
  }
}
