package coral.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import symlib.SymBool;
import symlib.parser.Parser;
import coral.PC;
import coral.util.visitors.CoralVisitor;

public class Reverser {

  public static void main(String[] args) throws Exception {
    Scanner s = new Scanner(new File(args[0]));
    
    while (s.hasNextLine()) {
      String line = s.nextLine();
      PC pc = (new Parser(line)).parsePC();
      
      //reverse and canonicalize
      List<SymBool> cons = pc.getConstraints();
      Collections.reverse(cons);
      
      System.out.println(CoralVisitor.processPC(pc));
    }
  }
}
