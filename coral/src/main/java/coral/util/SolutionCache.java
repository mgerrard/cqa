package coral.util;

import java.util.HashMap;
import java.util.Map;

import org.opt4j.core.Individual;

import coral.solvers.Env;
import coral.util.trees.Trie;

public class SolutionCache {
  
  //TODO make this more generic
  
  private Trie<String,PCSolutions<Individual>> trie;
  private PCSolutions<Individual> lastSolution;
  private Map<String,Env> solvedConstraints;
  
  
  public SolutionCache() {
    this.trie = new Trie<String,PCSolutions<Individual>>();
    lastSolution = null;
    solvedConstraints = new HashMap<String, Env>(10000);
  }
  
  public Map<String,Env> getCacheMap() {
    return solvedConstraints;
  }
  
  public Trie<String,PCSolutions<Individual>> getTrie() {
    return trie;
  }

  public void setLastSolution(PCSolutions<Individual> solution) {
    this.lastSolution = solution;
  }
  
  public PCSolutions<Individual> getLastSolution() {
    return lastSolution;
  }
  
  public void cacheSolvedConstraint(String constraint, Env solution) {
    solvedConstraints.put(constraint, solution);
  }
  
  public Env queryCache(String constraint) {
    return solvedConstraints.get(constraint);
  }
  
  public int cacheSize() {
    return solvedConstraints.size();
  }
}
