package coral.util.visitors;

import java.util.HashMap;
import java.util.Map;

import symlib.SymBool;
import symlib.SymBoolLiteral;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleLiteral;
import symlib.SymFloatLiteral;
import symlib.SymIntLiteral;
import symlib.SymLiteral;
import symlib.SymLongLiteral;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import coral.PC;
import coral.solvers.Unit;
import coral.util.Config;

public class SymLiteralUnitSearcher extends SymLiteralSearcher {

  private Map<SymLiteral, Unit> vars;

  public SymLiteralUnitSearcher() {
    vars = new HashMap<SymLiteral, Unit>();
  }
  
  public Map<SymLiteral, Unit> getUnits() {
    return vars;
  }

  public SymLiteralUnitSearcher(PC pc) {
    this();
    for (SymBool constraint : pc.getConstraints()) {
      constraint.accept(this);  
    }
  }
  

  
  enum OP_KIND {UNARY, BINARY};
  OP_KIND typeContext;
  int unitContext;
  @Override
  public void visitSymDouble(SymDouble number) {
    if (number instanceof SymDoubleLiteral) {
      addDouble((SymDoubleLiteral)number);
    } else if (number instanceof SymDoubleArith) {
      SymDoubleArith arith = (SymDoubleArith) number;
      visitSymDouble(arith.getA());
      visitSymDouble(arith.getB());
    } else if (number instanceof SymMathUnary) {
      SymMathUnary jmath = (SymMathUnary) number;
      int tmp = unitContext;
      OP_KIND tmp2 = typeContext;
      unitContext = jmath.getOp();      
      typeContext = OP_KIND.UNARY;
      visitSymDouble(jmath.getArg());
      unitContext = tmp;
      typeContext = tmp2;
    } else if (number instanceof SymMathBinary) {
      SymMathBinary jmath = (SymMathBinary) number;
      int tmp = unitContext;
      OP_KIND tmp2 = typeContext;
      unitContext = jmath.getOp();
      typeContext = OP_KIND.BINARY;
      visitSymDouble(jmath.getArg1());
      visitSymDouble(jmath.getArg2());
      unitContext = tmp;
      typeContext = tmp2;
    }
  }

  @Override
  protected void addBoolean(SymBoolLiteral bool) {
    vars.put(bool, Unit.LIMITED_BOOLEAN);
  }
  
  @Override
  protected void addInt(SymIntLiteral number) {
    vars.put(number, Unit.LIMITED_INT);
  }
  
  @Override
  protected void addFloat(SymFloatLiteral number) {
    vars.put(number, Unit.LIMITED_FLOAT);
  }
  
  @Override
  protected void addLong(SymLongLiteral number) {
    vars.put(number, Unit.LIMITED_INT);
  }
  
  @Override
  protected void addDouble(SymDoubleLiteral key) {
    Unit unit = getExpectedUnit();
    Unit old = vars.get(key);
    if (old == null) {      
      vars.put(key, unit);  
    } else if (unit != old) {
//      throw new RuntimeException("different inferred units for " + key + "..." + old + " (old) and " + unit);
    }
  }
  
  /**
   * checks function under current context
   * and infer the unit for this variable 
   * accordingly  
   * 
   * @return
   */
  private Unit getExpectedUnit() {
    Unit result = Unit.LIMITED_DOUBLE;
    int op = unitContext;
    if (Config.toggleValueInference) {
      if (typeContext == OP_KIND.UNARY) {
        if (op >= 0 && op <= 2) {
          result = Unit.RADIANS;
        } else if (op >= 3 && op <= 4) {
          result = Unit.ASIN_ACOS;
        } else if (op == 5) {
          result = Unit.ATAN;
        } else if (op >= 6 && op <= 10) {
          result = Unit.DOUBLE;
        } else {
          throw new RuntimeException("what unit is this?");
        }
      } else if (typeContext == OP_KIND.BINARY) {
        switch (op) {
        case 0:/* atan2 */
          result = Unit.LIMITED_DOUBLE;
          break;
        case 1:/* pow */
          result = Unit.LIMITED_DOUBLE;
          break;
        default:
          throw new RuntimeException("what unit is this?");
        }
      }
    }
    return result;
  }
 
}