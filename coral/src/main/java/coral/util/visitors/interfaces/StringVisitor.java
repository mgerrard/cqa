package coral.util.visitors.interfaces;

import symlib.FrozenSymBool;
import symlib.SymAsDouble;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLong;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;

public abstract class StringVisitor {

  public String visitSymNumber(SymNumber symNumber) {
    String result;
    if (symNumber instanceof SymCast) {
      result = visitSymCast((SymCast) symNumber);
    } else if (symNumber instanceof SymInt) {
      result = visitSymInt((SymInt) symNumber);
    } else if (symNumber instanceof SymDouble) {
      result = visitSymDouble((SymDouble) symNumber);
    } else if (symNumber instanceof SymFloat) {
      result = visitSymFloat((SymFloat) symNumber);
    } else if (symNumber instanceof SymLong) {
      result = visitSymLong((SymLong) symNumber);
    } else {
      throw new UnsupportedOperationException("missing case: " + symNumber.getClass());
    }
    
    return result;
  }
  
  public String visitSymBool(SymBool symBool) {
    String result;
    if (symBool instanceof FrozenSymBool) {
      result = ((FrozenSymBool)symBool).accept(this);
    } else if (symBool instanceof SymBoolConstant) {
      result = visitSymConstant((SymBoolConstant) symBool);
    } else if (symBool instanceof SymIntRelational) {
      result = visitSymRel((SymIntRelational) symBool);
    } else if (symBool instanceof SymLongRelational) {
      result = visitSymRel((SymLongRelational) symBool);
    } else if (symBool instanceof SymFloatRelational) {
      result = visitSymRel((SymFloatRelational) symBool);
    } else if (symBool instanceof SymDoubleRelational) {
      result = visitSymRel((SymDoubleRelational) symBool);
    } else if (symBool instanceof SymBoolOperations) {
      result = visitSymBoolOperations((SymBoolOperations) symBool);
    } else if (symBool instanceof SymBoolRelational) {
      result = visitSymBoolRelational((SymBoolRelational) symBool);
    } else {
      throw new UnsupportedOperationException("missing case: " + symBool.getClass());
    }
    return result;
  }

  public String visitSymInt(SymInt symInt) {
    String result;
    if (symInt instanceof SymIntConstant) {
      result = visitSymConstant((SymIntConstant) symInt);
    } else if (symInt instanceof SymIntLiteral) {
      result = visitSymIntLiteral((SymIntLiteral) symInt);
    } else if (symInt instanceof SymIntArith) {
      result = visitSymIntArith((SymIntArith) symInt);
    } else {
      throw new UnsupportedOperationException("missing case: " + symInt.getClass());
    }
    return result;
  }

  public String visitSymDouble(SymDouble symDouble) {
    String result;
    if (symDouble instanceof SymDoubleConstant) {
      result = visitSymDoubleConst((SymDoubleConstant) symDouble);
    } else if (symDouble instanceof SymDoubleLiteral) {
      result = visitSymDoubleLiteral((SymDoubleLiteral) symDouble);
    } else if (symDouble instanceof SymDoubleArith) {
      result = visitSymDoubleArith((SymDoubleArith) symDouble);
    } else if (symDouble instanceof SymMathUnary) {
      result = visitSymMathUnary((SymMathUnary) symDouble);
    } else if (symDouble instanceof SymMathBinary) {
      result = visitSymMathBinary((SymMathBinary) symDouble);
    } else {
      throw new UnsupportedOperationException("missing case: " + symDouble.getClass());
    }
    return result;
  }

  public String visitSymFloat(SymFloat number) {
    String result;
    if (number instanceof SymFloatConstant) {
      result = visitSymFloatConst((SymFloatConstant) number);
    } else if (number instanceof SymFloatLiteral) {
      result = visitSymFloatLiteral((SymFloatLiteral) number);
    } else if (number instanceof SymFloatArith) {
      result = visitSymArith((SymFloatArith) number);
    } else {
      throw new UnsupportedOperationException("missing case: " + number.getClass());
    }
    return result;
  }

  public String visitSymLong(SymLong number) {
    String result;
    if (number instanceof SymLongConstant) {
      result = visitSymLongConst((SymLongConstant) number);
    } else if (number instanceof SymLongLiteral) {
      result = visitSymLongLiteral((SymLongLiteral) number);
    } else {
      throw new UnsupportedOperationException("missing case: " + number.getClass());
    }
    return result;
  }

  abstract protected String visitSymConstant(SymBoolConstant arg);

  abstract protected String visitSymRel(SymIntRelational arg);

  abstract protected String visitSymRel(SymLongRelational arg);

  abstract protected String visitSymRel(SymFloatRelational arg);

  abstract protected String visitSymRel(SymDoubleRelational arg);

  abstract protected String visitSymConstant(SymIntConstant arg);

  abstract protected String visitSymIntLiteral(SymIntLiteral arg);

  abstract protected String visitSymIntArith(SymIntArith arg);

  abstract protected String visitSymDoubleConst(SymDoubleConstant arg);

  abstract protected String visitSymDoubleLiteral(SymDoubleLiteral arg);
  
  abstract protected String visitSymDoubleArith(SymDoubleArith arg);

  abstract protected String visitSymFloatConst(SymFloatConstant arg);

  abstract protected String visitSymFloatLiteral(SymFloatLiteral arg);

  abstract protected String visitSymArith(SymFloatArith arg);

  abstract protected String visitSymLongConst(SymLongConstant arg);

  abstract protected String visitSymLongLiteral(SymLongLiteral arg);

  abstract protected String visitSymBoolOperations(SymBoolOperations arg);

  abstract protected String visitSymBoolRelational(SymBoolRelational arg);

  abstract protected String visitSymMathUnary(SymMathUnary symDoubleJavaMath);
  
  abstract protected String visitSymMathBinary(SymMathBinary symDouble);

  abstract protected String visitSymCast(SymCast symCast);
}
