package coral.util.visitors.interfaces;

import symlib.FrozenSymBool;
import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLong;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;

public abstract class GenericVisitor<T> {

  public T visitSymNumber(SymNumber symNumber) {
    T result;

    if (symNumber instanceof SymInt) {
      result = visitSymInt((SymInt) symNumber);
    } else if (symNumber instanceof SymDouble) {
      result = visitSymDouble((SymDouble) symNumber);
    } else if (symNumber instanceof SymFloat) {
      result = visitSymFloat((SymFloat) symNumber);
    } else if (symNumber instanceof SymLong) {
      result = visitSymLong((SymLong) symNumber);
    } else {
      throw new UnsupportedOperationException(
          "missing case: " + symNumber.getClass());
    }

    return result;
  }

  public T visitSymBool(SymBool symBool) {
    T result;
    if (symBool instanceof FrozenSymBool) {
      result = ((FrozenSymBool) symBool).accept(this);
    } else if (symBool instanceof SymBoolConstant) {
      result = visitSymConstant((SymBoolConstant) symBool);
    } else if (symBool instanceof SymIntRelational) {
      result = visitSymRel((SymIntRelational) symBool);
    } else if (symBool instanceof SymLongRelational) {
      result = visitSymRel((SymLongRelational) symBool);
    } else if (symBool instanceof SymFloatRelational) {
      result = visitSymRel((SymFloatRelational) symBool);
    } else if (symBool instanceof SymDoubleRelational) {
      result = visitSymRel((SymDoubleRelational) symBool);
    } else if (symBool instanceof SymBoolOperations) {
      result = visitSymBoolOperations((SymBoolOperations) symBool);
    } else if (symBool instanceof SymBoolRelational) {
      result = visitSymBoolRelational((SymBoolRelational) symBool);
    } else {
      throw new UnsupportedOperationException(
          "missing case: " + symBool.getClass());
    }
    return result;
  }

  public T visitSymInt(SymInt symInt) {
    T result;
    if (symInt instanceof SymIntConstant) {
      result = visitSymConstant((SymIntConstant) symInt);
    } else if (symInt instanceof SymIntLiteral) {
      result = visitSymIntLiteral((SymIntLiteral) symInt);
    } else if (symInt instanceof SymIntArith) {
      result = visitSymIntArith((SymIntArith) symInt);
    } else if (symInt instanceof SymAsInt) {
      result = visitSymAsInt((SymAsInt) symInt);
    } else {
      throw new UnsupportedOperationException(
          "missing case: " + symInt.getClass());
    }
    return result;
  }

  public T visitSymDouble(SymDouble symDouble) {
    T result;
    if (symDouble instanceof SymDoubleConstant) {
      result = visitSymDoubleConst((SymDoubleConstant) symDouble);
    } else if (symDouble instanceof SymDoubleLiteral) {
      result = visitSymDoubleLiteral((SymDoubleLiteral) symDouble);
    } else if (symDouble instanceof SymDoubleArith) {
      result = visitSymDoubleArith((SymDoubleArith) symDouble);
    } else if (symDouble instanceof SymMathUnary) {
      result = visitSymMathUnary((SymMathUnary) symDouble);
    } else if (symDouble instanceof SymMathBinary) {
      result = visitSymMathBinary((SymMathBinary) symDouble);
    } else if (symDouble instanceof SymAsDouble) {
      result = visitSymAsDouble((SymAsDouble) symDouble);
    }else {
      throw new UnsupportedOperationException(
          "missing case: " + symDouble.getClass());
    }
    return result;
  }

  public T visitSymFloat(SymFloat number) {
    T result;
    if (number instanceof SymFloatConstant) {
      result = visitSymFloatConst((SymFloatConstant) number);
    } else if (number instanceof SymFloatLiteral) {
      result = visitSymFloatLiteral((SymFloatLiteral) number);
    } else if (number instanceof SymFloatArith) {
      result = visitSymArith((SymFloatArith) number);
    } else {
      throw new UnsupportedOperationException(
          "missing case: " + number.getClass());
    }
    return result;
  }

  public T visitSymLong(SymLong number) {
    T result;
    if (number instanceof SymLongConstant) {
      result = visitSymLongConst((SymLongConstant) number);
    } else if (number instanceof SymLongLiteral) {
      result = visitSymLongLiteral((SymLongLiteral) number);
    } else {
      throw new UnsupportedOperationException(
          "missing case: " + number.getClass());
    }
    return result;
  }

  abstract protected T visitSymConstant(SymBoolConstant arg);

  abstract protected T visitSymRel(SymIntRelational arg);

  abstract protected T visitSymRel(SymLongRelational arg);

  abstract protected T visitSymRel(SymFloatRelational arg);

  abstract protected T visitSymRel(SymDoubleRelational arg);

  abstract protected T visitSymConstant(SymIntConstant arg);

  abstract protected T visitSymIntLiteral(SymIntLiteral arg);

  abstract protected T visitSymIntArith(SymIntArith arg);

  abstract protected T visitSymDoubleConst(SymDoubleConstant arg);

  abstract protected T visitSymDoubleLiteral(SymDoubleLiteral arg);

  abstract protected T visitSymDoubleArith(SymDoubleArith arg);

  abstract protected T visitSymFloatConst(SymFloatConstant arg);

  abstract protected T visitSymFloatLiteral(SymFloatLiteral arg);

  abstract protected T visitSymArith(SymFloatArith arg);

  abstract protected T visitSymLongConst(SymLongConstant arg);

  abstract protected T visitSymLongLiteral(SymLongLiteral arg);

  abstract protected T visitSymBoolOperations(SymBoolOperations arg);

  abstract protected T visitSymBoolRelational(SymBoolRelational arg);

  abstract protected T visitSymMathUnary(SymMathUnary symDoubleJavaMath);

  abstract protected T visitSymMathBinary(SymMathBinary symDouble);

  abstract protected T visitSymAsDouble(SymAsDouble sym);

  abstract protected T visitSymAsInt(SymAsInt sym);

}
