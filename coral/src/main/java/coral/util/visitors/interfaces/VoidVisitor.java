package coral.util.visitors.interfaces;

import symlib.SymBool;
import symlib.SymNumber;

public interface VoidVisitor {

  void visitSymBool(SymBool bool);
  void visitSymNumber(SymNumber number);
  
}
