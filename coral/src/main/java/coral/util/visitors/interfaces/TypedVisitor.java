package coral.util.visitors.interfaces;

import symlib.SymBool;
import symlib.SymNumber;

public interface TypedVisitor {
  
  SymBool visitSymBool(SymBool bool);
  SymNumber visitSymNumber(SymNumber number);
  
}
