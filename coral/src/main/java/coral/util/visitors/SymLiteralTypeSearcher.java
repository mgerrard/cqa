package coral.util.visitors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import symlib.SymBool;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymConstant;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloatLiteral;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLongLiteral;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.solvers.Type;
import coral.solvers.Unit;
import coral.tests.Benchmark;
import coral.util.Config;

public class SymLiteralTypeSearcher extends SymLiteralSearcher {

  private boolean flexibleRange = false; //allow interval to be bigger than the one defined in Config
  private Map<SymLiteral, Type> vars;
  private Set<SymLiteral> changedHI; //
  private Set<SymLiteral> changedLO;

  public SymLiteralTypeSearcher(boolean flexibleRange) {
    this.flexibleRange = flexibleRange;
    vars = new HashMap<SymLiteral, Type>();
    changedHI = new HashSet<SymLiteral>(vars.size()*2);
    changedLO = new HashSet<SymLiteral>(vars.size()*2);
  }
  
  public Map<SymLiteral, Type> getUnits() {
    return vars;
  }

  public SymLiteralTypeSearcher(PC pc,boolean flexibleRange) {
    this(flexibleRange);
    for (SymBool constraint : pc.getConstraints()) {
      constraint.accept(this);  
    }
  }
  
  public void visitSymBool(SymBool bool) {
    
    if(bool instanceof SymBoolOperations) {
      
      SymBoolOperations rel = (SymBoolOperations) bool;
      int op = rel.getOp(); 
      if(op != SymBoolOperations.OR && op != SymBoolOperations.XOR) {
        super.visitSymBool(bool);
      }
      
    } else  if (bool instanceof SymDoubleRelational) {
      
      SymNumber num1 = ((SymDoubleRelational) bool).getA();
      SymNumber num2 = ((SymDoubleRelational) bool).getB();
      int op = ((SymDoubleRelational) bool).getOp();
      visitSymRelational(num1, num2, op);
      
    } else if (bool instanceof SymIntRelational) {

      SymNumber num1 = ((SymIntRelational) bool).getA();
      SymNumber num2 = ((SymIntRelational) bool).getB();
      int op = ((SymIntRelational) bool).getOp();
      visitSymRelational(num1, num2, op);
      
    } else {
      super.visitSymBool(bool);
    } 
     
  }
  
  protected void visitSymRelational(SymNumber n1, SymNumber n2, int op) {
    n1.accept(this);
    n2.accept(this);
    if (n1 instanceof SymLiteral && n2 instanceof SymConstant) {
      SymLiteral lit = (SymLiteral) n1;
      double tmpNumber = ((SymNumber) n2).evalNumber().doubleValue();
//      int limit = ((SymNumber) n2).evalNumber().intValue();
      int limit;
      if(tmpNumber > 0) {
        limit = (int) Math.ceil(tmpNumber);
      } else {
        limit = (int) Math.floor(tmpNumber);
      }
      switch (op) {
      case 0 : // GT:  a > c 
        updateLO(lit, limit);
        break;
      case 1 : // LT:  a < c 
        updateHI(lit, limit);
        break;
      case 2 : // LE:  a <= c 
        updateHI(lit, limit);
        break;
      case 3 : // LE:  a >= c 
        updateLO(lit, limit);
        break;
      case 4 : // LE:  a == c 
        updateLO(lit, limit);
        updateHI(lit, limit);
        break;
      default :
        break;
      }
    } else if (n1 instanceof SymConstant && n2 instanceof SymLiteral) {
      SymLiteral lit = (SymLiteral) n2;
      int limit = ((SymNumber) n1).evalNumber().intValue();
      switch (op) {
      case 0 : // GT:  c > a 
        updateHI(lit, limit);
        break;
      case 1 : // LT:  c < a 
        updateLO(lit, limit);
        break;
      case 2 : // LE:  c <= a 
        updateLO(lit, limit);
        break;
      case 3 : // LE:  c >= a 
        updateHI(lit, limit);
        break;
      case 4 : // LE:  c == a 
        updateLO(lit, limit);
        updateHI(lit, limit);
        break;
      default :
        break;
      }
    }
  }
  
  enum OP_KIND {UNARY, BINARY};
  OP_KIND typeContext;
  int unitContext;
  @Override
  public void visitSymDouble(SymDouble number) {
    if (number instanceof SymDoubleLiteral) {
      addDouble((SymDoubleLiteral)number);
    } else if (number instanceof SymDoubleArith) {
      SymDoubleArith arith = (SymDoubleArith) number;
      visitSymDouble(arith.getA());
      visitSymDouble(arith.getB());
    } else if (number instanceof SymMathUnary) {
      SymMathUnary jmath = (SymMathUnary) number;
      int tmp = unitContext;
      OP_KIND tmp2 = typeContext;
      unitContext = jmath.getOp();      
      typeContext = OP_KIND.UNARY;
      visitSymDouble(jmath.getArg());
      unitContext = tmp;
      typeContext = tmp2;
    } else if (number instanceof SymMathBinary) {
      SymMathBinary jmath = (SymMathBinary) number;
      int tmp = unitContext;
      OP_KIND tmp2 = typeContext;
      unitContext = jmath.getOp();
      typeContext = OP_KIND.BINARY;
      visitSymDouble(jmath.getArg1());
      visitSymDouble(jmath.getArg2());
      unitContext = tmp;
      typeContext = tmp2;
    }
  }

  @Override
  protected void addBoolean(SymBoolLiteral bool) {
    updateType(bool, Unit.LIMITED_BOOLEAN);
  }
  
  @Override
  protected void addInt(SymIntLiteral number) {
    updateType(number, Unit.LIMITED_INT);
  }
  
  @Override
  protected void addFloat(SymFloatLiteral number) {
    updateType(number, Unit.LIMITED_FLOAT);
  }
  
  @Override
  protected void addLong(SymLongLiteral number) {
    updateType(number, Unit.LIMITED_INT);
  }
  
  @Override
  protected void addDouble(SymDoubleLiteral key) {
    updateType(key, getExpectedUnit());    
  }
  
  private Type createType(Unit unit) {
    return new Type(unit, Config.RANGE.getLo(), Config.RANGE.getHi());
  }
  

  private void updateHI(SymLiteral var, int hi) {
    Type t1 = vars.get(var);
    if (t1 == null) {
      Unit u = var instanceof SymIntLiteral ? Unit.LIMITED_INT : Unit.LIMITED_DOUBLE;
      t1 = new Type(u, Config.RANGE.getLo(), hi);
      vars.put(var, t1);

    } else if (flexibleRange) {
      boolean changed = changedHI.contains(var);
      if (changed) { //already changed default limit, let's check if this one is better
        if (hi < t1.getHi()) {
          t1.setHi(hi);
        }
      } else { //new limit detected - this one will override the default
        changedHI.add(var);
        t1.setHi(hi);
      }
    
    } else { //old behaviour
      if (hi < t1.getHi() && hi > t1.getLo()) {
        t1.setHi(hi);
      }
    }
    int lo = t1.getLo();
    if ((lo == Config.RANGE.getLo()) && lo >= t1.getHi()) {
      t1.setLo(hi - (Config.RANGE.getHi() - lo));
    }
  }
  
  private void updateLO(SymLiteral var, int lo) {
    Type t1 = vars.get(var);
    if (t1 == null) {
      Unit u = var instanceof SymIntLiteral ? Unit.LIMITED_INT : Unit.LIMITED_DOUBLE;
      t1 = new Type(u, lo, Config.RANGE.getHi());
      vars.put(var, t1);
      
    } else if (flexibleRange){
      boolean changed = changedLO.contains(var);
      if (changed) { //already changed default limit, let's check if this one is better
        if (lo < t1.getLo()) {
          t1.setLo(lo);
        }
      } else { //new limit detected - this one will override the default
        changedLO.add(var);
        t1.setLo(lo);
      }
      
    } else {    
      if (lo > t1.getLo() && lo < t1.getHi()) {
        t1.setLo(lo);
      }
    }
    int hi = t1.getHi();
    if ((hi == Config.RANGE.getHi()) && hi <= t1.getLo()) {
      t1.setHi(lo + (hi - Config.RANGE.getLo()));
    }
    
  }
  
  private void updateType(SymLiteral var, Unit unit) {
    Type t1 = vars.get(var);
    Type t2 = createType(unit);
    if (t1 == null) {
      vars.put(var, t2);
    } else if (t1.getUnit() != t2.getUnit()) {
      choose(var, t1, t2);
    }
  }
  
  private void choose(SymLiteral lit, Type t1/*from the map*/, Type t2) {
    Unit unit1 = t1.getUnit();
    Unit unit2 = t2.getUnit();
    if (unit2.compareTo(unit1) < 1) {
      // unit 2 has a smaller range.  
      // you should change!
      vars.put(lit, t2);
    }
  }
  
  /**
   * checks function under current context
   * and infer the unit for this variable 
   * accordingly  
   * 
   * @return
   */
  private Unit getExpectedUnit() {
    Unit result = Unit.LIMITED_DOUBLE;
    int op = unitContext;
    if (Config.toggleValueInference) {
      if (typeContext == OP_KIND.UNARY) {
        if (op >= 0 && op <= 2) {
          result = Unit.RADIANS;
        } else if (op >= 3 && op <= 4) {
          result = Unit.ASIN_ACOS;
        } else if (op == 5) {
          result = Unit.ATAN;
        } else if (op >= 6 && op <= 10) {
          result = Unit.DOUBLE;
        } else {
          throw new RuntimeException("what unit is this?");
        }
      } else if (typeContext == OP_KIND.BINARY) {
        switch (op) {
        case 0:/* atan2 */
          result = Unit.LIMITED_DOUBLE;
          break;
        case 1:/* pow */
          result = Unit.LIMITED_DOUBLE;
          break;
        default:
          throw new RuntimeException("what unit is this?");
        }
      }
    }
    return result;
  }
  
  public static void main(String[] args) throws ParseException {
    Parser p = new Parser(Benchmark.pc85);
    PC pc = p.parsePC();
    SymLiteralTypeSearcher tSearcher = new SymLiteralTypeSearcher(pc,false);
    System.out.println(tSearcher.getUnits());
  }
}