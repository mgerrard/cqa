package coral.util.visitors;

import java.util.HashSet;
import java.util.Set;

import coral.util.visitors.interfaces.VoidVisitor;

import symlib.SymAsDouble;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;

public class SymLiteralSearcher implements VoidVisitor {

  private Set<SymIntLiteral> intVars;
  private Set<SymLongLiteral> longVars;
  private Set<SymFloatLiteral> floatVars;
  private Set<SymDoubleLiteral> doubleVars;
  private Set<SymBoolLiteral> booleanVars;

  public SymLiteralSearcher() {
    intVars = new HashSet<SymIntLiteral>();
    longVars = new HashSet<SymLongLiteral>();
    floatVars = new HashSet<SymFloatLiteral>();
    doubleVars = new HashSet<SymDoubleLiteral>();
    booleanVars = new HashSet<SymBoolLiteral>();
  }

  public SymLiteralSearcher(SymBool bool) {
    this();
    bool.accept(this);
  }

  /*
   * BoolBinary - verifies arg1 e arg2 Rel - verifies arg1 e arg2 BoolNot -
   * verifies arg BoolConst - do nothing
   */
  public void visitSymBool(SymBool bool) {
    if (bool instanceof SymIntRelational) {
      visitSymRelational((SymIntRelational) bool);
    } else if (bool instanceof SymFloatRelational) {
      visitSymRelational((SymFloatRelational) bool);
    } else if (bool instanceof SymDoubleRelational) {
      visitSymRelational((SymDoubleRelational) bool);
    } else if (bool instanceof SymLongRelational) {
      visitSymRelational((SymLongRelational) bool);
    } else if (bool instanceof SymBoolOperations) {
      visitSymBoolOperations((SymBoolOperations) bool);
    } else if (bool instanceof SymBoolConstant) {
      return;
    } else if (bool instanceof SymBoolLiteral) {
      addBoolean((SymBoolLiteral)bool);
    } else if (bool instanceof SymBoolRelational) {
      visitSymRelational((SymBoolRelational) bool);
    } else {
      throw new RuntimeException("missing case: " + bool.getClass());
    }
  }


  protected void visitSymRelational(SymBoolRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  private void visitSymBoolOperations(SymBoolOperations bool) {
    bool.getA().accept(this);
    if (bool.getB() != null) {
      bool.getB().accept(this);
    }
  }

  protected void visitSymRelational(SymIntRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  private void visitSymRelational(SymLongRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  private void visitSymRelational(SymFloatRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  protected void visitSymRelational(SymDoubleRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  /*
   * Arith - verifies operand1 e operand2 Int - if it is literal, adds itself
   * on list Float - if it is literal, adds itself on list.
   */
  public void visitSymNumber(SymNumber number) {
    if (number instanceof SymInt) {
      visitSymInt((SymInt) number);
    } else if (number instanceof SymFloat) {
      visitSymFloat((SymFloat) number);
    } else if (number instanceof SymLong) {
      visitSymLong((SymLong) number);
    } else if (number instanceof SymDouble) {
      visitSymDouble((SymDouble) number);
    } else {
      throw new RuntimeException("invalid type" + number.getClass());
    }
  }

  private void visitSymInt(SymInt number) {
    if (number instanceof SymIntLiteral) {
      addInt((SymIntLiteral)number);
    } else if (number instanceof SymIntArith) {
      SymIntArith arith = (SymIntArith) number;
      visitSymInt(arith.getA());
      visitSymInt(arith.getB());
    }
  }

  private void visitSymFloat(SymFloat number) {
    if (number instanceof SymFloatLiteral) {
      addFloat((SymFloatLiteral)number);
    } else if (number instanceof SymFloatArith) {
      SymFloatArith arith = (SymFloatArith) number;
      visitSymFloat(arith.getA());
      visitSymFloat(arith.getB());
    }
  }

  private void visitSymLong(SymLong number) {
    if (number instanceof SymLongLiteral) {
      addLong((SymLongLiteral)number);
    } else if (number instanceof SymLongArith) {
      SymLongArith arith = (SymLongArith) number;
      visitSymLong(arith.getA());
      visitSymLong(arith.getB());
    }
  }
  
  public void visitSymDouble(SymDouble number) {
    if (number instanceof SymDoubleLiteral) {
      addDouble((SymDoubleLiteral)number);
    } else if (number instanceof SymDoubleArith) {
      SymDoubleArith arith = (SymDoubleArith) number;
      visitSymDouble(arith.getA());
      visitSymDouble(arith.getB());
    } else if (number instanceof SymMathUnary) {
      SymMathUnary symMathUn = (SymMathUnary) number;
      visitSymDouble(symMathUn.getArg());
    } else if (number instanceof SymMathBinary) {
      SymMathBinary symMathBin = (SymMathBinary) number;
      visitSymDouble(symMathBin.getArg1());
      visitSymDouble(symMathBin.getArg2());
    } else if (number instanceof SymDoubleConstant) {      
    } else if (number instanceof SymCast) {
      visitSymNumber(((SymCast)number).getArg());
    } else {
      throw new UnsupportedOperationException(number.getClass()+"");
    }
  }


  public Set<SymIntLiteral> getIntVars() {
    return intVars;
  }

  public Set<SymFloatLiteral> getFloatVars() {
    return floatVars;
  }

  public Set<SymBoolLiteral> getBooleanVars() {
    return booleanVars;
  }
  
  public Set<SymDoubleLiteral> getDoubleVars() {
    return doubleVars;
  }
  
  protected void addBoolean(SymBoolLiteral bool) {
    booleanVars.add(bool);
  }
  protected void addInt(SymIntLiteral number) {
    intVars.add(number);
  }
  protected void addFloat(SymFloatLiteral number) {
    floatVars.add(number);
  }
  protected void addLong(SymLongLiteral number) {
    longVars.add(number);
  }
  protected void addDouble(SymDoubleLiteral number) {
    doubleVars.add(number);
  }

  public Set<SymLiteral> getVars() {
    Set<SymLiteral> allVars = new HashSet<SymLiteral>();

    allVars.addAll(intVars);
    allVars.addAll(longVars);
    allVars.addAll(floatVars);
    allVars.addAll(doubleVars);
    allVars.addAll(booleanVars);

    return allVars;
  }
}