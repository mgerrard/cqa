package coral.util.visitors;

import java.util.HashMap;
import java.util.Map;

import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.SolverKind;
import coral.solvers.Unit;
import coral.util.visitors.interfaces.TypedVisitor;

import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import symlib.Util;
import symlib.parser.ParseException;
import symlib.parser.Parser;

public class SymLiteralReplacer implements TypedVisitor {

  private Env env;

  public SymLiteralReplacer(Env env) {
    this.env = env;
  }

  /*
   * BoolBinary - cria um BoolBinary com o mesmo operador e com o arg1.accept e
   * arg2.accept BoolConst - retorna o msm BoolConst BoolNot - retorna o BoolNot
   * com o arg.accept Rel - substitui vars e retorna um Rel com o msm operador e
   * com o arg1 e arg2 substituidos
   */
  public SymBool visitSymBool(SymBool bool) {
    if (bool instanceof SymBoolConstant) {
      return bool;
    } else if (bool instanceof SymIntRelational) {
      return visitSymRelational((SymIntRelational) bool);
    } else if (bool instanceof SymFloatRelational) {
      return visitSymRelational((SymFloatRelational) bool);
    } else if (bool instanceof SymLongRelational) {
      return visitSymRelational((SymLongRelational) bool);
    } else if (bool instanceof SymDoubleRelational) {
      return visitSymRelational((SymDoubleRelational) bool);
    } else if (bool instanceof SymBoolOperations) {
      return visitSymBoolOperations((SymBoolOperations) bool);
    } else {
      // TODO Criar uma excecao aqui
      throw new RuntimeException("missing case: " + bool.getClass());
    }

  }

  private SymBool visitSymBoolOperations(SymBoolOperations bool) {
    if (bool.getB() != null) {
      SymBool boolA = bool.getA().accept(this);
      SymBool boolB = bool.getB().accept(this);
      return SymBoolOperations.create(boolA, boolB, bool.getOp());
    } else {
      return SymBoolOperations.create(bool.getA().accept(this), null, bool.getOp());
    }
  }

  /********* SymLong *********/
  private SymBool visitSymRelational(SymLongRelational bool) {
    SymLong oldArg1 = bool.getA();
    SymLong oldArg2 = bool.getB();

    if (oldArg1 instanceof SymLongArith) {
      oldArg1 = visitLongArith((SymLongArith) oldArg1);
    }
    if (oldArg2 instanceof SymLongArith) {
      oldArg2 = visitLongArith((SymLongArith) oldArg2);
    }

    SymLong arg1Temp;
    try {
      arg1Temp = (SymLong) read(env, (SymLiteral) oldArg1);
    } catch (ClassCastException e) {
      arg1Temp = oldArg1;
    }

    SymLong arg2Temp;
    try {
      arg2Temp = (SymLong) read(env, (SymLiteral) oldArg2);
    } catch (ClassCastException e) {
      arg2Temp = oldArg2;
    }

    int op = bool.getOp();
    
    SymBool result = null;
    if (arg1Temp == arg2Temp) {
      switch (op) {
      case SymLongRelational.EQ:
      case SymLongRelational.GE:
      case SymLongRelational.LE:
        result = Util.TRUE;
        break;
      case SymLongRelational.NE:
      case SymLongRelational.GT:
      case SymLongRelational.LT:
      default:
        result = Util.FALSE;
        break;
      }
    } else {
      result = SymLongRelational.create(arg1Temp, arg2Temp, op); 
    }

    return result;
  }

  private SymLong visitLongArith(SymLongArith arg) {
    SymLong oldArg1 = arg.getA();
    SymLong oldArg2 = arg.getB();

    if (oldArg1 instanceof SymLongArith) {
      oldArg1 = visitLongArith((SymLongArith) oldArg1);
    }
    if (oldArg2 instanceof SymLongArith) {
      oldArg2 = visitLongArith((SymLongArith) oldArg2);
    }

    SymLong arg1Temp;
    try {
      arg1Temp = (SymLong) read(env, (SymLiteral) oldArg1);
    } catch (ClassCastException e) {
      arg1Temp = oldArg1;
    }

    SymLong arg2Temp;
    try {
      arg2Temp = (SymLong) read(env, (SymLiteral) oldArg2);
    } catch (ClassCastException e) {
      arg2Temp = oldArg2;
    }

    int op = arg.getOp();

    return new SymLongArith(arg1Temp, arg2Temp, op);
  }

  private SymBool visitSymRelational(SymDoubleRelational bool) {
    return new SymDoubleRelational(visitSymDouble(bool.getA()), visitSymDouble(bool.getB()), bool.getOp());
  }
  
  private Object read(Env env, SymLiteral lit) {
    SymNumber result = env.getValue(lit);
    return result == null ? lit : result; 
  }

  private SymDouble visitSymDouble(SymDouble t) {
    SymDouble result;
    if (t instanceof SymDoubleArith) {
      result = visitDoubleArith((SymDoubleArith)t);
    } else if (t instanceof SymMathUnary) {
      result = visitMathUnary((SymMathUnary)t);
    } else if (t instanceof SymMathBinary) {
      result = visitMathBinary((SymMathBinary)t);
    } else if (t instanceof SymDoubleConstant) {
      result = t;
    } else if (t instanceof SymDoubleLiteral) {
      result = (SymDouble) read(env, (SymDoubleLiteral)t);
    } else if (t instanceof SymAsDouble) {
      result = visitSymAsDouble((SymAsDouble)t);
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }

  private SymDouble visitSymAsDouble(SymAsDouble t) {
    SymNumber arg = visitSymNumber(t.getArg());
    return Util.createASDouble(arg);
  }
  
  private SymInt visitSymAsInt(SymAsInt t) {
    SymNumber arg = visitSymNumber(t.getArg());
    return Util.createASInt(arg);
  }

  private SymInt visitSymInt(SymInt arg) {
    SymInt result;
    if (arg instanceof SymIntLiteral) {
      result = (SymInt) read(env, (SymLiteral) arg);
    } else if (arg instanceof SymIntArith) {
      result = visitSymIntArith((SymIntArith)arg);
    } else if (arg instanceof SymIntConstant) {
      result = arg;
    } else if (arg instanceof SymAsInt){
      result = visitSymAsInt((SymAsInt)arg);
    } else {
      throw new UnsupportedOperationException("missing case:" + arg.getClass());
    }
    return result;
  }

  private SymDouble visitMathUnary(SymMathUnary arg) {
    SymDouble oldArg1 = visitSymDouble(arg.getArg());
    return SymMathUnary.create(oldArg1, arg.getOp());
  }
  
  private SymDouble visitMathBinary(SymMathBinary arg) {
    SymDouble oldArg1 = visitSymDouble(arg.getArg1());
    SymDouble oldArg2 = visitSymDouble(arg.getArg2());
    return SymMathBinary.create(oldArg1, oldArg2, arg.getOp());
  }
    
  private SymDouble visitDoubleArith(SymDoubleArith arg) {
    SymDouble oldArg1 = visitSymDouble(arg.getA());
    SymDouble oldArg2 = visitSymDouble(arg.getB());
    return SymDoubleArith.create(oldArg1, oldArg2, arg.getOp());    
  }

  /********* SymFloat *********/
  private SymBool visitSymRelational(SymFloatRelational bool) {
    SymFloat oldArg1 = bool.getA();
    SymFloat oldArg2 = bool.getB();

    if (oldArg1 instanceof SymFloatArith) {
      oldArg1 = visitFloatArith((SymFloatArith) oldArg1);
    }
    if (oldArg2 instanceof SymFloatArith) {
      oldArg2 = visitFloatArith((SymFloatArith) oldArg2);
    }

    SymFloat arg1Temp;
    try {
      arg1Temp = (SymFloat) read(env, (SymLiteral) oldArg1);
    } catch (ClassCastException e) {
      arg1Temp = oldArg1;
    }

    SymFloat arg2Temp;
    try {
      arg2Temp = (SymFloat) read(env, (SymLiteral) oldArg2);
    } catch (ClassCastException e) {
      arg2Temp = oldArg2;
    }

    int op = bool.getOp();
    
    SymBool result = null;
    if (arg1Temp == arg2Temp) {
      switch (op) {
      case SymFloatRelational.EQ:
      case SymFloatRelational.GE:
      case SymFloatRelational.LE:
        result = Util.TRUE;
        break;
      case SymFloatRelational.NE:
      case SymFloatRelational.GT:
      case SymFloatRelational.LT:
      default:
        result = Util.FALSE;
        break;
      }
    } else {
      result = SymFloatRelational.create(arg1Temp, arg2Temp, op); 
    }

    return result;
  }

  private SymFloat visitFloatArith(SymFloatArith arg) {
    SymFloat oldArg1 = arg.getA();
    SymFloat oldArg2 = arg.getB();

    if (oldArg1 instanceof SymFloatArith) {
      oldArg1 = visitFloatArith((SymFloatArith) oldArg1);
    }
    if (oldArg2 instanceof SymFloatArith) {
      oldArg2 = visitFloatArith((SymFloatArith) oldArg2);
    }

    SymFloat arg1Temp;
    try {
      arg1Temp = (SymFloat) read(env, (SymLiteral) oldArg1);
    } catch (ClassCastException e) {
      arg1Temp = oldArg1;
    }

    SymFloat arg2Temp;
    try {
      arg2Temp = (SymFloat) read(env, (SymLiteral) oldArg2);
    } catch (ClassCastException e) {
      arg2Temp = oldArg2;
    }

    int op = arg.getOp();

    return SymFloatArith.create(arg1Temp, arg2Temp, op);
  }

  /********* SymInt *********/
  
  private SymBool visitSymRelational(SymIntRelational bool) {
    SymInt oldArg1 = bool.getA();
    SymInt oldArg2 = bool.getB();

    if (oldArg1 instanceof SymIntArith) {
      oldArg1 = visitSymIntArith((SymIntArith) oldArg1);
    }
    if (oldArg2 instanceof SymIntArith) {
      oldArg2 = visitSymIntArith((SymIntArith) oldArg2);
    }

    SymInt arg1Temp;
    try {
      arg1Temp = (SymInt) read(env, (SymLiteral) oldArg1);
    } catch (ClassCastException e) {
      arg1Temp = oldArg1;
    }

    SymInt arg2Temp;
    try {
      arg2Temp = (SymInt) read(env, (SymLiteral) oldArg2);
    } catch (ClassCastException e) {
      arg2Temp = oldArg2;
    }

    int op = bool.getOp();
    SymBool result = null;
    if (arg1Temp == arg2Temp) {
      switch (op) {
      case SymIntRelational.EQ:
      case SymIntRelational.GE:
      case SymIntRelational.LE:
        result = Util.TRUE;
        break;
      case SymIntRelational.NE:
      case SymIntRelational.GT:
      case SymIntRelational.LT:
      default:
        result = Util.FALSE;
        break;
      }
    } else {
      result = SymIntRelational.create(arg1Temp, arg2Temp, op); 
    }

    return result;
  }
 
  
  private SymInt visitSymIntArith(SymIntArith arg) {
    SymInt oldArg1 = arg.getA();
    SymInt oldArg2 = arg.getB();

    if (oldArg1 instanceof SymIntArith) {
      oldArg1 = visitSymIntArith((SymIntArith) oldArg1);
    }
    if (oldArg2 instanceof SymIntArith) {
      oldArg2 = visitSymIntArith((SymIntArith) oldArg2);
    }

    SymInt arg1Temp;
    try {
      arg1Temp = (SymInt) read(env, (SymLiteral) oldArg1);
    } catch (ClassCastException e) {
      arg1Temp = oldArg1;
    }

    SymInt arg2Temp;
    try {
      arg2Temp = (SymInt) read(env, (SymLiteral) oldArg2);
    } catch (ClassCastException e) {
      arg2Temp = oldArg2;
    }

    int op = arg.getOp();

    return SymIntArith.create(arg1Temp, arg2Temp, op);
  }

  /*
   * Int - retorna o msm BoolInt Float - retorna o msm BoolFloat Arith -
   * substitui vars e retorna um Arith com o msm operador e com o arg1 e arg2
   * substituidos
   */
  public SymNumber visitSymNumber(SymNumber number) {
    if (number instanceof SymIntArith) {
      return visitSymIntArith((SymIntArith) number);
    } else if (number instanceof SymLongArith) {
      return visitLongArith((SymLongArith) number);
    } else if (number instanceof SymFloatArith) {
      return visitFloatArith((SymFloatArith) number);
    } else if (number instanceof SymDoubleArith) {
      return visitDoubleArith((SymDoubleArith) number);
    } else if (number instanceof SymDouble) {
      return visitSymDouble((SymDouble) number);
    } else if (number instanceof SymInt) {
      return visitSymInt((SymInt) number);
    } else if (number instanceof SymLong) {
      return visitSymLong((SymLong) number);
    } else if (number instanceof SymFloat) {
      return visitSymFloat((SymFloat) number);
    } else {
      throw new RuntimeException("invalid type" + number.getClass());
    }
  }
  
  private SymNumber visitSymFloat(SymFloat arg) {
    // TODO add other cases as needed
    SymNumber result;
    if(arg instanceof SymFloatLiteral) {
      result = (SymNumber) read(env, (SymLiteral) arg);
    } else {
      //throw new RuntimeException("probabily you will need to add this case:" + arg.getClass());
      result = arg; //replicating correspondent case of the old visitSymNumber(SymNumber) 
    }
    return result;
  }

  private SymNumber visitSymLong(SymLong arg) {
    // TODO add other cases as needed
    SymNumber result;
    if(arg instanceof SymLongLiteral) {
      result = (SymNumber) read(env, (SymLiteral) arg);
    } else {
      //throw new RuntimeException("probabily you will need to add this case:" + arg.getClass());
      result = arg; //replicating correspondent case of the old visitSymNumber(SymNumber) 
    }
    return result;
  }

  public static void main(String[] args) throws ParseException {
    String query = "FLE(SUB(MUL(FCONST(1.0),FCONST(1.0)),FVAR(ID_1)),FCONST(0.0))";
    PC pc = new Parser(query).parsePC();
    Map<SymLiteral, Unit> var2Units = new SymLiteralUnitSearcher(pc).getUnits();
    SymLiteral var = var2Units.keySet().iterator().next();
    Map<SymLiteral, SymNumber> candidate = new HashMap<SymLiteral, SymNumber>();
    candidate.put(var, Util.createConstant(49.34f));
    Env env = new Env(candidate, Result.UNK, SolverKind.RANDOM);
    SymLiteralReplacer replacer = new SymLiteralReplacer(env);
    SymBool r = ((SymBool) pc.getConstraints().get(0)).accept(replacer);
    System.out.println(r);
  }

}