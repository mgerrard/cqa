package coral.util.visitors.adaptors;

import symlib.FrozenSymBool;
import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLongArith;
import symlib.SymLongConstant;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import coral.util.visitors.interfaces.VoidVisitor;

public class VoidVisitorAdaptor implements VoidVisitor {

  @Override
  public void visitSymBool(SymBool bool) {
    if (bool instanceof FrozenSymBool) {
      ((FrozenSymBool)bool).accept(this);
    } else if (bool instanceof SymIntRelational) {
      visitSymIntRelational((SymIntRelational) bool);
    } else if (bool instanceof SymLongRelational) {
      visitSymLongRelational((SymLongRelational) bool);
    } else if (bool instanceof SymFloatRelational) {
      visitSymFloatRelational((SymFloatRelational) bool);
    } else if (bool instanceof SymDoubleRelational) {
      visitSymDoubleRelational((SymDoubleRelational) bool);
    } else if (bool instanceof SymBoolOperations) {
      visitSymBoolOperations((SymBoolOperations) bool);
    } else if (bool instanceof SymBoolConstant) {
      visitSymBoolConstant((SymBoolConstant) bool);
    } else if (bool instanceof SymBoolLiteral) {
      visitSymBoolLiteral((SymBoolLiteral) bool);
    } else if (bool instanceof SymBoolRelational) {
      visitSymBoolRelational((SymBoolRelational) bool);
    } /*else if (bool instanceof SymMixedEquality) {
      visitSyMixedEquality((SymMixedEquality) bool);
    }*/ else {
      throw new RuntimeException("missing case: " + bool.getClass());
    }
  }
  
//  protected void visitSyMixedEquality(SymMixedEquality bool) {
//    bool.getA().accept(this);
//    bool.getB().accept(this);
//  }

  // GT, LT, LE, GE, EQ, NE
  protected void visitSymIntRelational(SymIntRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  // GT, LT, LE, GE, EQ, NE
  protected void visitSymLongRelational(SymLongRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  // GT, LT, LE, GE, EQ, NE
  protected void visitSymFloatRelational(SymFloatRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }
  
  protected void visitSymDoubleRelational(SymDoubleRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }
  
  // AND, OR, NEG, XOR
  protected void visitSymBoolOperations(SymBoolOperations bool) {
    bool.getA().accept(this);
    if (bool.getOp() != SymBoolOperations.NEG) {
      bool.getB().accept(this);
    }
  }
  
  protected void visitSymBoolConstant(SymBoolConstant bool) {}
  
  protected void visitSymBoolLiteral(SymBoolLiteral bool) {}
  
  protected void visitSymDoubleConstant(SymDoubleConstant number) { }

  protected void visitSymFloatConstant(SymFloatConstant number) { }

  protected void visitSymLongConstant(SymLongConstant number) { }

  protected void visitSymIntConstant(SymIntConstant number) { }

  protected void visitSymLiteral(SymLiteral number) { }
  
  protected void visitSymBoolRelational(SymBoolRelational bool) {
    bool.getA().accept(this);
    bool.getB().accept(this);
  }

  @Override
  public void visitSymNumber(SymNumber number) {
    if (number instanceof SymIntArith) {
      visitSymIntArith((SymIntArith) number);
    } else if (number instanceof SymMathBinary) {
      visitSymMathBinary((SymMathBinary) number);
    } else if (number instanceof SymMathUnary) {
      visitSymMathUnary((SymMathUnary) number);
    } else if (number instanceof SymLongArith) {
      visitSymLongArith((SymLongArith) number);
    } else if (number instanceof SymFloatArith) {
      visitSymFloatArith((SymFloatArith) number);
    } else if (number instanceof SymDoubleArith) {
      visitSymDoubleArith((SymDoubleArith) number);
    } else if (number instanceof SymLiteral) {
      visitSymLiteral((SymLiteral)number);
    } else if (number instanceof SymBoolConstant) {
      visitSymBoolConstant((SymBoolConstant)number);
    } else if (number instanceof SymIntConstant) {
      visitSymIntConstant((SymIntConstant)number);
    } else if (number instanceof SymLongConstant) {
      visitSymLongConstant((SymLongConstant)number);
    } else if (number instanceof SymFloatConstant) {
      visitSymFloatConstant((SymFloatConstant)number);
    } else if (number instanceof SymDoubleConstant) {
      visitSymDoubleConstant((SymDoubleConstant)number);
    } else if (number instanceof SymAsDouble) {
      visitSymAsDouble((SymAsDouble)number);
    } else if (number instanceof SymAsInt) {
      visitSymAsInt((SymAsInt)number);
    } else {
      throw new RuntimeException("invalid type" + number.getClass());
    }
  }

  private void visitSymAsInt(SymAsInt number) {
    number.getArg().accept(this);
  }

  protected void visitSymAsDouble(SymAsDouble number) {
    number.getArg().accept(this);
  }

  protected void visitSymMathUnary(SymMathUnary number) {
    number.getArg().accept(this);
  }

  protected void visitSymMathBinary(SymMathBinary number) {
    number.getArg1().accept(this);
    number.getArg2().accept(this);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymDoubleArith(SymDoubleArith number) {
    number.getA().accept(this);
    number.getB().accept(this);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymFloatArith(SymFloatArith number) {
    number.getA().accept(this);
    number.getB().accept(this);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymLongArith(SymLongArith number) {
    number.getA().accept(this);
    number.getB().accept(this);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymIntArith(SymIntArith number) {
    number.getA().accept(this);
    number.getB().accept(this);
  }

}