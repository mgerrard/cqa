package coral.util.visitors.adaptors;

import symlib.FrozenSymBool;
import symlib.SymAsDouble;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import symlib.Util;
import coral.util.visitors.interfaces.TypedVisitor;

public class TypedVisitorAdaptor implements TypedVisitor {

  @Override
  public SymBool visitSymBool(SymBool bool) {
    if (bool instanceof FrozenSymBool) {
      return ((FrozenSymBool)bool).accept(this);
    } else if (bool instanceof SymIntRelational) {
      return visitSymIntRelational((SymIntRelational) bool);
    } else if (bool instanceof SymLongRelational) {
      return visitSymLongRelational((SymLongRelational) bool);
    } else if (bool instanceof SymFloatRelational) {
      return visitSymFloatRelational((SymFloatRelational) bool);
    } else if (bool instanceof SymDoubleRelational) {
      return visitSymDoubleRelational((SymDoubleRelational) bool);
    } else if (bool instanceof SymBoolOperations) {
      return visitSymBoolOperations((SymBoolOperations) bool);
    } else if (bool instanceof SymBoolConstant) {
      return visitSymBoolConstant((SymBoolConstant) bool);
    } else if (bool instanceof SymBoolLiteral) {
      return visitSymBoolLiteral((SymBoolLiteral) bool);
    } else if (bool instanceof SymBoolRelational) {
      return visitSymBoolRelational((SymBoolRelational) bool);
    } /*else if (bool instanceof SymMixedEquality) {
      return visitSymMixedEquality((SymMixedEquality) bool);
    }*/ else {
      throw new RuntimeException("missing case: " + bool.getClass());
    }
  }
  
//  private SymBool visitSymMixedEquality(SymMixedEquality bool) {
//    SymDouble a = (SymDouble) bool.getA().accept(this);
//    SymInt b = (SymInt) bool.getB().accept(this);    
//    return Util.mixedEq(a, b);
//  }

  protected SymBool visitSymIntRelational(SymIntRelational bool) {
    SymInt a = (SymInt) bool.getA().accept(this);
    SymInt b = (SymInt) bool.getB().accept(this);
    return SymIntRelational.create(a, b, bool.getOp());
  }

  protected SymBool visitSymLongRelational(SymLongRelational bool) {
    SymLong a = (SymLong) bool.getA().accept(this);
    SymLong b = (SymLong) bool.getB().accept(this);
    return SymLongRelational.create(a, b, bool.getOp());
  }

  protected SymBool visitSymFloatRelational(SymFloatRelational bool) {
    SymFloat a = (SymFloat) bool.getA().accept(this);
    SymFloat b = (SymFloat) bool.getB().accept(this);
    return SymFloatRelational.create(a, b, bool.getOp());
  }
  
  protected SymBool visitSymDoubleRelational(SymDoubleRelational bool) {
    SymDouble a = (SymDouble) bool.getA().accept(this);
    SymDouble b = (SymDouble) bool.getB().accept(this);
    return SymDoubleRelational.create(a, b, bool.getOp());
  }
  
  protected SymBool visitSymBoolOperations(SymBoolOperations bool) {
    SymBool a = bool.getA().accept(this);
    SymBool b = bool.getB();
    
    if (bool.getOp() != SymBoolOperations.NEG) {
      b = b.accept(this);
    }
        
    return SymBoolOperations.create(a, b, bool.getOp());
  }
  
  protected SymBool visitSymBoolConstant(SymBoolConstant bool) {
    return bool; 
  }
  
  protected SymNumber visitSymDoubleConstant(SymDoubleConstant number) { 
    return number;
  }

  protected SymNumber visitSymFloatConstant(SymFloatConstant number) {
    return number;
  }

  protected SymNumber visitSymLongConstant(SymLongConstant number) { 
    return number;
  }

  protected SymNumber visitSymIntConstant(SymIntConstant number) { 
    return number;
  }
  
  protected SymBool visitSymBoolRelational(SymBoolRelational bool) {
    SymBool a = bool.getA().accept(this);
    SymBool b = bool.getB().accept(this);
    return SymBoolRelational.create(a, b, bool.getOp());
  }

  @Override
  public SymNumber visitSymNumber(SymNumber number) {
    if (number instanceof SymIntArith) {
      return visitSymIntArith((SymIntArith) number);
    } else if (number instanceof SymMathBinary) {
      return visitSymMathBinary((SymMathBinary) number);
    } else if (number instanceof SymMathUnary) {
      return visitSymMathUnary((SymMathUnary) number);
    } else if (number instanceof SymLongArith) {
      return visitSymLongArith((SymLongArith) number);
    } else if (number instanceof SymFloatArith) {
      return visitSymFloatArith((SymFloatArith) number);
    } else if (number instanceof SymDoubleArith) {
      return visitSymDoubleArith((SymDoubleArith) number);
    } else if (number instanceof SymDoubleLiteral) {
      return visitSymDoubleLiteral((SymDoubleLiteral)number);
    } else if (number instanceof SymLongLiteral) {
      return visitSymLongLiteral((SymLongLiteral)number);
    } else if (number instanceof SymFloatLiteral) {
      return visitSymFloatLiteral((SymFloatLiteral)number);
    } else if (number instanceof SymIntLiteral) {
      return visitSymIntLiteral((SymIntLiteral)number);
    } else if (number instanceof SymIntConstant) {
      return visitSymIntConstant((SymIntConstant)number);
    } else if (number instanceof SymLongConstant) {
      return visitSymLongConstant((SymLongConstant)number);
    } else if (number instanceof SymFloatConstant) {
      return visitSymFloatConstant((SymFloatConstant)number);
    } else if (number instanceof SymDoubleConstant) {
      return visitSymDoubleConstant((SymDoubleConstant)number);
    } else if (number instanceof SymAsDouble) {
      SymInt symInt = (SymInt)visitSymNumber(((SymAsDouble)number).getArg());
      return Util.createASDouble(symInt);
    } else {
      throw new RuntimeException("invalid type" + number.getClass());
    }
  }
  
  protected SymBool visitSymBoolLiteral(SymBoolLiteral bool) {
    return bool;
  }

  private SymNumber visitSymIntLiteral(SymIntLiteral number) {
    return number;
  }

  private SymNumber visitSymFloatLiteral(SymFloatLiteral number) {
    return number;
  }

  private SymNumber visitSymLongLiteral(SymLongLiteral number) {
    return number;
  }

  private SymNumber visitSymDoubleLiteral(SymDoubleLiteral number) {
    return number;
  }

  protected SymNumber visitSymMathUnary(SymMathUnary number) {
    SymDouble a = (SymDouble) number.getArg().accept(this);
    return SymMathUnary.create(a, number.getOp());
  }

  protected SymNumber visitSymMathBinary(SymMathBinary number) {
    SymDouble a = (SymDouble) number.getArg1().accept(this);
    SymDouble b = (SymDouble) number.getArg2().accept(this);
    return SymMathBinary.create(a, b, number.getOp());
  }

  protected SymNumber visitSymDoubleArith(SymDoubleArith number) {
    SymDouble a = (SymDouble) number.getA().accept(this);
    SymDouble b = (SymDouble) number.getB().accept(this);
    return SymDoubleArith.create(a, b, number.getOp());
  }

  protected SymNumber visitSymFloatArith(SymFloatArith number) {
    SymFloat a = (SymFloat) number.getA().accept(this);
    SymFloat b = (SymFloat) number.getB().accept(this);
    return SymFloatArith.create(a, b, number.getOp());
  }

  protected SymNumber visitSymLongArith(SymLongArith number) {
    SymLong a = (SymLong) number.getA().accept(this);
    SymLong b = (SymLong) number.getB().accept(this);
    return SymLongArith.create(a, b, number.getOp());
  }

  protected SymNumber visitSymIntArith(SymIntArith number) {
    SymInt a = (SymInt) number.getA().accept(this);
    SymInt b = (SymInt) number.getB().accept(this);
    return SymIntArith.create(a, b, number.getOp());
  }

}