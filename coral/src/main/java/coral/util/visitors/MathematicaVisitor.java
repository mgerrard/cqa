package coral.util.visitors;

import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.math3.util.Pair;

import coral.PC;
import coral.util.BigRational;
import coral.util.visitors.interfaces.StringVisitor;

public class MathematicaVisitor extends StringVisitor {

  @Override
  protected String visitSymConstant(SymBoolConstant arg) {
    return arg.evalBool() ? "True" : "False";
  }

  @Override
  protected String visitSymRel(SymIntRelational arg) {
    return arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this);
  }

  @Override
  protected String visitSymRel(SymLongRelational arg) {
    return arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this);
  }

  @Override
  protected String visitSymRel(SymFloatRelational arg) {
    return arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this);
  }

  @Override
  protected String visitSymRel(SymDoubleRelational arg) {
    return arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this);
  }

  @Override
  protected String visitSymConstant(SymIntConstant arg) {
    return "" + arg.eval();
  }

  @Override
  protected String visitSymIntLiteral(SymIntLiteral arg) {
    return "x" + arg.getId();
  }

  @Override
  protected String visitSymIntArith(SymIntArith arg) {
    String op = arg.getSymbolOp();
    if (op.equals("/")) {
      op = "~Quotient~";
    } else if (op.equals("%")) {
      op = "~Mod~";
    }
    return "(" + arg.getA().accept(this) + op + arg.getB().accept(this) + ")";
  }

  @Override
  protected String visitSymDoubleConst(SymDoubleConstant arg) {
    // Internal`StringToDouble[s]
    String result = toMathematicaScientificNotation(arg.eval());

    return result;
  }

  @Override
  protected String visitSymDoubleLiteral(SymDoubleLiteral arg) {
    return "x" + arg.getId();
  }

  @Override
  protected String visitSymDoubleArith(SymDoubleArith arg) {
    return "(" + arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this) + ")";
  }

  @Override
  protected String visitSymFloatConst(SymFloatConstant arg) {
    return arg.eval() + "";
  }

  @Override
  protected String visitSymFloatLiteral(SymFloatLiteral arg) {
    return "x" + arg.getId();
  }

  @Override
  protected String visitSymArith(SymFloatArith arg) {
    return "(" + arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this) + ")";
  }

  @Override
  protected String visitSymLongConst(SymLongConstant arg) {
    return arg.eval() + "";
  }

  @Override
  protected String visitSymLongLiteral(SymLongLiteral arg) {
    return "x" + arg.getId();
  }

  @Override
  protected String visitSymBoolOperations(SymBoolOperations arg) {
    return arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this);
  }

  @Override
  protected String visitSymBoolRelational(SymBoolRelational arg) {
    return arg.getA().accept(this) + arg.getSymbolOp()
        + arg.getB().accept(this);
  }

  @Override
  protected String visitSymMathUnary(SymMathUnary arg) {
    return arg.getMathematicaOp() + "[" + arg.getArg().accept(this) + "]";
  }

  @Override
  protected String visitSymMathBinary(SymMathBinary arg) {
    String args;
    if (arg.getOp() == SymMathBinary.ATAN2) { // Math.atan2(y,x) == ArcTan[x,y]
      args = arg.getArg2().accept(this) + "," + arg.getArg1().accept(this);
    } else {
      args = arg.getArg1().accept(this) + "," + arg.getArg2().accept(this);
    }
    return arg.getMathmematicaOp() + "[" + args + "]";
  }

  @Override
  protected String visitSymCast(SymCast symCast) {
    return symCast.getArg().accept(this);
  }

  public static String toMathematicaScientificNotation(double d) {
    String result;
    String val = "" + d;
    if (val.contains("E")) {
      String[] tmp = val.split("E");
      result = "(" + tmp[0] + " * " + "10^(" + tmp[1] + "))";
    } else {
      result = val;
    }
    return result;
  }

  public static String inputForProbabilityComputation(List<PC> pcs,
      Map<Integer, Pair<Integer, Integer>> domain, boolean nprobability) {
    StringBuilder builder = new StringBuilder();
    StringBuilder exprs = new StringBuilder();
    exprs.append("lexpr = { ");
    int i = 0;
    for (PC pc : pcs) {
      builder.append("exp" + i + " := ");
      exprs.append("exp" + i + ",");
      i++;
      for (SymBool bool : pc.getConstraints()) {
        MathematicaVisitor visitor = new MathematicaVisitor();
        builder.append(bool.accept(visitor));
        builder.append(" && ");
      }
      builder.delete(builder.length() - 4, builder.length());
      builder.append(";\n");
    }
    exprs.delete(exprs.length() - 1, exprs.length());
    exprs.append("};\n");
    builder.append(exprs);

    if (nprobability) {
      builder.append("vars = {");
      for (Entry<Integer, Pair<Integer, Integer>> entry : domain.entrySet()) {
        int id = entry.getKey();
        int lo = entry.getValue().getKey();
        int hi = entry.getValue().getSecond();
        builder.append("x" + id + " \\[Distributed] UniformDistribution[{" + lo
            + "," + hi + "}], ");
      }
      builder.delete(builder.length() - 2, builder.length());
      builder.append("};\n");

      builder.append("f[exp_, id_] := Block[{result},\n"
          + " result = AbsoluteTiming[NProbability[exp, vars]];\n"
          + " {id[[1]], result[[2]], result[[1]]} ]\n");
      builder.append("m = MapIndexed[f, lexpr]\n; Total[m]");

    } else { // sum with integers
      StringBuilder domainVolumes = new StringBuilder();
      builder.append("varlist = {");
      domainVolumes.append("domainVolumes = {");
      for (PC pc : pcs) {
        builder.append("{");
        long vol = 1;
        for (SymLiteral lit : pc.getSortedVars()) {
          int id = lit.getId();
          int lo = domain.get(id).getKey();
          int hi = domain.get(id).getSecond();
          builder.append("{x" + id + "," + lo + "," + hi + "}, ");
          vol = vol * (hi - lo + 1);
        }
        domainVolumes.append(vol + ",");
        builder.delete(builder.length() - 2, builder.length());
        builder.append("}, ");
      }
      domainVolumes.delete(domainVolumes.length() - 1, domainVolumes.length());
      builder.delete(builder.length() - 2, builder.length());
      builder.append("};\n");
      domainVolumes.append("};\n");
      builder.append(domainVolumes);
      builder.append("count = Range[1,Length[varlist]];\n");
      builder.append("t = Transpose[{lexpr,varlist,count,domainVolumes}];\n");
      builder.append(" f2[exp_, vars_, id_, vol_] := Block[{result},\n"
          + "result = Sum[Boole[exp], Evaluate[ Sequence @@ vars]] // AbsoluteTiming;\n"
          + "{id, N[result[[2]] / vol], result[[1]]}];\n" + "m = Map[f2 @@ # &, t]; Total[m]");
    }

    return builder.toString();

  }

  public static String generateParallelSum(PC pc,
      Map<Integer, Pair<Integer, Integer>> domain) {
    StringBuilder builder = new StringBuilder();
    builder.append("ParallelSum[Boole[");
    for (SymBool bool : pc.getConstraints()) {
      MathematicaVisitor visitor = new MathematicaVisitor();
      builder.append(bool.accept(visitor));
      builder.append(" && ");
    }
    builder.delete(builder.length() - 4, builder.length());
    builder.append(']');
    BigRational domainSize = BigRational.ONE;
    for (SymLiteral lit : pc.getSortedVars()) {
      int id = lit.getId();
      int lo = domain.get(id).getKey();
      int hi = domain.get(id).getSecond();
      builder.append(", {x" + id + "," + lo + "," + hi + "}");
      domainSize = domainSize.mul(hi - lo + 1);
    }
    builder.append("] / " + domainSize.toString());
    return builder.toString();
  }

  public static String generateNProbabilityCall(PC pc,
      Map<Integer, Pair<Integer, Integer>> domain) {
    StringBuilder builder = new StringBuilder();
    builder.append("NProbability[");
    for (SymBool bool : pc.getConstraints()) {
      MathematicaVisitor visitor = new MathematicaVisitor();
      builder.append(bool.accept(visitor));
      builder.append(" && ");
    }
    builder.delete(builder.length() - 4, builder.length());
    BigRational domainSize = BigRational.ONE;
    builder.append(", {");
    for (SymLiteral lit : pc.getSortedVars()) {
      int id = lit.getId();
      int lo = domain.get(id).getKey();
      int hi = domain.get(id).getSecond();
      builder.append("x" + id + " \\[Distributed] UniformDistribution[{" + lo
          + "," + hi + "}],");
      domainSize = domainSize.mul(hi - lo + 1);
    }
    builder.delete(builder.length() - 1, builder.length());
    builder.append("}");
    builder.append("] //AbsoluteTiming");
    return builder.toString();
  }
}
