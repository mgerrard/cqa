package coral.util.visitors;

import coral.util.visitors.interfaces.GenericVisitor;
import org.jetbrains.annotations.NotNull;
import symlib.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class PaverVisitor extends GenericVisitor<PaverInput> {
  
  private boolean intVarFound;
  private int slackCounter;

  // synthetic vars that should be declared
  private List<PaverSlackVariable> slackVars;
  private List<String> slackCons;
  private boolean enableExperimentalRewriting;

  public PaverVisitor(boolean enableExperimentalRewriting) {
    this.intVarFound = false;
    this.slackCounter = 0;
    this.slackVars = new ArrayList<>();
    this.slackCons = new ArrayList<>();
    this.enableExperimentalRewriting = enableExperimentalRewriting;
  }

  public PaverVisitor() {
    this(false);
  }

  public List<PaverSlackVariable> getSlackVars() {
    return slackVars;
  }

  public List<String> getSlackCons() {
    return slackCons;
  }

  //regex to match variables and functions
  public static final Pattern PATTERN = Pattern.compile("x|\\w\\(");
  
  public String translate(SymBool bool) {
    String result;
    try {
      result = bool.accept(this).cons;
    } catch (UnsupportedOperationException e) {
      result = "";
    }
    return result;
  }
  
  @Override
  protected PaverInput visitSymConstant(SymBoolConstant arg) {
    throw new UnsupportedOperationException("RealPaver does not support boolean values");
  }

  @Override
  protected PaverInput visitSymRel(SymIntRelational arg) {
    if(arg.getOp() == SymIntRelational.NE) {
      throw new UnsupportedOperationException("RealPaver does not support NE");
    }
    
    StringBuilder sb = new StringBuilder();
    
//    sb.append('(');
    sb.append(visitSymInt(arg.getA()));
    
    String op = null;
    
    switch(arg.getOp()) {
      case SymIntRelational.GT:
      case SymIntRelational.GE:
        op = " >= ";
        break;
      case SymIntRelational.EQ:
        op = " = ";
        break;
      case SymIntRelational.LE:
      case SymIntRelational.LT:
        op = " <= ";
        break;
    }
           
    sb.append(op);
    sb.append(visitSymInt(arg.getB()));
//    sb.append(')');
    
    return new PaverInput(true, sb.toString());
  }

  @Override
  protected PaverInput visitSymRel(SymLongRelational arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymRel(SymFloatRelational arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymRel(SymDoubleRelational arg) {
    if(arg.getOp() == SymDoubleRelational.NE) {
      throw new UnsupportedOperationException("RealPaver does not support NE");
    }
    
    StringBuilder sb = new StringBuilder();
    
//    sb.append('(');
    sb.append(visitSymDouble(arg.getA()));
    
    String op = null;
    
    switch(arg.getOp()) {
      case SymIntRelational.GT:
      case SymIntRelational.GE:
        op = " >= ";
        break;
      case SymIntRelational.EQ:
        op = " = ";
        break;
      case SymIntRelational.LE:
      case SymIntRelational.LT:
        op = " <= ";
        break;
    }
           
    sb.append(op);
    sb.append(visitSymDouble(arg.getB()));
//    sb.append(')');
    
    return new PaverInput(false, sb.toString());
  }

  @Override
  protected PaverInput visitSymConstant(SymIntConstant arg) {
    return new PaverInput(false, arg.toString());
  }

  @Override
  protected PaverInput visitSymIntLiteral(SymIntLiteral arg) {
    return new PaverInput(true, "x" + arg.getId());
  }

  @Override
  protected PaverInput visitSymIntArith(SymIntArith arg) {
    
    String op;
    int argOp = arg.getOp();
    PaverInput pi;
    if(enableExperimentalRewriting && argOp == SymIntArith.DIV) {
      pi = handleIntegerDivision(arg);
    } else if (enableExperimentalRewriting && argOp == SymIntArith.MOD) {
      pi = handleIntegerMod(arg);
    } else if (enableExperimentalRewriting && argOp >= SymIntArith.SHIFT_LEFT
        && argOp <= SymIntArith.UNSIGNED_SHIFT_RIGHT) {
      pi = handleIntegerShift(arg, argOp);
    }else if (argOp < SymIntArith.DIV) {
      pi = handleSimpleIntegerBinaryOp(arg, argOp);
    } else {
      throw new UnsupportedOperationException("Operation unsupported by realpaver: " + argOp);
    }
    return pi;
  }

  private static final int NUM_BITS_INT = 32;
  private PaverInput handleIntegerShift(SymIntArith arg, int argOp) {

    PaverInput left = visitSymInt(arg.getA());
    SymInt right = arg.getB();

    if (!(right instanceof SymIntConstant)) {
      throw new UnsupportedOperationException("non-constant bit shifts should be split/translated " +
          "before calling realpaver");
    }
    int rightArg = ((SymIntConstant) right).evalNumber().intValue();

    int bitsToShift;
    if (rightArg >= 0) {
      bitsToShift = rightArg % NUM_BITS_INT;
    } else {
      bitsToShift = (2 << (NUM_BITS_INT - 1) + rightArg) % NUM_BITS_INT;
    }
    int twoPowNumBits = 2 << bitsToShift;
    switch(argOp) {
      case SymIntArith.SHIFT_LEFT:
        PaverInput aTimes2PowBr = new PaverInput(true,"(" + left + ") * (2 ^ " + bitsToShift +")");
        return slackIntegerDivision(false,aTimes2PowBr,new PaverInput(true,"(2^"+NUM_BITS_INT+")"));
      case SymIntArith.SHIFT_RIGHT:
        return slackIntegerDivision(false,left,new PaverInput(true,"(2^"+bitsToShift+")"));
      case SymIntArith.UNSIGNED_SHIFT_RIGHT:
        throw new RuntimeException("Unimplemented for now!");
      default:
        throw new RuntimeException("Error! Unsupported op: " + argOp);
    }
  }

  private PaverInput handleIntegerMod(SymIntArith arg) {
    return slackIntegerDivision(arg,false);
  }

  private PaverInput  handleIntegerDivision(SymIntArith arg) {
    return slackIntegerDivision(arg,true);
  }

  private PaverInput slackIntegerDivision(SymIntArith arg, boolean quotient) {
    PaverInput left = visitSymInt(arg.getA());
    PaverInput right = visitSymInt(arg.getB());

    return slackIntegerDivision(quotient, left, right);
  }

  @NotNull
  private PaverInput slackIntegerDivision(boolean quotient, PaverInput left, PaverInput right) {
    PaverSlackVariable a = new PaverSlackVariable("a" + slackCounter);
    PaverSlackVariable b = new PaverSlackVariable("b" + slackCounter);
    PaverSlackVariable r = new PaverSlackVariable("r" + slackCounter);
    PaverSlackVariable q = new PaverSlackVariable("q" + slackCounter);
    slackCounter++;
    slackVars.add(a);
    slackVars.add(b);
    slackVars.add(r);
    slackVars.add(q);

    slackCons.add(a + " = " + left);
    slackCons.add(b + " = " + right);
//    a = q*b + r,
    slackCons.add(a + " = " + q + "*" + b + " + " + r);
//    -max(b,-b)+1 <= r,
    slackCons.add("-max(" + b + ",-" + b + ") + 1 <= " + r);
//    r <= max(b,-b)-1,
    slackCons.add(r + " <= max(" + b + ",-" + b + ") - 1");
//    max(q*b,-1*q*b)<=max(a,-a)
    slackCons.add("max(" + q + "*" + b + ",-1*" + q + "*" + b + ") <= max(" + a + ",-" + a + ")");

    if (quotient) {
      return new PaverInput(true,q.toString());
    } else {
      return new PaverInput(true,r.toString());
    }
  }

  @NotNull
  private PaverInput handleSimpleIntegerBinaryOp(SymIntArith arg, int argOp) {
    String op;
    PaverInput pi;
    op = SymIntArith.symbols[argOp];

    StringBuilder sb = new StringBuilder();
    PaverInput a = visitSymInt(arg.getA());
    PaverInput b = visitSymInt(arg.getB());

    if (argOp != SymIntArith.DIV || !intVarFound) {
      sb.append('(');
      sb.append(a);
      sb.append(" " + op + " ");
      sb.append(b);
      sb.append(')');
    } else {
      intVarFound = false;
    }
    pi = new PaverInput(true, sb.toString());
    return pi;
  }

  @Override
  protected PaverInput visitSymDoubleConst(SymDoubleConstant arg) {
    return new PaverInput(false,arg.toString());
  }

  @Override
  protected PaverInput visitSymDoubleLiteral(SymDoubleLiteral arg) {
    return new PaverInput(false,"x" + arg.getId());
  }

  @Override
  protected PaverInput visitSymFloatConst(SymFloatConstant arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymFloatLiteral(SymFloatLiteral arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymArith(SymFloatArith arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymLongConst(SymLongConstant arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymLongLiteral(SymLongLiteral arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymBoolOperations(SymBoolOperations arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymBoolRelational(SymBoolRelational arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected PaverInput visitSymMathUnary(SymMathUnary symDoubleJavaMath) {
    int argOp = symDoubleJavaMath.getOp();
    boolean isLog10 = false;
    String op;
    switch(argOp) {
      case SymMathUnary.ROUND:
        throw new UnsupportedOperationException("RealPaver does not support this operation: " + SymMathUnary.symbols[argOp]);
      case SymMathUnary.LOG10: //RealPaver does not support log10; we will use logn to reproduce it
        isLog10 = true;
        op = SymMathUnary.symbols[SymMathUnary.LOG];
        break;
      default:
        op = SymMathUnary.symbols[argOp];
    }
    
    StringBuilder sb = new StringBuilder();
    PaverInput visitedArg = visitSymDouble(symDoubleJavaMath.getArg());
    
    if(isLog10) {
      sb.append('(');
      sb.append(op);
      sb.append('(');
      sb.append(visitedArg.cons);
      sb.append(") / ");
      sb.append(op);
      sb.append("(10)");
      sb.append(')');
    } else {
      sb.append(op);
      sb.append('(');
      sb.append(visitedArg.cons);
      sb.append(')');
    }
    return new PaverInput(visitedArg.foundIntVar,sb.toString());
  }

  @Override
  protected PaverInput visitSymMathBinary(SymMathBinary symDouble) {
    StringBuffer sb = new StringBuffer();
    boolean intVar = false;
    
    if(symDouble.getOp() == SymMathBinary.ATAN2) {
      SymDouble arg1 = symDouble.getArg1();
      SymDouble arg2 = symDouble.getArg2();
      
      PaverInput y = visitSymDouble(arg1);
      PaverInput x = visitSymDouble(arg2);
      
      sb.append("2 * atan((sqrt(("+x.cons + ")^2 + ("+y.cons+")^2) - ("+x.cons+"))/("+y.cons+"))");
      intVar = y.foundIntVar || x.foundIntVar;
      
    } else { //RealPaver supports only integers as exponents
      SymDouble arg1 = symDouble.getArg1();
      SymDouble arg2 = symDouble.getArg2();
      PaverInput exponent = visitSymDouble(symDouble.getArg2());
      
      if(PATTERN.matcher(exponent.cons).find()) { //there is a variable/function in the exponent - error!;
        throw new UnsupportedOperationException("RealPaver does not support exponentiation with variables or functions present in the exponent");
      } else {
        double numericExponent = arg2.eval();
        boolean expIsNegative = numericExponent < 0;
        sb.append('(');
        
        if(expIsNegative) { //invert the expression
          numericExponent = numericExponent * -1;
          sb.append("1 / ");
        }

        PaverInput base = visitSymDouble(arg1);
        intVar = base.foundIntVar;
        if(numericExponent == 0.5) { //square root 
          sb.append("sqrt(");
          sb.append(base.cons);
          sb.append(')');
        } else if (numericExponent % 1 == 0) { //exponent is an integer
          sb.append(base.cons);
          sb.append(" ^ ");
          sb.append((int) numericExponent);
          sb.append(')');
        } else { 
          throw new UnsupportedOperationException("RealPaver does not support real numbers as an exponent: " + numericExponent);
        }
        
        if(expIsNegative) {
          sb.append(')');
        }
        
      }
    }
    return new PaverInput(intVar, sb.toString());
  }

  @Override
  protected PaverInput visitSymAsDouble(SymAsDouble sym) {
    return sym.getArg().accept(this);
  }

  @Override
  protected PaverInput visitSymAsInt(SymAsInt sym) {
    return sym.getArg().accept(this);
  }

  @Override
  protected PaverInput visitSymDoubleArith(SymDoubleArith arg) {
    String op;
    int argOp = arg.getOp();
    if(argOp > SymDoubleArith.DIV) {
      throw new UnsupportedOperationException("RealPaver does not support " + SymDoubleArith.symbols[argOp]);
    } else {
      op = SymDoubleArith.symbols[argOp];
    }
    
    StringBuilder sb = new StringBuilder();
    PaverInput a = visitSymDouble(arg.getA());
    PaverInput b = visitSymDouble(arg.getB());
    
    sb.append('(');
    sb.append(a.cons);
    sb.append(" " + op + " ");
    sb.append(b.cons);
    sb.append(')');
    
    return new PaverInput(a.foundIntVar || b.foundIntVar, sb.toString());
  }

  public static void main(String[] args) throws Exception {
    System.out.println(3.5 % 1);
    
//    PC pc = new Parser(DavidSamples.sample1).parsePC();
//    PaverVisitor pv = new PaverVisitor();
//    for(SymBool bool : pc.getConstraints()) {
//      try{
//      String s = bool.accept(pv);
//      System.out.print(s);
//      System.out.println(",");
//      } catch (Exception e) {
//        System.out.println("Not accepted:" + bool);
//      }
//    }
  }

  public static class PaverSlackVariable {
    public final String name;

    public PaverSlackVariable(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return name;
    }
  }
}

