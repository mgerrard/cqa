package coral.util.visitors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongRelational;
import symlib.SymNumber;
import coral.util.visitors.interfaces.VoidVisitor;

public class EqualitySearcher implements VoidVisitor {

  private Set<SymLiteral> simpleVars;

  private Map<SymLiteral, List<SymNumber>> heuristicVarsMap;

  public EqualitySearcher(SymBool bool) {
    simpleVars = new HashSet<SymLiteral>();
    heuristicVarsMap = new HashMap<SymLiteral, List<SymNumber>>();

    bool.accept(this);
  }

  /*
   * BoolBinary - verifies arg1 e arg2 Rel - verifies arg1 e arg2 BoolNot -
   * verifies arg BoolConst - do nothing
   */
  public void visitSymBool(SymBool bool) {
    // if (bool instanceof SymBoolBinary) {
    // visitSymBoolBinary((SymBoolBinary) bool);
    if (bool instanceof SymIntRelational) {
      visitSymRelational((SymIntRelational) bool);
    } else if (bool instanceof SymLongRelational) {
      visitSymRelational((SymLongRelational) bool);
    } else if (bool instanceof SymFloatRelational) {
      visitSymRelational((SymFloatRelational) bool);
    } else if (bool instanceof SymDoubleRelational) {
      visitSymRelational((SymDoubleRelational) bool);
    } else if (bool instanceof SymBoolOperations) {
      return; //don't apply heuristic to composite clauses
//      if(((SymBoolOperations) bool).getOp() != SymBoolOperations.OR) { //cannot apply heuristic to an "OR" operation
//        visitSymBoolOperations((SymBoolOperations) bool);
//      }
    } /*else if (bool instanceof SymMixedEquality) {
      visitSymMixedEquality((SymMixedEquality) bool);
    } */ else if (bool instanceof SymBoolConstant) {
      return;
    } else {
      // TODO Criar uma excecao aqui
      throw new RuntimeException("missing case: " + bool.getClass());
    }
  }

//  private void visitSymMixedEquality(SymMixedEquality bool) {
//    
//    //TODO Check This
//    SymDouble op1 = bool.getA();
//    SymInt op2 = bool.getB();
//    if ((op1 instanceof SymLiteral) || (op2 instanceof SymLiteral)) {
//      SymNumber exp = handleEquality(op1, op2);
//
//      exp.accept(this);
//    } else {
//      op1.accept(this);
//      op2.accept(this);
//    }
//  }

  private void visitSymBoolOperations(SymBoolOperations bool) {
    SymBool op1 = bool.getA();
    SymBool op2 = bool.getB();

    op1.accept(this);

    if (op2 != null) {
      op2.accept(this);
    }
  }

  private void visitSymRelational(SymDoubleRelational bool) {
    SymDouble op1 = bool.getA();
    SymDouble op2 = bool.getB();

    if (bool.getOp() == SymIntRelational.EQ) {
      if ((op1 instanceof SymLiteral) || (op2 instanceof SymLiteral)) {
        SymNumber exp = handleEquality(op1, op2);

        exp.accept(this);
      } else {
        op1.accept(this);
        op2.accept(this);
      }
    } else {
      op1.accept(this);
      op2.accept(this);
    }
  }

  protected SymNumber handleEquality(SymNumber op1, SymNumber op2) {
    // First, we divide the var and the exp to the mapping
    boolean op1Literal = (op1 instanceof SymLiteral);
    SymLiteral var = op1Literal ? (SymLiteral) op1 : (SymLiteral) op2;
    SymNumber exp = op1Literal ? op2 : op1;

    // The var is already mapped?
    if (heuristicVarsMap.containsKey(var)) {
      List<SymNumber> l = heuristicVarsMap.get(var);

      l.add(exp);
    } else {
      List<SymNumber> l = new LinkedList<SymNumber>();
      l.add(exp);

      heuristicVarsMap.put(var, l);
    }
    return exp;
  }

  private void visitSymRelational(SymFloatRelational bool) {
    SymFloat op1 = bool.getA();
    SymFloat op2 = bool.getB();

    if (bool.getOp() == SymIntRelational.EQ) {
      if ((op1 instanceof SymLiteral) || (op2 instanceof SymLiteral)) {
        // First, we divide the var and the exp to the mapping
        boolean op1Literal = (op1 instanceof SymLiteral);
        SymLiteral var = op1Literal ? (SymLiteral) op1 : (SymLiteral) op2;
        SymNumber exp = op1Literal ? op2 : op1;

        // The var is already mapped?
        if (heuristicVarsMap.containsKey(var)) {
          List<SymNumber> l = heuristicVarsMap.get(var);

          l.add(exp);
        } else {
          List<SymNumber> l = new LinkedList<SymNumber>();
          l.add(exp);

          heuristicVarsMap.put(var, l);
        }

        exp.accept(this);
      } else {
        op1.accept(this);
        op2.accept(this);
      }
    } else {
      op1.accept(this);
      op2.accept(this);
    }
  }

  private void visitSymRelational(SymLongRelational bool) {
    SymLong op1 = bool.getA();
    SymLong op2 = bool.getB();

    if (bool.getOp() == SymIntRelational.EQ) {
      if ((op1 instanceof SymLiteral) || (op2 instanceof SymLiteral)) {
        // First, we divide the var and the exp to the mapping
        boolean op1Literal = (op1 instanceof SymLiteral);
        SymLiteral var = op1Literal ? (SymLiteral) op1 : (SymLiteral) op2;
        SymNumber exp = op1Literal ? op2 : op1;

        // The var is already mapped?
        if (heuristicVarsMap.containsKey(var)) {
          List<SymNumber> l = heuristicVarsMap.get(var);

          l.add(exp);
        } else {
          List<SymNumber> l = new LinkedList<SymNumber>();
          l.add(exp);

          heuristicVarsMap.put(var, l);
        }

        exp.accept(this);
      } else {
        op1.accept(this);
        op2.accept(this);
      }
    } else {
      op1.accept(this);
      op2.accept(this);
    }
  }

  private void visitSymRelational(SymIntRelational bool) {
    SymInt op1 = bool.getA();
    SymInt op2 = bool.getB();

    if (bool.getOp() == SymIntRelational.EQ) {
      if ((op1 instanceof SymLiteral) || (op2 instanceof SymLiteral)) {
        // First, we divide the var and the exp to the mapping
        boolean op1Literal = (op1 instanceof SymLiteral);
        SymLiteral var = op1Literal ? (SymLiteral) op1 : (SymLiteral) op2;
        SymNumber exp = op1Literal ? op2 : op1;

        // The var is already mapped?
        if (heuristicVarsMap.containsKey(var)) {
          List<SymNumber> l = heuristicVarsMap.get(var);

          l.add(exp);
        } else {
          List<SymNumber> l = new LinkedList<SymNumber>();
          l.add(exp);

          heuristicVarsMap.put(var, l);
        }

        exp.accept(this);
      } else {
        op1.accept(this);
        op2.accept(this);
      }
    } else {
      op1.accept(this);
      op2.accept(this);
    }
  }

  /*
   * Arith - verifies operand1 e operand2 Int - if it is literal, adds itself on
   * list Float - if it is literal, adds itself on list.
   */
  public void visitSymNumber(SymNumber number) {
    if (number instanceof SymIntArith) {
      visitSymArith((SymIntArith) number);
    } else if (number instanceof SymLongArith) {
      visitSymArith((SymLongArith) number);
    } else if (number instanceof SymFloatArith) {
      visitSymArith((SymFloatArith) number);
    } else if (number instanceof SymDoubleArith) {
      visitSymArith((SymDoubleArith) number);
    } else if ((number instanceof SymFloat) || (number instanceof SymInt)
        || (number instanceof SymLong) || (number instanceof SymDouble)) {
      visitSymIntFloatBoolean(number);
    } else {
      throw new RuntimeException("invalid type" + number.getClass());
    }
  }

  private void visitSymArith(SymDoubleArith number) {
    SymDouble op1 = number.getA();
    SymDouble op2 = number.getB();

    op1.accept(this);
    if (op2 != null) {
      op2.accept(this);
    }
  }

  private void visitSymArith(SymFloatArith number) {
    SymFloat op1 = number.getA();
    SymFloat op2 = number.getB();

    op1.accept(this);
    if (op2 != null) {
      op2.accept(this);
    }
  }

  private void visitSymArith(SymLongArith number) {
    SymLong op1 = number.getA();
    SymLong op2 = number.getB();

    op1.accept(this);
    if (op2 != null) {
      op2.accept(this);
    }
  }

  private void visitSymIntFloatBoolean(SymNumber number) {
    // We only add a variable in the list if this variable is not in the
    // heuristic map
    if ((number instanceof SymLiteral)
        && !(heuristicVarsMap.containsKey(number))) {
      simpleVars.add((SymLiteral) number);
    }
  }

  private void visitSymArith(SymIntArith number) {
    SymInt op1 = number.getA();
    SymInt op2 = number.getB();

    op1.accept(this);
    if (op2 != null) {
      op2.accept(this);
    }
  }

  public Set<SymLiteral> getSimpleVars() {
    return simpleVars;
  }

  public Map<SymLiteral, List<SymNumber>> getDependentVarsMap() {
    return heuristicVarsMap;
  }
}