package coral.util.visitors;

import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import coral.util.visitors.interfaces.StringVisitor;

public class ICOSVisitor extends StringVisitor {

  @Override
  protected String visitSymConstant(SymBoolConstant arg) {
    throw new UnsupportedOperationException("ICOS does not support boolean values");
  }

  @Override
  protected String visitSymRel(SymIntRelational arg) {
    if(arg.getOp() == SymIntRelational.NE) {
      throw new UnsupportedOperationException("ICOS does not support NE");
    }

    StringBuilder sb = new StringBuilder();
    sb.append(visitSymInt(arg.getA()));
    
    String op = null;
    switch(arg.getOp()) {
      case SymIntRelational.GT:
      case SymIntRelational.GE:
        op = " >= ";
        break;
      case SymIntRelational.EQ:
        op = " = ";
        break;
      case SymIntRelational.LE:
      case SymIntRelational.LT:
        op = " <= ";
        break;
    }
           
    sb.append(op);
    sb.append(visitSymInt(arg.getB()));
    return sb.toString();
  }

  @Override
  protected String visitSymRel(SymLongRelational arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymRel(SymFloatRelational arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymRel(SymDoubleRelational arg) {
    if(arg.getOp() == SymDoubleRelational.NE) {
      throw new UnsupportedOperationException("ICOS does not support NE");
    }

    StringBuilder sb = new StringBuilder();
    sb.append(visitSymDouble(arg.getA()));
    
    String op = null;
    switch(arg.getOp()) {
      case SymDoubleRelational.GT:
      case SymDoubleRelational.GE:
        op = " >= ";
        break;
      case SymDoubleRelational.EQ:
        op = " = ";
        break;
      case SymDoubleRelational.LE:
      case SymDoubleRelational.LT:
        op = " <= ";
        break;
    }
           
    sb.append(op);
    sb.append(visitSymDouble(arg.getB()));
    return sb.toString();
  }

  @Override
  protected String visitSymConstant(SymIntConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymIntLiteral(SymIntLiteral arg) {
    return "x" + arg.getId();
  }

  @Override
  protected String visitSymIntArith(SymIntArith arg) {
    String op;
    int argOp = arg.getOp();
    if(argOp > SymIntArith.DIV) {
      throw new UnsupportedOperationException("ICOS does not support " + SymIntArith.symbols[argOp]);
    } else {
      op = SymIntArith.symbols[argOp];
    }
    
    StringBuilder sb = new StringBuilder();
    String a = visitSymInt(arg.getA());
    String b = visitSymInt(arg.getB());
    
    sb.append('(');
    sb.append(a);
    sb.append(" " + op + " ");
    sb.append(b);
    sb.append(')');
    
    return sb.toString();
  }

  @Override
  protected String visitSymDoubleConst(SymDoubleConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymDoubleLiteral(SymDoubleLiteral arg) {
    return "x" + arg.getId();
  }

  @Override
  protected String visitSymDoubleArith(SymDoubleArith arg) {
    String op;
    int argOp = arg.getOp();
    if(argOp > SymDoubleArith.DIV) {
      throw new UnsupportedOperationException("ICOS does not support " + SymDoubleArith.symbols[argOp]);
    } else {
      op = SymDoubleArith.symbols[argOp];
    }
    
    StringBuilder sb = new StringBuilder();
    String a = visitSymDouble(arg.getA());
    String b = visitSymDouble(arg.getB());
    
    sb.append('(');
    sb.append(a);
    sb.append(" " + op + " ");
    sb.append(b);
    sb.append(')');
    
    return sb.toString();
  }

  @Override
  protected String visitSymFloatConst(SymFloatConstant arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymFloatLiteral(SymFloatLiteral arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymArith(SymFloatArith arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymLongConst(SymLongConstant arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymLongLiteral(SymLongLiteral arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymBoolOperations(SymBoolOperations arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymBoolRelational(SymBoolRelational arg) {
    throw new UnsupportedOperationException("Not implemented (yet)");
  }

  @Override
  protected String visitSymMathUnary(SymMathUnary symDoubleJavaMath) {
    int argOp = symDoubleJavaMath.getOp();
    boolean isLog10 = false;
    boolean isTan = false;
    String op;
    switch(argOp) {
      case SymMathUnary.ROUND:
        throw new UnsupportedOperationException("ICOS does not support this operation: " + SymMathUnary.symbols[argOp]);
      case SymMathUnary.LOG10: //ICOS does not support log10; we will use the natural log. to reproduce it
        isLog10 = true;
        op = SymMathUnary.symbols[SymMathUnary.LOG];
        break;
      case SymMathUnary.TAN: //for some reason ICOS segfaults when using tan()
        isTan = true;
        op = SymMathUnary.symbols[SymMathUnary.TAN];
        break;
      default:
        op = SymMathUnary.symbols[argOp];
    }
    
    StringBuilder sb = new StringBuilder();
    String visitedArg = visitSymDouble(symDoubleJavaMath.getArg());
    
    if(isLog10) {
      sb.append('(');
      sb.append(op);
      sb.append('(');
      sb.append(visitedArg);
      sb.append(") / ");
      sb.append(op);
      sb.append("(10)");
      sb.append(')');
    } else if (isTan) {
      sb.append(translateTan(visitedArg));
    } else {
      sb.append(op);
      sb.append('(');
      sb.append(visitedArg);
      sb.append(')');
    }
    return sb.toString();
  }

  @Override
  protected String visitSymMathBinary(SymMathBinary symDouble) {
    StringBuffer sb = new StringBuffer();

    if(symDouble.getOp() == SymMathBinary.ATAN2) { //ICOS does not support ATAN
      SymDouble arg1 = symDouble.getArg1();
      SymDouble arg2 = symDouble.getArg2();
      
      String visitedArg1 = visitSymDouble(arg1);
      String visitedArg2 = visitSymDouble(arg2);
      String visitedArgs = "(" + visitedArg1 + ") / (" + visitedArg2 + ")";
      
      sb.append(translateTan(visitedArgs));
      
    } else {  
      SymDouble arg1 = symDouble.getArg1();
      SymDouble arg2 = symDouble.getArg2();
      sb.append('(');
      sb.append(visitSymDouble(arg1));
      sb.append(") ^ ("); 
      sb.append(visitSymDouble(arg2));
      sb.append(')');
    }
    return sb.toString();
  }

  @Override
  protected String visitSymCast(SymCast symCast) {
    return symCast.getArg().accept(this);
  }

  private static String translateTan(String visitedArg) {
    StringBuffer sb = new StringBuffer();
    sb.append('(');
    sb.append(SymMathUnary.symbols[SymMathUnary.SIN]);
    sb.append('(');
    sb.append(visitedArg);
    sb.append(") / ");
    sb.append(SymMathUnary.symbols[SymMathUnary.COS]);
    sb.append('(');
    sb.append(visitedArg);
    sb.append(')');
    sb.append(')');
    
    return sb.toString();
  }
  
}
