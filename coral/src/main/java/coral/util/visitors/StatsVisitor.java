package coral.util.visitors;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import symlib.SymBool;
import symlib.SymBoolOperations;
import symlib.SymDoubleArith;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntRelational;
import symlib.SymLongArith;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.counters.ResultData.Entry;
import coral.util.visitors.adaptors.VoidVisitorAdaptor;

public class StatsVisitor extends VoidVisitorAdaptor {

  public int totalMathOperators;
  public int totalMathFunctions;
  public int totalBooleanClauses;
  public Set<String> operatorsFound;
  public Set<String> functionsFound;
  public int totalMixedIntegerFloatClauses;

  public StatsVisitor() {
    totalMathFunctions = 0;
    totalMathOperators = 0;
    totalBooleanClauses = 0;
    totalMixedIntegerFloatClauses = 0;
    operatorsFound = new HashSet<String>();
    functionsFound = new HashSet<String>();
  }

  
  boolean foundFloat = false;
  boolean foundInt = false;
  
  // GT, LT, LE, GE, EQ, NE
  protected void visitSymIntRelational(SymIntRelational bool) {
    foundInt = true;
    super.visitSymIntRelational(bool);
    totalBooleanClauses++;
  }

  // GT, LT, LE, GE, EQ, NE
  protected void visitSymLongRelational(SymLongRelational bool) {
    foundInt = true;
    super.visitSymLongRelational(bool);
    totalBooleanClauses++;
  }

  // GT, LT, LE, GE, EQ, NE
  protected void visitSymFloatRelational(SymFloatRelational bool) {
    foundFloat = true;
    super.visitSymFloatRelational(bool);
    totalBooleanClauses++;
  }

  protected void visitSymDoubleRelational(SymDoubleRelational bool) {
    foundFloat = true;
    super.visitSymDoubleRelational(bool);
    totalBooleanClauses++;
  }

  // AND, OR, NEG, XOR
  protected void visitSymBoolOperations(SymBoolOperations bool) {
    foundFloat = false;
    foundInt = false;
    super.visitSymBoolOperations(bool);
    
    if (foundInt && foundFloat) {
      totalMixedIntegerFloatClauses++;
    }
  }

  protected void visitSymMathUnary(SymMathUnary number) {
    super.visitSymMathUnary(number);
    totalMathFunctions++;
    functionsFound.add(SymMathUnary.symbols[number.getOp()]);
  }

  protected void visitSymMathBinary(SymMathBinary number) {
    super.visitSymMathBinary(number);
    totalMathFunctions++;
    functionsFound.add(SymMathBinary.symbols[number.getOp()]);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymDoubleArith(SymDoubleArith number) {
    super.visitSymDoubleArith(number);
    totalMathOperators++;
    operatorsFound.add(SymDoubleArith.symbols[number.getOp()]);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymFloatArith(SymFloatArith number) {
    super.visitSymFloatArith(number);
    totalMathOperators++;
    operatorsFound.add(SymFloatArith.symbols[number.getOp()]);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymLongArith(SymLongArith number) {
    super.visitSymLongArith(number);
    totalMathOperators++;
    operatorsFound.add(SymLongArith.symbols[number.getOp()]);
  }

  // ADD, SUB, MULT, DIV, MOD
  protected void visitSymIntArith(SymIntArith number) {
    super.visitSymIntArith(number);
    totalMathOperators++;
    operatorsFound.add(SymIntArith.symbols[number.getOp()]);
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("[constraint stats] total: op=");
    sb.append(totalMathOperators);
    sb.append(" fun=");
    sb.append(totalMathFunctions);
    sb.append(" bool=");
    sb.append(totalBooleanClauses);
    sb.append(" mixed=");
    sb.append(totalMixedIntegerFloatClauses);
    
    sb.append("\n[constraint stats] distinct: op=");
    sb.append(operatorsFound.size());
    sb.append(" fun=");
    sb.append(functionsFound.size());
    
    return sb.toString();
  }

  public static void extractData(PC pc, Entry entry) {
    StatsVisitor sv = new StatsVisitor();
    for (SymBool sym : pc.getConstraints()) {
      sym.accept(sv);
    }
    entry.distinctFunctions.addAll(sv.functionsFound);
    entry.distinctOperations.addAll(sv.operatorsFound);
    entry.nClauses = sv.totalBooleanClauses;
    entry.nFunctions = sv.totalMathFunctions;
    entry.nOperations = sv.totalMathOperators;
        
    System.out.println(sv.toString());
  }
  
  public static void main(String[] args) throws Exception {
    Scanner s = new Scanner(new File("/home/mateus/workspace/coral-cons-7"));
    StatsVisitor sv = new StatsVisitor();
    int oldValue = 0;
    int line = 0;
    while (s.hasNextLine()) {
      try {
      line++;
      String cons = s.nextLine();
      PC pc = (new Parser(cons)).parsePC();
      for (SymBool sb : pc.getConstraints()) {
        sb.accept(sv);
      }
      if (oldValue != sv.totalMixedIntegerFloatClauses) {
        oldValue = sv.totalMixedIntegerFloatClauses;
        System.out.println("Found mixed at line " + line);
      }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    System.out.println(sv.toString());
  }
}