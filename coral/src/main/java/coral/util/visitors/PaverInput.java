package coral.util.visitors;

public class PaverInput {

  public final boolean foundIntVar;
  public final String cons;
  
  public PaverInput(boolean foundIntVar, String cons) {
    this.foundIntVar = foundIntVar;
    this.cons = cons;
  }
  
  public String toString() {
    return cons;
  }
}
