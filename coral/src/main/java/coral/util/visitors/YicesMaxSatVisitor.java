package coral.util.visitors;

import java.util.Set;

import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import coral.WeightedPC;
import coral.util.visitors.interfaces.StringVisitor;

public class YicesMaxSatVisitor extends StringVisitor {

  public static String getYicesInput(WeightedPC pc) {
    StringBuffer sb = new StringBuffer();
    Set<SymLiteral> vars = pc.getSortedVars();
    
    for (SymLiteral var : vars) {
      sb.append("(define ID_" + var.getId() + "::int)\n");
    }
    
    for (int i = 0; i < pc.getConstraints().size(); i++) {
      SymBool clause = pc.getConstraints().get(i);
      int weight = pc.weights.get(i);
      
      YicesMaxSatVisitor visitor = new YicesMaxSatVisitor();
      String yicesExpr = clause.accept(visitor);
      sb.append("(assert+ " + yicesExpr + " " + weight + ")\n");
    }
    
    sb.append("(max-sat)");
    
    return sb.toString();
  }
  
  @Override
  protected String visitSymConstant(SymBoolConstant arg) {
    return arg.evalBool() + "";
  }

  @Override
  protected String visitSymRel(SymIntRelational arg) {
    String a1 = arg.getA().accept(this);
    String a2 = arg.getB().accept(this);
    String op;
    if (arg.getOp() == SymIntRelational.NE) {
      op = "/=";
    } else if (arg.getOp() == SymIntRelational.EQ) {
      op = "=";
    } else {
      op = arg.getSymbolOp();
    }
    
    return "(" + op + " " + a1 + " " + a2 + ")";
  }

  @Override
  protected String visitSymRel(SymLongRelational arg) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  protected String visitSymRel(SymFloatRelational arg) {
    throw new RuntimeException("Not implemented");  
  }

  @Override
  protected String visitSymRel(SymDoubleRelational arg) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  protected String visitSymConstant(SymIntConstant arg) {
    return arg.eval() + "";
  }

  @Override
  protected String visitSymIntLiteral(SymIntLiteral arg) {
    return "ID_" + arg.getId();
  }

  @Override
  protected String visitSymIntArith(SymIntArith arg) {
    String a1 = arg.getA().accept(this);
    String a2 = arg.getB().accept(this);
    String op = arg.getSymbolOp();
    
    return "(" + op + " " + a1 + " " + a2 + ")";
  }

  @Override
  protected String visitSymDoubleConst(SymDoubleConstant arg) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymDoubleLiteral(SymDoubleLiteral arg) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymDoubleArith(SymDoubleArith arg) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  protected String visitSymFloatConst(SymFloatConstant arg) {
    throw new RuntimeException("Not implemented");}

  @Override
  protected String visitSymFloatLiteral(SymFloatLiteral arg) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymArith(SymFloatArith arg) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymLongConst(SymLongConstant arg) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymLongLiteral(SymLongLiteral arg) {
    throw new RuntimeException("Not implemented");  }

  private static final String[] BOOL_OP = new String[] {"and", "or", "not"} ;
  @Override
  protected String visitSymBoolOperations(SymBoolOperations arg) {
    if (arg.getOp() != SymBoolOperations.NEG) {
      String a1 = arg.getA().accept(this);
      String a2 = arg.getB().accept(this);
      String op = BOOL_OP[arg.getOp()];
      
      return "(" + op + " " + a1 + " " + a2 + ")";
    } else {
      String a1 = arg.getA().accept(this);
      
      return "(not " + a1 + ")";
    }
  }

  @Override
  protected String visitSymBoolRelational(SymBoolRelational arg) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymMathUnary(SymMathUnary symDoubleJavaMath) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymMathBinary(SymMathBinary symDouble) {
    throw new RuntimeException("Not implemented");  }

  @Override
  protected String visitSymCast(SymCast symCast) {
    return symCast.getArg().accept(this);
  }

}
