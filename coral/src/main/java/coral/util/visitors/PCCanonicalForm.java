package coral.util.visitors;

import java.util.HashMap;
import java.util.Map;

import coral.PC;
import coral.util.visitors.interfaces.TypedVisitor;

import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import symlib.Util;
import symlib.parser.ParseException;
import symlib.parser.Parser;


public class PCCanonicalForm implements TypedVisitor {

  private int varCounter;
  private Map<SymLiteral, SymLiteral> varsVisited;

  public PCCanonicalForm(){
    varCounter = 1;
    varsVisited = new HashMap<SymLiteral, SymLiteral>();
  }

  /* BOOLEAN */
  
  @Override
  public SymBool visitSymBool(SymBool bool) {
    if (bool instanceof SymBoolConstant) {
      return bool;
    } else if (bool instanceof SymBoolLiteral){
      return visitSymLiteral((SymBoolLiteral) bool);
    } else if (bool instanceof SymBoolRelational){
      return visitSymRelational((SymBoolRelational) bool);
    } else if (bool instanceof SymIntRelational) {
      return visitSymRelational((SymIntRelational) bool);
    } else if (bool instanceof SymDoubleRelational) {
      return visitSymRelational((SymDoubleRelational) bool);
    } else if (bool instanceof SymBoolOperations) {
      return visitSymBoolOperations((SymBoolOperations) bool);
    } else if (bool instanceof SymLongRelational){
      return visitSymRelational((SymLongRelational) bool);
    } else if (bool instanceof SymFloatRelational){
      return visitSymRelational((SymFloatRelational) bool);
    } else {
      throw new RuntimeException("missing case: " + bool.getClass());
    }
  }

  private SymBool visitSymRelational(SymBoolRelational bool) {
    SymBool a = visitSymBool(bool.getA());
    SymBool b = visitSymBool(bool.getB());
    return SymBoolRelational.create(a, b, bool.getOp());
  }

  private SymBool visitSymLiteral(SymBoolLiteral bool) {
    return (SymBool) canonicalize(bool);
  }

  private SymBool visitSymBoolOperations(SymBoolOperations bool) {
    int op = bool.getOp();
    SymBool sb;
    if(op == SymBoolOperations.NEG) {
      sb = SymBoolOperations.create(visitSymBool(bool.getA()), null, bool.getOp());
    } else {
      sb = SymBoolOperations.create(visitSymBool(bool.getA()), visitSymBool(bool.getB()), bool.getOp());
    }
    return sb;
  }

  /* INT */
  
  private SymInt visitSymInt(SymInt arg) {
    SymInt result;
    if (arg instanceof SymIntLiteral) {
      result = visitSymLiteral((SymIntLiteral) arg);
    } else if (arg instanceof SymIntArith) {
      result = visitSymIntArith((SymIntArith)arg);
    } else if (arg instanceof SymIntConstant) {
      result = arg;
    } else if (arg instanceof SymAsInt){
      result = visitSymAsInt((SymAsInt)arg);
    } else {
      throw new UnsupportedOperationException("missing case:" + arg.getClass());
    }
    return result;
  }

  private SymBool visitSymRelational(SymIntRelational bool) {
    return new SymIntRelational(visitSymInt(bool.getA()), visitSymInt(bool.getB()), bool.getOp());
  }

  private SymInt visitSymLiteral(SymIntLiteral t) {
    SymInt result;
    result = (SymInt) canonicalize(t);
    return result;
  }
  
  private SymInt visitSymAsInt(SymAsInt t) {
    SymNumber arg = visitSymNumber(t.getArg());
    return Util.createASInt(arg);
  }

  private SymInt visitSymIntArith(SymIntArith arg) {
    SymInt oldArg1 = visitSymInt(arg.getA());
    SymInt oldArg2 = visitSymInt(arg.getB());
    return SymIntArith.create(oldArg1, oldArg2, arg.getOp());
  }

  /* FLOAT */
  
  private SymFloat visitSymFloat(SymFloat sym) {
    SymFloat result;
    if (sym instanceof SymFloatArith) {
      result = visitSymArith((SymFloatArith) sym);
    } else if (sym instanceof SymFloatConstant) {
      result = sym;
    } else if (sym instanceof SymFloatLiteral) {
      result = visitSymLiteral((SymFloatLiteral) sym);
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }
  
  private SymBool visitSymRelational(SymFloatRelational bool) {
    SymFloat a = visitSymFloat(bool.getA());
    SymFloat b = visitSymFloat(bool.getB());
    return SymFloatRelational.create(a, b, bool.getOp());
  }

  private SymFloat visitSymLiteral(SymFloatLiteral sym) {
    SymFloat result = (SymFloat) canonicalize(sym);
    return result;
  }

  private SymFloat visitSymArith(SymFloatArith sym) {
    SymFloat a = visitSymFloat(sym.getA());
    SymFloat b = visitSymFloat(sym.getB());
    return SymFloatArith.create(a, b, sym.getOp());
  }
  
  /* LONG */ 

  private SymLong visitSymLong(SymLong sym) {
    SymLong result;
    if (sym instanceof SymLongArith) {
      result = visitSymArith((SymLongArith) sym);
    } else if (sym instanceof SymLongConstant) {
      result = sym;
    } else if (sym instanceof SymLongLiteral) {
      result = visitSymLiteral((SymLongLiteral) sym);
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }
  
  private SymLong visitSymLiteral(SymLongLiteral sym) {
    SymLong result = (SymLong) canonicalize(sym);
    return result;
  }

  private SymLong visitSymArith(SymLongArith sym) {
    SymLong a = visitSymLong(sym.getA());
    SymLong b = visitSymLong(sym.getB());
    return SymLongArith.create(a, b, sym.getOp());
  }

  private SymBool visitSymRelational(SymLongRelational bool) {
    SymLong a = visitSymLong(bool.getA());
    SymLong b = visitSymLong(bool.getB());
    return SymLongRelational.create(a, b, bool.getOp());  
  }

  /* DOUBLE */
  
  private SymDouble visitSymDouble(SymDouble t) {

    SymDouble result;
    if (t instanceof SymDoubleArith) {
      result = visitDoubleArith((SymDoubleArith)t);
    } else if (t instanceof SymMathUnary) {
      result = visitMathUnary((SymMathUnary)t);
    } else if (t instanceof SymMathBinary) {
      result = visitMathBinary((SymMathBinary)t);
    } else if (t instanceof SymDoubleConstant) {
      result = t;
    } else if (t instanceof SymDoubleLiteral) {
      result = visitSymLiteral((SymDoubleLiteral)t);
    } else if (t instanceof SymAsDouble) {
      result = visitSymAsDouble((SymAsDouble)t);
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }
  
  private SymBool visitSymRelational(SymDoubleRelational bool) {
    return new SymDoubleRelational(visitSymDouble(bool.getA()), visitSymDouble(bool.getB()), bool.getOp());
  }

  private SymDouble visitSymLiteral(SymDoubleLiteral t) {
    SymDouble result = (SymDouble) canonicalize(t);
    return result;
  }

  private SymDouble visitSymAsDouble(SymAsDouble t) {
    SymNumber arg = visitSymNumber(t.getArg());
    return Util.createASDouble(arg);
  }

  private SymDouble visitMathBinary(SymMathBinary arg) {
    SymDouble oldArg1 = visitSymDouble(arg.getArg1());
    SymDouble oldArg2 = visitSymDouble(arg.getArg2());
    return SymMathBinary.create(oldArg1, oldArg2, arg.getOp());
  }

  private SymDouble visitMathUnary(SymMathUnary arg) {
    SymDouble oldArg1 = visitSymDouble(arg.getArg());
    return SymMathUnary.create(oldArg1, arg.getOp());
  }

  private SymDouble visitDoubleArith(SymDoubleArith arg) {
    SymDouble oldArg1 = visitSymDouble(arg.getA());
    SymDouble oldArg2 = visitSymDouble(arg.getB());
    return SymDoubleArith.create(oldArg1, oldArg2, arg.getOp());    
  }

  @Override
  public SymNumber visitSymNumber(SymNumber number) { 
    if (number instanceof SymDouble) {
      return visitSymDouble((SymDouble) number);
    } else if (number instanceof SymInt) {
      return visitSymInt((SymInt) number);
    } else if (number instanceof SymLong) {
      return visitSymLong((SymLong) number);
    } else if (number instanceof SymFloat) {
      return visitSymFloat((SymFloat) number);
    } else {
      throw new RuntimeException("invalid type" + number.getClass());
    }
  }

  private SymLiteral canonicalize(SymLiteral t) {
    SymLiteral result;
    if(!varsVisited.containsKey(t)) {
      if(t instanceof SymDouble) {
        result = SymDoubleLiteral.createForParsing(varCounter++);
      } else if(t instanceof SymInt) {
        result = SymIntLiteral.createForParsing(varCounter++);
      } else if(t instanceof SymFloat) {
        result = SymFloatLiteral.createForParsing(varCounter++);
      } else if(t instanceof SymLong) {
        result = SymLongLiteral.createForParsing(varCounter++);
      } else if(t instanceof SymBool) {
        result = SymBoolLiteral.createForParsing(varCounter++);
      } else {
        throw new RuntimeException("Unknown literal type: " + t.getClass());
      }
      varsVisited.put(t, result);
    }
    else {
      result = varsVisited.get(t);
    }
    return result;
  }

  public static void main(String[] args) throws ParseException {
    String query = "DLE(SUB(MUL(DCONST(1.0),DCONST(1.0)),DVAR(ID_12)),DCONST(0.0));DEQ(DVAR(ID_12),DVAR(ID_14))";
    PC pc = new Parser(query).parsePC();
    System.out.println(pc);
    System.out.println(pc.getCanonicalForm());
    query = "ILE(SUB(MUL(ICONST(1),ICONST(1)),IVAR(ID_12)),ICONST(0));IEQ(IVAR(ID_12),IVAR(ID_14))";
    pc = new Parser(query).parsePC();
    System.out.println(pc);
    PC pc2 = pc.getCanonicalForm();
    System.out.println(pc2);
  }

}
