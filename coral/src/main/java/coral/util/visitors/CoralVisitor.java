package coral.util.visitors;

import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymCast;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.parser.ParseException;
import coral.PC;
import coral.tests.Benchmark;
import coral.util.visitors.interfaces.StringVisitor;

public class CoralVisitor extends StringVisitor {

  @Override
  protected String visitSymConstant(SymBoolConstant arg) {
    return "BCONST(" + arg.evalBool() + ")";
  }

  @Override
  protected String visitSymRel(SymIntRelational arg) {
    int opCode = arg.getOp();
    String a = arg.getA().accept(this);
    String b = arg.getB().accept(this);
    String op = SymIntRelational.logSymbols[opCode];
    
    return op + "(" + a + "," + b + ")";
  }

  @Override
  protected String visitSymRel(SymLongRelational arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymRel(SymFloatRelational arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymRel(SymDoubleRelational arg) {
    int opCode = arg.getOp();
    String a = arg.getA().accept(this);
    String b = arg.getB().accept(this);
    String op = SymDoubleRelational.logSymbols[opCode];
    
    return op + "(" + a + "," + b + ")";
  }

  @Override
  protected String visitSymConstant(SymIntConstant arg) {
    return "ICONST("+arg.eval()+")";
  }

  @Override
  protected String visitSymIntLiteral(SymIntLiteral arg) {
    return "IVAR(ID_" + arg.getId() + ")";
  }

  @Override
  protected String visitSymIntArith(SymIntArith arg) {
    int opCode = arg.getOp();
    String a = arg.getA().accept(this);
    String b = arg.getB().accept(this);
    String op = SymIntArith.logSymbols[opCode];
    
    return op + "(" + a + "," + b + ")";
  }

  @Override
  protected String visitSymDoubleConst(SymDoubleConstant arg) {
    return "DCONST("+arg.eval()+")";
  }

  @Override
  protected String visitSymDoubleLiteral(SymDoubleLiteral arg) {
    return "DVAR(ID_" + arg.getId() + ")";
  }

  @Override
  protected String visitSymDoubleArith(SymDoubleArith arg) {
    int opCode = arg.getOp();
    String a = arg.getA().accept(this);
    String b = arg.getB().accept(this);
    String op = SymDoubleArith.logSymbols[opCode];
    
    return op + "(" + a + "," + b + ")";
  }

  @Override
  protected String visitSymFloatConst(SymFloatConstant arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymFloatLiteral(SymFloatLiteral arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymArith(SymFloatArith arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymLongConst(SymLongConstant arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymLongLiteral(SymLongLiteral arg) {
    throw new RuntimeException("Not done yet");
  }

  @Override
  protected String visitSymBoolOperations(SymBoolOperations arg) {
    int opCode = arg.getOp();
    String a = arg.getA().accept(this);
    String b;
    
    if(opCode != SymBoolOperations.NEG) {
      b = arg.getB().accept(this);
    } else { //neg. is an unary expression
      b = "";
    }
    
    String result;
    
    if(opCode == SymBoolOperations.AND) {
      result = "BAND(" + a + "," + b + ")";
    } else {
      String op = SymBoolOperations.logSymbols[opCode];

      if (opCode != SymBoolOperations.NEG) {    
        result = op + "(" + a + "," + b + ")";
      } else {
        result = op + "(" + a + ")";
      }
    }
    return result;
  }

  @Override
  protected String visitSymBoolRelational(SymBoolRelational arg) {
    int opCode = arg.getOp();
    String a = arg.getA().accept(this);
    String b = arg.getB().accept(this);
    String op = SymBoolRelational.logSymbols[opCode];
    
    return op + "(" + a + "," + b + ")";
  }

  @Override
  protected String visitSymMathUnary(SymMathUnary arg) {
    int opCode = arg.getOp();
    String a = arg.getArg().accept(this);
    String op = SymMathUnary.logSymbols[opCode];
    
    return op + "(" + a +")";
  }

  @Override
  protected String visitSymMathBinary(SymMathBinary arg) {
    int opCode = arg.getOp();
    String a = arg.getArg1().accept(this);
    String b = arg.getArg2().accept(this);
    String op = SymMathBinary.logSymbols[opCode];
    
    return op + "(" + a + "," + b + ")";
  }

  @Override
  protected String visitSymCast(SymCast cast) {
    String arg = cast.getArg().accept(this);
    if (cast instanceof SymAsDouble) {
      return "ASDOUBLE(" + arg + ")";
    } else if (cast instanceof SymAsInt) {
      return "ASINT(" + arg + ")";
    } else {
      throw new RuntimeException("Missing case: " + cast.getClass());
    }
  }

  public static String processPC(PC pc) {
      StringBuffer processed = new StringBuffer();
      CoralVisitor cv = new CoralVisitor();
      for(SymBool bool : pc.getConstraints()) {
        processed.append(bool.accept(cv));
        processed.append(';');
      }
      processed.deleteCharAt(processed.length()-1);
      return processed.toString();
  }
}
