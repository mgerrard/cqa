package coral.util.trees;

public abstract class TrieVisitor<T,W> {

  public abstract void visitNode(TrieNode<T,W> node);
  
}
