package coral.util.trees;

import java.util.ArrayDeque;
import java.util.Deque;

public abstract class TrieWalker<T,W,R> {
  
  public abstract void doSomethingWithTheRoot(TrieNode<T,W> node);
  
  public abstract void doSomething(TrieNode<T,W> node);
  
  public void walk(Trie<T,W> trie) {
    TrieNode<T,W> root = trie.root;
    doSomethingWithTheRoot(root);
    
    Deque<TrieNode<T,W>> stack = new ArrayDeque<TrieNode<T,W>>();
    for (TrieNode<T,W> child : root.children.values()) {
      stack.push(child);
    }
    
    while (!stack.isEmpty()) {
      TrieNode<T,W> current = stack.pop();
        doSomething(current);
        
        for (TrieNode<T,W> child : current.children.values()) {
          stack.push(child);
        }
    }
  }
}
