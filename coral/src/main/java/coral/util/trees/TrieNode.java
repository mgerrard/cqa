package coral.util.trees;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TrieNode<T,W> {

  public T object;
  public W payload;
  public boolean marked = false;
  public Map<T, TrieNode<T,W>> children;

  public TrieNode(T data) {
      this.object = data;
      this.payload = null;
      children = new LinkedHashMap<T, TrieNode<T,W>>();
  }
  
  public TrieNode(T data,W payload) {
    this.object = data;
    this.payload = payload;
    children = new LinkedHashMap<T, TrieNode<T,W>>();
  }
  
  public void setPayload(W payload) {
    this.payload = payload;
  }
  
  public boolean isLeaf() {
    return children.isEmpty();
  }

  public TrieNode<T,W> getChild(T object) {
      if (children != null) {
          if (children.containsKey(object)) {
              return children.get(object); 
          }
      }
      return null;
  }  
  
  public String toString(int i) {
    StringBuffer sb = new StringBuffer();
    String padding = "";
    for (int j = 0; j < i; j++) {
      padding = padding + "  ";
    }
    
    sb.append(padding + "> " + object + " : " + payload + "\n");
    
    for(Entry<T, TrieNode<T, W>> entry : children.entrySet()) {
      sb.append(entry.getValue().toString(i+1));
    }
    
    return sb.toString();
  }
  
  public void accept(TrieVisitor<T, W> visitor) {
    visitor.visitNode(this);
  }
}