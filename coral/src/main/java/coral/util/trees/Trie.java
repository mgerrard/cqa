package coral.util.trees;

import java.util.List;

public class Trie<T,W> {

  /**
   * The payload on this node is ignored
   */
  public TrieNode<T,W> root;

  public Trie() {
    this.root = new TrieNode<T,W>(null);
  }
  
  public Trie(T t,W w) {
    this.root = new TrieNode<T,W>(t,w);
  }
  
  public void insert(List<T> objects,W payload) {

    //preconditions
    assert objects != null;
    assert objects.size() > 0;
    
    TrieNode<T,W> current = this.root;
    
    for (T object : objects) {
      TrieNode<T,W> child = current.getChild(object);

      if (child != null) {
        current = child;
      } else {
        TrieNode<T,W> newNode = new TrieNode<T,W>(object);
        current.children.put(object, newNode);
        current = newNode;
      }
    }
    
    current.setPayload(payload);
  }
  
  public void insertList(List<T> objects,List<W> payloads) {

    //preconditions
    assert objects != null;
    assert payloads != null;
    assert objects.size() > 0;
    assert objects.size() == payloads.size();
    
    TrieNode<T,W> current = this.root;
    
    for (int i = 0; i < objects.size(); i++) {
      T object = objects.get(i);
      W payload = payloads.get(i);
      TrieNode<T,W> child = current.getChild(object);

      if (child != null) {
        current = child;
      } else {
        TrieNode<T,W> newNode = new TrieNode<T,W>(object);
        current.children.put(object, newNode);
        current = newNode;
      }
      current.setPayload(payload);
    }
  }

  public W searchPrefix(List<T> objects) {

    TrieNode<T,W> current = this.root;
    W returnValue = null;

    for (T object : objects) {
      TrieNode<T,W> child = current.getChild(object);
      returnValue = current.payload;
      
      if(child == null) {
        break;
      } else {
        current = child;
      }
    }
    
    returnValue = current.payload;
    return returnValue;
  }
  
  public String toString() {
    return root.toString(0);
  }
  
}
