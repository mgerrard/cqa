package coral.util;

import java.util.List;

import coral.PC;

public class PCSolutions<T> {

  private PC pc;
  private List<T> solutions;
  
  public PCSolutions(PC pc, List<T> solutions) {
    this.pc = pc;
    this.solutions = solutions;
  }

  public PC getPc() {
    return pc;
  }

  public List<T> getSolution() {
    return solutions;
  }
}
