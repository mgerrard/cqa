package coral;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import symlib.SymBool;

/**
 * PC + weights for each clause.
 * 
 * @author mateus
 * 
 */

public class WeightedPC extends PC {

  public final List<Integer> weights;

  public WeightedPC(List<SymBool> constraints, List<Integer> weights) {
    super(constraints);
    
    if (constraints.size() != weights.size()) {
      throw new RuntimeException("WeightedPC: constraints.size() != weights.size()");
    }
    
    this.weights = weights;
  }
  
  public WeightedPC(List<SymBool> constraints) {
    super(constraints);
    weights = new ArrayList<Integer>(constraints.size());
    for (int i = 0; i < constraints.size(); i++) {
      weights.add(0);
    }
  }

  public WeightedPC() {
    super();
    weights = new ArrayList<Integer>();
  }
}