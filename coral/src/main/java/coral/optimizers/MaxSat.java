package coral.optimizers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import com.google.common.io.Files;

import symlib.SymBinaryExpression;
import symlib.SymBool;
import symlib.SymConstant;
import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.Util;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.WeightedPC;
import coral.simplifier.Simplify;
import coral.solvers.Env;
import coral.solvers.PartitionedCache;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.tests.Helper;
import coral.util.Config;
import coral.util.visitors.CoralVisitor;
import coral.util.visitors.YicesMaxSatVisitor;

public class MaxSat {

  public abstract static class WeightedPartitionedCache extends PartitionedCache {
    protected Map<SymBool, Integer> weightMap;
  }
  
  public static class Result {
    public final int cost;
    public final Set<SymBool> maxSat;
    public final Set<SymBool> minUnsat;

    public Result(int cost, Set<SymBool> maxSat, Set<SymBool> minUnsat) {
      super();
      this.cost = cost;
      this.maxSat = maxSat;
      this.minUnsat = minUnsat;
    }
  }
  
  @Test
  public void testMaxSat() throws ParseException {
    Solver solver = SolverKind.PSO_OPT4J.get();
    PC pc = (new Parser("DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(DVAR(ID_3),DVAR(ID_4)));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DCONST(6)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))));DGT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)))")).parsePC();
    Env env = solver.call(pc);

    Set<SymBool> maxSat = new HashSet<SymBool>();
    Set<SymBool> minUnsat = new HashSet<SymBool>();

    for (SymLiteral lit : pc.getSortedVars()) {
      lit.setCte(env.getValue(lit).evalNumber());
    }
    
    for (SymBool clause : pc.getConstraints()) {
      ReversePolish revPol = GenReversePolishExpression.createReversePolish(clause);
      //TODO add support to compiled constraints
      double tempValue = revPol.eval()/*0 or 1*/.doubleValue();
      
      if (tempValue == 1) {
        maxSat.add(clause);
      } else {
        minUnsat.add(clause);
      }
    }
    
    System.out.println("MAXSAT: " + maxSat.toString());
    System.out.println("MINUNSAT: " + minUnsat.toString());
    
//    (guard1 = (x1 ≥ 0)) ∧ (a1 = x1 )∧
//    ((guard1 ∧ (a3 = a1 )) ∨ (¬guard1 ∧ (a3 = a2 )))∧
//    (guard2 = (y1 < 5)) ∧ (b1 = a3 + 1)∧
//    ((guard2 ∧ (b3 = b1 )) ∨ (¬guard2 ∧ (b3 = b2 )))∧ 
//    (b2 = a3 + 2)

  }

  public static final boolean SIMPLIFY = true;
  
  public static void main(String[] args) throws Exception {
    Config.pcCanonicalization = false;
    Config.partitionConstraints = false;
    Config.cacheSolutions = true;
    Config.storeLastIndividuals = true;
    Config.stagnationLimit = 150;
    Config.maxInsertedIndividuals = 1;
    Config.normValue = 1000;
    Config.rangeHI = 1;
    Config.rangeLO = -1;

    WeightedPartitionedCache pcache = prepareCache();
    
    for (int fileId = 0; fileId <= 0; fileId++) {
      Config.seed = 1212554546454l;
      long start = System.nanoTime();
      //parse file
//      String filename = "inputs/odin/tc10iter10formula" + fileId + ".ys.coral";
      String filename = "inputs/odin/complexformula/v4/formula" + fileId + ".ys.coral";
//      String filename = "inputs/odin/tmp";
//      String filename = "inputs/odin/small";
      System.out.println("[coral] processing file: " + filename);
      BufferedReader br = new BufferedReader(new FileReader(filename));
      List<Integer> weights = new ArrayList<Integer>();
      StringBuffer pcs = new StringBuffer();
      String s;
      while ((s = br.readLine()) != null) {
        s = s.trim();
        if (s.equals("") || s.startsWith("#")) {
          continue;
        }
        String[] toks = s.split("##");
        int weight = Integer.parseInt(toks[0]);
        weights.add(weight);
        String formula = toks[1];
        pcs.append(formula);
        pcs.append(';');
      }
      br.close();
      pcs.deleteCharAt(pcs.length() - 1);
      
      PC pc = (new Parser(pcs.toString())).parsePC();
//      searchUnsatWithYices(weights, pc);
      runMaxSat(pcache, start, weights, pc);
    }
    pcache.clearCache();
  }

  private static void searchUnsatWithYices(List<Integer> weights, PC pc)
      throws IOException, FileNotFoundException, InterruptedException {

    //check if original pc is SAT
    WeightedPC originalPC = new WeightedPC(pc.getConstraints(), weights);
    String originalYicesInput = YicesMaxSatVisitor.getYicesInput(originalPC);
    int cost = runYices(originalYicesInput);
    
    if (cost == 0) {
      System.out.println("[yices] Original pc is SAT!");
    } else {
      System.out.println("[yices] Original pc is UNSAT! MAXSAT cost: " + cost);
      //simplify pc
      Simplify.Data sdata = Simplify.simplify(pc);
      PC simPc = sdata.pc;
  
      WeightedPC tmpPC = new WeightedPC(simPc.getConstraints(), weights);
      String yicesInString = YicesMaxSatVisitor.getYicesInput(tmpPC);
  
      cost = runYices(yicesInString);
      if (cost == 0) {
        System.out.println("[yices] Simplified pc is SAT!");
      } else {
        System.out.println("[yices] Simplified pc is UNSAT!");
      }
    }
  }

  private static int runYices(String yicesInString) throws IOException,
      FileNotFoundException, InterruptedException {
    int cost = -1;
    File tmpF = File.createTempFile("bla", null);
    BufferedWriter bw = Files.newWriter(tmpF, Charset.defaultCharset());
    bw.write(yicesInString);
    bw.close();
    ProcessBuilder pb = new ProcessBuilder("yices","-e", tmpF.getCanonicalPath());
    Process p = pb.start();
    p.waitFor();
    BufferedReader is = new BufferedReader(new InputStreamReader(
        p.getInputStream()));
    String line;

    while ((line = is.readLine()) != null) {
      if (line.contains("cost")) {
        cost = Integer.parseInt(line.split(" ")[1]);
      }
    }
    
    if (cost < 0) {
      throw new RuntimeException("Error while calling yices");
    } else {
      return cost;
    }
  }

  public static void runMaxSat(WeightedPartitionedCache pcache, long start,
      List<Integer> weights, PC pc) throws Exception {

    PC simPc;
    Env simMap;
    
    if (SIMPLIFY) {
      Simplify.Data sdata = Simplify.simplify(pc);
      simPc = sdata.pc;
      simMap = sdata.mapping;
    } else {
      simPc = pc;
      simMap = null;
    }
    
    /**
     * equivalence classes
     */
    // associative arrays/lists
    List<Set<SymNumber>> clusters = new ArrayList<Set<SymNumber>>();
    List<Set<String>> ids = new ArrayList<Set<String>>();
    Map<Set<SymNumber>, Number> ctes = new HashMap<Set<SymNumber>, Number>();
//    computeEquivalenceSets(simMap, clusters, ids, ctes);

    // printing equivalence classes
    System.out.println("==== equivalence classes of expressions");
    for (Set<String> set : ids) {
      System.out.println(set);
    }
    System.out.println("====");
    
    //TODO fix cost mapping
    WeightedPC wpc = new WeightedPC(simPc.getConstraints(), weights);
    Result result = solveAndProcess(pcache, wpc, pc, simMap);
    
    System.out.println("[coral] MAXSAT: " + result.maxSat.size());
    System.out.println("[coral] MINUNSAT: " + result.minUnsat.size());
    System.out.println("[coral] COST: " + result.cost);
    Config.showConcreteValues = true;
    System.out.println("[coral] VARMAP:" + pc.getVars());
    Config.showConcreteValues = false;
    System.out.println("[coral] Time (ms): " +(System.nanoTime() - start) / 1000000.0);
    
    //translate to coral format the unsat core
    PC tmpPc = new PC(new ArrayList<SymBool>(result.minUnsat));
    String coralInput = CoralVisitor.processPC(tmpPc);
    System.out.println("[coral] MINUNSAT (coral format): " + coralInput);
  }

  public static void computeEquivalenceSets(Env simMap,
      List<Set<SymNumber>> clusters, List<Set<String>> ids,
      Map<Set<SymNumber>, Number> ctes) {
    for (Entry<SymLiteral, SymNumber> entry : simMap.getMapLiteralToNumber().entrySet()) {
      boolean found = false;
      SymNumber key = (SymNumber) entry.getKey();
      SymNumber value = entry.getValue();
      for (int i = 0; i < ids.size(); i++) {
        Set<String> set = ids.get(i);
        if (set.contains(key.toString()) || set.contains(value.toString())) {
          set.add(key.toString()); set.add(value.toString());
          Set<SymNumber> eqClass = clusters.get(i);
          eqClass.add(key); eqClass.add(value);
          Number cte =  key instanceof SymConstant ? key.evalNumber() : value instanceof SymConstant ? value.evalNumber() : null;
          if (cte != null) {
            if (ctes.get(eqClass) != null) {
              throw new RuntimeException("UNSAT!");
            }
            ctes.put(eqClass, cte);
          }
          found = true;
          break;
        }
      }
      if (!found) {
        // adding objects
        Set<SymNumber> eqClass = new HashSet<SymNumber>();
        clusters.add(eqClass);
        eqClass.add(key); eqClass.add(value);
        // adding ids
        Set<String> set = new HashSet<String>();
        ids.add(set);
        set.add(key.toString()); set.add(value.toString());
      }
    }

    // simplifying expressions
    for (int i = 0; i < clusters.size(); i++) {
      Set<SymNumber> expr = clusters.get(i);
      Set<SymNumber> others = new HashSet<SymNumber>();
      for (SymNumber sn : expr) {
        if (sn instanceof SymBinaryExpression) {
          SymBinaryExpression binExpr = (SymBinaryExpression) sn;
          SymNumber a = binExpr.getA(); SymNumber b = binExpr.getB();
          Number val = null;
          if (a instanceof SymLiteral) {
            for (Set<SymNumber> t : clusters) {
              if (t == expr) continue;
              if (t.contains(a)) {
                Number n = ctes.get(t);
                if (n != null) {
                  ((SymLiteral) a).setCte(n);
                  val = sn.evalNumber();
                  break;
                }
              }
            }
          } else if (b instanceof SymLiteral) {
            for (Set<SymNumber> t : clusters) {
              if (t == expr) continue;
              if (t.contains(b)) {
                Number n = ctes.get(t);
                if (n != null) {
                  ((SymLiteral) b).setCte(n);
                  val = sn.evalNumber();
                  break;
                }
              }
            }
          }
          if (val != null) {
            if (ctes.get(expr) != null) {
              throw new RuntimeException("UNSAT!");
            }
            SymNumber cte = Util.createConstant(val); 
            others.add(cte);
            ctes.put(expr, val);
            ids.get(i).add(cte.toString());
          }
        }
      }
      expr.addAll(others);
    }
  }

  public static int computeCost(PC pc, WeightedPC wpc, Set<SymBool> maxSat,
      Set<SymBool> minUnsat) {
    int cost = 0;
    for (int i = 0; i < pc.getConstraints().size(); i++) {
      SymBool clause = pc.getConstraints().get(i);
      int weight = wpc.weights.get(i);
      ReversePolish revPol = GenReversePolishExpression.createReversePolish(clause);
      //TODO add support to compiled constraints
      double tempValue = revPol.eval()/*0 or 1*/.doubleValue();
      
      if (tempValue == 1) {
        maxSat.add(clause);
      } else {
        minUnsat.add(clause);
        cost += weight;
      }
    }
    return cost;
  }

  public static Result solveAndProcess(WeightedPartitionedCache pcache, WeightedPC wpc, PC pc, Env simMap)
      throws Exception {
    Result result = null;
    
    //TODO extract constant
    for (int i = 0; i < 3; i++) {
      Env env;
      boolean restart = false;
      
      if (Config.partitionConstraints) {
        Map<SymBool, Integer> weightMap = new HashMap<SymBool, Integer>();
        for (int j = 0; j < wpc.weights.size(); j++) {
          weightMap.put(wpc.getConstraints().get(j), wpc.weights.get(j));
        }
        pcache.weightMap = weightMap;
        env = pcache.analyzeOne(wpc.getConstraints());
      } else {
        Solver solver = SolverKind.PSO_OPT4J.get();
        env = solver.call(wpc);
        if (solver.isTerminated()) {
          restart = true;
        }
      }
      
      Env combined = Env.combineEnv(simMap, env, env.getSolverKind());
      
      //Do it twice - some values may depend on others
      setValues(pc, combined);
      
      Set<SymBool> maxSat = new HashSet<SymBool>();
      Set<SymBool> minUnsat = new HashSet<SymBool>();
      
      int cost = computeCost(pc, wpc, maxSat, minUnsat);
      
      if (result == null || result.cost > cost) {
        result = new Result(cost, maxSat, minUnsat);
      } 
      
      if (restart) {
        System.out.println("Restarting (incrementing seed)...");
        Config.seed = Config.seed + 17;
      } else {
        break;
      }
    }
    
    return result;
  }

  private static void setValues(PC pc, Env combined) {
    for (int i = 0; i < 2; i++) {
      for (SymLiteral lit : pc.getSortedVars()) {
        SymNumber val = combined.getValue(lit);
        if (val != null) {
          lit.setCte(val.evalNumber());
        } else { //probably the variable was removed in the rewrite;
          System.err.println("Warning: var " + lit + " not found in env; assuming a value of zero");
          //TODO this is just a quick hack. need to check bounds and choose an apropriate value   
          lit.setCte(0);
        }
      }
    }
  }

  private static WeightedPartitionedCache prepareCache() {
    return new WeightedPartitionedCache() {
      @Override
      public Env processConstraint(Set<SymBool> cons) throws Exception {
        System.out.println(cons.size());
        List<SymBool> conlist = new ArrayList<SymBool>(cons); 
        List<Integer> weights = new ArrayList<Integer>(conlist.size());
        
        for (int i = 0; i < conlist.size(); i++) {
          SymBool tmp = conlist.get(i);
          weights.add(weightMap.get(tmp));
        }
        
        WeightedPC wpc = new WeightedPC(conlist, weights);
//          return Helper.solveAndPrint(solver, wpc);
        Solver solver = SolverKind.PSO_OPT4J.get();
        return Helper.solveAndPrint(solver, new PC(wpc.getConstraints()));
      }
    };
  }
}
