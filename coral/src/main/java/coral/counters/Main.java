package coral.counters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import symlib.SymIntLiteral;
import symlib.SymLiteral;
import symlib.SymLongLiteral;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.counters.MCUtil.ORComposer;
import coral.counters.ResultData.Entry;
import coral.counters.estimates.EstimateResult;
import coral.counters.estimates.PCEstimateResult;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;
import coral.util.options.Option;
import coral.util.options.Options;
import coral.util.visitors.StatsVisitor;

public class Main {

  private static Options options = new Options(Main.class, Config.class);

  @Option("Makes the domain of all variables = [-100,100]")
  public static boolean FIXED_100_DOMAIN = false; 
  
  private static final String DOMAIN = "{-100,100}";
  private static final boolean REPORT_STATS_CONS = true;

  @Option("read domains from file")
  public static boolean USE_DOMAINS_FILE = false;

	@Option("run with Mathematica")
	public static boolean mcUseMathematica = false;

	@Option("use NProbability to compute probabilities")
  public static boolean mcUseNProbability = true;
	
	@Option("normalize results")
	public static boolean normalize = true;

  @Option("partition/cache constraints sent to the selected model counter")
  public static boolean mcPartitionAndCache = false;
  
  @Option("disable caching")
  public static boolean mcDisableCache = false;
  
  @Option("call interval solver for each partition")
  public static boolean mcCallIntSolPartition = false;

  @Option("Number of times to run the model counter")
  public static int mcNumExecutions = 1;
  
  @Option("File with seeds to use for reseeding the rng in each run - make sure to have enough seeds!")
  public static String mcSeedFile = "";
  
  @Option("Reset rng at each simulation - review this with antonio later")
  public static boolean mcResetRngEachSimulation = true;
  
  @Option("Estimate using discretization (antonio version)")
  public static boolean mcDiscretize = false;

  @Option("Estimate using discretization (mateus version)")
  public static boolean mcDiscretizeWithMateusApproach = false;

  /**
   * Run the counter 30 times and return the volume estimate/error 
   * @param constraints
   * @param domain
   * @param mc
   * @return
   */

  //TODO refactor it to only take a single domain array
  public static double[] runRegCoral(List<PC> constraints, List<Interval[]> domains) {
        
    List<Double> results = new ArrayList<Double>();
    List<Double> antonioVarianceEstimates = new ArrayList<Double>();
    int times = mcNumExecutions;
    boolean reseedEachRun = !mcSeedFile.isEmpty();

    Scanner seedScan = null;
    if (reseedEachRun) {
      try {
        System.out.println("[regCoral] using seeds from file: " + mcSeedFile);
        seedScan = new Scanner(new File(mcSeedFile));
      } catch (FileNotFoundException e) {
        throw new RuntimeException(e);
      }
    }
    
    ModelCounter mc = getModelCounter();
    if(mc instanceof CachedCounter){
      long timeStart = System.nanoTime();
      ((CachedCounter) mc).loadPartition(constraints);
      long timeEnd = System.nanoTime();
      System.out.println("[regCoral] partitioning cost (s): " + ((timeEnd - timeStart) / 1000000000.0));
    }

    for (int i = 0; i < times; i++) {
      MCUtil.ORComposer resultComposer = new ORComposer(Config.mcMaxSamples);
      ResultData.Entry entry;
      
      //reset seed/randomstream if needed
      if (reseedEachRun) {
        String newSeedLine = seedScan.nextLine();
        long newSeed = Long.parseLong(newSeedLine);
        System.out.println("[regCoral] new seed from file: " + newSeed);
        mc.reset(newSeed);
      } 
      
      int j = 0;
      for (PC pc : constraints) {
        if (mcCallIntSolPartition) {
          entry = Main.runPartitionedCounter(domains.get(j), pc, mc);
        } else {
          try {
            entry = Main.runCounter(domains.get(j), pc, mc);
          } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error running regCoral (probably realpaver-related) - " + e.getMessage());
          }  
        }
        mc.reset();
        resultComposer.compose(entry.estimateResult, entry.variance);
        j++;
      }
      double estimateTotal = resultComposer.getResult()[0];
      double varianceTotal = resultComposer.getResult()[1];

      System.out.println("[regCoral] variance estimate (antonio): " + varianceTotal);
      System.out.println("[regCoral] volume estimate: " + estimateTotal);
      results.add(estimateTotal);
      antonioVarianceEstimates.add(varianceTotal);

      if (mcPartitionAndCache) {
        ((CachedCounter)mc).clearCache();
      }
    }
    
    //TODO replace with well-tested implementations
    double avgEstimate = MCUtil.avg(results);
    double antonioVarianceEstimate = MCUtil.avg(antonioVarianceEstimates);
    // std. error of the mean according to CLT - check
    // http://mathworld.wolfram.com/CentralLimitTheorem.html for more info
    double error;
    if (results.size() < 2) {
      System.out.println("[regCoral] Skipping standard error computation; not enough samples");
      error = Double.NaN;
    } else {
      error = MCUtil.sampleStdev(results,avgEstimate) / Math.sqrt(mcNumExecutions);
    }
    System.out.println("[regCoral] simulation results: " + results.toString());
    System.out.println("[regCoral] variance estimate results: " + antonioVarianceEstimates.toString());
    return new double[]{avgEstimate,error,antonioVarianceEstimate};
  }

  public static ModelCounter getModelCounter() {
    ModelCounter mc;
    
    if (Main.mcPartitionAndCache) {
		//      if (!Config.mcMergeBoxes) {
		//        throw new RuntimeException("Partitioning only works with box merging");
		//      }
      if (Main.mcUseMathematica) {
        mc = new MathematicaMonteCarloCounter(false);
      } else {
        mc = new MonteCarloCounter();
      }
      mc = new CachedCounter(mc,mcCallIntSolPartition);
    } else {
      if (Main.mcUseMathematica) {
        mc = new MathematicaMonteCarloCounter(normalize);
      } else {
        mc = new MonteCarloCounter();
      }
    }
    
    return mc;
  }

  public static void main(String[] args) throws ParseException, IOException, InterruptedException {
    options.ignore_options_after_arg(true);
    if (args != null && args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      System.out.println(Arrays.toString(args));
    }
    
    options.printOptions();
  
    File constraints = new File(args[0]);
    String benchmarkName = args[1];
    Scanner s = new Scanner(constraints);
    ResultData executionData = new ResultData();
    ModelCounter mc = getModelCounter();
    
    int i = 0;
    while (s.hasNextLine()) {
      String cons = s.nextLine();
      
      if (cons.trim().isEmpty()) {
        continue;
      }
      

      i++;
      System.out.println("-----------------------------");      
      
      String domains = null;
      long timeStart = System.nanoTime();
      PC pc = (new Parser(cons)).parsePC();
      System.out.println(i);
      
      //TODO dumb code. should be refactored
      if (USE_DOMAINS_FILE) {
        domains= s.nextLine();
      } else { //use default domains
        int nVars = pc.getVars().size();
        String defaultDomain = "{" + Config.rangeLO + "," + Config.rangeHI + "}";
        domains = "[" + defaultDomain ;
        
        for (int j = 0; j < nVars - 1; j++) {
          domains = domains + "," + defaultDomain;
        }
        domains = domains + "]";
      }
      
      if (FIXED_100_DOMAIN) {
        int nVars = pc.getVars().size();
        domains = "[" + DOMAIN;
        
        for (int j = 0; j < nVars - 1; j++) {
          domains = domains + "," + DOMAIN;
        }
        domains = domains + "]";
      }
      
//      ResultData.Entry entry = MonteCarloCounter.runCounter(domains, pc);
      ResultData.Entry entry;
      Interval[] parsedDomain = MCUtil.parseDomain(domains);
      
      if (mcCallIntSolPartition) {
        entry = Main.runPartitionedCounter(parsedDomain, pc, mc);  
      } else {
        entry = Main.runCounter(parsedDomain, pc, mc);  
      }

      if (REPORT_STATS_CONS) {
        long tstamp = System.nanoTime();
        StatsVisitor.extractData(pc, entry);
        //remove time spent in reporting from total:
        timeStart = timeStart + (System.nanoTime() - tstamp);
      }
      double totalTime = (System.nanoTime() - timeStart) / 1000000000.0;
      
      entry.time = totalTime;
      entry.name = benchmarkName + "-" + i;
      entry.analyticResult = -1;
      executionData.addEntry(entry);
      mc.reset();
      System.out.printf("total time spent (s): %f\n",totalTime);
    }
    
    if (mc instanceof CachedCounter) {
      ((CachedCounter)mc).printCacheReport();
    }
    
    if (i == 0) {
      System.out.println("[coral] no constraints found. outputting empty csv data");
      executionData.addEntry(new Entry());
    }
    
    System.out.println("Outputting MERGED/FOLDED results in csv:");
    System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    System.out.println(executionData.fold(benchmarkName).toAggregateCSV());
    s.close();
  }

  //TODO pass domain to realpaver, instead of the configured bound (rangeHI,rangeLO)
  public static ResultData.Entry runCounter(Interval[] domains, PC pc, ModelCounter mc)
      throws IOException, InterruptedException {
    ResultData.Entry entry;
    int budget = (int) Config.mcMaxSamples;
    if (mc instanceof MonteCarloCounter) {
      entry = ((MonteCarloCounter) mc).entry;
    } else {
      entry = new Entry();
    }
    IntervalSolverCaller isolver = coral.solvers.rand.Util.getIntervalSolver(Config.intervalSolver);
    List<Interval[]> boxes;
    EstimateResult estimate;

    if (Config.mcSkipPaving) {
      estimate = mc.count(pc, domains, budget);
    } else {
      boxes = isolver.getPaving(pc,domains);
      
      if (boxes.size() == 0) {
        System.out.println("[realpaver] no solutions found for current PC!");
        estimate = PCEstimateResult.ZERO;
      } else {
        if (Config.mcMergeBoxes) {
          Interval[] singleBox = Interval.mergeIntervals(boxes);
          boxes = new ArrayList<Interval[]>();
          boxes.add(singleBox);
        }
        
        if (domains != null) {
          estimate = mc.count(pc, boxes, domains, budget);
        } else {
          estimate = mc.count(pc, boxes, budget);
        }
      }
    }
  
    if (normalize) {
      entry.estimateResult = estimate.getMean();
      entry.variance = estimate.getVariance();
    } else {
      //compute volume
      double domainVolume = Interval.computeVolume(domains,pc.getIntDomainArray()).doubleValue();
      entry.estimateResult = estimate.getMean() * domainVolume;
      //Var(a*X) = a^2*Var(X)
      entry.variance = estimate.getVariance() * domainVolume * domainVolume;
    }

    return entry;
  }

  /**
   * Run a partitioned counter without calling realpaver for each constraint
   * @param domainString
   * @param pc
   * @param mc
   * @return volume estimate
   */
  
  public static ResultData.Entry runPartitionedCounter(Interval[] domains, PC pc, ModelCounter mc) {
    EstimateResult estimate;
    ResultData.Entry entry = new Entry();
    
    if (domains != null) {
      estimate = mc.count(pc, domains,(int) Config.mcMaxSamples);
    } else {
      throw new RuntimeException("we should had received the default domain from the main function");
    }
    
    if (normalize) {
      entry.estimateResult = estimate.getMean();
      entry.variance = estimate.getVariance();
    } else {
      //compute volume
      double domainVolume = Interval.computeVolume(domains,pc.getIntDomainArray()).doubleValue();
      entry.estimateResult = estimate.getMean() * domainVolume;
      //Var(a*X) = a^2*Var(X)
      entry.variance = estimate.getVariance() * domainVolume * domainVolume;
    }
    
    return entry;
  }
  
}
