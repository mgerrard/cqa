package coral.counters;

import java.util.List;

import coral.PC;
import coral.counters.estimates.EstimateResult;
import coral.util.Interval;

public interface ModelCounter {

  public void reset();
  
  public void reset(long seed);
  
  public EstimateResult count(PC pc, List<Interval[]> boxes, int nSamples);
  
  public EstimateResult count(PC pc, Interval[] domains, int nSamples);
  
  public EstimateResult count(PC pc, List<Interval[]> boxes, Interval[] domains, int nSamples);
  
  public EstimateResult count(CountingProblem cp, int nSamples);
  
}
