package coral.counters.discretization;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;

import com.google.common.base.Preconditions;

public class Discretization {

	private static double rhobeg = 1.0;
	private static final double rhoend = 1.0e-9;
	private static final int iprint = 0;
	private static final int maxfun = 50000;
	private static final double epsilon = 1.0e-5;// Minimum interval length

	public static void main(String[] args) {

		Discretization optimizer = new Discretization();

		RealDistribution distribution = new NormalDistribution(50, 50);
		final double lb = -100.0;
		final double ub = 100.0;
		final int numOfIntervals = 6; // >=2

		double[] cuttingPoints = optimizer.findBestAllocation(distribution, lb, ub, numOfIntervals);

		System.out.println("Ranges:");
		System.out.println("[" + lb + ",\t" + cuttingPoints[0] + "] -> " + optimizer.densityHistogramElementHeight(distribution, lb, cuttingPoints[0], lb, ub)
				+ "\tPr: " + (optimizer.truncatedCDF(distribution, cuttingPoints[0], lb, ub)));
		for (int i = 1; i < numOfIntervals - 1; i++) {
			System.out.println("[" + cuttingPoints[i - 1] + ",\t" + cuttingPoints[i] + "] -> "
					+ optimizer.densityHistogramElementHeight(distribution, cuttingPoints[i - 1], cuttingPoints[i], lb, ub) + "\tPr: "
					+ (optimizer.truncatedCDF(distribution, cuttingPoints[i], lb, ub) - optimizer.truncatedCDF(distribution, cuttingPoints[i - 1], lb, ub)));
		}
		System.out.println("[" + cuttingPoints[numOfIntervals - 1 - 1] + ",\t" + ub + "] -> "
				+ optimizer.densityHistogramElementHeight(distribution, cuttingPoints[numOfIntervals - 1 - 1], ub, lb, ub) + "\tPr: "
				+ (1.0 - optimizer.truncatedCDF(distribution, cuttingPoints[numOfIntervals - 1 - 1], lb, ub)));

		System.out.println("\nMathematica format for visualization:");
		System.out.println("rect[" + lb + ", " + cuttingPoints[0] + ", " + optimizer.densityHistogramElementHeight(distribution, lb, cuttingPoints[0], lb, ub)
				+ ", x], ");
		for (int i = 1; i < numOfIntervals - 1; i++) {
			System.out.println("rect[" + cuttingPoints[i - 1] + ", " + cuttingPoints[i] + ", "
					+ +optimizer.densityHistogramElementHeight(distribution, cuttingPoints[i - 1], cuttingPoints[i], lb, ub) + ", x], ");
		}
		System.out.println("rect[" + cuttingPoints[numOfIntervals - 1 - 1] + ", " + ub + ", "
				+ optimizer.densityHistogramElementHeight(distribution, cuttingPoints[numOfIntervals - 1 - 1], ub, lb, ub) + ", x]");

	}

	private double densityHistogramElementHeight(RealDistribution distribution, double a, double b, double lb, double ub) {
		return (truncatedCDF(distribution, b, lb, ub) - truncatedCDF(distribution, a, lb, ub)) / (b - a);
	}

	private double truncatedCDF(RealDistribution distribution, double x, double lb, double ub) {
		if (x <= lb) {
			return 0.0;
		}
		if (x >= ub) {
			return 1.0;
		}
		return (distribution.cumulativeProbability(x) - distribution.cumulativeProbability(lb))
				/ (distribution.cumulativeProbability(ub) - distribution.cumulativeProbability(lb));
	}

	public double[] findBestAllocation(final RealDistribution distribution, final double lb, final double ub, final int numOfIntervals) {
		Preconditions.checkArgument(numOfIntervals >= 2);
		Calcfc calcfc = new Calcfc() {
			public double Compute(int n, int m, double[] x, double[] con) {
				con[0] = x[0] - lb;
				for (int i = 1; i < numOfIntervals - 1; i++) {
					con[i] = x[i] - x[i - 1] - epsilon;
				}
				con[numOfIntervals - 1] = ub - x[numOfIntervals - 1 - 1] - epsilon;

				double entropy = truncatedCDF(distribution, x[0], lb, ub) * Math.log(truncatedCDF(distribution, x[0], lb, ub));
				double error = findMax(distribution, lb, x[0]) - findMin(distribution, lb, x[0]);
				for (int i = 1; i < numOfIntervals - 1; i++) {
					error += findMax(distribution, x[i - 1], x[i]) - findMin(distribution, x[i - 1], x[i]);
					entropy += (truncatedCDF(distribution, x[i], lb, ub) - truncatedCDF(distribution, x[i - 1], lb, ub))
							* Math.log(truncatedCDF(distribution, x[i], lb, ub) - truncatedCDF(distribution, x[i - 1], lb, ub));
				}
				error += findMax(distribution, x[numOfIntervals - 1 - 1], ub) - findMin(distribution, x[numOfIntervals - 1 - 1], ub);
				entropy += (1.0 - truncatedCDF(distribution, x[numOfIntervals - 1 - 1], lb, ub))
						* Math.log(1.0 - truncatedCDF(distribution, x[numOfIntervals - 1 - 1], lb, ub));

				entropy = -1 * entropy;

				double ret = error / Math.sqrt(entropy); // among the
															// distributions
															// with minimal
															// error, pick the
															// one with maximum
															// entropy
				return ret;

			}
		};
		double initialIntervalLength = (ub - lb) / (numOfIntervals * 1d);
		double x[] = new double[numOfIntervals - 1];
		for (int i = 0; i < numOfIntervals - 1; i++) {
			x[i] = lb + (i + 1) * initialIntervalLength;
		}
		CobylaExitStatus exitStatus = Cobyla.FindMinimum(calcfc, numOfIntervals - 1, numOfIntervals, x, rhobeg, rhoend, iprint, maxfun);
		return x;
	}

	private double findMin(final RealDistribution distribution, final double a, final double b) {
		return findOptimum(1d, distribution, a, b);
	}

	private double findMax(final RealDistribution distribution, final double a, final double b) {
		return findOptimum(-1d, distribution, a, b);
	}

	private double findOptimum(final double sign, final RealDistribution distribution, final double a, final double b) {
		Calcfc calcfc = new Calcfc() {
			public double Compute(int n, int m, double[] x, double[] con) {
				// Constraining the search between a and b
				con[0] = x[0] - a;
				con[1] = b - x[0];

				return sign * distribution.density(x[0]);
			}
		};
		double[] x = { a };
		CobylaExitStatus exitStatus = Cobyla.FindMinimum(calcfc, 1, 2, x, rhobeg, rhoend, iprint, maxfun);
		return x[0];
	}

}
