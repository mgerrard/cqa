package coral.counters.estimates;

import coral.counters.MCUtil;

public class PCEstimateResult implements EstimateResult {

  // One object for each partition; if partitioning is disabled we will
  // have a 1-sized array for the entire PC.
  public final EstimateResult[] partitionResults;

  // Aggregated results for the partition array.
  public final double mean;
  public final double variance;
  public final long nSamples;

  public static final PCEstimateResult ZERO = new PCEstimateResult(
      new EstimateResult[] { PartitionEstimateResult.ZERO });

  public PCEstimateResult(EstimateResult[] partitionResults) {
    if (partitionResults.length < 1) {
      throw new IllegalArgumentException(
          "length of argument must be bigger than 1");
    } else {
      for (int i = 0; i < partitionResults.length; i++) {
        if (partitionResults[i] == null) {
          throw new IllegalArgumentException("Null element at position " + i
              + " of the array.");
        }
      }
    }

    this.partitionResults = partitionResults;

    double m1 = partitionResults[0].getMean();
    double v1 = partitionResults[0].getVariance();
    long samples = partitionResults[0].getNumberOfSamples();

    for (int i = 1; i < partitionResults.length; i++) {
      double m2 = partitionResults[i].getMean();
      double v2 = partitionResults[i].getVariance();
      long s = partitionResults[i].getNumberOfSamples();

      v1 = MCUtil.partitionVar(m1, m2, v1, v2);
      m1 = m1 * m2;
      samples = samples + s;
    }

    this.mean = m1;
    this.variance = v1;
    this.nSamples = samples;
  }

  public String toString() {
    return "[pc-er] e=" + mean + " v=" + variance + " s=" + nSamples;
  }

  @Override
  public long getNumberOfSamples() {
    return nSamples;
  }

  @Override
  public double getMean() {
    return mean;
  }

  @Override
  public double getVariance() {
    return variance;
  }

  @Override
  public EstimateResult[] getSubResults() {
    return partitionResults;
  }

  @Override
  public EstimateResult mergeResults(EstimateResult newResult) {
    PCEstimateResult res = (PCEstimateResult) newResult;
    EstimateResult[] merged = new EstimateResult[partitionResults.length]; 
    
    for (int i = 0; i < partitionResults.length; i++) {
      merged[i] = partitionResults[i].mergeResults(res.partitionResults[i]);
    }
    
    return new PCEstimateResult(merged);
  }

  public PCEstimateResult improve(int partitionIndex, EstimateResult newPartitionResult) {
    EstimateResult[] merged = new EstimateResult[partitionResults.length];
    
    for (int i = 0; i < merged.length; i++) {
      if (i == partitionIndex) {
        merged[i] = partitionResults[i].mergeResults(newPartitionResult);
      } else {
        merged[i] = partitionResults[i];
      }
    }
    
    return new PCEstimateResult(merged);
  }
}
