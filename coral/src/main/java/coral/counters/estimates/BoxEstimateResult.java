package coral.counters.estimates;

public class BoxEstimateResult implements EstimateResult {

  public final double probability; // = box weight when all variables are
                                   // uniform
  public final long nSamples;
  public final long nHits;
  public final double mean;
  public final double variance;

  public static BoxEstimateResult ZERO = new BoxEstimateResult();

  public BoxEstimateResult(double probability, long nSamples, long nHits) {
    this.probability = probability;
    this.nHits = nHits;
    this.nSamples = nSamples;

    if (nSamples == 0) {
      this.mean = 0;
      this.variance = 0;
    } else {
      double avg = ((double) nHits) / nSamples;
      double binomialVariance = (avg * (1 - avg) / nSamples);
      this.mean = avg * probability;
      this.variance = binomialVariance * Math.pow(probability, 2);
    }
  }

  // not for external use
  private BoxEstimateResult() {
    this.probability = 0;
    this.nSamples = 0;
    this.nHits = 0;
    this.mean = 0;
    this.variance = 0;
  }

  @Override
  public long getNumberOfSamples() {
    return nSamples;
  }

  @Override
  public double getMean() {
    return mean;
  }

  @Override
  public double getVariance() {
    return variance;
  }

  @Override
  public EstimateResult[] getSubResults() {
    return new EstimateResult[] {};
  }
  
  public String toString() {
    return "[box-er] e=" + mean + " v=" + variance + " s=" + nSamples;
  }

  @Override
  public EstimateResult mergeResults(EstimateResult newResult) {
    BoxEstimateResult res = (BoxEstimateResult) newResult;
    
    //some defensive programming...
    if (res.probability != this.probability) {
      throw new RuntimeException("Something is wrong with the merging - probabilities aren't equal");
    }
    
    long newNSamples = res.getNumberOfSamples() + this.getNumberOfSamples();
    long newNHits = res.nHits + this.nHits;
    
    return new BoxEstimateResult(probability,newNSamples,newNHits);
  }
}
