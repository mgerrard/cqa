package coral.counters.estimates;

public interface EstimateResult {

  public long getNumberOfSamples();
  
  public double getMean();
  
  public double getVariance();
  
  public EstimateResult[] getSubResults();
  
  public EstimateResult mergeResults(EstimateResult newResult);
  
}
