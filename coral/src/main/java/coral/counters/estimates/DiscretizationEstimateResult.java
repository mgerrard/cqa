package coral.counters.estimates;

public class DiscretizationEstimateResult implements EstimateResult {

  public final long nSamples;
  public final double probability;
  public final double mean;
  public final double variance;

  public static final DiscretizationEstimateResult ZERO = new DiscretizationEstimateResult();
  
  public DiscretizationEstimateResult(double probability, long nSamples, double mean, double variance) {
    this.probability = probability;
    this.nSamples = nSamples;
    this.mean = mean;
    this.variance = variance;
  }

  // not for external use
  private DiscretizationEstimateResult() {
    this.probability = 0;
    this.nSamples = 0;
    this.mean = 0;
    this.variance = 0;
  }

  @Override
  public long getNumberOfSamples() {
    return nSamples;
  }

  @Override
  public double getMean() {
    return mean;
  }

  @Override
  public double getVariance() {
    return variance;
  }

  @Override
  public EstimateResult[] getSubResults() {
    return new EstimateResult[] {};
  }
  
  public String toString() {
    return "[box-er] e=" + mean + " v=" + variance + " s=" + nSamples;
  }

  @Override
  public EstimateResult mergeResults(EstimateResult newResult) {
    throw new RuntimeException("Not implemented (and probably will never be)");
  }
}
