package coral.counters.estimates;

public class MathematicaEstimateResult implements EstimateResult {

  public final long nSamples;
  public final double mean;
  public final double variance;

  public static final MathematicaEstimateResult ZERO = new MathematicaEstimateResult();
  
  public MathematicaEstimateResult(long nSamples, double mean, double variance) {
    this.nSamples = nSamples;
    this.mean = mean;
    this.variance = variance;
  }

  // not for external use
  private MathematicaEstimateResult() {
    this.nSamples = 0;
    this.mean = 0;
    this.variance = 0;
  }

  @Override
  public long getNumberOfSamples() {
    return nSamples;
  }

  @Override
  public double getMean() {
    return mean;
  }

  @Override
  public double getVariance() {
    return variance;
  }

  @Override
  public EstimateResult[] getSubResults() {
    return new EstimateResult[] {};
  }
  
  public String toString() {
    return "[box-er] e=" + mean + " v=" + variance + " s=" + nSamples;
  }

  @Override
  public EstimateResult mergeResults(EstimateResult newResult) {
    throw new RuntimeException("Not implemented (and probably will never be)");
  }

}
