package coral.counters.estimates;

public class PartitionEstimateResult implements EstimateResult {

  // One entry for each box in the result
  public final EstimateResult[] boxResults;
  public final long nSamples;
  public final double mean;
  public final double variance;

  public static PartitionEstimateResult ZERO = new PartitionEstimateResult(
      new EstimateResult[] { BoxEstimateResult.ZERO });

  public PartitionEstimateResult(EstimateResult[] boxResults) {
    if (boxResults.length < 1) {
      throw new IllegalArgumentException(
          "length of argument must be bigger than 1");
    } else {
      for (int i = 0; i < boxResults.length; i++) {
        if (boxResults[i] == null) {
          throw new IllegalArgumentException("Null element at position " + i
              + " of the array.");
        }
      }
    }

    double m1 = boxResults[0].getMean();
    double v1 = boxResults[0].getVariance();
    long s1 = boxResults[0].getNumberOfSamples();

    // box weight is already factored in each entry

    for (int i = 1; i < boxResults.length; i++) {
      double m2 = boxResults[i].getMean();
      double v2 = boxResults[i].getVariance();
      long s2 = boxResults[i].getNumberOfSamples();

      m1 = m1 + m2;
      v1 = v1 + v2;
      s1 = s1 + s2;
    }

    this.boxResults = boxResults;
    this.mean = m1;
    this.variance = v1;
    this.nSamples = s1;
  }

  @Override
  public long getNumberOfSamples() {
    return nSamples;
  }

  @Override
  public double getMean() {
    return mean;
  }

  @Override
  public double getVariance() {
    return variance;
  }

  @Override
  public EstimateResult[] getSubResults() {
    return boxResults;
  }
  
  public String toString() {
    return "[part-er] e=" + mean + " v=" + variance + " s=" + nSamples;
  }
  
  @Override
  public EstimateResult mergeResults(EstimateResult newResult) {
    PartitionEstimateResult res = (PartitionEstimateResult) newResult;
    EstimateResult[] merged = new EstimateResult[boxResults.length]; 
    
    for (int i = 0; i < boxResults.length; i++) {
      merged[i] = boxResults[i].mergeResults(res.boxResults[i]);
    }
    
    return new PartitionEstimateResult(merged);
  }
}
