package coral.counters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.eval.Elem;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import coral.PC;
import coral.counters.MCUtil.DiscreteRegion;
import coral.counters.ResultData.Entry;
import coral.counters.estimates.BoxEstimateResult;
import coral.counters.estimates.DiscretizationEstimateResult;
import coral.counters.estimates.EstimateResult;
import coral.counters.estimates.PartitionEstimateResult;
import coral.counters.rvars.RandomVariable;
import coral.util.Config;
import coral.util.Interval;

public class MonteCarloCounter implements ModelCounter {

  public MonteCarloCounter() {
    seed = Config.mcSeed;
    rng = new Well44497b(seed);
//    rng = new MRG32k3a(seed); //uncomment this if you want to use the old RNG 
    entry = new Entry();
  }
  
  public MonteCarloCounter(RandomGenerator rng, long seed) {
    this.seed = seed;
    this.rng = rng;
    entry = new Entry();
  }

  public void reset() {
    entry = new Entry();
  }
  
  public void reset(long newSeed) {
    reset();
    seed = newSeed;
    rng.setSeed(seed);
  }
  
  public ResultData.Entry entry;
  public RandomGenerator rng;
  public long seed;
  
  /**
   * 'domains' must contain only the intervals for the variables on the PC. 
   */
  
  @Override
  public EstimateResult count(PC pc, Interval[] domains, int budget) {
    //use domain as a box
    List<Interval[]> box = new ArrayList<Interval[]>();
    box.add(domains);
    
    double domainVolume = Interval.computeVolume(domains,pc.getIntDomainArray()).doubleValue();
    return count(pc,box,domains, domainVolume, budget);
  }
  
//  private static final Interval DUMMY_DOMAIN = new Interval(0, 1);

  /**
   * All interval arrays in 'boxes' must contain only the intervals for 
   * the variables on the PC. 
   */
  
  @Override
  public EstimateResult count(PC pc, List<Interval[]> boxes, int budget) {
    //create dummy domain (we already have the boxes from the interval solver)
//    int nVars = boxes.get(0).length;
//    Interval[] domains = new Interval[nVars];
//    for (int i = 0; i < nVars; i++) {
//      domains[i] = DUMMY_DOMAIN;
//    }

    //no domain specified
    double domainVolume = 0;
    for (Interval[] box : boxes) {
      domainVolume += Interval.computeVolume(box,pc.getIntDomainArray()).doubleValue();
    }
    
    return count(pc,boxes,new Interval[]{}, domainVolume, budget); 
  }
  
  /**
   * 'domains' must contain only the intervals for the variables on the PC.
   * All interval arrays in 'boxes' must contain only the intervals for 
   * the variables on the PC. 
   */
  
  @Override
  public EstimateResult count(PC pc, List<Interval[]> boxes, Interval[] domain, int budget) {
    double domainVolume = Interval.computeVolume(domain,pc.getIntDomainArray()).doubleValue();
    return count(pc,boxes,domain,domainVolume, budget);
  }
  
  private EstimateResult count(PC pc, List<Interval[]> boxes, Interval[] domains, double domainVolume, int budget) {
    BoxEstimateResult[] estimates = new BoxEstimateResult[boxes.size()];
    
    int[] samplesPerBox = computeNumSamples(boxes, pc.getIntDomainArray(), budget);
    int i = 0;
    
    for (Interval[] box : boxes) {
      BoxEstimateResult regionResult = simulate(pc, box, samplesPerBox[i],domainVolume);
      estimates[i] = regionResult;
      i++;
    }

    //Reset RNG (if number of samples is small)
    if (Main.mcResetRngEachSimulation) {
      rng.setSeed(seed);
    }
    
    return new PartitionEstimateResult(estimates);
  }
  
  private int[] computeNumSamples(List<Interval[]> boxes, Boolean[] intDomains, int budget) {
    int[] nsamples = new int[boxes.size()];
    double[] sizes = new double[boxes.size()];
    
    if (Config.mcDistributeSamples) {
      double total = 0;
      for (int i = 0; i < boxes.size(); i++) {
        sizes[i] = Interval.computeVolume(boxes.get(i),intDomains).doubleValue();
      }

      //size which will result in the minimum amount of samples
      double cut = Config.mcMinSamplesPerBox * total / budget;
      double totalMinusCuts = 0;
      for (int i = 0; i < boxes.size(); i++) {
        if (sizes[i] < cut) {
          nsamples[i] = Config.mcMinSamplesPerBox;
          budget = budget - Config.mcMinSamplesPerBox;
        } else {
          totalMinusCuts += sizes[i];
        }
      }
      
      for (int i = 0; i < boxes.size(); i++) {
        if (nsamples[i] == 0) {
          int samples = (int) Math.floor(sizes[i] * budget / totalMinusCuts);
          nsamples[i] = samples;
        }
      }
      
    } else {
      for (int i = 0; i < boxes.size(); i++) {
        nsamples[i] = budget / boxes.size();
      }
    }
    
    return nsamples;
  }
  
  //Use Largest Remainder method to allocate samples 
  private int[] computeNumSamplesProportionally(List<Interval[]> boxes, int budget, double[] votes) {
    int[] nsamples = new int[boxes.size()];
    double[] remainders = new double[boxes.size()];
    double sum = 0;
    int allocated = 0;
    
    for (double proportion: votes) {
      sum += proportion;
    }
    for (int i = 0; i < nsamples.length; i++) {
      double proportion = votes[i];
      double quota = budget * (proportion / sum); 
      nsamples[i] = (int) Math.floor(quota);
      remainders[i] = quota - nsamples[i];
      allocated += nsamples[i];
    }
    
    if (allocated < budget) {
      //dumb n^2 loop, but n will always be < 10, so this shouldn't be an issue
      int toAllocate = budget - allocated;
      while (toAllocate > 0) {
        int index = 0;
        double biggest = 0;
        
        for (int i = 0; i < remainders.length; i++) {
          if (remainders[i] > biggest) {
            index = i;
            biggest = remainders[i];
          }
        }
        
        remainders[index] = 0;
        nsamples[index]++;
        toAllocate--;
      }
    }
    
    return nsamples;
  }
  
  private String toString(List<Interval[]> boxes) {
    StringBuffer sb = new StringBuffer();
    
    for (Interval[] box : boxes) {
      sb.append(Arrays.toString(box));
      sb.append(" ## ");
    }
    
    return sb.toString();
  }

  private BoxEstimateResult simulate(PC pc, Interval[] box, int nSamples, double domainVolume) {
    double boxWeight = MCUtil.boxWeight(box, domainVolume);
    int hits = 0;
    
    if (boxWeight == 0) {
      System.out.println("[monte carlo] 0-vol box - skipping simulation...");
      return BoxEstimateResult.ZERO;
    } else {
      Set<SymLiteral> varSet = pc.getSortedVars();
      List<Elem[]> l = prepareRPNState(pc);
      
      for (int i = 0; i < nSamples; i++) {
//      for (SymLiteral lit : varSet) {
//        Interval iv = box[lit.getId() - 1];
//        lit.setCte((rng.nextDouble() * iv.getLength()) + iv.lo);
//      }
        
        Iterator<SymLiteral> it = varSet.iterator();
        for (Interval iv : box) {
          SymLiteral lit = it.next();
          lit.setCte((rng.nextDouble() * iv.getLength()) + iv.lo().doubleValue());
        }
        
        /** reverse polish code **/
        boolean isSAT = true;
        isSAT = evalRNP(l);
        if (isSAT) {
          hits++;
        }
      }
      
      return new BoxEstimateResult(boxWeight,nSamples,hits);
    }
  }
  
  private static boolean evalRNP(List<Elem[]> l) {
    boolean result = true;
    for (Elem[] elem : l) {
      ReversePolish rpol = new ReversePolish(elem);
      if (rpol.eval().intValue()==0) {
        result = false;
        break;
      }
    }
    return result;
  }

  private static List<Elem[]> prepareRPNState(PC pc) {
    /** reverse polish code **/
    List<Elem[]> l = new ArrayList<Elem[]>();
    for (SymBool bExp : pc.getConstraints()) {
      GenReversePolishExpression revPolExpr = new GenReversePolishExpression();
      revPolExpr.visitSymBool(bExp);
      l.add(revPolExpr.getElems());  
    }
    return l;
  }

  //TODO just for testing; put this somewhere else
  public boolean useRejectionSampling = false;

  public Map<Long,PartitionEstimateResult> cpToResult = new HashMap<Long, PartitionEstimateResult>();
//  public Map<CountingProblem,PartitionEstimateResult> cpToResult = new HashMap<CountingProblem, PartitionEstimateResult>();
  
  @Override
  public EstimateResult count(CountingProblem cp, int budget) {
    List<Interval[]> boxes = cp.boxes;
    Boolean[] integerDomains = cp.integerDomains;
    EstimateResult[] estimates;
    int nsamples = budget;
    int nvars = cp.pc.getVars().size();
    
    if (nvars == 0) {
      throw new RuntimeException("Empty pcs screw the computation!");
    } else if (Main.mcDiscretizeWithMateusApproach) { // split each variable domain in six regions
      long start = System.nanoTime();
      double domainVolume = Interval.computeVolume(cp.domain,integerDomains).doubleValue();
      int nregions = Config.mcDiscretizationRegions;
      
//      System.out.println("[qcoral] Split factor: " + nregions + " Total regions: " + ((long)Math.pow(nregions,nvars)));
      long i = 0;
      Iterator<DiscreteRegion[]> regionIt = MCUtil.getAllRegions(cp.domain,cp.getRVarsInPC(),nregions);
      double probability = 0.0;
      double totalVolEstimate = 0;
      double stratifiedVariance = 0;
      
      long end = System.nanoTime();
      DistributionAwareQCoral.discretizationCost += (end - start) / 1000000000.0;
      while (regionIt.hasNext()) {
        start = System.nanoTime();
        DiscreteRegion[] region = regionIt.next();
        end = System.nanoTime();
        DistributionAwareQCoral.discretizationCost += (end - start) / 1000000000.0;
        BoxEstimateResult regionResult = simulateUniform(cp.pc, region, nsamples, domainVolume);
        //boxWeight is already factored in regionResult
        totalVolEstimate += regionResult.getMean();
        stratifiedVariance += regionResult.getVariance();
        probability += regionResult.probability;
        i++;
      }
      
      estimates = new DiscretizationEstimateResult[] { new DiscretizationEstimateResult(
          probability, nsamples, totalVolEstimate, stratifiedVariance) };
      
    } else { //default mode
      estimates = new EstimateResult[boxes.size()];
      int[] samplesPerBox;
      if (Config.mcProportionalBoxSampleAllocation) {
        //TODO major hack - refactor as soon as possible
        PartitionEstimateResult per = cpToResult.get(MCUtil.boxId(boxes));
//        PartitionEstimateResult per = cpToResult.get(cp);
        double[] boxVariance = new double[boxes.size()];
        
        for(int i = 0; i < boxVariance.length; i++) {
          boxVariance[i] = per.getSubResults()[i].getVariance();
        }
        
        samplesPerBox = computeNumSamplesProportionally(boxes, nsamples, boxVariance);
//        for (int i = 0; i < boxVariance.length; i++) {
//          if (boxVariance[i] == 0 && samplesPerBox[i] > 0) {
//            throw new RuntimeException("[qCORAL-consistency] error in allocating samples to boxes");
//          }
//        }
        
      } else {
        samplesPerBox = computeNumSamples(boxes,integerDomains, nsamples);
      }
      
      for (int i = 0; i < boxes.size(); i++) {
        Interval[] box = boxes.get(i);
        EstimateResult regionResult;
        if (Main.mcDiscretize) {
          regionResult = simulate(cp.pc,box,samplesPerBox[i],cp.domainVolume.doubleValue());
        } else {
          regionResult = simulateWithDistributions(cp.pc,box,cp.lit2rvar,samplesPerBox[i],cp.domainVolume.doubleValue());
        }
        estimates[i] = regionResult;
      }
    }
    
    return new PartitionEstimateResult(estimates);
  }

  private static class VarInfo {
    SymLiteral lit;
    RandomVariable rvar;
    Interval iv;
    double cdfDomainLo;
    double cdfDomainHi;
    
    public VarInfo(SymLiteral lit, RandomVariable rvar, Interval iv, double cdfDomainLo, double cdfDomainHi) {
      this.lit = lit;
      this.rvar = rvar;
      this.iv = iv;
      //FIXME check with antonio if we need to subtract 1 if the var is an integer
      this.cdfDomainLo = cdfDomainLo;//rvar.getCDF(iv.lo);
      this.cdfDomainHi = cdfDomainHi;//rvar.getCDF(iv.hi);
    }
  }
  
  private BoxEstimateResult simulateUniform(PC pc, DiscreteRegion[] regions, int nSamples, double domainVolume) {
    double boxWeight = MCUtil.boxWeight(regions, domainVolume);
    int hits = 0;
    
    if (boxWeight == 0) {
      System.out.println("[monte carlo] 0-vol box - skipping simulation...");
      return BoxEstimateResult.ZERO;
    } else {
      Set<SymLiteral> varSet = pc.getSortedVars();
      List<Elem[]> l = prepareRPNState(pc);
      
      for (int i = 0; i < nSamples; i++) {
        Iterator<SymLiteral> it = varSet.iterator();
        for (DiscreteRegion region : regions) {
          Interval iv = region.box;
          SymLiteral lit = it.next();
          lit.setCte((rng.nextDouble() * iv.getLength()) + iv.lo().doubleValue());
        }
        
        /** reverse polish code **/
        boolean isSAT = true;
        isSAT = evalRNP(l);
        
        if (isSAT) {
          hits++;
        }
      }
      
      double probability = 1;
      for(DiscreteRegion region : regions) {
        probability = probability *  region.probability; 
      }
      
      return new BoxEstimateResult(probability, nSamples, hits);
    }
  }
  
  private EstimateResult simulateWithDistributions(PC pc, Interval[] box, Map<SymLiteral,RandomVariable> lit2RVar, int nSamples, double domainVolume) {
    //TODO wrap "box" in a single class to avoid recomputations
    double boxWeight = MCUtil.boxWeight(box, domainVolume);
    int hits = 0;
    
    EstimateResult result;
    
    if (boxWeight == 0) {
      System.out.println("[monte carlo] 0-vol box - skipping simulation...");
      result = BoxEstimateResult.ZERO;
    } else {
      Set<SymLiteral> varSet = pc.getSortedVars();
      List<VarInfo> varsInfo = truncateVars(box, lit2RVar, varSet);
      
      //truncated variable probability
      double probability = 1;
      for(VarInfo info : varsInfo) {
        RandomVariable rvar = info.rvar;
        probability = probability * ((rvar.getCDFUpper() - rvar.getCDFLower()) / (info.cdfDomainHi - info.cdfDomainLo));
      }
      
      if (probability == 0.0 || Double.isNaN(probability)) {
        System.out.println("[monte carlo] 0-probability box - skipping simulation...");
        return BoxEstimateResult.ZERO; //TODO check this with Antonio
      }
      
      List<Elem[]> l = prepareRPNState(pc);
      
      for (int i = 0; i < nSamples; i++) {
        for (VarInfo vInfo : varsInfo) {
          SymLiteral lit = vInfo.lit;
          RandomVariable rvar = vInfo.rvar;
          Number sample;
          if (useRejectionSampling) {
            sample = rvar.rejectionSample();
          } else {
            sample = rvar.sample();
          }
          lit.setCte(sample);
        }
        
        /** reverse polish code **/
        boolean isSAT = true;
        isSAT = evalRNP(l); 
        if (isSAT) {
          hits++;
        }
      }
      
      result = new BoxEstimateResult(probability, nSamples, hits);
    }
    
    return result;
  }

//  private EstimateResult computeEstimate(DescriptiveStatistics stats,
//      double probability) {
//    EstimateResult result;
//    double avg = stats.getMean();
//    double binomialVariance = (avg * (1 - avg) / stats.getN());
//    result = new EstimateResult(avg * /*boxWeight **/ probability, binomialVariance * Math.pow(probability,2));
//    return result;
//  }

  private List<VarInfo> truncateVars(Interval[] box,
      Map<SymLiteral, RandomVariable> lit2RVar, Set<SymLiteral> varSet) {
    List<VarInfo> varsInfo = new ArrayList<VarInfo>(varSet.size());
    
    Iterator<SymLiteral> it = varSet.iterator();
    for (Interval iv : box) {
      SymLiteral lit = it.next();
      RandomVariable rvar = lit2RVar.get(lit);
      double cdfLo = rvar.getCDFLower();
      double cdfHi = rvar.getCDFUpper();
      
      RandomVariable truncated = rvar.truncate(iv);
      
      VarInfo vInfo = new VarInfo(lit, truncated, iv, cdfLo, cdfHi);
      varsInfo.add(vInfo);
    }
    
    return varsInfo;
  }
}
