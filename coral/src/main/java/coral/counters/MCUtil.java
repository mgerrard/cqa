package coral.counters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import symlib.SymLiteral;
import coral.counters.MCUtil.DiscreteRegion;
import coral.counters.discretization.Discretization;
import coral.counters.estimates.PCEstimateResult;
import coral.counters.rvars.RandomVariable;
import coral.counters.rvars.RealRandomVariable;
import coral.util.Config;
import coral.util.Interval;

public class MCUtil {
  
  /**
   * Interval format: [{x1_lo,x1_hi},{x2_lo,x2_hi},...]
   * @param domain
   * @return
   */
  
  public static final Pattern DOMAIN_PATTERN = Pattern.compile("\\{(.+?),(.+?)\\}");
  
  public static Interval[] parseDomain(String domain) {
    List<Interval> ivs = new ArrayList<Interval>();
    
    Matcher m = DOMAIN_PATTERN.matcher(domain);
    while(m.find()) {
      double lo = Double.parseDouble(m.group(1));
      double hi = Double.parseDouble(m.group(2));
      ivs.add(new Interval(lo,hi));
    }
    
    return ivs.toArray(new Interval[]{});
  }
  
  public static void main(String[] args) {
    System.out.println(Arrays.toString(parseDomain("[{22.2,33.3},{123.2,33.4},{11.0,44.3}]")));
  }

  public static Interval[] filterIntervals(Interval[] box, Set<SymLiteral> varSet) {
    List<Integer> boxIndexes = new ArrayList<Integer>();
    for (SymLiteral lit : varSet) {
      boxIndexes.add(lit.getId() - 1);
    }
    
    Interval[] intervalsToUse = new Interval[boxIndexes.size()];
    for (int i = 0; i < intervalsToUse.length; i++) {
      intervalsToUse[i] = box[boxIndexes.get(i)];
    }
    return intervalsToUse;
  }

  public static List<Interval[]> filterBoxes(List<Interval[]> boxes,
      Set<SymLiteral> sortedVars) {
    List<Interval[]> filtered = new ArrayList<Interval[]>();
    for (Interval[] box : boxes) {
      filtered.add(filterIntervals(box, sortedVars));
    }
    
    return filtered;
  }

  public static double avg(List<Double> data) {
    double tmp = 0;
    
    for (double d : data) {
      tmp += d;
    }
    
    return tmp / data.size();
  }

  public static double sampleStdev(List<Double> data, double avg) {
    double tmp = 0.0;
    for (double d : data) {
      tmp += Math.pow(d - avg, 2);
    }
    tmp = tmp / (data.size() - 1);
    return Math.sqrt(tmp);
  }
  
  public static double partitionVar(double mean1, double mean2, double var1, double var2) {
    return Math.pow(mean1, 2) * var2 + Math.pow(mean2, 2) * var1 + var1 * var2;
  }
  
  public static class ORComposer {
    public double accumulatedConstants = 0;
    public double meanEstimate = 0;
    public double varianceEstimate = 0;
    
    final long nSamples;
    public ORComposer(long spentBudget) {
      this.nSamples = spentBudget; 
    }
    
    public void compose(double mean, double var) {
//      System.out.println("composing [" + meanEstimate + "," + varianceEstimate + "] with [" + mean + "," + var + "]");
      if (var == 0) { //store constants 
        accumulatedConstants += mean;
      } else { //compose variance
        varianceEstimate = varianceEstimate + var;// - 2 * (meanEstimate * mean) / (nSamples); 
//      return var1 + var2 + 2*Math.sqrt(var1*var2);
        meanEstimate += mean;
      }
    }
    
    public double[] getResult() {
      return new double[] {accumulatedConstants + meanEstimate, varianceEstimate};
    }
  }

  static class BacktrackNode {
    final Interval[] regions;
    final double[] probabilities;
    int current;

    public BacktrackNode(Interval[] regions, double[] probabilities) {
      this.regions = regions;
      this.probabilities = probabilities;
      current = 0;
      
      if (regions.length != probabilities.length) {
        throw new IllegalArgumentException("The length of both arguments must be the same!");
      }
    }
    
    DiscreteRegion get() {
      return new DiscreteRegion(regions[current], probabilities[current]);
    }
    
    
    void increment() {
      current++;
    }
    
    boolean hasNext() {
      return current + 1 < regions.length;
    }
    
    public String toString() {
      return "{" + Arrays.toString(regions) + "}[" +current+"]";
    }
  }

  /**
   * Returns the weight (relative size) of the box, i.e. volume(box)/domainVolume 
   * @param box
   * @return weight of the box
   */
  
  public static double boxWeight(Interval[] box, double domainVolume) {
    double weight = 1;
    if (domainVolume == 0) {
      weight = 0;
    } else {
      for (Interval iv : box) {
        if (iv != null) { //null boxes = vars that 
          weight = weight * iv.getLength();
        }
      }
      weight = weight / domainVolume;
    }
    
    return weight;
  }
  
  public static double boxWeight(DiscreteRegion[] regions, double domainVolume) {
    Interval[] box = new Interval[regions.length];
    for (int i = 0; i < regions.length; i++) {
      box[i] = regions[i].box;
    }
    return boxWeight(box, domainVolume);
  }
  

  /**
   * compute variance according to equation (9) on the paper
   * @param boxes
   * @param estimates
   * @return
   */
  public static double variance(List<Interval[]> boxes, List<PCEstimateResult> estimates, double domainVolume) {
    double stratifiedVariance = 0;
    
    for (int j = 0; j < estimates.size(); j++) {
      Interval[] box = boxes.get(j);
      double boxWeight = boxWeight(box,domainVolume);
      
      PCEstimateResult estimate = estimates.get(j);
      double var = estimate.variance; 
      
      stratifiedVariance += Math.pow(boxWeight,2) * var;  
    }
    return stratifiedVariance;
  }

  public static class DiscreteRegion {
    final Interval box;
    final double probability;

    public DiscreteRegion(Interval region, double probability) {
      this.box = region;
      this.probability = probability;
      if (probability < 0 || probability > 1) {
        throw new IllegalArgumentException("Probability must be between 0 and 1!");
      }
    }
  }
  
  public static Iterator<DiscreteRegion[]> getAllRegions(Interval[] domain, RandomVariable[] rvars, int nregions) {
    
    final List<BacktrackNode> btnodes = new ArrayList<BacktrackNode>();
    final Stack<BacktrackNode> stack = new Stack<BacktrackNode>();
    
    int i = 0;
    for (Interval varDomain : domain) {
      Interval[] splitDomain;
      double[] probabilities;
      RandomVariable rvar = rvars[i];
      
      if (rvar.isUniform()) { //don't split
        splitDomain = new Interval[]{varDomain};
        probabilities = new double[]{1};
      } else {
        probabilities = new double[nregions];
        if (Config.optimizeDiscretization) {
          Discretization optimizer = new Discretization();
          double[] cuttingPoints = optimizer.findBestAllocation(
              ((RealRandomVariable) rvar).rdist, varDomain.lo().doubleValue(), varDomain.hi().doubleValue(),
              nregions);
          
          splitDomain = new Interval[nregions];
          splitDomain[0] = new Interval(varDomain.lo().doubleValue(), cuttingPoints[0]);
          for (int j = 1; j < cuttingPoints.length - 1; j++) {
            splitDomain[j] = new Interval(cuttingPoints[j], cuttingPoints[j+1]);
          }
          splitDomain[nregions - 1] = new Interval(
              cuttingPoints[cuttingPoints.length - 1], varDomain.hi().doubleValue());
        } else {
          splitDomain = varDomain.split(nregions);
        }

        double cdfAtHi = rvar.getCDF(varDomain.hi().doubleValue());
        double cdfAtLo = rvar.getCDF(varDomain.lo().doubleValue());
        
        for (int j = 0; j < splitDomain.length; j++) {
          Interval region = splitDomain[j];
          //scale the (truncated variable) probability for the original domain
          probabilities[j] = (rvar.getCDF(region.hi().doubleValue()) - rvar.getCDF(region.lo().doubleValue())) / (cdfAtHi - cdfAtLo);
        }
      }
      
      btnodes.add(new BacktrackNode(splitDomain,probabilities));
      i++;
    }
    stack.addAll(btnodes);
    
    return new Iterator<DiscreteRegion[]>() {
      
      @Override
      public boolean hasNext() {
        return !stack.isEmpty();
      }
  
      @Override
      public DiscreteRegion[] next() {
        if (stack.isEmpty()) {
          throw new RuntimeException("End of backtracking");
        } else {
          //fill
          if (stack.size() < btnodes.size()) {
            for(int i = stack.size(); i < btnodes.size(); i++) {
              BacktrackNode node = btnodes.get(i);
              node.current = 0;
              stack.push(node);
            }
          }
          //choose
          DiscreteRegion[] domainRegion = new DiscreteRegion[btnodes.size()];
          for(int i = 0; i < stack.size(); i++) {
            domainRegion[i] = stack.get(i).get();
          }
                    
          //advance/clear
          boolean keepGoing = true;
          while (keepGoing && !stack.isEmpty()) {
            BacktrackNode node = stack.peek();
            if (node.hasNext()) {
              node.increment();
              keepGoing = false;
            } else {
              stack.pop();
            }
          }
          return domainRegion;
        }
      }
  
      @Override
      public void remove() {
        throw new RuntimeException("Not implemented");
      }
    };
  }
  
  public static Long boxId(List<Interval[]> boxes){
    HashFunction hashFunction = Hashing.sha512();
    Hasher hasher = hashFunction.newHasher();
    
    for (Interval[] box : boxes) {
      for(int i=0;i<box.length;i++){
        hasher.putInt(i);
        hasher.putDouble(box[i].lo().doubleValue());
        hasher.putDouble(box[i].hi().doubleValue());
      }
    }
    return hasher.hash().asLong();
  }
}
