package coral.counters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import symlib.parser.Parser;
import coral.PC;
import coral.util.Config;
import coral.util.Interval;
import coral.util.options.Options;

public class RunBenchmark {
  
  private static final boolean REPORT_STATS_CONS = true;
  
  private static Options options = new Options(Main.class, Config.class);
  
  public static void main(String[] args) throws Exception {
    System.out.println(args.length);
    options.ignore_options_after_arg(true);
    if (args != null && args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      System.out.println(Arrays.toString(args));
    }
    //no reason to normalize in this case
    Main.normalize = false;

    options.printOptions();  
    
    String[][] benchmark = Benchmarks.EVERYTHING;
    
    List<String> results = new ArrayList<String>();
    
    for (String[] bench : benchmark) {
      System.out.println("-----------------------------");
      System.out.printf("name: %s, analytic result: %s\n",bench[0],bench[2]);

      long timeStart = System.nanoTime();
      PC pc = (new Parser(bench[1])).parsePC();
      
      String domain = bench[3];
      Interval[] parsedDomain = MCUtil.parseDomain(domain);
      List<Interval[]> domains = new ArrayList<Interval[]>();
      domains.add(parsedDomain);
      
      List<PC> cons = new ArrayList<PC>();
      cons.add(pc);
      
      
      double[] entry = Main.runRegCoral(cons, domains);
      
      double totalTime = (System.nanoTime() - timeStart) / 1000000000.0;
	  //      String result = String.format("[regCoral - results] name: %s, analytic result: %s, estimate: %f, stdev: %f, time: %f",bench[0],bench[2],entry[0], entry[1],totalTime);
      String result = String.format("[regCoral-csvresults] %s,%s,%f,%f,%f,%d,%d",bench[0],bench[2],entry[0], entry[1],totalTime,Config.mcMaxSamples,Main.mcNumExecutions);
      results.add(result);
    }
    System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	System.out.println("[regCoral-csvresults] name,analytic,estimate,stdev,time,samples,exec");
    for (String r : results) {
      System.out.println(r);
    }
  }
}
