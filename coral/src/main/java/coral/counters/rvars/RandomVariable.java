package coral.counters.rvars;

import org.apache.commons.math3.distribution.UniformRealDistribution;

import coral.util.Interval;

public abstract class RandomVariable {

  public final Interval bounds;
  

  /**
   * Uniform distribution between 0-1; used in truncation sampling
   */
  protected final UniformRealDistribution uniformReal;

  protected RandomVariable(Interval bounds, UniformRealDistribution uniformReal) {
    this.bounds = bounds;
    this.uniformReal = uniformReal;
  }

  /**
   * Use the truncation sampling algorithm sent by Antonio (if applicable)
   * 
   * @return Number: a sample
   */

  public abstract Number sample();

  /**
   * Use rejection sampling (slower)
   * 
   * @return Number: a sample
   */

  public abstract Number rejectionSample();

  /**
   * Compute a new RandomVariable restricted to newBounds
   * 
   * @param newBounds
   * @return
   */
  public abstract RandomVariable truncate(Interval newBounds);
  
  public abstract double getCDFLower();
  public abstract double getCDFUpper();
  public abstract double getCDF(double bound);
  public abstract boolean isUniform();
}
