package coral.counters.rvars;

import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

import coral.util.Interval;

public class RealRandomVariable extends RandomVariable {

  public final RealDistribution rdist;
  private final double cdfAtLB;
  private final double cdfAtUB;

  public RealRandomVariable(RealDistribution rdist, Interval bounds,
      UniformRealDistribution uniformReal) {
    super(bounds, uniformReal);
    this.rdist = rdist;
    this.cdfAtLB = rdist.cumulativeProbability(bounds.lo().doubleValue());
    this.cdfAtUB = rdist.cumulativeProbability(bounds.hi().doubleValue());
  }

  @Override
  public Number sample() {
    double uniformSample = uniformReal.sample();
    double sample = rdist.inverseCumulativeProbability(cdfAtLB + uniformSample
        * (cdfAtUB - cdfAtLB));
    return sample;
  }

  @Override
  public Number rejectionSample() {
    double sample;
    do {
      sample = rdist.sample();
    } while (sample > bounds.hi().doubleValue() || sample < bounds.lo().doubleValue());
    return sample;
  }

  @Override
  public RealRandomVariable truncate(Interval newBounds) {
    RealDistribution rd = this.rdist;
    return new RealRandomVariable(rd, newBounds, uniformReal);
  }

  @Override
  public double getCDFLower() {
    return cdfAtLB;
  }

  @Override
  public double getCDFUpper() {
    return cdfAtUB;
  }

  @Override
  public double getCDF(double bound) {
    return rdist.cumulativeProbability(bound);
  }
  
  public boolean isUniform() {
    return rdist instanceof UniformRealDistribution;
  }

}
