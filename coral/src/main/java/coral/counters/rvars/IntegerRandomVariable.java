package coral.counters.rvars;

import org.apache.commons.math3.distribution.IntegerDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

import coral.util.Interval;

public class IntegerRandomVariable extends RandomVariable {

  public final IntegerDistribution idist;
  private final double cdfAtLB;
  private final double cdfAtUB;

  public IntegerRandomVariable(IntegerDistribution idist, Interval bounds,
      UniformRealDistribution uniformReal) {
    super(bounds, uniformReal);
    this.idist = idist;
    this.cdfAtLB = idist.cumulativeProbability(bounds.intLo() - 1);
    this.cdfAtUB = idist.cumulativeProbability(bounds.intHi()); 
  }

  @Override
  public Number sample() {
    double uniformSample = uniformReal.sample();
    int sample = idist.inverseCumulativeProbability(cdfAtLB + uniformSample * (cdfAtUB - cdfAtLB));
    return sample;
  }

  @Override
  public Number rejectionSample() {
    int sample;
    do {
      sample = idist.sample();
    } while (sample > bounds.intHi() || sample < bounds.intLo());
    return sample;
  }

  @Override
  public IntegerRandomVariable truncate(Interval newBounds) {
    IntegerDistribution rd = this.idist;
    return new IntegerRandomVariable(rd, newBounds, uniformReal);
  }

  @Override
  public double getCDFLower() {
    return cdfAtLB;
  }

  @Override
  public double getCDFUpper() {
    return cdfAtUB;
  }

  @Override
  public double getCDF(double bound) {
    return idist.cumulativeProbability((int) bound);
  }
  
  public boolean isUniform() {
    return idist instanceof UniformIntegerDistribution;
  }
  
//  public static void main(String[] args) {
//    IntegerRandomVariable rv = new IntegerRandomVariable(
//        new UniformIntegerDistribution(0, 20), new Interval(15, 20),
//        new UniformRealDistribution());
//    int[] score = new int[100];
//    for (int i = 0; i < 100000; i++) {
//      int sample = rv.sample().intValue();
//      score[sample]++;
//    }
//    for (int i = 0; i < score.length; i++) {
//      System.out.print(score[i] + " ");
//    }
//  }
}
