package coral.counters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.commons.math3.random.RandomGenerator;

import symlib.SymBool;
import coral.PC;
import coral.PartitionedCache;
import coral.counters.CachedCounter.ImprovementResult;
import coral.counters.estimates.EstimateResult;
import coral.counters.estimates.PCEstimateResult;
import coral.counters.estimates.PartitionEstimateResult;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;

public class CachedCounter extends PartitionedCache implements ModelCounter {
  
  interface Callback {
    PC getDefaultPC();
    Interval[] getDefaultInterval(PC partition);
    EstimateResult callCounter(PC partition);
    EstimateResult callCounter(PC partition, List<Interval[]> paving);
  }
  
  private final boolean callIntervalSolverForPartitions;
  private final ModelCounter counter;
  private final IntervalSolverCaller isolver;
  private Callback callback;

  //TODO throw exception if the same partition appears twice with distinct domains
  public EstimateResult processConstraint(Set<SymBool> conjuncts) {
    PC pc = new PC(new ArrayList<SymBool>(conjuncts)); 
    EstimateResult result;
    if (callIntervalSolverForPartitions) {
      try {
        List<Interval[]> paving = isolver.getPaving(pc,callback.getDefaultInterval(pc));
        if (paving.size() == 0) {
          result = PartitionEstimateResult.ZERO;
        } else if (Config.mcMergeBoxes && paving.size() > 1) {
          List<Interval[]> merged = new ArrayList<Interval[]>();
          merged.add(Interval.mergeIntervals(paving));
          result = callback.callCounter(pc,merged);
        } else {
          result = callback.callCounter(pc,paving);
        }
      } catch (Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Error while calling realpaver: " + e.getMessage());
      }
    } else {
      result = callback.callCounter(pc);
    }
    return result;
  }
  
  public CachedCounter(ModelCounter counter, boolean callIS) {
    super();
    this.counter = counter;
    this.callIntervalSolverForPartitions = callIS;
    isolver = coral.solvers.rand.Util.getIntervalSolver(Config.intervalSolver);
    prob2partClauses = new LinkedHashMap<List<SymBool>, List<Set<SymBool>>>(5000);
    prob2part = new HashMap<List<SymBool>, Set<Partition>>(5000);
    part2dep = new HashMap<Partition, Set<Partition>>(5000);
    allPartitions = new ArrayList<CachedCounter.Partition>(5000);
    pc2problem = new HashMap<List<SymBool>, CountingProblem>(5000);
  }
  public void reset() {
    counter.reset();
  }
  
  public void reset(long seed) {
    counter.reset(seed);
  }

  @Override
  public EstimateResult count(final PC pc, final List<Interval[]> boxes, final int budget) {
    callback = new Callback() {
      public EstimateResult callCounter(PC partition) {
        return filterAndCount(partition, boxes, counter, budget);
      }
      @Override
      public EstimateResult callCounter(PC partition, List<Interval[]> paving) {
        return filterAndCount(partition, paving, counter, budget);
      }
      @Override
      public Interval[] getDefaultInterval(PC partition) {
        return MCUtil.filterIntervals(Interval.mergeIntervals(boxes),partition.getSortedVars());
      }
      @Override
      public PC getDefaultPC() {
        return pc;
      }
    };
    EstimateResult total = analyzeOne(pc.getConstraints());
    return total;
  }

  @Override
  public EstimateResult count(final PC pc, final Interval[] domains, final int budget) {
    callback = new Callback() {
      public EstimateResult callCounter(PC partition) {
        Interval[] filteredIntervals = MCUtil.filterIntervals(domains, partition.getSortedVars());
        return counter.count(partition, filteredIntervals, budget);
      }

      @Override
      public EstimateResult callCounter(PC partition, List<Interval[]> paving) {
        return filterAndCount(partition, paving, domains, counter, budget);
      }

      @Override
      public Interval[] getDefaultInterval(PC partition) {
        return MCUtil.filterIntervals(domains,partition.getSortedVars());
      }

      @Override
      public PC getDefaultPC() {
        return pc;
      }
    };
    
    EstimateResult total = analyzeOne(pc.getConstraints());
    
    return total;
  }

  @Override
  public EstimateResult count(final PC pc, final List<Interval[]> boxes, final Interval[] domains, final int budget) {
    callback = new Callback() {
      public EstimateResult callCounter(PC partition) {
        return filterAndCount(partition, boxes, domains, counter, budget);
      }

      @Override
      public EstimateResult callCounter(PC partition, List<Interval[]> paving) {
        return filterAndCount(partition, paving, domains, counter, budget);
      }

      @Override
      public Interval[] getDefaultInterval(PC partition) {
        return MCUtil.filterIntervals(domains,partition.getSortedVars());
      }

      @Override
      public PC getDefaultPC() {
        return pc;
      }
    };
    EstimateResult total = analyzeOne(pc.getConstraints());
    return total;
  }
  
  @Override
  public EstimateResult count(final CountingProblem cp, final int budget) {
    callback = buildCallback(cp, budget);
    pc2problem.put(cp.pc.getConstraints(), cp);
    EstimateResult total = analyzeOne(cp.pc.getConstraints());
    return total;
  }

  private Callback buildCallback(final CountingProblem cp, final int budget) {
    return new Callback() {
      public EstimateResult callCounter(PC partition) {
        return filterAndCount(partition, cp, counter, budget);
      }

      @Override
      public EstimateResult callCounter(PC partition, List<Interval[]> paving) {
        return filterAndCount(partition, paving, cp, counter, budget);
      }

      @Override
      public Interval[] getDefaultInterval(PC partition) {
        return MCUtil.filterIntervals(cp.domain,partition.getSortedVars());
      }

      @Override
      public PC getDefaultPC() {
        return cp.pc;
      }
    };
  }
  
  private static EstimateResult filterAndCount(PC pc, List<Interval[]> boxes, ModelCounter counter, int budget) {
    List<Interval[]> filteredBoxes = MCUtil.filterBoxes(boxes, pc.getSortedVars());
    return counter.count(pc, filteredBoxes, budget);
  }
  
  private static EstimateResult filterAndCount(PC pc, List<Interval[]> boxes, Interval[] domains, ModelCounter counter, int budget) {
    Interval[] filteredDomains = MCUtil.filterIntervals(domains, pc.getSortedVars());
    List<Interval[]> filteredBoxes = MCUtil.filterBoxes(boxes, pc.getSortedVars());
    return counter.count(pc, filteredBoxes, filteredDomains, budget); 
  }
  
  private static EstimateResult filterAndCount(PC pc, CountingProblem cp,
      ModelCounter counter, int budget) {
    List<Interval[]> filteredBoxes = MCUtil.filterBoxes(cp.boxes, pc.getSortedVars());
    Interval[] filteredDomain = MCUtil.filterIntervals(cp.domain, pc.getSortedVars());
    CountingProblem filteredProblem = new CountingProblem(pc, filteredBoxes, cp.lit2rvar, filteredDomain);
    return counter.count(filteredProblem, budget);
  }
  
  private static EstimateResult filterAndCount(PC pc,
      List<Interval[]> boxes, CountingProblem cp, ModelCounter counter, int budget) {
    List<Interval[]> filteredBoxes = MCUtil.filterBoxes(boxes, pc.getSortedVars());
    Interval[] filteredDomain = MCUtil.filterIntervals(cp.domain, pc.getSortedVars());
    CountingProblem filteredProblem = new CountingProblem(pc, filteredBoxes, cp.lit2rvar, filteredDomain);
    EstimateResult result = counter.count(filteredProblem, budget);
    
    //TODO fix this hack
    latestFilteredBoxes = filteredBoxes;
    if (Config.storeVariance) {
//    ((MonteCarloCounter)counter).cpToResult.put(filteredProblem, (PartitionEstimateResult) result);
      ((MonteCarloCounter)counter).cpToResult.put(MCUtil.boxId(filteredBoxes), (PartitionEstimateResult) result);
    }
    
    return result;
  }

  private static List<Interval[]> latestFilteredBoxes;
  
  // Iterative improvement stuff
  
  public static class Position {
    public final CountingProblem problem;
    public final int pcIndex;
    public final int partitionIndex;
    public Position(CountingProblem problem, int pcIndex, int partitionIndex) {
      this.problem = problem;
      this.pcIndex = pcIndex;
      this.partitionIndex = partitionIndex;
    }
  }
  
  public static class Partition {
    public final Set<SymBool> clauses;
    public final List<Position> appearances;

    public int timesImproved;
    public double derivative;
    public PartitionEstimateResult result;
    
    public Partition(Set<SymBool> clauses) {
      this.clauses = clauses;
      this.appearances = new ArrayList<Position>();
      this.timesImproved = 0;
      this.result = null;
    }

    //TODO refactor this
    public double updateRank(Map<List<SymBool>,Set<Partition>> prob2part, Set<Partition> deps) {
      double myVar = result.getVariance();
      long myNSamples = result.getNumberOfSamples();
      
      double total;
      
      if (myNSamples == 0) { //realpaver returned no boxes, partition is infeasible
        total = 0;
      } else if (Config.mcDerivativeVersion == 2) { //exact derivative
        double factor = myVar/myNSamples; //Va/Na
        total = 0;
        
        for (Position pos : appearances) {
          List<SymBool> pcCons = pos.problem.pc.getConstraints();
          Set<Partition> pcPartitions = prob2part.get(pcCons);
          
          double depProd = 1;
          for (Partition dep : pcPartitions) {
            double depMean = dep.result.getMean();
            double depVar = dep.result.getVariance();
            depProd *= ((depMean * depMean) + depVar);
          }
          
          total += depProd * factor; 
        }
      } else { //upper bound
        if (Config.mcDerivativeVersion == 0) {
          total = myVar/myNSamples;
        } else if (Config.mcDerivativeVersion == 1) {
          total = 1;
        } else {
          throw new RuntimeException();
        }
        double depSum = 0;
        for (Partition dep : deps) {
          double depMean = dep.result.getMean();
          double depVar = dep.result.getVariance();
          
          depSum = depSum + ((depMean * depMean) + depVar);
        }
        total = total * depSum;
      }
      this.derivative = total;
      return total;
    }
  }
  
  public static class ImprovementResult {
    public final int pcIndex;
    public final int partitionIndex;
    public final EstimateResult partitionResult;
    
    public ImprovementResult(int pcIndex, int partitionIndex,
        EstimateResult partitionResult) {
      this.pcIndex = pcIndex;
      this.partitionIndex = partitionIndex;
      this.partitionResult = partitionResult;
    }
  }
  
  public Map<List<SymBool>,List<Set<SymBool>>> prob2partClauses;
  public Map<List<SymBool>,Set<Partition>> prob2part;
  public List<Partition> allPartitions;
  public Map<Partition,Set<Partition>> part2dep;
  public Map<List<SymBool>,CountingProblem> pc2problem;
  
  @Override
  protected void notifyPartitions(List<Set<SymBool>> pcPartitions) {
    if (Config.mcIterativeImprovement) {
      prob2partClauses.put(callback.getDefaultPC().getConstraints(), pcPartitions);
    }
  }
  
  public boolean prepareIterative() {
    try {
      computePartitionDependencies();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } 

    //compute derivatives
    double max = 0;
    int i = 0;
    for (Partition part : allPartitions) {
      Set<Partition> deps = part2dep.get(part);
      double rank = part.updateRank(prob2part,deps);
      if (rank > max) {
        max = rank;
      }
//      System.out.println("c"+i+"[r="+deps.size()+"]: mean=" + part.result.mean + " var=" + part.result.variance + " derivative="+part.derivative);
//      System.out.println("GREPTHIS " + i+","+deps.size()+"," + part.result.mean + "," + part.result.variance + ","+part.derivative);
      i++;
    }
    
    // Two possibilities: all variables are dependent and so there
    // is no partitions, or the variance for all constraints is 0.
    // In any case, notify the caller.
    return max > 0;
  }
  
  public void prepareUniform() {
    for (Partition part : allPartitions) {
      part.derivative = 1;
    }
  }

  

  private void computePartitionDependencies() throws ExecutionException {
    int i = 0;
    Map<Set<SymBool>,Partition> tmp = new HashMap<Set<SymBool>, CachedCounter.Partition>();
    for (java.util.Map.Entry<List<SymBool>, List<Set<SymBool>>> e : prob2partClauses.entrySet()) {
      List<SymBool> pc = e.getKey();
      List<Set<SymBool>> partitionClauses = e.getValue();
      CountingProblem problem = pc2problem.get(pc);

      //prepare maps, collect partitions
      Set<Partition> probPartitions = prob2part.get(pc);
      if (probPartitions == null) {
        probPartitions = new HashSet<CachedCounter.Partition>();
        prob2part.put(pc, probPartitions);
      }
      
      List<Partition> partitions = new ArrayList<CachedCounter.Partition>();
      for(Set<SymBool> partClauses : partitionClauses) {
        Partition partition = tmp.get(partClauses);
        
        if (partition == null) { //create 
          partition = new Partition(partClauses);
          partition.result = (PartitionEstimateResult) getCached(partClauses);
          tmp.put(partClauses, partition);
          part2dep.put(partition, new HashSet<CachedCounter.Partition>());
          allPartitions.add(partition);
        }
        partitions.add(partition);
      }
      
      //store dependencies
      int j = 0;
      for (Partition part : partitions) {
        part.appearances.add(new Position(problem,i,j));
        probPartitions.add(part);
        Set<Partition> dependencies = part2dep.get(part);
        for(int k = 0; k < partitions.size(); k++) {
          if (k != j) {
            dependencies.add(partitions.get(k));
          }
        }
        j++;
      }
      i++;
    }
  }

  public ImprovementResult[] improve(int startingNSamples) {
    Partition chosenPart = choosePartition();
    ImprovementResult[] improved = computeImprovements(startingNSamples,
        chosenPart, false);
    //update only the affected results
    updateDerivatives(chosenPart);
    return improved;
  }
  
  //choose one random partition
  public ImprovementResult[] improve(int budget, RandomGenerator rng) {
    Partition chosenPart = choosePartitionAtRandom(rng);
    ImprovementResult[] improved = computeImprovements(budget,
        chosenPart, false);
    //update only the affected results
    updateDerivatives(chosenPart);
    return improved;
  }

  private Partition choosePartition() {
    Partition chosenPart = allPartitions.get(0);
    Partition old = chosenPart;
    int c = 0, c1 = 0,c2 = 0;
    for (Partition part : allPartitions) {
      if (part.derivative > chosenPart.derivative) {
        old = chosenPart;
        c2 = c1;
        chosenPart = part;
        c1 = c;
      }
      c++;
    }
//    System.out.println("[qCORAL-iterative] chosen partition: c" + c1);
//    System.out.println("[qCORAL-iterative]    > mean=" + chosenPart.result.mean + " var=" + chosenPart.result.variance + " deriv=" + chosenPart.derivative);
//    System.out.println("[qCORAL-iterative]  c"+c2+"> mean=" + old.result.mean + " var=" + old.result.variance + " deriv=" + old.derivative);
    return chosenPart;
  }
  
  private Partition choosePartitionAtRandom(RandomGenerator rng) {
    List<Partition> list = new ArrayList<Partition>(allPartitions.size());
    for (Partition part : allPartitions) {
      if (part.result.variance > 0) {
        list.add(part);
      }
    }
    int size = list.size();
    return list.get(rng.nextInt(size));
  }
  
  public List<ImprovementResult[]> improveProportionally(long budget) {
    double rankSum = 0;
    long spentSamples = 0;
    for (Partition part: allPartitions) {
      rankSum += part.derivative;
      spentSamples += part.result.nSamples;
    }
    
    List<ImprovementResult[]> improvements = new ArrayList<ImprovementResult[]>(allPartitions.size());
    
    for (Partition part: allPartitions) {
      long improvBudget = (long) Math.ceil(budget * (part.derivative / rankSum));
      if (improvBudget != 0) {
        ImprovementResult[] partitionImprovement = computeImprovements((int) improvBudget, part, true);
        improvements.add(partitionImprovement);
      }
    }
    
    long improvedSamples = 0;
    for (Partition part: allPartitions) {
      improvedSamples += part.result.nSamples;
    }
    improvedSamples = improvedSamples - spentSamples;
    
    System.out
        .println("[qCORAL-allocation] Proportional improvement ended. Spent "
            + improvedSamples + " - original budget was " + budget);
    
    return improvements;
  }
  

  private ImprovementResult[] computeImprovements(int budget,
      Partition chosenPart, boolean atLeastOnePerBox) {
    ImprovementResult[] improved = new ImprovementResult[chosenPart.appearances.size()];

    int i = 0;
    for (Position pos : chosenPart.appearances) {
      CountingProblem problem = pos.problem;
      if (atLeastOnePerBox) {
        //TODO make sure this is cached properly
        callback = buildCallback(problem, budget);
        PC pc = new PC(new ArrayList<SymBool>(chosenPart.clauses)); 
        List<Interval[]> boxes;
        try {
          boxes = isolver.getPaving(pc,callback.getDefaultInterval(pc));
          if (budget < boxes.size()) {
            budget = boxes.size();
          }
        } catch (IOException e) {
          System.out.println("Error calling realpaver: " + e.getMessage());
          e.printStackTrace();
        } catch (InterruptedException e) {
          System.out.println("Error calling realpaver: " + e.getMessage());
          e.printStackTrace();
        }
      }
      
      callback = buildCallback(problem, budget);
      
      //call the solver in the first iteration, and then use the cached result to
      //update the estimates
      EstimateResult result;
      if (i == 0) {
        result = updateOne(problem.pc.getConstraints(),pos.partitionIndex);
        chosenPart.result = (PartitionEstimateResult) result.getSubResults()[pos.partitionIndex];
      } else {
        result = updateOne(problem.pc.getConstraints(),-1);
      }
      
      improved[i] = new ImprovementResult(pos.pcIndex, pos.partitionIndex, result);
      i++;
    }
    return improved;
  }

  private void updateDerivatives(Partition chosenPart) {
    Set<Partition> chosenPartDeps = part2dep.get(chosenPart);
    chosenPart.updateRank(prob2part,chosenPartDeps);
    for (Partition part : chosenPartDeps) {
      part.updateRank(prob2part,part2dep.get(part)); 
    }
  }
  
  private EstimateResult updateOne(List<SymBool> conjunct, int partitionIndex) {
    int size = partitionResult.getIndependentVarsClusters().size();
    EstimateResult[] results = new EstimateResult[size];
    
    int i = 0;
    for (Set<String> cluster : partitionResult.getIndependentVarsClusters()) {
      //TODO maybe cache this?
      Set<SymBool> problem = extractPartition(conjunct, cluster);
      if (problem.isEmpty()) { //none of the vars in cluster are in the conjunct
        continue;
      }
      
      EstimateResult cached = null;
      cached = getCached(problem);
      if (i == partitionIndex) {
        EstimateResult update = processConstraint(problem);
        cached = cached.mergeResults(update);
        addToCache(problem, cached);

        //TODO another instance of the same hack - fix this later
        if (Config.mcProportionalBoxSampleAllocation) {
          ((MonteCarloCounter)counter).cpToResult.put(MCUtil.boxId(latestFilteredBoxes), (PartitionEstimateResult) cached);
        }
      }
      results[i] = cached;
      
      i++;
    }
    
    if (i != size) {
      EstimateResult[] tmp = new EstimateResult[i];
      System.arraycopy(results, 0, tmp, 0, i);
      results = tmp;
    }
    
    return new PCEstimateResult(results);
  }
}
