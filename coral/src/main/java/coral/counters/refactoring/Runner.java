package coral.counters.refactoring;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import coral.PC;
import coral.counters.CountingProblem;
import coral.counters.DistributionAwareQCoral;
import coral.counters.Main;
import coral.counters.refactoring.Partitioner.PartitionData;
import coral.counters.refactoring.counting.Counter;
import coral.counters.refactoring.counting.ExaustiveCounter;
import coral.counters.refactoring.counting.MonteCarloCounter;
import coral.counters.refactoring.estimate.DumbRankedEstimate;
import coral.counters.refactoring.estimate.Estimate;
import coral.counters.refactoring.estimate.PCEstimate;
import coral.counters.refactoring.estimate.RankedPartitionEstimate;
import coral.counters.refactoring.problem.BoxedProblem;
import coral.counters.refactoring.problem.Domain;
import coral.counters.refactoring.problem.PCProblem;
import coral.counters.refactoring.problem.PartitionProblem;
import coral.counters.refactoring.problem.Problem;
import coral.util.Config;
import coral.util.options.Options;
import coral.util.trees.Trie;
import coral.util.trees.TrieNode;
import coral.util.trees.TrieWalker;
import coral.util.visitors.MathematicaVisitor;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;
import org.apache.commons.math3.util.Pair;
import symlib.SymLiteral;
import symlib.eval.Evaluator;
import symlib.eval.compilation.JavaPcCompiler;
import symlib.eval.compilation.PcCompiler;
import symlib.parser.ParseException;

public class Runner {

  private static Options options = new Options(Main.class, Config.class);
  private static final boolean DEBUG = false;
  public Map<CountingProblem,PCEstimate> lastSetOfEstimates = null;

  public static void main(String[] args) throws ParseException, IOException, InterruptedException {
    args = processOptions(args);
    RandomGenerator rng = new Well44497b(Config.mcSeed);

    File input = new File(args[0]);
    Scanner s = new Scanner(input);
    List<CountingProblem> problems = DistributionAwareQCoral.processInput(s,rng);
    //dumpMathematicaBruteForceInput(problems);
    long start = System.nanoTime();
    Estimate estimate;
    Runner runner = new Runner();
    if (Config.mcIterativeImprovement) {
      estimate = runner.runIterativeAnalysis(rng, problems, start);
    } else if (Config.mcExaustiveSearch) {
      estimate = runner.runExaustiveSearch(problems);//runAnalysis(rng, problems);
    } else {
      throw new RuntimeException("Invalid option: choose between iterative or exaustive search");
    }
    
    long end = System.nanoTime();
    double time = (end - start) / 1000000000.0;
    
    System.out.printf("mean=%e,variance=%e, stdev=%e, time=%f\n",estimate.estimate,estimate.variance,Math.sqrt(estimate.variance),time);
    System.out
        .printf(  
            "[qCORAL:results] samples=%d, mean=%e, variance=%e, time=%f, stdev=%e\n",
            estimate.nSamples, estimate.estimate, estimate.variance, time,
            Math.sqrt(estimate.variance));
    
    s.close();
  }

  public static Estimate runExaustiveSearch(List<CountingProblem> problems) {
    //problem modelling
    List<PCProblem> pcProblems = buildModel(problems);
    
    // Partitioning
    Multimap<PartitionProblem,PCProblem> part2pcs = HashMultimap.create();
    Partitioner partitioner = new Partitioner();
    PartitionData<PCProblem> partitionData = partitioner.partition(pcProblems);
    Set<PartitionProblem> partProblems = partitionData.partitions;

    // Compilation
    long compileStart = System.nanoTime();
    Map<PartitionProblem,Evaluator> part2eval = new LinkedHashMap<>();
    PcCompiler compiler = new JavaPcCompiler();
    for (PartitionProblem pp : partProblems) {
      compiler.include(pp.getPC());
    }
    Map<PC,Evaluator> pc2eval = compiler.compile();

    System.out.println("[qCORAL-Compilation] compilation cost (s): "
        + (System.nanoTime() - compileStart)/Math.pow(10,9));

    // RealPaver
    Map<PartitionProblem, List<BoxedProblem>> part2box = callRealPaver(partProblems);
    
    //counters
    Counter mc = new ExaustiveCounter();
    //result map
    Map<PartitionProblem,RankedPartitionEstimate> part2estimate = Maps.newLinkedHashMap();

    double mean = 0;
    long nsamples = 0;
    for (PartitionProblem partProblem : partProblems) {
      List<BoxedProblem> boxedProblems = part2box.get(partProblem);
      Estimate[] boxEstimates = countPartition(1, mc, boxedProblems, pc2eval);
      RankedPartitionEstimate estimate = new DumbRankedEstimate(boxEstimates);
      mean += estimate.partitionEstimate.estimate;
      nsamples += estimate.partitionEstimate.nSamples;
    }

    return new Estimate(mean, 0.0, nsamples);
  }

  private static void dumpMathematicaBruteForceInput(
      List<CountingProblem> problems) {
    try {
      String name = "mathematica-parallel-sum.math";
      PrintWriter writer = new PrintWriter(name,"UTF-8");
      List<PC> pcs = new ArrayList<PC>();
      Map<Integer,Pair<Integer,Integer>> domainMap = new LinkedHashMap<Integer, Pair<Integer, Integer>>();
      for (CountingProblem p : problems) {
        pcs.add(p.pc);
        int i = 0;
        for (SymLiteral lit : p.pc.getSortedVars()) {
          domainMap.put(lit.getId(), Pair.create(p.domain[i].intLo(),p.domain[i].intHi()));
          i++;
        }
      }
      writer.println(MathematicaVisitor.inputForProbabilityComputation(pcs, domainMap, false));
      writer.close();
      System.out.println("[mathematica-dump] Parallel sum input file dumped to " + name);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  

  public Estimate runIterativeAnalysis(RandomGenerator rng,
      List<CountingProblem> problems, long start) {
    System.out.println("[qCORAL-iterative] Iterative algorithm was chosen. Partitioning, caching and interval solving are now enabled.");
    Config.cachePavingResults = true;
    Main.mcPartitionAndCache = true;
    Main.mcCallIntSolPartition = true;
    final boolean useExactDerivativeInRanking = true;
    boolean dumbRanking = Config.mcDumbRankingHeuristic;
    
    Estimate finalEstimate;
    
    //options
    //if initialSamplingBudget == globalBudget...
    final long globalBudget = Config.mcMaxSamples;
    final long initialSamplingBudget = Config.mcInitialPartitionBudget;
    long spentBudget = 0;

    //problem modelling
    List<PCProblem> pcProblems = buildModel(problems) ;
    
    // Partitioning
    Multimap<PartitionProblem,PCProblem> part2pcs = HashMultimap.create();
    Partitioner partitioner = new Partitioner();
    PartitionData<PCProblem> partitionData = partitioner.partition(pcProblems);
    Set<PartitionProblem> partProblems = partitionData.partitions;

    // Compilation
    long compileStart = System.nanoTime();
    Map<PartitionProblem,Evaluator> part2eval = new LinkedHashMap<>();
    PcCompiler compiler = new JavaPcCompiler();
    for (PartitionProblem pp : partProblems) {
      compiler.include(pp.getPC());
    }
    Map<PC,Evaluator> pc2eval = compiler.compile();

    System.out.println("[qCORAL-Compilation] compilation cost (s): "
        + (System.nanoTime() - compileStart)/Math.pow(10,9));

    //samples per partition
    int distinctPartitions = partProblems.size();
    long samplesPerPartition = initialSamplingBudget / distinctPartitions;
    System.out.println("[qCORAL] Samples per partition ("+ distinctPartitions + " total): " + samplesPerPartition);
    
    // RealPaver
    Map<PartitionProblem, List<BoxedProblem>> part2box;
    if (Config.mcUseRealPaver) {
      part2box = callRealPaver(partProblems);
    } else {
      part2box = fakeRealPaver(partProblems);
    }

    //counters
    Counter mc = new MonteCarloCounter(rng, Config.mcSeed);
    //result map
    Map<PartitionProblem,RankedPartitionEstimate> part2estimate = Maps.newLinkedHashMap();
    
    
    // Initial estimate
    for (PartitionProblem partProblem : partProblems) {
      List<BoxedProblem> boxedProblems = part2box.get(partProblem);
      Estimate[] boxEstimates = countPartition(samplesPerPartition, mc, boxedProblems, pc2eval);
      RankedPartitionEstimate rankedEstimate;
      if (dumbRanking) {
        rankedEstimate = new DumbRankedEstimate(boxEstimates);
      } else {
        rankedEstimate = new RankedPartitionEstimate(boxEstimates);
      }
      part2estimate.put(partProblem, rankedEstimate);
      spentBudget += rankedEstimate.partitionEstimate.nSamples;
      
//      System.out.println(rankedEstimate);
    }

    //link the results of partitions that appear in the same PC 
    populatePartitionEstimateDeps(partitionData, part2estimate);
    
    //do the same with pcs and their partitions
    Map<PCProblem, PCEstimate> pcprob2estimate = buildPCEstimateMap(pcProblems,
        partitionData, part2estimate);
    
    Estimate initialEstimate = aggregate(pcprob2estimate.values(), spentBudget, false);

    //now for the iterative version
    final int incrementBudget = Config.mcSamplesPerIncrement;
    boolean proportionalBudget = Config.mcAllocateBudgetProportionally;
    boolean proportionalBoxSamples = Config.mcProportionalBoxSampleAllocation;
    boolean nonRankedIterative = Config.mcNonRankedIterativeImprovement;
    
    
    System.out.printf(
            "[qCORAL-iterative] initial run done. %d samples were spent. %d remaining; step=%d\n",
            spentBudget, globalBudget - spentBudget, incrementBudget);
    
    //prepare the problem list
    boolean performRanking = !nonRankedIterative; 
    List<ProblemPlusEstimate> ppeList = prepareProblemsForIterative(
        part2estimate, performRanking, useExactDerivativeInRanking);
    boolean allRanksEqualToZero = true;
    for (ProblemPlusEstimate ppe : ppeList) {
      if (ppe.estimate.rank != 0) {
        allRanksEqualToZero = false;
        break;
      }
    }
    
    if (performRanking && allRanksEqualToZero) { //spend all the remaining samples uniformly 
      long remainingBudget = globalBudget - spentBudget;
      samplesPerPartition = (int) (remainingBudget / distinctPartitions);
      System.err
          .println("Not possible to rank partitions. possible reasons include:\n"
              + "1. All variables are dependent on each other. \n"
              + "2. RealPaver returned very precise boxes. \n"
              + "The remainder of the samples will be spent uniformly.");
      Config.mcAllocateBudgetProportionally = true;
      
      long spentUniformBudget = updateEstimatesUniformly(ppeList, part2box, pc2eval, mc,
          samplesPerPartition);
      finalEstimate = aggregate(pcprob2estimate.values(), spentUniformBudget
          + spentBudget, true);
      System.out.println("[qCORAL-iterative] samples=" + finalEstimate.nSamples
          + " mean=" + finalEstimate.estimate + " var=" + finalEstimate.variance);
      
      Map<CountingProblem,PCEstimate> probToEstimates = new LinkedHashMap<>();
      int i = 0;
      for (CountingProblem prob : problems) {
      	PCProblem pcprob = pcProblems.get(i);
      	PCEstimate est = pcprob2estimate.get(pcprob);
      	probToEstimates.put(prob, est);
      	i++;
      }
      lastSetOfEstimates = probToEstimates;

    } else { //default incremental behavior
      boolean lastIteration = false;
      Estimate iterativeEstimate = initialEstimate;
      long spentIterativeBudget = 0;
      
      while (iterativeEstimate.variance > Config.mcTargetVariance
          && globalBudget > spentBudget + spentIterativeBudget && !lastIteration) {
        
        //compute number of samples
        long currentIterationBudget;
        if (spentIterativeBudget + spentBudget + incrementBudget > globalBudget) { 
          lastIteration = true;  // don't overrun the budget
          currentIterationBudget = globalBudget - (spentIterativeBudget + spentBudget);
        } else {
          currentIterationBudget = incrementBudget;
        }
        
        long samplesSpentInThisIteration;
        if (proportionalBudget) {
          //improve every partition with rank > 0
          List<ProblemPlusEstimate> toBeImproved = collectRankNotZero(ppeList);
          samplesSpentInThisIteration = improveRankedPartitionsProportionally(
              toBeImproved, part2box, pc2eval, mc, currentIterationBudget,
              proportionalBoxSamples,useExactDerivativeInRanking);
          
          //update pc estimates
          updatePCEstimates(partitionData, pcprob2estimate, toBeImproved);
          
        } else if (nonRankedIterative) {
          //improve all partitions uniformly (with at least one sample for each box)
          samplesPerPartition = currentIterationBudget / distinctPartitions;
          samplesSpentInThisIteration = updateEstimatesUniformly(ppeList, part2box, pc2eval, mc,
              samplesPerPartition);
          //update *all* pc estimates
          updateAllPCEstimates(pcprob2estimate);
        } else {
          //improve best ranked partition
          ProblemPlusEstimate bestRanked = findBestRanked(ppeList);
          improveRankedPartition(bestRanked, part2box, pc2eval, mc,
              currentIterationBudget, proportionalBoxSamples,
              useExactDerivativeInRanking);
          samplesSpentInThisIteration = currentIterationBudget;
          updatePCEstimates(partitionData, pcprob2estimate, Lists.newArrayList(bestRanked));
        }
        spentIterativeBudget += samplesSpentInThisIteration;
        
        iterativeEstimate = aggregate(pcprob2estimate.values(), spentBudget + spentIterativeBudget, false);
        long now = System.nanoTime();
        System.out.println("[qCORAL-iterative] samples="
            + (spentBudget + spentIterativeBudget) + " mean="
            + iterativeEstimate.estimate + " var=" + iterativeEstimate.variance
            + " time=" + ((now - start) / 1000000000.0));
      }
      finalEstimate = iterativeEstimate;
      Map<CountingProblem,PCEstimate> probToEstimates = new LinkedHashMap<>();
      int i = 0;
      for (CountingProblem prob : problems) {
      	PCProblem pcprob = pcProblems.get(i);
      	PCEstimate est = pcprob2estimate.get(pcprob);
      	probToEstimates.put(prob, est);
      	i++;
      }
      lastSetOfEstimates = probToEstimates;
    }
    
    return finalEstimate;
  }

  private static void improveRankedPartition(ProblemPlusEstimate bestRanked,
                                             Map<PartitionProblem, List<BoxedProblem>> part2box,
                                             Map<PC,Evaluator> pc2Eval,
                                             Counter mc,
                                             long currentIterationBudget,
                                             boolean proportionalBoxSamples,
                                             final boolean useExactDerivativeInRanking) {
    PartitionProblem problem = bestRanked.problem;
    RankedPartitionEstimate currentEstimate = bestRanked.estimate;
    List<BoxedProblem> boxedProblems = part2box.get(problem);
    Estimate[] improvedEstimates = improveEstimate(boxedProblems,
        currentEstimate, pc2Eval,mc, currentIterationBudget,
        proportionalBoxSamples);
//    System.out.println(">> old estimate: " + currentEstimate);
    currentEstimate.updateBoxEstimates(improvedEstimates);
    currentEstimate.updateRank();
//    System.out.println(">> new estimate: " + currentEstimate);
  }

  private static void updateAllPCEstimates(
      Map<PCProblem, PCEstimate> estimateMap) {
    for (PCEstimate estimate : estimateMap.values()) {
      estimate.update();
    }
  }
  
  private static void updatePCEstimates(PartitionData<PCProblem> partitionData,
      Map<PCProblem, PCEstimate> pcprob2estimate,
      Collection<ProblemPlusEstimate> toBeImproved) {
    Set<PCProblem> impactedProblems = Sets.newLinkedHashSet(); 
    for (ProblemPlusEstimate improved : toBeImproved) {
      PartitionProblem improvedPartition = improved.problem;
      Collection<PCProblem> relatedSourceProblems = partitionData.part2source
          .get(improvedPartition);
      impactedProblems.addAll(relatedSourceProblems);
    }
    for (PCProblem impactedProblem : impactedProblems) {
      PCEstimate estimate = pcprob2estimate.get(impactedProblem);
      estimate.update();
    }
  }

  private static ProblemPlusEstimate findBestRanked(
      List<ProblemPlusEstimate> ppeList) {
    ProblemPlusEstimate bestRanked = ppeList.get(0);
    ProblemPlusEstimate secondBestRanked = ppeList.get(0);
    for (ProblemPlusEstimate ppe : ppeList) {
      if (ppe.estimate.rank > bestRanked.estimate.rank) {
        secondBestRanked = bestRanked;
        bestRanked = ppe;
      }
    }
    
//    System.out.println("[iterative] partition chosen: " + bestRanked.estimate);
//    System.out.println("            second place:     " + secondBestRanked.estimate);
    
    return bestRanked;
  }

  private static long improveRankedPartitionsProportionally(List<ProblemPlusEstimate> toBeImproved,
                                                            Map<PartitionProblem, List<BoxedProblem>> part2box,
                                                            Map<PC,Evaluator> pc2eval,
                                                            Counter mc,
                                                            long iterationBudget,
                                                            boolean proportionalBoxSamples,
                                                            boolean exactDerivative) {
    double[] ranks = collectRanks(toBeImproved);
    double rankSum = 0;
    for (double r : ranks) {
      rankSum += r;
    }
    long samplesSpentInThisIteration = 0;

    //every partition takes at least 1 sample
    for (ProblemPlusEstimate ppe : toBeImproved) {
      RankedPartitionEstimate estimate = ppe.estimate;
      PartitionProblem problem = ppe.problem;
      long oldNSamples = estimate.partitionEstimate.nSamples;
      
      List<BoxedProblem> boxes = part2box.get(problem);
      long budget = (long) Math.ceil(iterationBudget * (ppe.estimate.rank / rankSum));
      Estimate[] improvedEstimates = improveEstimate(boxes,
          estimate, pc2eval, mc, budget, proportionalBoxSamples);
      estimate.updateBoxEstimates(improvedEstimates);
      long newNSamples = estimate.partitionEstimate.nSamples;
      samplesSpentInThisIteration += (newNSamples - oldNSamples);
    }
    
    //update ranks
    for (ProblemPlusEstimate ppe : toBeImproved) {
      RankedPartitionEstimate estimate = ppe.estimate;
      estimate.updateRank();
    }
    
    return samplesSpentInThisIteration;
  }

  private static List<ProblemPlusEstimate> collectRankNotZero(
      Collection<ProblemPlusEstimate> ppes) {
    List<ProblemPlusEstimate> toBeImproved = Lists.newArrayList();
    for (ProblemPlusEstimate ppe : ppes) {
      if (ppe.estimate.rank > 0) {
        toBeImproved.add(ppe);
      }
    }
    return toBeImproved;
  }

  private static Estimate[] improveEstimate(List<BoxedProblem> boxedProblems,
                                            RankedPartitionEstimate currentEstimate,
                                            Map<PC,Evaluator> pc2eval,
                                            Counter mc,
                                            long budget,
                                            boolean proportionalBoxSamples) {
    int size = boxedProblems.size();
    if (size == 0) {
      return new Estimate[]{};
    }
    
    Estimate[] boxEstimates = new Estimate[size];
    int[] samplesPerBox = new int[size];
    
    if (proportionalBoxSamples) { //split one third equally and two thirds proportionally
      double[] boxRanks = new double[size];
      int nonZeroDomains = 0;
      for (int i = 0; i < size; i++) {
        BoxedProblem current = boxedProblems.get(i);
        if (current.getDomain().volume.isZero()) { //ignore 0-volume boxes
          boxRanks[i] = 0;
        } else {
          nonZeroDomains++;
          Estimate boxEstimate = currentEstimate.boxEstimates[i];
          boxRanks[i] = boxEstimate.variance * current.getDomain().volume.doubleValue();
        }
      }
        
      long totalSpent = 0;
      long defaultPerNonZeroDomain = budget / (3 * nonZeroDomains); 
      for (int i = 0; i < size; i++) {
        if (boxedProblems.get(i).getDomain().volume.isZero()) {
          samplesPerBox[i] = 0;
        } else {
          samplesPerBox[i] = (int) defaultPerNonZeroDomain;
          totalSpent += defaultPerNonZeroDomain;
        }
      }
      long remainingBudget = budget - totalSpent;
      int[] proportionalSamplesPerBox = CountingUtils
          .computeNumSamplesProportionally((int) remainingBudget, boxRanks);
      
      for (int i = 0; i < size; i++) {
        samplesPerBox[i] = samplesPerBox[i] + proportionalSamplesPerBox[i]; 
      }
    } else {
      Arrays.fill(samplesPerBox, (int) budget / size);
    }
    
//    System.out.println(">> (" + budget + ") " + Arrays.toString(samplesPerBox));

    for (int i = 0; i < size; i++) {
      BoxedProblem current = boxedProblems.get(i);
      Evaluator evaluator = pc2eval.get(current.getPC());
      int boxBudget = samplesPerBox[i];
      Estimate estimate = mc.count(current, evaluator, boxBudget);
      boxEstimates[i] = estimate;
    }
    
    return boxEstimates;
  }

  private static double[] collectRanks(List<ProblemPlusEstimate> toBeImproved) {
    double[] ranks = new double[toBeImproved.size()];
    for (int i = 0; i < ranks.length; i++) {
      ranks[i] = toBeImproved.get(i).estimate.rank;
    }
    return ranks;
  }

  private static Estimate aggregate(Collection<PCEstimate> estimates, long samples, boolean update) {
    double mean = 0;
    double variance = 0;
    int i = 0;
    for (PCEstimate estimate : estimates) {
      if (update) {
        estimate.update();
      }
      
      if (DEBUG) {
        System.out.println(estimate.pcEstimate.estimate + "," + estimate.pcEstimate.variance);
      }
      i++;
      
      mean += estimate.pcEstimate.estimate;
      variance += estimate.pcEstimate.variance;
    }
    
    return new Estimate(mean, variance, samples);
  }

  private static long updateEstimatesUniformly(Collection<ProblemPlusEstimate> ppes,
      Map<PartitionProblem, List<BoxedProblem>> part2box, Map<PC,Evaluator> pc2eval, Counter mc,
      long samplesPerPartition) {
    long spentUniformBudget = 0;
    for (ProblemPlusEstimate ppe : ppes) {
      PartitionProblem partProblem = ppe.problem;
      List<BoxedProblem> boxedProblems = part2box.get(partProblem);
      //take at least one sample in each box.
      long samples = Math.max(samplesPerPartition, boxedProblems.size());
      Estimate[] newBoxEstimates = countPartition(samples, mc, boxedProblems, pc2eval);
      RankedPartitionEstimate partEstimate = ppe.estimate;
      
      long oldNSamples = partEstimate.partitionEstimate.nSamples;
      partEstimate.updateBoxEstimates(newBoxEstimates);
      long newNSamples = partEstimate.partitionEstimate.nSamples;
      spentUniformBudget += newNSamples - oldNSamples;
    }
    return spentUniformBudget;
  }

  private static Estimate[] countPartition(long samplesPerPartition,
                                           Counter mc,
                                           List<BoxedProblem> boxedProblems,
                                           Map<PC, Evaluator> pc2eval) {
    int size = boxedProblems.size();
    int nonZeroPartitions = 0;
    for (int i = 0; i < size; i++) {  
      BoxedProblem boxp = boxedProblems.get(i);
      if (boxp.getDomain().volume.isPositive()) {
        nonZeroPartitions++;
      }
    }
    if (size == 0 || nonZeroPartitions == 0) {
      return new Estimate[]{};
    }

    Estimate[] boxEstimates = new Estimate[size];
    long samplesPerBox = samplesPerPartition / nonZeroPartitions;

    for (int i = 0; i < size; i++) {
      BoxedProblem current = boxedProblems.get(i);
      Estimate estimate;
      Evaluator evaluator = pc2eval.get(current.getPC());
      if (current.getDomain().volume.isPositive()) { 
        estimate = mc.count(current, evaluator, samplesPerBox);
      } else {
        estimate = Estimate.ZERO;
      }
      boxEstimates[i] = estimate;
    }
    return boxEstimates;

  }

  private static List<ProblemPlusEstimate> prepareProblemsForIterative(
      Map<PartitionProblem, RankedPartitionEstimate> part2estimate,
      boolean updateRank,
      boolean useExactDerivativeInRanking) {
    List<ProblemPlusEstimate> ppeList = Lists.newArrayListWithCapacity(part2estimate.size());
    for (Entry<PartitionProblem, RankedPartitionEstimate> entry : part2estimate.entrySet()) {
      PartitionProblem problem = entry.getKey(); 
      RankedPartitionEstimate estimate = entry.getValue();
      if (updateRank) {
        estimate.updateRank();
      }
      ProblemPlusEstimate ppe = new ProblemPlusEstimate(estimate, problem);
      ppeList.add(ppe);
    }
    return ppeList;
  }

  public static class ProblemPlusEstimate implements Comparable<ProblemPlusEstimate> {
    public final RankedPartitionEstimate estimate;
    public final PartitionProblem problem;
    public ProblemPlusEstimate(RankedPartitionEstimate estimate,
        PartitionProblem problem) {
      this.estimate = estimate;
      this.problem = problem;
    }
    @Override
    public int compareTo(ProblemPlusEstimate e2) {
      return (int) Math.signum(e2.estimate.rank - this.estimate.rank)  ;
    }
  }

  private static Map<PartitionProblem, List<BoxedProblem>> fakeRealPaver
      (Set<PartitionProblem> partProblems) {
    System.out.println("[qCORAL] realpaver is disabled!");
    Map<PartitionProblem, List<BoxedProblem>> part2box = new LinkedHashMap<>();
    for (PartitionProblem pp : partProblems) {
      part2box.put(pp, ImmutableList.of(new BoxedProblem(pp.getDomain(), pp)));
    }
    return part2box;
  }

  private static Map<PartitionProblem, List<BoxedProblem>> callRealPaver(
      Set<PartitionProblem> partProblems) {
    Map<PartitionProblem,List<BoxedProblem>> part2box = Maps.newLinkedHashMap();
    Paver paver = new Paver();
    for (PartitionProblem partProblem : partProblems) {
      List<BoxedProblem> boxedProblems = paver.paving(partProblem);
      part2box.put(partProblem, boxedProblems);
    }
    return part2box;
  }

  public static List<PCProblem> buildModel(List<CountingProblem> problems) {
    List<PCProblem> pcProblems = Lists.newArrayListWithCapacity(problems.size()); 
    for (CountingProblem problem : problems) {
      PC pc = problem.pc;
      Domain domain = new Domain(pc.getSortedVars(), Lists.newArrayList(problem.domain),
          Lists.newArrayList(problem.integerDomains));
      PCProblem newProb = new PCProblem(pc, domain, problem.lit2rvar);
      pcProblems.add(newProb);
    }
    return pcProblems;
  }

  private static Map<PCProblem, PCEstimate> buildPCEstimateMap(
      List<PCProblem> pcProblems, PartitionData<PCProblem> partitionData,
      Map<PartitionProblem, RankedPartitionEstimate> part2estimate) {
    Map<PCProblem,PCEstimate> pcprob2estimate = Maps.newLinkedHashMap();
    for (PCProblem problem : pcProblems) {
      Collection<PartitionProblem> partitions = partitionData.source2part
          .get(problem);
      List<RankedPartitionEstimate> partEstimates = Lists
          .newArrayListWithCapacity(partitions.size());
      for (PartitionProblem pp : partitions) {
        RankedPartitionEstimate ppEst = part2estimate.get(pp);
        partEstimates.add(ppEst);
      }
      PCEstimate estimate = new PCEstimate(ImmutableList.copyOf(partEstimates));
      pcprob2estimate.put(problem, estimate);
    }
    return pcprob2estimate;
  }

  private static <T extends Problem> void populatePartitionEstimateDeps(PartitionData<T> pdata,      
      final Map<PartitionProblem, RankedPartitionEstimate> part2estimate) {
    Map<PartitionProblem, Trie<PartitionProblem,RankedPartitionEstimate>> related = pdata.relatedPartitions;
    Set<PartitionProblem> partProblems = pdata.partitions;
    
    for (PartitionProblem p1 : partProblems) {
      RankedPartitionEstimate estimate = part2estimate.get(p1);
      Trie<PartitionProblem,RankedPartitionEstimate> trie = related.get(p1);
      //populate trie with the estimates
      TrieWalker<PartitionProblem, RankedPartitionEstimate,Void> walker 
        = new TrieWalker<PartitionProblem, RankedPartitionEstimate, Void>() {

        @Override
        public void doSomethingWithTheRoot(
            TrieNode<PartitionProblem, RankedPartitionEstimate> node) {
          PartitionProblem p2 = node.object;
          RankedPartitionEstimate p2Estimate = part2estimate.get(p2);
          node.setPayload(p2Estimate);
        }

        @Override
        public void doSomething(
            TrieNode<PartitionProblem, RankedPartitionEstimate> node) {
          PartitionProblem p2 = node.object;
          RankedPartitionEstimate p2Estimate = part2estimate.get(p2);
          node.setPayload(p2Estimate);
        }
      };
      walker.walk(trie);
      estimate.relatedEstimates = trie; 
    }
  }
  
  private static String[] processOptions(String[] args) {
    options.ignore_options_after_arg(true);
    if (args != null && args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      System.out.println(Arrays.toString(args));
    }

    //options.printOptions();
    return args;
  }
}
