package coral.counters.refactoring.estimate;

public class DumbRankedEstimate extends RankedPartitionEstimate {

  public DumbRankedEstimate(Estimate[] boxEstimates) {
    super(boxEstimates);
  }
  
  @Override
  public double updateRank() {
    this.rank = this.partitionEstimate.variance;
    return this.rank;
  }

}
