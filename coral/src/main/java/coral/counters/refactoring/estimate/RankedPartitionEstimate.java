package coral.counters.refactoring.estimate;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

import com.google.common.collect.Lists;

import coral.counters.refactoring.problem.PartitionProblem;
import coral.util.trees.Trie;
import coral.util.trees.TrieNode;
import coral.util.trees.TrieVisitor;
import coral.util.trees.TrieWalker;

/**
 * Class to store estimates for a partition. Not immutable - we want to update
 * the results during the estimation problem and there may be references to the
 * same object in multiple places (priority queue and a map).
 */

public class RankedPartitionEstimate {
  private static int COUNTER = 0;
  
  public double rank;
  public Estimate partitionEstimate;
  public final Estimate[] boxEstimates;
  public Trie<PartitionProblem, RankedPartitionEstimate> relatedEstimates;
  public final int id = COUNTER++;

  public RankedPartitionEstimate(Estimate[] boxEstimates) {
    this.boxEstimates = boxEstimates;
    this.partitionEstimate = Estimate.aggregateBoxResults(Arrays
        .asList(boxEstimates));
    this.rank = 0;
    relatedEstimates = null;
  }

  /**
   * Visitor for computing the updated rank (the derivative) for
   * a single partition. All PCs that contain this partition are
   * stored in a trie, where each node is a distinct partition.
   * This allow us to reuse partial derivative results when many
   * PCs share the same prefix.
   */

  public static class RankingVisitor extends
      TrieVisitor<PartitionProblem, RankedPartitionEstimate> {

    double rank = 0;
    double partial = 0;

    public void visit(Trie<PartitionProblem, RankedPartitionEstimate> trie) {
      TrieNode<PartitionProblem, RankedPartitionEstimate> node = trie.root;
      Estimate estimate = node.payload.partitionEstimate;
      partial = estimate.variance / estimate.nSamples;

      if (node.isLeaf()) {
        rank = partial;
        return;
      } else {
        for (TrieNode<PartitionProblem, RankedPartitionEstimate> child : node.children
            .values()) {
          child.accept(this);
        }
      }
    }

    @Override
    public void visitNode(
        TrieNode<PartitionProblem, RankedPartitionEstimate> node) {
      Estimate estimate = node.payload.partitionEstimate;
      double oldPartial = partial;
      partial = partial * (Math.pow(estimate.estimate, 2) + estimate.variance);

      if (node.isLeaf()) {
        rank = rank + partial;
      } else {
        for (TrieNode<PartitionProblem, RankedPartitionEstimate> child : node.children
            .values()) {
          child.accept(this);
        }
      }
      partial = oldPartial;
    }
  }

  public double updateRank() {
    if (this.partitionEstimate.variance == 0) {
      this.rank = 0;
    } else {
      RankingVisitor visitor = new RankingVisitor();
      visitor.visit(relatedEstimates);
      this.rank = visitor.rank;
    }

    return this.rank;
  }
  
  public String toString() {
    return "["+id+"](" + partitionEstimate + ") - rank: " + rank;
  }

  public void updateBoxEstimates(Estimate[] newBoxEstimates) {
    for (int i = 0; i < newBoxEstimates.length; i++) {
      Estimate old = boxEstimates[i];
      Estimate cur = newBoxEstimates[i];
      if (cur.nSamples > 0) { // skip empty boxes
        boxEstimates[i] = old.merge(cur);
      }
    }
    this.partitionEstimate = Estimate.aggregateBoxResults(Arrays
        .asList(boxEstimates));
  }
}