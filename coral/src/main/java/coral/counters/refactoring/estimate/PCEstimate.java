package coral.counters.refactoring.estimate;

import java.util.List;

import com.google.common.collect.Lists;

public class PCEstimate {

  public Estimate pcEstimate;
  public final List<RankedPartitionEstimate> partitionEstimates;

  public PCEstimate(List<RankedPartitionEstimate> partitionEstimates) {
    this.partitionEstimates = partitionEstimates;
    List<Estimate> collected = collect(partitionEstimates);
    this.pcEstimate = Estimate.aggregatePartitionResults(collected);
  }

  public void update() {
    List<Estimate> collected = collect(partitionEstimates);
    this.pcEstimate = Estimate.aggregatePartitionResults(collected);
  }

  private List<Estimate> collect(
      List<RankedPartitionEstimate> partitionEstimates) {
    List<Estimate> collected = Lists
        .newArrayListWithCapacity(partitionEstimates.size());
    for (RankedPartitionEstimate rankedEstimate : partitionEstimates) {
      collected.add(rankedEstimate.partitionEstimate);
    }
    return collected;
  }

}
