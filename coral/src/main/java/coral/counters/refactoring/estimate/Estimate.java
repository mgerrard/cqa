package coral.counters.refactoring.estimate;

import java.util.Collection;

import coral.counters.MCUtil;

public class Estimate {

  public final double estimate; //AKA mean
  public final double variance;
  public final long nSamples;

  public Estimate(double estimate, double variance, long nSamples) {
    this.estimate = estimate;
    this.variance = variance;
    this.nSamples = nSamples;
  }
  
  /**
   * Compute the estimate for hit-or-miss mc 
   * @param probability
   * @param nSamples
   * @param hits
   */
  
  public Estimate (double probability, long nSamples, long hits) {
    this.nSamples = nSamples;

    if (nSamples == 0) {
      this.estimate = 0;
      this.variance = 0;
    } else {
      double avg = ((double) hits) / nSamples;
      double binomialVariance = (avg * (1 - avg) / nSamples);
      this.estimate = avg * probability;
      this.variance = binomialVariance * Math.pow(probability, 2);
    }
  }
  
  public static Estimate ZERO = new Estimate(0, 0, 0); 

  // assumption: box weight is already factored in each entry
  public static Estimate aggregateBoxResults(Collection<Estimate> boxEstimates) {
    double m1 = 0;
    double v1 = 0;
    long s1 = 0;

    for (Estimate estimate : boxEstimates) {
      double m2 = estimate.estimate;
      double v2 = estimate.variance;
      long s2 = estimate.nSamples;

      m1 = m1 + m2;
      v1 = v1 + v2;
      s1 = s1 + s2;
    }
    return new Estimate(m1, v1, s1);
  }

  public static Estimate aggregatePartitionResults(
      Collection<Estimate> results) {

    boolean first = true;
    double m1 = 1;
    double v1 = 1;
    long samples = 0;

    for (Estimate estimate : results) {
      if (first) {
        m1 = estimate.estimate;
        v1 = estimate.variance;
        samples = estimate.nSamples;
        first = false;
      } else {
        double m2 = estimate.estimate;;
        double v2 = estimate.variance;
        long s = estimate.nSamples;
        
        v1 = MCUtil.partitionVar(m1, m2, v1, v2);
        m1 = m1 * m2;
        samples = samples + s;
      }
    }
    
    return new Estimate(m1, v1, samples);
  }
  
  //formulas taken from the report sent by antonio 
  public Estimate merge(Estimate e) {
    long newNSamples = this.nSamples + e.nSamples;
    double newEstimate = ((this.estimate * this.nSamples) + (e.nSamples * e.estimate)) / newNSamples;
    double newVar;
    if (this.variance == 0 && e.variance == 0) {
      newVar = 0;
    } else {
      newVar = ((1 - newEstimate) * newEstimate) / newNSamples;
    }
    return new Estimate(newEstimate, newVar, newNSamples);
  }
  
  public String toString() {
    return "est: " + estimate + " var:" + variance + " ns:" + nSamples;
  }
}
