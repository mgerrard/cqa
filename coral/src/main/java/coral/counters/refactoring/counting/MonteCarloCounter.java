package coral.counters.refactoring.counting;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.math3.random.RandomGenerator;

import symlib.SymLiteral;
import symlib.eval.Elem;
import coral.PC;
import coral.counters.refactoring.CountingUtils;
import coral.counters.refactoring.CountingUtils.VarInfo;
import coral.counters.refactoring.estimate.Estimate;
import coral.counters.refactoring.problem.Domain;
import coral.counters.refactoring.problem.Problem;
import coral.counters.rvars.RandomVariable;
import symlib.eval.Evaluator;
import symlib.eval.ReversePolish;
import symlib.eval.ReversePolishEvaluator;

public class MonteCarloCounter implements Counter {

  RandomGenerator rng;
  long seed;

  public MonteCarloCounter(RandomGenerator rng, long seed) {
    this.rng = rng;
    this.seed = seed;
  }

  @Override
  public void reset() {
    rng.setSeed(seed);
  }

  @Override
  public void reset(long seed) {
    this.seed = seed;
    reset();
  }

  @Override
  public Estimate count(Problem problem, long nsamples) {
    return count(problem, ReversePolishEvaluator.create(problem.getPC()), nsamples);
  }

  @Override
  public Estimate count(Problem problem, Evaluator evaluator, long nsamples) {
    Domain domain = problem.getDomain();
    PC pc = problem.getPC();
    Set<SymLiteral> vars = pc.getSortedVars();
    int nvars = vars.size();
    Estimate estimate;

    if (nvars == 0) {
      throw new RuntimeException(
          "No variables are present in the informed problem!");
    } else {
      estimate = simulateWithDistributions(pc, domain, problem.getVarMap(),
          evaluator, nsamples);
    }

    return estimate;
  }

  private Estimate simulateWithDistributions(PC pc,
                                             Domain domain,
                                             Map<SymLiteral, RandomVariable> lit2RVar,
                                             Evaluator evaluator,
                                             long nSamples) {
    Estimate result;
    int hits = 0;

    if (domain.volume.isZero()) {
      System.out.println("[monte carlo] 0-vol box - skipping simulation...");
      result = Estimate.ZERO;
    } else {
      SortedSet<SymLiteral> varSet = pc.getSortedVars();
      List<VarInfo> varsInfo = CountingUtils.truncateVars(domain, lit2RVar,
          varSet);

      // truncated variable probability
      double probability = 1;
      for (VarInfo info : varsInfo) {
        RandomVariable rvar = info.rvar;
        probability = probability
            * ((rvar.getCDFUpper() - rvar.getCDFLower()) / (info.cdfDomainHi - info.cdfDomainLo));
      }

      if (probability == 0.0 || Double.isNaN(probability)) {
        System.out
            .println("[monte carlo] 0-probability box - skipping simulation...");
        return Estimate.ZERO; // TODO check this with Antonio
      }

      Number[] args = new Number[varsInfo.size()];
      for (int i = 0; i < nSamples; i++) {
        int j = 0;
        for (VarInfo vInfo : varsInfo) {
          SymLiteral lit = vInfo.lit;
          RandomVariable rvar = vInfo.rvar;
          args[j] = rvar.sample();
          j++;
        }

        boolean isSAT = evaluator.isSAT(args);
        if (isSAT) {
          hits++;
        }
      }

      result = new Estimate(probability, nSamples, hits);
    }

    return result;
  }
}
