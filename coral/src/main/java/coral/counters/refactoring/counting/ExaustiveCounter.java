package coral.counters.refactoring.counting;

import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

import com.google.common.collect.ImmutableList;

import coral.PC;
import coral.counters.refactoring.CountingUtils;
import coral.counters.refactoring.CountingUtils.VarInfo;
import coral.counters.refactoring.estimate.Estimate;
import coral.counters.refactoring.problem.Domain;
import coral.counters.refactoring.problem.Problem;
import coral.counters.rvars.RandomVariable;
import coral.util.BigRational;
import coral.util.Interval;
import symlib.SymDouble;
import symlib.SymInt;
import symlib.SymLiteral;
import symlib.eval.Elem;
import symlib.eval.Evaluator;
import symlib.eval.ReversePolishEvaluator;

public class ExaustiveCounter implements Counter {

  enum TYPE {
    DOUBLE, INT
  };

  @Override
  public Estimate count(Problem problem, long nsamples) {
    Evaluator evaluator = ReversePolishEvaluator.create(problem.getPC());
    return count(problem,evaluator,nsamples);
  }

  @Override
  public Estimate count(Problem problem, Evaluator evaluator, long nsamples) {
    PC pc = problem.getPC();
    Domain domain = problem.getDomain();
    SortedSet<SymLiteral> varSet = pc.getSortedVars();

    if (domain.volume.isZero()) {
      return Estimate.ZERO;
    } else if (varSet.size() == 0) {
      throw new RuntimeException("No variables found!");
    }

    // truncated variable probability
    double probability = 1;
    List<VarInfo> varsInfo = CountingUtils.truncateVars(domain,
        problem.getVarMap(), varSet);
    for (VarInfo info : varsInfo) {
      RandomVariable rvar = info.rvar;
      probability = probability * ((rvar.getCDFUpper() - rvar.getCDFLower())
          / (info.cdfDomainHi - info.cdfDomainLo));
    }

    if (probability == 0.0 || Double.isNaN(probability)) {
      System.out
          .println("[monte carlo] 0-probability box - skipping simulation...");
      return Estimate.ZERO;
    }

    List<SymLiteral> lits = ImmutableList.<SymLiteral> builder()
        .addAll(pc.getSortedVars()).build();

    Number[] values = new Number[lits.size()];
    TYPE[] types = new TYPE[lits.size()];
    Interval[] ivs = new Interval[lits.size()];

    int i = 0;
    for (SymLiteral lit : lits) {
      ivs[i] = domain.idToInterval.get(i).iv;
      if (lit instanceof SymInt) {
        values[i] = ivs[i].intLo();
        types[i] = TYPE.INT;
      } else if (lit instanceof SymDouble) {
        values[i] = ivs[i].doubleLo();
        types[i] = TYPE.DOUBLE;
      } else {
        throw new RuntimeException(lit.getClass().toString());
      }
    }

    int k = 0;
    long hits = 0;
    long samples = 0;
    boolean first = true;

    while (true) {
      if (first) {
        first = false;
      } else {
        // test for ending condition
        TYPE current = types[k];
        Interval iv = ivs[k];

        boolean incrementK;

        // we use equality because all possible values will be sampled
        // *in order*. Given this assumption, >= or <= might hide an
        // error somewhere else

        switch (current) {
          case DOUBLE:
            incrementK = values[k].doubleValue() == iv.doubleHi();
            break;
          case INT:
            incrementK = values[k].intValue() == iv.intHi();
            break;
          default:
            throw new RuntimeException("Missing case: " + current);
        }

        if (incrementK) {
          k++;
          if (k + 1 > lits.size()) { // end of the backtracking
            break;
          } else {
            for (int j = 0; j < k; j++) { // reset previous values
              TYPE ptype = types[j];
              Interval piv = ivs[j];
              switch (ptype) {
                case DOUBLE:
                  values[j] = piv.doubleLo();
                  break;
                case INT:
                  values[j] = piv.intLo();
                  break;
                default:
                  throw new RuntimeException("Missing case: " + current);
              }
            }
          }
        }
        // increment
        switch (types[k]) {
          case DOUBLE:
            values[k] = Math.nextUp(values[k].doubleValue());
            break;
          case INT:
            values[k] = values[k].intValue() + 1;
            break;
          default:
            throw new RuntimeException("Missing case: " + current);
        }
      }

      // eval
      if (evaluator.isSAT(values)) {
        hits++;
      }
      samples++;
    }

    return new Estimate(new BigRational(hits, samples)
        .mul(BigRational.valueOf(probability)).doubleValue(), 0.0, samples);
  }

  @Override
  public void reset() {
  }

  @Override
  public void reset(long seed) {
  }
}