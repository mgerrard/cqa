package coral.counters.refactoring.counting;

import coral.counters.refactoring.estimate.Estimate;
import coral.counters.refactoring.problem.Problem;
import symlib.eval.Evaluator;

public interface Counter {

  public Estimate count(Problem problem, long nsamples);

  public Estimate count(Problem problem, Evaluator evaluator, long nsamples);

  public void reset();
    
  public void reset(long seed);
}
