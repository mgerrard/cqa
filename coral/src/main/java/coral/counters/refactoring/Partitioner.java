package coral.counters.refactoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;

import coral.PC;
import coral.counters.refactoring.estimate.RankedPartitionEstimate;
import coral.counters.refactoring.problem.Domain;
import coral.counters.refactoring.problem.PartitionProblem;
import coral.counters.refactoring.problem.Problem;
import coral.util.trees.Trie;
import coral.util.visitors.SymLiteralSearcher;
import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import symlib.SymBool;
import symlib.SymLiteral;

public class Partitioner {

  protected PartitionResult partitionResult;

  public static class PartitionResult {
    private ImmutableSet<Set<String>> independentVarsClusters;

    public PartitionResult(
        HashMultimap<String, SymBool> constraintsRelatedToAVar,
        ImmutableSet<Set<String>> independentVarsClusters) {
      this.independentVarsClusters = independentVarsClusters;
    }

    public ImmutableSet<Set<String>> getIndependentVarsClusters() {
      return independentVarsClusters;
    }
  }

  public static class PartitionData<T extends Problem> {
    /** Partitions **/
    public final Set<PartitionProblem> partitions;
    /** Partitions that appear in the same path condition */
    public final Map<PartitionProblem,Trie<PartitionProblem,RankedPartitionEstimate>> relatedPartitions;
    /** Source problem -> partitions*/
    public final Multimap<T,PartitionProblem> source2part;
    /** partition -> source problems */
    public final Multimap<PartitionProblem,T> part2source;
    
    
    public PartitionData(
        Map<PartitionProblem,Trie<PartitionProblem,RankedPartitionEstimate>> relatedPartitions,
        Multimap<T, PartitionProblem> source2part,
        Multimap<PartitionProblem, T> part2source) {
      this.relatedPartitions = relatedPartitions;
      this.source2part = source2part;
      this.part2source = part2source;
      this.partitions = ImmutableSet.copyOf(source2part.values());
    }
  }
  
  public <T extends Problem> PartitionData<T> partition(List<T> problems) {
    loadPartition(problems);
    Multimap<T,PartitionProblem> source2part = LinkedHashMultimap.create();
    Map<PartitionProblem,Trie<PartitionProblem,RankedPartitionEstimate>> related = Maps.newLinkedHashMap();

    //fill the maps
    for (T problem : problems) {
      List<PartitionProblem> partitions = decompose(problem);
      Collections.sort(partitions);
      
      for (PartitionProblem p1 : partitions) {
        source2part.put(problem, p1);
        
        Trie<PartitionProblem,RankedPartitionEstimate> trie = getOrDefault(p1,related);
        List<PartitionProblem> pcopy = Lists.newArrayList(partitions);
        pcopy.remove(p1);
        if (pcopy.size() > 0) {
          trie.insert(pcopy, null);
        }
      }
    }
    //partitions shouldn't change, so we will use immutable maps here
    ImmutableMultimap<T,PartitionProblem> frozenSourcemap = ImmutableMultimap.copyOf(source2part);
    return new PartitionData<T>(ImmutableMap.copyOf(related),
        frozenSourcemap, frozenSourcemap.inverse());
  }

  private <K,V> void freezeMultisets(
      Map<K, Multiset<V>> related) {
    for (Map.Entry<K, Multiset<V>> entry : related.entrySet()) {
      Multiset<V> set = entry.getValue(); 
      entry.setValue(ImmutableMultiset.copyOf(set));
    }
  }

  private Trie<PartitionProblem,RankedPartitionEstimate> getOrDefault(PartitionProblem key,
      Map<PartitionProblem, Trie<PartitionProblem, RankedPartitionEstimate>> related) {
    Trie<PartitionProblem,RankedPartitionEstimate> val = related.get(key);
    if (val == null) {
      val = new Trie<PartitionProblem, RankedPartitionEstimate>(key,null);
      related.put(key, val);
    }
    return val;
  }

  private void loadPartition(List<? extends Problem> problems) {
    List<SymBool> conjunctionOfAllThePCs = Lists.newLinkedList();
    for (Problem problem : problems) {
      conjunctionOfAllThePCs.addAll(problem.getPC().getConstraints());
    }
    this.partitionResult = partition(conjunctionOfAllThePCs.iterator());
  }

  private PartitionResult partition(Iterator<SymBool> conjuncts) {
    HashMultimap<String, SymBool> constraintsRelatedToAVar = HashMultimap
        .<String, SymBool> create();
    UndirectedSparseGraph<String, Integer> dependencyGraph = new UndirectedSparseGraph<String, Integer>();
    int edgeCounter = 0;
    while (conjuncts.hasNext()) {
      SymBool constraint = conjuncts.next();
      SymLiteralSearcher vis = new SymLiteralSearcher();
      constraint.accept(vis);
      ArrayList<String> vars = new ArrayList<String>();
      for (SymLiteral v : vis.getVars()) {
        vars.add(v.toString());
      }
      // ArrayList<String> vars = new
      // ArrayList<String>(constraint.getVarsSet());
      for (int i = 0; i < vars.size(); i++) {
        constraintsRelatedToAVar.put(vars.get(i), constraint);
        // Notice j=i to add self-dependency
        for (int j = i; j < vars.size(); j++) {
          dependencyGraph.addEdge(edgeCounter++, vars.get(i), vars.get(j));
        }
      }
    }
    WeakComponentClusterer<String, Integer> clusterer = new WeakComponentClusterer<String, Integer>();
    Set<Set<String>> clusters = clusterer.transform(dependencyGraph);
    ImmutableSet<Set<String>> independentVarsClusters = ImmutableSet
        .<Set<String>> copyOf(clusters);
    return new PartitionResult(constraintsRelatedToAVar,
        independentVarsClusters);
  }

  Map<Set<SymBool>,PartitionProblem> cache = Maps.newHashMap();
  
  private <T extends Problem> List<PartitionProblem> decompose(Problem problem) {
    List<PartitionProblem> allProblems = Lists.newArrayList();
    List<SymBool> conjunct = problem.getPC().getConstraints();

    for (Set<String> cluster : partitionResult.getIndependentVarsClusters()) {
      Set<SymBool> partitionClauses = extractPartition(conjunct, cluster);
      if (partitionClauses.isEmpty()) {
        // none of the vars in cluster are in the conjunct
        continue;
      } else {
        PartitionProblem partition = cache.get(partitionClauses);
        if (partition == null) {
          List<SymBool> partitionConjunct = Lists.newArrayList(partitionClauses);
          PC partitionPC = new PC(partitionConjunct);
          Domain filteredDomain = CountingUtils.filterDomain(problem.getDomain(), partitionPC.getSortedVars());
          partition = new PartitionProblem(partitionPC,
              filteredDomain, problem.getVarMap());
          cache.put(partitionClauses, partition);
        }
        allProblems.add(partition);
      }
    }
    return allProblems;
  }

  private Set<SymBool> extractPartition(List<SymBool> conjunct,
      Set<String> cluster) {
    Set<SymBool> problem = new LinkedHashSet<SymBool>();
    HashMultimap<String, SymBool> locallyRelatedConstraints = HashMultimap
        .<String, SymBool> create();
    for (SymBool symBool : conjunct) {
      SymLiteralSearcher vis = new SymLiteralSearcher();
      symBool.accept(vis);
      for (SymLiteral v : vis.getVars()) {
        locallyRelatedConstraints.put(v.toString(), symBool);
      }
    }

    for (String var : cluster) {
      problem.addAll(locallyRelatedConstraints.get(var));
    }
    problem = Sets.intersection(problem, Sets.newLinkedHashSet(conjunct)).immutableCopy();
    return problem;
  }
}
