package coral.counters.refactoring;

import com.google.common.collect.ImmutableList;
import java.util.List;

import com.google.common.collect.Lists;

import coral.PC;
import coral.counters.refactoring.problem.BoxedProblem;
import coral.counters.refactoring.problem.Domain;
import coral.counters.refactoring.problem.Problem;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;
import coral.util.callers.RealPaverCaller;
import java.util.stream.Collectors;

public class Paver {

  private IntervalSolverCaller isolver;
  
  public Paver() {
    isolver = new RealPaverCaller(true);
  }
  
  public <T extends Problem> List<BoxedProblem> paving(T problem) {
    List<BoxedProblem> boxedProblems = Lists.newArrayList();
    Domain domain = problem.getDomain();
    List<Interval> domainArray = domain.idToInterval.values().stream()
        .map(vd -> vd.iv)
        .collect(Collectors.toList());
    
    try {
      PC pc = problem.getPC();
      List<Interval[]> paving = isolver.getPaving(pc,domainArray.toArray(new Interval[]{}));
	    paving = Interval.filterIdenticalBoxes(paving);
      for (Interval[] box : paving) {
        //ImmutableLists don't like nulls. let's remove them
        List<Interval> filteredBoxes = Lists.newArrayList();
        for (int i = 0; i < box.length; i++) {
          if (box[i] != null) {
            filteredBoxes.add(box[i]);
          }
        }

        Domain boxDomain = domain.update(filteredBoxes);
        BoxedProblem boxedProblem = new BoxedProblem(boxDomain, problem);
        boxedProblems.add(boxedProblem);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Error while calling realpaver: " + e.getMessage());
    }
    
    return boxedProblems;
  }
}
