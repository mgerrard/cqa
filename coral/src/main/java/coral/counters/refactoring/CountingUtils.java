package coral.counters.refactoring;

import java.util.*;

import java.util.stream.Collectors;
import symlib.SymBool;
import symlib.SymLiteral;
import symlib.eval.Elem;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import coral.PC;
import coral.counters.refactoring.problem.Domain;
import coral.counters.rvars.RandomVariable;
import coral.util.Interval;

public class CountingUtils {

  public static class VarInfo {
    public final SymLiteral lit;
    public final RandomVariable rvar;
    public final Interval iv;
    public final double cdfDomainLo;
    public final double cdfDomainHi;

    public VarInfo(SymLiteral lit, RandomVariable rvar, Interval iv,
        double cdfDomainLo, double cdfDomainHi) {
      this.lit = lit;
      this.rvar = rvar;
      this.iv = iv;
      // FIXME check with antonio if we need to subtract 1 if the var is an
      // integer
      this.cdfDomainLo = cdfDomainLo;// rvar.getCDF(iv.lo);
      this.cdfDomainHi = cdfDomainHi;// rvar.getCDF(iv.hi);
    }
  }

  public static List<VarInfo> truncateVars(Domain box,
      Map<SymLiteral, RandomVariable> lit2RVar, SortedSet<SymLiteral> varSet) {
    List<VarInfo> varsInfo = new ArrayList<VarInfo>(varSet.size());

    for (SymLiteral lit : varSet) {
      Interval iv = box.idToInterval.get(lit.getId()).iv;
      RandomVariable rvar = lit2RVar.get(lit);
      double cdfLo = rvar.getCDFLower();
      double cdfHi = rvar.getCDFUpper();

      RandomVariable truncated = rvar.truncate(iv);

      VarInfo vInfo = new VarInfo(lit, truncated, iv, cdfLo, cdfHi);
      varsInfo.add(vInfo);
    }

    return varsInfo;
  }

  public static boolean evalRNP(List<Elem[]> l) {
    boolean result = true;
    for (Elem[] elem : l) {
      ReversePolish rpol = new ReversePolish(elem);
      if (rpol.eval().intValue() == 0) {
        result = false;
        break;
      }
    }
    return result;
  }

  public static List<Elem[]> prepareRPNState(PC pc) {
    /** reverse polish code **/
    List<Elem[]> l = new ArrayList<Elem[]>();
    for (SymBool bExp : pc.getConstraints()) {
      GenReversePolishExpression revPolExpr = new GenReversePolishExpression();
      revPolExpr.visitSymBool(bExp);
      l.add(revPolExpr.getElems());
    }
    return l;
  }

  /**
   *  Use Largest Remainder method to allocate samples
   * @param budget
   * @param votes
   * @return number of samples proportional to each vote
   */
  public static int[] computeNumSamplesProportionally(int budget, double[] votes) {
    int[] nsamples = new int[votes.length];
    double[] remainders = new double[votes.length];
    double sum = 0;
    int allocated = 0;

    for (double proportion : votes) {
      sum += proportion;
    }
    for (int i = 0; i < nsamples.length; i++) {
      double proportion = votes[i];
      double quota = budget * (proportion / sum);
      nsamples[i] = (int) Math.floor(quota);
      remainders[i] = quota - nsamples[i];
      allocated += nsamples[i];
    }

    if (allocated < budget) {
      // dumb n^2 loop, but n will always be < 10, so this shouldn't be an issue
      int toAllocate = budget - allocated;
      while (toAllocate > 0) {
        int index = 0;
        double biggest = 0;

        for (int i = 0; i < remainders.length; i++) {
          if (remainders[i] > biggest) {
            index = i;
            biggest = remainders[i];
          }
        }

        remainders[index] = 0;
        nsamples[index]++;
        toAllocate--;
      }
    }

    return nsamples;
  }
  
  public static Domain filterDomain(Domain domain, SortedSet<SymLiteral> varSet) {
    Set<Integer> ids = varSet.stream()
        .map(v -> v.getId())
        .collect(Collectors.toSet());
    return domain.filter(ids);
  }
}
