package coral.counters.refactoring.problem;

import java.util.Map;

import symlib.SymLiteral;
import coral.PC;
import coral.counters.rvars.RandomVariable;

public class PartitionProblem implements Problem, Comparable<PartitionProblem>{

  private final PC partition;
  private final Domain domain;
  private final Map<SymLiteral, RandomVariable> varMap;
  private final int hashCode;

  public PartitionProblem(PC partition, Domain domain,
      Map<SymLiteral, RandomVariable> varMap) {
    this.partition = partition;
    this.domain = domain;
    this.varMap = varMap;

    final int prime = 31;
    int result = 1;
    result = prime * result + partition.hashCode();
    result = prime * result + domain.hashCode();
    result = prime * result + varMap.hashCode();
    this.hashCode = result;
  }

  @Override
  public PC getPC() {
    return partition;
  }

  @Override
  public Domain getDomain() {
    return domain;
  }

  @Override
  public Map<SymLiteral, RandomVariable> getVarMap() {
    return varMap;
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PartitionProblem other = (PartitionProblem) obj;
    if (domain == null) {
      if (other.domain != null)
        return false;
    } else if (!domain.equals(other.domain))
      return false;
    if (partition == null) {
      if (other.partition != null)
        return false;
    } else if (!partition.equals(other.partition))
      return false;
    if (varMap == null) {
      if (other.varMap != null)
        return false;
    } else if (!varMap.equals(other.varMap))
      return false;
    return true;
  }

  @Override
  public int compareTo(PartitionProblem o) {
    return this.hashCode - o.hashCode;
  }

  public String toString() {
    return this.partition.toString() + " [" + this.domain.toString() + "]";
  }
}
