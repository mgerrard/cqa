package coral.counters.refactoring.problem;

import java.util.Map;

import symlib.SymLiteral;
import coral.PC;
import coral.counters.rvars.RandomVariable;

public interface Problem {
  public PC getPC();
  public Domain getDomain();
  public Map<SymLiteral,RandomVariable> getVarMap();
}
