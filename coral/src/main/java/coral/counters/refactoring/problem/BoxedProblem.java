package coral.counters.refactoring.problem;

import java.util.Map;

import symlib.SymLiteral;
import coral.PC;
import coral.counters.rvars.RandomVariable;

public class BoxedProblem implements Problem {

  private Domain box;
  private Problem source;
  private int hashCode;

  public BoxedProblem(Domain box, Problem source) {
    this.box = box;
    this.source = source;
    
    final int prime = 31;
    int result = 1;
    result = prime * result + ((box == null) ? 0 : box.hashCode());
    result = prime * result + ((source == null) ? 0 : source.hashCode());
    this.hashCode = result;
  }

  @Override
  public PC getPC() {
    return source.getPC();
  }

  @Override
  public Domain getDomain() {
    return box;
  }

  @Override
  public Map<SymLiteral, RandomVariable> getVarMap() {
    return source.getVarMap();
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BoxedProblem other = (BoxedProblem) obj;
    if (box == null) {
      if (other.box != null)
        return false;
    } else if (!box.equals(other.box))
      return false;
    if (source == null) {
      if (other.source != null)
        return false;
    } else if (!source.equals(other.source))
      return false;
    return true;
  }

}
