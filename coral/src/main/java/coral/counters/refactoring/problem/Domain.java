package coral.counters.refactoring.problem;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import coral.util.BigRational;
import coral.util.Interval;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import symlib.SymLiteral;

public class Domain {

  public final ImmutableSortedMap<Integer, VarData> idToInterval;
  public final BigRational volume;

  public Domain(Collection<SymLiteral> lits, Collection<Interval> ivs,
      Collection<Boolean> isIntegerDomain) {
    Preconditions.checkArgument(ivs.size() == isIntegerDomain.size());
    Preconditions.checkArgument(lits.size() == isIntegerDomain.size());
    //Stream.concat()

    Map<Integer, VarData> tmp = new HashMap<>();
    Iterator<SymLiteral> itLit = lits.iterator();
    Iterator<Interval> itIv = ivs.iterator();
    Iterator<Boolean> itInt = isIntegerDomain.iterator();

    while (itLit.hasNext()) {
      SymLiteral lit = itLit.next();
      VarData data = new VarData(itIv.next(), itInt.next());
      tmp.put(lit.getId(), data);
    }

    this.idToInterval = ImmutableSortedMap.copyOf(tmp);
    this.volume = Interval.computeVolume(ivs, isIntegerDomain);
  }

  public Domain(Map<Integer,VarData> domainData) {
    this.idToInterval = ImmutableSortedMap.copyOf(domainData);
    List<Interval> ivs = new ArrayList<>();
    List<Boolean> ints = new ArrayList<>();

    for (VarData d : idToInterval.values()) {
      ivs.add(d.iv);
      ints.add(d.isInt);
    }
    this.volume = Interval.computeVolume(ivs, ints);
  }

  public Domain filter(Collection<Integer> varIds) {
    Map<Integer,VarData> tmp = new HashMap<>();
    for (Integer id : varIds) {
      tmp.put(id,this.idToInterval.get(id));
    }
    return new Domain(tmp);
  }

  public Domain update(Collection<Interval> newIvs) {
    Map<Integer,VarData> tmp = new HashMap<>();
    Iterator<Interval> itNewIvs = newIvs.iterator();
    idToInterval.entrySet().stream()
        .forEach(e -> {
          Interval newIv = itNewIvs.next();
          VarData old = e.getValue();
          tmp.put(e.getKey(), new VarData(newIv,old.isInt));
        });
    return new Domain(tmp);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Domain domain = (Domain) o;

    if (!idToInterval.equals(domain.idToInterval)) {
      return false;
    }
    return volume.equals(domain.volume);
  }

  @Override
  public int hashCode() {
    int result = idToInterval.hashCode();
    result = 31 * result + volume.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Domain{" +
        "idToInterval=" + idToInterval +
        ", volume=" + volume +
        '}';
  }

  public static class VarData {

    public final Interval iv;
    public final boolean isInt;

    public VarData(Interval iv, boolean isInt) {
      this.iv = iv;
      this.isInt = isInt;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }

      VarData varData = (VarData) o;

      if (isInt != varData.isInt) {
        return false;
      }
      return iv != null ? iv.equals(varData.iv) : varData.iv == null;
    }

    @Override
    public int hashCode() {
      int result = iv != null ? iv.hashCode() : 0;
      result = 31 * result + (isInt ? 1 : 0);
      return result;
    }

    @Override
    public String toString() {
      return "VarData{" +
          "iv=" + iv +
          ", isInt=" + isInt +
          '}';
    }
  }
}
