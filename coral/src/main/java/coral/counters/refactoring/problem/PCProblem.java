package coral.counters.refactoring.problem;

import java.util.Map;

import symlib.SymLiteral;
import coral.PC;
import coral.counters.rvars.RandomVariable;

public class PCProblem implements Problem {

  private final PC pc;
  private final Domain domain;
  private final Map<SymLiteral, RandomVariable> varMap;
  private final int hashCode;
  
  public PCProblem(PC pc, Domain domain, Map<SymLiteral, RandomVariable> varMap) {
    this.pc = pc;
    this.domain = domain;
    this.varMap = varMap;
    
    final int prime = 31;
    int result = 1;
    result = prime * result + ((domain == null) ? 0 : domain.hashCode());
    result = prime * result + ((pc == null) ? 0 : pc.hashCode());
    result = prime * result + ((varMap == null) ? 0 : varMap.hashCode());
    
    this.hashCode = result; 
  }

  @Override
  public PC getPC() {
    return pc;
  }

  @Override
  public Domain getDomain() {
    return domain;
  }

  @Override
  public Map<SymLiteral, RandomVariable> getVarMap() {
    return varMap;
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PCProblem other = (PCProblem) obj;
    if (domain == null) {
      if (other.domain != null)
        return false;
    } else if (!domain.equals(other.domain))
      return false;
    if (pc == null) {
      if (other.pc != null)
        return false;
    } else if (!pc.equals(other.pc))
      return false;
    if (varMap == null) {
      if (other.varMap != null)
        return false;
    } else if (!varMap.equals(other.varMap))
      return false;
    return true;
  }
}
