package coral.counters;

import java.util.List;
import java.util.Map;
import java.util.Set;

import symlib.SymIntLiteral;
import symlib.SymLiteral;
import symlib.SymLongLiteral;
import coral.PC;
import coral.counters.rvars.RandomVariable;
import coral.util.BigRational;
import coral.util.Interval;

public class CountingProblem {

  public final PC pc;
  public final List<Interval[]> boxes;
  public final Map<SymLiteral,RandomVariable> lit2rvar;
  public final Interval[] domain;
  public final Boolean[] integerDomains;
  public final BigRational domainVolume;

  public CountingProblem(PC pc, List<Interval[]> boxes, 
      Map<SymLiteral, RandomVariable> lit2rvar, Interval[] domain) {
    this.pc = pc;
    this.boxes = boxes;
    this.lit2rvar = lit2rvar;
    this.domain = domain;
    
    Set<SymLiteral> sortedVars = pc.getSortedVars();
    
    this.integerDomains = pc.getIntDomainArray();
    this.domainVolume = Interval.computeVolume(domain,integerDomains);
  }
  
  public RandomVariable[] getRVarsInPC() {
    Set<SymLiteral> lits = pc.getSortedVars();
    RandomVariable[] rvars = new RandomVariable[lits.size()];
    int i = 0;
    for (SymLiteral lit : lits) {
      rvars[i] = lit2rvar.get(lit);
      i++;
    }
    return rvars;
  }
}