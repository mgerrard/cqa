package coral.counters;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.util.Config;
import coral.util.Interval;
import coral.util.options.Options;

public class RegCoralMain {

  private static Options options = new Options(Main.class, Config.class);
  
  
  public static void main(String[] args) throws FileNotFoundException, ParseException {
    options.ignore_options_after_arg(true);
    if (args != null && args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      System.out.println(Arrays.toString(args));
    }
    
    options.printOptions();
  
    File input = new File(args[0]);
    Scanner s = new Scanner(input);
    
    long timeStart = System.nanoTime();
    List<PC> constraints = new ArrayList<PC>();
    List<Interval[]> domains = new ArrayList<Interval[]>();
    Interval defaultInterval = new Interval(Config.rangeLO, Config.rangeHI);
    
    //load constraints
    while (s.hasNextLine()) {
      String cons = s.nextLine();
      
      if (cons.trim().isEmpty()) {
        continue;
      }

      PC pc = (new Parser(cons)).parsePC();
      constraints.add(pc);
      
      Interval[] domain;

      if (Main.USE_DOMAINS_FILE) {
        String domainString = s.nextLine();
        domain = MCUtil.parseDomain(domainString);
      } else { //use default domains
        int nVars = pc.getVars().size();
        domain = new Interval[nVars];
        for (int i = 0; i < nVars; i++) {
          domain[i] = defaultInterval;
        }
      }
      
      domains.add(domain);
    }
    
    long middleTime = System.nanoTime();
    double middle = (middleTime - timeStart) / 1000000000.0;
    double[] result = Main.runRegCoral(constraints, domains);

    long endTime = System.nanoTime();
    double end = (endTime - middleTime) / 1000000000.0;
    System.out.println("------------------------------------------------------");
    System.out.printf("[regCoral] report for experiment %s: \n",args[0]);
    System.out.printf("[regCoral] total time spent loading constraints (s): %f\n",middle);
    System.out.printf("[regCoral] total time spent running simulations (s): %f\n",end);
    System.out.printf("[regCoral] result: %f, error: %f\n",result[0],result[1]);
    System.out.println("[regCoral] tabulated data (order: estimate, stdev (if multiple executions were done), stdev (square of estimated variance), time, nsamples, file");
    System.out.printf("[regCoral] full data: " + result[0] + " & " +result[1]+" & " +Math.sqrt(result[2])+" & %f & %d & %s\n", middle+end, Config.mcMaxSamples, args[0]);
    System.out.println("[regCoral-csvresults] file,estimate,stdev-multiple,stdev-varestimate,time,samples,n-exec");
    System.out.printf( "[regCoral-csvresults] %s,%f,%f,%f,%f,%d,%d\n",args[0],result[0],result[1],Math.sqrt(result[2]), middle+end,Config.mcMaxSamples,Main.mcNumExecutions);

    System.out.println("------------------------------------------------------");
    s.close();
  }
}
