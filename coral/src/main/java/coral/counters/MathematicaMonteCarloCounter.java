package coral.counters;

import java.util.ArrayList;
import java.util.List;

import coral.PC;
import coral.counters.estimates.EstimateResult;
import coral.counters.estimates.MathematicaEstimateResult;
import coral.util.Interval;
import coral.util.Config;
import coral.util.callers.MathematicaCaller;

public class MathematicaMonteCarloCounter implements ModelCounter {
  
  public final MathematicaCaller mc;
  public final boolean normalize; //divide the volume by the length of the domain 
  
  public MathematicaMonteCarloCounter() {
    mc = new MathematicaCaller();
    normalize = false;
  }
  
  public MathematicaMonteCarloCounter(boolean normalize) {
    mc = new MathematicaCaller();
    this.normalize = normalize;
  }

  @Override
  public EstimateResult count(PC pc, Interval[] domains, int budget) {
    List<Interval[]> box = new ArrayList<Interval[]>();
    box.add(domains);
    return count(pc,box,domains,budget);
  }

  @Override
  public EstimateResult count(PC pc, List<Interval[]> boxes, int budget) {
    double estimate = mc.integrateAdaptativeMonteCarlo(pc, boxes);
    if (coral.counters.Main.mcResetRngEachSimulation) {
      mc.reseed(Config.mcSeed);
    }
    return new MathematicaEstimateResult(0,estimate,0);
  }

  @Override
  public EstimateResult count(PC pc, List<Interval[]> boxes, Interval[] domains, int budget) {
    //we already have the boxes, so we just use the domains to normalize the result
    double tmp = mc.integrateAdaptativeMonteCarlo(pc, boxes);
    if (coral.counters.Main.mcResetRngEachSimulation) {
      mc.reseed(Config.mcSeed);
    }
    if (normalize) {
      for (Interval iv : domains) {
        tmp /= iv.getLength();
      }
    }
    return new MathematicaEstimateResult(0,tmp,0);
  }
  
  public void reset() {
    //do nothing, for now
  }

  @Override
  public void reset(long seed) {
    mc.reseed(seed);
  }

  @Override
  public EstimateResult count(CountingProblem cp, int budget) {
    double result = mc.probability(cp, coral.counters.Main.mcUseNProbability);
    return new MathematicaEstimateResult(0,result,0);
  }
}
