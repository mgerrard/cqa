package coral.counters;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ResultData {
  
  private List<Entry> data;
  
  public List<Entry> getData() {
    return data;
  }

  public ResultData() {
    data = new ArrayList<ResultData.Entry>();
  }
  
  public void addEntry(Entry e) {
    data.add(e);
  }
  
  public static class Entry {
    public String name;
    public double analyticResult;
    public double estimateResult;
    public double variance;
    
    public int nBoxes;
    public double stdev;
    public double time;
    
    public int nOperations;
    public int nFunctions;
    public int nClauses;
    public Set<String> distinctOperations = new HashSet<String>();
    public Set<String> distinctFunctions = new HashSet<String>();
    
    public double lowerbound95;
    public double upperbound95;
    
    public String toCSV() {
      return String.format("%s,%f,%f,%d,%f,%f,%d,%d,%d,%d,%d,%f\n", name,analyticResult,estimateResult,nBoxes,stdev,time,nOperations,nFunctions,nClauses,distinctOperations.size(),distinctFunctions.size(),variance);
    }
    
    public String toAggregateCSV() {
      return String.format("%s,%f,%f,%f,%f,%d,%f,%d,%d,%d,%d,%d,%f\n", name,analyticResult,estimateResult,lowerbound95,upperbound95,nBoxes,time,nOperations,nFunctions,nClauses,distinctOperations.size(),distinctFunctions.size(),variance);
    }
  }
  
  public ResultData fold(String entryName) {
    Entry entry = new Entry();
    entry.name = entryName;
    for (Entry e : data) {
      entry.analyticResult += e.analyticResult;
      entry.estimateResult += e.estimateResult;
      entry.nBoxes += e.nBoxes;
      entry.nClauses += e.nClauses;
      entry.distinctFunctions.addAll(e.distinctFunctions);
      entry.distinctOperations.addAll(e.distinctOperations);
      entry.nFunctions += e.nFunctions;
      entry.nOperations += e.nOperations;
      entry.lowerbound95 += e.lowerbound95;
      entry.upperbound95 += e.upperbound95;
      entry.time += e.time;
    }
    entry.stdev = -1; //no sense in adding stdevs
    entry.variance = -1;
    
    ResultData newData = new ResultData();
    newData.addEntry(entry);
    return newData;
  }
  
  public String toCSV() {
    StringBuffer sb = new StringBuffer();
    sb.append("name,analyticResult,estimateResult,nBoxes,stdev,time,nOperations,nFunctions,nClauses,nDistinctOperations,nDistinctFunctions,variance\n");
    for (Entry e : data) {
      sb.append(e.toCSV());
    }
    
    return sb.toString();
  }
  
  public String toAggregateCSV() {
    StringBuffer sb = new StringBuffer();
    sb.append("name,analyticResult,estimateResult,lowerbound95,upperbound95,nBoxes,time,nOperations,nFunctions,nClauses,nDistinctOperations,nDistinctFunctions,variance\n");
    for (Entry e : data) {
      sb.append(e.toAggregateCSV());
    }
    
    return sb.toString();
  }
}
