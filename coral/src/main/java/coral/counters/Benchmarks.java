package coral.counters;

public class Benchmarks {
  
  //radius 1, volume=4.1887
  public final static String SPHERE = "DLE(ADD(POW_(DVAR(ID_1),DCONST(2.0)),ADD(POW_(DVAR(ID_2),DCONST(2.0)),POW_(DVAR(ID_3),DCONST(2.0)))),DCONST(1.0))"; 
  
  //taken from: http://www.csee.umbc.edu/~squire/reference/polyhedra.shtml#tetrahedron
  //edge = 1.6329932, volume=0.512682464
  public final static String TETRAHEDRON =
        "DGE(ADD(MUL(DCONST(-1.413573701),DVAR(ID_1)),ADD(MUL(DCONST(-2.449501487),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(DCONST(0),ADD(MUL(DCONST(3.003003003),DVAR(ID_3)),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1.413573701),DVAR(ID_1)),ADD(MUL(DCONST(2.449501487),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(2.83014862),DVAR(ID_1)),ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0))";

  //edge = 2, volume = 8
  public final static String CUBE = "DLE(DVAR(ID_1),DCONST(1));"
      +"DLE(MUL(DCONST(-1),DVAR(ID_1)),DCONST(1));"
      +"DLE(DVAR(ID_2),DCONST(1));"
      +"DLE(MUL(DCONST(-1),DVAR(ID_2)),DCONST(1));"
      +"DLE(DVAR(ID_3),DCONST(1));"
      +"DLE(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1))";

  //Taken from SAGE (sagemath.org) polytope list
  
  //edge = 1, volume = 2.18169499
  public final static String ICOSAHEDRON = "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(DVAR(ID_3),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DCONST(0),ADD(MUL(DCONST(2.618033989),DVAR(ID_3)),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(2.618033989),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(2.618033989),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DCONST(0),ADD(MUL(DCONST(2.618033989),DVAR(ID_3)),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DVAR(ID_2),ADD(DVAR(ID_3),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(2.618033989),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DCONST(0),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(2.618033989),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(DCONST(0),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DVAR(ID_2),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DCONST(0),ADD(MUL(DCONST(-2.618033989),DVAR(ID_3)),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(-2.618033989),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(-2.618033989),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DCONST(0),ADD(MUL(DCONST(-2.618033989),DVAR(ID_3)),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.309016994)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-2.618033989),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(DCONST(0),DCONST(2.118033989)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-2.618033989),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DCONST(0),DCONST(2.118033989)))),DCONST(0))";
  
  //volume = 43/3
  public final static String RHOMBICUBOCTAHEDRON = "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DCONST(0),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DCONST(0),ADD(DCONST(0),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DVAR(ID_2),ADD(DCONST(0),DCONST(3.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DCONST(0),ADD(DVAR(ID_3),DCONST(3.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DVAR(ID_2),ADD(DVAR(ID_3),DCONST(4.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DVAR(ID_2),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(3.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(3.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(DVAR(ID_2),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(DVAR(ID_2),ADD(DVAR(ID_3),DCONST(3.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DCONST(0),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(DVAR(ID_2),ADD(DCONST(0),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(DVAR(ID_1),ADD(DCONST(0),ADD(DCONST(0),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(DCONST(0),ADD(DVAR(ID_3),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(DCONST(0),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DCONST(0),ADD(DVAR(ID_3),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(DVAR(ID_3),DCONST(3.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(DVAR(ID_2),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DVAR(ID_3),DCONST(2.0)))),DCONST(0));"
      + "DGE(ADD(DCONST(0),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(DCONST(0),DCONST(1.0)))),DCONST(0));"
      + "DGE(ADD(MUL(DCONST(-1),DVAR(ID_1)),ADD(MUL(DCONST(-1),DVAR(ID_2)),ADD(MUL(DCONST(-1),DVAR(ID_3)),DCONST(1.0)))),DCONST(0))";

  //taken from: http://mathworld.wolfram.com/Sphere-SphereIntersection.html
  //Volume of the intersection of two spheres with radius 3 and distance between spheres equal to 2.0837781
  //result = 56.5485
  
  public final static String SPHERE_SPHERE_INTERSECTION =
       "DLE(ADD(POW_(DVAR(ID_1),DCONST(2)),ADD(POW_(DVAR(ID_2),DCONST(2)),POW_(DVAR(ID_3),DCONST(2)))),DCONST(9));"
      +"DLE(ADD(POW_(SUB(DVAR(ID_1),DCONST(2.0837781)),DCONST(2)),ADD(POW_(DVAR(ID_2),DCONST(2)),POW_(DVAR(ID_3),DCONST(2)))),DCONST(9))"
           ;
  
  //Taken from: http://math.stackexchange.com/questions/329170/volume-of-intersection-between-a-cone-and-cylinder
  //Intersection between cone and cylinder.
  //result = 2π−32/9 ~= 2.7276 
  
  public final static String CONE_CYLINDER =
       "DLE(ADD(SQRT_(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2)))),DVAR(ID_3)),DCONST(2));"
      +"DLE(ADD(POW_(SUB(DVAR(ID_1),DCONST(1)),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),DCONST(1));"
      +"DGE(DVAR(ID_3),DCONST(0))"
      ;
  
  //solids of revolution. taken from: http://mathworld.wolfram.com/SolidofRevolution.html

  //x^2+y^2<=(a^2 (h-z)^2)/h^2 and 0<=z<=h
  //h = 1, r = 1, oriented along the z-axis, base at z=0. Volume = pi/3
  public final static String CONE = 
      "DLE(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),POW_(SUB(DCONST(1),DVAR(ID_3)),DCONST(2)));" +
      "DLE(DVAR(ID_3),DCONST(1));" + 
      "DGE(DVAR(ID_3),DCONST(0))"
      ;
  
  //x^2+y^2<=(a (h-z)+z b)^2/h^2 and 0<=z<=h
  //h = 1, a = 1, b = 0.5... volume = 1.8326
  public final static String CONICAL_FRUSTRUM =
      "DLE(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),POW_(ADD(SUB(DCONST(1),DVAR(ID_3)),MUL(DCONST(0.5),DVAR(ID_3))),DCONST(2)));" +
      "DLE(DVAR(ID_3),DCONST(1));" + 
      "DGE(DVAR(ID_3),DCONST(0))";
  
  //x^2+y^2<=a^2 and -h/2<=z<=h/2
  //h = 1, a = 1 volume = 3.1415
  public final static String CYLINDER = 
      "DLE(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),DCONST(1.0));" +
      "DLE(DVAR(ID_3),DCONST(0.5));" + 
      "DGE(DVAR(ID_3),DCONST(-0.5))";
  
  //(x^2+y^2)/a^2+z^2/c^2 <= 1
  //a = 2, c = 1. volume = 16.755160819 
  public final static String OBLATE_SPHEROID =
      "DLE(ADD(DIV(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),DCONST(4)),POW_(DVAR(ID_3),DCONST(2))),DCONST(1.0))"
      ;
  
  //(c-sqrt(x^2+y^2))^2+z^2<=a^2
  //a = 1/4, c=1. volume = 1.23370055
  public final static String TORUS =
      "DLE(ADD(POW_(SUB(DCONST(1),SQRT_(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))))),DCONST(2)),POW_(DVAR(ID_3),DCONST(2))),DCONST(0.0625))"
      ;
      
  
  //x^2+y^2+z^2<=(((a-b)^2+h^2) ((a+b)^2+h^2))/(4 h^2) and 
  //sqrt((((a-b)^2+h^2) ((a+b)^2+h^2))/(4 h^2)-max(a, b)^2)<=z and
  //z <=sqrt((((a-b)^2+h^2) ((a+b)^2+h^2))/(4 h^2)-max(a, b)^2)+h
  //(assuming base radii a, b and height h)
  //a = 4, b = 3, h = 2.645. volume = 113.557882056
  public final static String SPHERICAL_SEGMENT = 
      "DLE(ADD(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),POW_(DVAR(ID_3),DCONST(2))),DCONST(16));" +
      "DLE(DCONST(0.000751),DVAR(ID_3));" +
      "DLE(DVAR(ID_3),DCONST(2.645751))"
      ;
  
  //FIXME: there is something strange with those formulas. computing the volume with wolfram alpha for these parameters result in negative volume :S
  //From: http://mathworld.wolfram.com/TorisphericalDome.html
  // x² + y² + z² < R² for sqrt(x² + y²) < r
  // (c - sqrt(x² + y²))² < a² - (z - (R - h))² for r < sqrt(x² + y²) < a + c 
  // z >= R - h
  //a = 1, c = 2, R = 5, r=2.5 h = 1.53589, volume = 28.957?
//  public final static String TORISPHERICAL_DOME = 
//      "BOR(BAND(" +
//          "DLE(ADD(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))),POW_(DVAR(ID_3),DCONST(2))),DCONST(25))," + 
//          "DLE(SQRT_(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2)))),DCONST(2.5))" +
//      "),BAND(" +
//          "DLE(POW_(SUB(DCONST(2),SQRT_(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2))))),DCONST(2)),SUB(DCONST(1),POW_(SUB(DVAR(ID_3),SUB(DCONST(5),DCONST(1.53589))),DCONST(2))))," +
//          "BAND(" +
//            "DLE(DCONST(2.5),SQRT_(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2)))))," +
//            "BAND(" +
//              "DLE(SQRT_(ADD(POW_(DVAR(ID_1),DCONST(2)),POW_(DVAR(ID_2),DCONST(2)))),DCONST(3))," +
//              "DLE(DVAR(ID_3),DCONST(3.46411))" +
//            ")" +
//          ")" +
//      "))"
      ;
      
  // |NAME|PC|VOL|
  public final static String[][] CONVEX_POLYHEDRA = new String[][] {
    new String[] {"Sphere", SPHERE, "4.1887","[{-10,10},{-10,10},{-10,10}]" },
    new String[] {"Tetrahedron", TETRAHEDRON, "0.512682464","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cube", CUBE, "8","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Icosahedron", ICOSAHEDRON, "2.18169499","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Rhombicuboctahedron",RHOMBICUBOCTAHEDRON,"14.333333333","[{-10,10},{-10,10},{-10,10}]"},
  };
  
  public final static String[][] SOLIDS_OF_REVOLUTION = new String[][] {
    new String[] {"Sphere intersection", SPHERE_SPHERE_INTERSECTION, "56.5485","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cone-cylinder intersection", CONE_CYLINDER, "2.7276","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cone", CONE, "1.047197551","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Conical frustrum", CONICAL_FRUSTRUM, "1.8326","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cylinder", CYLINDER, "3.1415","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Oblate spheroid", OBLATE_SPHEROID, "16.755160819","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Torus", TORUS, "1.23370055","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Spherical segment", SPHERICAL_SEGMENT, "113.557882056","[{-10,10},{-10,10},{-10,10}]"}
  };
  
  public final static String[][] EVERYTHING = new String[][] {
    new String[] {"Sphere", SPHERE, "4.1887","[{-10,10},{-10,10},{-10,10}]" },
    new String[] {"Tetrahedron", TETRAHEDRON, "0.512682464","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cube", CUBE, "8","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Icosahedron", ICOSAHEDRON, "2.18169499","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Rhombicuboctahedron",RHOMBICUBOCTAHEDRON,"14.333333333","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Sphere intersection", SPHERE_SPHERE_INTERSECTION, "56.5485","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cone-cylinder intersection", CONE_CYLINDER, "2.7276","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cone", CONE, "1.047197551","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Conical frustrum", CONICAL_FRUSTRUM, "1.8326","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Cylinder", CYLINDER, "3.1415","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Oblate spheroid", OBLATE_SPHEROID, "16.755160819","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Torus", TORUS, "1.23370055","[{-10,10},{-10,10},{-10,10}]"},
    new String[] {"Spherical segment", SPHERICAL_SEGMENT, "113.557882056","[{-10,10},{-10,10},{-10,10}]"}
  };
}
