package coral.counters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.GeometricDistribution;
import org.apache.commons.math3.distribution.IntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

import symlib.FrozenSymBool;
import symlib.SymBool;
import symlib.SymDouble;
import symlib.SymDoubleConstant;
import symlib.SymDoubleRelational;
import symlib.SymLiteral;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.counters.CachedCounter.ImprovementResult;
import coral.counters.MCUtil.DiscreteRegion;
import coral.counters.MCUtil.ORComposer;
import coral.counters.estimates.EstimateResult;
import coral.counters.estimates.PCEstimateResult;
import coral.counters.rvars.IntegerRandomVariable;
import coral.counters.rvars.RandomVariable;
import coral.counters.rvars.RealRandomVariable;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;
import coral.util.options.Options;

public class DistributionAwareQCoral {

  /**
   * Input file format: 
   * """"""""""""""""""""""""" 
   * :Variables: 
   * 1 $vartype $lo $hi $parameters... 
   * 2 $vartype $lo $hi $parameters... 
   * ...
   * 
   * ;; This is a comment 
   * :Constraints:
   * DEQ(DVAR(V_1),DVAR(V_2));... 
   * 
   * ;; you can also define a domain for a single constraint 
   * ;; in this case, the domain of V1 is [-1,1], and V_2 is [-2,2]
   * [{-1,1},{-2,2}]#DEQ(DVAR(V_2),DVAR(V_1))
   * ...
   * 
   * """""""""""""""""""""""""
   */

  private static Options options = new Options(Main.class, Config.class);

  public static void main(String[] args) throws ParseException, IOException, InterruptedException {
    args = processOptions(args);
    RandomGenerator rng = new Well44497b(Config.mcSeed);

    File input = new File(args[0]);
    Scanner s = new Scanner(input);
    List<CountingProblem> problems = processInput(s,rng);
    
//    ResultData data = new ResultData();
//    for (CountingProblem p : problems) {
//      PC pc = p.pc;
//      Entry e = new Entry();
//      StatsVisitor.extractData(pc, e);
//      data.addEntry(e);
//    }
//    int size = data.getData().size();
//    ResultData newData = data.fold("bla");
//    Entry e = newData.getData().get(0);
//    System.out.println("ands : " + e.nClauses);
//    System.out.println("ops: " + e.nOperations);
//    System.out.println("functions: " + e.nFunctions);
//    System.out.println("size: " + size);
    

    long start = System.nanoTime();
    MCUtil.ORComposer composer;
    if (Config.mcIterativeImprovement) {
      composer = runIterativeAnalysis(rng, problems, start);
    } else {
      composer = runAnalysis(rng, problems);
    }
    
    long end = System.nanoTime();
    double[] results = composer.getResult();
    double time = (end - start) / 1000000000.0;
    
    if (Main.mcDiscretize || Main.mcDiscretizeWithMateusApproach) {
      System.out.println("[qCORAL] Time spent in discretization: " + discretizationCost);
      time = time - discretizationCost;
    }
    
    System.out.printf("mean=%e,variance=%e, stdev=%e, time=%f\n",results[0],results[1],Math.sqrt(results[1]),time);
    System.out.println("[regCoral-csvresults] filename,mean,ignore,sd,time,samples,ignore-again");
    System.out.printf("[regCoral-csvresults] %s,%e,0,%e,%f,%d,0",args[0],results[0],Math.sqrt(results[1]),time,Config.mcMaxSamples);
    
    s.close();
  }

  private static MCUtil.ORComposer runAnalysis(RandomGenerator rng,
      List<CountingProblem> problems) throws IOException, InterruptedException {
    ModelCounter counter = new MonteCarloCounter(rng,Config.mcSeed);
    if (Main.mcPartitionAndCache) {
      CachedCounter cc = new CachedCounter(counter, Main.mcCallIntSolPartition);
      cc.loadProblems(problems);
      counter = cc;
    } else if (Main.mcUseMathematica) {
      counter = new MathematicaMonteCarloCounter();
    } else if (Main.mcDiscretize && !Main.mcPartitionAndCache) {
      throw new RuntimeException("Discretization only works with partitioning/caching");
    }
    if (Main.mcDiscretizeWithMateusApproach && Main.mcDiscretize) {
      throw new RuntimeException("choose only one approach for discretization!");
    }
      
    MCUtil.ORComposer composer = new MCUtil.ORComposer(Config.mcMaxSamples);
    for (CountingProblem cp : problems) {
      if (Main.mcDiscretize) {
          runDiscretization(counter, composer, cp);
          System.err.println("[qcoral-partial] " + composer.meanEstimate);
      } else {
        if (Main.mcPartitionAndCache || Main.mcUseMathematica || Config.mcSkipPaving) {
          //do nothing
        } else { //call the intervalSolver
          IntervalSolverCaller isolver = coral.solvers.rand.Util.getIntervalSolver(Config.intervalSolver);
          List<Interval[]> boxes = isolver.getPaving(cp.pc, cp.boxes.get(0));
          if (boxes.size() == 0) {
            continue;
          } else {
            CountingProblem boxedCP = new CountingProblem(cp.pc, boxes, cp.lit2rvar, cp.domain);
            cp = boxedCP;
          }
        }
        EstimateResult estimate = counter.count(cp,(int) Config.mcMaxSamples);
        System.err.println("[qcoral-partial] " + estimate.getMean());
        composer.compose(estimate.getMean(), estimate.getVariance());
      }
    }
      
    return composer;
  }
  
  private static ORComposer runIterativeAnalysis(RandomGenerator rng,
      List<CountingProblem> problems, long start) {
    System.out.println("[qCORAL-iterative] Iterative algorithm was chosen. Partitioning, caching and interval solving are now enabled.");
    Config.cachePavingResults = true;
    Main.mcPartitionAndCache = true;
    Main.mcCallIntSolPartition = true;
    
    final long partitionBudget = Config.mcMaxSamples;
    final long initialSamplingBudget = Config.mcInitialPartitionBudget;
    final long incrementBudget = Config.mcSamplesPerIncrement;
    boolean proportionalBoxSamples = Config.mcProportionalBoxSampleAllocation;
    Config.mcProportionalBoxSampleAllocation = false;
    Config.storeVariance = proportionalBoxSamples;

    if (partitionBudget < initialSamplingBudget) {
      throw new RuntimeException("Initial round budget > execution budget!");
    }

    ModelCounter mc = new MonteCarloCounter(rng, Config.mcSeed);
    CachedCounter cc = new CachedCounter(mc, true);
    cc.loadProblems(problems);

    // Initial estimate
    double mean = 0;
    double variance = 0;
    EstimateResult[] estimates = new PCEstimateResult[problems.size()];
    for (int i = 0; i < problems.size(); i++) {
      // TODO make this work with "mixed" sources of estimates
      CountingProblem cp = problems.get(i);
      PCEstimateResult estimate = (PCEstimateResult) cc.count(cp, (int) initialSamplingBudget);
      estimates[i] = estimate;
      mean = mean + estimate.mean;
      variance = variance + estimate.variance;
    }

    // The maximum budget is equal to the number of evaluations using the
    // non-iterative algorithm. 
    // In other words, budget = n_distinct_partitions * partitionBudget
    final long distinctPartitions = cc.getCacheMisses(); 
    final long globalBudget = distinctPartitions * partitionBudget;
    long spentBudget = distinctPartitions * initialSamplingBudget;

    System.out.printf(
            "[qCORAL-iterative] initial run done. %d samples were spent. %d remaining; step=%d\n",
            spentBudget, globalBudget - spentBudget, incrementBudget);
    
    boolean possibleToRank = cc.prepareIterative();
    if (!possibleToRank) { //all partition ranks == 0
      //spend all the remaining samples uniformly
      System.err
          .println("Not possible to rank partitions. possible reasons include:\n"
              + "1. All variables are dependent on each other. \n"
              + "2. RealPaver returned very precise boxes. \n"
              + "The remainder of the samples will be spent uniformly.");
      cc.prepareUniform();
      Config.mcAllocateBudgetProportionally = true;
      List<ImprovementResult[]> improvements = improve((int) (globalBudget - spentBudget), cc);

      for (ImprovementResult[] partitionImprovement : improvements) {
        for (ImprovementResult improv : partitionImprovement) {
          int pcIndex = improv.pcIndex;
          EstimateResult newResult = improv.partitionResult;
          estimates[pcIndex] = newResult;
        }
      }
      
      mean = 0;
      variance = 0;
      spentBudget = globalBudget;
      for (EstimateResult estimate : estimates) {
        mean += estimate.getMean();
        variance += estimate.getVariance();
      }
      System.out.println("[qCORAL-iterative] samples=" + spentBudget
          + " mean=" + mean + " var=" + variance);

    } else { //default incremental behavior
      
      // enable proportional box sampling if needed
      Config.mcProportionalBoxSampleAllocation = proportionalBoxSamples;
      Config.storeVariance = false;

      boolean keepIterating = true;
      while (variance > Config.mcTargetVariance && globalBudget > spentBudget
          && keepIterating) {
        List<ImprovementResult[]> improvements;
        if (incrementBudget + spentBudget > globalBudget) { // don't overrun the
                                                            // budget
          improvements = improve((int) (globalBudget - spentBudget), cc);
          keepIterating = false; // avoid infinite loops
        } else {
          improvements = improve((int) incrementBudget, cc);
        }

        if (Config.mcNSamplesInRandomPartition > 0) {
          improvements.addAll(improveRandom(Config.mcNSamplesInRandomPartition, cc, rng));
        }
        
        for (ImprovementResult[] partitionImprovement : improvements) {
          boolean countedSamples = false;
          for (ImprovementResult improv : partitionImprovement) {
            // TODO refactor with the above
            int pcIndex = improv.pcIndex;
            EstimateResult newResult = improv.partitionResult;
            EstimateResult oldResult = estimates[pcIndex];
            estimates[pcIndex] = newResult;

            // update our statistics
            mean = mean - oldResult.getMean();
            variance = variance - oldResult.getVariance();

            mean = mean + newResult.getMean();
            variance = variance + newResult.getVariance();

            // changes in a partition can affect multiple pcs.
            // we don't want to add the number of samples multiple times
            if (!countedSamples) {
              countedSamples = true;
              spentBudget = spentBudget - oldResult.getNumberOfSamples();
              spentBudget = spentBudget + newResult.getNumberOfSamples();
            }
          }
        }
        long now = System.nanoTime();
        System.out.println("[qCORAL-iterative] samples=" + spentBudget
            + " mean=" + mean + " var=" + variance + " time=" + ((now - start) / 1000000000.0));
      }
    }
      
    ORComposer or = new ORComposer(spentBudget);
    or.compose(mean, variance);
    return or;
  }
  
  
  private static List<ImprovementResult[]>  improveRandom(
      int budget, CachedCounter cc, RandomGenerator rng) {
    ArrayList<ImprovementResult[]> arr = new ArrayList<CachedCounter.ImprovementResult[]>();
    arr.add(cc.improve(budget, rng));
    return arr; 
  }

  private static List<ImprovementResult[]> improve(int budget, CachedCounter cc) {
    if (Config.mcAllocateBudgetProportionally) {
      return cc.improveProportionally(budget);
    } else {
      ArrayList<ImprovementResult[]> arr = new ArrayList<CachedCounter.ImprovementResult[]>();
      arr.add(cc.improve(budget));
      return arr;
    }
  }

  public static double discretizationCost = 0;
  private static void runDiscretization(ModelCounter counter,
      MCUtil.ORComposer composer, CountingProblem cp) {
    int nregions = Config.mcDiscretizationRegions;
    long start = System.nanoTime();
    PC pc = cp.pc;
    RandomVariable[] rvars = cp.getRVarsInPC();
    List<SymLiteral> vars = new ArrayList<SymLiteral>(pc.getSortedVars());
    Iterator<DiscreteRegion[]> regionIt = MCUtil.getAllRegions(cp.domain,rvars,nregions);
    long end = System.nanoTime();
    discretizationCost += (end - start) / 1000000000.0;
    
    while (regionIt.hasNext()) {
      start = System.nanoTime();
      List<SymBool> copyConstraints = new ArrayList<SymBool>(pc.getConstraints());
      PC copyPC = new PC(copyConstraints);
      DiscreteRegion[] region = regionIt.next();
      double probability = 1;
      //use the region as the new domain 
      Interval[] newDomain = new Interval[region.length];
      
      //copyPC = pc && varRegion
      for (int i = 0; i < region.length; i++) {
        DiscreteRegion varRegion = region[i];
        SymLiteral var = vars.get(i);
        SymBool lower = SymDoubleRelational.create((SymDouble) var, new SymDoubleConstant(
            varRegion.box.lo().doubleValue()), SymDoubleRelational.GE);
        SymBool upper = SymDoubleRelational.create((SymDouble) var, new SymDoubleConstant(
            varRegion.box.hi().doubleValue()), SymDoubleRelational.LE);
        copyPC.addConstraint(lower);
        copyPC.addConstraint(upper);
        
        newDomain[i] = varRegion.box;
        probability *= varRegion.probability;
      }
      ArrayList<Interval[]> tmp = new ArrayList<Interval[]>();
      tmp.add(newDomain);
      end = System.nanoTime();
      discretizationCost += (end - start) / 1000000000.0;
      CountingProblem regionCP = new CountingProblem(copyPC, tmp, cp.lit2rvar, newDomain);
      EstimateResult estimate = counter.count(regionCP, (int) Config.mcMaxSamples); 
      composer.compose(estimate.getMean() * probability, estimate.getVariance() * Math.pow(probability, 2));
    }
  }

  private enum Mode {
    VAR, CONS
  };

  public static List<CountingProblem> processInput(Scanner scanner, RandomGenerator rng) throws ParseException {
    Map<Integer, RandomVariable> id2rvar = new HashMap<Integer, RandomVariable>();
    Set<SymLiteral> allLits = new HashSet<SymLiteral>();
    UniformRealDistribution urd = new UniformRealDistribution(rng, 0, 1);
    List<ConstraintPlusDomain> cdata = new ArrayList<ConstraintPlusDomain>();
    
    Mode mode = Mode.VAR;
    String line;
    int i = 0;
    while (scanner.hasNextLine()) {
      i++;
      line = scanner.nextLine();
      if (line.isEmpty() || line.startsWith(";")) {
        continue;
      }

      if (line.startsWith(":")) {
        if (line.toUpperCase().contains("VARIABLES")) {
          mode = Mode.VAR;
        } else if (line.toUpperCase().contains("CONSTRAINTS")) {
          mode = Mode.CONS;
        }
        
      } else if (mode == Mode.VAR) {
        IntPlusRVar data = 
            parseVariable(line,rng,urd);
        int id = data.id;
        if (id2rvar.containsKey(id)) {
          throw new RuntimeException("Input Error (line " + i + "): Variable "
              + id + "already defined");
        } else {
          id2rvar.put(id, data.rvar);
        }
      } else if (mode == Mode.CONS) {
        ConstraintPlusDomain data = parseConstraint(line,id2rvar);
        cdata.add(data);
        allLits.addAll(data.constraint.getVars());
      }
    }
    
    Map<SymLiteral,RandomVariable> lit2rvar = new HashMap<SymLiteral, RandomVariable>();
    
    
    for (SymLiteral lit : allLits) {
      RandomVariable rvar = id2rvar.get(lit.getId());
      if (rvar == null) {
        throw new RuntimeException("Input Error (line ?): Variable " + lit + " was not declared.");
      } else {
        rvar = lit2rvar.put(lit, rvar);
      }
    }
    
    List<CountingProblem> problems = new ArrayList<CountingProblem>(cdata.size());
    for (i = 0; i < cdata.size(); i++) {
      ConstraintPlusDomain data = cdata.get(i);
      PC constraint = data.constraint;
      Interval[] domain = data.domain;
      List<Interval[]> tmp = new ArrayList<Interval[]>(1);
      tmp.add(domain);
      CountingProblem problem;
      
      if (data.ownDomain) { //create new variable map with specific domain for the constraint
        Map<SymLiteral,RandomVariable> ownLit2Rvar = new HashMap<SymLiteral, RandomVariable>();
        
        int j = 0;
        for (SymLiteral lit : constraint.getSortedVars()) {
          RandomVariable var = lit2rvar.get(lit);
          Interval ownInterval = domain[j]; 
          var = var.truncate(ownInterval);
          ownLit2Rvar.put(lit, var);
          j++;
        }
        problem = new CountingProblem(constraint, tmp, ownLit2Rvar, domain);
        
      } else {
        problem = new CountingProblem(constraint, tmp, lit2rvar, domain);
      }
      problems.add(problem);
    }

    return problems;
  }

  private static ConstraintPlusDomain parseConstraint(String line,
      Map<Integer, RandomVariable> id2rvar) throws ParseException {

    Interval[] domain;
    PC constraint;
    ConstraintPlusDomain data;
    
    if (line.contains("#")) {
      String[] toks = line.split("#");
      constraint = (new Parser(toks[1])).parsePC();
      domain = MCUtil.parseDomain(toks[0]);
      data = new ConstraintPlusDomain(domain, constraint, true); 
    } else {
      constraint = (new Parser(line)).parsePC();
      Set<SymLiteral> lits = constraint.getSortedVars();
      domain = new Interval[lits.size()];
      
      Iterator<SymLiteral> it = lits.iterator();
      for (int i = 0; i < lits.size(); i++) {
        SymLiteral lit = it.next();
        RandomVariable rv = id2rvar.get(lit.getId());
        domain[i] = rv.bounds;
      }
      //precompute hashes
      List<SymBool> clauses = constraint.getConstraints();
      for (int i = 0; i < constraint.size(); i++) {
        clauses.set(i, new FrozenSymBool(clauses.get(i)));
      }
      data = new ConstraintPlusDomain(domain, constraint, false);
    }
    return data;
  }

  //add more as needed
  private enum VarType {UNIFORM_INT,UNIFORM_REAL,NORMAL,BINOMIAL,POISSON,EXPONENTIAL,TSTUDENT,GEOMETRIC}
  
  private static IntPlusRVar parseVariable(String line, RandomGenerator rng, UniformRealDistribution urd) {
    String[] toks = line.trim().split(" +");
    int id = Integer.parseInt(toks[0]);
    VarType type = VarType.valueOf(toks[1]);
    double lo = Double.parseDouble(toks[2]);
    double hi = Double.parseDouble(toks[3]);
    Interval bounds = new Interval(lo, hi); 
    
    RandomVariable rvar;
    
    switch (type) {
    case BINOMIAL: {
      int ntrials = Integer.parseInt(toks[4]);
      double p = Double.parseDouble(toks[5]);
      IntegerDistribution rd = new BinomialDistribution(rng, ntrials, p);
      rvar = new IntegerRandomVariable(rd, bounds, urd);
      break;
    }
    case EXPONENTIAL: {
      double mean = Double.parseDouble(toks[4]);
      RealDistribution rd = new ExponentialDistribution(rng, mean);
      rvar = new RealRandomVariable(rd, bounds, urd);
      break;
    }
    case NORMAL: {
      double mean = Double.parseDouble(toks[4]);
      double stdev = Double.parseDouble(toks[5]);
      RealDistribution rd = new NormalDistribution(rng, mean, stdev);
      rvar = new RealRandomVariable(rd, bounds, urd);
      break;
    }
    case POISSON: {
      double lambda = Double.parseDouble(toks[4]);
      IntegerDistribution rd = new PoissonDistribution(rng, lambda,
          PoissonDistribution.DEFAULT_EPSILON,
          PoissonDistribution.DEFAULT_MAX_ITERATIONS);
      rvar = new IntegerRandomVariable(rd, bounds, urd);
      break;
    }
    case TSTUDENT: {
      throw new RuntimeException("Still need to finish t-student");
//      double mean = Double.parseDouble(toks[4]);
//      double stdev = Double.parseDouble(toks[5]);
//      RealDistribution rd = new TDistribution(rng, mean, stdev);
//      rvar = new RealRandomVariable(rd, bounds, urd);
//      break;
    }
    case GEOMETRIC: { 
      double p = Double.parseDouble(toks[4]);
      IntegerDistribution rd = new GeometricDistribution(rng,p);
      rvar = new IntegerRandomVariable(rd, bounds, urd);
      break;
    }
    case UNIFORM_INT: {
      IntegerDistribution rd = new UniformIntegerDistribution(rng,(int)lo, (int)hi);
      rvar = new IntegerRandomVariable(rd,bounds,urd);
      break;
    }
    case UNIFORM_REAL: {
      RealDistribution rd = new UniformRealDistribution(rng,lo,hi);
      rvar = new RealRandomVariable(rd,bounds,urd);
      break;
    }
    default:
      throw new RuntimeException("Need to implement support to this distribution!");
    }
    
    IntPlusRVar data = new IntPlusRVar(id,rvar);
    return data;
  }

  private static class ConstraintPlusDomain {
    Interval[] domain;
    PC constraint;
    boolean ownDomain; 
    public ConstraintPlusDomain(Interval[] domain, PC constraint, boolean ownDomain) {
      this.domain = domain;
      this.constraint = constraint;
      this.ownDomain = ownDomain;
    }
  }
  private static class IntPlusRVar {
    Integer id;
    RandomVariable rvar;
    public IntPlusRVar(Integer id, RandomVariable rvar) {
      this.id = id;
      this.rvar = rvar;
    }
  }

  public static String[] processOptions(String[] args) {
    options.ignore_options_after_arg(true);
    if (args != null && args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      System.out.println(Arrays.toString(args));
    }

    //options.printOptions();
    return args;
  }
}
