package coral.counters;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Arrays;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.counters.ResultData.Entry;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;
import coral.util.visitors.StatsVisitor;
import coral.util.options.Option;
import coral.util.options.Options;


public class DumpIntervals {
    private static Options options = new Options(Main.class, Config.class);
  public static void main(String[] args) throws ParseException, IOException, InterruptedException {
    
    //    Config.mcDefaultSamplesPerBox = 90000;
    options.ignore_options_after_arg(true);
    if (args != null && args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      //System.out.println(Arrays.toString(args));
    }
    
    //printOptions();
  
    File constraints = new File(args[0]);
    String benchmarkName = args[1];
    Scanner s = new Scanner(constraints);
    ResultData executionData = new ResultData();
    
    int i = 0;
    while (s.hasNextLine()) {
      i++;
      System.out.println("-----------------------------");
      String cons = s.nextLine();
      String domains = s.nextLine();
      long timeStart = System.nanoTime();
      PC pc = (new Parser(cons)).parsePC();

    IntervalSolverCaller isolver = coral.solvers.rand.Util.getIntervalSolver(Config.intervalSolver);
//  PC pc = new Parser(args[0]).parsePC().getCanonicalForm();
    Interval[] voldomains = MCUtil.parseDomain(domains);
    List<Interval[]> boxes = isolver.getPaving(pc,voldomains);
    System.out.println("volcomp: " + Arrays.toString(voldomains));
    System.out.println("realpaver: " + listToString(boxes));
	}
  }

	public static String listToString(List<Interval[]> list) {
		String result = "";
		for (int i = 0; i < list.size(); i++) {
			result += " +++ " + Arrays.toString(list.get(i));
		}
		return result;
	}
}
