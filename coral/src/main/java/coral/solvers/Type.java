package coral.solvers;

public class Type {

  private Unit unit;
  private int lo;
  private int hi;

  public Type(Unit unit, int lo, int hi) {
    super();
    this.unit = unit;
    this.lo = lo;
    this.hi = hi;
  }
  
  public int getLo() {
    return lo;
  }

  public int getHi() {
    return hi;
  }

  public Unit getUnit() {
    return unit;
  }
  
//  private boolean isRangeUndef = false;
//
//  public Type(Unit unit) {
//    super();
//    this.unit = unit;
//    isRangeUndef = true;
//  }
//
//  public boolean isRangeUndef() {
//    return isRangeUndef;
//  }
//  
//  public void setRangeUndef(boolean isRangeUndef) {
//    this.isRangeUndef = isRangeUndef;
//  }

  public void setHi(int newVal) {
    this.hi = newVal;
  }

  public void setLo(int newVal) {
    this.lo = newVal;    
  }
  
  public void setUnit(Unit unit) {
    this.unit = unit;
  }
  
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("{unit: " + unit);
    sb.append(", lo: " + lo);
    sb.append(", hi: " + hi);
//    sb.append(", isRangeUndef: " + isRangeUndef + "}");
    return sb.toString();
  }

}