package coral.solvers.rand;

import java.util.*;

import org.apache.commons.math.random.RandomDataImpl;

import symlib.SymIntLiteral;
import symlib.SymLiteral;

import coral.solvers.Type;
import coral.solvers.Unit;
import coral.util.Config;
import coral.util.Interval;
import coral.util.Range;
import coral.util.callers.IntervalSolverCaller;
import coral.util.callers.RealPaverCaller;
import coral.util.callers.RealPaverCaller.VarData;


public class Util {

	public static final double ONE_TWELVE_PI= (Math.PI)/12;
	public static final double ONE_NINE_PI= (Math.PI)/9;
  public static final double ONE_EIGHT_PI= (Math.PI)/8;
  public static final double ONE_SIX_PI= (Math.PI)/6;            
  public static final double ONE_FOURTH_PI= (Math.PI)/4;         
  public static final double ONE_THIRD_PI= (Math.PI)/3; 
  public static final double FIVE_TWELVE_PI= (Math.PI)*(5d/12d);
  public static final double ONE_HALF_PI= (Math.PI)/2;        
  public static final double SEVEN_TWELVE_PI= (Math.PI)*(7d/12d);
  public static final double TWO_THIRDS_PI= (Math.PI)*(2d/3d);     
  public static final double THREE_QUARTS_PI= (Math.PI)*(3d/4d); 
  public static final double ELEVEN_TWELVE_PI= (Math.PI)*(11d/12d);
  public static final double THIRTEEN_TWELVE_PI= (Math.PI)*(13d/12d);
  public static final double SEVENTEEN_TWELVE_PI= (Math.PI)*(17d/12d);
  public static final double NINETEEN_TWELVE_PI= (Math.PI)*(19d/12d);
  public static final double TWENTY_THREE_TWELVE_PI= (Math.PI)*(23d/12d);
  public static final double FIVE_SIXTS_PI= (Math.PI)*(5d/6d);   
  public static final double SEVEN_SIXTS_PI= (Math.PI)*(7d/6d);  
  public static final double FIVE_FOURTHS_PI= (Math.PI)*(5d/4d); 
  public static final double FOUR_THIRDS_PI= (Math.PI)*(4d/3d);  
  public static final double THREE_HALFS_PI= (Math.PI)*(3d/2d);  
  public static final double FIVE_THIRDS_PI= (Math.PI)*(5d/3d);  
  public static final double SEVEN_QUARTS_PI= (Math.PI)*(7d/4d); 
  public static final double ELEVEN_SIXTS_PI= (Math.PI)*(11d/6d);
	
  private static long TOTAL_TIME_UNSOLVED_CONSTRAINTS = 0;
  private static int N_UNSOLVED_CONSTRAINTS = 0;
  
  public static void reportUnsolvedConstraint(long time) {
    N_UNSOLVED_CONSTRAINTS++;
    TOTAL_TIME_UNSOLVED_CONSTRAINTS += time;
  }
  
  public static double reportAverageTimeOnUnsolvedConstraints() {
    double time;
    if(N_UNSOLVED_CONSTRAINTS > 0) {
      time = TOTAL_TIME_UNSOLVED_CONSTRAINTS / N_UNSOLVED_CONSTRAINTS;
    } else {
      time = 0.0;
    }
    return time;
  }
  
  
  // TOREVERT static
  public static float generateFloatValueRand(RandomDataImpl random, Range range) {
    int low = range.getLo() * 100;
    if (range.getLo() == Integer.MIN_VALUE) {
      low = Integer.MIN_VALUE;
    }

    int high = range.getHi() * 100;
    if (range.getHi() == Integer.MAX_VALUE) {
      high = Integer.MAX_VALUE;
    }

    int nextInt = random.nextInt(low, high);
    return (float) nextInt / 100;
  }

  // TOREVERT static
  public static int generateIntValueRand(RandomDataImpl random, Range range) {
    return random.nextInt(range.getLo(), range.getHi());
  }

  public static double[] getFractionsOfPI(boolean translateToDegrees) {
    double[] result = new double[]{
  	    0d,             // 0.00...
  	    ONE_TWELVE_PI,  // 0.26179938779914943653855361527329
  	    ONE_SIX_PI,     // 0.5235987755982988
  	    ONE_FOURTH_PI,  // 0.7853981633974483
  	    ONE_THIRD_PI,   // 1.0471975511965976
  	    ONE_HALF_PI,    // 1.5707963267948966
  	    TWO_THIRDS_PI,  // 2.0943951023931953
  	    THREE_QUARTS_PI,// 2.356194490192345 
  	    FIVE_SIXTS_PI,  // 2.6179938779914944
  	    Math.PI,        // 3.141592653589793
  	    SEVEN_SIXTS_PI, // 3.6651914291880923
  	    FIVE_FOURTHS_PI,// 3.9269908169872414
  	    FOUR_THIRDS_PI, // 4.1887902047863905  
  	    THREE_HALFS_PI, // 4.71238898038469
  	    FIVE_THIRDS_PI, // 5.235987755982989  
  	    SEVEN_QUARTS_PI,// 5.497787143782138
  	    ELEVEN_SIXTS_PI,// 5.759586531581287
  	    2*Math.PI       // 6.283185307179586
  	  };
      if (translateToDegrees) {
        result = toDegrees(result);
      }
  	  return result;
  }

  private static double[] toDegrees(double[] result) {
    double[] degrees = new double[result.length];
    for (int i = 0; i < degrees.length; i++) {
      degrees[i] = Math.toDegrees(degrees[i]);
    }
    return degrees;
  }

  public static double[] getFractionsOfPILarger(boolean translateToDegrees) {
    double[] result = new double[]{
        0d,                    // 0.00...
        ONE_TWELVE_PI,         // 0.26179938779914943653855361527329
        ONE_NINE_PI,	         // 0.34906585039886591538473815369772
        ONE_EIGHT_PI,	         // 0.39269908169872415480783042290994
        ONE_SIX_PI,            // 0.5235987755982988
        ONE_FOURTH_PI,         // 0.7853981633974483
        ONE_THIRD_PI,          // 1.0471975511965976
        FIVE_TWELVE_PI,        // 1.3089969389957471826927680763665
        ONE_HALF_PI,           // 1.5707963267948966
        SEVEN_TWELVE_PI,       // 1.832595714594046055769875306913
        TWO_THIRDS_PI,         // 2.0943951023931953
        THREE_QUARTS_PI,       // 2.356194490192345 
        FIVE_SIXTS_PI,         // 2.6179938779914944
        ELEVEN_TWELVE_PI,      // 2.8797932657906438019240897680062
        Math.PI,               // 3.141592653589793
        THIRTEEN_TWELVE_PI,    // 3.4033920413889426750011969985528
        SEVEN_SIXTS_PI,        // 3.6651914291880923
        FIVE_FOURTHS_PI,       // 3.9269908169872414
        FOUR_THIRDS_PI,        // 4.1887902047863905  
        SEVENTEEN_TWELVE_PI,   // 4.450589592585540421155411459646
        THREE_HALFS_PI,        // 4.71238898038469
        NINETEEN_TWELVE_PI,    // 4,9741883681838392942325186901925
        FIVE_THIRDS_PI,        // 5.235987755982989  
        SEVEN_QUARTS_PI,       // 5.497787143782138
        ELEVEN_SIXTS_PI,       // 5.759586531581287
        TWENTY_THREE_TWELVE_PI,// 6.0213859193804370403867331512857
        2*Math.PI              // 6.283185307179586
    };
    if (translateToDegrees) {
      result = toDegrees(result);
    }
    return result;
  }
  
  public static double[] generateInterval(double lo, double hi, int size) {
    double[] interval;
  	double range = hi - lo;
    interval = new double[size];    
    double INC = (double)(range/size); 
    int j = 0;
    for (double i = lo; i < hi & j < size; i += INC) {
      interval[j++] = i;
    }
    return interval;
  }
  
  
  static double prec = -1; 
  public static double round(double val) {
    if(Double.isNaN(val)) {
      return val; 
    } else {
      // Ex.  10.5832752837, integerPart = 10, decimalPart = .5832752837;
      long integerPart = (long)val;
      double decimalPart = val - integerPart;
      
      if (prec == -1) {
        prec = Math.pow(10, Config.fractionPrecision);  
      }
      
      double tmp1 = decimalPart * prec;
//      double tmp = Math.round(tmp1) / prec;

      double tmp = ((double) ((long) tmp1))/prec;
      
//      assert (tmp > 1);
      return ((double)integerPart)+tmp;
    } 
  }
  
  public static boolean isRadian(Type type) {
    boolean returnVal = false;
    Unit unit = type.getUnit();
    if(unit.compareTo(Unit.ATAN) <= 0) { 
      returnVal = true;
    }
    return returnVal;
  }
  
  public static List<VarData> extractVarData(Set<SymLiteral> vars, Map<Integer,Interval> idToBox) {
    List<VarData> data = new ArrayList<>(vars.size());
    int i = 0;
    
    for(SymLiteral lit : vars) {
      int id = lit.getId();
      boolean isInt = lit instanceof SymIntLiteral;
      Interval box = idToBox != null ? idToBox.get(id) : null;
      data.add(new RealPaverCaller.VarData(id, isInt, box));
      i++;
    }
    
    return data;
  }
  
  public static IntervalSolverCaller getIntervalSolver(String solverName) {
    if(solverName.equals("realpaver")) {
      return new RealPaverCaller();
    } else {
      throw new RuntimeException("Unknown interval solver: " + solverName);
    }
  }
}
