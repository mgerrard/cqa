package coral.solvers.rand;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.Util;
import symlib.eval.Elem;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.Type;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;
import coral.util.callers.RealPaverCaller;
import coral.util.visitors.SymLiteralTypeSearcher;

/**
 * @author Diego Cavalcanti
 * @author Rohit Gheyi
 * @author Marcelo d'Amorim
 * @author Mateus Borges
 * 
 */
public class RandomSolver extends Solver {

  private NumberGenerator numGenerator;

  public RandomSolver() {
    super(SolverKind.RANDOM);
    numGenerator = new NumberGenerator(Config.RANGE);
  }
  
  public Env call(PC pc, Guard guard, Map<Integer,Interval> intervalSolverResults) {
    return runSolver(pc,guard,intervalSolverResults);
  }
    
  public Env call(PC pc, Guard guard) {
    return runSolver(pc, guard, super.getBox());
  }

  private Env runSolver(PC pc, Guard guard,Map<Integer,Interval> intervalSolverResults) {

    /** reverse polish code **/
    List<Elem[]> l = new ArrayList<Elem[]>();
    for (SymBool bExp : pc.getConstraints()) {
      GenReversePolishExpression revPolExpr = new GenReversePolishExpression();
      revPolExpr.visitSymBool(bExp);
      l.add(revPolExpr.getElems());  
    }
    
    Env result = null;
    Map<SymLiteral, Type> var2Units = new SymLiteralTypeSearcher(pc,Config.flexibleRange).getUnits();
    Set<SymLiteral> varSet = pc.getSortedVars();
    varSet.addAll(var2Units.keySet());
    
    int i = 0;
    boolean firstIteration = true;

    while (!guard.shouldStop() && (i++) < Config.nIterationsRANDOM) {

      try {
        
        if(firstIteration) {
          // set all values to 0
          for (SymLiteral var: var2Units.keySet()) {
            var.setCte(0);
          }
          firstIteration = false;
          
        } else {
         
          gen(var2Units,intervalSolverResults);
          
        }

        /** reverse polish code **/
        boolean isSAT = true;
        for (Elem[] elem : l) {
          ReversePolish rpol = new ReversePolish(elem);
          if (rpol.eval().intValue()==0) {
            isSAT = false;
            break;
          }
        }
        
        if (isSAT) { /*SOLVED BRANCH*/
          result = createEnv(var2Units);
          break;
        } 
      } catch (ArithmeticException _) {
        continue; // arithmetic error.  optimistic solution.
      }
    }
    
    return result;
  }

  private Env createEnv(Map<SymLiteral, Type> var2Units) {
    Env result;
    result = new Env(null, Result.UNK, kind);
    result.setResult(Result.SAT);
    
    Map<SymLiteral, SymNumber> map = new HashMap<SymLiteral, SymNumber>();
    result.setMap(map);
    for (SymLiteral lit : var2Units.keySet()) {
      map.put(lit, Util.createConstant(lit.getCte()));
    }
    return result;
  }

  private void gen(Map<SymLiteral, Type> var2Units,Map<Integer,Interval> intervals) {
    for (Entry<SymLiteral, Type> entry : var2Units.entrySet()) {
      Number val;
      Type t = entry.getValue();
      switch (t.getUnit()) {
      case ASIN_ACOS: // for arc sin and arc co-sine
        val = numGenerator.genASinACos();
        break;
      case ATAN: // for arc tangent
        val = numGenerator.genAtan();
        break;
      case RADIANS:
        val = numGenerator.genRadian();
        break;
      case DEGREES:
        val = numGenerator.genDegree();
        break;
      case LIMITED_INT:
        val = /*t.isRangeUndef() ? numGenerator.genInt() :*/
          numGenerator.genInt(t.getLo(), t.getHi());
        break;
      case LIMITED_DOUBLE:
        if(Config.enableIntervalBasedSolver) {
          int litId = entry.getKey().getId();
          if(intervals.containsKey(litId)) {
            Interval interval = intervals.get(litId);
            val = /*t.isRangeUndef() ? numGenerator.genDouble() :*/
              numGenerator.genDouble(interval.lo().doubleValue(), interval.hi().doubleValue());
          } else {
            val = numGenerator.genDouble(t.getLo(), t.getHi()); 
          }
        } else {
          val = /*t.isRangeUndef() ? numGenerator.genDouble() :*/
              numGenerator.genDouble(t.getLo(), t.getHi());
        }
        break;
      case LIMITED_FLOAT:
        val = /*t.isRangeUndef() ? numGenerator.genFloat() :*/
          numGenerator.genFloat(t.getLo(), t.getHi());
        break;
      case DOUBLE: //FIXME: what is this for??
        val = numGenerator.genDouble(); 
        break;
      case LIMITED_BOOLEAN:
        val = numGenerator.genBoolean()?1:0;
        break;
      default:
        throw new UnsupportedOperationException();
      }
      entry.getKey().setCte(val);
    }    
  }

  @Override  
  protected Env recall(PC pc, Guard guard) {
    return call(pc, guard);
  }

  @Override
  protected Env call(PC optimizedPc, PC originalPc, Guard guard) {
    return call(optimizedPc,guard);
  }
} 