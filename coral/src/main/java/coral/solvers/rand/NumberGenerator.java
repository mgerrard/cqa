package coral.solvers.rand;

import java.util.Arrays;

import org.apache.commons.math.random.RandomDataImpl;

import coral.solvers.Type;
import coral.solvers.UnsatException;
import coral.util.Config;
import coral.util.Range;

/**
 * 
 * Generator of random numbers used by **all** 
 * randomized solvers
 * 
 * @author mab
 * @author damorim
 *
 */
public class NumberGenerator {

  /*** static fields ***/
  private static double[] asinacos;
  private static Range rangeAsinAcos;

  private static double[] atan;
  private static Range rangeAtan;

  private static double[] radians;
  private static Range rangeRadians;

  private static double[] degrees;
  private static Range rangeDegrees;


  /*** instance fields ***/
  private RandomDataImpl randomGen;
  private Range regularRange;

  public NumberGenerator(Range range) {
    super();
    randomGen = new RandomDataImpl();
    randomGen.reSeed(Config.seed);
    this.regularRange = range;

    if (radians == null) {
      int size = 1000;

      // sincos
      double lo = -1.0d, hi = 1.0d;
      asinacos = coral.solvers.rand.Util.generateInterval(lo, hi, size);
      asinacos[0] = 0.0; 
      rangeAsinAcos = new Range(0, asinacos.length-1);

      // atan
      lo = -5d; hi = 5d;
      atan = coral.solvers.rand.Util.generateInterval(lo, hi, size);
      atan[0] = 0.0;
      rangeAtan = new Range(0, atan.length-1);

      // radians
      if(Config.radianSize == Config.NORMAL)
        radians = coral.solvers.rand.Util.getFractionsOfPI(false);
      else if (Config.radianSize == Config.LARGE)
        radians = coral.solvers.rand.Util.getFractionsOfPILarger(false);
      else
        throw new RuntimeException("Error setting size of pi fractions");
      // Util.getFractionsOfPILarger(Unit.RADIANS);
      rangeRadians = new Range(0, radians.length-1);

      // degrees
      if(Config.degreeSize == Config.NORMAL)
        degrees = coral.solvers.rand.Util.getFractionsOfPI(true);
      else if (Config.degreeSize == Config.LARGE)
        degrees = coral.solvers.rand.Util.getFractionsOfPILarger(true);
      // Util.getFractionsOfPILarger(Unit.DEGREES);
      else 
        throw new RuntimeException("Error setting size of pi fractions");
      rangeDegrees = new Range(0, degrees.length-1);
    }
  }

  public boolean genBoolean() {
    return randomGen.nextInt(0, 1)==1;
  }

  public double genDouble() {
    return genDouble(regularRange.getLo(), regularRange.getHi());
  }
  
  public double genDouble(double lo, double hi) {
    double result;

    if (lo > hi) {
      throw new UnsatException();
    } else if (lo == hi) {
      result = lo;
    } else {
      result = randomGen.nextUniform(lo, hi);
    }
    
    return result;
  }

  public double genDouble(int lo, int hi) {
    double result;
    if (lo > hi) {
      throw new UnsatException();
    } else if (lo == hi) {
      result = lo;
    } else {
      result = coral.solvers.rand.Util.round(randomGen.nextUniform(lo, hi));
    }
    return result;    
  }
  
  public int genInt(int lo, int hi) {
    //FIXME: please, do as genDouble
    return randomGen.nextInt(lo, hi); 
  }

  public int genInt() {
    return genInt(regularRange.getLo(), regularRange.getHi());
  }
  
  public float genFloat() {
    //FIXME: please check this *100!!
    return genFloat(regularRange.getLo()*100, regularRange.getHi()*100);
  }

  public float genFloat(int lo, int hi) {
    // FIXME: please, do as genDouble 
    return randomGen.nextInt(lo, hi);
  }

  public double genDegree() {
    int v = coral.solvers.rand.Util.generateIntValueRand(randomGen, rangeDegrees);      
    return degrees[v];
  }

  public double genRadian() {
    int v = coral.solvers.rand.Util.generateIntValueRand(randomGen, rangeRadians);      
    return radians[v];
  }

  public double genASinACos() { 
    int v = coral.solvers.rand.Util.generateIntValueRand(randomGen, rangeAsinAcos);      
    return asinacos[v];
  }

  public double genAtan() {
    int v = coral.solvers.rand.Util.generateIntValueRand(randomGen, rangeAtan);      
    return atan[v];
  }


  /**
   * @param unit
   * @return lower bound of value range associated  
   * input unit 
   */
  public int getLowerBound(Type type) {
    int lo;
    switch(type.getUnit()){
    case RADIANS:
    case DEGREES:
    case ASIN_ACOS:
    case ATAN:
      lo = 0;
      break;
    default:
      lo = type.getLo();
      break;
    }
    return lo;    
  }

  /**
   * @param unit
   * @return upper bound of value range associated 
   * to the input unit 
   */
  public int getUpperBound(Type type) {
    int hi;
    switch(type.getUnit()){
    case RADIANS:
      hi = radians.length-1;
      break;
    case DEGREES:
      hi = degrees.length-1;
      break;
    case ASIN_ACOS:
      hi = asinacos.length-1;
      break;
    case ATAN:
      hi = atan.length-1;
      break;
    default:
      hi = type.getHi();
      break;
    }
    return hi;
  }

  public Number getRadian(int pos) {
    return radians[pos];
  }

  public Number getDegree(int pos) {
    return degrees[pos];
  }

  public Number getAtan(int pos) {
    return atan[pos];
  }

  public Number getAsinacos(int pos) {
    return asinacos[pos];
  }
  
  //search in the arrays of pre-computed values the index
  //of the given value
  public static int searchValueIndex(double value, Type typeVar) {
    int returnValue;
    
    switch(typeVar.getUnit()){
    case ASIN_ACOS:
      returnValue = Arrays.binarySearch(asinacos, value);
      break;
    case ATAN:
      returnValue = Arrays.binarySearch(atan, value);
      break;
    case RADIANS:
      returnValue = Arrays.binarySearch(radians, value);
      break;
    case DEGREES:
      returnValue = Arrays.binarySearch(degrees, value);
      break;
    default:
      throw new RuntimeException("tried to search value index of type:" + typeVar.getUnit());
    }
    
    return returnValue;
  } 

}