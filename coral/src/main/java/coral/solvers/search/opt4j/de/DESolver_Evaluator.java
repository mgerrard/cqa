package coral.solvers.search.opt4j.de;

import com.google.inject.Inject;

import coral.solvers.search.opt4j.ctp.CTP_Evaluator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class DESolver_Evaluator extends CTP_Evaluator{

  @Inject
  public DESolver_Evaluator(CTP_Problem problem) {
    super(problem);
  }

}
