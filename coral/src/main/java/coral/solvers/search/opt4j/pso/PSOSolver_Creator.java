 package coral.solvers.search.opt4j.pso;

import com.google.inject.Inject;

import coral.solvers.rand.NumberGenerator;
import coral.solvers.search.opt4j.ctp.CTP_Creator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class PSOSolver_Creator extends CTP_Creator{

	@Inject
	public PSOSolver_Creator(CTP_Problem problem, NumberGenerator numberGenerator){
		super(problem,numberGenerator);
	}
}
