package coral.solvers.search.opt4j;

import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.optimizer.mopso.MOPSO;
import org.opt4j.optimizer.mopso.Particle;
import org.opt4j.start.Constant;

import com.google.inject.Inject;

import coral.util.Config;

public class RestartListener implements OptimizerIterationListener {

  final int limit;
  private double bestFitness;
  private int iterations = 0;

  @Inject
  public RestartListener(/*@Constant(value = "stagnationLimit") int limit*/) {
    this.limit = Config.stagnationLimit;
    System.out.println("[opt4j] enabling random restart...");
  }

  @Override
  public void iterationComplete(Optimizer optimizer, int iteration) {
    if (iteration == 1) {
      return;
    }
    
    if (optimizer instanceof MOPSO) {
      MOPSO mopso = (MOPSO) optimizer;
      Particle bestParticle = mopso.getCurrentBestIndividual();
      double fitness = bestParticle.getBestObjectives().array()[0];
      if (this.bestFitness != fitness) {
        this.bestFitness = fitness;
        iterations = 0;
      } else {
        iterations++;
      }

      if (iterations > limit) { // random restart
        System.out.println("Stopping optimization at iteration: " + iteration);
        mopso.terminate();
      }

      // Objectives bestParticleObjt = bestParticle.getBestObjectives();
      // Collection<Individual> indiv = mopso.getIndividuals();
    } else {
      throw new RuntimeException("Implement this for other solvers");
    }
  }
}
