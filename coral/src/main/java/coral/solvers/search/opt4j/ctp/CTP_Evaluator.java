package coral.solvers.search.opt4j.ctp;

import java.util.Arrays;
import java.util.Collection;

import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;

import coral.solvers.search.opt4j.PCPhenotype;

public abstract class CTP_Evaluator implements Evaluator<PCPhenotype>  {

	private final CTP_Problem problem;
	
	public CTP_Evaluator(CTP_Problem problem) {
		this.problem = problem;
	}
	
	@Override
	public Objectives evaluate(PCPhenotype phenotype) {
		double score = problem.getFitness().evaluate(phenotype);
		Objectives objectives = new Objectives();
	  objectives.add(CTP_Problem.oneObjective, score);
	  return objectives;		
	}

	public Collection<Objective> getObjectives() {
		return Arrays.asList(new Objective[]{CTP_Problem.oneObjective});
	}

}
