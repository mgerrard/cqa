package coral.solvers.search.opt4j.de;

import com.google.inject.Inject;

import coral.solvers.rand.NumberGenerator;
import coral.solvers.search.opt4j.ctp.CTP_Creator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class DESolver_Creator extends CTP_Creator{

  @Inject
  public DESolver_Creator(CTP_Problem problem, NumberGenerator numberGenerator) {
    super(problem, numberGenerator);
  }

}
