package coral.solvers.search.opt4j.fitness;

import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.start.Constant;

import com.google.inject.Inject;

public class SAWUpdateWeightListener implements ExtendedOptimizerIterationListener {

  private SAW saw;
  private final int maxHit; //set to population size -1
  
//  @Inject
//  public SAWUpdateWeightListener(SAW jigsaw) {
//    this.saw = jigsaw;
//  }
  
  @Inject
  public SAWUpdateWeightListener(FitnessFunction jigsaw,@Constant(value = "evalsPerRound") int nEvals) {
    this.saw = (SAW) jigsaw;
    this.maxHit = nEvals;
  }
  
  @Override
  public void iterationComplete(Optimizer optimizer, int iteration) {
    saw.updateWeightsIfNeeded(maxHit);
  }

}
