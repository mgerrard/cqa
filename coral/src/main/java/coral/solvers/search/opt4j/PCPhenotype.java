package coral.solvers.search.opt4j;

import java.util.Map;

import org.opt4j.core.Phenotype;

import symlib.SymLiteral;
import symlib.SymNumber;


public class PCPhenotype implements CoralPhenotype {
  
  private Map<SymLiteral, SymNumber> map;  

  public PCPhenotype(Map<SymLiteral, SymNumber> map) {
    super();
    this.map = map;
  }
  
  public Map<SymLiteral, SymNumber> getMap() {
    return map;
  }

  public String toString() {
  	return this.map.toString();
  }

}
