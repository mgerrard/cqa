package coral.solvers.search.opt4j.fitness;

import coral.solvers.search.opt4j.PCPhenotype;

/**
 * A fitness function evaluates how fit a 
 * candidate solution is to solve the 
 * optimization problem 
 * 
 * @author damorim
 * @author mab
 *
 */
public interface FitnessFunction {
  
  double evaluate(PCPhenotype genecode);
  double getCurrentMaxScore();
  public PCPhenotype getBestParticle(); 
  
}
