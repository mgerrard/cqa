package coral.solvers.search.opt4j.de;

import java.util.Map;

import coral.PC;
import coral.solvers.SolverKind;
import coral.solvers.search.opt4j.ctp.CTP_Module;
import coral.util.Interval;

public class DESolver_Module extends CTP_Module{

  public DESolver_Module(PC pc, Map<Integer, Interval> box) {
    super(pc, SolverKind.DE_OPT4J, box);
  }
  
  public DESolver_Module(PC pc, PC originalPc, Map<Integer, Interval> box) {
    super(pc, originalPc, SolverKind.DE_OPT4J, box);
  }

  @Override
  protected void config() {
    bindProblem(DESolver_Creator.class, DESolver_Decoder.class, DESolver_Evaluator.class);
  }

}
