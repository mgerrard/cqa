package coral.solvers.search.opt4j.ga;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Population;
import org.opt4j.optimizer.ea.EvolutionaryAlgorithmModule;
import org.opt4j.start.Opt4JTask;

import com.google.inject.Module;

import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.UnsatException;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.solvers.search.opt4j.SolutionFoundException;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.util.Config;

public class GASolver extends Solver {
  
  PC originalPc = null;
  
  public GASolver() {
    super(SolverKind.GA_OPT4J);
  }

  @Override
  protected Env call(PC pc, Guard guard) {
    Collection<Module> modules = new ArrayList<Module>();
    GASolver_Module gm;
    
    if(originalPc == null) {
      gm = new GASolver_Module(pc, super.getBox());
    } else {
      gm = new GASolver_Module(pc,originalPc,super.getBox());
    }
    modules.add(gm);
    
    FitnessFunction ff = gm.getFitnessFunction();
    EvolutionaryAlgorithmModule em = new CoralEvolutionaryAlgorithmModule(ff);
    em.setAlpha(Config.nPopulationGA);
    em.setCrossoverRate(Config.crossoverRateGA);
    em.setGenerations(Config.nIterationsGA);
    em.setLambda(Config.nPopulationGA / 4);
    em.setMu(Config.nPopulationGA / 4);
    modules.add(em);
    
    Opt4JTask task = new Opt4JTask(false);
    task.init(modules);
    Env result = null;
    try {
      task.execute();
      Set<Individual> population = task.getInstance(Population.class);
      for(Individual iv : population) {
        PCPhenotype p = (PCPhenotype) iv.getPhenotype();
        result = isSolution(pc,p);
        if(result != null) {
          break;
        }
      }
    } catch (SolutionFoundException _) {
      PCPhenotype p = _.getBestPhenotype();
      result = isSolution(pc, p); /* double check */
    } catch (UnsatException _){
      result = new Env(null, Result.UNSAT, kind);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      task.close();
    }
    return result;
  }

  @Override
  protected Env call(PC optimizedPc, PC originalPc, Guard guard) {
    this.originalPc = originalPc;
    return call(optimizedPc,guard);
  }

  @Override
  protected Env recall(PC pc, Guard guard) {
    return call(pc,guard);
  }

}
