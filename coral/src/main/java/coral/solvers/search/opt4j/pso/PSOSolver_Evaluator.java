package coral.solvers.search.opt4j.pso;

import com.google.inject.Inject;

import coral.solvers.search.opt4j.ctp.CTP_Evaluator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class PSOSolver_Evaluator extends CTP_Evaluator{
		
	@Inject
	public PSOSolver_Evaluator(CTP_Problem problem) {
		super(problem);
	}

}
