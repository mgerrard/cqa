package coral.solvers.search.opt4j.de;

import org.opt4j.optimizer.de.DifferentialEvolutionModule;
import org.opt4j.start.Constant;

import coral.solvers.search.opt4j.CoralterationListener;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.solvers.search.opt4j.fitness.SAW;
import coral.solvers.search.opt4j.fitness.SAWUpdateWeightListener;
import coral.util.Config;

public class CoralDifferentialEvolutionModule extends DifferentialEvolutionModule{

  FitnessFunction f;
  @Constant(value = "evalsPerRound")
  int evalsPerRound = Config.nPopulationDE - 1;
  
  public int getEvalsPerRound() {
    return evalsPerRound;
  }

  public void setEvalsPerRound(int evalsPerRound) {
    this.evalsPerRound = evalsPerRound;
  }

  public CoralDifferentialEvolutionModule(FitnessFunction f) {
    super();
    this.f = f;
  }
  
  @Override
  public void config() {
    super.config();
    addOptimizerIterationListener(CoralterationListener.class);
    if(f instanceof SAW) {
      addOptimizerIterationListener(SAWUpdateWeightListener.class);
    }
  }
}
