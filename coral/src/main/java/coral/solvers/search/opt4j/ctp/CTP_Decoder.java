package coral.solvers.search.opt4j.ctp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opt4j.core.Individual;
import org.opt4j.core.problem.Decoder;
import org.opt4j.genotype.DoubleGenotype;

import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.Util;
import coral.solvers.Type;
import coral.solvers.Unit;
import coral.solvers.rand.NumberGenerator;
import coral.solvers.search.opt4j.PCPhenotype;

public abstract class CTP_Decoder implements
    Decoder<DoubleGenotype, PCPhenotype> {

  private CTP_Problem problem;
  List<Individual> lastSolutions;
  
  public CTP_Decoder(CTP_Problem problem) {
    this.problem = problem;
    lastSolutions = problem.getLastGoodAnswer();
  }

  @Override
  public PCPhenotype decode(DoubleGenotype genotype) {

    SymLiteral[] vars = problem.getVars();
    Map<SymLiteral, Type> varToUnit = problem.getVars2Unit();    
    NumberGenerator tData = problem.getNumberGenerator();

    Map<SymLiteral, SymNumber> map = new HashMap<SymLiteral, SymNumber>();
    for (int i = 0; i < vars.length; i++) {
      // variable and unit
      SymLiteral current = vars[i];
      Unit unit = varToUnit.get(current).getUnit();
      // individual value
      double gen = genotype.get(i);
      SymNumber val;
      /***
       * we need to distinguish two kinds of domains: a finite discrete domains,
       * and the rest.
       ***/
      val = gen(tData, unit, gen);
      map.put(vars[i], val);
    }
    
    PCPhenotype data = new PCPhenotype(map);
    return data;
  }

  private SymNumber gen(NumberGenerator tData, Unit unit, double gen) {
    int pos;
    SymNumber val;
    switch (unit) {
    case RADIANS:
      pos = (int) Math.round(gen);
      val = Util.createConstant(tData.getRadian(pos));
      break;
    case DEGREES:
      pos = (int) Math.round(gen);
      val = Util.createConstant(tData.getDegree(pos));
      break;
    case ATAN:
      pos = (int) Math.round(gen);
      val = Util.createConstant(tData.getAtan(pos));
      break;
    case ASIN_ACOS:
      pos = (int) Math.round(gen);
      val = Util.createConstant(tData.getAsinacos(pos));
      break;
    case LIMITED_INT:
      val = Util.createConstant((int)gen);
      break;
    case LIMITED_LONG:
      val = Util.createConstant((long)gen);
      break;
    case LIMITED_FLOAT:
      val = Util.createConstant((float)gen);
      break;
    default:
      val = Util.createConstant(gen);
      break;
    }
    return val;
  }

}