package coral.solvers.search.opt4j.pso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Population;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.optimizer.mopso.MOPSOModule;
import org.opt4j.start.Opt4JTask;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.parser.ParseException;
import symlib.parser.Parser;

import com.google.inject.Module;

import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.UnsatException;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.solvers.search.opt4j.SolutionFoundException;
import coral.solvers.search.opt4j.ctp.CTP_Problem;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.solvers.search.opt4j.fitness.SAW;
import coral.solvers.search.opt4j.fitness.SAWUpdateWeightListener;
import coral.tests.Benchmark;
import coral.tests.Helper;
import coral.util.Config;
import coral.util.Interval;
import coral.util.PCSolutions;
import coral.util.trees.Trie;

public class PSOSolver extends Solver {

  PC originalPc = null; //!= null when solving a optimized constraint
  Set<Individual> lastPopulation = null;
  boolean measureDistance = true;
  
  public Set<Individual> getLastPopulation() {
    return lastPopulation;
  }
  
  private boolean prematureTermination = false;
  
  public boolean isTerminated() {
    return prematureTermination;
  }

  public PSOSolver() {
    super(SolverKind.PSO_OPT4J);
  }

  @Override
  protected Env call(PC pc, Guard guard) {
    Collection<Module> modules = new ArrayList<Module>();
    
    PSOSolver_Module pm;
    if(originalPc != null) {
      pm = new PSOSolver_Module(pc,originalPc,super.getBox());
    } else {
      pm = new PSOSolver_Module(pc,super.getBox());
    }
    FitnessFunction fitness = pm.getFitnessFunction();
    modules.add(pm);

    MOPSOModule mopso = new HackedMOPSOModule(fitness);
    mopso.setParticles(Config.nParticlesPSO);
    mopso.setIterations(Config.nIterationsPSO);
    mopso.setPerturbation(Config.perturbationPSO);
    modules.add(mopso);

    Opt4JTask task = new Opt4JTask(false);
    task.init(modules);
    Env result = null;
    try {
      task.execute();
//      Archive archive = task.getInstance(Archive.class);
      if (task.getIteration() < Config.nIterationsPSO) {
        prematureTermination = true;
      }
      Set<Individual> population = task.getInstance(Population.class);
      lastPopulation = population;
      //to use if 
      PCPhenotype bestParticle = null;
      // best solutions are store in archive
      for (Individual individual : population) {
        PCPhenotype p = (PCPhenotype) individual.getPhenotype();
        result = isSolution(pc, p);
        
        if (result != null) {
          break;
        }
      }
      
      //no solution found, put best element in env
      result = new Env(fitness.getBestParticle().getMap(),Result.UNK,this.kind);
      
    } catch (SolutionFoundException _) {
      PCPhenotype p = _.getBestPhenotype();
      result = isSolution(pc, p); /* double check */
      if (result != null) {
//        System.out.println("Solution found!");
      }
    } catch (UnsatException _){
      result = new Env(null, Result.UNSAT, kind);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {      
      
//      if(measureDistance && result != null && result.getResult() == Result.SAT) {
//        CTP_Problem problem = task.getInstance(CTP_Problem.class);
//        Map<Integer,Interval> box = problem.getIntervalSolverResults();
//      
//        double distance = 0;
//        boolean onTheBox = true;
//        
//        for(Map.Entry<SymLiteral, SymNumber> entry : result.getMapLiteralToNumber().entrySet()) {
//          SymLiteral lit = entry.getKey();
//          SymNumber num = entry.getValue();
//          int id = lit.getId();
//          double val = num.evalNumber().doubleValue();
//          
//          Interval iv = box.get(id);
//          if(iv != null) {
//            if(val > iv.hi) {
//              onTheBox = false;
//              double delta = Math.pow((val - iv.hi), 2);
//              distance += delta;
////              System.out.println("box:" + iv);
////              System.out.println("("+val+" - "+iv.hi+")^2="+delta);
//            } else if (val < iv.lo) {
//              onTheBox = false;
//              double delta = Math.pow((val - iv.lo), 2);
//              distance += delta;
////              System.out.println("box:" + iv);
////              System.out.println("("+val+" - "+iv.lo+")^2="+delta);
//            }
//          }
//        }
//        
//        distance = Math.sqrt(distance);
//        
//        if(onTheBox) {
//          Helper.timebuffer.append(" #ON THE BOX ");
//        } else {
//          Helper.timebuffer.append(" ##OUT OF THE BOX. DISTANCE:" + distance + " ");
////          System.out.println("##VARS:" + distance);
//        }
//      }
      
      if(Config.enableIndividualStoring || Config.storeLastIndividuals) {
        Population population = task.getInstance(Population.class);
        if(originalPc != null) {
          extractOldSolutions(population,originalPc); 
        } else {
          extractOldSolutions(population,pc);
        }
      }
      task.close();
    }
    return result;
  }

  public static class IncompleteInitializationException extends RuntimeException {}
  
  // currently we are using the $x best particles of previous searches in new
  // runs ($x = Config.maxInsertedIndividuals).
  // TODO evaluate other combinations (ex.: best + ($x-1) random ones)
  
  private void extractOldSolutions(Population population, PC pc) {

    SortedSet<Individual> sortedOldSolutions = new TreeSet<Individual>(
        new Comparator<Individual>() {
          @Override
          public int compare(Individual o1, Individual o2) {
            int returnVal = 0;
            
            if (o1.getObjectives() == null || o2.getObjectives() == null) {
            //needs to be a runtimeException because of the signature
              throw new IncompleteInitializationException(); 
            }
            
            if (o1.getObjectives().dominates(o2.getObjectives())) {
              returnVal = -1;
            } else if (o2.getObjectives().dominates(o1.getObjectives())) {
              returnVal = 1;
            }
            return returnVal;
          }
        });

    try {
      sortedOldSolutions.addAll(population);
    } catch (IncompleteInitializationException e) {
      //solver found a solution before the first iteration; state is inconsistent
      //just skip the storage of solutions
      sortedOldSolutions.clear();
    }
        
    int i = 0;
    Iterator<Individual> iterator = sortedOldSolutions.iterator();
    List<Individual> bestIndividuals = new ArrayList<Individual>(Config.maxInsertedIndividuals);
    
    //if the constraint was solved in the first iteration, sortedOldSets has less
    //objects than the usual. I think this happens because some particles weren't
    //evaluated
    while (i < Config.maxInsertedIndividuals && iterator.hasNext()) {
      bestIndividuals.add(iterator.next());
      i++;
    }
    
    //insert the best solutions into the cache
    PCSolutions<Individual> oldSolutions = new PCSolutions<Individual>(pc, bestIndividuals);
    
    if(Config.enableIndividualStoring) {
      Trie<String,PCSolutions<Individual>> trie = Config.cache.getTrie();
      List<String> pcsToStrings = new ArrayList<String>(bestIndividuals.size());
      for(SymBool clause : pc.getConstraints()) {
        pcsToStrings.add(clause.toString());
      }    
      trie.insert(pcsToStrings, oldSolutions);
    } else if (Config.storeLastIndividuals) {
      Config.cache.setLastSolution(oldSolutions);
    } else {
      throw new RuntimeException("What the hell just happened? How i've ended here?");
    }
//    System.out.println("Inserted " + pc);
//    System.out.println("to trie: " + trie);
//    System.out.println("\n");
    
  }

  @Override
  protected Env recall(PC pc, Guard guard) {
    Env env = this.call(pc, guard);
    return env;
  }
  
  public static void main(String[] args) throws ParseException {
    // String pc1 = "DGT(ADD(SIN_(DVAR(ID_1)),COS_(DVAR(ID_2))),DCONST(1.0))";
    // String pc1 =
    // "BAND(DLT(SIN_(SIN_(MUL(DVAR(ID_1),DVAR(ID_2)))),DCONST(0.0)),DGT(COS_(MUL(DVAR(ID_1),DCONST(2.0))),DCONST(0.25)))";;
    String pc1 = Benchmark.pc5;
    PC pc = (new Parser(pc1)).parsePC();
    PSOSolver solver = new PSOSolver();
    Env env = solver.call(pc);
    System.out.println(env);
  }

  @Override
  protected Env call(PC optimizedPc, PC originalPc, Guard guard) {
    this.originalPc = originalPc;
    return call(optimizedPc, guard);
  }
  
}