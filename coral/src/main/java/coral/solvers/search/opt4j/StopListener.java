package coral.solvers.search.opt4j;

import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerIterationListener;

import coral.solvers.Solver.Guard;

public class StopListener implements OptimizerIterationListener{

	private Guard guard;
	
	public StopListener(Guard guard) {
		this.guard = guard;
	}
	
	@Override
	public void iterationComplete(Optimizer optimizer, int iteration) {
	  boolean val = guard.shouldStop();
		if(val) {
			optimizer.stopOptimization();
		}
	}

}
