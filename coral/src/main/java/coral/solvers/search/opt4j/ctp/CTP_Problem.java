package coral.solvers.search.opt4j.ctp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.opt4j.core.Individual;
import org.opt4j.core.Objective;
import org.opt4j.core.Objective.Sign;

import symlib.SymBool;
import symlib.SymDoubleLiteral;
import symlib.SymFloatLiteral;
import symlib.SymIntLiteral;
import symlib.SymLiteral;
import coral.PC;
import coral.WeightedPC;
import coral.solvers.SolverKind;
import coral.solvers.Type;
import coral.solvers.Unit;
import coral.solvers.rand.NumberGenerator;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.solvers.search.opt4j.fitness.SAW;
import coral.solvers.search.opt4j.fitness.WeightedFitnessFunction;
import coral.util.Config;
import coral.util.Interval;
import coral.util.PCSolutions;
import coral.util.callers.IntervalSolverCaller;
import coral.util.callers.RealPaverCaller;
import coral.util.visitors.SymLiteralTypeSearcher;

public class CTP_Problem {

	private final PC pc;
	private final PC originalPc;
	private final SymLiteral[] vars;
	private final FitnessFunction fitness;
	private final Number[] samplePositions;
  private final Map<SymLiteral, Type> var2Units; 
  public static final Objective oneObjective = new Objective("fitness", Sign.MAX);
  private final double[] lowerBounds;
  private final double[] upperBounds;
  private Map<Integer,Interval> intervalSolverResults; 
  private final NumberGenerator numberGenerator;
//  private final Env oldEnv; //env from first execution of problem
  
	public CTP_Problem(PC pc,PC originalPc, NumberGenerator tdata,SolverKind kind, Map<Integer,Interval> box) {

    Set<SymLiteral> tmp = pc.getVars();
    
	  // create data
		this.pc = pc;	
		this.numberGenerator = tdata;
		this.vars = tmp.toArray(new SymLiteral[tmp.size()]);
		//TODO refactor this
		if (pc instanceof WeightedPC) {
		  this.fitness = new WeightedFitnessFunction((WeightedPC) pc);
		} else {
		  this.fitness = new SAW(pc,kind);
		}
				
    this.samplePositions = new Number[vars.length];    
    this.var2Units = new SymLiteralTypeSearcher(pc,Config.flexibleRange).getUnits();        
    this.lowerBounds = new double[this.vars.length];
    this.upperBounds = new double[this.vars.length];
    this.originalPc = originalPc != null ? originalPc : pc;
    
//    this.oldEnv = oldEnv;
    
    if (vars.length == 0) {
      throw new RuntimeException("zero vars - eval the pc"); 
    }
        
		// initialize
    for (int i = 0; i < vars.length; i++) {
      SymLiteral symLiteral = vars[i];
      Type defaultType;
      
      if (symLiteral instanceof SymIntLiteral) {
        samplePositions[i] = new Integer(0);
        defaultType = new Type(Unit.LIMITED_INT,Config.rangeLO,Config.rangeHI);
      } else if (symLiteral instanceof SymFloatLiteral) {
        samplePositions[i] = new Float(0);
        defaultType = new Type(Unit.LIMITED_FLOAT,Config.rangeLO,Config.rangeHI);
      } else if (symLiteral instanceof SymDoubleLiteral){
        samplePositions[i] = new Double(0);
        defaultType = new Type(Unit.LIMITED_DOUBLE,Config.rangeLO,Config.rangeHI);
      } else {
        throw new RuntimeException("Unexpected type: " + symLiteral.getClass());
      }
      
      Type type = var2Units.get(symLiteral);            
      if (type == null) { //populate with default type
        type = defaultType;
        var2Units.put(symLiteral, type);
      }
      
      lowerBounds[i] = tdata.getLowerBound(type);
      upperBounds[i] = tdata.getUpperBound(type);
    }
    
      intervalSolverResults = box != null? box : new TreeMap<Integer,Interval>();
  }
	
	public PC getPc() {
		return pc;
	}

  public SymLiteral[] getVars() {    
    return vars;
  }	
	
	public FitnessFunction getFitness() {
		return fitness;
	}
	
	public Number[] getSamplePositions() {
		return samplePositions;
	}
	
	public Map<SymLiteral, Type> getVars2Unit() {
		return var2Units;
	}
	
	public double[] getLowerBounds() {
		return lowerBounds;
	}
	
	public double[] getUpperBounds() {
		return upperBounds;
	}
		
	public NumberGenerator getNumberGenerator() {
	  return numberGenerator;
	}

	//TODO optimize this!
  public List<Individual> getLastGoodAnswer() {
    List<Individual> returnList;
    PCSolutions<Individual> lastSolutions = null;
    
    if(Config.enableIndividualStoring) {
      List<SymBool> clauses = originalPc.getConstraints();
      List<String> pcsToStrings = new ArrayList<String>(clauses.size());
      
      for (SymBool clause : clauses) {
        pcsToStrings.add(clause.toString());
      }
      lastSolutions = Config.cache.getTrie().searchPrefix(pcsToStrings);
    } else if (Config.storeLastIndividuals) {
      lastSolutions = Config.cache.getLastSolution();
    } 
    
    if(lastSolutions != null) {
//      System.out.println("## returning solutions for " + originalPc+ "::");
//      System.out.println("   originally from:"+lastSolutions.getPc());
      returnList = lastSolutions.getSolution();
    }  else {
//      System.out.println("## no previous solution for " + originalPc);
      returnList = new ArrayList<Individual>();
    }
    
    return returnList;
  }
  
  public void transformTypeVar(SymLiteral var, Unit newUnit) {
    for(int i = 0; i < vars.length; i++) {
      if(vars[i].equals(var)) {
        Type type = var2Units.get(var);
        type.setUnit(newUnit);
        lowerBounds[i] = numberGenerator.getLowerBound(type);
        upperBounds[i] = numberGenerator.getUpperBound(type);
      }
    }
  }

  public Map<Integer, Interval> getIntervalSolverResults() {
    return intervalSolverResults;
  }
}
