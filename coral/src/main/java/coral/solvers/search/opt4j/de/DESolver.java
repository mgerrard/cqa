package coral.solvers.search.opt4j.de;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Population;
import org.opt4j.optimizer.de.DifferentialEvolutionModule;
import org.opt4j.start.Opt4JTask;

import com.google.inject.Module;

import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.UnsatException;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.solvers.search.opt4j.SolutionFoundException;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.util.Config;

public class DESolver extends Solver{

  PC originalPc = null; //not null when solving an optimized constraint
  
  public DESolver() {
    super(SolverKind.DE_OPT4J);
  }

  @Override
  protected Env call(PC pc, Guard guard) {
    Collection<Module> modules = new ArrayList<Module>();
    DESolver_Module dm;
    
    if(originalPc == null) {
      dm = new DESolver_Module(pc, super.getBox());
    } else {
      dm = new DESolver_Module(pc, originalPc, super.getBox());
    }
    modules.add(dm);

    FitnessFunction ff = dm.getFitnessFunction();
    DifferentialEvolutionModule dem = new CoralDifferentialEvolutionModule(ff);
    dem.setAlpha(Config.nPopulationDE);
    dem.setGenerations(Config.nIterationsDE);
    dem.setScalingFactor(Config.scalingFactorDE);
    modules.add(dem);
    
    Opt4JTask task = new Opt4JTask(false);
    task.init(modules);
    Env result = null;
    try {
      task.execute();
      Set<Individual> population = task.getInstance(Population.class);
      for(Individual iv : population) {
        PCPhenotype p = (PCPhenotype) iv.getPhenotype();
        result = isSolution(pc,p);
        if(result != null) {
          break;
        }
      }
    } catch (SolutionFoundException _) {
      PCPhenotype p = _.getBestPhenotype();
      result = isSolution(pc, p); /* double check */
    } catch (UnsatException _){
      result = new Env(null, Result.UNSAT, kind);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      task.close();
    }
    return result;
  }

  @Override
  protected Env call(PC optimizedPc, PC originalPc, Guard guard) {
    this.originalPc = originalPc;  
    return call(optimizedPc, guard);
  }

  @Override
  protected Env recall(PC pc, Guard guard) {
    return this.call(pc,guard);
  }

}
