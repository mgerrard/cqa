/***********************************************
 * Stepwise Adaptation of Weights
 ***********************************************
 *
 * Each clause has a weight that increases to 
 * identify the difficult clauses.
 * 
 ***********************************************/
package coral.solvers.search.opt4j.fitness;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

import symlib.SymBool;
import symlib.SymBoolOperations;
import symlib.SymDoubleRelational;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import coral.PC;
import coral.solvers.SolverKind;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.solvers.search.opt4j.SolutionFoundException;
import coral.util.Config;


public class SAW implements FitnessFunction {

  private PC pc;
  /**
   * weight of constraint "i"
   */
  private double[] weights;
  /**
   * number of times constraint "i" 
   * evaluated to false  
   */
  private int[] counter;
  /**
   * last increment applied to counter 
   */
  private int[] lastIncrement;
  //  private SolverKind kind;
  /**
   * threshold that defines when to increase 
   * weight.  The weight of a constraint is 
   * increased if counter > MAX_HIT 
   */
  private static final int MAX_HIT = Config.maxHit;

  //FIXME refactor this
  private int numSat = 0;

  public SAW(PC pc,SolverKind kind) {
    this.pc = pc;
    //    this.kind = kind;

    int numOfConstraints = pc.getConstraints().size();
    weights = new double[numOfConstraints];
    Arrays.fill(weights, 1); 
    counter = new int[numOfConstraints];
    Arrays.fill(counter, 0);
    lastIncrement = new int[numOfConstraints];
    Arrays.fill(lastIncrement, 0);
  }

  @Override
  public double evaluate(PCPhenotype position) {
    updateCounter();

    /**
     * evaluate fitness of candidate
     */
    Map<SymLiteral, SymNumber> map = position.getMap();		

    for (Map.Entry<SymLiteral, SymNumber> entry : map.entrySet()) {
      entry.getKey().setCte(entry.getValue().evalNumber());
    }
    List<SymBool> constraints = pc.getConstraints();
    double fitnessValue = 0d;

    int len = constraints.size();
    numSat = 0;
    for (int i = 0; i < len; i++) {
      SymBool constraint = constraints.get(i);
      double tempValue;
      try {
        if(Util.isORConstraint(constraint)) {
          tempValue = Util.computeMaxFitnessOR(constraint);
        } else {
          tempValue = Util.computeClause(constraint);
        }
        //now is inside off
        //numSat += (tempValue == 1 ? 1 : 0);
        numSat += (tempValue == 1 ? 1 : 0);  
        fitnessValue += (tempValue * weights[i]);
      } 
      catch (ArithmeticException _) { }
      catch (NullPointerException _) { System.out.println(constraint); }
    }

    if (len == numSat) {
      throw new SolutionFoundException(position,this.getCurrentMaxScore());
    }

    /**
     * remember best fitness value and position
     */
    if (bestFitness < fitnessValue) { // new best
      bestFitness = fitnessValue;
      bestParticle = position;
      particleChanged = true;
    } else {
      particleChanged = false;
    }

    return fitnessValue;
  }

  /**
   * best fitness value and particle
   */
  private double bestFitness = -1; //avoid null values by making the first particle the current best
  private PCPhenotype bestParticle;
  private boolean particleChanged;

  private void updateCounter() {

    if (particleChanged) {
      Map<SymLiteral, SymNumber> map = bestParticle.getMap();
      for (Map.Entry<SymLiteral, SymNumber> entry : map.entrySet()) {
        entry.getKey().setCte(entry.getValue().evalNumber());
      }
      List<SymBool> constraints = pc.getConstraints();
      for (int i = 0; i < constraints.size(); i++) {
        SymBool constraint = constraints.get(i);
        try {
          //TODO add support to compiled constraints
          ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
          boolean result = revPol.eval().intValue() == 1;
          // increase counter if constraint is not satisfied
          lastIncrement[i] = result ? 0 : 1;
          counter[i] += result ? 0 : 1;

        } 
        catch (ArithmeticException _) { }
        catch (NullPointerException _) { System.out.println(constraint); }
      }
    } else { //update counter with pre-computed values from best particle
      for (int i = 0; i < counter.length; i++) {
        // increase counter if constraint is not satisfied
        counter[i] += lastIncrement[i];
      }
    }
  }

  //Update weights. Call at the end of the optimizer iteration.
  public void updateWeightsIfNeeded(int maxHit) {
    for (int i = 0; i < counter.length; i++) {
      // increase weight of constraint and reset counter
      if (counter[i] >= maxHit) {
        weights[i]++;
        counter[i] = 0;
      }
    }
   
    //update best fitness
    if(bestParticle != null) { //
      bestFitness = evaluate(bestParticle);
    }
  }

  @Override
  public double getCurrentMaxScore() {
    double tmp = 0;
    for(double w : weights) {
      tmp += w;
    }
    return tmp;
  }
  
  public PCPhenotype getBestParticle() {
    return bestParticle;
  }

}
