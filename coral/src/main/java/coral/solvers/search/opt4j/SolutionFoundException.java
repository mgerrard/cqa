package coral.solvers.search.opt4j;

@SuppressWarnings("serial")
public class SolutionFoundException extends RuntimeException {

  PCPhenotype bestPhenotype;
  double maxScore;
  
  public SolutionFoundException(PCPhenotype bestPhenotype,double maxScore) {
    super();
    this.bestPhenotype = bestPhenotype;
    this.maxScore = maxScore;
  }

  public PCPhenotype getBestPhenotype() {
    return bestPhenotype;
  }

  public double getMaxScore() {
    return maxScore;
  }

}
