package coral.solvers.search.opt4j.ga;

import org.opt4j.optimizer.ea.EvolutionaryAlgorithmModule;
import org.opt4j.start.Constant;

import coral.solvers.search.opt4j.CoralterationListener;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.solvers.search.opt4j.fitness.SAW;
import coral.solvers.search.opt4j.fitness.SAWUpdateWeightListener;
import coral.util.Config;

public class CoralEvolutionaryAlgorithmModule extends EvolutionaryAlgorithmModule {
  
  FitnessFunction f;
  @Constant(value = "evalsPerRound")
  int evalsPerRound = Config.nPopulationGA - 1;
  
  public int getEvalsPerRound() {
    return evalsPerRound;
  }

  public void setEvalsPerRound(int evalsPerRound) {
    this.evalsPerRound = evalsPerRound;
  }

  public CoralEvolutionaryAlgorithmModule(FitnessFunction f) {
    super();
    this.f = f;
  }
  
  @Override
  public void config() {
    super.config();
    addOptimizerIterationListener(CoralterationListener.class);
    if(f instanceof SAW) {
      addOptimizerIterationListener(SAWUpdateWeightListener.class);
    }
  }
}
