package coral.solvers.search.opt4j.fitness;

import java.util.List;
import java.util.Map;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.SymNumber;
import coral.WeightedPC;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.solvers.search.opt4j.SolutionFoundException;

/**
 * Fitness function to use with WeightedPCs.
 * @author mateus
 */

// TODO copy-pasted from SAW; merge with it later
public class WeightedFitnessFunction implements FitnessFunction {

  public final WeightedPC pc;
  private double bestFitness;
  private PCPhenotype bestParticle;
  //FIXME refactor this
  private int numSat = 0;

  
  public WeightedFitnessFunction(WeightedPC pc) {
    this.pc = pc;
  }
  
  @Override
  public double evaluate(PCPhenotype position) {
    /**
     * evaluate fitness of candidate
     */
    Map<SymLiteral, SymNumber> map = position.getMap();   

    for (Map.Entry<SymLiteral, SymNumber> entry : map.entrySet()) {
      entry.getKey().setCte(entry.getValue().evalNumber());
    }
    List<SymBool> constraints = pc.getConstraints();
    double fitnessValue = 0d;

    int len = constraints.size();

    for (int i = 0; i < len; i++) {
      SymBool constraint = constraints.get(i);
      double tempValue;
      try {
        if(Util.isORConstraint(constraint)) {
          tempValue = Util.computeMaxFitnessOR(constraint);
        } else {
          tempValue = Util.computeClause(constraint);
        }
        //now is inside off
        numSat += (tempValue == 1 ? 1 : 0);
        fitnessValue += (tempValue * pc.weights.get(i));
      } 
      catch (ArithmeticException _) { }
    }

    if (len == numSat) {
      throw new SolutionFoundException(position,this.getCurrentMaxScore());
    }

    /**
     * remember best fitness value and position
     */
    if (bestFitness < fitnessValue) { // new best
      bestFitness = fitnessValue;
      bestParticle = position;
    }

    return fitnessValue;
  }

  @Override
  public double getCurrentMaxScore() {
    double tmp = 0;
    for(int w : pc.weights) {
      tmp += w;
    }
    return tmp;
  }

  @Override
  public PCPhenotype getBestParticle() {
    return bestParticle;
  }
  
}
