package coral.solvers.search.opt4j.ga;

import com.google.inject.Inject;

import coral.solvers.search.opt4j.ctp.CTP_Decoder;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class GASolver_Decoder extends CTP_Decoder {
  	
	@Inject
	public GASolver_Decoder(CTP_Problem problem) {
		super(problem);
	}
		
}
