package coral.solvers.search.opt4j.pso;

import com.google.inject.Inject;

import coral.solvers.search.opt4j.ctp.CTP_Decoder;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class PSOSolver_Decoder extends CTP_Decoder{
	
	@Inject
	public PSOSolver_Decoder(CTP_Problem problem) {
		super(problem);
	}

}
