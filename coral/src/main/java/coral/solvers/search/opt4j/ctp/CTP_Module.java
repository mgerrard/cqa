package coral.solvers.search.opt4j.ctp;

import java.util.Map;

import org.opt4j.core.Individual;
import org.opt4j.core.problem.ProblemModule;

import com.google.inject.Provides;

import coral.PC;
import coral.solvers.Env;
import coral.solvers.SolverKind;
import coral.solvers.rand.NumberGenerator;
import coral.solvers.search.opt4j.fitness.FitnessFunction; 
import coral.util.Config;
import coral.util.Interval;

public abstract class CTP_Module extends ProblemModule {

	public final CTP_Problem problem;
	public final NumberGenerator numberGenerator;

  public CTP_Module(PC pc,SolverKind kind, Map<Integer,Interval> box) {
		numberGenerator = new NumberGenerator(Config.RANGE);
		problem = new CTP_Problem(pc,null, numberGenerator, kind,box);
	}
  
  public CTP_Module(PC pc,PC originalPc,SolverKind kind, Map<Integer,Interval> box) {
    numberGenerator = new NumberGenerator(Config.RANGE);
    problem = new CTP_Problem(pc,originalPc, numberGenerator, kind,box);
  }

  @Provides
	public CTP_Problem getProblem(){
		return problem;		
	}	
	
	@Provides
	public NumberGenerator getNumberGenerator() {
		return numberGenerator;
	}
	
	@Provides
	public FitnessFunction getFitnessFunction() {
    return problem.getFitness();
	}
}
