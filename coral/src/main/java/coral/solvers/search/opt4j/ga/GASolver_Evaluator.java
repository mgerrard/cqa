package coral.solvers.search.opt4j.ga;

import com.google.inject.Inject;

import coral.solvers.search.opt4j.ctp.CTP_Evaluator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class GASolver_Evaluator extends CTP_Evaluator {

	@Inject
	public GASolver_Evaluator(CTP_Problem problem) {
		super(problem);
	}

}
