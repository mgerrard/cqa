package coral.solvers.search.opt4j;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;
import org.opt4j.core.Genotype;
import org.opt4j.genotype.ListGenotype;

import symlib.SymDoubleLiteral;
import symlib.SymIntLiteral;
import symlib.SymLiteral;
import coral.solvers.rand.Util;
import coral.util.Range;

@SuppressWarnings("serial")
public class NumberGenotype extends ArrayList<Number> implements ListGenotype<Number> {
  
  private Range range;  
  private SymLiteral[] lits;

	public NumberGenotype(SymLiteral[] lits, Range range) {
	  this.lits = lits;
		this.range = range;
	}


	/**
	 * Returns the lower bound for the {@code i}-th element.
	 * 
	 * @param index
	 *            the {@code i}-th element
	 * @return the lower bound of the {@code i}-th element
	 */
	public double getLowerBound(int index) {
		return range.getLo();
	}

	/**
	 * Returns the upper bound for the {@code i}-th element.
	 * 
	 * @param index
	 *            the {@code i}-th element
	 * @return the upper bound of the {@code i}-th element
	 */
	public double getUpperBound(int index) {
		return range.getHi();
	}

	
	//TODO revise this method - why we are using reflection here?
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opt4j.core.Genotype#newInstance()
	 */
	@SuppressWarnings("unchecked")
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends NumberGenotype> cstr = this.getClass()
					.getConstructor(SymLiteral[].class, Range.class);
			return (G) cstr.newInstance(lits, range.getLo(), range.getHi());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Initialize this genotype with {@code n} random values.
	 * 
	 * @param random
	 *            the random number generator
	 * @param n
	 *            the number of elements in the resulting genotype
	 */
	public void init(RandomDataImpl random) {
		for (int i = 0; i < lits.length; i++) {			
			SymLiteral lit = lits[i];
			Number value;
			if (lit instanceof SymIntLiteral) {
			  value = Util.generateIntValueRand(random, range);
			} else if (lit instanceof SymDoubleLiteral) {
			  value = (double) Util.generateFloatValueRand(random, range);
			} else {
			  throw new RuntimeException("please, complete this!");
			}
			
			if (i >= size()) {
				add(value);
			} else {
				set(i, value);
			}
		}
	}
	
	
}
