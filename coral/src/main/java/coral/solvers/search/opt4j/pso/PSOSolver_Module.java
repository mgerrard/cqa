package coral.solvers.search.opt4j.pso;

import java.util.Map;

import coral.PC;
import coral.solvers.SolverKind;
import coral.solvers.search.opt4j.ctp.CTP_Module;
import coral.util.Interval;

public class PSOSolver_Module extends CTP_Module{

  public PSOSolver_Module(PC pc,Map<Integer,Interval> box) {
    super(pc,SolverKind.PSO_OPT4J,box);
  }

	public PSOSolver_Module(PC pc, PC originalPc, Map<Integer,Interval> box) {
    super(pc,originalPc,SolverKind.PSO_OPT4J,box);
  }
	
	@Override
	protected void config() {
		 bindProblem(
			        PSOSolver_Creator.class, 
			        PSOSolver_Decoder.class, 
			        PSOSolver_Evaluator.class 
	     );
	}		
	
	
}
