package coral.solvers.search.opt4j.ga;

import com.google.inject.Inject;

import coral.solvers.rand.NumberGenerator;
import coral.solvers.search.opt4j.ctp.CTP_Creator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class GASolver_Creator extends CTP_Creator {

	@Inject
	public GASolver_Creator(CTP_Problem problem, NumberGenerator numberGenerator) {
		super(problem,numberGenerator);
	}
		
}
