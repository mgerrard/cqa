package coral.solvers.search.opt4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math.stat.descriptive.moment.StandardDeviation;
import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.optimizer.de.DifferentialEvolution;
import org.opt4j.optimizer.mopso.MOPSO;
import org.opt4j.optimizer.mopso.Particle;

import symlib.SymLiteral;

import com.google.inject.Inject;

import coral.solvers.Type;
import coral.solvers.Unit;
import coral.solvers.rand.Util;
import coral.solvers.search.opt4j.ctp.CTP_Evaluator;
import coral.solvers.search.opt4j.ctp.CTP_Problem;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.util.Config;

public class CoralterationListener implements OptimizerIterationListener {

  FitnessFunction fitness;
  CTP_Problem problem;

  @Inject
  public CoralterationListener(FitnessFunction fitness, CTP_Problem problem) {
    this.fitness = fitness;
    this.problem = problem;
  }

  @Override
  public void iterationComplete(Optimizer optimizer, int iteration) {
    iteration = iteration - 1; // in previous versions of opt4j the number
                               // started from 0 - now it starts at 1
    SymLiteral[] vars = problem.getVars();
    
//    if (optimizer instanceof MOPSO) {
//      MOPSO mopso = (MOPSO) optimizer;
//    Particle bestParticle = mopso.getCurrentBestIndividual();
//    Objectives bestParticleObjt = bestParticle.getBestObjectives();
//    Collection<Individual> indiv = mopso.getIndividuals();
//    } else if (optimizer instanceof DifferentialEvolution) {
//      DifferentialEvolution de = (DifferentialEvolution) optimizer;
//      
//    } else {
//      throw new RuntimeException("Something is wrong here...");
//    }
    
    if (iteration == Config.radianSearchLimit) { // change type of radian variables to double
      boolean[] varsToChange = new boolean[vars.length];
      Arrays.fill(varsToChange, false);

      for (Entry<SymLiteral, Type> entry : problem.getVars2Unit().entrySet()) {
        Type type = entry.getValue();
        if (Util.isRadian(type)) {
          type.setUnit(Unit.LIMITED_DOUBLE);
          int pos = 0;
          SymLiteral lit = entry.getKey();
          while (lit != vars[pos])
            pos++;
          varsToChange[pos] = true;
        }
      }
      // change bounds inside the genotype
      // TODO enabling this keeps coral from solving constraints 22 and 23 - why?
      // mopso.radianTypesToDouble(varsToChange);
    }
  }
}
