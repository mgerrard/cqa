package coral.solvers.search.opt4j.ctp;

import java.util.List;
import java.util.Map;

import org.opt4j.core.Individual;
import org.opt4j.core.problem.Creator;
import org.opt4j.genotype.DoubleBounds;
import org.opt4j.genotype.DoubleGenotype;

import symlib.SymLiteral;
import symlib.SymNumber;
import coral.solvers.Type;
import coral.solvers.Unit;
import coral.solvers.rand.NumberGenerator;
import coral.solvers.rand.Util;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.util.Config;
import coral.util.Interval;

public abstract class CTP_Creator implements Creator<DoubleGenotype> {

  private final CTP_Problem problem;
  private final NumberGenerator numberGenerator;
  private boolean firstElement = Config.insertZeroIndividual;
  List<Individual> lastSolutions;
  private int oldElementCounter = 0; //for the "recall"
  private int intervalElementsCounter = 0; // number of elements generated from the intervals returned by the
                                           // external solver

  public CTP_Creator(CTP_Problem problem, NumberGenerator numberGenerator) {
    this.problem = problem;
    this.numberGenerator = numberGenerator;
    lastSolutions = problem.getLastGoodAnswer();
    if(lastSolutions != null && lastSolutions.size() > 0) {
      firstElement = false;
      oldElementCounter = lastSolutions.size();
    } else if (Config.enableIntervalBasedSolver) {
      intervalElementsCounter = Config.maxInsertedIndividuals + 1; //first is always zero
    }
  }

  @Override
  public DoubleGenotype create() {
    DoubleGenotype dgen;

    double[] lowerBounds = problem.getLowerBounds();
    double[] upperBounds = problem.getUpperBounds();
    /**
     * in principle, [lower,upper]Bounds passed as argument to the constructor
     * are necessary!
     */
    dgen = new DoubleGenotype(new DoubleBounds(lowerBounds, upperBounds));

    initGenotype(dgen, lowerBounds, upperBounds);
    firstElement = false;
    oldElementCounter = oldElementCounter - 1; 
    intervalElementsCounter = intervalElementsCounter - 1; 
    return dgen;
  }

  private void initGenotype(DoubleGenotype dgen, double[] lowerBounds, double[] upperBounds) {
    int n = lowerBounds.length;
    
    for (int i = 0; i < n; i++) {
      double lo = lowerBounds[i];
      double hi = upperBounds[i];
      double value = 0;
      if (firstElement && lo <= 0 && hi >= 0) {
        value = 0.0;	        
      } else if (oldElementCounter > 0) {
        PCPhenotype lastSolPhen = (PCPhenotype) lastSolutions.get(oldElementCounter - 1).getPhenotype();
        Map<SymLiteral,SymNumber> values = lastSolPhen.getMap();
        SymLiteral var = problem.getVars()[i];
        
        //for some reason, the best particle has the complete phenotype (with the 
        //variables normally removed by optimization) 
        boolean varExists = values.containsKey(var); 
        
        if(!varExists) {
          value = numberGenerator.genDouble((int)lo, (int)hi);
        } else {
          value = values.get(var).evalNumber().doubleValue();
     
          //if var is of type sin/cos/etc., search the corresponding array index
          Type typeVar = problem.getVars2Unit().get(var);
          if(Util.isRadian(typeVar)) {
            int index = NumberGenerator.searchValueIndex(value,typeVar);
            
            //index not found - possibly the last run(s) took more than 100 iterations and
            //all types have been transformed into doubles
            if(index < 0) {
              //transform variable into double //TODO check if this is the best option            
              problem.transformTypeVar(var, Unit.LIMITED_DOUBLE);
            } else {
              value = index;
            }
          }        
        }
      } else if (intervalElementsCounter > 0) { 
        SymLiteral var = problem.getVars()[i];
        int id = var.getId();
        Map<Integer,Interval> intervals = problem.getIntervalSolverResults();
        if(intervals.containsKey(id)) {
          Interval iv = intervals.get(id);        
          value = numberGenerator.genDouble(iv.lo().doubleValue(), iv.hi().doubleValue());
        } else {
          value = numberGenerator.genDouble((int)lo, (int)hi);
        }
      } else {
        value = numberGenerator.genDouble((int)lo, (int)hi);
      }
      
      if (i >= dgen.size()) {
        dgen.add(value);
      } else {
        dgen.set(i, value);
      }
    }
  }
 
}