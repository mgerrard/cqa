package coral.solvers.search.opt4j.de;

import com.google.inject.Inject;

import coral.solvers.search.opt4j.ctp.CTP_Decoder;
import coral.solvers.search.opt4j.ctp.CTP_Problem;

public class DESolver_Decoder extends CTP_Decoder{

  @Inject
  public DESolver_Decoder(CTP_Problem problem) {
    super(problem);
  }

}
