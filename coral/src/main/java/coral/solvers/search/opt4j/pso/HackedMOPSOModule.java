package coral.solvers.search.opt4j.pso;

import org.opt4j.optimizer.mopso.MOPSOModule;
import org.opt4j.start.Constant;

import com.google.inject.Provides;

import coral.solvers.search.opt4j.CoralterationListener;
import coral.solvers.search.opt4j.RestartListener;
import coral.solvers.search.opt4j.fitness.FitnessFunction;
import coral.solvers.search.opt4j.fitness.SAW;
import coral.solvers.search.opt4j.fitness.SAWUpdateWeightListener;
import coral.util.Config;

public class HackedMOPSOModule extends MOPSOModule {

  FitnessFunction f;
  @Constant(value = "evalsPerRound")
  int nParticles = Config.nParticlesPSO - 1;
  @Constant(value = "stagnationLimit")
  int iterationsForRestart = Config.stagnationLimit;
  
  HackedMOPSOModule(FitnessFunction f) {
    super();
    this.f = f;
  }

  @Override
  protected void config() {
    super.config();
    addOptimizerIterationListener(CoralterationListener.class);
    if(f instanceof SAW) {
      addOptimizerIterationListener(SAWUpdateWeightListener.class);
    } else if(Config.stagnationLimit > 0) {
      addOptimizerIterationListener(RestartListener.class);
    }
  }

  public int getnParticles() {
    return nParticles;
  }

  public void setnParticles(int nParticles) {
    this.nParticles = nParticles;
  }
  
}