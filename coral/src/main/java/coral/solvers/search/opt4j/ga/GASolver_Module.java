package coral.solvers.search.opt4j.ga;

import java.util.Map;

import coral.PC;
import coral.solvers.SolverKind;
import coral.solvers.search.opt4j.ctp.CTP_Module;
import coral.util.Interval;

public class GASolver_Module extends CTP_Module {

  public GASolver_Module(PC pc, Map<Integer, Interval> box) {
    super(pc, SolverKind.GA_OPT4J, box);
  }
  
  public GASolver_Module(PC pc, PC originalPc, Map<Integer, Interval> box) {
    super(pc, originalPc, SolverKind.GA_OPT4J, box);
  }	
	
  @Override
  protected void config() {
    bindProblem(
        GASolver_Creator.class, 
        GASolver_Decoder.class, 
        GASolver_Evaluator.class 
        );
  }

}