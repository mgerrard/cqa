package coral.solvers.search.opt4j.fitness;

import coral.util.Config;
import symlib.SymBool;
import symlib.SymBoolOperations;
import symlib.SymDoubleRelational;
import symlib.SymIntRelational;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;

public class Util {

  /**
   * visita recursivamente as clausulas que compoem o OR e retorna o maior valor de fitness entre elas
   * @param constraint
   * @return double
   */
  public static double computeMaxFitnessOR(SymBool constraint) {
    double a1,a2;
    double r;
    if(isORConstraint(constraint)) {
      a1 = computeMaxFitnessOR(((SymBoolOperations) constraint).getA());
      if(a1 == 1) { //short circuit
        r = a1;
      } else {
        a2 = computeMaxFitnessOR(((SymBoolOperations) constraint).getB());
        r = Math.max(a1, a2);
      }
    } else {
      r = computeClause(constraint);
    }
    
    return r;
  }

  public static boolean isORConstraint(SymBool constraint) {
    return constraint instanceof SymBoolOperations
    && ((SymBoolOperations) constraint).getOp() == SymBoolOperations.OR;
  }

  public static double computeClause(SymBool constraint) {
    ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
    //TODO add support to compiled constraints
    double tempValue = revPol.eval()/*0 or 1*/.doubleValue();
    
    boolean c1 = tempValue != 1; 
    boolean c2 = constraint instanceof SymDoubleRelational && ((SymDoubleRelational)constraint).getOp() != SymDoubleRelational.NE;
    boolean c3 = constraint instanceof SymIntRelational && ((SymIntRelational)constraint).getOp() != SymIntRelational.NE; 
    if(c1 && (c2 || c3)) {
      double far = norm(Math.abs(revPol.getLeftSide().doubleValue() - revPol.getRightSide().doubleValue()));
      if(Double.isNaN(far)) { 
        tempValue = 0; 
      } else {
        far = far != 0 ? far : norm(1);
        tempValue = 1.0 - far;
      }
    }

    return tempValue;
  }
  
  public static double norm(double value){
    return value > Config.normValue ? 1 : value/Config.normValue;
  }


}
