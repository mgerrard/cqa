package coral.solvers.search.local;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import symlib.SymDoubleLiteral;
import symlib.SymFloatLiteral;
import symlib.SymIntLiteral;
import symlib.SymLiteral;
import symlib.SymLongLiteral;
import symlib.SymNumber;
import symlib.Util;
import avm.AVMSolutionFoundException;
import avm.PexAVM;
import coral.PC;
import coral.solvers.Env;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.Type;
import coral.tests.Helper;
import coral.util.Config;
import coral.util.Interval;
import coral.util.visitors.SymLiteralTypeSearcher;

public class AVMSolver extends Solver {

  public AVMSolver() {
    super(SolverKind.AVM);
  }

  private Env runSolver(PC pc, Guard guard,
      Map<Integer, Interval> intervalSolverResults) {

    FFAdapter ff = new FFAdapter(pc);
    Map<SymLiteral, Type> var2Units = new SymLiteralTypeSearcher(pc,Config.flexibleRange).getUnits();
    Set<SymLiteral> vars = pc.getSortedVars();
    Interval[] boxes = new Interval[vars.size()];
    
    int i = 0;
    for(SymLiteral lit : vars) {
      int id = lit.getId();
      Interval iv = intervalSolverResults.get(id);
      if (iv != null) {
        boxes[i] = iv;
      } else {
        Type t = var2Units.get(lit);
        boxes[i] = new Interval(t.getLo(),t.getHi());
      }
      i++;
    }

    PexAVM avm = new PexAVM(ff, vars.size(), boxes, Config.nIterationsAVM);
    double[] answer;
    answer = avm.search();

    Env env;
    if (answer != null && Helper.eval(pc, answer)) {

      env = new Env(null, Result.UNK, kind);
      env.setResult(Result.SAT);

      Map<SymLiteral, SymNumber> result = new HashMap<SymLiteral, SymNumber>();

      int j = 0;
      for (SymLiteral var : vars) {
        if (var instanceof SymDoubleLiteral) {
          result.put(var, Util.createConstant(answer[j]));
        } else if (var instanceof SymIntLiteral) {
          result.put(var, Util.createConstant((int) answer[j]));
        } else if (var instanceof SymLongLiteral) {
          result.put(var, Util.createConstant((long) answer[j]));
        } else if (var instanceof SymFloatLiteral) {
          result.put(var, Util.createConstant((float) answer[j]));
        } else {
          throw new RuntimeException("what?");
        }
        j++;
      }

      env.setMap(result);

    } else {
      env = new Env(null, Result.UNK, kind);
    }

//    System.out.println(env);
    return env;
  }

  @Override
  protected Env call(PC pc, Guard guard) {
    return runSolver(pc, guard, super.getBox());
  }

  @Override
  protected Env call(PC optimizedPc, PC originalPc, Guard guard) {
    return runSolver(optimizedPc, guard, super.getBox());
  }

  @Override
  protected Env recall(PC pc, Guard guard) {
    return runSolver(pc, guard, super.getBox());
  }

}
