package coral.solvers.search.local;

import java.util.Arrays;
import java.util.List;

import symlib.SymBool;
import symlib.SymBoolOperations;
import symlib.SymDoubleRelational;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;

import avm.AVMSolutionFoundException;
import avm.FitnessFunction;
import coral.PC;
import coral.util.Config;

public class FFAdapter implements FitnessFunction {
    private PC pc;
    private int numSat = 0;
    static boolean DEBUG = false;

    public FFAdapter(PC pc) {
      super();
      this.pc = pc;
    }
    
    @Override
    public double eval(double[] input) throws AVMSolutionFoundException {
      double tmp = coralFitness(pc, input);
      if (DEBUG) {
        System.out.printf("%f : %s \n", tmp, Arrays.toString(input));
      }
      return tmp;
    }

    public double max() {
      return pc.getConstraints().size();
    }
    
    /********************************************
     * @throws AVMSolutionFoundException 
     * @TODO 
     * this is a QUICKandDIRTY implementation of 
     * fitness function for AVM based on our SAW
     * implementation. It does not use weights.
     * This code needs to be refactored!
     * (merged with SAW)
     ********************************************/
    
    private double coralFitness(PC pc, double[] vector) throws AVMSolutionFoundException {

      int j = 0;
      for (SymLiteral lit : pc.getSortedVars()) {
        lit.setCte(vector[j]);
        j++;
      }

      numSat = 0;
      List<SymBool> constraints = pc.getConstraints();
      double fitnessValue = 0d;

      int len = constraints.size();
      for (int i = 0; i < len; i++) {
        SymBool constraint = constraints.get(i);
        double tempValue;
        try {
          if(isORConstraint(constraint)) {
            tempValue = computeMaxFitnessOR(constraint);
          } else {
            tempValue = computeClause(constraint);
          }
          fitnessValue += tempValue;
        } 
        catch (ArithmeticException _) { }
        catch (NullPointerException _) { System.out.println(constraint); }
      }

      if (len == numSat && fitnessValue != 0) {
        throw new AVMSolutionFoundException(vector);
      }

      return fitnessValue;
    }
    
    private static boolean isORConstraint(SymBool constraint) {
      return constraint instanceof SymBoolOperations
          && ((SymBoolOperations) constraint).getOp() == SymBoolOperations.OR;
    }

    private double computeMaxFitnessOR(SymBool constraint) {
      double a1,a2;
      double r;
      if(isORConstraint(constraint)) {
        a1 = computeMaxFitnessOR(((SymBoolOperations) constraint).getA());
        if(a1 == 1) { //short circuit
          r = a1;
        } else {
          a2 = computeMaxFitnessOR(((SymBoolOperations) constraint).getB());
          r = Math.max(a1, a2);
        }
      } else {
        r = computeClause(constraint);
      }

      return r;
    }

    private double computeClause(SymBool constraint) {
      ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
      //TODO add support to compiled constraints
      double tempValue = revPol.eval()/*0 or 1*/.doubleValue();

      numSat += (tempValue == 1 ? 1 : 0);  

      boolean c1 = tempValue != 1;
      boolean c2 = constraint instanceof SymDoubleRelational && ((SymDoubleRelational)constraint).getOp() != SymDoubleRelational.NE;
      boolean c3 = constraint instanceof SymIntRelational && ((SymIntRelational)constraint).getOp() != SymIntRelational.NE; 
      if(c1 && (c2 || c3)) {
        double far = norm(Math.abs(revPol.getLeftSide().doubleValue() - revPol.getRightSide().doubleValue()));
        if(Double.isNaN(far)) { 
          tempValue = 0; 
        } else {
          far = far != 0 ? far : norm(1);
          tempValue = 1.0 - far;
        }
      }

      return tempValue;
    }

    private static double norm(double value){
      return value > Config.normValue ? 1 : value/Config.normValue;
    } 

}
