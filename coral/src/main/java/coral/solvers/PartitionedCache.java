package coral.solvers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import symlib.SymBool;
import symlib.SymLiteral;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import coral.util.Config;
import coral.util.visitors.SymLiteralSearcher;
import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

//TODO merge it with the other version (used in regCoral)
public abstract class PartitionedCache {

  public PartitionedCache() {
  }

  static class PartitionResult {
    HashMultimap<String, SymBool> constraintsRelatedToAVar;
    Set<Set<String>> independentVarsClusters;

    public PartitionResult() {
      this.constraintsRelatedToAVar = HashMultimap.<String, SymBool> create();
      this.independentVarsClusters = Sets.newHashSet();
    }
    
    public PartitionResult(
        HashMultimap<String, SymBool> constraintsRelatedToAVar,
        Set<Set<String>> independentVarsClusters) {
      this.constraintsRelatedToAVar = constraintsRelatedToAVar;
      this.independentVarsClusters = independentVarsClusters;
    }
    
    public void addAll(PartitionResult result) {
      this.constraintsRelatedToAVar.putAll(result.constraintsRelatedToAVar);
      this.independentVarsClusters.addAll(result.independentVarsClusters);
    }
  }

  /***
   * partition one conjunction of constraints
   * 
   * @param conjuncts
   * @return
   */
  private PartitionResult partition(List<SymBool> conjuncts) {
    HashMultimap<String, SymBool> constraintsRelatedToAVar = HashMultimap
        .<String, SymBool> create();
    UndirectedSparseGraph<String, Integer> dependencyGraph = new UndirectedSparseGraph<String, Integer>();
    int edgeCounter = 0;
    for (SymBool constraint : conjuncts) {
      SymLiteralSearcher vis = new SymLiteralSearcher();
      constraint.accept(vis);
      ArrayList<String> vars = new ArrayList<String>();
      for (SymLiteral v : vis.getVars()) {
        vars.add(v.toString());
      }
      // ArrayList<String> vars = new
      // ArrayList<String>(constraint.getVarsSet());
      for (int i = 0; i < vars.size(); i++) {
        constraintsRelatedToAVar.put(vars.get(i), constraint);
        // Notice j=i to add self-dependency
        for (int j = i; j < vars.size(); j++) {
          dependencyGraph.addEdge(edgeCounter++, vars.get(i), vars.get(j));
        }
      }
    }
    WeakComponentClusterer<String, Integer> clusterer = new WeakComponentClusterer<String, Integer>();
    Set<Set<String>> clusters = clusterer.transform(dependencyGraph);
    ImmutableSet<Set<String>> independentVarsClusters = ImmutableSet
        .<Set<String>> copyOf(clusters);
    return new PartitionResult(constraintsRelatedToAVar,
        independentVarsClusters);
  }

  LoadingCache<Set<SymBool>, Env> cache = CacheBuilder.newBuilder()
      .maximumSize(10000).recordStats()
      .build(new CacheLoader<Set<SymBool>, Env>() {
        public Env load(Set<SymBool> problem) throws Exception {
          return processConstraint(problem);
        }
      });

  public void clearCache() {
    System.out.println("Hit rate:         " + cache.stats().hitRate());
    System.out.println("Hit count:        " + cache.stats().hitCount());
    System.out.println("Miss count:       " + cache.stats().missCount());
    System.out.println("Avg load penalty: "
        + cache.stats().averageLoadPenalty());
    System.out.println("Evictions:        " + cache.stats().evictionCount());
    cache.invalidateAll();
  }

  /***
   * run the analysis (e.g., SAT solving, model counting, volume computation,
   * etc.) on a disjunction of related constraints provided on input
   * 
   * @param disjuncts
   * @return
   * @throws Exception 
   */

  public Env analyzeOne(List<SymBool> conjunct) throws Exception {
    PartitionResult partitionResult = partition(conjunct);
    Env finalEnv = null;
    
    for (Set<String> cluster : partitionResult.independentVarsClusters) {
      HashMultimap<String, SymBool> locallyRelatedConstraints = HashMultimap
          .<String, SymBool> create();
      for (SymBool symBool : conjunct) {
        SymLiteralSearcher vis = new SymLiteralSearcher();
        symBool.accept(vis);
        for (SymLiteral v : vis.getVars()) {
          locallyRelatedConstraints.put(v.toString(), symBool);
        }
      }

      Set<SymBool> problem = new HashSet<SymBool>();
      for (String var : cluster) {
        problem.addAll(locallyRelatedConstraints.get(var));
      }
      // TODO this is inefficient, but jus to check the results
      problem = Sets.intersection(problem, Sets.newHashSet(conjunct));
      Env cached = null;
      if (Config.cacheSolutions) {
        try {
          cached = cache.get(problem);
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
      } else {
        cached = processConstraint(problem);      
      }
      
      //if one partition is unsat/unk, bail out
      if (Config.stopIfPartitionUnsat) {
        if (cached == null) {
          return new Env(Result.UNK, null);
        } else if (cached.getResult() != Result.SAT) {
          return cached;
        }
      }
      
      if (finalEnv == null) {
        finalEnv = cached;
      } else {
        finalEnv = Env.combineEnv(finalEnv, cached, cached.getSolverKind());
      }
    }
    return finalEnv;
  }
  
  public abstract Env processConstraint(Set<SymBool> pc) throws Exception;
}
