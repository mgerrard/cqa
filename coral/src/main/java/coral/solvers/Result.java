package coral.solvers;

public enum Result {
  SAT,
  UNSAT,
  UNK //UNKNOWN
}
