package coral.solvers;

public enum Unit {

  RADIANS, 
  DEGREES, 
  ASIN_ACOS, 
  ATAN,   
  LIMITED_INT, 
  LIMITED_DOUBLE, 
  LIMITED_FLOAT,
  LIMITED_BOOLEAN, 
  LIMITED_LONG,
  DOUBLE; 
  
}
