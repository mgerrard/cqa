package coral.solvers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import coral.PC;
import coral.simplifier.DP_Eq;
import coral.simplifier.Rewrite;
import coral.solvers.search.opt4j.PCPhenotype;
import coral.util.Config;
import coral.util.Interval;
import coral.util.callers.IntervalSolverCaller;

public abstract class Solver {

  protected SolverKind kind;
  private Map<Integer,Interval> box; //interval solver's results
  
  protected Solver(SolverKind kind) {
    this.kind = kind;
  }

  public SolverKind getKind() {
    return kind;
  }

  public Callable<Env> getCallable(final PC pc) {
    // creating task
    return new Callable<Env>() {
      public Env call() throws Exception {
        
        if(Config.enableIndividualStoring && Config.storeLastIndividuals) {
          throw new Error("Enable solution storage OR last solution storage - not the two at the same time!");
        }
        
        if(Config.enableIntervalBasedSolver && Config.toggleValueInference) {
          //disable value inference 
          Config.toggleValueInference = false;
        }
        
        PC canPc = pc;
        
        if(Config.pcCanonicalization) {
          canPc = pc.getCanonicalForm();
        }

//        if(Config.cacheSolutions) {
//          String pcString = canPc.toString();
//          Env cachedEnv = Config.cache.queryCache(pcString);
//          
//          if(cachedEnv != null) { //cache hit, job's done
////            System.out.println("Cache hit for canPc:" + pcString);
//            return cachedEnv;
//          }
//        }
        
        /*********************************************
         * Rewriting rules
         *********************************************/
        PC tmpPC = Config.removeSimpleEqualities ?
            Rewrite.rew(canPc) : canPc;
            
        /*********************************************
         * equality decision procedure simplifies the 
         * constraint before calling solver 
         *********************************************/
        PC finalPc;
        Env dpEnv;
        if(Config.removeSimpleEqualities) {
          Object[] partialInfo = DP_Eq.removeSimpleEqualities(tmpPC);
          finalPc = (PC) partialInfo[0];
          dpEnv = (Env) partialInfo[1];     
          box = ((Map<Integer, Interval>) partialInfo[2]);
        } else  {
          finalPc = canPc;
          dpEnv = new Env(null,kind);
        }
        
      if (Config.enableIntervalBasedSolver && !Config.simplifyUsingIntervalSolver) { 
        //box == null; interval solver wasn't used yet
        IntervalSolverCaller isolver = coral.solvers.rand.Util.getIntervalSolver(Config.intervalSolver);
        box = (isolver.callSolver(finalPc, Config.intervalSolverTimeout, Config.intervalSolverPrecision));
      } else if (!Config.enableIntervalBasedSolver) {
        box = new TreeMap();
      }

        // call solver
        Env solverEnv;
        if(Config.removeSimpleEqualities) {
//          System.out.println("ORIGINALPC:" + canPc);
//          System.out.println("\nPROCESSEDPC:" + finalPc);
//          System.out.println("\nENV:" + dpEnv);
          solverEnv = Solver.this.call(finalPc,canPc);
        } else {
          solverEnv = Solver.this.call(finalPc);
        }

        // combine solutions
        Env finalEnv;
        if(solverEnv != null /* && solverEnv.getResult() == Result.SAT */) {
          finalEnv = Env.combineEnv(solverEnv, dpEnv, kind);
        } else {
          finalEnv = solverEnv;
        }

        if(finalEnv != null) {
          if(finalEnv.getResult() == Result.SAT && !isSolution(canPc, finalEnv)) { 
            // Two possibilities here: an error has occurred in the rewrite, or
            // the rewritten constraint is mathematically right but the reported
            // answer is not valid in the original constraint (floating-point semantics,
            // etc). Either way, we will execute again the solver with the original pc. 
//            System.out.println("\nRecalling for pc:" + canPc);
//            System.out.println("Original solution: " + finalEnv);
            finalEnv = Solver.this.recall(canPc,finalEnv);
//            System.out.println("Recalled env:" + finalEnv);
            
//            finalEnv.setResult(Result.UNK); //set solution as UNK
//            System.err.println("## WARNING: a wrong solution has been reported. please contact the developers.");
//            System.err.println("##        : original constraint: " + canPc.toString());
//            System.err.println("##        : rewritten constraint: " + finalPc.toString());
           
            if(finalEnv == null || finalEnv.getResult() == Result.UNK || !isSolution(canPc, finalEnv)) { //check one last time
              finalEnv = new Env(Result.UNK,kind); //solution not found. boo!
            } else {
//              System.out.println("Recalling successfull!");
            }
          }
        } else {
          finalEnv = new Env(Result.UNK, Solver.this.getKind()); //create a dummy env
        }
        
        finalEnv.setPC(canPc);
        
//        if(Config.cacheSolutions && finalEnv.getResult()==Result.SAT) {
//          //put this solution into the cache
//          String pcName = canPc.toString();
//          Config.cache.cacheSolvedConstraint(pcName,finalEnv);
//        }
        
        return finalEnv;
      }
    };
  }

/**
 * used only for concurrent execution  
 */
//  public static ExecutorService executorService;
//  public Future<Env> solve(final PC pc) {    
//    Callable<Env> callable = getCallable(pc);
//    if (executorService == null) {
//      executorService = Executors.newFixedThreadPool(Config.numThreads);
//    }
//    return executorService.submit(callable);
//  }

  /**
   * coordination to stop the solver
   */
  private boolean pleaseStop;

  public void setPleaseStop(boolean arg) {
    pleaseStop = arg;
  }

  public boolean getPleaseStop() {
    return pleaseStop;
  }

  /**
   * this is necessary to enforce the use
   * of a single guard for solvers that
   * encapsulate others (e.g., hybrid).
   */
  public static interface Guard {
    boolean shouldStop();
  }

  protected Guard getGuard() {
    return new Guard() { 
      public boolean shouldStop() { 
        return pleaseStop; 
      }; 
    };
  }

  /**   
   * @return name of the constraint solver
   */
  public String getName() {
    return getKind().toString();
  }

  public void setBox(Map<Integer,Interval> box) {
    this.box = box;
  }

  public Map<Integer,Interval> getBox() {
    return box;
  }

  /**
   * calls the constraint solver algorithm
   *
   * @param pc
   * @return an assignment to the constraint in
   * input variable pc, if one can be found.
   */
  public Env call(PC pc) {
    Env env;
    
    if (pc.getVars().size() == 0) {
      Result result = pc.eval() ? Result.SAT : Result.UNSAT; 
      env = new Env(result, kind);
    } else {
      env = call(pc, getGuard());
    }
    
    return env;
  }
  
/*
 * version of call() for use with optimized constraints; the original pc
 * is required for proper operation of the cache 
 */
  public Env call(PC optimizedPc, PC originalPC) {
    Env env;
    
    if (optimizedPc.getVars().size() == 0) {
//      System.out.println("##SOLVED WITH FILTER (BY DP)");
      Result result = optimizedPc.eval() ? Result.SAT : Result.UNSAT; 
      env = new Env(result, kind);
    } else {
      env = call(optimizedPc,originalPC, getGuard());
    }
    
    return env;
  }
  
  abstract protected Env call(PC pc, Guard guard);

  abstract protected Env call(PC optimizedPc, PC originalPc, Guard guard);
  
  // we need the old env because the optimized constraint can have
  // less variables than the original one
  
  public Env recall(PC pc, Env env) {
    return recall(pc, getGuard());
  }
  
  abstract protected Env recall(PC pc, Guard guard);
  
  protected Env isSolution(PC pc, PCPhenotype p) {
    Env result = null;
    for (Map.Entry<SymLiteral, SymNumber> entry : p.getMap().entrySet()) {
      entry.getKey().setCte(entry.getValue().evalNumber());
    }
    List<SymBool> constraints = pc.getConstraints();
    boolean solved = true;
    for (int i = 0; i < constraints.size(); i++) {
      SymBool constraint = constraints.get(i);
      try {
        ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
        //TODO add support to compiled constraints
        if (revPol.eval().intValue() == 0) {
          solved = false;
          break;
        }
      } 
      catch (ArithmeticException _) { }
      catch (NullPointerException _) { System.out.println(constraint); }
    }
    if (solved) {
      result = new Env(p.getMap(), Result.SAT, kind);
    }
    return result;
  }
  
  protected boolean isSolution(PC pc, Env p) {
    for (Map.Entry<SymLiteral, SymNumber> entry : p.getMapLiteralToNumber().entrySet()) {
      entry.getKey().setCte(entry.getValue().evalNumber());
    }
    List<SymBool> constraints = pc.getConstraints();
    boolean solved = true;
    for (int i = 0; i < constraints.size(); i++) {
      SymBool constraint = constraints.get(i);
      try {
        ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
        //TODO add support to compiled constraints
        if (revPol.eval().intValue() == 0) {
          solved = false;
          break;
        }
      } 
      catch (ArithmeticException _) { }
      catch (NullPointerException _) { System.out.println(constraint); }
    }
    
    return solved;
  }

  public boolean isTerminated() {
    return false;
  }
}