package coral.solvers;

import java.util.HashMap;
import java.util.Map;

import symlib.SymLiteral;
import symlib.SymNumber;
import symlib.Util;
import symlib.eval.GenReversePolishExpression;
import coral.PC;
import coral.util.visitors.SymLiteralReplacer;

public class Env {

  /**
   * assignment of variables to values
   */
  private Map<SymLiteral, SymNumber> mapLiteralToNumber;

  /**
   * goal constraint for this assignment
   */
  private PC pc;

  /**
   * label to signal whether or not this candidate
   * solution satisfies or no the path condition  
   */
  private Result result;

  /**
   * label to identify which solver generated this 
   * candidate solution
   */
  private SolverKind solverKind;

  public Env(
      Map<SymLiteral, SymNumber> mapLiteralToNumber, 
      Result result, 
      SolverKind kind) {
    this.mapLiteralToNumber = mapLiteralToNumber;
    this.result = result;
    this.solverKind = kind;
  }

  public Env(Result result, SolverKind kind) {
    this(new HashMap<SymLiteral, SymNumber>(), result, kind);
  }

  public SolverKind getSolverKind() {
    return solverKind;
  }

  public void addKeyValue(SymLiteral key, SymNumber value) {
    this.mapLiteralToNumber.put(key, value);
  }

  public SymNumber getValue(SymLiteral key) {
    return key == null ? null : mapLiteralToNumber.get(key);
  }

  public void setMap(Map<SymLiteral, SymNumber> map) {
    mapLiteralToNumber = map;
  }

  public Map<SymLiteral, SymNumber> getMapLiteralToNumber() {
    return mapLiteralToNumber;
  }

  public Result getResult() {
    return result;
  }

  public void setResult(Result result) {
    this.result = result;
  }

  public PC getPC() {
    return pc;
  }

  public void setPC(PC pc) {
    this.pc = pc;
  }

  /**
   * this method combines two candidate solutions in one.  this 
   * can be used to communicate solutions of solvers that operate
   * in distinct partitions of the constraint system. the method
   * assumes that the literals in the two environments are distinct
   * 
   * @param baseEnv
   * @param dpEnv
   * @param kind
   * @return a combined environment
   */
  public static Env combineEnv(Env baseEnv, Env dpEnv, SolverKind kind) {
    if (baseEnv == null && dpEnv == null) {
      throw new RuntimeException("how come???");
    }    
    Env result;   
    if (baseEnv == null) {
      result = dpEnv;
      result.setResult(Result.UNK);
      result.solverKind = kind;
    } else if (dpEnv == null) {
      result = baseEnv;
    } else {
      // combine maps:      
      Map<SymLiteral, SymNumber> tmpMap = baseEnv.getMapLiteralToNumber();
      for (Map.Entry<SymLiteral, SymNumber> entry : dpEnv.getMapLiteralToNumber().entrySet()) {
        SymLiteral key = entry.getKey();
        SymNumber value = entry.getValue();
        
        //TODO: chance of doing this several times!
        // replace environment
        SymLiteralReplacer replacer = new SymLiteralReplacer(baseEnv);
        value = replacer.visitSymNumber(value);

        //TODO add support to compiled constraints
        Number concreteVal = GenReversePolishExpression.createReversePolish(value).eval();
        tmpMap.put(key, Util.createConstant(concreteVal));
      }
      result = new Env(tmpMap, baseEnv.getResult(), kind);
    }    
    return result;
  }


  public String toString() {
    if(mapLiteralToNumber == null) {
      return "Empty env";
    } else {
      return mapLiteralToNumber.toString();
    }
  }

}