package coral.solvers;

import coral.solvers.rand.RandomSolver;

public enum SolverKind {
   
  RANDOM, PSO_OPT4J, GA_OPT4J, REVERSE_PSO, FILTERED_PSO, AVM, DE_OPT4J;
  
  public Solver get() {
    Solver solver = null;
    switch (this) {
    case RANDOM:
      solver = new RandomSolver();
      break;
    case GA_OPT4J:
    	solver = new coral.solvers.search.opt4j.ga.GASolver();
    	break;
    case PSO_OPT4J:
    	solver = new coral.solvers.search.opt4j.pso.PSOSolver();
    	break;
    case AVM:
      solver = new coral.solvers.search.local.AVMSolver();
      break;
    case DE_OPT4J:
      solver = new coral.solvers.search.opt4j.de.DESolver();
      break;
    default:
      throw new UnsupportedOperationException("invalid kind of solver");
    }
    return solver;
  }

};

