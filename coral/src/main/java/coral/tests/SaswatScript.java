package coral.tests;

import symlib.parser.Parser;
import coral.PC;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.util.Config;
import coral.util.options.Options;

public class SaswatScript {
  
  private static Options options = new Options(Config.class);

  public static void main(String[] args) throws Exception {
    
    Config.load(args,options);
    Config.printOptions(options);
    
    String[] constraints = Helper.read("allcons"); 
    int nConstraints = constraints.length;
    boolean[][] tableResults = new boolean[nConstraints][6];
    int[] scores = new int[6];
    
    //disable cache
    Config.cacheSolutions = false;

    System.out.println("Solver order: \n PSO RANDOM PSO+REALPAVER RANDOM+REALPAVER PSO+ICOS RANDOM+ICOS");

    int c = 0;
    for (String cons : constraints) {
      PC pc = (new Parser(cons)).parsePC();
      pc = pc.getCanonicalForm();

      tableResults[c][0] = solveWithPsoOnly(pc);
      tableResults[c][1] = solveWithRandomOnly(pc);
      tableResults[c][2] = solveWithPsoPlusRealPaver(pc);
      tableResults[c][3] = solveWithRandomPlusRealPaver(pc);
      tableResults[c][4] = solveWithPsoPlusIcos(pc);
      tableResults[c][5] = solveWithRandomPlusIcos(pc);

      for (int i = 0; i < 6; i++) {
        scores[i] += tableResults[c][i] ? 1 : 0;
        System.out.print(tableResults[c][i] ? '.' : 'x');
      }
      System.out.println();      
      c++;
    }

    System.out.println("------------------------");
    System.out.println("Total for each solver:");
    System.out.println("PSO: " + scores[0]);
    System.out.println("RANDOM: " + scores[1]);
    System.out.println("PSO+RealPaver: " + scores[2]);
    System.out.println("RANDOM+RealPaver: " + scores[3]);
    System.out.println("PSO-Icos: " + scores[4]);
    System.out.println("RANDOM-Icos: " + scores[5]);
  
  }
  
  public static boolean solveWithPsoOnly(PC pc) throws Exception  {
    Config.enableIntervalBasedSolver = false;
    Config.toggleValueInference = true;
    
    Solver solver = SolverKind.PSO_OPT4J.get();
        return solver.getCallable(pc).call().getResult() == Result.SAT;
  }
  
  public static boolean solveWithRandomOnly(PC pc) throws Exception  {
    Config.enableIntervalBasedSolver = false;
    Config.toggleValueInference = true;
    
    Solver solver = SolverKind.RANDOM.get();
        return solver.getCallable(pc).call().getResult() == Result.SAT;
  }
  public static boolean solveWithPsoPlusRealPaver(PC pc) throws Exception  {
    Config.enableIntervalBasedSolver = true;
    Config.toggleValueInference = false;
    Config.intervalSolver = "realpaver";
    
    Solver solver = SolverKind.PSO_OPT4J.get();
        return solver.getCallable(pc).call().getResult() == Result.SAT;
  }
  public static boolean solveWithRandomPlusRealPaver(PC pc) throws Exception  {
  Config.enableIntervalBasedSolver = true;
  Config.toggleValueInference = false;
  Config.intervalSolver = "realpaver";
    
    Solver solver = SolverKind.RANDOM.get();
        return solver.getCallable(pc).call().getResult() == Result.SAT;
  }
  public static boolean solveWithPsoPlusIcos(PC pc) throws Exception  {
    Config.enableIntervalBasedSolver = true;
    Config.toggleValueInference = false;
    Config.intervalSolver = "icos";

    Solver solver = SolverKind.PSO_OPT4J.get();
        return solver.getCallable(pc).call().getResult() == Result.SAT;
  }
  public static boolean solveWithRandomPlusIcos(PC pc) throws Exception  {
    Config.enableIntervalBasedSolver = true;
    Config.toggleValueInference = false;
    Config.intervalSolver = "icos";
    
    Solver solver = SolverKind.RANDOM.get();
    return solver.getCallable(pc).call().getResult() == Result.SAT;
  }
  
  
  
}
