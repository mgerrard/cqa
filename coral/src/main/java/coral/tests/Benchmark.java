package coral.tests;

public class Benchmark {
  // sin(x) + cos(y) > 1.0
  public static final String pc1 = "DGT(ADD(SIN_(DVAR(ID_1)),COS_(DVAR(ID_2))),DCONST(1.0))"; 
  // sin(x) - cos(y) < 0.0000000001
  public static final String pc2 = "DLT(SUB(SIN_(DVAR(ID_1)),COS_(DVAR(ID_2))),DCONST(0.0000000001))";
  // sin(x) - cos(y) == 0.0
  public static final String pc3 = "DEQ(SUB(SIN_(DVAR(ID_1)),COS_(DVAR(ID_2))),DCONST(0.0))";
  // exp(x) > 0.0
  public static final String pc4 =  "DGT(EXP_(DVAR(ID_1)),DCONST(0.0))";
  // sin A + sin B + sin C = 4 * cos A * cos B * cos C
  public static final String pc5 = "DEQ(ADD(ADD(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2))),SIN_(DVAR(ID_3))),MUL(MUL(MUL(COS_(DVAR(ID_1)),COS_(DVAR(ID_2))),COS_(DVAR(ID_3))),DCONST(4.0)))";
  // cos A + cos B + cos C > 4 sin A/2 sin B/2 sin C/2 
  public static final String pc6 = "DGT(ADD(ADD(COS_(DVAR(ID_1)),COS_(DVAR(ID_2))),COS_(DVAR(ID_3))),MUL(MUL(MUL(SIN_(DIV(DVAR(ID_1),DCONST(2.0))),SIN_(DIV(DVAR(ID_2),DCONST(2.0)))),SIN_(DIV(DVAR(ID_3),DCONST(2.0)))),DCONST(4.0)))"; 
  // (sin(2x - y)/(cos(2y + y) + 1) = cos(2z + x)/(sin(2w + y) - 1)  
  public static final String pc7 = "DEQ(DIV(SIN_(SUB(MUL(DVAR(ID_1),DCONST(2.0)),DVAR(ID_2))),ADD(COS_(ADD(MUL(DVAR(ID_2),DCONST(2.0)),DVAR(ID_2))),DCONST(1.0))),DIV(COS_(ADD(MUL(DVAR(ID_3),DCONST(2.0)),DVAR(ID_1))),ADD(SIN_(ADD(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_2))),DCONST(1.0))))";
  // cos(3x+2y-z) * sin(z+x+y) == cos(z*x*y)
  public static final String pc8 = "DEQ(MUL(COS_(SUB(ADD(MUL(DCONST(3.0),DVAR(ID_1)),MUL(DCONST(2.0),DVAR(ID_2))),DVAR(ID_3))),SIN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))),COS_(MUL(MUL(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3))))";
  // sin(cos(x*y)) < cos(sin(x*y))
  public static final String pc9 = "DLT(SIN_(COS_(MUL(DVAR(ID_1),DVAR(ID_2)))),COS_(SIN_(MUL(DVAR(ID_1),DVAR(ID_2)))))";
  // sin(x*cos(y*sin(z))) > cos(x*sin(y*cos(z)))
  public static final String pc10 = "DGT(SIN_(MUL(DVAR(ID_1),COS_(MUL(DVAR(ID_2),SIN_(DVAR(ID_3)))))),COS_(MUL(DVAR(ID_1),SIN_(MUL(DVAR(ID_2),COS_(DVAR(ID_3)))))))";
  // asin(x) < cos(y)*cos(z) - atan(w)
  public static final String pc11 = "DLT(ASIN_(DVAR(ID_1)),SUB(MUL(COS_(DVAR(ID_2)),COS_(DVAR(ID_3))),ATAN_(DVAR(ID_4))))";
  // (asin(x) * asin(y))-1 < atan(z) * atan(w)  
  public static final String pc12 = "DLT(SUB(MUL(ASIN_(DVAR(ID_1)),ASIN_(DVAR(ID_2))),DCONST(1.0)),MUL(ATAN_(DVAR(ID_3)),ATAN_(DVAR(ID_4))))";
  // sin(y) * asin(x) - 300 < cos(y)*cos(z) - atan(w)
  public static final String pc13 = "DLT(MUL(SIN_(DVAR(ID_3)),ASIN_(DVAR(ID_1))),SUB(MUL(COS_(DVAR(ID_2)),COS_(DVAR(ID_3))),ATAN_(DVAR(ID_4))))";
  // sin(y) * asin(x) - 300 < cos(y)*cos(z) - atan(w)
  public static final String pc14 = "DLT(SUB(MUL(SIN_(DVAR(ID_3)),ASIN_(DVAR(ID_1))),DCONST(300.0)),SUB(MUL(COS_(DVAR(ID_2)),COS_(DVAR(ID_3))),ATAN_(DVAR(ID_4))))";
  // ((asin(1) * asin(cos(9*57)))-1) < (atan(0) * atan(0)) solução; x=1,y=513,z=0,w=0
  public static final String pc15 = "DLT(SUB(MUL(ASIN_(DVAR(ID_1)),ASIN_(COS_(DVAR(ID_2)))),DCONST(1.0)),MUL(ATAN_(DVAR(ID_3)),ATAN_(DVAR(ID_4))))";
  // ((((tan_(($V4-$V1))*cos_(sin_(($V4/$V5))))-atan_((($V2+20.0)+$V3)))+asin_(($V2-15.0))) < ((sin_(($V4*$V4))*cos_((($V1*$V4)*$V5)))-tan_((cos_((($V1*$V4)*$V1))+sin_($V4)))))  
  public static final String pc16 = "DLT(ADD(SUB(MUL(TAN_(SUB(DVAR(ID_4),DVAR(ID_1))),COS_(SIN_(DIV(DVAR(ID_4),DVAR(ID_5))))),ATAN_(ADD(ADD(DVAR(ID_2),DCONST(20)),DVAR(ID_3)))),ASIN_(SUB(DVAR(ID_2),DCONST(15)))),SUB(MUL(SIN_(MUL(DVAR(ID_4),DVAR(ID_4))),COS_(MUL(MUL(DVAR(ID_1),DVAR(ID_4)),DVAR(ID_5)))),TAN_(ADD(COS_(MUL(MUL(DVAR(ID_1),DVAR(ID_4)),DVAR(ID_1))),SIN_(DVAR(ID_4),DVAR(ID_5)))))";
  // asin(x) * acos(x) < atan(x)  
  public static final String pc17 = "DLT(MUL(ASIN_(DVAR(ID_1)),ACOS_(DVAR(ID_1))),ATAN_(DVAR(ID_1)))";
  // (1+acos(x)) < asin(x)
  public static final String pc18 = "DLT(ADD(DCONST(1.0),ACOS_(DVAR(ID_1))),ASIN_(DVAR(ID_1)))";
  // 3*acos(x) < atan(y) + asin(w)
  public static final String pc19 = "DLT(MUL(DCONST(3.0),ACOS_(DVAR(ID_1))),ADD(ATAN_(DVAR(ID_2)),ASIN_(DVAR(ID_3))))";
  // sin(sin((x*y)) < 0 && cos(2x) > 0.25
  public static final String pc20 = "DLT(SIN_(SIN_(MUL(DVAR(ID_1),DVAR(ID_2)))),DCONST(0.0));DGT(COS_(MUL(DVAR(ID_1),DCONST(2.0))),DCONST(0.25))";
  // cos(x*y) < 0 && sin(2x) > 0.25
  public static final String pc21 = "DLT(COS_(MUL(DVAR(ID_1),DVAR(ID_2))),DCONST(0.0));DGT(SIN_(MUL(DVAR(ID_1),DCONST(2.0))),DCONST(0.25))";
  // (sin_(cos_(($V1*$V2))) < cos_(sin_(($V2*$V3)))) & 
  // ((sin_((($V4*2.0)-$V2))/(cos_((($V6*2.0)+$V7))+1.0)) == (cos_((($V3*2.0)+$V1))/(sin_((($V4*2.0)+$V5))+1.0))))
  public static final String pc22 = 
    "DLT(SIN_(COS_(MUL(DVAR(ID_1),DVAR(ID_2)))),COS_(SIN_(MUL(DVAR(ID_2),DVAR(ID_3)))));" +
    "DEQ(DIV(SIN_(SUB(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_2)))," +
    "ADD(COS_(ADD(MUL(DVAR(ID_6),DCONST(2.0)),DVAR(ID_7))),DCONST(1.0)))," +
    "DIV(COS_(ADD(MUL(DVAR(ID_3),DCONST(2.0)),DVAR(ID_1)))," +
    "ADD(SIN_(ADD(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_5)))," +
    "DCONST(1.0))))";
  // (sin(2x - y)/(cos(2y + x) + 1) = cos(2z + x)/(sin(2w + y) - 1) &
  // sin(x*y*z*w) > 0 &
  // cos(x*y*z*w) < 0
  public static final String pc23 =
    "DEQ(DIV(SIN_(SUB(MUL(DVAR(ID_1),DCONST(2.0)),DVAR(ID_2))),ADD(COS_(ADD(MUL(DVAR(ID_2),DCONST(2.0)),DVAR(ID_1))),DCONST(1.0))),DIV(COS_(ADD(MUL(DVAR(ID_3),DCONST(2.0)),DVAR(ID_1))),SUB(SIN_(ADD(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_2))),DCONST(1.0))));" +
    "DGT(SIN_(MUL(MUL(MUL(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)),DVAR(ID_4))),DCONST(0.0));" +
    "DLT(COS_(MUL(MUL(MUL(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)),DVAR(ID_4))),DCONST(0.0))";
  public static final String pc24 =
    "DLT(SIN_(COS_(MUL(DVAR(ID_1),DVAR(ID_2)))),COS_(SIN_(MUL(DVAR(ID_1),DVAR(ID_2)))));" +
    "DEQ(DIV(SIN_(SUB(MUL(DVAR(ID_1),DCONST(2.0)),DVAR(ID_2))),ADD(COS_(ADD(MUL(DVAR(ID_2),DCONST(2.0)),DVAR(ID_2))),DCONST(1.0)))," +
    "DIV(COS_(ADD(MUL(DVAR(ID_3),DCONST(2.0)),DVAR(ID_1))),ADD(SIN_(ADD(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_2))),DCONST(1.0))))";
  //  sin(cos(x*y)) < cos(sin(x*z)) &
  // (sin(2w - y)/(cos(2y + v) + 1) = cos(2z + x)/(sin(2w + v) - 1)
  public static final String pc25 = 
    "DLT(SIN_(COS_(MUL(DVAR(ID_1),DVAR(ID_2)))),COS_(SIN_(MUL(DVAR(ID_1),DVAR(ID_3)))));" +
    "DEQ(DIV(SIN_(SUB(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_2))),ADD(COS_(ADD(MUL(DVAR(ID_2),DCONST(2.0)),DVAR(ID_5))),DCONST(1.0))),DIV(COS_(ADD(MUL(DVAR(ID_3),DCONST(2.0)),DVAR(ID_1))),ADD(SIN_(ADD(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_5))),DCONST(1.0))))";
  // sin(cos(x*y)) < cos(sin(x*z)) 
  // (sin(2w - y)/(cos(2y + v) + 1) = cos(2z + x)/(sin(2w + v) - 1) 
  // sin(x*y*z*w) > 0 && cos(x*y*z*w) < 0
  public static final String pc26 = 
    "DLT(SIN_(COS_(MUL(DVAR(ID_1),DVAR(ID_2)))),COS_(SIN_(MUL(DVAR(ID_1),DVAR(ID_3)))));" +
    "DEQ(DIV(SIN_(SUB(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_2))),ADD(COS_(ADD(MUL(DVAR(ID_2),DCONST(2.0)),DVAR(ID_5))),DCONST(1.0))),DIV(COS_(ADD(MUL(DVAR(ID_3),DCONST(2.0)),DVAR(ID_1))),ADD(SIN_(ADD(MUL(DVAR(ID_4),DCONST(2.0)),DVAR(ID_5))),DCONST(1.0))));" +
    "DGT(SIN_(MUL(MUL(MUL(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)),DVAR(ID_4))),DCONST(0.0));" +
    "DLT(COS_(MUL(MUL(MUL(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)),DVAR(ID_4))),DCONST(0.0))";
  // sin(x*cos(y*sin(z))) > cos(x*sin(y*cos(z))) && sin(cos(x*y)) < cos(sin(x*y))
  public static final String pc27 = 
    "DGT(SIN_(MUL(DVAR(ID_1),COS_(MUL(DVAR(ID_2),SIN_(DVAR(ID_3)))))),COS_(MUL(DVAR(ID_1),SIN_(MUL(DVAR(ID_2),COS_(DVAR(ID_3)))))));" +
    "DLT(SIN_(COS_(MUL(DVAR(ID_1),DVAR(ID_2)))),COS_(SIN_(MUL(DVAR(ID_1),DVAR(ID_2))))";
  // log($V1) == 2.0
  public static final String pc28 = "DEQ(LOG_(DVAR(ID_1)),DCONST(2.0))";
  // 
  public static final String pc29 = "DGT(EXP_(DVAR(ID_1)),DCONST(5.0))";
  // log_10($v1) == 2.0
  public static final String pc30 = "DEQ(LOG10_(DVAR(ID_1)),DCONST(2))";
  // 
  public static final String pc31 = "DGT(ROUND_(DVAR(ID_1)),DCONST(5.0))";
  // 
  public static final String pc32 = "DGT(SQRT_(DVAR(ID_1)),DCONST(5.0))";
  // sqrt(sin($V1)) > sqrt(cos($V1))
  public static final String pc33 = "DGT(SQRT_(SIN_(DVAR(ID_1))),SQRT_(COS_(DVAR(ID_1))))";
  // sqrt(sin($V1)) < sqrt(cos($V1))
  public static final String pc34 = "DLT(SQRT_(SIN_(DVAR(ID_1))),SQRT_(COS_(DVAR(ID_2))))"; 
  //  1.0/sqrt(sin($V1)) > sqrt(cos(exp($V2)))
  public static final String pc35 = "DGT(DIV(DCONST(1),SQRT_(SIN_(DVAR(ID_1)))),SQRT_(COS_(EXP_(DVAR(ID_2)))))"; 
  // ((log10($V3)*(1.0/sqrt(sin_($V1)))) == sqrt(cos_(exp($V2))))
  public static final String pc36 = "DEQ(MUL(LOG10_(DVAR(ID_3)),DIV(DCONST(1),SQRT_(SIN_(DVAR(ID_1))))),SQRT_(COS_(EXP_(DVAR(ID_2)))))"; 
  // ((log10(tan_($V3))/(1.0/sqrt(sin_($V1)))) == sqrt(cos_(exp($V2))))
  public static final String pc37 = "DEQ(DIV(LOG10_(TAN_(DVAR(ID_3))),DIV(DCONST(1),SQRT_(SIN_(DVAR(ID_1))))),SQRT_(COS_(EXP_(DVAR(ID_2)))))";
  // (atan2_($V1,$V2) == 1.0)
  public static final String pc38 = "DEQ(ATAN2_(DVAR(ID_1),DVAR(ID_2)),DCONST(1.0))";
  // (pow_($V1,$V2) == 1.0)
  public static final String pc39 = "DEQ(POW_(DVAR(ID_1),DVAR(ID_2)),DCONST(1.0))";
  // pow(x,2) == x + y
  public static final String pc40 = "DEQ(POW_(DVAR(ID_1),DCONST(2)),ADD(DVAR(ID_1),DVAR(ID_2)))";
  // pow(x,2) == x + y &
  // x >= -1
  // y <=  2
  public static final String pc41 = 
    "DEQ(POW_(DVAR(ID_1),DCONST(2)),ADD(DVAR(ID_1),DVAR(ID_2)));" +
    "DGE(DVAR(ID_1),DCONST(1));" +
    "DLE(DVAR(ID_2),DCONST(2))";
  // pow(x,y) > pow(y,x) &
  // x > 1
  // y <= 10
  public static final String pc42 = 
    "DGT(POW_(DVAR(ID_1),DVAR(ID_2)),POW_(DVAR(ID_2),DVAR(ID_1)));"+
    "DGT(DVAR(ID_1),DCONST(1));"+
    "DLE(DVAR(ID_2),DCONST(10))";

  // pow(x,y) > pow(y,x) &
  // exp(x,y) > exp(y,x) &
  // y < x ^ 2
  public static final String pc43 = 
    "DGT(POW_(DVAR(ID_1),DVAR(ID_2)),POW_(DVAR(ID_2),DVAR(ID_1)));"+
    "DGT(EXP_(DVAR(ID_2)),EXP_(DVAR(ID_1)));"+
    "DLT(DVAR(ID_2),POW_(DVAR(ID_1),DCONST(2)))";

  // pow(x,y) > pow(y,x) &
  // exp(x,y) < exp(y,x)
  public static final String pc44 = 
    "DGT(POW_(DVAR(ID_1),DVAR(ID_2)),POW_(DVAR(ID_2),DVAR(ID_1)));"+
    "DLT(EXP_(DVAR(ID_2)),EXP_(DVAR(ID_1)))";

  // 
  public static final String pc45 = 
    "DLT(SQRT_(EXP_(ADD(DVAR(ID_1),DVAR(ID_2)))),POW_(DVAR(ID_3),DVAR(ID_1)));" +
    "DGT(DVAR(ID_1),DCONST(0));" +
    "DGT(DVAR(ID_2),DCONST(1));" +
    "DGT(DVAR(ID_3),DCONST(1));" +
    "DLE(DVAR(ID_2),ADD(DVAR(ID_1),DCONST(2)))";

  // sqrt(e^(x + z)) < z^x,
  // x > 0, y > 1, z > 1, y < 1, y < x + 2, w = x + 2
  public static final String pc46 = 
    "DLT(SQRT_(EXP_(ADD(DVAR(ID_1),DVAR(ID_2)))),POW_(DVAR(ID_3),DVAR(ID_1)));" +
    "DGT(DVAR(ID_1),DCONST(0));" +
    "DGT(DVAR(ID_2),DCONST(1));" +
    "DGT(DVAR(ID_3),DCONST(1));" +
    "DLE(DVAR(ID_2),ADD(DVAR(ID_1),DCONST(2)));" +
    "DEQ(ADD(DVAR(ID_1),DCONST(2)),DVAR(ID_4))";

  // e ^ (x + y) == e ^ z 
  public static final String pc47 = 
    "DEQ(EXP_(ADD(DVAR(ID_1),DVAR(ID_2))),EXP_(DVAR(ID_3)))";

  // x + y != z
  public static final String pc48 = 
    "DEQ(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3))";

  //x^2 + 3*sqrt(y) < x*y && x < y ^ 2 && x + y < 50 //556 possible integer solutions
  public static final String pc49 = 
    "DLT(ADD(POW_(DVAR(ID_1),DCONST(2.0)),MUL(DCONST(3.0),SQRT_(DVAR(ID_2)))),MUL(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_1),POW_(DVAR(ID_1),DCONST(3.0)));DLT(ADD(DVAR(ID_1),DVAR(ID_2)),DCONST(50))";
  //x^2 + 3*sqrt(y) < x*y && x < y ^ 2 && x + y < 50 && x = -13 + y //18 possible integer solutions
  public static final String pc50 = 
    "DLT(ADD(POW_(DVAR(ID_1),DCONST(2.0)),MUL(DCONST(3.0),SQRT_(DVAR(ID_2)))),MUL(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_1),POW_(DVAR(ID_1),DCONST(3.0)));DLT(ADD(DVAR(ID_1),DVAR(ID_2)),DCONST(50));DEQ(DVAR(ID_1),ADD(DCONST(-13),DVAR(ID_2)))";
  //x^2 + 3*sqrt(y) < x*y && x < y ^ 2 && x + y < 50 && x = -13 + y && x ^ x < log10(y) //one integer solution
  public static final String pc51 = 
    "DLT(ADD(POW_(DVAR(ID_1),DCONST(2.0)),MUL(DCONST(3.0),SQRT_(DVAR(ID_2)))),MUL(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_1),POW_(DVAR(ID_1),DCONST(3.0)));DLT(ADD(DVAR(ID_1),DVAR(ID_2)),DCONST(50));DEQ(DVAR(ID_1),ADD(DCONST(-13),DVAR(ID_2)));DLT(POW_(DVAR(ID_1),DVAR(ID_1)),LOG10_(DVAR(ID_2))) ";
  //x ^ tan(y) + z < x * atan(z) && sen(y) + cos(y) + tan(y) >= x - z
  public static final String pc52 =
    "DLT(ADD(POW_(DVAR(ID_1),TAN_(DVAR(ID_2))),DVAR(ID_3)),MUL(DVAR(ID_1),ATAN_(DVAR(ID_3))));DGE(ADD(ADD(SIN_(DVAR(ID_2)),COS_(DVAR(ID_2))),TAN_(DVAR(ID_2))),SUB(DVAR(ID_1),DVAR(ID_3)))";
  //x ^ tan(y) + z < x * atan(z) && sen(y) + cos(y) + tan(y) >= x - z && atan(x) + atan(y) > y
  public static final String pc53 = 
    "DLT(ADD(POW_(DVAR(ID_1),TAN_(DVAR(ID_2))),DVAR(ID_3)),MUL(DVAR(ID_1),ATAN_(DVAR(ID_3))));DGE(ADD(ADD(SIN_(DVAR(ID_2)),COS_(DVAR(ID_2))),TAN_(DVAR(ID_2))),SUB(DVAR(ID_1),DVAR(ID_3)));DGT(ADD(ATAN_(DVAR(ID_1)),ATAN_(DVAR(ID_3))),DVAR(ID_2))";
  //x ^ tan(y) + z < x * atan(z) && sen(y) + cos(y) + tan(y) >= x - z && atan(x) + atan(y) > y && log(x^tan(y)) < log(z)
  public static final String pc54 =
    "DLT(ADD(POW_(DVAR(ID_1),TAN_(DVAR(ID_2))),DVAR(ID_3)),MUL(DVAR(ID_1),ATAN_(DVAR(ID_3))));DGE(ADD(ADD(SIN_(DVAR(ID_2)),COS_(DVAR(ID_2))),TAN_(DVAR(ID_2))),SUB(DVAR(ID_1),DVAR(ID_3)));DGT(ADD(ATAN_(DVAR(ID_1)),ATAN_(DVAR(ID_3))),DVAR(ID_2));DLT(LOG_(POW_(DVAR(ID_1),TAN_(DVAR(ID_2)))),LOG_(DVAR(ID_3)))";
  //x ^ tan(y) + z < x * atan(z) && 
  // sen(y) + cos(y) + tan(y) >= x - z && 
  // atan(x) + atan(y) > y && 
  // log(x^tan(y)) < log(z) && 
  // sqrt(y+z) > sqrt(x^(x-y))  
  public static final String pc55 =
    "DLT(ADD(POW_(DVAR(ID_1),TAN_(DVAR(ID_2))),DVAR(ID_3)),MUL(DVAR(ID_1),ATAN_(DVAR(ID_3))));DGE(ADD(ADD(SIN_(DVAR(ID_2)),COS_(DVAR(ID_2))),TAN_(DVAR(ID_2))),SUB(DVAR(ID_1),DVAR(ID_3)));DGT(ADD(ATAN_(DVAR(ID_1)),ATAN_(DVAR(ID_3))),DVAR(ID_2));DLT(LOG_(POW_(DVAR(ID_1),TAN_(DVAR(ID_2)))),LOG_(DVAR(ID_3)));DGT(SQRT_(ADD(DVAR(ID_2),DVAR(ID_3))),SQRT_(POW_(DVAR(ID_1),SUB(DVAR(ID_1),DVAR(ID_2)))))";
  //x * y + atan(z) * sin(w*t) > x/y + z + tan(w+t)
  public static final String pc56 =
    "DGT(ADD(MUL(DVAR(ID_1),DVAR(ID_2)),MUL(ATAN_(DVAR(ID_3)),SIN_(MUL(DVAR(ID_4),DVAR(ID_5))))),DCONST(0.0))";
  //x * y + atan(z) * sin(w*t) > x/y + z + tan(w+t) && POW(log10(x),log10(y)) <= POW(log10(z+w+t),tan(w*t))
  public static final String pc57 =
    "DGT(ADD(MUL(DVAR(ID_1),DVAR(ID_2)),MUL(ATAN_(DVAR(ID_3)),SIN_(MUL(DVAR(ID_4),DVAR(ID_5))))),DCONST(0.0));DLE(POW_(LOG10_(DVAR(ID_1)),LOG10_(DVAR(ID_2))),POW_(LOG10_(ADD(ADD(DVAR(ID_3),DVAR(ID_4)),DVAR(ID_5))),TAN_(MUL(DVAR(ID_4),DVAR(ID_5)))))";
  //x * y + atan(z) * sin(w*t) > x/y + z + tan(w+t) && POW(log10(x),log10(y)) <= POW(log10(z+w+t),tan(w*t)) && tan(w*(x+y)) + sin(t*(y+z)) > asin(x+y+z) + acos(x+y+z) + atan(x+y+z)
  public static final String pc58 =
    "DGT(ADD(MUL(DVAR(ID_1),DVAR(ID_2)),MUL(ATAN_(DVAR(ID_3)),SIN_(MUL(DVAR(ID_4),DVAR(ID_5))))),DCONST(0.0));DLE(POW_(LOG10_(DVAR(ID_1)),LOG10_(DVAR(ID_2))),POW_(LOG10_(ADD(ADD(DVAR(ID_3),DVAR(ID_4)),DVAR(ID_5))),TAN_(MUL(DVAR(ID_4),DVAR(ID_5)))));DGT(ADD(TAN_(MUL(DVAR(ID_4),ADD(DVAR(ID_1),DVAR(ID_2)))),SIN_(MUL(DVAR(ID_5),ADD(DVAR(ID_2),DVAR(ID_3))))),ADD(ADD(ASIN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3))),ACOS_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))),ATAN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))))";
  //x * y + atan(z) * sin(w*t) > x/y + z + tan(w+t) && POW(log10(x),log10(y)) <= POW(log10(z+w+t),tan(w*t))	&& tan(w*(x+y)) + sin(t*(y+z)) > asin(x+y+z) + acos(x+y+z) + atan(x+y+z) && w = t * 3 / 4
  public static final String pc59 =
    "DGT(ADD(MUL(DVAR(ID_1),DVAR(ID_2)),MUL(ATAN_(DVAR(ID_3)),SIN_(MUL(DVAR(ID_4),DVAR(ID_5))))),DCONST(0.0));DLE(POW_(LOG10_(DVAR(ID_1)),LOG10_(DVAR(ID_2))),POW_(LOG10_(ADD(ADD(DVAR(ID_3),DVAR(ID_4)),DVAR(ID_5))),TAN_(MUL(DVAR(ID_4),DVAR(ID_5)))));DGT(ADD(TAN_(MUL(DVAR(ID_4),ADD(DVAR(ID_1),DVAR(ID_2)))),SIN_(MUL(DVAR(ID_5),ADD(DVAR(ID_2),DVAR(ID_3))))),ADD(ADD(ASIN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3))),ACOS_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))),ATAN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))));DEQ(DVAR(ID_4),MUL(DVAR(ID_5),DIV(DCONST(3),DCONST(4))))";
  //x * y + atan(z) * sin(w*t) > x/y + z + tan(w+t) && POW(log10(x),log10(y)) <= POW(log10(z+w+t),tan(w*t))	&& tan(w*(x+y)) + sin(t*(y+z)) > asin(x+y+z) + acos(x+y+z) + atan(x+y+z) && w = t * 3 / 4 && x < 2y - 3z
  public static final String pc60 =
    "DGT(ADD(MUL(DVAR(ID_1),DVAR(ID_2)),MUL(ATAN_(DVAR(ID_3)),SIN_(MUL(DVAR(ID_4),DVAR(ID_5))))),DCONST(0.0));DLE(POW_(LOG10_(DVAR(ID_1)),LOG10_(DVAR(ID_2))),POW_(LOG10_(ADD(ADD(DVAR(ID_3),DVAR(ID_4)),DVAR(ID_5))),TAN_(MUL(DVAR(ID_4),DVAR(ID_5)))));DGT(ADD(TAN_(MUL(DVAR(ID_4),ADD(DVAR(ID_1),DVAR(ID_2)))),SIN_(MUL(DVAR(ID_5),ADD(DVAR(ID_2),DVAR(ID_3))))),ADD(ADD(ASIN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3))),ACOS_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))),ATAN_(ADD(ADD(DVAR(ID_1),DVAR(ID_2)),DVAR(ID_3)))));DEQ(DVAR(ID_4),MUL(DVAR(ID_5),DIV(DCONST(3),DCONST(4))));DLT(DVAR(ID_1),SUB(MUL(DCONST(2),DVAR(ID_2)),MUL(DCONST(3),DVAR(ID_3))))";
  //x + y > z / w && sqrt(x) > z / y && z*2 + w*3 + x*7 < pow(y,6) && z + w > x + y && w < x/y
  public static final String pc61 = 
    "DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(DVAR(ID_3),DVAR(ID_4)));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DCONST(6)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)))";
  //x + y > z / w && sqrt(x) > z / y && z*2 + w*3 + x*7 < pow(y,6) && z + w > x + y && w < x/y && x > (w+y-z)
  public static final String pc62 = 
    "DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(DVAR(ID_3),DVAR(ID_4)));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DCONST(6)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)))";
  //x + y > z / w && sqrt(x) > z / y && log(x*y) > log(t+w+z) && z*2 + w*3 + x*7 < pow(y,6) && z + w > x + y && w < x/y && x > (w+y-z) && log10(t*x) < sqrt(w*y*z)
  public static final String pc63 =
    "DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(DVAR(ID_3),DVAR(ID_4)));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DCONST(6)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))))";
  //x + y > z / (w + t) && sqrt(x) > z / y && log(x*y) > log(t+w+z) && z*2 + w*3 + x*7 < pow(y,t) && z + w > x + y && w < x/y && x > (w+y-z) && log10(t*x) < sqrt(w*y*z) && 																																																																																																																																																																																																																									x * (t + y) > log(w*z*3) 
  public static final String pc64 = 
    "DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(DVAR(ID_3),ADD(DVAR(ID_4),DVAR(ID_5))));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DVAR(ID_5)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))));DGT(MUL(DVAR(ID_1),ADD(DVAR(ID_5),DVAR(ID_2))),LOG_(MUL(MUL(DVAR(ID_4),DVAR(ID_3)),DCONST(3))))";
  //x + y > z / (w + t) && sqrt(x) > z / y && log(x*y) > log(t+w+z) && z*2 + w*3 + x*7 < pow(y,t) && z + w > x + y && w < x/y && x > (w+y-z) && log10(t*x) < sqrt(w*y*z) && 																																																																																																																																																																																																																									x * cos(t + y) > log(w*z*3)
  public static final String pc65 = //igual à 64, mas com uma operacao trigonometrica na ultima constraint -  
    "DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(DVAR(ID_3),ADD(DVAR(ID_4),DVAR(ID_5))));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DVAR(ID_5)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))));DGT(MUL(DVAR(ID_1),COS_(ADD(DVAR(ID_5),DVAR(ID_2)))),LOG_(MUL(MUL(DVAR(ID_4),DVAR(ID_3)),DCONST(3))))";
  //x + y > (z+ t) / (w + t) && sqrt(x) > z / y && log(x*y) > log(t+w+z) && z*2 + w*3 + x*7 < pow(y,t) && z + w > x + y && w < x/y && x > (w+y-z) && log10(t*x) < sqrt(w*y*z) && x * cos(t + y) > log(w*z*3) && cos(t) > cos(y)
  public static final String pc66 = 
    "DGT(ADD(DVAR(ID_1),DVAR(ID_2)),DIV(ADD(DVAR(ID_3),DVAR(ID_5)),ADD(DVAR(ID_4),DVAR(ID_5))));DGT(SQRT_(DVAR(ID_1)),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),POW_(DVAR(ID_2),DVAR(ID_5)));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))));DGT(MUL(DVAR(ID_1),COS_(ADD(DVAR(ID_5),DVAR(ID_2)))),LOG_(MUL(MUL(DVAR(ID_4),DVAR(ID_3)),DCONST(3))));DGT(COS_(DVAR(ID_5)),COS_(DVAR(ID_2)))";
  //x - y + tan(v)> (z+ t) / (w + t) && sqrt(x-t) > z / y && log(x*y) > log(t+w+z) && z*2 + w*3 + x*7 < pow(y,t)*cos(v) && z + w > x + y && w < x/y && x > (w+y-z) && log10(t*x) < sqrt(w*y*z) && x * cos(t + y) > log(w*z*3) && cos(t) * sin(v) > cos(y)
  public static final String pc67 = 
    "DGT(SUB(DVAR(ID_1),ADD(DVAR(ID_2),TAN_(DVAR(ID_6)))),DIV(ADD(DVAR(ID_3),DVAR(ID_5)),ADD(DVAR(ID_4),DVAR(ID_5))));DGT(SQRT_(SUB(DVAR(ID_1),DVAR(ID_6))),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),MUL(POW_(DVAR(ID_2),DVAR(ID_5)),COS_(DVAR(ID_6))));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))));DGT(MUL(DVAR(ID_1),COS_(ADD(DVAR(ID_5),DVAR(ID_2)))),LOG_(MUL(MUL(DVAR(ID_4),DVAR(ID_3)),DCONST(3))));DGT(MUL(COS_(DVAR(ID_5)),SIN_(DVAR(ID_6))),COS_(DVAR(ID_2)))";
  //x - y + tan(v)> (z+ t) / (w + t) && sqrt(x-t) > z / y && log(x*y) > log(t+w+z) && z*2 + w*3 + x*7 < pow(y,t)*cos(v) && z + w > x + y && w < x/y && x > (w+y-z) && log10(t*x) < sqrt(w*y*z) && x * cos(t + y) > log(w*z*3) && cos(t) * sin(v) > cos(y) && sin(x*y) + sin(z*w) + sin(t*v) < cos(x*y) + cos(z*w) + cos(t*v)
  public static final String pc68 = 
    "DGT(SUB(DVAR(ID_1),ADD(DVAR(ID_2),TAN_(DVAR(ID_6)))),DIV(ADD(DVAR(ID_3),DVAR(ID_5)),ADD(DVAR(ID_4),DVAR(ID_5))));DGT(SQRT_(SUB(DVAR(ID_1),DVAR(ID_6))),DIV(DVAR(ID_3),DVAR(ID_2)));DGT(LOG_(MUL(DVAR(ID_1),DVAR(ID_2))),LOG_(ADD(ADD(DVAR(ID_5),DVAR(ID_4)),DVAR(ID_3))));DLT(ADD(ADD(MUL(DVAR(ID_3),DCONST(2)),MUL(DVAR(ID_4),DCONST(3))),MUL(DVAR(ID_1),DCONST(7))),MUL(POW_(DVAR(ID_2),DVAR(ID_5)),COS_(DVAR(ID_6))));DGT(ADD(DVAR(ID_3),DVAR(ID_4)),ADD(DVAR(ID_1),DVAR(ID_2)));DLT(DVAR(ID_4),DIV(DVAR(ID_1),DVAR(ID_2)));DGT(DVAR(ID_1),SUB(ADD(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3)));DLT(LOG10_(MUL(DVAR(ID_5),DVAR(ID_1))),SQRT_(MUL(MUL(DVAR(ID_4),DVAR(ID_2)),DVAR(ID_3))));DGT(MUL(DVAR(ID_1),COS_(ADD(DVAR(ID_5),DVAR(ID_2)))),LOG_(MUL(MUL(DVAR(ID_4),DVAR(ID_3)),DCONST(3))));DGT(MUL(COS_(DVAR(ID_5)),SIN_(DVAR(ID_6))),COS_(DVAR(ID_2)));DLT(ADD(ADD(SIN_(MUL(DVAR(ID_1),DVAR(ID_2))),SIN_(MUL(DVAR(ID_3),DVAR(ID_4)))),SIN_(MUL(DVAR(ID_5),DVAR(ID_6)))),ADD(ADD(COS_(MUL(DVAR(ID_1),DVAR(ID_2))),COS_(MUL(DVAR(ID_3),DVAR(ID_4)))),COS_(MUL(DVAR(ID_5),DVAR(ID_6)))))";
  //sin(a) > sin(b) > sin(c) > sin(d) > sin(e) > sin(f) > sin(g) > sin(h) > sin(i) > sin(j) > sin(k) > sin(l) > sin(m) > sin(n) > sin(o) > sin(p) > sin(q) > sin(r) > sin(s) > sin(t) > sin(u) > sin(v) > sin(x) > sin(z)
  public static final String pc69 =
    "DGT(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2)));DGT(SIN_(DVAR(ID_2)),SIN_(DVAR(ID_3)));DGT(SIN_(DVAR(ID_3)),SIN_(DVAR(ID_4)));DGT(SIN_(DVAR(ID_4)),SIN_(DVAR(ID_5)));DGT(SIN_(DVAR(ID_5)),SIN_(DVAR(ID_6)));DGT(SIN_(DVAR(ID_6)),SIN_(DVAR(ID_7)));DGT(SIN_(DVAR(ID_7)),SIN_(DVAR(ID_8)));DGT(SIN_(DVAR(ID_8)),SIN_(DVAR(ID_9)));DGT(SIN_(DVAR(ID_9)),SIN_(DVAR(ID_10)));DGT(SIN_(DVAR(ID_10)),SIN_(DVAR(ID_11)));DGT(SIN_(DVAR(ID_11)),SIN_(DVAR(ID_12)));DGT(SIN_(DVAR(ID_12)),SIN_(DVAR(ID_13)));DGT(SIN_(DVAR(ID_13)),SIN_(DVAR(ID_14)));DGT(SIN_(DVAR(ID_14)),SIN_(DVAR(ID_15)));DGT(SIN_(DVAR(ID_15)),SIN_(DVAR(ID_16)));DGT(SIN_(DVAR(ID_16)),SIN_(DVAR(ID_17)));DGT(SIN_(DVAR(ID_17)),SIN_(DVAR(ID_18)));DGT(SIN_(DVAR(ID_18)),SIN_(DVAR(ID_19)));DGT(SIN_(DVAR(ID_19)),SIN_(DVAR(ID_20)));DGT(SIN_(DVAR(ID_20)),SIN_(DVAR(ID_21)));DGT(SIN_(DVAR(ID_21)),SIN_(DVAR(ID_22)));DGT(SIN_(DVAR(ID_22)),SIN_(DVAR(ID_23)));DGT(SIN_(DVAR(ID_23)),SIN_(DVAR(ID_24)));DGT(SIN_(DVAR(ID_24)),SIN_(DVAR(ID_25)));DGT(SIN_(DVAR(ID_25)),SIN_(DVAR(ID_26)))";
  public static final String pc70 = // meia porção da de cima (13 segmentos)
    "DGT(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2)));DGT(SIN_(DVAR(ID_2)),SIN_(DVAR(ID_3)));DGT(SIN_(DVAR(ID_3)),SIN_(DVAR(ID_4)));DGT(SIN_(DVAR(ID_4)),SIN_(DVAR(ID_5)));DGT(SIN_(DVAR(ID_5)),SIN_(DVAR(ID_6)));DGT(SIN_(DVAR(ID_6)),SIN_(DVAR(ID_7)));DGT(SIN_(DVAR(ID_7)),SIN_(DVAR(ID_8)));DGT(SIN_(DVAR(ID_8)),SIN_(DVAR(ID_9)));DGT(SIN_(DVAR(ID_9)),SIN_(DVAR(ID_10)));DGT(SIN_(DVAR(ID_10)),SIN_(DVAR(ID_11)));DGT(SIN_(DVAR(ID_11)),SIN_(DVAR(ID_12)));DGT(SIN_(DVAR(ID_12)),SIN_(DVAR(ID_13)))";
  public static final String pc71 = // 1/4 (pso e random resolvem)
    "DGT(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2)));DGT(SIN_(DVAR(ID_2)),SIN_(DVAR(ID_3)));DGT(SIN_(DVAR(ID_3)),SIN_(DVAR(ID_4)));DGT(SIN_(DVAR(ID_4)),SIN_(DVAR(ID_5)));DGT(SIN_(DVAR(ID_5)),SIN_(DVAR(ID_6)));DGT(SIN_(DVAR(ID_6)),SIN_(DVAR(ID_7)))";
  public static final String pc72 = // 3/8 nenhum resolve (limite superior dos solvers?)
    "DGT(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2)));DGT(SIN_(DVAR(ID_2)),SIN_(DVAR(ID_3)));DGT(SIN_(DVAR(ID_3)),SIN_(DVAR(ID_4)));DGT(SIN_(DVAR(ID_4)),SIN_(DVAR(ID_5)));DGT(SIN_(DVAR(ID_5)),SIN_(DVAR(ID_6)));DGT(SIN_(DVAR(ID_6)),SIN_(DVAR(ID_7)));DGT(SIN_(DVAR(ID_7)),SIN_(DVAR(ID_8)));DGT(SIN_(DVAR(ID_8)),SIN_(DVAR(ID_9)));DGT(SIN_(DVAR(ID_9)),SIN_(DVAR(ID_10)))";
  public static final String pc73 = // 8/26 (pso e random resolvem) 
    "DGT(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2)));DGT(SIN_(DVAR(ID_2)),SIN_(DVAR(ID_3)));DGT(SIN_(DVAR(ID_3)),SIN_(DVAR(ID_4)));DGT(SIN_(DVAR(ID_4)),SIN_(DVAR(ID_5)));DGT(SIN_(DVAR(ID_5)),SIN_(DVAR(ID_6)));DGT(SIN_(DVAR(ID_6)),SIN_(DVAR(ID_7)));DGT(SIN_(DVAR(ID_7)),SIN_(DVAR(ID_8)))";
  public static final String pc74 = // 9/26 (random e pso resolvem)
    "DGT(SIN_(DVAR(ID_1)),SIN_(DVAR(ID_2)));DGT(SIN_(DVAR(ID_2)),SIN_(DVAR(ID_3)));DGT(SIN_(DVAR(ID_3)),SIN_(DVAR(ID_4)));DGT(SIN_(DVAR(ID_4)),SIN_(DVAR(ID_5)));DGT(SIN_(DVAR(ID_5)),SIN_(DVAR(ID_6)));DGT(SIN_(DVAR(ID_6)),SIN_(DVAR(ID_7)));DGT(SIN_(DVAR(ID_7)),SIN_(DVAR(ID_8)));DGT(SIN_(DVAR(ID_8)),SIN_(DVAR(ID_9)))";
  // a > b > c > d > e ... (9) // (limited doubles) (random e pso resolvem)
  public static final String pc75 =  
    "DGT(DVAR(ID_1),DVAR(ID_2));DGT(DVAR(ID_2),DVAR(ID_3));DGT(DVAR(ID_3),DVAR(ID_4));DGT(DVAR(ID_4),DVAR(ID_5));DGT(DVAR(ID_5),DVAR(ID_6));DGT(DVAR(ID_6),DVAR(ID_7));DGT(DVAR(ID_7),DVAR(ID_8));DGT(DVAR(ID_8),DVAR(ID_9))";
  //a > b > c > d > e ... (10) // (limited doubles) (apenas pso resolve :D)
  public static final String pc76 =  
    "DGT(DVAR(ID_1),DVAR(ID_2));DGT(DVAR(ID_2),DVAR(ID_3));DGT(DVAR(ID_3),DVAR(ID_4));DGT(DVAR(ID_4),DVAR(ID_5));DGT(DVAR(ID_5),DVAR(ID_6));DGT(DVAR(ID_6),DVAR(ID_7));DGT(DVAR(ID_7),DVAR(ID_8));DGT(DVAR(ID_8),DVAR(ID_9));DGT(DVAR(ID_9),DVAR(ID_10))";
  
  /*
   * ID_1 =  C_v_8_SYMREAL
   * ID_2 = C_Psi0_deg_7_SYMREAL
   * ID_3 = A_Psi0_deg_3_SYMREAL 
   * ID_4 = A_v_4_SYMREAL
   * ID_5 = bank_ang_12_SYMREAL
   * ID_6 = Cturn_10_SYMINT 
   * ID_7 = Cdir_11_SYMINT 
   * ID_8 = Aturn_9_SYMINT
   * 
   * x1 = ( pow (x11,CONST_2.0))
   * x11 = (x111 - (ID_4 * CONST_0.0))
   * x111 = (ID_1 * x1111)
   * x1111 = ( sin x11111)
   * x11111 = (x111111 + x111112)
   * x111111 = ((ID_2  * CONST_0.017453292519943295) - (ID_3*      CONST_0.017453292519943295))
   * x111112 = (x1111121 / x1111122)
   * x1111121 = (x11111211 *      ID_1)
   * x11111211 = (x...1 * CONST_-1.0)
   * x...1 = (x....1 / ID_4)
   * x....1 = (x.....1 * CONST_0.0)
   * x.....1 = (x.;.1 /      CONST_68443.0)
   * x.;.1 = (( pow (ID_4,CONST_2.0))      / 
   *                      (( sin ((ID_5 * CONST_0.017453292519943295))) / ( cos      ((ID_5 * CONST_0.017453292519943295)))))
   *                      
   * x1111122 = (x.2 /      CONST_68443.0)
   * x.2 = (( pow (ID_1,CONST_2.0)) / 
   *          (( sin      ((ID_5 * CONST_0.017453292519943295))) / ( cos      ((ID_5 * CONST_0.017453292519943295)))))
   *          
   * x2 = (pow(x21,CONST_2.0))
   * x21 = (x22 - (ID_4 * CONST_1.0))
   * x22 = (ID_1 * x23)
   * x23 = ( cos x24)
   * x24 = (x25 + 2x6)
   * x25 = ((ID_2 *      CONST_0.017453292519943295) - (ID_3*      CONST_0.017453292519943295))
   * x26 = (x261 / x262)
   * x261 = (x2611 *      ID_1)
   * x2611 = (x2612 * CONST_-1.0)
   * x2612 = (x2613 / ID_4)
   * x2613 = (x2614 * CONST_0.0)
   * x2614 = (x2615 /      CONST_68443.0)
   * x2615 = (( pow (ID_4,CONST_2.0))
   *              / (( sin ((ID_5 * CONST_0.017453292519943295))) / ( cos      ((ID_5 * CONST_0.017453292519943295)))))
   * x262 = (x2621 /      CONST_68443.0)
   * x2621 = (( pow (ID_1,CONST_2.0)) 
   *            / (( sin      ((ID_5 * CONST_0.017453292519943295))) / ( cos      ((ID_5 * CONST_0.017453292519943295)))))
  //CONST_0.0 == (x1 + x2)
   *  && ID_6 != CONST_0 && ID_8 != CONST_0 && ID_7 == CONST_0 && ID_1 > CONST_0.0 && ID_4 > CONST_0.0 && ID_5 < CONST_90.0 &&      ID_5 > CONST_0.0 && ID_7 < CONST_3 &&      ID_7 >= CONST_0 
  */
  public static final String pc77 = 
    "DEQ(DCONST(0.0),ADD(POW_(SUB(MUL(DVAR(ID_1),SIN_(ADD(SUB(MUL(DVAR(ID_2),DCONST(0.017453292519943295)),MUL(DVAR(ID_3),DCONST(0.017453292519943295))),DIV(MUL(MUL(DIV(MUL(DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),DIV(SIN_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))),COS_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))))),DCONST(68443.0)),DCONST(0.0)),DVAR(ID_4)),DCONST(-1.0)),DVAR(ID_1)),DIV(DIV(POW_(DVAR(ID_1),DCONST(2.0)),DIV(SIN_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))),COS_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))))),DCONST(68443.0)))))),MUL(DVAR(ID_4),DCONST(0.0))),DCONST(2.0)),POW_(SUB(MUL(DVAR(ID_1),COS_(ADD(SUB(MUL(DVAR(ID_2),DCONST(0.017453292519943295)),MUL(DVAR(ID_3),DCONST(0.017453292519943295))),DIV(MUL(MUL(DIV(MUL(DIV(DIV(POW_(DVAR(ID_4),DCONST(2.0)),DIV(SIN_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))),COS_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))))),DCONST(68443.0)),DCONST(0.0)),DVAR(ID_4)),DCONST(-1.0)),DVAR(ID_1)),DIV(DIV(POW_(DVAR(ID_1),DCONST(2.0)),DIV(SIN_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))),COS_(MUL(DVAR(ID_5),DCONST(0.017453292519943295))))),DCONST(68443.0)))))),MUL(DVAR(ID_4),DCONST(1.0))),DCONST(2.0))));INE(IVAR(ID_6),ICONST(0));INE(IVAR(ID_8),ICONST(0);IEQ(IVAR(ID_7),ICONST(0));DGT(DVAR(ID_1),DCONST(0.0)));DGT(DVAR(ID_4),DCONST(0.0));DLT(DVAR(ID_5),DCONST(90.0));DGT(DVAR(ID_5),DCONST(0.0));ILT(IVAR(ID_7),ICONST(3));IGE(DVAR(ID_7),ICONST(0.0))";

  /*
   * $1 = ($V81 == 0)
   * $2 = ($V82 != 0)
   * $3 = ($V83 != 0)
   * $4 = (0.0 == $41)
   * $41 = ($411+$412)
   * $411 = pow_($4111,2.0)
   * $4111 = ($41111-(0.0*$V87))
   * $41111 = ($V84*sin_(((0.017453292519943295*$V85)-(0.017453292519943295*$V86))))
   * $412 = pow_($4121,2.0)
   * $4121 = ($41211-(1.0*$V87))
   * $41211 = ($V84*cos_($412111))
   * $412111 = (((0.017453292519943295*$V85)-(0.017453292519943295*$V86))+0.0)
   * $a = AND($1,$2)
   * $b = AND($a,$3)
   */
//"AND(AND(AND(AND(AND(AND(AND($b,$4),                                                                                                                                                                                                                                                                                                                                                                                                                                        ($V81 >= 0)),($V81 < 3)),($V88 > 0.0)),($V88 < 90.0)),($V87 > 0.0)),($V84 > 0.0)))";
  public static final String pc78 =
    //DEQ(DCONST(0.0),ADD(POW_(SUB(MUL(DVAR(ID_84),SIN_(SUB(MUL(DCONST(0.017453292519943295),DVAR(ID_85)),MUL(DCONST(0.017453292519943295),DVAR(ID_86))))),MUL(DCONST(0.0),DVAR(ID_87))),DCONST(2.0)),POW_(MUL(DVAR(ID_84),COS_(ADD(SUB(MUL(DCONST(0.017453292519943295),DVAR(ID_85)),MUL(DCONST(0.017453292519943295),DVAR(ID_86))),DCONST(0.0)))),DCONST(2.0))))
    //IEQ(IVAR(ID_81),ICONST(0));;INE(IVAR(ID_83),ICONST(0));IGE(IVAR(ID_81),ICONST(0));ILT(IVAR(ID_81),ICONST(3));DGT(DVAR(ID_88),DCONST(0.0));DLT(DVAR(ID_88),DCONST(90.0));DGT(DVAR(ID_87),DCONST(0.0));DGT(DVAR(ID_84),DCONST(0.0));
    "DEQ(DCONST(0.0),ADD(POW_(SUB(MUL(DVAR(ID_84),SIN_(SUB(MUL(DCONST(0.017453292519943295),DVAR(ID_85)),MUL(DCONST(0.017453292519943295),DVAR(ID_86))))),MUL(DCONST(0.0),DVAR(ID_87))),DCONST(2.0)),POW_(MUL(DVAR(ID_84),COS_(ADD(SUB(MUL(DCONST(0.017453292519943295),DVAR(ID_85)),MUL(DCONST(0.017453292519943295),DVAR(ID_86))),DCONST(0.0)))),DCONST(2.0))));INE(IVAR(ID_82),ICONST(0))";
    
  //FLoPSy constraints
  
  //(1.5 - x1 * (1 - x2)) == 0
  public static final String pc79 = //Beale
  "DEQ(SUB(DCONST(1.5),MUL(DVAR(ID_1),SUB(DCONST(1.0),DVAR(ID_2)))),DCONST(0.0))";
  
  //(-13 + x1 + ((5 - x2) * x2 - 2) * x2) + (-29 + x1 + ((x2 + 1) * x2 - 14) * x2) == 0
  public static final String pc80 = //Freudenstein And Roth
  "DEQ(ADD(ADD(ADD(DCONST(-13.0),DVAR(ID_1)),MUL(SUB(MUL(SUB(DCONST(5.0),DVAR(ID_2)),DVAR(ID_2)),DCONST(2.0)),DVAR(ID_2)))," +
    "ADD(ADD(DCONST(-29.0),DVAR(ID_1)),MUL(SUB(MUL(ADD(DVAR(ID_2),DCONST(1.0)),DVAR(ID_2)),DCONST(14)),DVAR(ID_2)))),DCONST(0.0))";
  
  // (Math.Pow(10, 4) * x1 * x2 - 1) == 0 && (Math.Pow(Math.E, -x1)
  //        + Math.Pow(Math.E, -x2) - 1.0001) == 0
  public static final String pc81 = //Powell
    "DEQ(SUB(MUL(MUL(POW_(DCONST(10),DCONST(4)),DVAR(ID_1)),DVAR(ID_2)),DCONST(-1.0)),DCONST(0.0));" +
    "DEQ(ADD(POW_(DCONST(2.71828183),MUL(DVAR(ID_1),DCONST(-1))),SUB(POW_(DCONST(2.71828183),MUL(DVAR(ID_2),DCONST(-1))),DCONST(-1.0001))),DCONST(0.0))";
  
  // Math.Pow((1 - x1), 2) + 100 * (Math.Pow((x2 - x1 * x1), 2)) == 0
  public static final String pc82 = //Rosenbrock
    "DEQ(ADD(POW_(SUB(DCONST(1),DVAR(ID_1)),DCONST(2.0)),MUL(DCONST(100),POW_(SUB(DVAR(ID_2),MUL(DVAR(ID_1),DVAR(ID_1))),DCONST(2.0)))),DCONST(0.0))";
  
  //(10 * (x2 - x1 * x1)) == 0 && (1 - x1) == 0 && 
  //(Math.Sqrt(90) * (x4 - x3 * x3)) == 0 && (1 - x3) == 0 &&
  //(Math.Sqrt(10) * (x2 + x4 - 2)) == 0 && (Math.Pow(10, -0.5) * (x2 - x4)) == 0
  
  public static final String pc83 = //Wood
    "DEQ(MUL(DCONST(10.0),SUB(DVAR(ID_2),MUL(DVAR(ID_1),DVAR(ID_1)))),DCONST(0.0));" +
    "DEQ(SUB(DCONST(1.0),DVAR(ID_1)),DCONST(0.0));" +
    "DEQ(MUL(SQRT_(DCONST(90.0)),SUB(DVAR(ID_4),MUL(DVAR(ID_3),DVAR(ID_3)))),DCONST(0.0));" + 
    "DEQ(SUB(DCONST(1.0),DVAR(ID_3)),DCONST(0.0));" + 
    "DEQ(MUL(SQRT_(DCONST(10.0)),ADD(DVAR(ID_2),SUB(DVAR(ID_4),DCONST(2.0)))),DCONST(0.0));" + 
    "DEQ(MUL(POW_(DCONST(10.0),DCONST(-0.5)),SUB(DVAR(ID_2),DVAR(ID_4))),DCONST(0.0))"; 

  //symbolic execution of strings (MSExample)
  //x == -1 OR y > x && y <= 30 && y >= 1 && 1 <= 30 && 1 >= 1 &&
  //y == z && x < 0
  public static final String pc84 =  
    "BOR(IEQ(IVAR(ID_1),ICONST(-1)),IGT(IVAR(ID_2),IVAR(ID_1)));ILE(IVAR(ID_2),ICONST(30));IGE(IVAR(ID_2),ICONST(1));ILE(ICONST(1),ICONST(30));IGE(ICONST(1),ICONST(1));IEQ(IVAR(ID_2),IVAR(ID_3));ILT(IVAR(ID_1),ICONST(0)";
  
  //x == -1 OR y > x && y <= 30 && y >= 1 && 1 <= 30 && 1 >= 1 &&
  //y == z && x >= 0
  
  public static final String pc85 = 
    "BOR(IEQ(IVAR(ID_1),ICONST(-1)),IGT(IVAR(ID_2),IVAR(ID_1)));ILE(IVAR(ID_2),ICONST(30));IGE(IVAR(ID_2),ICONST(1));ILE(ICONST(1),ICONST(30));IGE(ICONST(1),ICONST(1));IEQ(IVAR(ID_2),IVAR(ID_3));IGE(IVAR(ID_1),ICONST(0)";
  
  //($V336 == $V335),($V337 == $V335),((false | ($V336 == -1)) | ($V337 >= 6))),                                                                              ($V338 >= 9)),(($V335+1) >= 0)),($V339 >= (0+$V338))),($V338 <= $V339)),($V338 <= 30)),                                                                                       ($V338 >= 1)),($V338 == ($V339-($V335+1)))),($V338 == $V340)),($V341 == $V335)),($V342 == $V339)),($V343 == $V335)),                                                                                                    ((false | ($V341 == -1)) | ($V342 > $V343))),($V339 <= 30)),($V339 >= 1)),($V339 == $V344)),($V335 >= 0)))
  
  public static final String pc86 = 
    //"IEQ(IVAR(ID_336),IVAR(ID_335));IEQ(IVAR(ID_337),IVAR(ID_335));BOR(BCONST(FALSE),BOR(IEQ(IVAR(ID_336),ICONST(-1)),IGE(IVAR(ID_337),ICONST(6))));IGE(IVAR(ID_338),ICONST(9));IGE(ADD(IVAR(ID_335),ICONST(1)),ICONST(0));IGE(IVAR(ID_339),ADD(ICONST(0),IVAR(ID_338)));ILE(IVAR(ID_338),IVAR(ID_339));ILE(IVAR(ID_338),ICONST(30));IGE(IVAR(ID_338),ICONST(1));IEQ(IVAR(ID_338),SUB(IVAR(ID_339),ADD(IVAR(ID_335),ICONST(1))));IEQ(IVAR(ID_338),IVAR(ID_340));IEQ(IVAR(ID_341),IVAR(ID_335));IEQ(IVAR(ID_342),IVAR(ID_339));IEQ(IVAR(ID_343),IVAR(ID_335));BOR(BCONST(FALSE);BOR(IEQ(IVAR(ID_341),ICONST(-1)),IGT(IVAR(ID_342),IVAR(ID_343))));ILE(IVAR(ID_339),ICONST(30));IGE(IVAR(ID_339),ICONST(1));IEQ(IVAR(ID_339),IVAR(ID_344));IGE(IVAR(ID_335),ICONST(0))";
    "IEQ(IVAR(ID_336),IVAR(ID_335));IEQ(IVAR(ID_337),IVAR(ID_335));BOR(BOR(BCONST(FALSE),IEQ(IVAR(ID_336),ICONST(-1))),IGE(IVAR(ID_337),ICONST(6)));IGE(IVAR(ID_338),ICONST(9));IGE(ADD(IVAR(ID_335),ICONST(1)),ICONST(0));IGE(IVAR(ID_339),ADD(ICONST(0),IVAR(ID_338)));ILE(IVAR(ID_338),IVAR(ID_339));ILE(IVAR(ID_338),ICONST(30));IGE(IVAR(ID_338),ICONST(1));IEQ(IVAR(ID_338),SUB(IVAR(ID_339),ADD(IVAR(ID_335),ICONST(1))));IEQ(IVAR(ID_338),IVAR(ID_340));IEQ(IVAR(ID_341),IVAR(ID_335));IEQ(IVAR(ID_342),IVAR(ID_339));IEQ(IVAR(ID_343),IVAR(ID_335));BOR(BOR(BCONST(FALSE),IEQ(IVAR(ID_341),ICONST(-1))),IGT(IVAR(ID_342),IVAR(ID_343)));ILE(IVAR(ID_339),ICONST(30));IGE(IVAR(ID_339),ICONST(1));IEQ(IVAR(ID_339),IVAR(ID_344));IGE(IVAR(ID_335),ICONST(0))";
  
  public static final String pc87 = "DEQ(DCONST(2.0),DVAR(ID_1));DGT(DVAR(ID_2),DCONST(-1.0));DEQ(ADD(DVAR(ID_2),DVAR(ID_1)),POW_(DVAR(ID_2),DCONST(2.0)))";
  
  public static final String pc88 = "DEQ(DCONST(0.0),MUL(SUB(ADD(MUL(MUL(DIV(DIV(DIV(SUB(ADD(MUL(DVAR(ID_1),DCONST(10.0)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)),DCONST(0.055)),DCONST(2.0)),DIV(DIV(DIV(SUB(ADD(MUL(DVAR(ID_1),DCONST(10.0)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)),DCONST(0.055)),DCONST(2.0))),DCONST(0.5)),DIV(DIV(SUB(DVAR(ID_1),DVAR(ID_2)),DCONST(0.055)),DCONST(2.0))),DCONST(-0.04759988869075444)),DCONST(0.18181818181818185)))";
  
  public static final String pc89 = "DEQ(DCONST(30.0),ADD(DVAR(ID_1),DVAR(ID_2)));DEQ(DVAR(ID_1),DCONST(1.1))";
  
  public static final String pc90 = "DEQ(TAN_(MUL(DVAR(ID_1),DCONST(0.017453292519943295))),DCONST(0.0));DGT(DVAR(ID_1),DCONST(0.0))";
  
  //test case reported by david
  
  // ((3.0 + y) == (y - x)):
  public static final String pc91 = "DEQ(ADD(DCONS(3.0),DVAR(ID_2)),SUB(DVAR(ID_2),DVAR(ID_1)))";
  //((y + 3) == (y - x))
  public static final String pc92 = "DEQ(ADD(DVAR(ID_2),DCONS(3.0)),SUB(DVAR(ID_2),DVAR(ID_1)))";
  //AND((($V10-$V11) == ((sin_($V12)+cos_($V12))+tan_($V12))),((pow_($V10,tan_($V12))+$V11) < ($V10*atan_($V11))))
  public static final String pc93 = "DEQ(SUB(DVAR(ID_1),DVAR(2)),ADD(ADD(SIN_(DVAR(ID_3)),COS_(DVAR(ID_3))),TAN_(DVAR(ID_3))));DLT(ADD(POW_(DVAR(ID_1),TAN_(DVAR(ID_3))),DVAR(ID_2)),MUL(DVAR(ID_1),ATAN_(DVAR(ID_2))))";
  //sent by saswat
  public static final String pc94 = "DLT(DVAR(ID_1),DCONST(-1));DGT(DVAR(ID_1),DCONST(-1.9))";
  //sent by saswat; impossible to solve with removeSimpleEqualities=true
  public static final String pc95 = "DGT(DVAR(ID_1),DCONST(0.0));DEQ(ADD(DVAR(ID_1),DCONST(1E12)),DCONST(1E12))";
  
  //sent by saswat; can only be solved with flexibleRange=true
  public static final String pc96 = "DGT(ADD(DVAR(ID_1),DCONST(1.0e-8)),DCONST(1.0e+3));DLE(DVAR(ID_1),DCONST(1.0e+3))";
  
  //reported by daniel liew
  public static final String pc97 = "LEQ(LCONST(2),LVAR(ID_1))";
  public static final String pc98 = "FEQ(LCONST(2),FVAR(ID_1))";
  
  //reported by corina
  public static final String pc99 = "DGT(MUL(SUB(ADD(MUL(MUL(DIV(DIV(DVAR(ID_1),DCONST(7.855339059327378E-4)),DCONST(2.0)),DIV(DIV(DVAR(ID_1),DCONST(7.855339059327378E-4)),DCONST(2.0))),DCONST(0.5)),SUB(DCONST(0.0),DIV(DIV(DVAR(ID_2),DCONST(7.855339059327378E-4)),DCONST(2.0)))),DCONST(3.332757323673897)),DCONST(0.18181818181818185)),DCONST(0.0));DLT(DIV(DIV(DVAR(ID_2),DCONST(7.855339059327378E-4)),DCONST(2.0)),DCONST(0.0))";
  public static final String pc100 = "DLT(MUL(SUB(ADD(MUL(MUL(DIV(DIV(DVAR(ID_1),DCONST(7.855339059327378E-4)),DCONST(2.0)),DIV(DIV(DVAR(ID_1),DCONST(7.855339059327378E-4)),DCONST(2.0))),DCONST(0.5)),SUB(DCONST(0.0),DIV(DIV(DVAR(ID_2),DCONST(7.855339059327378E-4)),DCONST(2.0)))),DCONST(3.332757323673897)),DCONST(0.18181818181818185)),DCONST(0.0));DLT(DIV(DIV(DVAR(ID_2),DCONST(7.855339059327378E-4)),DCONST(2.0)),DCONST(0.0))";
  public static final String pc101 = "DEQ(DCONST(0.0),MUL(SUB(ADD(MUL(MUL(DIV(DIV(DVAR(ID_3),DCONST(0.055)),DCONST(2.0)),DIV(DIV(DVAR(ID_3),DCONST(0.055)),DCONST(2.0))),DCONST(0.5)),DIV(DIV(DVAR(ID_4),DCONST(0.055)),DCONST(2.0))),DCONST(0.04759988869075444)),DCONST(0.18181818181818185)));DEQ(DCONST(0.0),DIV(DIV(DVAR(ID_4),DCONST(0.055)),DCONST(2.0)))";
  public static final String pc102 = "DGT(MUL(SUB(ADD(MUL(MUL(DIV(DIV(DIV(SUB(ADD(MUL(DVAR(ID_5),DCONST(10.0)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)),DCONST(0.055)),DCONST(2.0)),DIV(DIV(DIV(SUB(ADD(MUL(DVAR(ID_5),DCONST(10.0)),DCONST(-0.0)),DCONST(0.0)),DCONST(1.0)),DCONST(0.055)),DCONST(2.0))),DCONST(0.5)),DIV(DIV(SUB(DVAR(ID_5),DVAR(ID_6)),DCONST(0.055)),DCONST(2.0))),DCONST(0.04759988869075444)),DCONST(0.18181818181818185)),DCONST(0.0));DEQ(DCONST(0.0),DIV(DIV(SUB(DVAR(ID_5),DVAR(ID_6)),DCONST(0.055)),DCONST(2.0)))";
  public static final String pc103 = "DGT(MUL(SUB(ADD(MUL(MUL(DIV(DIV(DVAR(ID_7),DCONST(0.055)),DCONST(2.0)),DIV(DIV(DVAR(ID_7),DCONST(0.055)),DCONST(2.0))),DCONST(0.5)),DIV(DIV(DVAR(ID_8),DCONST(0.055)),DCONST(2.0))),DCONST(0.04759988869075444)),DCONST(0.18181818181818185)),DCONST(0.0));DEQ(DCONST(0.0),DIV(DIV(DVAR(ID_8),DCONST(0.055)),DCONST(2.0)))";
  
  
  
  public static String [] getAllBenchmarks(){
    return  new String[] {
        Benchmark.pc1, 
        Benchmark.pc2, 
        Benchmark.pc3,
        Benchmark.pc4, 
        Benchmark.pc5,  
        Benchmark.pc6, 
        Benchmark.pc7,
        Benchmark.pc8, 
        Benchmark.pc9, 
        Benchmark.pc10, 
        Benchmark.pc11,
        Benchmark.pc12, 
        Benchmark.pc13, 
        Benchmark.pc14, 
        Benchmark.pc15,
        Benchmark.pc16,  
        Benchmark.pc17, 
        Benchmark.pc18,  
        Benchmark.pc19, 
        Benchmark.pc20, 
        Benchmark.pc21, 
        Benchmark.pc22,  
        Benchmark.pc23, 
        Benchmark.pc24, 
        Benchmark.pc25, 
        Benchmark.pc26, 
        Benchmark.pc27,
        Benchmark.pc28, 
        Benchmark.pc29, 
        Benchmark.pc30, 
        Benchmark.pc31, 
        Benchmark.pc32, 
        Benchmark.pc33,
        Benchmark.pc34, 
        Benchmark.pc35, 
        Benchmark.pc36, 
        Benchmark.pc37, 
        Benchmark.pc38,  
        Benchmark.pc39, 
        Benchmark.pc40, 
        Benchmark.pc41,
        Benchmark.pc42,
        Benchmark.pc43,
        Benchmark.pc44,
        Benchmark.pc45,
        Benchmark.pc46,
        Benchmark.pc47,
        Benchmark.pc48,
        Benchmark.pc49,
        Benchmark.pc50,
        Benchmark.pc51,
        Benchmark.pc52,
        Benchmark.pc53,
        Benchmark.pc54,
        Benchmark.pc55,
        Benchmark.pc56,
        Benchmark.pc57,
        Benchmark.pc58,
        Benchmark.pc59,
        Benchmark.pc60,
        Benchmark.pc61,
        Benchmark.pc62,
        Benchmark.pc63,
        Benchmark.pc64,
        Benchmark.pc65,
        Benchmark.pc66,
        Benchmark.pc67,
        Benchmark.pc68,
        Benchmark.pc69,
        Benchmark.pc70,
        Benchmark.pc71,
        Benchmark.pc72,
        Benchmark.pc73,
        Benchmark.pc74,
        Benchmark.pc75,
        Benchmark.pc76,
        pc77,
        pc78,
        pc79,
        pc80,
        pc81,
        pc82,
        pc83,
        pc84,
        pc85,
        pc86,
        pc87,
        pc88,
        pc89,
        pc90,
        pc91,
        pc92,
        pc93,
        pc94,
        pc95,
        pc96,
        pc97,
//        pc98
    };
  }

  public static String[] getSomeUnsolvedBenchmarks(){
    return new String[]{
//        Benchmark.pc5, 
        Benchmark.pc7, 
        Benchmark.pc8, 
//        Benchmark.pc16, 
        Benchmark.pc22, 
        Benchmark.pc23, 
        Benchmark.pc24, 
        Benchmark.pc25, 
        Benchmark.pc26, 
//        Benchmark.pc36, 
//        Benchmark.pc37, 
//        Benchmark.pc38, 
//        Benchmark.pc39, 
//        Benchmark.pc41, 
        Benchmark.pc51, 
//        Benchmark.pc55, 
        Benchmark.pc58, 
//        Benchmark.pc59, 
//        Benchmark.pc60, 
        Benchmark.pc63, 
        Benchmark.pc64, 
        Benchmark.pc65, 
        Benchmark.pc66, 
        Benchmark.pc67, 
        Benchmark.pc68, 
//        Benchmark.pc69, 
//        Benchmark.pc70, 
        Benchmark.pc71, 
        Benchmark.pc72, 
        Benchmark.pc73, 
        Benchmark.pc74, 
        Benchmark.pc75, 
        Benchmark.pc76
    };
  }
  
  public static String [] getAllSolvedBenchmarks(){

    return new String [] {
        Benchmark.pc1, 
        Benchmark.pc2, 
        Benchmark.pc3, 
        Benchmark.pc4, 
        Benchmark.pc6, 
        Benchmark.pc9, 
        Benchmark.pc10, 
        Benchmark.pc11, 
        Benchmark.pc12, 
        Benchmark.pc13, 
        Benchmark.pc14, 
        Benchmark.pc15, 
        Benchmark.pc17, 
        Benchmark.pc18, 
        Benchmark.pc19, 
        Benchmark.pc20, 
        Benchmark.pc21, 
        Benchmark.pc27, 
        Benchmark.pc28, 
        Benchmark.pc29, 
        Benchmark.pc30, 
        Benchmark.pc31, 
        Benchmark.pc32, 
        Benchmark.pc33, 
        Benchmark.pc34, 
        Benchmark.pc35, 
        Benchmark.pc40, 
        Benchmark.pc42, 
        Benchmark.pc43, 
        Benchmark.pc44, 
        Benchmark.pc45, 
        Benchmark.pc46, 
        Benchmark.pc47, 
        Benchmark.pc48, 
        Benchmark.pc49, 
        Benchmark.pc50, 
        Benchmark.pc52, 
        Benchmark.pc53, 
        Benchmark.pc54, 
        Benchmark.pc56, 
        Benchmark.pc57, 
        Benchmark.pc61, 
        Benchmark.pc62
    };
  }
  
  public static String[] getIrregularSolvingBenchmarks(){
    return new String[]{
        Benchmark.pc7, 
        Benchmark.pc8, 
        Benchmark.pc22, 
        Benchmark.pc23, 
        Benchmark.pc24, 
        Benchmark.pc25, 
        Benchmark.pc26,  
        Benchmark.pc51,  
        Benchmark.pc58,  
        Benchmark.pc63, 
        Benchmark.pc64, 
        Benchmark.pc65, 
        Benchmark.pc66, 
        Benchmark.pc67, 
        Benchmark.pc68, 
        Benchmark.pc71, 
        Benchmark.pc72, 
        Benchmark.pc73, 
        Benchmark.pc74, 
        Benchmark.pc75, 
        Benchmark.pc76
    };
  }
  
  public static String[] getFLoPSyBenchmarks(){
    return new String[]{
        pc79,
        pc80,
        pc81,
        pc82,
        pc83
    };
  }
  
  public static String[] getDavidBenchmarks(){
    return new String[]{pc91,pc92,pc93};
  }
  
  public static String[] getCorinaBenchmarks() {
    return new String[]{pc99,pc100,pc101,pc102,pc103};
  }
}
