package coral.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import coral.solvers.SolverKind;
import coral.util.Config;
import coral.util.options.Option;
import coral.util.options.Options;

import symlib.parser.ParseException;

public class RunBenchmark {
  
  @Option("solver")
  public static String solver = "AVM";
  
  @Option("benchmark")
  public static int benchmark = 1;

	@Option("file")
	public static String inputfile="";
  
  private static Options options = new Options(RunBenchmark.class, Config.class);
  
  public static void main(String[] args) throws ParseException, InterruptedException, ExecutionException, FileNotFoundException, IOException {
    
    Config.load(args,options);
    Config.printOptions(options);
    
    SolverKind kind;
    if (solver.contains("PSO_OPT4J")) {
      kind = SolverKind.PSO_OPT4J; 
    } else if (solver.contains("GA_OPT4J")) {
      kind = SolverKind.GA_OPT4J; 
    } else if (solver.equals("RANDOM")) {
      kind = SolverKind.RANDOM; 
    } else if (solver.equals("AVM")){
      kind = SolverKind.AVM;
    } else if (solver.equals("DE_OPT4J")){
      kind = SolverKind.DE_OPT4J;
    } else {
      throw new RuntimeException("did not set solver");
    }
    
    String[] pcs;
    
    switch (benchmark) {
    
    case 0: /* math functions */
        pcs = new String[] {Benchmark.pc63/*Benchmark.pc52*/};
      break;
    case 1: /* math functions */
      pcs = Benchmark.getAllBenchmarks();
      break;
    case 2: /* math functions */
      pcs = Benchmark.getAllSolvedBenchmarks();
      break;
    case 3: /* math functions */
      pcs = Benchmark.getSomeUnsolvedBenchmarks();
      break;
    case 4: /* */
      pcs = Benchmark.getIrregularSolvingBenchmarks();
      break;
    case 5: /* */
      pcs = new String[]{Benchmark.pc77};
      break;
    case 6:
      pcs = Benchmark.getFLoPSyBenchmarks();
      break;
    case 7:
      pcs = DavidSamples.getAllBenchmarks();
      break;
    case 8:
      pcs = Benchmark.getCorinaBenchmarks();
      break;
    case 10:
		pcs = Helper.read(inputfile);
      break;
    default:
      throw new RuntimeException();
    }
    Helper.run(kind, pcs);
  }
}