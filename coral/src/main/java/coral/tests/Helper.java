package coral.tests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import symlib.SymBool;
import symlib.SymLiteral;
import symlib.eval.GenReversePolishExpression;
import symlib.eval.ReversePolish;
import symlib.parser.ParseException;
import symlib.parser.Parser;
import coral.PC;
import coral.solvers.Env;
import coral.solvers.PartitionedCache;
import coral.solvers.Result;
import coral.solvers.Solver;
import coral.solvers.SolverKind;
import coral.solvers.rand.Util;
import coral.util.Config;

public class Helper {

	static int satCount = 0;
	static boolean REPORT_SOLVED = false;
	static boolean REPORT_UNSOLVED = true;

	
  public static void run(final SolverKind kind, String[] pcs)
      throws ParseException, InterruptedException, ExecutionException {
		
		List<String> pcsSolved = new ArrayList<String>();
		List<String> pcsNotSolved = new ArrayList<String>();
		
		PartitionedCache pcache = new PartitionedCache() {
      @Override
      public Env processConstraint(Set<SymBool> cons) throws Exception {
        Solver solver = kind.get();
        return solveAndPrint(solver, new PC(new ArrayList<SymBool>(cons)));
      }
    };
    
		System.out.printf("Running %s to solve %s constraints.  Timeout=%s\n", kind, pcs.length+"", Config.timeout);

		for (int i = 0; i < pcs.length; i++) {
		  
		  // start/end timestamps
	    long startTime = 0;
	    long endTime;
	    if(Config.reportTimeUnsolved) {
	      startTime = System.currentTimeMillis();
	    }
		  
			String pcStr = pcs[i];
			PC pc = (new Parser(pcStr)).parsePC();
			Env env = null;
			char status; /*.=Solved, x=Unsolved, T=Timeout*/

			try {
			  if (Config.partitionConstraints) {
			    env = pcache.analyzeOne(pc.getConstraints());
			  } else {
			    Solver solver = kind.get();
			    env = solveAndPrint(solver, pc);
			  }
			  status = env != null && env.getResult() == Result.SAT ? '.' : 'x';
			} catch (TimeoutException _) {
			  status = 'T';
			} catch (Exception e) {
			  status = 'E';
			  e.printStackTrace();
      }
			
			if(Config.reportTimeUnsolved && (env == null || env.getResult() == Result.UNK)) {
			  endTime = System.currentTimeMillis();
		    Util.reportUnsolvedConstraint(endTime - startTime); 
		  }
			
			boolean isSolved = status == '.';
			satCount += isSolved ? 1 : 0;
			System.out.print(status);
//			System.out.print((i+1)%80==0?"\n":"");

			List<String> list = isSolved ? pcsSolved : pcsNotSolved;

			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append("pc ");
			strBuffer.append(i + 1);
			strBuffer.append(" (");
			strBuffer.append(isSolved?"SOLVED":"NOT SOLVED");
			strBuffer.append(")  ");
			strBuffer.append(pc);			
			strBuffer.append(", ");
//			strBuffer.append(isSolved ? env : "");
			strBuffer.append("\n");			
			list.add(strBuffer.toString());      
		}
		System.out.println();
		
		if (REPORT_UNSOLVED) {
		  for (String line : pcsNotSolved) {
	      System.out.print(line);  
	    }  
		}
		
		if (REPORT_SOLVED) {
		  for (String line : pcsSolved) {
	      System.out.print(line);  
	    }  
		}
		
		if(Config.reportTimeUnsolved) {
      double timeUnsolved = Util.reportAverageTimeOnUnsolvedConstraints();
      System.out.println("Avg. time spent on unsolved constraints (seconds): " + timeUnsolved / 1000);
    }
		
		System.out.printf("Total: %d out of %d solved\n", satCount, pcs.length);
  }

  static boolean useTimeout = false;
  
	public static Env solveAndPrint(final Solver solver, final PC pc)
			throws InterruptedException, ExecutionException, TimeoutException {
	  
	  Object reference = new Object();
		final Object[] hack = new Object[] { reference };
		Runnable solverJob = new Runnable() {
			@Override
			public void run() {
			  try {
			    hack[0] = solver.getCallable(pc).call();
			  } catch (Exception e) {
			    e.printStackTrace();
			  }
			}
		};
		if (useTimeout) {
		  Thread t = new Thread(solverJob);
		  t.start();
		  t.join(Config.timeout);
		  solver.setPleaseStop(true);
		  while (hack[0] == reference) { 
		    Thread.sleep(10);
		  }
		} else {
		  solverJob.run();
		}
		return (Env) hack[0];

//	  return (/*(RandomSolver)*/solver).call(pc/*, null*/);
	}
	
	
	public static String[] read(String queryFile) throws FileNotFoundException,
	IOException {
	  String[] arQuery;
	  List<String> queries = new ArrayList<String>();    
	  //System.out.printf("########### %s ###########\n", queryFile);
	  Reader in = new FileReader(queryFile);
	  BufferedReader br = new BufferedReader(in);
	  String s;
	  while ((s = br.readLine()) != null) {
	    s = s.trim();
	    if (s.equals("") || s.startsWith("#")) {
	      continue;
	    }
	    queries.add(s);
	  }    
	  arQuery = (String[]) queries.toArray(new String[queries.size()]);
	  return arQuery;
	}
	
	public static boolean eval(PC pc, double[] vars) {
    Set<SymLiteral> sortedLits = pc.getSortedVars();
    
    int i = 0;
    for (SymLiteral lit: sortedLits) {
      lit.setCte(vars[i]);
      i++;
    }
    
    List<SymBool> constraints = pc.getConstraints();
    boolean solved = true;
    for (int j = 0; j < constraints.size(); j++) {
      SymBool constraint = constraints.get(j);
      try {
        ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
        if (revPol.eval().intValue() == 0) {
          solved = false;
          break;
        }
      } 
      catch (ArithmeticException _) { }
      catch (NullPointerException _) { System.out.println("NPE: " + constraint); }
    }
    
    return solved;
  }
	
	public static boolean eval(PC pc) {
    List<SymBool> constraints = pc.getConstraints();
    boolean solved = true;
    for (int i = 0; i < constraints.size(); i++) {
      SymBool constraint = constraints.get(i);
      try {
        ReversePolish revPol = GenReversePolishExpression.createReversePolish(constraint);
        if (revPol.eval().intValue() == 0) {
          solved = false;
          break;
        }
      } 
      catch (ArithmeticException _) { }
      catch (NullPointerException _) { System.out.println("NPE: " + constraint); }
    }
    return solved;
  }
	
}