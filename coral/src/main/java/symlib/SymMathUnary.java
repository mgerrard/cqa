package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymMathUnary implements SymDouble {

  // cos, sin, tan: input in radians
  // acos, asin, atan: input in degrees
  // exp, log, log10, round, sqrt: input is any double 
  
  /******* constants *******/
  // radian inputs
  public static final int SIN = 0;
  public static final int COS = 1;
  public static final int TAN = 2;
  // degree inputs
  public static final int ASIN = 3;
  public static final int ACOS = 4;
  public static final int ATAN = 5;
  // any double
  public static final int EXP = 6;
  public static final int LOG = 7;
  public static final int LOG10 = 8;
  public static final int ROUND = 9;
  public static final int SQRT = 10;
  
  public static final String[] symbols = new String[] { 
    "sin", "cos", "tan", 
    "asin", "acos", "atan",
    "exp", "log", "log10", "round", "sqrt"
  };
  
  public static final String[] mathematicaSymbols = new String[] {
    "Sin","Cos","Tan","ArcSin","ArcCos","ArcTan","Exp", "Log", "Log10", "Round", "Sqrt"
  };

  public static String[] logSymbols = new String[] {
    "SIN_", "COS_", "TAN_", 
    "ASIN_", "ACOS_", "ATAN_",
    "EXP_", "LOG_", "LOG10_", "ROUND_", "SQRT_"};
  
  /*******  fields ********/
  private SymDouble arg;
  private int op;

  public SymMathUnary(SymDouble a, int op) {
    this.arg = a;
    this.op = op;
  }

  public static SymDouble create(SymDouble a, int op) {
    return new SymMathUnary(a, op);
  }
  

  public double eval() {
    return eval(((SymDouble) arg).eval(), op);
  }

  private static double eval(double a, int op) {
    double result;
    switch (op) {
    case SIN:
      result = Math.sin(a);
      break;
    case COS:
      result = Math.cos(a);
      break;
    case TAN:
      result = Math.tan(a);
      break;
    case ASIN:
      result = Math.asin(a);
      break;
    case ACOS:
      result = Math.acos(a);
      break;
    case ATAN:
      result = Math.atan(a);
      break;      
    case EXP:
      result = Math.exp(a);
      break;      
    case LOG:
      result = Math.log(a);
      break;      
    case LOG10:
      result = Math.log10(a);
      break;
    case ROUND:
      result = Math.round(a);
      break;
    case SQRT:
      result = Math.sqrt(a);
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }
  
  public static int getOp(String strOp) {
    int result = -1;
    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }
    return result;
  }
  
  public SymDouble getArg() {
    return arg;
  }
  
  public int getOp() {
    return op;
  }
  
  @Override
  public int compareTo(SymNumber o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public SymNumber clone() {
    return create((SymDouble) this.arg.clone(), op);
  }
  
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(symbols[op]);
    builder.append("(");
    builder.append(arg.toString());
    builder.append(")");

    return builder.toString();
  }

  /********  visitors  ********/

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymDouble(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    throw new UnsupportedOperationException();
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  public String getMathematicaOp() {
    return mathematicaSymbols[op];
  }
  
}