package symlib;

import java.util.Comparator;

public class SymLiteralComparator implements Comparator<SymLiteral> {

  @Override
  public int compare(SymLiteral o1, SymLiteral o2) {
    if (o1.getId() < o2.getId()) {
      return -1;
    } else {
      return 1;
    }
  }

}
