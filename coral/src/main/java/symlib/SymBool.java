package symlib;

import coral.util.visitors.interfaces.GenericVisitor;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public abstract class SymBool {

  public abstract boolean evalBool();

  public abstract SymBool neg();

  public abstract SymBool clone();

  public String accept(StringVisitor visitor) {
    return visitor.visitSymBool(this);
  }

  public void accept(VoidVisitor visitor) {
    visitor.visitSymBool(this);
  }

  public SymBool accept(TypedVisitor visitor) {
    return visitor.visitSymBool(this);
  }
  
  public int hashCode() {
    return this.toString().hashCode();
  }

  public <T> T accept(GenericVisitor<T> visitor) {
    return visitor.visitSymBool(this);
  }

}
