package symlib;


public interface SymInt extends SymNumber {

  public int eval();

}
