package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymFloatArith extends SymBinaryExpression implements SymFloat {
  private boolean constainsLiteral;

  // double arith
  public static final int ADD = 0;

  public static final int SUB = 1;

  public static final int MULT = 2;

  public static final int DIV = 3;

  public static final int MOD = 4;

  public static String[] symbols = new String[] { "+", "-", "*", "/", "%" };

  public static String[] logSymbols = new String[] { "ADD", "SUB", "MUL", "DIV",
  "MOD" };

  public static SymFloat create(SymFloat a, SymFloat b, int op) {
    SymFloat result;

    if (!a.containsLiteral() && !b.containsLiteral()) {
      if (b.eval() == 0 && (op == DIV || op == MOD)) {
        throw new ArithmeticException("division by zero.");
      } else {
        result = Util.createConstant(eval(a.eval(), b.eval(), op));
      }
    } else {
      result = new SymFloatArith(a, b, op);
      ((SymFloatArith) result).constainsLiteral = true;
    }

    return result;
  }

  private SymFloatArith(SymFloat a, SymFloat b, int op) {
    super(a, b, op);
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public SymFloat getA() {
    return (SymFloat) a;
  }

  public SymFloat getB() {
    return (SymFloat) b;
  }

  public void setA(SymFloat a) {
    this.a = a;
    this.constainsLiteral = a.containsLiteral();
  }

  public void setB(SymFloat b) {
    this.b = b;
    this.constainsLiteral = b.containsLiteral();
  }

  public int getOp() {
    return op;
  }

  public float eval() {
    return eval(((SymFloat) a).eval(), ((SymFloat) b).eval(), op);
  }

  private static float eval(float a, float b, int op) {
    float result;
    switch (op) {
    case ADD:
      result = a + b;
      break;
    case SUB:
      result = a - b;
      break;
    case MULT:
      result = a * b;
      break;
    case DIV:
      result = a / b;
      break;
    case MOD:
      result = a % b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(a);
    builder.append(symbols[op]);
    builder.append(b);
    builder.append(")");

    return builder.toString();
  }

  public boolean containsLiteral() {
    return this.constainsLiteral;
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymFloat(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }


  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return logSymbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymFloatArith other = (SymFloatArith) o;

      boolean result = true;

      result = result && (other.a.equals(this.a));
      result = result && (other.b.equals(this.b));
      result = result && (other.op == this.op);

      return result;
    }
  }


  /**
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    result += 3 + this.a.hashCode() + this.b.hashCode() + this.op;

    return result;
  }

  @Override
  public SymNumber clone() {
    SymFloat a = (SymFloat) this.a.clone();
    SymFloat b = (SymFloat) this.b.clone();
    return SymFloatArith.create(a, b, op);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }

  public String getSymbolOp() {
    return symbols[op];
  }
}
