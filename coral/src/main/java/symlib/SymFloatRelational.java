package symlib;

public class SymFloatRelational extends SymBool {
  private boolean constainsLiteral;

  public static final int GT = 0;

  public static final int LT = 1;

  public static final int LE = 2;

  public static final int GE = 3;

  public static final int EQ = 4;

  public static final int NE = 5;

  public static String[] symbols = new String[] { ">", "<", "<=", ">=", "==", "!=" };

  public static String[] logSymbols = new String[] { "FGT", "FLT", "FLE", "FGE",
    "FEQ", "FNE" };

  static int[] neg = new int[] { LE, GE, GT, LT, NE, EQ };

  static int[] results = new int[] { 1, -1, 0, 0, 0, 1 };

  static int[] resultsNeg = new int[] { 0, 0, 1, -1, 1, 0 };

  SymFloat a, b;

  int op;

  private SymFloatRelational(SymFloat a, SymFloat b, int op) {
    super();
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public static SymBool create(SymFloat a, SymFloat b, int op) {
    SymBool result;

    if (!a.containsLiteral() && !b.containsLiteral()) {
      result = Util.createConstant(eval(a.eval(), b.eval(), op));
    } else {
      result = new SymFloatRelational(a, b, op);
      ((SymFloatRelational) result).constainsLiteral = true;
    }

    return result;
  }

  public SymFloat getA() {
    return a;
  }

  public SymFloat getB() {
    return b;
  }

  public void setA(SymFloat a) {
    this.a = a;
  }

  public void setB(SymFloat b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  public boolean evalBool() {
    return eval(a.eval(), b.eval(), op);
  }

  private static boolean eval(double a, double b, int op) {
    boolean result;
    switch (op) {
    case GT:
      result = a > b;
      break;
    case LT:
      result = a < b;
      break;
    case LE:
      result = a <= b;
      break;
    case GE:
      result = a >= b;
      break;
    case EQ:
      result = a == b;
      break;
    case NE:
      result = a != b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(a.toString());
    builder.append(" ");
    builder.append(symbols[op]);
    builder.append(" ");
    builder.append(b.toString());
    builder.append(")");

    return builder.toString();
  }

  public SymBool neg() {
    return new SymFloatRelational(a, b, neg[op]);
  }

  public boolean containsLiteral() {
    return this.constainsLiteral;
  }

  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return symbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymFloatRelational other = (SymFloatRelational) o;

      boolean result = true;

      if (this.op == SymFloatRelational.EQ) {
        result = result
        && (other.a.equals(this.a) || other.a.equals(this.b));
        result = result
        && (other.b.equals(this.b) || other.b.equals(this.a));
        result = result && (other.op == this.op);
      } else {
        result = result && (other.a.equals(this.a));
        result = result && (other.b.equals(this.b));
        result = result && (other.op == this.op);
      }

      return result;
    }
  }

  @Override
  public SymBool clone() {
    SymFloat a = (SymFloat) this.a.clone();
    SymFloat b = (SymFloat) this.b.clone();
    return SymFloatRelational.create(a, b, op);
  }

  public String getSymbolOp() {
    return symbols[op];
  }
}
