package symlib;


public class SymDoubleRelational extends SymBool {

  public static final int GT = 0;

  public static final int LT = 1;

  public static final int LE = 2;

  public static final int GE = 3;

  public static final int EQ = 4;

  public static final int NE = 5;

  public static String[] symbols = new String[] { ">", "<", "<=", ">=", "==", "!=" };

  public static String[] logSymbols = new String[] { "DGT", "DLT", "DLE", "DGE",
    "DEQ", "DNE" };

  static int[] neg = new int[] { LE, GE, GT, LT, NE, EQ };

  static int[] results = new int[] { 1, -1, 0, 0, 0, 1 };

  static int[] resultsNeg = new int[] { 0, 0, 1, -1, 1, 0 };

  SymDouble a, b;

  int op;

  public static SymBool create(SymDouble a, SymDouble b, int op) {
    return new SymDoubleRelational(a, b, op);
  }

  public SymDoubleRelational(SymDouble a, SymDouble b, int op) {
    super();
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public SymDouble getA() {
    return a;
  }

  public SymDouble getB() {
    return b;
  }

  public void setA(SymDouble a) {
    this.a = a;
  }

  public void setB(SymDouble b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  public boolean evalBool() {
    return eval(a.eval(), b.eval(), op);
  }

  private static boolean eval(double a, double b, int op) {
    double ra = a, rb = b;
  	boolean result;
    switch (op) {
    case GT:
      result = ra > rb;
      break;
    case LT:
      result = ra < rb;
      break;
    case LE:
      result = ra <= rb;
      break;
    case GE:
      result = ra >= rb;
      break;
    case EQ:
      result = ra == rb;
      break;
    case NE:
      result = ra != rb;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(a.toString());
    builder.append(" ");
    builder.append(symbols[op]);
    builder.append(" ");
    builder.append(b.toString());
    builder.append(")");

    return builder.toString();
  }

  public SymBool neg() {
    return new SymDoubleRelational(a, b, neg[op]);
  }

  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return symbols[op];
  }
  

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymDoubleRelational other = (SymDoubleRelational) o;

      boolean result = true;

      if (this.op == SymDoubleRelational.EQ) {
        result = result
        && (other.a.equals(this.a) || other.a.equals(this.b));
        result = result
        && (other.b.equals(this.b) || other.b.equals(this.a));
        result = result && (other.op == this.op);
      } else {
        result = result && (other.a.equals(this.a));
        result = result && (other.b.equals(this.b));
        result = result && (other.op == this.op);
      }

      return result;
    }
  }

  @Override
  public SymBool clone() {
    SymDouble a = (SymDouble) this.a.clone();
    SymDouble b = (SymDouble) this.b.clone();
    return SymDoubleRelational.create(a, b, op);
  }

  public String getSymbolOp() {
    return symbols[op];
  }

  
}