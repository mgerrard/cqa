package symlib;

import coral.util.Config;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymFloatLiteral implements SymFloat, SymLiteral {

  /**********************************************
   * concolic execution enables one to interpret 
   * a symbolic variable on the concrete domain
   * using a fixed random value. 
   **********************************************/
  int id;
  float val;

  public SymFloatLiteral(float concreteVal) {
    this.id =  Util.newID();
    this.val = concreteVal;
  }

  public static SymFloatLiteral createForParsing(int id) {
    SymFloatLiteral result = new SymFloatLiteral(0);
    result.id = id;
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("$V");
    builder.append(id);
    if(Config.showConcreteValues) {
      builder.append("(");
      builder.append(val);
      builder.append(")");
    }

    return builder.toString();
  }

  public boolean containsLiteral() {
    return true;
  }

  public float eval() {
    return val;
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymFloat(this);
  }

  @Override
  public void setCte(Number val) {
    this.val = val.floatValue();
  }
  
  @Override
  public Number getCte() {
    return val;
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }


  @Override
  public int getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymFloatLiteral other = (SymFloatLiteral) o;

      return (other.id == this.id) ? true : false;
    }
  }

  @Override
  public SymNumber clone() {
    return this;
  }
  
 @Override
  public int hashCode() {
    int result = 0;

    result += 3 + this.id;

    return result;
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
}
