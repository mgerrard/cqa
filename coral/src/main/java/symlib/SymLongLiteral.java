package symlib;

import coral.util.Config;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymLongLiteral implements SymLong, SymLiteral {

  /**********************************************
   * concolic execution enables one to interpret a symbolic variable on the
   * concrete domain using a fixed random value.
   **********************************************/
  int id;
  long val;

  public SymLongLiteral(long concreteVal) {
    this.id = Util.newID();
    this.val = concreteVal;
  }

  public static SymLongLiteral createForParsing(int id) {
    SymLongLiteral result = new SymLongLiteral(0);
    result.id = id;
    return result;
  }

  public String toString() {
    StringBuilder builder = createLiteralString();

    return builder.toString();
  }

  protected StringBuilder createLiteralString() {
    StringBuilder builder = new StringBuilder();

    builder.append("$V");
    builder.append(id);
    if(Config.showConcreteValues) {
      builder.append("(");
      builder.append(val);
      builder.append(")");
    }
    return builder;
  }

  public boolean containsLiteral() {
    return true;
  }

  public long eval() {
    return val;
  }

  @Override
  public void setCte(Number val) {
    this.val = val.longValue();
  }
  
  @Override
  public Number getCte() {
    return val;
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymLong(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }


  @Override
  public int getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymLongLiteral other = (SymLongLiteral) o;

      return (other.id == this.id) ? true : false;
    }
  }

  @Override
  public SymNumber clone() {
    return this;
  }

  /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    result += 5 + this.id;

    return result;
  }

  
  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
}
