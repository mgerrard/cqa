package symlib;

public interface SymDouble extends SymNumber {

  // concolic
  public double eval();

}
