package symlib;

public class Util {

  private static int id = 1;

  public static int newID() {
    return id++;
  }

  public static final SymInt ZERO = Util.createConstant(0);
  public static final SymInt ONE = Util.createConstant(1);
  public static final SymBool TRUE = SymBoolConstant.TRUE;
  public static final SymBool FALSE = SymBoolConstant.FALSE;
  public static final SymFloat ZEROF = Util.createConstant(0.0f);
  public static final SymDouble ZEROD = Util.createConstant(0.0);
  public static final SymLong ZEROL = Util.createConstant(0l);

  public static void resetID() {
    id = 1;
  }

  // factories
  public static SymIntLiteral createSymLiteral(int cte) {
    return new SymIntLiteral(cte);
  }

  public static SymDoubleLiteral createSymLiteral(double val) {
    return new SymDoubleLiteral(val);
  }

  public static SymFloatLiteral createSymLiteral(float val) {
    return new SymFloatLiteral(val);
  }

  public static SymLongLiteral createSymLiteral(long val) {
    return new SymLongLiteral(val);
  }

  public static SymBoolLiteral createSymLiteral(boolean val) {
    return new SymBoolLiteral(val);
  }

  public static SymNumber createConstant(Number val) {
    SymNumber result;
    if (val instanceof Integer) {
      result = createConstant(val.intValue()); 
    } else if (val instanceof Long) {
      result = createConstant(val.longValue()); 
    } else if (val instanceof Double) {
      result = createConstant(val.doubleValue()); 
    } else if (val instanceof Float) {
      result = createConstant(val.floatValue()); 
    } else {
      throw new UnsupportedOperationException();
    }    
    return result;
  }
  
  // factories for constants
  public static SymInt createConstant(int val) {
    return new SymIntConstant(val);
  }

  public static SymLong createConstant(long val) {
    return new SymLongConstant(val);
  }

  public static SymDouble createConstant(double val) {
    return new SymDoubleConstant(val);
  }

  public static SymFloat createConstant(float val) {
    return new SymFloatConstant(val);
  }

  public static SymBool createConstant(boolean val) {
    return val?TRUE:FALSE;
  }

  // SymBool

  public static SymBool eq(SymBool arg1, SymBool arg2) {
    return SymBoolRelational.create(arg1, arg2, SymBoolRelational.EQ);
  }

  public static SymBool ne(SymBool arg1, SymBool arg2) {
    return SymBoolRelational.create(arg1, arg2, SymBoolRelational.NE);
  }

  public static SymBool or(SymBool arg1, SymBool arg2) {
    return SymBoolOperations.create(arg1, arg2, SymBoolOperations.OR);
  }

  public static SymBool and(SymBool arg1, SymBool arg2) {
    return SymBoolOperations.create(arg1, arg2, SymBoolOperations.AND);
  }

  public static SymBool xor(SymBool arg1, SymBool arg2) {
    return SymBoolOperations.create(arg1, arg2, SymBoolOperations.XOR);
  }

  public static SymBool neg(SymBool arg1) {
    return SymBoolOperations.create(arg1, null, SymBoolOperations.NEG);
  }

  // SymInt: relational
  public static SymBool gt(SymInt arg1, SymInt arg2) {
    return SymIntRelational.create(arg1, arg2, SymIntRelational.GT);
  }

  public static SymBool lt(SymInt arg1, SymInt arg2) {
    return SymIntRelational.create(arg1, arg2, SymIntRelational.LT);
  }

  public static SymBool ge(SymInt arg1, SymInt arg2) {
    return SymIntRelational.create(arg1, arg2, SymIntRelational.GE);
  }

  public static SymBool le(SymInt arg1, SymInt arg2) {
    return SymIntRelational.create(arg1, arg2, SymIntRelational.LE);
  }

  public static SymBool eq(SymInt arg1, SymInt arg2) {
    return SymIntRelational.create(arg1, arg2, SymIntRelational.EQ);
  }

  public static SymBool ne(SymInt arg1, SymInt arg2) {
    return SymIntRelational.create(arg1, arg2, SymIntRelational.NE);
  }

  // SymInt: arithmetic
  public static SymInt add(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.ADD);
  }

  public static SymInt sub(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.SUB);
  }

  public static SymInt mul(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.MULT);
  }

  public static SymInt div(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.DIV);
  }

  public static SymInt mod(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.MOD);
  }

  // SymInt: bitwise int
  public static SymInt and(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.AND);
  }

  public static SymInt or(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.OR);
  }

  public static SymInt xor(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.XOR);
  }

  public static SymInt sl(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.SHIFT_LEFT);
  }

  public static SymInt sr(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.SHIFT_RIGHT);
  }

  public static SymInt usr(SymInt arg1, SymInt arg2) {
    return SymIntArith.create(arg1, arg2, SymIntArith.UNSIGNED_SHIFT_RIGHT);
  }

  public static SymInt cmp(SymInt arg1) {
    return SymIntArith.create(arg1, null, SymIntArith.CMP);
  }

  // SymLong: arithmetic
  public static SymLong add(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.ADD);
  }

  public static SymLong sub(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.SUB);
  }

  public static SymLong mul(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.MULT);
  }

  public static SymLong div(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.DIV);
  }

  public static SymLong mod(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.MOD);
  }

  // SymLong: bitwise
  public static SymLong and(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.AND);
  }

  public static SymLong or(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.OR);
  }

  public static SymLong xor(SymLong arg1, SymLong arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.XOR);
  }

  public static SymLong arithmeticShiftLeft(SymLong arg1, SymInt arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.SHIFT_LEFT);
  }

  public static SymLong arithmeticShiftRight(SymLong arg1, SymInt arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.SHIFT_RIGHT);
  }

  public static SymLong logicalShiftRight(SymLong arg1, SymInt arg2) {
    return SymLongArith.create(arg1, arg2, SymLongArith.UNSIGNED_SHIFT_RIGHT);
  }

  // SymLong: relational
  public static SymBool gt(SymLong arg1, SymLong arg2) {
    return SymLongRelational.create(arg1, arg2, SymLongRelational.GT);
  }

  public static SymBool lt(SymLong arg1, SymLong arg2) {
    return SymLongRelational.create(arg1, arg2, SymLongRelational.LT);
  }

  public static SymBool ge(SymLong arg1, SymLong arg2) {
    return SymLongRelational.create(arg1, arg2, SymLongRelational.GE);
  }

  public static SymBool le(SymLong arg1, SymLong arg2) {
    return SymLongRelational.create(arg1, arg2, SymLongRelational.LE);
  }

  public static SymBool eq(SymLong arg1, SymLong arg2) {
    return SymLongRelational.create(arg1, arg2, SymLongRelational.EQ);
  }

  public static SymBool ne(SymLong arg1, SymLong arg2) {
    return SymLongRelational.create(arg1, arg2, SymLongRelational.NE);
  }

  // SymDouble: arithmetic
  public static SymDouble add(SymDouble arg1, SymDouble arg2) {
    return SymDoubleArith.create(arg1, arg2, SymDoubleArith.ADD);
  }

  public static SymDouble sub(SymDouble arg1, SymDouble arg2) {
    return SymDoubleArith.create(arg1, arg2, SymDoubleArith.SUB);
  }

  public static SymDouble mul(SymDouble arg1, SymDouble arg2) {
    return SymDoubleArith.create(arg1, arg2, SymDoubleArith.MULT);
  }

  public static SymDouble div(SymDouble arg1, SymDouble arg2) {
    return SymDoubleArith.create(arg1, arg2, SymDoubleArith.DIV);
  }

  public static SymDouble mod(SymDouble arg1, SymDouble arg2) {
    return SymDoubleArith.create(arg1, arg2, SymDoubleArith.MOD);
  }

  // SymDouble: relational
  public static SymBool gt(SymDouble arg1, SymDouble arg2) {
    return new SymDoubleRelational(arg1, arg2, SymDoubleRelational.GT);
  }

  public static SymBool lt(SymDouble arg1, SymDouble arg2) {
    return new SymDoubleRelational(arg1, arg2, SymDoubleRelational.LT);
  }

  public static SymBool ge(SymDouble arg1, SymDouble arg2) {
    return new SymDoubleRelational(arg1, arg2, SymDoubleRelational.GE);
  }

  public static SymBool le(SymDouble arg1, SymDouble arg2) {
    return new SymDoubleRelational(arg1, arg2, SymDoubleRelational.LE);
  }

  public static SymBool eq(SymDouble arg1, SymDouble arg2) {
    return new SymDoubleRelational(arg1, arg2, SymDoubleRelational.EQ);
  }

  public static SymBool ne(SymDouble arg1, SymDouble arg2) {
    return new SymDoubleRelational(arg1, arg2, SymDoubleRelational.NE);
  }

  // SymFloat: arithmetic
  public static SymFloat add(SymFloat arg1, SymFloat arg2) {
    return SymFloatArith.create(arg1, arg2, SymFloatArith.ADD);
  }

  public static SymFloat sub(SymFloat arg1, SymFloat arg2) {
    return SymFloatArith.create(arg1, arg2, SymFloatArith.SUB);
  }

  public static SymFloat mul(SymFloat arg1, SymFloat arg2) {
    return SymFloatArith.create(arg1, arg2, SymFloatArith.MULT);
  }

  public static SymFloat div(SymFloat arg1, SymFloat arg2) {
    return SymFloatArith.create(arg1, arg2, SymFloatArith.DIV);
  }

  public static SymFloat mod(SymFloat arg1, SymFloat arg2) {
    return SymFloatArith.create(arg1, arg2, SymFloatArith.MOD);
  }

  // SymFloat: relational
  public static SymBool gt(SymFloat arg1, SymFloat arg2) {
    return SymFloatRelational.create(arg1, arg2, SymFloatRelational.GT);
  }

  public static SymBool lt(SymFloat arg1, SymFloat arg2) {
    return SymFloatRelational.create(arg1, arg2, SymFloatRelational.LT);
  }

  public static SymBool ge(SymFloat arg1, SymFloat arg2) {
    return SymFloatRelational.create(arg1, arg2, SymFloatRelational.GE);
  }

  public static SymBool le(SymFloat arg1, SymFloat arg2) {
    return SymFloatRelational.create(arg1, arg2, SymFloatRelational.LE);
  }

  public static SymBool eq(SymFloat arg1, SymFloat arg2) {
    return SymFloatRelational.create(arg1, arg2, SymFloatRelational.EQ);
  }

  public static SymBool ne(SymFloat arg1, SymFloat arg2) {
    return SymFloatRelational.create(arg1, arg2, SymFloatRelational.NE);
  }
  
//  public static SymBool mixedEq(SymDouble arg1, SymInt arg2){
//    return new SymMixedEquality(arg1, arg2);
//  }
  
  public static SymDouble createASDouble(SymNumber arg){
    return SymAsDouble.cast(arg);
  }
  
  public static SymInt createASInt(SymNumber arg){
    return new SymAsInt(arg);
  }

  // double functions
  public static SymDouble sin(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.SIN);
  }
  
  public static SymDouble cos(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.COS);
  }
  
  public static SymDouble tan(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.TAN);
  }
  
  public static SymDouble asin(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.ASIN);
  }
  
  public static SymDouble acos(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.ACOS);
  }
  
  public static SymDouble atan(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.ATAN);
  }
  
  public static SymDouble exp(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.EXP);
  }
  
  public static SymDouble log(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.LOG);
  }
  
  public static SymDouble log10(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.LOG10);
  }
  
  public static SymDouble round(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.ROUND);
  }
  
  public static SymDouble sqrt(SymDouble d1) {
    return new SymMathUnary(d1, SymMathUnary.SQRT);
  }
  
  public static SymDouble atan2(SymDouble d1, SymDouble d2) {
    return new SymMathBinary(d1, d2, SymMathBinary.ATAN2);
  }
  
  public static SymDouble pow(SymDouble d1, SymDouble d2) {
    return new SymMathBinary(d1, d2, SymMathBinary.POW);
  }
  
}
