package symlib.eval;

import java.util.ArrayList;
import java.util.List;

import symlib.SymAsDouble;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleRelational;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatRelational;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLongArith;
import symlib.SymLongConstant;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;
import coral.util.visitors.adaptors.VoidVisitorAdaptor;

public class GenReversePolishExpression extends VoidVisitorAdaptor {
  
  
  public static ReversePolish createReversePolish(SymBool constraint) {
    GenReversePolishExpression genRevPol = new GenReversePolishExpression();
    genRevPol.visitSymBool(constraint);
    Elem[] elems = genRevPol.getElems();
    return new ReversePolish(elems);
  }
  
  public static ReversePolish createReversePolish(SymNumber expr) {
    GenReversePolishExpression genRevPol = new GenReversePolishExpression();
    genRevPol.visitSymNumber(expr);
    Elem[] elems = genRevPol.getElems();
    return new ReversePolish(elems);
  }
  
  private List<Elem> elems = new ArrayList<Elem>();
  
//  @Override
//  protected void visitSyMixedEquality(SymMixedEquality bool) {
//    super.visitSyMixedEquality(bool);
//    elems.add(OP.DEQ/*implementation is the same.*/);
//  }
  
  @Override
  protected void visitSymAsDouble(SymAsDouble number) {
    super.visitSymAsDouble(number);
    elems.add(OP.ASDOUBLE);
  }

  @Override
  protected void visitSymIntRelational(SymIntRelational bool) {
    super.visitSymIntRelational(bool);
    Elem elem;
    switch (bool.getOp()) {
    case 0:
      elem = OP.IGT;
      break;
    case 1:
      elem = OP.ILT;
      break;
    case 2:
      elem = OP.ILE;
      break;
    case 3:
      elem = OP.IGE;
      break;
    case 4:
      elem = OP.IEQ;
      break;
    case 5:
      elem = OP.INE;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymLongRelational(SymLongRelational bool) {
    super.visitSymLongRelational(bool);
    Elem elem;
    switch (bool.getOp()) {
    case 0:
      elem = OP.LGT;
      break;
    case 1:
      elem = OP.LLT;
      break;
    case 2:
      elem = OP.LLE;
      break;
    case 3:
      elem = OP.LGE;
      break;
    case 4:
      elem = OP.LEQ;
      break;
    case 5:
      elem = OP.LNE;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymFloatRelational(SymFloatRelational bool) {
    super.visitSymFloatRelational(bool);
    Elem elem;
    switch (bool.getOp()) {
    case 0:
      elem = OP.FGT;
      break;
    case 1:
      elem = OP.FLT;
      break;
    case 2:
      elem = OP.FLE;
      break;
    case 3:
      elem = OP.FGE;
      break;
    case 4:
      elem = OP.FEQ;
      break;
    case 5:
      elem = OP.FNE;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }
  
  @Override
  protected void visitSymDoubleRelational(SymDoubleRelational bool) {
    super.visitSymDoubleRelational(bool);
    Elem elem;
    switch (bool.getOp()) {
    case 0:
      elem = OP.DGT;
      break;
    case 1:
      elem = OP.DLT;
      break;
    case 2:
      elem = OP.DLE;
      break;
    case 3:
      elem = OP.DGE;
      break;
    case 4:
      elem = OP.DEQ;
      break;
    case 5:
      elem = OP.DNE;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }
  
  @Override
  protected void visitSymBoolOperations(SymBoolOperations bool) {
    super.visitSymBoolOperations(bool);
    Elem elem;
    switch (bool.getOp()) {
    case 0:
      elem = OP.BAND;
      break;
    case 1:
      elem = OP.BOR;
      break;
    case 2:
      elem = OP.BNEG;
      break;
    case 3:
      elem = OP.BXOR;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }
  
  Cte ctrue = new Cte(1);
  Cte cfalse = new Cte(0);
  
  @Override
  protected void visitSymBoolConstant(SymBoolConstant bool) {
    elems.add(bool.evalBool()?ctrue:cfalse);
  }
  
  @Override
  protected void visitSymBoolLiteral(SymBoolLiteral bool) {
    throw new UnsupportedOperationException();
  }
  
  @Override
  protected void visitSymBoolRelational(SymBoolRelational bool) {
    super.visitSymBoolRelational(bool);
    Elem elem;
    switch (bool.getOp()) {
    case 0:
      elem = OP.BEQ;
      break;
    case 1:
      elem = OP.BNEQ;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }
  
  @Override
  protected void visitSymDoubleConstant(SymDoubleConstant number) { 
    elems.add(new Cte(number.evalNumber()));
  }

  @Override
  protected void visitSymFloatConstant(SymFloatConstant number) { 
    elems.add(new Cte(number.evalNumber()));    
  }

  @Override
  protected void visitSymLongConstant(SymLongConstant number) { 
    elems.add(new Cte(number.evalNumber()));    
  }

  @Override
  protected void visitSymIntConstant(SymIntConstant number) { 
    elems.add(new Cte(number.evalNumber()));    
  }

  @Override
  protected void visitSymLiteral(SymLiteral number) {
    elems.add(new Lit((SymLiteral)number));
  }

  @Override
  protected void visitSymMathUnary(SymMathUnary number) {
    super.visitSymMathUnary(number);
    Elem elem;
    switch (number.getOp()) {
    case 0:
      elem = OP.SIN;
      break;
    case 1:
      elem = OP.COS;
      break;
    case 2:
      elem = OP.TAN;
      break;
    case 3:
      elem = OP.ASIN;
      break;
    case 4:
      elem = OP.ACOS;
      break;      
    case 5:
      elem = OP.ATAN;
      break;
    case 6:
      elem = OP.EXP;
      break;
    case 7:
      elem = OP.LOG;
      break;
    case 8:
      elem = OP.LOG10;
      break;
    case 9:
      elem = OP.ROUND;
      break;
    case 10:
      elem = OP.SQRT;
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymMathBinary(SymMathBinary number) {
    super.visitSymMathBinary(number);
    Elem elem;
    switch (number.getOp()) {
    case 0:
      elem = OP.ATAN2;
      break;
    case 1:
      elem = OP.POW;
      break;
    case 2:
      elem = OP.MAX;
      break;
    case 3:
      elem = OP.MIN;
      break;
    case 4:
      elem = OP.IDIV; //use the default integer division implementation
      break;
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymDoubleArith(SymDoubleArith number) {
    super.visitSymDoubleArith(number);
    Elem elem;
    switch (number.getOp()) {
    case 0:
      elem = OP.DADD;
      break;
    case 1:
      elem = OP.DSUB;
      break;
    case 2:
      elem = OP.DMULT;
      break;
    case 3:
      elem = OP.DDIV;
      break;
    case 4:
      elem = OP.DMOD;
      break;      
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymFloatArith(SymFloatArith number) {
    super.visitSymFloatArith(number);
    Elem elem;
    switch (number.getOp()) {
    case 0:
      elem = OP.FADD;
      break;
    case 1:
      elem = OP.FSUB;
      break;
    case 2:
      elem = OP.FMULT;
      break;
    case 3:
      elem = OP.FDIV;
      break;
    case 4:
      elem = OP.FMOD;
      break;      
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymLongArith(SymLongArith number) {
    super.visitSymLongArith(number);
    Elem elem;
    switch (number.getOp()) {
    case 0:
      elem = OP.LADD;
      break;
    case 1:
      elem = OP.LSUB;
      break;
    case 2:
      elem = OP.LMULT;
      break;
    case 3:
      elem = OP.LDIV;
      break;
    case 4:
      elem = OP.LMOD;
      break;      
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }

  @Override
  protected void visitSymIntArith(SymIntArith number) {
    super.visitSymIntArith(number);
    Elem elem;
    switch (number.getOp()) {
    case 0:
      elem = OP.IADD;
      break;
    case 1:
      elem = OP.ISUB;
      break;
    case 2:
      elem = OP.IMULT;
      break;
    case 3:
      elem = OP.IDIV;
      break;
    case 4:
      elem = OP.IMOD;
      break;      
    default:
      throw new UnsupportedOperationException();
    }
    elems.add(elem);
  }
  
  public Elem[] getElems() {
     Elem[] result = new Elem[elems.size()];
     elems.toArray(result);
     return result;
  }

}