package symlib.eval;

public class Cte implements Elem {
  public Cte(Number val) {
    super();
    this.val = val;
  }
  Number val;
  @Override
  public void eval(ReversePolish revPol) {
    revPol.push(val);
  }
  public String toString() { return val.toString(); }
}