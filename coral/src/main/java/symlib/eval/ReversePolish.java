package symlib.eval;

/**
 * 
 * this is a template showing how we can obtain 
 * much faster evaluation.  
 * 
 * note that sub-expression are identified and 
 * possibly evaluated with specified intervals.
 * 
 * see examples in main
 * 
 * @author damorim
 *
 */
public class ReversePolish  {

  private Number[] stack;
  private int next;
  private Elem[] elems;
  private Number  leftSide;
  public Number getLeftSide() {
    return leftSide;
  }

  public Number getRightSide(){
    return stack[1];
  }

  public ReversePolish(Elem[] elems) {
    this.stack = new Number[100];
    this.next = 0;
    this.elems = elems;
  }

  Number pop() { return stack[--next]; }  
  void push(Number val) { stack[next++] = val; }

  public Number eval() {
    int lo = 0/*inclusive*/;
    int hi = elems.length/*exclusive*/;
    for (int i = lo; i < hi; i++) {
      leftSide = i == hi -1 ? stack[0]: null;
      Elem elem = elems[i];
      elem.eval(this);
    }
    if (next != 1) {
      throw new RuntimeException("ERROR:" + next + " -> " + stack);
    }
    return stack[--next];
  }

  public boolean isSAT() {
    return this.eval().intValue() != 0;
  }
}