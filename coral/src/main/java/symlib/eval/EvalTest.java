package symlib.eval;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import coral.PC;
import coral.counters.CountingProblem;
import coral.counters.DistributionAwareQCoral;
import coral.counters.Main;
import coral.counters.refactoring.CountingUtils;
import coral.counters.refactoring.Partitioner;
import coral.counters.refactoring.Runner;
import coral.counters.refactoring.problem.Domain;
import coral.counters.refactoring.problem.PCProblem;
import coral.counters.refactoring.problem.PartitionProblem;
import coral.util.Config;
import coral.util.Interval;
import coral.util.options.Options;
import java.util.stream.Collectors;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;
import symlib.SymLiteral;
import symlib.eval.compilation.JavaPcCompiler;
import symlib.eval.compilation.PcCompiler;
import symlib.eval.compilation.TccPcCompiler;
import symlib.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by mateus on 27/12/16.
 */
public class EvalTest {

  private static Options options = new Options(Main.class, Config.class);

  static final int N_EVAL = 1000000;

  public static void main(String[] args) throws FileNotFoundException, ParseException, InvocationTargetException, IllegalAccessException {

    long executionStart = System.nanoTime();
    args = processOptions(args);
    RandomGenerator rng = new Well44497b(Config.mcSeed);

    File input = new File(args[0]);
    Scanner s = new Scanner(input);
    List<CountingProblem> problems = DistributionAwareQCoral.processInput(s, rng);

    List<PCProblem> pcProblems = Runner.buildModel(problems);

    // Partitioning
    Multimap<PartitionProblem, PCProblem> part2pcs = HashMultimap.create();
    Partitioner partitioner = new Partitioner();
    Partitioner.PartitionData<PCProblem> partitionData = partitioner.partition(pcProblems);
    Set<PartitionProblem> partProblems = new TreeSet<>(
        (o1, o2) -> o1.getPC().toCompareStrings().compareTo(o2.getPC().toCompareStrings()));
    partProblems.addAll(partitionData.partitions);

    if (args[1].equalsIgnoreCase("compiled")) {
      long start = System.nanoTime();
      PcCompiler compiler = new JavaPcCompiler();
      for (PartitionProblem pp : partProblems) {
        PC pc = pp.getPC();
        compiler.include(pc);
      }
      Map<PC, Evaluator> pcToMethod = compiler.compile();
      long compilationCost = System.nanoTime() - start;

      System.out.println("compilation cost: " + compilationCost / Math.pow(10.0, 9));
      int i = 0;
      for (PartitionProblem pp : partProblems) {
        start = System.nanoTime();
        PC pc = pp.getPC();
        Evaluator pcEval = pcToMethod.get(pc);
        Set<SymLiteral> lits = pc.getSortedVars();
        Number[] mArgs = new Number[lits.size()];

        Domain domain = pp.getDomain();
        //assuming everything is a double for this small experiment
        long hits = sample(rng, pcEval, mArgs, domain);

        long evaluationCost = System.nanoTime() - start;
        System.out.println(hits / (double) N_EVAL + "," + evaluationCost / Math.pow(10.0, 9));
      }
    } else if (args[1].equalsIgnoreCase("compiled-obo")) {
      long totalCompilationCost = 0;
      for (PartitionProblem pp : partProblems) {
        long start = System.nanoTime();
        PC pc = pp.getPC();
        PcCompiler compiler = new JavaPcCompiler();
        compiler.include(pc);
        Map<PC, Evaluator> pcToMethod = compiler.compile();
        long compilationCost = System.nanoTime() - start;
        totalCompilationCost += compilationCost;

        long hits = 0;
        Evaluator eval = pcToMethod.get(pc);
        Set<SymLiteral> lits = pc.getSortedVars();
        Number[] mArgs = new Number[lits.size()];

        Domain domain = pp.getDomain();
        //assuming everything is a double for this small experiment
        hits = sample(rng, eval, mArgs, domain);

        long evaluationCost = System.nanoTime() - start;
        System.out.println(hits / (double) N_EVAL + "," + evaluationCost / Math.pow(10.0, 9));
      }
      System.out.println("compilation cost: " + totalCompilationCost / Math.pow(10.0, 9));
    } else if (args[1].equalsIgnoreCase("crosscheck")) {
      PcCompiler compiler1 = new TccPcCompiler();
      PcCompiler compiler2 = new JavaPcCompiler();

      for (PartitionProblem pp : partProblems) {
        PC pc = pp.getPC();
        compiler1.include(pc);
        compiler2.include(pc);
      }

      Map<PC, Evaluator> pcToMethod1 = compiler1.compile();
      Map<PC, Evaluator> pcToMethod2 = compiler2.compile();

      int i = 0;
      for (PartitionProblem pp : partProblems) {
        PC pc = pp.getPC();
        Evaluator pcEval1 = pcToMethod1.get(pc);
        Evaluator pcEval2 = pcToMethod2.get(pc);
        Set<SymLiteral> lits = pc.getSortedVars();
        Number[] mArgs = new Number[lits.size()];

        Domain domain = pp.getDomain();
        //assuming everything is a double for this small experiment
        long hits = sampleCross(rng, pcEval1, pcEval2, mArgs, domain);
        System.out.println(hits / (double) N_EVAL);
      }
    } else {
      for (PartitionProblem pp : partProblems) {
        long hits = 0;
        long start = System.nanoTime();
        PC pc = pp.getPC();
        List<SymLiteral> lits = ImmutableList.copyOf(pc.getSortedVars());
        Domain domain = pp.getDomain();
        //assuming everything is a double for this small experiment
        for (int i = 0; i < N_EVAL; i++) {
          for (int j = 0; j < lits.size(); j++) {
            SymLiteral lit = lits.get(j);
            Interval iv = domain.idToInterval.get(lit.getId()).iv;
            Number sample = rng.nextDouble() * (iv.doubleHi() - iv.doubleLo()) + iv.doubleLo();
            lit.setCte(sample);
          }

          List<Elem[]> l = CountingUtils.prepareRPNState(pc);

          /** reverse polish code **/
          boolean isSAT = true;
          isSAT = CountingUtils.evalRNP(l);
          if (isSAT) {
            hits++;
          }
        }

        long evaluationCost = System.nanoTime() - start;
        System.out.println(hits / (double) N_EVAL + "," + evaluationCost / Math.pow(10.0, 9));
      }
    }
    System.out.println("Total time: " + (System.nanoTime() - executionStart) / Math.pow(10.0, 9));
  }

  private static long sampleCross(RandomGenerator rng, Evaluator pcEval1, Evaluator pcEval2, Number[] mArgs, Domain domain) {
    long hits = 0;
    Interval[] ivs = domain.idToInterval.values().stream()
        .map(data -> data.iv)
        .collect(Collectors.toList())
        .toArray(new Interval[]{});
    for (int i = 0; i < N_EVAL; i++) {
      for (int j = 0; j < mArgs.length; j++) {
        Interval iv = ivs[j];
        mArgs[j] = rng.nextDouble() * (iv.doubleHi() - iv.doubleLo()) + iv.doubleLo();
      }
      Boolean result1 = pcEval1.isSAT(mArgs);
      Boolean result2 = pcEval2.isSAT(mArgs);
      if (result1 != result2) {
        System.out.println("cross-check error:");
        System.out.println(Arrays.toString(mArgs));
        System.out.println(pcEval1);
        System.out.println(pcEval2);
      }
      if (result1) {
        hits++;
      }
    }
    return hits;
  }

  private static long sample(RandomGenerator rng, Evaluator evaluator, Number[] mArgs, Domain domain) throws IllegalAccessException, InvocationTargetException {
    //System.out.println(evaluator);
    long hits = 0;
    Interval[] ivs = domain.idToInterval.values().stream()
        .map(data -> data.iv)
        .collect(Collectors.toList())
        .toArray(new Interval[]{});
    for (int i = 0; i < N_EVAL; i++) {
      for (int j = 0; j < mArgs.length; j++) {
        Interval iv = ivs[j];
        mArgs[j] = rng.nextDouble() * (iv.doubleHi() - iv.doubleLo()) + iv.doubleLo();
      }
      Boolean result = evaluator.eval(mArgs).intValue() != 0;
      if (result) {
        hits++;
      }
    }
    return hits;
  }

  private static String[] processOptions(String[] args) {
    options.ignore_options_after_arg(true);
    if (args.length > 0) {
      args = Options.loadOptionsOnly(args, options);
      System.out.println(Arrays.toString(args));
    }

    //options.printOptions();
    return args;
  }
}
