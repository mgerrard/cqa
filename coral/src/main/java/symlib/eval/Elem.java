package symlib.eval;

public interface Elem {
  void eval(ReversePolish revPol);
} 
