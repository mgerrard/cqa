package symlib.eval;

import coral.PC;
import coral.counters.refactoring.CountingUtils;
import symlib.SymLiteral;

import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by mateus on 30/01/17.
 */
public class ReversePolishEvaluator implements Evaluator {

  private final SortedSet<SymLiteral> vars;
  private final List<Elem[]> exprs;

  public ReversePolishEvaluator(SortedSet<SymLiteral> vars, List<Elem[]> exprs) {
    this.vars = vars;
    this.exprs = exprs;
  }

  @Override
  public Number eval(Number[] args) {
    Iterator<SymLiteral> it = vars.iterator();
    for (Number arg : args) {
      SymLiteral lit = it.next();
      lit.setCte(arg);
    }

    if (it.hasNext()) {
      throw new IllegalArgumentException("Number of literals and arguments doesn't match!");
    }
    return CountingUtils.evalRNP(exprs) ? 1 : 0;
  }

  public static ReversePolishEvaluator create(PC pc) {
    List<Elem[]> exprs = CountingUtils.prepareRPNState(pc);
    return new ReversePolishEvaluator(pc.getSortedVars(), exprs);
  }
}
