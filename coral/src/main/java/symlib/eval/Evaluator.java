
package symlib.eval;

/**
 * Created by mateus on 16/12/16.
 */
public interface Evaluator {

  public Number eval(Number[] args);

  public default boolean isSAT(Number[] args) {
    return eval(args).intValue() == 1;
  }
}
