package symlib.eval.compilation;

import coral.PC;
import coral.util.visitors.interfaces.StringVisitor;
import symlib.*;

import java.util.Set;

/**
 * Created by mateus on 28/12/16.
 */
public class JavaPCVisitor extends StringVisitor {

  boolean isJava;

  public JavaPCVisitor(boolean isJava) {
    this.isJava = isJava;
  }

  @Override
  protected String visitSymConstant(SymBoolConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymRel(SymIntRelational arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymIntRelational.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymRel(SymLongRelational arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymLongRelational.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymRel(SymFloatRelational arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymFloatRelational.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymRel(SymDoubleRelational arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymDoubleRelational.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymConstant(SymIntConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymIntLiteral(SymIntLiteral arg) {
    return "v" + arg.getId();
  }

  @Override
  protected String visitSymIntArith(SymIntArith arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymIntArith.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymDoubleConst(SymDoubleConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymDoubleLiteral(SymDoubleLiteral arg) {
    return "v" + arg.getId();
  }

  @Override
  protected String visitSymDoubleArith(SymDoubleArith arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymDoubleArith.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymFloatConst(SymFloatConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymFloatLiteral(SymFloatLiteral arg) {
    return "v" + arg.getId();
  }

  @Override
  protected String visitSymArith(SymFloatArith arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymFloatArith.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymLongConst(SymLongConstant arg) {
    return arg.toString();
  }

  @Override
  protected String visitSymLongLiteral(SymLongLiteral arg) {
    return "v" + arg.getId();
  }

  @Override
  protected String visitSymBoolOperations(SymBoolOperations arg) {
    String left = arg.getA().accept(this);

    if (arg.getOp() == SymBoolOperations.NEG) {
      return "!" + left;
    } else {
      String right = arg.getB().accept(this);
      return "(" + left + " " + SymBoolOperations.symbols[arg.getOp()] + " " + right + ")";
    }
  }

  @Override
  protected String visitSymBoolRelational(SymBoolRelational arg) {
    String left = arg.getA().accept(this);
    String right = arg.getB().accept(this);
    return "(" + left + " " + SymBoolRelational.symbols[arg.getOp()] + " " + right + ")";
  }

  @Override
  protected String visitSymMathUnary(SymMathUnary arg) {
    String left = arg.getArg().accept(this);
    if (isJava) {
      return "Math." + SymMathUnary.symbols[arg.getOp()].replace("_","") + "(" + left  + ")";
    } else {
      return SymMathUnary.symbols[arg.getOp()] + "(" + left  + ")";
    }
  }

  @Override
  protected String visitSymMathBinary(SymMathBinary arg) {
    String left = arg.getArg1().accept(this);
    String right = arg.getArg2().accept(this);
    if (isJava) {
      return "Math." + SymMathBinary.symbols[arg.getOp()].replace("_","") + "(" + left + "," + right + ")";
    } else {
      return SymMathBinary.symbols[arg.getOp()] + "(" + left + "," + right + ")";
    }
  }

  @Override
  protected String visitSymCast(SymCast cast) {
    String arg = cast.getArg().accept(this);
    if (cast instanceof SymAsDouble) {
      return "(double) (" + arg + ")";
    } else if (cast instanceof SymAsInt) {
      return "(int) (" + arg + ")";
    } else {
      throw new RuntimeException("Missing case: " + cast.getClass());
    }
  }

  public static StringBuilder genFunctionSourceCode(PC pc, int id, boolean isJava) {
    StringBuilder sb = new StringBuilder();
    if (isJava) {
      sb.append("public static boolean eval");
    } else {
      sb.append("int eval");
    }
    sb.append(id);
    sb.append('(');

    Set<SymLiteral> vars = pc.getSortedVars();
    for (SymLiteral var : vars) {
      if (var instanceof SymInt) {
        sb.append("int ");
      } else if (var instanceof SymDouble) {
        sb.append("double ");
      } else {
        throw new RuntimeException("Not implemented: " + var.getClass());
      }
      sb.append("v");
      sb.append(var.getId());
      sb.append(',');
    }
    sb.delete(sb.length()-1,sb.length());
    sb.append(") { \n");

    sb.append("return ");
    for (SymBool bool : pc.getConstraints()) {
      sb.append("\n (");
      sb.append(bool.accept(new JavaPCVisitor(isJava)));
      sb.append(')');
      sb.append(" &&");
    }
    sb.delete(sb.length()-2,sb.length());
    sb.append("; \n } \n");
    return sb;
  }
}
