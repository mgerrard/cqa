package symlib.eval.compilation;

import coral.PC;
import symlib.eval.Evaluator;

import java.util.Map;

/**
 * Created by mateus on 28/12/16.
 */
public interface PcCompiler {

  public boolean include(PC toCompile);

  public Map<PC,Evaluator> compile();
}
