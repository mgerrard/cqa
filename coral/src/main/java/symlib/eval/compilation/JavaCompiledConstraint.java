package symlib.eval.compilation;

import symlib.eval.Evaluator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by mateus on 02/01/17.
 */
public class JavaCompiledConstraint implements Evaluator {

  private Method compiledMethod;

  public JavaCompiledConstraint(Method compiledMethod) {
    this.compiledMethod = compiledMethod;
  }

  @Override
  public Number eval(Number[] args) {
    try {
      boolean result = (Boolean) compiledMethod.invoke(null,args);
      return result ? 1 : 0;
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      if (e.getCause() instanceof ArithmeticException) { //division by 0, usually
        return 0;
      } else {
        throw new RuntimeException(e);
      }
    }
  }

  @Override
  public boolean isSAT(Number[] args) {
    return eval(args).equals(1);
  }
}
