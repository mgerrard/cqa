package symlib.eval.compilation;

import coral.PC;
import java.util.concurrent.atomic.AtomicInteger;
import net.openhft.compiler.CachedCompiler;
import net.openhft.compiler.CompilerUtils;
import symlib.eval.Evaluator;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by mateus on 28/12/16.
 */
public class JavaPcCompiler implements PcCompiler {

  private static AtomicInteger classId = new AtomicInteger();
  private Set<PC> pcsToCompile;

  public JavaPcCompiler() {
    this.pcsToCompile = new LinkedHashSet<>();
  }

  @Override
  public boolean include(PC toCompile) {
    return pcsToCompile.add(toCompile);
  }

  @Override
  public Map<PC, Evaluator> compile() {
    StringBuilder buf = new StringBuilder();
    buf.append("package eval; \n" +
        "public class Eval"+classId+" {");

    Map<Integer,PC> hashToPC = new LinkedHashMap<>();
    for (PC pc : pcsToCompile) {
      int id = Math.abs(pc.hashCode());
      StringBuilder pcBuf = JavaPCVisitor.genFunctionSourceCode(pc, id, true);
      buf.append(pcBuf);
      hashToPC.put(id,pc);
    }
    buf.append("}");
//    System.out.println(buf);

    Map<PC,Evaluator> pcToMethod = new LinkedHashMap<>();
    try {
      Class klass = compileAndLoadClass(buf.toString());
      for (Method m : klass.getMethods()) {
        String name = m.getName();
//        System.out.println(name);
        if (name.startsWith("eval")) {
          int hash = Integer.parseInt(name.substring(4));

          pcToMethod.put(hashToPC.get(hash),new JavaCompiledConstraint(m));
        }
      }
    } catch (ClassNotFoundException e) {
      System.out.println("Error during class loading: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }

    return pcToMethod;
  }

  private Class compileAndLoadClass(String classStr) throws ClassNotFoundException {
    // TODO investigate whether the java compiler API is thread-safe
    // net.openhft.compiler.CachedCompiler.compileFromJava(CachedCompiler.java:101)
    synchronized (classId) {
      Class klass = new CachedCompiler(null,null)
          .loadFromJava("eval.Eval" + classId.getAndIncrement(), classStr);
      return klass;
    }
  }

}
