package symlib.eval.compilation;

import com.sun.jna.Function;
import symlib.eval.Evaluator;

/**
 * Created by mateus on 16/12/16.
 */
public class TccCompiledConstraint implements Evaluator {

  private String name;
  private Function nativeFunction;

  public TccCompiledConstraint(Function nativeFunction, String name) {
    this.name = name;
    this.nativeFunction = nativeFunction;
  }

  @Override
  public Number eval(Number[] args) {
    return nativeFunction.invokeInt(args);
  }

  @Override
  public boolean isSAT(Number[] args) {
    return ! eval(args).equals(0);
  }

  @Override
  public String toString() {
    return name + ":" + nativeFunction;
  }
}
