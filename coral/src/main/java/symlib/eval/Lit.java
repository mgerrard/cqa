package symlib.eval;

import symlib.SymLiteral;

public class Lit implements Elem {
  SymLiteral l;
  public Lit(SymLiteral l) {
    super();
    this.l = l;
  }
  @Override
  public void eval(ReversePolish revPol) {
    revPol.push(l.getCte());
  }
  public String toString() { return l.toString(); }
}
