package symlib.eval;


enum OP implements Elem {
  
  DADD,DSUB,DMULT,DDIV,DMOD,
  IADD,ISUB,IMULT,IDIV,IMOD,
  FADD,FSUB,FMULT,FDIV,FMOD,
  LADD,LSUB,LMULT,LDIV,LMOD,
  BEQ, BNEQ,
  BAND, BOR, BNEG, BXOR,
  DGT,DLT,DLE,DGE,DEQ,DNE,
  IGT,ILT,ILE,IGE,IEQ,INE,
  FGT,FLT,FLE,FGE,FEQ,FNE,
  LGT,LLT,LLE,LGE,LEQ,LNE,
  SIN, COS, TAN,
  ASIN, ACOS, ATAN,
  EXP, LOG, LOG10,
  ROUND, SQRT,
  ATAN2, POW,
  ASDOUBLE, MAX, MIN;

  @Override
  public void eval(ReversePolish revPol) {
    int int_a, int_b;
    float float_a, float_b;
    double double_a, double_b;
    long long_a, long_b;
    boolean boolean_a, boolean_b;
    switch (this) {
    case DADD:
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a + double_b);
      break;
    case DSUB: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a - double_b);     
      break;
    case DMULT: 
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a * double_b);          
      break;
    case DDIV: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a / double_b);          
      break;
    case DMOD: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a % double_b);          
      break;
    case IADD:
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a + int_b);
      break;
    case ISUB: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a - int_b);
      break;
    case IMULT: 
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a * int_b);          
      break;
    case IDIV: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a / int_b);          
      break;
    case IMOD: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a % int_b);          
      break;
    case FADD:
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a + float_b);
      break;
    case FSUB: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a - float_b);
      break;
    case FMULT: 
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a * float_b);
      break;
    case FDIV: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a / float_b);          
      break;
    case FMOD: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a % float_b);          
      break;
    case LADD:
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a + long_b);
      break;
    case LSUB: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a - long_b);
      break;
    case LMULT: 
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a * long_b);
      break;
    case LDIV: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a / long_b);          
      break;
    case LMOD: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a % long_b);          
      break;
    case BEQ: // note it is reversed
      boolean_b = revPol.pop().intValue()==1;
      boolean_a = revPol.pop().intValue()==1;
      revPol.push(boolean_a==boolean_b?1:0);          
      break;
    case BNEQ: // note it is reversed
      boolean_b = revPol.pop().intValue()==1;
      boolean_a = revPol.pop().intValue()==1;
      revPol.push(boolean_a!=boolean_b?1:0);
      break;
    case BAND: // note it is reversed
      boolean_b = revPol.pop().intValue()==1;
      boolean_a = revPol.pop().intValue()==1;
      revPol.push(boolean_a&&boolean_b?1:0);          
      break;
    case BOR: // note it is reversed
      boolean_b = revPol.pop().intValue()==1;
      boolean_a = revPol.pop().intValue()==1;
      revPol.push(boolean_a||boolean_b?1:0);
      break;
    case BNEG: // note it is reversed
      boolean_a = revPol.pop().intValue()==1;
      revPol.push(!boolean_a?1:0);
      break;        
    case BXOR: // note it is reversed
      boolean_b = revPol.pop().intValue()==1;
      boolean_a = revPol.pop().intValue()==1;
      revPol.push(boolean_a^boolean_b?1:0);
      break;
    case DGT:
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a>double_b?1:0);
      break;
    case DLT: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a<double_b?1:0);
      break;
    case DLE: 
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a<=double_b?1:0);          
      break;
    case DGE: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a>=double_b?1:0);          
      break;
    case DEQ: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a==double_b?1:0);          
      break;
    case DNE: // note it is reversed
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a!=double_b?1:0);          
      break;
    case IGT:
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a>int_b?1:0);
      break;
    case ILT: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a<int_b?1:0);
      break;
    case ILE: 
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a<=int_b?1:0);          
      break;
    case IGE: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a>=int_b?1:0);          
      break;
    case IEQ: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a==int_b?1:0);          
      break;
    case INE: // note it is reversed
      int_b = revPol.pop().intValue();
      int_a = revPol.pop().intValue();
      revPol.push(int_a!=int_b?1:0);          
      break;
    case FGT:
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a>float_b?1:0);
      break;
    case FLT: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a<float_b?1:0);
      break;
    case FLE: 
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a<=float_b?1:0);          
      break;
    case FGE: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a>=float_b?1:0);          
      break;
    case FEQ: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a==float_b?1:0);          
      break;
    case FNE: // note it is reversed
      float_b = revPol.pop().floatValue();
      float_a = revPol.pop().floatValue();
      revPol.push(float_a!=float_b?1:0);          
      break;        
    case LGT:
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a>long_b?1:0);
      break;
    case LLT: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a<long_b?1:0);
      break;
    case LLE: 
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a<=long_b?1:0);          
      break;
    case LGE: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a>=long_b?1:0);          
      break;
    case LEQ: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a==long_b?1:0);          
      break;
    case LNE: // note it is reversed
      long_b = revPol.pop().longValue();
      long_a = revPol.pop().longValue();
      revPol.push(long_a!=long_b?1:0);          
      break;
    case SIN:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.sin(double_a));
      break;
    case COS:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.cos(double_a));
      break;
    case TAN:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.tan(double_a));
      break;
    case ASIN:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.asin(double_a));
      break;
    case ACOS:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.acos(double_a));
      break;
    case ATAN:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.atan(double_a));
      break;
    case EXP:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.exp(double_a));
      break;
    case LOG:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.log(double_a));
      break;        
    case LOG10:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.log10(double_a));
      break;
    case ROUND:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.round(double_a));
      break;        
    case SQRT:
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.sqrt(double_a));
      break;
    case ATAN2:
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.atan2(double_a, double_b));
      break;        
    case POW:
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.pow(double_a, double_b));
      break;
    case ASDOUBLE:
      double_a = revPol.pop().doubleValue();
      revPol.push(double_a);
      break;
    case MAX:
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.max(double_a, double_b));
      break;
    case MIN:
      double_b = revPol.pop().doubleValue();
      double_a = revPol.pop().doubleValue();
      revPol.push(Math.min(double_a, double_b));
      break;
    default:
      break;
    }
  }    
};
