package symlib;

public interface SymLong extends SymNumber {

  public long eval();

}
