package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymIntArith extends SymBinaryExpression implements SymInt {
  
  // integer arith
  public static final int ADD = 0;
  public static final int SUB = 1;
  public static final int MULT = 2;
  public static final int DIV = 3;
  public static final int MOD = 4;
  // bitwise
  public static final int AND = 5;
  public static final int OR = 6;
  public static final int XOR = 7;
  public static final int SHIFT_LEFT = 8;
  public static final int SHIFT_RIGHT = 9;
  public static final int UNSIGNED_SHIFT_RIGHT = 10;

  public static final int CMP = 11; /*complement*/

  public static final String[] symbols = new String[] { "+", "-", "*", "/", "%", "&", "|",
    "^", "<<", ">>", ">>>", "~" };

  public static String[] logSymbols = new String[] { "ADD", "SUB", "MUL", "DIV",
    "MOD", "AND", "OR", "XOR", "SHR", "SHL", "USHR", "CMP" };

  private SymIntArith(SymInt a, SymInt b, int op) {
    super(a, b, op);
  }

  public static SymInt create(SymInt arg1, SymInt arg2, int op) {
    return new SymIntArith(arg1, arg2, op);
  }

  public SymInt getA() {
    return (SymInt) a;
  }

  public SymInt getB() {
    return (SymInt) b;
  }

  public void setA(SymInt a) {
    this.a = a;
  }

  public void setB(SymInt b) {
    this.b = b;
  }

  public String getSymbolOp() {
    return symbols[op];
  }

  public int eval() {
    if (b != null) {
      return eval(((SymInt) a).eval(), ((SymInt) b).eval(), op);
    } else {
      return eval(((SymInt) a).eval(), op);
    }
  }

  private static int eval(int a, int op) {
    int result;
    switch (op) {
    case CMP:
      result = ~a;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  private static int eval(int a, int b, int op) {
    int result;
    switch (op) {
    case ADD:
      result = a + b;
      break;
    case SUB:
      result = a - b;
      break;
    case MULT:
      result = a * b;
      break;
    case DIV:
      result = a / b;
      break;
    case MOD:
      result = a % b;
      break;
    case AND:
      result = a & b;
      break;
    case OR:
      result = a | b;
      break;
    case XOR:
      result = a ^ b;
      break;
    case SHIFT_LEFT:
      result = a << b;
      break;
    case SHIFT_RIGHT:
      result = a >> b;
      break;
    case UNSIGNED_SHIFT_RIGHT:
      result = a >>> b;
      break;
    case CMP:
      result = ~a;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }

    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    if (b != null) {
      builder.append("(");
      builder.append(a);
      builder.append(symbols[op]);
      builder.append(b);
      builder.append(")");
    } else {
      builder.append("(");
      builder.append(symbols[op]);
      builder.append(a);
      builder.append(")");
    }

    return builder.toString();
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymInt(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }


  @Override
  public Number evalNumber() {
    return this.eval();
  }

  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymIntArith other = (SymIntArith) o;

      boolean result = true;

      result = result && (other.a.equals(this.a));

      if (other.b == null && this.b == null) {
        result = result && true;
      } else if (other.b == null && this.b != null) {
        result = false;
      } else if (other.b != null && this.b == null) {
        result = false;
      } else {
        result = result && (other.b.equals(this.b));
      }

      result = result && (other.op == this.op);

      return result;
    }
  }

  /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1 Double - 2 Float - 3 Int - 4 Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    if (this.b != null) {
      result += 4 + this.a.hashCode() + this.b.hashCode() + this.op;
    } else {
      result += 4 + this.a.hashCode() + this.op;
    }

    return result;
  }

  @Override
  public SymNumber clone() {
    SymInt a = (SymInt) this.a.clone();

    SymInt b = null;
    if (this.b != null) {
      b = (SymInt) this.b.clone();
    }

    return SymIntArith.create(a, b, op);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
}
