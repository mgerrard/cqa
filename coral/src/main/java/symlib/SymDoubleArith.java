package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymDoubleArith extends SymBinaryExpression implements SymDouble {
  
  // double arith
  public static final int ADD = 0;

  public static final int SUB = 1;

  public static final int MULT = 2;

  public static final int DIV = 3;

  public static final int MOD = 4;

  public static final String[] symbols = new String[] { "+", "-", "*", "/", "%" };

  public static String[] logSymbols = new String[] { "ADD", "SUB", "MUL", "DIV",
  "MOD" };

  protected SymDoubleArith(SymDouble a, SymDouble b, int op) {
    super(a, b, op);
  }

  public static SymDouble create(SymDouble a, SymDouble b, int op) {
    return new SymDoubleArith(a, b, op);
  }

  public SymDouble getA() {
    return (SymDouble) a;
  }

  public SymDouble getB() {
    return (SymDouble) b;
  }

  public void setA(SymDouble a) {
    this.a = a;
  }

  public void setB(SymDouble b) {
    this.b = b;
  }

  public String getSymbolOp() {
    return symbols[op];
  }

  public double eval() {
    return eval(((SymDouble) a).eval(), ((SymDouble) b).eval(), op);
  }

  private static double eval(double a, double b, int op) {
    double result;
    switch (op) {
    case ADD:
      result = a + b;
      break;
    case SUB:
      result = a - b;
      break;
    case MULT:
      result = a * b;
      break;
    case DIV:
      result = a / b;
      break;
    case MOD:
      result = a % b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(a);
    builder.append(symbols[op]);
    builder.append(b);
    builder.append(")");

    return builder.toString();
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymDouble(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }

  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return logSymbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymDoubleArith other = (SymDoubleArith) o;

      boolean result = true;

      result = result && (other.a.equals(this.a));
      result = result && (other.b.equals(this.b));
      result = result && (other.op == this.op);

      return result;
    }
  }

  @Override
  public SymNumber clone() {
    SymDouble a = (SymDouble) this.a.clone();
    SymDouble b = (SymDouble) this.b.clone();
    return SymDoubleArith.create(a, b, op);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
}
