package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymDoubleConstant implements SymDouble, SymConstant {
  
  double cte;

  public SymDoubleConstant(double cte) {
    this.cte = cte;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(cte);

    return builder.toString();
  }

  public boolean containsLiteral() {
    return false;
  }

  public double eval() {
    return cte;
  }

  public SymDouble negate() {
    return Util.createConstant(-cte);
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymDouble(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymDoubleConstant other = (SymDoubleConstant) o;

      return (other.cte == this.cte) ? true : false;
    }
  }

/*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    result += 2 + this.cte;

    return result;
  }

  @Override
  public SymNumber clone() {
    return new SymDoubleConstant(cte);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
   
}
