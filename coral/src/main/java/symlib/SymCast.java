package symlib;

import coral.util.visitors.interfaces.VoidVisitor;

public abstract class SymCast implements SymNumber {

  private SymNumber arg;

  public SymCast(SymNumber arg) {
    this.arg = arg;
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(arg);
  }

  @Override
  public Number evalNumber() {
    return arg.evalNumber();
  }

  @Override
  public int compareTo(SymNumber arg0) {
    return arg.compareTo(arg0);
  }
  
  public abstract SymNumber clone();

  public SymNumber getArg() {
    return arg;
  }
 
  public String toString() {
    return arg.toString();
  }
  
}
