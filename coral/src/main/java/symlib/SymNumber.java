package symlib;

import coral.util.visitors.interfaces.GenericVisitor;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public interface SymNumber extends Comparable<SymNumber> {
  
  public String accept(StringVisitor visitor);

  public void accept(VoidVisitor visitor);
  
  public SymNumber accept(TypedVisitor typedVisitorAdaptor);

  public default <T> T accept(GenericVisitor<T> visitor) {
    return visitor.visitSymNumber(this);
  }

  public Number evalNumber();

  public boolean equals(Object o);

  public SymNumber clone();

}
