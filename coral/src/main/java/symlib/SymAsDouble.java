package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymAsDouble extends SymCast implements SymDouble {
  
  private SymAsDouble(SymNumber arg) {
    super(arg);
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public double eval() {
    return this.evalNumber().doubleValue();
  }

  @Override
  public SymNumber clone() {
    return new SymAsDouble(super.getArg().clone());
  }
  
  public String toString() {
    return "ASDOUBLE(" + super.getArg() + ")"; 
  }

  public static SymDouble cast(SymNumber sym) {
    if (sym instanceof SymCast) {
      sym = ((SymCast) sym).getArg();
    }
    if (sym instanceof SymDouble) {
      return (SymDouble) sym;
    }
    if (sym instanceof SymConstant) {
      Number constant = sym.evalNumber();
      return new SymDoubleConstant(constant.doubleValue());
    }
    return new SymAsDouble(sym);
  }
}
