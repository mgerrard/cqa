package symlib;


public class SymLongRelational extends SymBool {

  public static final int GT = 0;

  public static final int LT = 1;

  public static final int LE = 2;

  public static final int GE = 3;

  public static final int EQ = 4;

  public static final int NE = 5;

  public static String[] symbols = new String[] { ">", "<", "<=", ">=", "==", "!=" };

  public static String[] logSymbols = new String[] { "LGT", "LLT", "LLE", "LGE",
    "LEQ", "LNE" };

  static int[] neg = new int[] { LE, GE, GT, LT, NE, EQ };

  static int[] results = new int[] { 1, -1, 0, 0, 0, 1 };

  static int[] resultsNeg = new int[] { 0, 0, 1, -1, 1, 0 };

  SymLong a, b;

  int op;

  private SymLongRelational(SymLong a, SymLong b, int op) {
    super();
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public static SymBool create(SymLong a, SymLong b, int op) {
    return new SymLongRelational(a, b, op);
  }

  public SymLong getA() {
    return a;
  }

  public SymLong getB() {
    return b;
  }

  public void setA(SymLong a) {
    this.a = a;
  }

  public void setB(SymLong b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  public boolean evalBool() {
    return eval(a.eval(), b.eval(), op);
  }

  private static boolean eval(long a, long b, int op) {
    boolean result;
    switch (op) {
    case GT:
      result = a > b;
      break;
    case LT:
      result = a < b;
      break;
    case LE:
      result = a <= b;
      break;
    case GE:
      result = a >= b;
      break;
    case EQ:
      result = a == b;
      break;
    case NE:
      result = a != b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(a.toString());
    builder.append(" ");
    builder.append(symbols[op]);
    builder.append(" ");
    builder.append(b.toString());
    builder.append(")");

    return builder.toString();
  }

  public SymBool neg() {
    return new SymLongRelational(a, b, neg[op]);
  }

  public String getLogSymbol() {
    return symbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return logSymbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymLongRelational other = (SymLongRelational) o;

      boolean result = true;

      if (this.op == SymLongRelational.EQ) {
        result = result
        && (other.a.equals(this.a) || other.a.equals(this.b));
        result = result
        && (other.b.equals(this.b) || other.b.equals(this.a));
        result = result && (other.op == this.op);
      } else {
        result = result && (other.a.equals(this.a));
        result = result && (other.b.equals(this.b));
        result = result && (other.op == this.op);
      }

      return result;
    }
  }

  @Override
  public SymBool clone() {
    SymLong a = (SymLong) this.a.clone();
    SymLong b = (SymLong) this.b.clone();
    return SymLongRelational.create(a, b, op);
  }

  public String getSymbolOp() {
    return symbols[op];
  }

}
