package symlib;

import coral.util.Config;


public class SymBoolLiteral extends SymBool implements SymLiteral {
  
  private boolean val;
  private int id;

  public SymBoolLiteral(boolean concreteVal) {
    this.id = Util.newID();
    this.val = concreteVal;
  }

  public static SymBoolLiteral createForParsing(int id) {
    SymBoolLiteral result = new SymBoolLiteral(false);
    result.id = id;
    return result;
  }

  @Override
  public boolean evalBool() {
    return val;
  }

  @Override
  public SymBool neg() {
    return SymBoolOperations.create(this, null, SymBoolOperations.NEG);
  }

  @Override
  public void setCte(Number val) {
    this.val = (val.intValue() > 0) ? true : false;
  }
  
  @Override
  public Number getCte() {
    return val ? 1 : 0;
  }

  @Override
  public int getId() {
    return id;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("$V");
    builder.append(id);
    if(Config.showConcreteValues) {
      builder.append("(");
      builder.append(val);
      builder.append(")");
    }

    return builder.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymBoolLiteral other = (SymBoolLiteral) o;

      return (other.id == this.id) ? true : false;
    }
  }

  @Override
  public SymBool clone() {
    return this;
  }

  /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
//  @Override
//  public int hashCode() {
//    int result = 0;
//
//    result += 1 + this.id;
//
//    return result;
//  }


}
