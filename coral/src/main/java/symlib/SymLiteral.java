package symlib;

public interface SymLiteral {
  public void setCte(Number val);
  public Number getCte();
  public int getId();
  public boolean equals(Object o);
}
