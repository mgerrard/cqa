package symlib;

public interface SymFloat extends SymNumber {

  // concolic
  public float eval();

  public boolean containsLiteral();
}
