package symlib;

import coral.solvers.Unit;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymMathBinary implements SymDouble {
  
  /******* constants *******/
  // radian inputs
  public static final int ATAN2 = 0; // atan2: inputs are two doubles for cartesian coordinates
  public static final int POW = 1; // pow: inputs are any two doubles
  public static final int MAX = 2; // max: inputs are any two doubles
  public static final int MIN = 3; // min: inputs are any two doubles
  public static final int INT_DIV = 4; // integer division: inputs are any two numbers
  
  public static String[] symbols = new String[] { "atan2_", "pow_", "max_", "min_", "int_div_" };
  
  public static String[] mathematicaSymbols = new String[] {"ArcTan","Power","Max","Min","No_Idea_Check_It_Later_IntDiv"};

  public static String[] logSymbols = new String[] { "ATAN2_", "POW_", "MAX_", "MIN_","INTDIV_" };
  
  /*******  fields ********/
  private SymDouble arg1, arg2;
  private int op;
  
  public SymMathBinary(SymDouble arg1, SymDouble arg2, int op) {
    this.arg1 = arg1;
    this.arg2 = arg2;
    this.op = op;
  }

  public static Unit getExpectedUnit(int op) {
    Unit result;
    if (op == 0) {
      result = Unit.RADIANS;
    } else if (op == 1) {
      result = Unit.LIMITED_INT;
    } else {
      throw new RuntimeException("what unit is this?");
    }
    return result;
  }

  public static SymDouble create(SymDouble arg1, SymDouble arg2, int op) {
    return new SymMathBinary(arg1, arg2, op);
  }

  public double eval() {
    return eval(((SymDouble) arg1).eval(), ((SymDouble) arg2).eval(), op);
  }

  private static double eval(double a, double b, int op) {
    double result;
    switch (op) {
    case ATAN2:
      result = Math.atan2(a,b);
      break;
    case POW:
      result = Math.pow(a,b);
      break;
    case MAX:
      result = Math.max(a, b);
      break;
    case MIN:
      result = Math.min(a, b);
      break;
    case INT_DIV:
      result = ((int) a) / ((int) b);
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }
  
  public static int getOp(String strOp) {
    int result = -1;
    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }
    return result;
  }
  
  public SymDouble getArg1() {
    return arg1;
  }
  
  public SymDouble getArg2() {
    return arg2;
  }
  
  public int getOp() {
    return op;
  }
  
  public String getMathmematicaOp() {
    return mathematicaSymbols[op];
  }
  
  @Override
  public int compareTo(SymNumber o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public SymNumber clone() {
    return create((SymDouble) this.arg1.clone(), (SymDouble) this.arg2.clone(), op);
  }
  
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(symbols[op]);
    builder.append("(");
    builder.append(arg1.toString());
    builder.append(",");
    builder.append(arg2.toString());
    builder.append(")");

    return builder.toString();
  }


  /********  visitors  ********/

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymDouble(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }
  
}