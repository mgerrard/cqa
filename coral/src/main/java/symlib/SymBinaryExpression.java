package symlib;

public abstract class SymBinaryExpression {
  
  protected SymNumber a;
  protected SymNumber b;
  protected int op;

  public SymBinaryExpression(SymNumber a, SymNumber b, int op) {
    super();
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public SymNumber getA() {
    return a;
  }

  public void setA(SymNumber a) {
    this.a = a;
  }

  public SymNumber getB() {
    return b;
  }

  public void setB(SymNumber b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  public void setOp(int op) {
    this.op = op;
  }


}
