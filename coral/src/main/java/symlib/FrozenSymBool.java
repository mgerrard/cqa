package symlib;

import coral.util.visitors.interfaces.GenericVisitor;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

/**
 * Caches the hashCode of a symBool, and "marks" it as immutable. Ideally we
 * should replace the entire tree with immutable objects.
 * 
 * @author mateus
 *
 */

public class FrozenSymBool extends SymBool {

  final SymBool bool;
  final int hashCode;

  public FrozenSymBool(SymBool bool) {
    this.bool = bool;
    hashCode = bool.hashCode();
  }
  
  @Override
  public boolean evalBool() {
    return bool.evalBool();
  }

  @Override
  public SymBool neg() {
    return bool.neg();
  }

  @Override
  public SymBool clone() {
    return new FrozenSymBool(bool.clone());
  }
  
  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymBool(bool);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymBool(bool);
  }

  @Override
  public SymBool accept(TypedVisitor visitor) {
    return visitor.visitSymBool(bool);
  }
  
  @Override
  public int hashCode() {
    return hashCode;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FrozenSymBool other = (FrozenSymBool) obj;
    if (bool == null) {
      if (other.bool != null)
        return false;
    } else if (!bool.equals(other.bool))
      return false;
    if (hashCode != other.hashCode)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return bool.toString();
  }

  public <T> T accept(GenericVisitor<T> genericVisitor) {
  	return genericVisitor.visitSymBool(bool);
  }
}
