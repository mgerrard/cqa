package symlib;


public class SymBoolRelational extends SymBool {
	
  public static final int EQ = 0;

  public static final int NE = 1;

  public static String[] symbols = new String[] { "==", "!=" };

  public static String[] logSymbols = new String[] { "BEQ", "BNE" };

  static String[] negSymbols = new String[] { "!=", "==" };

  static int[] neg = new int[] { NE, EQ };

  SymBool a, b;

  int op;

  public static SymBool create(SymBool a, SymBool b, int op) {
    return new SymBoolRelational(a, b, op); 
  }

  private SymBoolRelational(SymBool a, SymBool b, int op) {
    this.a = a;
    this.b = b;
    this.op = op;
  }

  @Override
  public boolean evalBool() {
    return eval(a.evalBool(), b.evalBool(), op);
  }

  public SymBool getA() {
    return a;
  }

  public SymBool getB() {
    return b;
  }

  public void setA(SymBool a) {
    this.a = a;
  }

  public void setB(SymBool b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  private static boolean eval(boolean a, boolean b, int op) {
    boolean result;

    switch (op) {
    case EQ:
      result = a == b;
      break;
    case NE:
      result = a != b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }

    return result;
  }

  @Override
  public SymBool neg() {
    return SymBoolOperations.create(this, null, SymBoolOperations.NEG);
  }

  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(a.toString());
    builder.append(" ");
    builder.append(symbols[op]);
    builder.append(" ");
    builder.append(b.toString());
    builder.append(")");

    return builder.toString();
  }

  public static String getOp(int op) {
    return symbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymBoolRelational other = (SymBoolRelational) o;

      boolean result = true;

      if (this.op == SymBoolRelational.EQ) {
        result = result
        && (other.a.equals(this.a) || other.a.equals(this.b));
        result = result
        && (other.b.equals(this.b) || other.b.equals(this.a));
        result = result && (other.op == this.op);
      } else {
        result = result && (other.a.equals(this.a));
        result = result && (other.b.equals(this.b));
        result = result && (other.op == this.op);
      }

      return result;
    }
  }

 /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
//  @Override
//  public int hashCode() {
//    int result = 0;
//
//    result += 1 + this.a.hashCode() + this.b.hashCode() + this.op;
//
//    return result;
//  }

  @Override
  public SymBool clone() {
    SymBool a = this.a.clone();
    SymBool b = this.b.clone();
    return SymBoolRelational.create(a, b, op);
  }

  public String getSymbolOp() {
    return symbols[op];
  }

}
