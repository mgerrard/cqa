package symlib;

import coral.util.Config;
import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymDoubleLiteral implements SymDouble, SymLiteral {

  /**********************************************
   * concolic execution enables one to interpret a symbolic variable on the
   * concrete domain using a fixed random value.
   **********************************************/
  private int id;
  double val;

  public SymDoubleLiteral(double concreteVal) {
    this.setId(Util.newID());
    this.val = concreteVal;
  }

  public static SymDoubleLiteral createForParsing(int id) {
    SymDoubleLiteral result = new SymDoubleLiteral(0);
    result.setId(id);
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("$V");
    builder.append(getId());
    if(Config.showConcreteValues) {
      builder.append("(");
      builder.append(val);
      builder.append(")");
    }

    return builder.toString();
  }

  public boolean containsLiteral() {
    return true;
  }

  public double eval() {
    return val;
  }

  @Override
  public void setCte(Number val) {
    this.val = val.doubleValue();
  }
  
  @Override
  public Number getCte() {
    return val;
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymDouble(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymDoubleLiteral other = (SymDoubleLiteral) o;

      return (other.getId() == this.getId());
    }
  }

 /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    result += 2 + this.getId();

    return result;
  }


  @Override
  public SymNumber clone() {
    return this;
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
  
public void setId(int id) {
	this.id = id;
}
}
