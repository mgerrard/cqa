package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymLongArith extends SymBinaryExpression implements SymLong {

  // integer arith
  public static final int ADD = 0;
  public static final int SUB = 1;
  public static final int MULT = 2;
  public static final int DIV = 3;
  public static final int MOD = 4;
  // bitwise
  public static final int AND = 5;
  public static final int OR = 6;
  public static final int XOR = 7;
  public static final int SHIFT_LEFT = 8;
  public static final int SHIFT_RIGHT = 9;
  public static final int UNSIGNED_SHIFT_RIGHT = 10;

  public static String[] symbols = new String[] { "+", "-", "*", "/", "%", "&", "|",
    "^", "<<", ">>", ">>>" };

  public static String[] logSymbols = new String[] { "ADD", "SUB", "MUL", "DIV",
    "MOD", "AND", "OR", "XOR", "SHR", "SHL", "USHR", "CMP" };


  public SymLongArith(SymLong a, SymLong b, int op) {
    super(a, b, op);
    if (op >= SHIFT_LEFT) {
      throw new UnsupportedOperationException(
      "This operation needs a Long and an Integer arguments.");
    }
  }

  public SymLongArith(SymLong a, SymInt b, int op) {
    super(a, b, op);
    if (op < SHIFT_LEFT) {
      throw new UnsupportedOperationException(
      "This operation needs two Long arguments.");
    }
  }

  public SymLong getA() {
    return (SymLong) a;
  }

  public SymLong getB() {
    return (SymLong) b;
  }

  public void setA(SymLong a) {
    this.a = a;
  }

  public void setB(SymLong b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  public long eval() {
    if (op > SHIFT_LEFT) {
      return eval(((SymLong) a).eval(), ((SymLong) b).eval(), op);
    } else {
      return eval(((SymLong) a).eval(), ((SymInt) b).eval(), op);
    }
  }

  private static long eval(long a, int b, int op) {
    long result;
    switch (op) {
    case SHIFT_LEFT:
      result = a << b;
      break;
    case SHIFT_RIGHT:
      result = a >> b;
      break;
    case UNSIGNED_SHIFT_RIGHT:
      result = a >>> b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  private static long eval(long a, long b, int op) {
    long result;
    switch (op) {
    case ADD:
      result = a + b;
      break;
    case SUB:
      result = a - b;
      break;
    case MULT:
      result = a * b;
      break;
    case DIV:
      result = a / b;
      break;
    case MOD:
      result = a % b;
      break;
    case AND:
      result = a & b;
      break;
    case OR:
      result = a | b;
      break;
    case XOR:
      result = a ^ b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(a);
    builder.append(symbols[op]);
    builder.append(b);

    return builder.toString();
  }

  public static SymLong create(SymLong arg1, SymLong arg2, int op) {
    return new SymLongArith(arg1, arg2, op);
  }

  public static SymLong create(SymLong arg1, SymInt arg2, int op) {
    return new SymLongArith(arg1, arg2, op);
  }


  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymLong(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }


  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return logSymbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymLongArith other = (SymLongArith) o;

      boolean result = true;

      result = result && (other.a.equals(this.a));
      result = result && (other.b.equals(this.b));
      result = result && (other.op == this.op);

      return result;
    }
  }

  /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    result += 5 + this.a.hashCode() + this.b.hashCode() + this.op;

    return result;
  }

  @Override
  public SymNumber clone() {
    SymLong a = (SymLong) this.a.clone();
    SymLong b = (SymLong) this.b.clone();
    return SymLongArith.create(a, b, op);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
}
