package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymIntConstant implements SymInt, SymConstant {
  int cte;

  public SymIntConstant(int cte) {
    this.cte = cte;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(cte);

    return builder.toString();
  }

  public boolean containsLiteral() {
    return false;
  }

  public int eval() {
    return cte;
  }

  public SymInt negate() {
    return Util.createConstant(-cte);
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymInt(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }

  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymIntConstant other = (SymIntConstant) o;

      return (other.cte == this.cte) ? true : false;
    }
  }

  @Override
  public SymNumber clone() {
    return new SymIntConstant(cte);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
  
   @Override
  public int hashCode() {
    int result = 0;

    result += 4 + this.cte;

    return result;
  }
}
