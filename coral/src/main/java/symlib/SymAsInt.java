package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;

public class SymAsInt extends SymCast implements SymInt {
  
  public SymAsInt(SymNumber arg) {
    super(arg);
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymNumber(this) ;
  }

  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }
  
  @Override
  public int eval() {
    return this.evalNumber().intValue();
  }
  
  @Override
  public SymNumber clone() {
    return new SymAsInt(super.getArg().clone());
  }
  
  public String toString() {
    return "ASINT(" + super.getArg() + ")"; 
  }
}
