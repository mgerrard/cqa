package symlib.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import symlib.SymBool;
import symlib.SymBoolLiteral;
import symlib.SymBoolOperations;
import symlib.SymBoolRelational;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatConstant;
import symlib.SymFloatLiteral;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongConstant;
import symlib.SymLongLiteral;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.Util;
import coral.PC;

/********************************************
 * recursive-descent implementation of a
 * parser for the language of symbolic 
 * boolean expressions.  this should help we 
 * evaluate solvers from files
 ********************************************/

public class Parser {

  String s;
  int pos;
  
  public Parser(String s) {
    super();
    this.s = s.replaceAll("\\s+","");
  }
  
  public PC parsePC() throws ParseException {
    List<SymBool> parts = new ArrayList<SymBool>();
    String token = null;
    do {
      try {
        parts.add(parseSymBool());
      } catch (RuntimeException _) {
    	  _.printStackTrace();
        throw new ParseException();
      }
    } while ((token = consume(1)/*SEPARATOR*/) != null && token.equals(";"));
    return new PC(parts);
  }

  public SymBool parseSymBool() {
    SymBool result;
    String lookTwo = lookahead(2);
    String lookThree = lookahead(3);
    String lookFour = lookahead(4);
    if (startsWithAny(lookFour, new String[]{"BCON"})!=null) {
      result = parseSymBoolConstant();
    } else if (startsWithAny(lookTwo, new String[]{"BV"})!=null) {
      result = parseSymBoolLiteral();
    } else if (startsWithAny(lookFour, new String[]{"BAND","BOR(","BNOT","BXOR"})!=null) {
      result = parseSymBoolOperations();
    } else if (startsWithAny(lookThree, SymBoolRelational.logSymbols)!=null) {
      result = parseSymBoolRelational();
    } else if (startsWithAny(lookThree, SymDoubleRelational.logSymbols)!=null) {
      result = parseSymDoubleRelational();
    } else if (startsWithAny(lookThree, SymLongRelational.logSymbols)!=null) {
      result = parseSymLongRelational();
    } else if (startsWithAny(lookThree, SymIntRelational.logSymbols)!=null) {
      result = parseSymIntRelational();
    } else if (startsWithAny(lookThree, SymFloatRelational.logSymbols)!=null) {
      result = parseSymFloatRelational();
    } else {
      throw new UnsupportedOperationException(lookahead(10) +", pos=" + pos);
    }
    return result;
  }

  /*****************
   * SymFloat
   *****************/
  public SymFloat parseSymFloat() {
    SymFloat result;
    String lookThree = lookahead(3);
    if (startsWithAny(lookThree, SymFloatArith.logSymbols)!=null) {
      result = parseSymFloatArith();
    } else if (startsWithAny(lookThree, new String[]{"FCO"})!=null) {
      result = parseSymFloatConstant();
    } else if (startsWithAny(lookThree, new String[]{"FVA"})!=null) {
      result = parseSymFloatLiteral();
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }

  private SymFloat parseSymFloatConstant() {
    String[] tokens = parseSimpleNode();
    float v = Float.parseFloat(tokens[1]);
    return new SymFloatConstant(v);
  }

  private SymFloat parseSymFloatArith() {
    String token = consume(3); // operator
    int code; 
    if ((code = SymFloatArith.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymFloat arg1 = parseSymFloat();
    consume(1); // comma
    SymFloat arg2 = parseSymFloat();
    consume(1); // close paren
    return SymFloatArith.create(arg1, arg2, code);
  }
  
  /*****************
   * 
   * SymLiteral
   * 
   *****************/
  
  Map<Integer, SymLiteral> map = new HashMap<Integer, SymLiteral>(); 
  private SymInt parseSymIntLiteral() {
    int v = extractID();
    SymLiteral result = (SymLiteral) map.get(v);
    if (result == null) {
      result = SymIntLiteral.createForParsing(v);
      map.put(v, result);
    }
    return (SymInt) result;
  }

  private SymFloat parseSymFloatLiteral() {
    int v = extractID();
    SymLiteral result = (SymLiteral) map.get(v);
    if (result == null) {
      result = SymFloatLiteral.createForParsing(v);
      map.put(v, result);
    }
    return (SymFloat) result;
  }
  
  private SymLong parseSymLongLiteral() {
    int v = extractID();
    SymLiteral result = (SymLiteral) map.get(v);
    if (result == null) {
      result = SymLongLiteral.createForParsing(v);
      map.put(v, result);
    }
    return (SymLong) result;
  }

  private SymDouble parseSymDoubleLiteral() {
    int v = extractID();
    SymLiteral result = (SymLiteral) map.get(v);
    if (result == null) {
      result = SymDoubleLiteral.createForParsing(v);
      map.put(v, result);
    }
    return (SymDouble) result;
  }
  
  private SymBool parseSymBoolLiteral() {
    int v = extractID();
    SymLiteral result = (SymLiteral) map.get(v);
    if (result == null) {
      result = SymBoolLiteral.createForParsing(v);
      map.put(v, result);
    }
    return (SymBool) result;
  }

  private int extractID() {
    String[] tokens = parseSimpleNode();
    String s = tokens[1];
    int v = Integer.parseInt(s.substring(s.indexOf("_")+1));
    return v;
  }
  
  /*****************
   * SymInt
   *****************/
  private SymInt parseSymInt() {
    //TODO: I have no clue why Mitsuo defined SymBool 
    // as subclass of SymInt
    SymInt result;
    String lookFour = lookahead(4);
    String lookFive = lookahead(5);
    if (startsWithAny(lookFour, SymIntArith.logSymbols)!=null) {
      result = parseSymIntArith();
    } else if (startsWithAny(lookFour, new String[]{"ICON"})!=null) {
      result = parseSymIntConstant();
    } else if (startsWithAny(lookFour, new String[]{"IVAR"})!=null) {
      result = parseSymIntLiteral();
    } else if (startsWithAny(lookFive, new String[]{"ASINT"})!=null) {
      result = parseSymCastToInt();
    } else {
      throw new UnsupportedOperationException(lookahead(10) +", pos=" + pos + " .......");
    }
    return result;
  }
  
  private SymInt parseSymCastToInt() { 
    SymInt result;
    String lookFive = lookahead(5);
    String token = startsWithAny(lookFive, new String[]{"ASINT"});
    if (token != null) {
      String op = consume(token.length());
      consume(1); // open par
      SymInt arg = Util.createASInt(parseSymDouble()); //TODO add support for other types?
      consume(1); // close par
      result = arg;
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }

  private SymInt parseSymIntConstant() {
    String[] tokens = parseSimpleNode();
    int v = Integer.parseInt(tokens[1]);
    return new SymIntConstant(v);
  }
  
  private SymInt parseSymIntArith() {
    String token = consume(lookahead(4).equals("USHR")?4:(lookahead(2).equals("OR")?2:3)); 
    int code; 
    if ((code = SymIntArith.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymInt arg1 = parseSymInt();
    SymInt arg2 = null;
    if (code != SymIntArith.CMP) { // unary
      consume(1); // comma
      arg2 = parseSymInt();      
    }
    consume(1); // close paren
    return SymIntArith.create(arg1, arg2, code);
  }

  /*****************
   * SymLong
   *****************/
  private SymLong parseSymLong() {
    SymLong result;
    String lookFour = lookahead(4);
    if (startsWithAny(lookFour, SymLongArith.logSymbols)!=null) {
      result = parseSymLongArith();
    } else if (startsWithAny(lookFour, new String[]{"LCON"})!=null) {
      result = parseSymLongConstant();
    } else if (startsWithAny(lookFour, new String[]{"LVAR"})!=null) {
      result = parseSymLongLiteral();
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }
  
  private SymLong parseSymLongConstant() {
    String[] tokens = parseSimpleNode();
    int v = Integer.parseInt(tokens[1]);
    return new SymLongConstant(v);
  }

  private SymLong parseSymLongArith() {
    String token = consume(lookahead(4).equals("USHR")?4:3);
    int code; 
    if ((code = SymLongArith.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymLong arg1 = parseSymLong();
    consume(1); // comma
    SymLong arg2 = parseSymLong();
    consume(1); // close paren
    return SymLongArith.create(arg1, arg2, code);
  }
  
  /*****************
   * SymDouble
   *****************/
  private SymDouble parseSymDouble() {
    SymDouble result;
    String lookEight = lookahead(8);
    if (startsWithAny(lookEight, SymDoubleArith.logSymbols)!=null) {
      result = parseSymDoubleArith();
    } else if (startsWithAny(lookEight, new String[]{"DCON"})!=null) {
      result = parseSymDoubleConstant();
    } else if (startsWithAny(lookEight, new String[]{"DVAR"})!=null) {
      result = parseSymDoubleLiteral();
    } else if (startsWithAny(lookEight, SymMathUnary.logSymbols)!=null) {
      result = parseSymUnaryMath();
    } else if (startsWithAny(lookEight, SymMathBinary.logSymbols)!=null) {
      result = parseSymBinaryMath();
    } else if (startsWithAny(lookEight, new String[]{"ASDOUBLE"})!=null){
      result = parseSymCastToDouble(); 
    } else {
      throw new UnsupportedOperationException(lookEight);
    }
    
    return result;
  }
  
  private SymDouble parseSymCastToDouble() { 
    SymDouble result;
    String lookEight = lookahead(8);
    String token = startsWithAny(lookEight, new String[]{"ASDOUBLE"});
    if (token != null) {
      String op = consume(token.length());
      consume(1); // open par
      SymDouble arg = Util.createASDouble(parseSymInt()); //TODO add support for other types?
      consume(1); // close par
      result = arg;
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }

  private SymDouble parseSymUnaryMath() {
    SymDouble result;
    String lookSix = lookahead(6);
    String token = startsWithAny(lookSix, SymMathUnary.logSymbols);
    if (token != null) {
      String op = consume(token.length());
      consume(1); // open par
      SymDouble arg = parseSymDouble();
      consume(1); // close par
      result = SymMathUnary.create(arg, SymMathUnary.getOp(op));
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }
  
  private SymDouble parseSymBinaryMath() {
    SymDouble result;
    String lookSeven = lookahead(7);
    String token = startsWithAny(lookSeven, SymMathBinary.logSymbols);
    if (token != null) {
      String op = consume(token.length());
      consume(1); // open par
      SymDouble arg1 = parseSymDouble();
      consume(1); // comma
      SymDouble arg2 = parseSymDouble();
      consume(1); // close par
      result = SymMathBinary.create(arg1, arg2, SymMathBinary.getOp(op));
    } else {
      throw new UnsupportedOperationException();
    }
    return result;
  }

  private SymDouble parseSymDoubleConstant() {
    String[] tokens = parseSimpleNode();
    double v = Double.parseDouble(tokens[1]);
    return new SymDoubleConstant(v);
  }

  private SymDouble parseSymDoubleArith() {
    String token = consume(3); // operator
    int code; 
    if ((code = SymDoubleArith.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymDouble arg1 = parseSymDouble();
    consume(1); // comma
    SymDouble arg2 = parseSymDouble();
    consume(1); // close paren
    return SymDoubleArith.create(arg1, arg2, code);
  }

  /*****************
   * relationals
   *****************/
  private SymBool parseSymFloatRelational() {
    String token = consume(3); // operator
    int code; 
    if ((code = SymFloatRelational.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymFloat arg1 = parseSymFloat();
    consume(1); // comma
    SymFloat arg2 = parseSymFloat();
    consume(1); // close paren
    return SymFloatRelational.create(arg1, arg2, code);
  }

  private SymBool parseSymIntRelational() {
    String token = consume(3); // operator
    int code; 
    if ((code = SymIntRelational.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymInt arg1 = parseSymInt();
    consume(1); // comma
    SymInt arg2 = parseSymInt();
    consume(1); // close paren
    return SymIntRelational.create(arg1, arg2, code);
  }

  private SymBool parseSymLongRelational() {
    String token = consume(3); // operator
    int code; 
    if ((code = SymLongRelational.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymLong arg1 = parseSymLong();
    consume(1); // comma
    SymLong arg2 = parseSymLong();
    consume(1); // close paren
    return SymLongRelational.create(arg1, arg2, code);
  }

  private SymBool parseSymDoubleRelational() {
    String token = consume(3); // operator
    int code; 
    if ((code = SymDoubleRelational.getOp(token))==-1) {
      throw new RuntimeException("invalid token: " + token);
    }
    consume(1); // open paren
    SymDouble arg1 = parseSymDouble();
    consume(1); // comma
    SymDouble arg2 = parseSymDouble();
    consume(1); // close paren
    return SymDoubleRelational.create(arg1, arg2, code);
  }

  private SymBool parseSymBoolRelational() {
    String look = lookahead(3);
    int op;
    if (startsWithAny(look, new String[]{"BEQ"})!=null) {
      consume(3);
      op = SymBoolRelational.EQ;
    } else if (startsWithAny(look, new String[]{"BNE"})!=null) {
      consume(3);
      op = SymBoolRelational.NE;
    } else {
      throw new RuntimeException();
    }
    consume(1); // (
    SymBool arg1 = parseSymBool(); // first argument    
    consume(1); // comma
    SymBool arg2 = parseSymBool(); // second argument
    consume(1); // )
    return SymBoolRelational.create(arg1, arg2, op);
  }

  /********************
   * boolean operations
   ********************/
  private SymBool parseSymBoolOperations() {
    String look = lookahead(2);
    int op;
    if (startsWithAny(look, new String[]{"BA"})!=null) {
      consume(4);
      op = SymBoolOperations.AND;
    } else if (startsWithAny(look, new String[]{"BO"})!=null) {
      consume(3);
      op = SymBoolOperations.OR;
    } else if (startsWithAny(look, new String[]{"BN"})!=null) {
      consume(4);
      op = SymBoolOperations.NEG;
    } else if (startsWithAny(look, new String[]{"BX"})!=null) {
      consume(4);
      op = SymBoolOperations.XOR;
    } else {
      throw new RuntimeException();
    }
    consume(1); // (
    SymBool arg1 = parseSymBool();
    SymBool arg2 = null;
    if (op != SymBoolOperations.NEG) {
       consume(1); // comma
       arg2 = parseSymBool(); // second argument
    } 
    consume(1); // )
    return SymBoolOperations.create(arg1, arg2, op);
  }

  private SymBool parseSymBoolConstant() {
    String[] tokens = parseSimpleNode();
//    return new SymBoolConstant();
    return tokens[1].equals("TRUE") ? Util.TRUE : Util.FALSE; 
  }

  /******************
   * util functions
   ******************/
  
  // parses name(name)
  private String[] parseSimpleNode() {
    int parenOpen = s.indexOf("(",pos);
    int parenClose = s.indexOf(")",pos);
    String token1 = s.substring(pos, parenOpen).trim();
    String token2 = s.substring(parenOpen+1,parenClose).trim();
    pos = parenClose + 1;
    return new String[]{token1, token2};
  }


  private String startsWithAny(String str, String[] strings) {
    String result = null;
    for (String s : strings) {
      if (str.startsWith(s)) {
        result = s;
        break;
      }
    }
    return result;
  }

  private String lookahead(int num) {
    String result = read(num, false);
    if (result != null && (result.startsWith(",") || result.startsWith(")") )) {
      throw new RuntimeException("likely an error " + result +", pos=" + pos);
    }
    return result;
  }
  
  private String consume(int num) {
    return read(num, true);
  }
  
  private String read(int num, boolean mod) {
    String result = null;
    try {
      int lo = pos;
      int hi = mod ? pos += num : pos + num;
      result = s.substring(lo, hi);
    } catch (IndexOutOfBoundsException _) {
      // result == null
    }
    return result;
  }
  
}