package symlib;

public class SymBoolOperations extends SymBool {

  public static final int AND = 0;

  public static final int OR = 1;

  public static final int NEG = 2;

  public static final int XOR = 3;

  public static String[] symbols = new String[] { "&&", "||", "!", "^" };

  public static String[] logSymbols = new String[] { "BAND", "BOR", "BNOT", "BXOR" };

  static String[] negSymbols = new String[] { "||", "&&", "", "" };

  static int[] neg = new int[] { OR, AND };

  SymBool a, b;

  int op;

  public static SymBoolOperations create(SymBool a, SymBool b, int op) {
    return new SymBoolOperations(a, b, op);
  }

  private SymBoolOperations(SymBool a, SymBool b, int op) {
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public SymBool getA() {
    return a;
  }

  public SymBool getB() {
    return b;
  }

  public void setA(SymBool a) {
    this.a = a;
  }

  public void setB(SymBool b) {
    this.b = b;
  }

  public int getOp() {
    return op;
  }

  @Override
  public boolean evalBool() {
    return (b != null) ? eval(a.evalBool(), b.evalBool(), op) : eval(a
        .evalBool(), false, op);
  }

  private static boolean eval(boolean a, boolean b, int op) {
    boolean result;
    switch (op) {
    case AND:
      result = a & b;
      break;
    case OR:
      result = a | b;
      break;
    case NEG:
      result = !a;
      break;
    case XOR:
      result = a ^ b;
      break;
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  @Override
  public SymBool neg() {
    SymBool result;
    switch (op) {
    case AND:
      if (a==Util.FALSE || b==Util.FALSE) {
        result = Util.TRUE;
      } else if (a==Util.TRUE) {
        result = b.neg();
      } else if (b==Util.TRUE) {
        result = a.neg();
      } else {
        result = Util.neg(this);
      }
      break;
    case OR:
      result = Util.and(a.neg(), b.neg());
      break;
    case NEG:
      result = a.neg();
      break;
    case XOR:
      throw new UnsupportedOperationException();
    default:
      throw new RuntimeException("should not happen!");
    }
    return result;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    if (b == null) {
      builder.append(symbols[op]);
      builder.append(a.toString());
    } else {
      builder.append("(");
      builder.append(a.toString());
      builder.append(" ");
      builder.append(symbols[op]);
      builder.append(" ");
      builder.append(b.toString());
      builder.append(")");
    }

    return builder.toString();
  }

  public String getLogSymbol() {
    return logSymbols[op];
  }

  public static int getOp(String strOp) {
    int result = -1;

    for (int i = 0; i < logSymbols.length; i++) {
      if (strOp.equals(logSymbols[i])) {
        result = i;
        break;
      }
    }

    return result;
  }

  public static String getOp(int op) {
    return logSymbols[op];
  }
  
  public String getSymbolOp() {
    return symbols[op];
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymBoolOperations other = (SymBoolOperations) o;

      boolean result = true;

      result = result && (other.a.equals(this.a));
      result = result && (other.b.equals(this.b));
      result = result && (other.op == this.op);

      return result;
    }
  }

 /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1 Double - 2 Float - 3 Int - 4 Long - 5
   */
//  @Override
//  public int hashCode() {
//    int result = 0;
//
//    result += 1 + this.a.hashCode() + this.b.hashCode() + this.op;
//
//    return result;
//  }

  @Override
  public SymBool clone() {
    SymBool a_ = this.a.clone();
    SymBool b_ = null;
    if (b != null) {
      b_ = this.b.clone();
    }
    return SymBoolOperations.create(a_, b_, op);
  }

}
