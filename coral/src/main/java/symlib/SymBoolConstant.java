package symlib;

public class SymBoolConstant extends SymBool implements SymConstant {

  public static final SymBool TRUE = new SymBoolConstant(true);
  public static final SymBool FALSE = new SymBoolConstant(false);
  
  boolean val;

  private SymBoolConstant(boolean val) {
    this.val = val;
  }

  public boolean evalBool() {
    return val;
  }

  public String toString() {
    return val + "";
  }

  public boolean choose() {
    throw new UnsupportedOperationException();
  }

  public SymBool neg() {
    return val ? Util.FALSE : Util.TRUE;
  }

  public boolean containsLiteral() {
    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymBoolConstant other = (SymBoolConstant) o;

      return (other.val == this.val) ? true : false;
    }
  }

  @Override
  public SymBool clone() {
    return new SymBoolConstant(val);
  }
  
   /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
//  @Override
//  public int hashCode() {
//    int result = 0;
//
//    result += 1 + (this.evalBool()?1:0);
//
//    return result;
//  }

}
