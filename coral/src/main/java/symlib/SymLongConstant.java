package symlib;

import coral.util.visitors.interfaces.StringVisitor;
import coral.util.visitors.interfaces.TypedVisitor;
import coral.util.visitors.interfaces.VoidVisitor;

public class SymLongConstant implements SymLong, SymConstant {
  long cte;

  public SymLongConstant(long cte) {
    this.cte = cte;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(cte);

    return builder.toString();
  }

  public boolean containsLiteral() {
    return false;
  }

  public long eval() {
    return cte;
  }

  public SymLong negate() {
    return Util.createConstant(-cte);
  }

  @Override
  public String accept(StringVisitor visitor) {
    return visitor.visitSymLong(this);
  }

  @Override
  public void accept(VoidVisitor visitor) {
    visitor.visitSymNumber(this);
  }

  @Override
  public Number evalNumber() {
    return this.eval();
  }
  
  @Override
  public SymNumber accept(TypedVisitor visitor) {
    return visitor.visitSymNumber(this);
  }


  @Override
  public boolean equals(Object o) {
    if (!o.getClass().equals(this.getClass())) {
      return false;
    } else {
      SymLongConstant other = (SymLongConstant) o;

      return (other.cte == this.cte) ? true : false;
    }
  }

  @Override
  public SymNumber clone() {
    return new SymLongConstant(cte);
  }

  @Override
  public int compareTo(SymNumber o) {
    Number otherValue = o.evalNumber();
    Number thisValue = this.evalNumber();

    return (thisValue.doubleValue() > otherValue.doubleValue()) ? 1
        : (thisValue.doubleValue() < otherValue.doubleValue()) ? -1 : 0;
  }
  
  /*
   * hashCode() = Data Type Code + (value or id)
   * 
   * Boolean - 1
   * Double - 2
   * Float - 3
   * Int - 4
   * Long - 5
   */
  @Override
  public int hashCode() {
    int result = 0;

    result += 5 + this.cte;

    return result;
  }
}
