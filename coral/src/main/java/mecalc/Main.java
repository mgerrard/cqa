package mecalc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
  
  static Random r = new Random();
  

  /**
   * we have all this.  mock.
   */
  interface BooleanExpression {    
    boolean eval(/*input*/);
  }
  
  static BooleanExpression exp1 = new BooleanExpression() {
    public boolean eval() {
      boolean res = r.nextBoolean();
      System.out.println("exp1 " + res);
      return res;
    }
  };
  

  static BooleanExpression exp2 = new BooleanExpression() {
    public boolean eval() {
      boolean res = r.nextBoolean();
      System.out.println("exp2 " + res);
      return res; 
    }
  };
  
  static BooleanExpression exp3 = new BooleanExpression() {
    public boolean eval() {
      boolean res = r.nextBoolean();
      System.out.println("exp3 " + res);
      return res;
    }
  };
 
  /**
   * Mateus, please check how long we spend on evaluating expressions.
   * I think this is where Monte Carlo simulation spends most of its time. 
   * If you confirm that, please consider reusing the computations.
   * 
   */
  public static void main(String[] args) {
    v2(null);
  }
  
  public static void v1(String[] args) {
    // evaluate each expression ONCE
    boolean[] exps = new boolean[]{exp1.eval(), exp2.eval(), exp3.eval()};    
    boolean path1 = exps[0] && exps[1] && exps[2];
    boolean path2 = exps[0] && exps[2];
    boolean path3 = exps[0];
    System.out.println("DONE");
  }
  
  public static void v2(String[] args) {
    BooleanExpression[] exps = new BooleanExpression[]{exp1, exp2, exp3};
    
    // this allows you to separate time of creating expressions and paths 
    // from the time to evaluate them 
    int[] path1 = new int[]{0,1,2};
    int[] path2 = new int[]{0,2};
    int[] path3 = new int[]{0};
    List<int[]> paths = new ArrayList<int[]>();
    paths.add(path1);
    paths.add(path2);
    paths.add(path3);    
    
    // evaluate each expression ONCE
    boolean[] vals = new boolean[exps.length];
    for (int i = 0; i < exps.length; i++) {
      vals[i] = exps[i].eval();
    }
    
    for (int[] path : paths) {
      int i;
      for (i = 0; i < path.length; i++) {
        boolean tmp = vals[path[i]];
        if (!tmp) break;
      }
      boolean sat = false;
      if (i == path.length) {
        sat = true;
      }
      System.out.printf("%s => %s\n", Arrays.toString(path), sat);
    }
    
  }

}