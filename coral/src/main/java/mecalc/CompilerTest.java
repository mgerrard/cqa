package mecalc;

import net.openhft.compiler.CompilerUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by mateus on 15/12/16.
 */
public class CompilerTest {

    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {

        StringBuilder buf = new StringBuilder();
        buf.append("package eval; \n" +
                "public class Eval0001 {" +
                "public static boolean eval(int x, int y) {" +
                "   return Math.pow(x,2) > y;" +
                "}" +
                "}");
        Class klass = CompilerUtils.CACHED_COMPILER.loadFromJava("eval.Eval0001", buf.toString());
        Method toEval = null;
        for (Method m : klass.getMethods()) {
            System.out.println(m.getName());
            if (m.getName().equals("eval")) {
                toEval = m;
            }
        }

        Boolean result = (Boolean) toEval.invoke(null,30,400);
        System.out.println(result);


//        TCCState *s;
//    int (*foobar_func)(int);
//    void *mem;
//
//        s = tcc_new();
//        tcc_set_output_type(s, TCC_OUTPUT_MEMORY);
//        tcc_compile_string(s, my_program);
//        tcc_add_symbol(s, "add", add);
//
//        mem = malloc(tcc_relocate(s, NULL));
//        tcc_relocate(s, mem);
//
//        foobar_func = tcc_get_symbol(s, "foobar");
//
//        tcc_delete(s);
//
//        printf("foobar returned: %d\n", foobar_func(32));
//
//        free(mem);
//        return 0;
    }
}
