package avm;

/**
 * TODO merge this with the other FitnessFunction interface
 *  
 * @author mateus
 * @author damorim
 *
 */

public interface FitnessFunction {
  public double eval(double[] input) throws AVMSolutionFoundException;
  public double max();
};