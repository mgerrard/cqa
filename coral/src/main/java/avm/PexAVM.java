package avm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import coral.solvers.rand.NumberGenerator;
import coral.util.Config;
import coral.util.Interval;

/**
 * Java translation of Flopsy's AVM implementation. The original code
 * is GPLv2, so this file shouldn't be distributed with CORAL.
 * 
 * @author mateus
 *
 */

public class PexAVM {
  
  private static boolean DEBUG = false;

  static enum Result {FOUND,NOT_FOUND,NO_MORE_ITERATIONS}
   
  static class VariableInfo {
    public final int id;
    public int precision;
    
    public VariableInfo(int id, int precision) {
      this.id = id;
      this.precision = precision;
    }
    
    public String toString() {
      return id+"p"+precision;
    }
  }
  
  private List<VariableInfo> variableInfos;
  
  private FitnessFunction ff;
  private double[] bestModel;
  private double[] currentModel;
  private double[] initialModel;
    
  private double currentFitness;
  private double bestFitness;

  //region search parameters
  private short direction;
  private short lastDirection;
  private int index;
  private int lastIndex;
  private int patternMoves;

  private int fitnessEvaluations;
  private int fitnessBudget;
  
  private boolean increasedPrecision;
  private boolean precisionChangeSuccess;
  private int precisionVariableIndex;
  
  private NumberGenerator numGen;
  private Interval[] boxes;

  public PexAVM(FitnessFunction fun, int nVars, Interval[] boxes, int fitnessBudget) {
    this.ff = fun;
    this.fitnessBudget = fitnessBudget;
    this.fitnessEvaluations = 0;
    this.variableInfos = new ArrayList<VariableInfo>();
    this.increasedPrecision = false;
    this.precisionChangeSuccess = false;
    this.precisionVariableIndex = 0;
    this.currentFitness = 0;
    this.bestFitness = -1;
    this.initialModel = new double[nVars];
    this.numGen = new NumberGenerator(Config.RANGE);
    this.currentModel = new double[nVars];
    this.bestModel = new double[nVars];
    this.boxes = boxes;
    
    for (int i = 0; i < nVars; i++) {
      Interval iv = boxes[i];
      initialModel[i] = numGen.genDouble(iv.lo().doubleValue(), iv.hi().doubleValue());
    }
  }
  
  private boolean evaluateCurrentModel() throws AVMSolutionFoundException {
    this.currentFitness = ff.eval(currentModel);
    if(DEBUG) {
      System.out.println(this.currentFitness + " : " + Arrays.toString(this.currentModel));
    }
    this.fitnessEvaluations++;
    if (this.currentFitness > this.bestFitness) {
      updateBestModel();
      return true;
    } else {
      return false;
    }
  }
  
  private void updateBestModel() {
    if(DEBUG) {
      System.out.println("updating best model found so far");
    }
    
    if(this.bestModel != null) {
      System.arraycopy(currentModel, 0, bestModel, 0, currentModel.length);
      this.bestFitness = this.currentFitness;
    }  
  }
  
  private void resetModel() {
    //FIXME check if this method has any importance outside flopsy
  }
  
  public double[] search() {
	  double[] answer = null;
	  
	  try {
		  startSearch();
	  } catch (AVMSolutionFoundException e) {
			if (DEBUG) {
				System.out.println("found answer after " + fitnessEvaluations
						+ " fitness evaluations");
			}
			answer = e.solution;
			if(answer == null) {
				throw new RuntimeException("No solution found in AVMException!");
			}
	  }
	  
	  return answer;
  }

  private double[] startSearch() throws AVMSolutionFoundException {
    Result result;
    boolean foundImprovingMove = false;
    
    System.arraycopy(initialModel, 0, currentModel, 0, currentModel.length);
    
    if(!tryBuildVariableVector()) {
      return null;
    }
    
    resetExplorationParameters(true);
    
    evaluateCurrentModel();
    this.bestFitness = this.currentFitness;
    
    if(DEBUG) {
      System.out.println("starting avm...");
    }
    
    boolean randomizeLocal= true;
        
    while((result = terminate(currentModel)) == Result.NOT_FOUND) {
      if(requiresRestart()) {
        if(randomizeLocal) {
          if(DEBUG) {
            System.out.println("performing localized random restart after " + fitnessEvaluations + " evaluations" );
          }
          
          randomizeFraction();
          randomizeLocal = false;
        } else {
          if(DEBUG) {
            System.out.println("performing random restart after " + fitnessEvaluations + " evaluations" );
          }
          
          for (int i = 0; i < this.variableInfos.size(); i++) {
            VariableInfo variableInfo = this.variableInfos.get(i);
            int precision = variableInfo.precision;
            if(precision != 0) {
              variableInfos.set(i, new VariableInfo(variableInfo.id,0));
            }
          }
          
          randomizeInputVariablesBitConverter();
          randomizeLocal = true;
        }
        
        resetExplorationParameters(true);
        this.bestFitness = 0;
        evaluateCurrentModel();
        updateBestModel();
        this.bestFitness = this.currentFitness;
      } else {
        foundImprovingMove = exploreNeighbourhood();
        
        boolean restartExploration = false;
        
        while (foundImprovingMove && ((result = terminate(currentModel)) == Result.NOT_FOUND)) {
          if (this.increasedPrecision) {
            this.precisionChangeSuccess = true;
          }
          foundImprovingMove = makePatternMove();
          restartExploration = true;
        }
        
        if (result == Result.FOUND || result == Result.NO_MORE_ITERATIONS) {
          break;
        }
        
        if(restartExploration) {
          resetExplorationParameters(true);
        }
      }
    }
    if(DEBUG) {
      System.out.println("best model found:" + Arrays.toString(bestModel));
    }
    return currentModel;
  }
  
  private boolean exploreNeighbourhood() throws AVMSolutionFoundException {
    this.lastDirection = this.direction;
    this.lastIndex = this.index;
    
    boolean worked = makeNumericMove(false);
    
    if(this.direction < 0) {
      this.direction = 1;
    } else {
      this.direction = -1;
      this.index = this.index + 1;
    }
    
    return worked;
  }
  
  private boolean makePatternMove() throws AVMSolutionFoundException {
    this.patternMoves++;
    return makeNumericMove(true);
  }
  
  private boolean makeNumericMove(boolean patternMove) throws AVMSolutionFoundException {
    double delta = 0.0;
    int dir = this.direction;
    int index = this.index;
    
    if(patternMove) {
      dir = this.lastDirection;
      index = this.lastIndex;
    }
    
    //debug point here
    
    VariableInfo variableInfo = this.variableInfos.get(index);
    int currentVarId = variableInfo.id;
    
    delta = ((double) dir) * Math.pow(10, -variableInfo.precision) * Math.pow(2, this.patternMoves);
    currentModel[currentVarId] += delta;
    
    boolean worked = evaluateCurrentModel();
    
    if(!worked) { //undo changes
      currentModel[currentVarId] -= delta;
    }
    
    return worked;
  }
  
  private void randomizeFraction() {
    
    for (int i = 0; i < this.variableInfos.size(); i++) {
      VariableInfo variableInfo = this.variableInfos.get(i);
      int variableId = variableInfo.id;
      int precision = variableInfo.precision;
      
      if(precision == 0) {
        randomizeInputVariableBitConverter(variableId);
        continue;
      }
      
      //TODO check if type(var) is double and call randomBitConverter otherwise
      
      //flopsy checked here if the var was not initialized (and assigned a random value);
      //we start with values from realpaver, so we can skip this step
      double rand = numGen.genDouble(0,1);
      
      double r8value = currentModel[i];
      r8value += Math.pow(10, -precision) * rand;
      currentModel[i] = r8value;
    }
  }
  
  //this method randomizes the value of the variable with id varId
  private void randomizeInputVariableBitConverter(int varId) {
    //TODO check if var is integer
    Interval iv = boxes[varId];
    currentModel[varId] = numGen.genDouble(iv.lo().doubleValue(), iv.hi().doubleValue());
  }
  
  private void randomizeInputVariablesBitConverter() {
    for(int i = 0; i < variableInfos.size(); i++) {
      randomizeInputVariableBitConverter(i);
    }
  }
  
  private boolean tryBuildVariableVector() {
    this.variableInfos.clear();
    for(int i = 0; i < initialModel.length; i++) {
      this.variableInfos.add(new VariableInfo(i,0));
    }
    if(this.variableInfos.size() == 0) {
      return false;
    }
    
    return true;
  }

  private static int[] processNumber(String s) {
    int[] tuple = null;
    int decPoint,digits;
    
    int indexE = s.indexOf('E');
    if(indexE != -1) {
      int expoent = Integer.parseInt(s.substring(indexE+2));
      decPoint = 1;
      digits = s.length() - 3 + expoent;
    } else {
      decPoint = s.indexOf('.');
      
      if(decPoint != -1)
        digits = s.length() - 1;
      else 
        digits = s.length();
    }
    
    tuple = new int[] {digits,decPoint};
    return tuple;
  }
  
  private boolean expandNeighbourhood(double termValue, int precision) {
    int[] tuple = processNumber(Double.toString(termValue));
    int digits = tuple[0];
    int decimalPoint = tuple[1];
    
    if(decimalPoint == -1) {
      if (digits < 15 && precision < 15) {
        return true;
      }
    } else {
      if ((decimalPoint + 1) < 15 && (precision + decimalPoint) < 15) {
        return true;
      }
    }
    
    return false;
  }
  
  private int tryChangePrecision(boolean increase, int precision, double termValue) {
    
    if(!increase) {
      precision = precision > 0 ? precision - 1 : 0;
    } else {
      precision++;
    }
    
    return precision;
  }
  
  private boolean tryExpandNeighbourhood() {
    if(this.increasedPrecision && !this.precisionChangeSuccess) {
      this.precisionVariableIndex++;
    }
    
    this.increasedPrecision = false;
    this.precisionChangeSuccess = false;
    
    while(this.precisionVariableIndex < this.variableInfos.size()) {
      
      VariableInfo variableInfo = this.variableInfos.get(this.precisionVariableIndex);
      int precision = variableInfo.precision;
      double termValue = currentModel[this.precisionVariableIndex];
      
      //note: only considering case Layout.R8 here
      if(expandNeighbourhood(termValue,precision)) {
        precision = tryChangePrecision(true,precision,termValue);
        //need to check if the last case of the previous "if" could be reached.
        this.variableInfos.set(this.precisionVariableIndex, new VariableInfo(variableInfo.id,precision));
        this.increasedPrecision = true;
        return true;
      }
      this.precisionVariableIndex++;
    }
    this.precisionVariableIndex = 0;
    return false;
  }
  
  private boolean requiresRestart() {
    if(this.index + 1 > this.variableInfos.size()) {
      if(tryExpandNeighbourhood()) {
        resetExplorationParameters(false);
        return false;
      }
      return true;
    }
    return false;
  }
  
  private void resetExplorationParameters(boolean full) {
    this.patternMoves = 0;
    this.direction = this.lastDirection = -1;
    this.index = this.lastIndex = 0;
    if(full) {
      this.increasedPrecision = this.precisionChangeSuccess = false;
      this.precisionVariableIndex = 0;
    }
  }

  private Result terminate(double[] model) throws AVMSolutionFoundException {
    ff.eval(model);

    if(fitnessEvaluations >= fitnessBudget) {
      if(DEBUG) {
        System.out.println("failed to find an answer after " + fitnessEvaluations + " fitness evaluations");
      }
      return Result.NO_MORE_ITERATIONS;
    } else {
      return Result.NOT_FOUND;
    }
  }
}
