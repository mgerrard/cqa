package avm;

public class AVMSolutionFoundException extends Exception {

  public final double[] solution;
  
  public AVMSolutionFoundException(double[] solution) {
    super();
    this.solution = solution;
  }
  
}
