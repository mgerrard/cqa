CORAL is a collection of randomized constraint solvers for dealing
with numeric constraints, often involving floating-point variables and
mathematical functions.  A constraint solver takes as input a
constraint involving symbolic variables and reports as output an
assignment of values to variables.  For example, CORAL outputs
{**complete**} when solving the constraint sin(x) + cos(y) = 1.

The target application of CORAL is symbolic execution of scientific
programs.  Symbolic execution is a technique to systematically
generate test input data.  It requires a constraint solver component
to solve the constraints it generates during the analysis of a
program.  Certain classes of constraints, known as decidable, admit a
(decision) procedure that can deterministically answer whether or not
a constraint is satisfiable (with a witness solution).  The theory of
linear integer arithmetic is an example of decidable theory.

Unfortunately, symbolic execution, specially of scientific programs,
can generate undecidable constraints or constraints that the solver
does not support.  CORAL targets such kinds of programs.  The
hypothesis is that symbolic execution of these programs generate
constraints which are not amenable to solving with the only help of
one particular decision procedure.

The approach of CORAL to find a solution is to attempt to reduce the
problem (e.g., the domain of x is significantly reduced when we
observe it is used as parameter of function sin) and randomize search.
Currently, CORAL includes three types of solvers: RANDOM, GA (for
genetic algorithm) and PSO (for particle swarm optimization).  For the
heuristic-search solvers (GA and PSO), CORAL uses the opt4j
(http://opt4j.sourceforge.net/) implementation.

Potential users should try to run the script "run_manual_benchmark"
that shows how these solvers perform on a particular configuration of
inputs.

Important packages:

- coral.simplifier: equality decision procedure and rewriting rules
- coral.solvers: solvers implementation
- coral.tests: benchmarks and tests
- coral.util: utilities as configuration classes and visitors
- symlib: library of symbolic expressions
- symlib.eval: reverse polish calculator of constraints
- symlib.parser: recursive-descent parser of textual constraints

People involved:

Mateus Borges (mab@cin.ufpe.br)
Matheus Arrais (mbas@cin.ufpe.br)
Marcelo d'Amorim (damorim@cin.ufpe.br)

- enjoy
