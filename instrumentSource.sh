#!/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euo pipefail
else
    # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi
shopt -s nullglob globstar extglob

require(){ hash "$@" || exit 127; }

usage() {
	printf "Usage:"
	printf "  ./instrumentSources.sh source-file"
}

if (($# == 0)); then
	usage
    exit 1
fi

file="$1"
instr_file="$file.instr.c"
cp $file $instr_file

export PATTERN='extern void __VERIFIER_assume(int);'
export REPLACEMENT='#ifdef __UNCIVL__\n#include <civlc.cvh>\n#define __VERIFIER_assume(f) $assume(f)\n#else\nextern void __VERIFIER_assume(int);\n#endif'

if grep -Fl '#define __VERIFIER_assume(f) $assume(f)'  "$file" ; then
	echo "source file already instrumented: $file"
else
	echo "instrumenting $file ..."
	sed -i "s/${PATTERN}/${REPLACEMENT}/" "$instr_file" || true
	#perl -pe 'print $ENV{"PATTERN"}; print $ENV{"REPLACEMENT"}' "$file" || true
fi
sed -i -e ' /\(printf\|puts\)/ s~^\(//\)*~//~' "$instr_file" || true
