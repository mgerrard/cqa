#!/usr/bin/env bash

#set -x
SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CIVL_JAR="${SCRIPT_ROOT}/uncivl.jar"
PCP_RESTART_CMD="${SCRIPT_ROOT}/PathConditionsProbability/restartServerByPort.sh"

LOG_FOLDER="${SCRIPT_ROOT}/uncivl-logs"
mkdir -p "$LOG_FOLDER"

DEPTH_BOUND=1000

SEARCH="$1"
SUBJECT="$2"
TIMEOUT="${3:-3615}"
PCP_PORT="${4:-9001}"
SEED="${5:-227880627}"


usage() {
	echo "usage: ./runUNCIVL {search} {c-program-source} {timeout in seconds (default=3615)} {pcp-port (default=9001)} {seed}"
	echo "       where search is in [pse,sse,rnd]"
	echo "note that any running instances of PCP (or other java programs) that are listening to the specified port will be killed"
}

run_pse() {
	# why use arrays: https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md#use-arrays-ftw
	DFS_ARGS=(
		-jar
		"${CIVL_JAR}"
		verify
		-D__UNCIVL__
		-svcomp17
		-maxdepth="${DEPTH_BOUND}"
		-countPaths
		-errorBound=99999999
		-saveStates=true
		-simplifyCounting
		-pcpAddress=localhost:"${PCP_PORT}"
		-seed="${SEED}"
		)
	TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
	BASENAME="$(basename ${SUBJECT})"
	LOGFILE="${LOG_FOLDER}/${BASENAME}__pse__${TIMESTAMP}.log"
	timeout -k 10 "$TIMEOUT" /usr/bin/time java -Xmx8G "${DFS_ARGS[@]}" "$SUBJECT" 2>&1 | tee "$LOGFILE"
}

run_sse() {
	PSE_ARGS=(
		-jar
		"${CIVL_JAR}"
		verify
		-D__UNCIVL__
		-svcomp17
		-statistical
		-maxdepth="${DEPTH_BOUND}"
		-errorBound=99999999
		-saveStates=false
		-simplifyCounting
		-pcpAddress=localhost:"${PCP_PORT}"
		-seed="${SEED}"
	)
	TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
	BASENAME="$(basename ${SUBJECT})"
	LOGFILE="${LOG_FOLDER}/${BASENAME}__sse__${SEED}__${TIMESTAMP}.log"
	timeout -k 10 "$TIMEOUT" /usr/bin/time java -Xmx8G "${PSE_ARGS[@]}" "$SUBJECT" 2>&1 | tee "$LOGFILE"
}

run_rnd() {
	PSE_ARGS=(
		-jar
		"${CIVL_JAR}"
		verify
		-D__UNCIVL__
		-svcomp17
		-randomExploration
		-maxdepth="${DEPTH_BOUND}"
		-errorBound=99999999
		-saveStates=false
		-simplifyCounting
		-pcpAddress=localhost:"${PCP_PORT}"
		-seed="${SEED}"
	)
	TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
	BASENAME="$(basename ${SUBJECT})"
	LOGFILE="${LOG_FOLDER}/${BASENAME}__rnd__${SEED}__${TIMESTAMP}.log"
	timeout -k 10 "$TIMEOUT" /usr/bin/time java -Xmx8G "${PSE_ARGS[@]}" "$SUBJECT" 2>&1 | tee "$LOGFILE"
}


if [[ ! "$SUBJECT" || ! -f "$SUBJECT" ]]; then 
	echo "Error: Invalid or inexistent source file."
	usage
	exit 1
fi

# ensure that any __VERIFIER_assume calls are macro-ed into $assume
if grep -Fl '__VERIFIER_assume' "$SUBJECT" && ! grep -Fl '#define __VERIFIER_assume(f) $assume(f)'  "$SUBJECT" ; then
	echo "Error: source file wasn't instrumented correctly (missing \$assume)"
	exit 1
fi


PCP_PID=$("$PCP_RESTART_CMD" "$PCP_PORT" | tail -n1; exit ${PIPESTATUS[0]})
if [[ $? != 0 ]] ; then
	echo "Error: Invalid port number: $PCP_PORT"
	usage
	exit 1
fi

cleanup() {
	kill $PCP_PID
	sleep 5s
	kill -9 $PCP_PID
	exit
}
trap cleanup INT TERM EXIT

case "$SEARCH" in
	pse)
		run_pse
		;;
	sse)
		run_sse
		;;
	rnd)
		run_rnd
		;;
	*)
		echo "Invalid civl search type: $SEARCH"
		usage
		exit 1
		;;
esac
