# Conditional Quantitative Analysis

This repository includes one instantiation of the infrastructure making up a 
Conditional Quantitative Analysis (CQA),
as described in the eponymous article in Transactions on Software Engineering.

CQA combines evidence from different underlying analyses to compute bounds on the probability
of reaching a failure in some program.
Unlike the classical formulation of reachability in program analysis, where either a
path to a failure state exists or not, quantifying the probability
of reaching a failure state requires considering many paths, in
general.
Instead of trying to enumerate all program paths explicitly, CQA 
first determines which regions of the input space can lead to a failure 
state, and only quantifies this reduced portion of the program state
space.

The 136 subjects discussed in the TSE article are found in the `subjects/` directory;
the supplementary appendix and raw data generated in the evaluation are found
in the `artifacts/` directory;
and instructions on installing and running the tools from the study are
given below.
*These tools have only been tested on machines running Ubuntu 18.04.*

Assuming all tools have been successfully installed, you can run the five different kinds
of quantitative analyses described in the article---CQA_#, CQA_pse, CQA_sse, 
PSE, and SSE---on `foo.c` with the respective commands:

```bash
./cqa.sh mc foo.c  # 'mc' stands for 'model counting'
./cqa.sh pse foo.c
./cqa.sh sse foo.c
./pse.sh foo.c
./sse.sh foo.c
```

---

## Install ALPACA

First install ALPACA following the instructions [here](https://bitbucket.org/mgerrard/alpaca/), 
checking out revision `7b9f93a` on the `main` branch.

---

## Install Quantitative Tools

### Ubuntu 18.04 dependencies (can be installed with `sudo apt install ...`):

- `maven`
- `openjdk-8-jdk`
- `m4`
- `make`
- `automake`
- `autoconf`
- `libgmp-dev`
- `bison`
- `flex`
- `libtool`

The subsequent installation steps assume you start from this base directory.

#### Install qCORAL:
  
```bash
cd coral
./mvn-install.sh
mvn install
```

#### Install Realpaver:

```bash
tar -xzvf realpaver-0.4.tar.gz
cd realpaver-0.4
# if 'configure' fails below,
# try running only 'make'
./configure; make
sudo make install
```

#### Install Z3 (with Java bindings):

```bash
git clone https://github.com/Z3Prover/z3.git
cd z3/
python scripts/mk_make.py --java --python
cd build/
make -j10
sudo make install
cp com.microsoft.z3.jar ../../PathConditionsProbability/
cd ../../PathConditionsProbability/
./mvn-install-z3.sh
mvn clean package assembly:single install
```

#### Install NTL (assuming GMP prefix is the same as mine)

```bash
unzip ntl.zip
cd ntl/src
./configure NTL_GMP_LIP=on PREFIX=/usr GMP_PREFIX=/usr/include/x86_64-linux-gnu
make
sudo make install
```

#### Install Barvinok:

```bash
git clone https://repo.or.cz/barvinok.git
cd barvinok/
./get_submodules.sh
./autogen.sh
./configure --prefix=/usr --with-gmp-prefix=/usr/include/x86_64-linux-gnu --with-ntl-prefix=/usr
make
make check
sudo make install
```

#### Install PCP:

```bash
cd PathConditionsProbability/
mvn clean -DskipTests install
```
---
Run small test: `./cqa.sh mc test.c`

---

#### Acknowledgements

This material is based in part upon work supported by the 
National Science Foundation under grant numbers 1617916 and 1901769,
by the U.S. Army Research Office under grant number W911NF-19-1-0054,
and by the DARPA ARCOS program under contract FA8750-20-C-0507.
