#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: ./pse <foo.c>" >&2
    exit 1
fi

file=$1

if [ ! -f $file ]; then
    echo "error: file '$file' not found" >&2
    exit 1
fi

echo "* Running PSE on $file"
echo
java -jar uncivl.jar config
./instrumentSource.sh $file
./runUNCIVL.sh pse "$file.instr.c"

