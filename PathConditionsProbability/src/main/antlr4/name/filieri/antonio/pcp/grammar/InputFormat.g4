grammar InputFormat;

/* some fragments of this grammar were taken from https://github.com/antlr/grammars-v4/blob/master/clojure/Clojure.g4
  and https://es-static.fbk.eu/people/griggio/misc/smtlib2parser.html */


input: command*;

command: '(' (cmd_set_option 
            | cmd_dec_const
            | cmd_dec_var
            | cmd_dec_dep_var
            | cmd_assert
            | cmd_count
            | cmd_paving
            | cmd_clear
            | cmd_exit)
         ')'
	;

cmd_set_option: 'set-option' key=SYMBOL val=literal;
cmd_dec_const: 'declare-const' name=SYMBOL type=('Int'| 'Float' | 'Double' | 'Long' | 'Boolean' | 'String' | 'BV' | 'Data') val=expr;
cmd_dec_var: 'declare-var' name=SYMBOL type=typedecP ; 
cmd_dec_dep_var: 'declare-dependent-var' name=SYMBOL type=('Int'| 'Float' | 'Double' | 'Long' | 'Boolean' | 'String' | 'BV' | 'Data');

cmd_assert: 'assert' expr+;
cmd_count: 'count';
cmd_paving: 'paving';
cmd_clear: 'clear';
cmd_exit: 'exit';

typedecP : '(' typedec ')';            
            
typedec:   type=('Int'|'Long') lb=expr ub=expr            #DecUniformInt
           | type=('Float'|'Double')  lb=expr ub=expr     #DecUniformFloat
           | 'Normal' lb=expr ub=expr mean=expr sd=expr        #DecNormalFloat
           | 'Exponential' lb=expr ub=expr mean=expr           #DecExponentialFloat
           | 'Gamma' lb=expr ub=expr shape=expr scale=expr     #DecGammaFloat
           | 'Bernoulli' prob=expr             #DecBernoulliBoolean
           | 'HistogramInt' '(' bin+ ')'             #DecHistogramInt
           | 'HistogramFloat' '(' bin+ ')'           #DecHistogramFloat
           | 'String' regex=expr                     #DecString
           | 'BV' length=expr                        #DecBitVector
           | 'Data' whatever=expr                    #DecDataStructure

       ;

bin : '(' prob=expr lo=expr hi=expr ')'
       ;

expr: '(' op=('+'|'-'|'*'|'/'|'%'|'div'|'mod') expr expr+ ')'      #ExprArith
    | '(' '-' expr ')'                                             #ExprUnaryMinus
    | '(' op=('<'|'>'|'<='|'>='|'='|'!=') expr expr ')'            #ExprCmp
    | '(' op=('and'|'or') expr+ ')'                                #ExprBoolean
    | '(' 'not' expr ')'                                           #ExprNot
    | '(' 'ite' cond=expr ctrue=expr cfalse=expr ')'               #ExprIte
    | '(' fun=('sin'|'cos'|'tan'|'sqrt'|'log10'|'log') expr ')'    #ExprMathFunUnary                      
    | '(' fun=('pow'|'max'|'min'|'atan2') expr expr ')'            #ExprMathBinary
    | '(' fun=('char-at'|'starts-with'|'ends-with'|'substring')
               expr expr')'                                        #ExprString
    | '(' 'expr' stringLit ')'                                     #ExprInfix
    | '(' fun=('concat'|'bvand'|'bvor'|'bvnand'| 'bvnor' |
               'bvxor'|'bvxnor'|
               'bvadd'|'bvsub'|'bvmul'   |
               'bvudiv'|'bvurem' |
               'bvsdiv'|'bvsrem'|'bvsmod' |
               'bvshl' |'bvlshr'| 'bvashr' ) expr expr ')'         #ExprBvBin
    | '(' fun=('bvult' |'bvule' | 'bvugt' |
               'bvuge' |'bvslt' | 'bvsle' |
               'bvsgt' |'bvsge' | 'bvcomp') expr expr ')'          #ExprBvCmp
    | '(' 'extract' hi=expr lo=expr bv=expr ')'                    #ExprBvExtract
    | '(' fun=('bvnot'|'bvneg') expr')'                            #ExprBvUnary
    | '(' 'let' '(' letDef+ ')' expr ')'                           #ExprLet
    | literal                                                      #ExprLiteral
    ;

letDef: '(' name=literal expr ')';


//NOTE: we omit the initial underline in '(extract hi lo bv)' since
//there are no plans to support indexed identifiers

literal: stringLit
       | numberLit
       | booleanLit
       | bvLit
       | symbol
       ;

stringLit : STRING;

numberLit : intLit
 //    | decimal
       | floatLit
       ;

intLit : NUMERAL;
floatLit: FLOAT;
booleanLit : BOOLEAN;
bvLit : value=BINARY   #BvBinLit
      | value=HEX      #BvHexLit
      ;
symbol : SYMBOL;

    
FLOAT
    : '-'? [0-9]+ FLOAT_TAIL
    | '-'? 'Infinity'
    | '-'? 'NaN'
    ;

fragment
FLOAT_TAIL
    : FLOAT_DECIMAL FLOAT_EXP
    | FLOAT_DECIMAL
    | FLOAT_EXP
    ;

fragment
FLOAT_DECIMAL
    : '.' [0-9]+
    ;

fragment
FLOAT_EXP
    : [eE] '-'? [0-9]+
    ;
         

NUMERAL : '0' | '-'? ([1-9][0-9]*) ;
HEX : '#x'[0-9abcdefABCDEF]+ ;
BINARY : '#b'[0-1]+ ;

//DECIMAL : '0' | '-'? ([1-9][0-9]*)[.]([0-9]+);
STRING : '"' ( ~'"' | '\\' '"' )* '"' ;
BOOLEAN : 'true' | 'false';
//KEYWORD : ':' [0-9a-zA-Z~!@$%^&*_+=<>.?/\-]* ;
SYMBOL : [a-zA-Z~!@$%^&*_+=<>.?/\-:] [0-9a-zA-Z~!@$%^&*_+=<>.?/\-]* ;
WS : (' ' | '\t' | '\n' | '\r' | '\f')+  -> channel(HIDDEN);
COMMENT : ';' .*? ('\n'|'\r'| EOF)  -> channel(HIDDEN);