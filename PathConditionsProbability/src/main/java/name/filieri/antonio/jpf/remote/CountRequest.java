package name.filieri.antonio.jpf.remote;

import java.io.Serializable;

import name.filieri.antonio.jpf.domain.Problem;

public class CountRequest implements Serializable {
	public enum Command {
		COUNT, STOP
	}
	public static final CountRequest STOP = new CountRequest(null, Command.STOP);

	private static final long serialVersionUID = 5258818771968014450L;

	private final Problem problem;
	private final Command command;

	public CountRequest(Problem problem) {
		this(problem, Command.COUNT);
	}

	public CountRequest(Problem problem, Command command) {
		super();
		this.problem = problem;
		this.command = command;
	}

	public final Problem getProblem() {
		return problem;
	}

	public final Command getCommand() {
		return command;
	}
}
