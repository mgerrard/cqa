package name.filieri.antonio.pcp;

import com.google.common.escape.Escaper;
import com.google.common.escape.Escapers;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Stream;
import name.filieri.antonio.pcp.Options.Key;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.ast.commands.PavingCommand;
import name.filieri.antonio.pcp.errors.StorageErrorListener;
import name.filieri.antonio.pcp.errors.StorageErrorListener.ErrorMessage;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.utils.ExitRequestException;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class PCPServer extends Thread {

  private final ServerSocket serverSocket;
  private final CommandEvaluator evaluator;
  private final boolean singleSocket;
  private volatile boolean isStopped = false;

  public PCPServer(int port, boolean singleSocket, boolean overridePreprocessing)
      throws IOException {
    this.serverSocket = new ServerSocket(port);

    Options options = Options.loadFromUserHomeOrDefault();
    if (overridePreprocessing) {
      System.out.println(
          "[pcp-server] Preprocessing will not be applied due to the current set of options!");
      options.processOption(Key.OVERRIDE_PREPROCESSING, "true");
    }
    SymbolTable stable = new SymbolTable();
    Partitioner partitioner = new ConstraintPartitioner();
    CountDispatcher dispatcher = new PCPDispatcher(options);
    this.evaluator = new CommandEvaluator(stable, partitioner, dispatcher, options);
    this.singleSocket = singleSocket;
  }

  public static void main(String[] args) {
    PCPServer server = null;
    try {
      int port;
      if (args.length == 0) {
        port = 9991;
      } else {
        port = Integer.parseInt(args[0]);
      }
      boolean singleSocket = Stream.of(args).anyMatch(s -> s.trim().equals("-ss"));
      boolean overridePreprocessing = Stream.of(args).anyMatch(s -> s.trim().equals("-op"));
      server = new PCPServer(port, singleSocket, overridePreprocessing);
      server.start();
      System.out.println("PCPV2 Server started at: " + port);
      server.join();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      if (server != null) {
        try {
          server.serverSocket.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    System.out.println("PCPV2 Server finished");
  }

  private void sendErrorMsg(PrintWriter out, String message) {
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append("(error");
    Escaper escaper = Escapers.builder()
        .addEscape('"', "''")
        .build();
    errorMsg.append(" \"");
    errorMsg.append(escaper.escape(message));
    errorMsg.append(" \"");
    errorMsg.append(")");
    out.println(errorMsg);
    out.flush();
  }

  private void sendParseErrorMsg(PrintWriter out, StorageErrorListener errorListener) {
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append("(error");
    Escaper escaper = Escapers.builder()
        .addEscape('"', "''")
        .build();
    for (ErrorMessage msg : errorListener.messages) {
      errorMsg.append(" \"");
      errorMsg.append(escaper.escape(msg.toString()));
      errorMsg.append(" \"");
    }
    errorMsg.append(")");
    out.println(errorMsg);
    out.flush();
  }

  @Override
  public void run() {
    boolean replySent;
    PrintWriter out = null;
    while (!isStopped) {
      replySent = false;
      System.out.println("[pcpserver] Waiting for connection...");

      try (Socket clientSocket = serverSocket.accept();
          BufferedReader in = new BufferedReader(
              new InputStreamReader(clientSocket.getInputStream(),
                  StandardCharsets.UTF_8))
      ) {
        System.out.println("[pcpserver] Connected!");

        out = new PrintWriter(clientSocket.getOutputStream());

        String line;

        while (!in.ready()) {
          Thread.sleep(1);
        }

        long openParenthesis = 0;

        StringBuilder sb = new StringBuilder();
        while ((line = in.readLine()) != null) {
          if (line.trim().isEmpty()) {
            continue;
          }
          long leftp = line.chars().filter(ch -> ch == '(').count();
          long rightp = line.chars().filter(ch -> ch == ')').count();
          openParenthesis += (leftp - rightp);
          if (openParenthesis > 0) {
            //line is likely bigger than 8kb; store fragment and read more
            sb.append(line);
            continue;
          } else {
            //rebuild line, clear buffer
            sb.append(line);
            line = sb.toString();
            sb = new StringBuilder();
            openParenthesis = 0; //parser will notify user if openParenthesis < 0
          }
          System.out.println("[pcpserver] query received: " + line);
          StorageErrorListener errorListener = new StorageErrorListener();
          ANTLRInputStream input = new ANTLRInputStream(line);
          InputFormatLexer lexer = new InputFormatLexer(input);
          lexer.addErrorListener(errorListener);
          InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));
          parser.addErrorListener(errorListener);

          ParseTree tree = parser.input();
          if (!errorListener.messages.isEmpty()) {
            sendParseErrorMsg(out, errorListener);
            continue;
          }

          List<Command> commands;
          try {
            commands = AstBuilder.parseCommands(evaluator.getSymbolTable(), tree);
          } catch (RuntimeException e) {
            sendErrorMsg(out, e.getMessage());
            continue;
          }

          for (Command command : commands) {
            Reply reply = command.accept(evaluator);
            if (reply.error) {
              out.println("(error \"" + reply.message + "\")");
              out.flush();
            } else if (command instanceof CountCommand) {
              CountResult result = reply.result;
              out.println("(" + result + ")");
              out.flush();
            } else if (command instanceof PavingCommand) {
              out.print(reply.message);
              out.flush();
            }
            replySent = true;
          }
        }
      } catch (ExitRequestException e) {
        try {
          serverSocket.close();
        } catch (IOException ex) {
          //we're leaving anyway...
        }
        isStopped = true;
      } catch (Exception e) {
        System.out.println("[PCPServer] Unexpected exception thrown:  " + e);
        if (!replySent && out != null && !out.checkError()) {
          out.println("(error \"Internal error\")");
          out.flush();
        }
        e.printStackTrace();
      } finally {
        out.close();
        if (singleSocket) {
          System.out.println("[PCPServer] Socket closed; finishing server...");
          isStopped = true;
        }
      }
    }
  }
}