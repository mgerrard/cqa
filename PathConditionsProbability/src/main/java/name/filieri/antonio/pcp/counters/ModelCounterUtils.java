package name.filieri.antonio.pcp.counters;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FPExpr;
import com.microsoft.z3.FPSort;
import com.microsoft.z3.Solver;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;

public class ModelCounterUtils {

	public static HashFunction getHashFunction() {
		return Hashing.goodFastHash(32);
	}

	public static class Z3VarMapping {

		public final ImmutableMap<String, BitVecExpr> nameToBvs;
		public final ImmutableMap<String, FPExpr> nameToFps;
		public final ImmutableList<BoolExpr> domain;

		public Z3VarMapping(ImmutableMap<String, BitVecExpr> nameToBvs,
				ImmutableMap<String, FPExpr> nameToFps,
				ImmutableList<BoolExpr> domain) {
			this.nameToBvs = nameToBvs;
			this.nameToFps = nameToFps;
			this.domain = domain;
		}

		public Z3VarMapping(Map<String, BitVecExpr> nameToZ3BV, Map<String, FPExpr> nameToZ3FP,
				List<BoolExpr> domain) {
			this.nameToBvs = ImmutableMap.copyOf(nameToZ3BV);
			this.nameToFps = ImmutableMap.copyOf(nameToZ3FP);
			this.domain = ImmutableList.copyOf(domain);
		}
	}

	/**
	 * Declare bitvector variables (define-const) within the solver. Floating-point variables will result in
	 * the declaration of two z3 variables: a bitvector, which we'll use for bitblasting, and a SMT-LIB
	 * floating point variable that will be used for other operations. See
	 * the following note from the standard:
	 *
	 * :note
	 * "There is no function for converting from (_ FloatingPoint eb sb) to the
	 * corresponding IEEE 754-2008 binary format, as a bit vector (_ BitVec m) with
	 * m = eb + sb, because (_ NaN eb sb) has multiple, well-defined representations.
	 * Instead, an encoding of the kind below is recommended, where f is a term
	 * of sort (_ FloatingPoint eb sb):
	 *
	 * (declare-fun b () (_ BitVec m))
	 * (assert (= ((_ to_fp eb sb) b) f))
	 * "
	 * @param vars
	 * @param ctx
	 * @param solver
	 * @return
	 */


	public static Z3VarMapping declareVars(Set<Variable> vars, Context ctx) {
		Map<String, BitVecExpr> nameToZ3BV = new HashMap<>();
		Map<String, FPExpr> nameToZ3FP = new HashMap<>();
		List<BoolExpr> domainConstraints = new ArrayList<>();
		for (Variable var : vars) {
			BoundedVariable nvar = (BoundedVariable) var;
			int bvlength;
			String lower;
			String higher;
			switch (nvar.getType()) {
			case BOOLEAN:
				bvlength = 1;
				lower = "0";
				higher = "1";
				break;
			case DOUBLE:
				bvlength = 64;
				lower = nvar.getLowerBound().doubleValue() + "";
				higher = nvar.getUpperBound().doubleValue() + "";
				break;
			case FLOAT:
				bvlength = 32;
				lower = nvar.getLowerBound().floatValue() + "";
				higher = nvar.getUpperBound().floatValue() + "";
				break;
			case INT:
				bvlength = 32;
				lower = nvar.getLowerBound().intValue() + "";
				higher = nvar.getUpperBound().intValue() + "";
				break;
			case LONG:
				bvlength = 64;
				lower = nvar.getLowerBound().longValue() + "";
				higher = nvar.getUpperBound().longValue() + "";
				break;
			case BV:
				bvlength = ((BitVectorVariable) var).getLength();
				lower = nvar.getLowerBound() + "";
				higher = nvar.getUpperBound() + "";
				break;
			case DATA:
			case STRING:
			default:
				throw new RuntimeException("Unsupported var type for bitblasted model counters! " + nvar);
			}

			BitVecExpr bv = ctx.mkBVConst("v" + var.getName(), bvlength);
			nameToZ3BV.put(var.getName(), bv);
			if (nvar.getType() == ExpressionType.BV) {
				domainConstraints.add(ctx.mkBVUGE(bv, ctx.mkBV(lower, bvlength)));
				domainConstraints.add(ctx.mkBVULE(bv, ctx.mkBV(higher, bvlength)));
			} else if (nvar.getType().isFloat()) {
				FPSort sort = bvlength == 32 ? ctx.mkFPSort32() : ctx.mkFPSort64();
				FPExpr fpvar = (FPExpr) ctx.mkConst("fp_v"+ var.getName(), sort);
				nameToZ3FP.put(var.getName(),fpvar);

				domainConstraints.add(ctx.mkEq(ctx.mkFPToFP(bv,sort),fpvar));
				domainConstraints.add(ctx.mkFPGEq(fpvar, (FPExpr) ctx.mkNumeral(lower, sort)));
				domainConstraints.add(ctx.mkFPLEq(fpvar, (FPExpr) ctx.mkNumeral(higher, sort)));
			} else {
				domainConstraints.add(ctx.mkBVSGE(bv, ctx.mkBV(lower, bvlength)));
				domainConstraints.add(ctx.mkBVSLE(bv, ctx.mkBV(higher, bvlength)));
			}
		}
		return new Z3VarMapping(nameToZ3BV,nameToZ3FP,domainConstraints);
	}

	public static void assertConstraints(Constraint constraint, Context ctx, Solver solver,
			Z3VarMapping varMapping) {
		Z3BitVecVisitor visitor = new Z3BitVecVisitor(ctx, varMapping);
		constraint.getExpr().accept(visitor);

		Deque<Expr> deque = visitor.getEvalStack();
		if (deque.size() != 1) {
			throw new RuntimeException("Error: there should be only one expression in the stack!");
		}
		solver.add((BoolExpr) deque.pop());
	}

	public static List<BoolExpr> collectZ3Constraints(Constraint constraint, Context ctx, Solver solver,
			Z3VarMapping varMap) {
		List<BoolExpr> z3Exprs = new ArrayList<>();
		Z3BitVecVisitor visitor = new Z3BitVecVisitor(ctx, varMap);
		constraint.getExpr().accept(visitor);

		Deque<Expr> deque = visitor.getEvalStack();
		if (deque.size() != 1) {
			throw new RuntimeException("Error: there should be only one expression in the stack!");
		}
		z3Exprs.add((BoolExpr) deque.pop());
		return z3Exprs;
	}
}
