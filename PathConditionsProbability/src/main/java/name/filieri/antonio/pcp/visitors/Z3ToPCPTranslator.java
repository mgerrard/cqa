package name.filieri.antonio.pcp.visitors;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.IntNum;
import com.microsoft.z3.enumerations.Z3_decl_kind;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;

public class Z3ToPCPTranslator {

  private final Map<Expr, Variable> z3ToPCPVars;

  public Z3ToPCPTranslator(Map<Expr, Variable> z3ToPCPVars) {
    this.z3ToPCPVars = z3ToPCPVars;
  }

  public BooleanExpression translate(BoolExpr[] conjuncts) {
    List<BooleanExpression> translatedClauses = Arrays.stream(conjuncts)
        .map(arg -> (BooleanExpression) translate(arg))
        .collect(Collectors.toList());
    return new BooleanNaryExpression(BooleanNaryExpression.Function.AND, translatedClauses);
  }

  public Expression translate(Expr expr) {
    if (expr.isIntNum()) {
      return new NumericConstant(((IntNum) expr).getInt(), ExpressionType.INT);
    } else if (expr.isConst() && expr.isInt()
        && expr.getFuncDecl().getDeclKind() == Z3_decl_kind.Z3_OP_UNINTERPRETED) {
      return z3ToPCPVars.get(expr);
    } else if (expr.isTrue()) {
      return BooleanConstant.TRUE;
    } else if (expr.isFalse()) {
      return BooleanConstant.FALSE;
    } else if (expr.isAdd()) {
      return handleNumericBinOp(Function.ADD, expr);
    } else if (expr.isSub()) {
      if (expr.getArgs().length == 1) {
        Expression arg = translate(expr.getArgs()[0]);
        return new NumericBinaryExpression(NumericConstant.valueOf(0), arg, Function.SUB);
      } else {
        return handleNumericBinOp(Function.SUB, expr);
      }
    } else if (expr.isMul()) {
      return handleNumericBinOp(Function.MUL, expr);
    } else if (expr.isDiv() || expr.isIDiv()) {
      return handleNumericBinOp(Function.DIV, expr);
    } else if (expr.isAnd()) {
      return handleBooleanNAryOp(BooleanNaryExpression.Function.AND, expr);
    } else if (expr.isOr()) {
      return handleBooleanNAryOp(BooleanNaryExpression.Function.OR, expr);
    } else if (expr.isNot()) {
      BooleanExpression first = (BooleanExpression) translate(expr.getArgs()[0]);
      return new NotExpression(first);
    } else if (expr.isGE()) {
      return handleComparison(NumericComparisonExpression.Function.GE, expr);
    } else if (expr.isGT()) {
      return handleComparison(NumericComparisonExpression.Function.GT, expr);
    } else if (expr.isEq()) {
      return handleComparison(NumericComparisonExpression.Function.EQ, expr);
    } else if (expr.isLT()) {
      return handleComparison(NumericComparisonExpression.Function.LT, expr);
    } else if (expr.isLE()) {
      return handleComparison(NumericComparisonExpression.Function.LE, expr);
    } else {
      throw new RuntimeException("Not implemented yet: \n"
          + "  expr: " + expr + "\n  sort=" + expr.getSort());
    }
  }

  private NumericComparisonExpression handleComparison(NumericComparisonExpression.Function func,
      Expr expr) {
    if (expr.getArgs().length > 2) {
      throw new RuntimeException("comparison with > 2 args - need to handle this");
    }
    Expression first = translate(expr.getArgs()[0]);
    Expression second = translate(expr.getArgs()[1]);
    return new NumericComparisonExpression(first, second, func);
  }

  private BooleanNaryExpression handleBooleanNAryOp(BooleanNaryExpression.Function func,
      Expr expr) {
    List<BooleanExpression> translatedArgs = Arrays.stream(expr.getArgs())
        .map(arg -> (BooleanExpression) translate(arg))
        .collect(Collectors.toList());
    return new BooleanNaryExpression(func, translatedArgs);
  }

  private NumericBinaryExpression handleNumericBinOp(Function func, Expr expr) {
    Expression first = translate(expr.getArgs()[0]);
    Expression second = translate(expr.getArgs()[1]);
    NumericBinaryExpression binOp = new NumericBinaryExpression(first, second, func);

    for (int i = 2; i < expr.getArgs().length; i++) {
      Expression next = translate(expr.getArgs()[i]);
      binOp = new NumericBinaryExpression(binOp, next, func);
    }

    return binOp;
  }
}
