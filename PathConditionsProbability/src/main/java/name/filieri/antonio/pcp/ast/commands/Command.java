package name.filieri.antonio.pcp.ast.commands;

import name.filieri.antonio.pcp.ast.Node;

/**
 * Node interface for built-in commands (declare-var, etc.)
 */

public interface Command extends Node {

  CommandType getCommandType();

  <T> T accept(CommandVisitor<T> visitor);

  enum CommandType {
    SET_OPTION, DECLARE_CONST, DECLARE_VAR, DECLARE_EXPR, ASSERT, COUNT, CLEAR, PAVING, DECLARE_DEP_VAR, EXIT
  }
}
