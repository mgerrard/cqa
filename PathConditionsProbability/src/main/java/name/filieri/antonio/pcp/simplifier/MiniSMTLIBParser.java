package name.filieri.antonio.pcp.simplifier;

import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.google.common.base.VerifyException;
import com.google.common.collect.ImmutableList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.simplifier.MiniSMTLIBParser.Token.Type;

public class MiniSMTLIBParser {

  private final Reader reader;
  private final Map<String, Variable> nameToPcpVar;

  private static Set<Character> ID_SYMBOLS;

  {
    ID_SYMBOLS = new HashSet<>();
    for (char c = 'a'; c <= 'z'; c++) {
      ID_SYMBOLS.add(c);
    }
    for (char c = 'A'; c <= 'Z'; c++) {
      ID_SYMBOLS.add(c);
    }
    ID_SYMBOLS.addAll(ImmutableList
        .of('+', '=', '/', '*', '%', '?', '!', '$', '-', '_', '~', '&', '^', '<', '>', '@', '.'));
  }


  public MiniSMTLIBParser(File f, Map<String, Variable> nameToPcpVar) throws FileNotFoundException {
    this.reader = new BufferedReader(
        new InputStreamReader(new FileInputStream(f),
            StandardCharsets.UTF_8));
    this.nameToPcpVar = nameToPcpVar;
  }

  public MiniSMTLIBParser(String s, Map<String, Variable> nameToPcpVar) {
    this.reader = new StringReader(s);
    this.nameToPcpVar = nameToPcpVar;
  }

  public Expression parse() throws ParserException {
    //populate the lookahead;
    try {
      scanToken();
      return expression();
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
        e.printStackTrace(); //don't think we can do anything else, really
      }
    }
  }

  public BooleanExpression parseGoals() throws ParserException {
    //populate the lookahead;
    try {
      scanToken();
      return goals();
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
        e.printStackTrace(); //don't think we can do anything else, really
      }
    }
  }

  private BooleanExpression goals() throws ParserException {
    expect(scanToken(), Type.LEFT_PAREN);
    Token op = scanToken();
    expect(op, Type.IDENTIFIER);
    checkError(op);

    if (!op.content.equals("goals")) {
      throw new ParserException("Expected 'goals', found: " + op.content);
    }

    List<BooleanExpression> subgoals = new ArrayList<>();
    subgoals.add(goal()); //at least one goal

    Token lookahead = peekToken();
    while (lookahead.type != Type.RIGHT_PAREN) {
      subgoals.add(goal());
      lookahead = peekToken();
    }

    expect(scanToken(), Type.RIGHT_PAREN);
    return new BooleanNaryExpression(BooleanNaryExpression.Function.AND, subgoals);
  }

  private BooleanExpression goal() throws ParserException {
    expect(scanToken(), Type.LEFT_PAREN);
    Token op = scanToken();
    expect(op, Type.IDENTIFIER);

    if (!op.content.equals("goal")) {
      throw new ParserException("Expected 'goal', found: " + op.content);
    }

    List<BooleanExpression> conjunction = new ArrayList<>();

    //at least one expression/constant
    Token lookahead = peekToken();
    do {
      if (lookahead.type == Type.IDENTIFIER) { //must be a constant
        if (lookahead.content.equals("true")) {
          conjunction.add(BooleanConstant.TRUE);
          scanToken();
        } else if (lookahead.content.equals("false")) {
          conjunction.add(BooleanConstant.FALSE);
          scanToken();
        } else {
          throw new VerifyException("We don't have boolean variables (everything becomes an integer), so this should never be reached!");
        }
      } else {
        conjunction.add((BooleanExpression) expression());
      }

      lookahead = peekToken();
    } while (lookahead.type == Type.IDENTIFIER || lookahead.type == Type.LEFT_PAREN);


    boolean mayBeArg = false;
    while (lookahead.type == Type.KEYWORD) {
      scanToken();
      lookahead = peekToken();

      if (lookahead.type == Type.IDENTIFIER
          || lookahead.type == Type.NUMBER) { //skip argument
        scanToken();
        lookahead = peekToken();
      }
    }

    expect(scanToken(), Type.RIGHT_PAREN);

    if (conjunction.size() == 1) {
      return conjunction.get(0);
    }
    return new BooleanNaryExpression(BooleanNaryExpression.Function.AND, conjunction);
  }

  private void checkError(Token op) throws ParserException {
    if (op.content.equals("error")) {
      CharBuffer cb = CharBuffer.allocate(8192); //8kb
      try {
        reader.read(cb);
      } catch (IOException e) {
        throw new ParserException(e);
      }
      throw new ParserException("SMTLIB tool reports an error: " + cb.toString());
    }
  }

  private Expression expression() throws ParserException {
    expect(scanToken(), Type.LEFT_PAREN);

    Token op = scanToken();
    expect(op, Type.IDENTIFIER);

    checkError(op);

    List<Expression> args = new ArrayList<>();
    Token lookahead = peekToken();
    String lookaheadContents = lookahead.content;
    while (lookahead.type != Type.RIGHT_PAREN) {
      if (lookahead.type == Type.IDENTIFIER) {
        if (lookaheadContents.equals("true")) {
          scanToken();
          args.add(BooleanConstant.TRUE);
        } else if (lookaheadContents.equals("false")) {
          scanToken();
          args.add(BooleanConstant.FALSE);
        } else {
          args.add(variable());
        }
      } else if (lookahead.type == Type.NUMBER) {
        args.add(constant());
      } else if (lookahead.type == Type.LEFT_PAREN) {
        args.add(expression());
      } else {
        throw new ParserException(
            "Not sure what to do with " + lookahead.type + ":" + lookaheadContents);
      }
      lookahead = peekToken();
      lookaheadContents = lookahead.content;
    }
    expect(scanToken(), Type.RIGHT_PAREN);

    return buildExpression(op.content, args);
  }

  private Expression buildExpression(String op, List<Expression> args) throws ParserException {
    switch (op) {
      case "<":
      case "<=":
      case "=":
      case ">=":
      case ">":
      case "!=":
        Preconditions.checkArgument(args.size() == 2);
        NumericComparisonExpression comp = new NumericComparisonExpression(args.get(0), args.get(1),
            NumericComparisonExpression.Function.fromSymbol(op).get());
        return comp;
      case "+":
        return binExprHelper(args, Function.ADD);
      case "*":
        return binExprHelper(args, Function.MUL);
      case "/":
      case "div": //FIXME this is integer division
        return binExprHelper(args, Function.DIV);
      case "mod":
        return binExprHelper(args, Function.MOD);
      case "-":
        if (args.size() == 1) {
          return binExprHelper(ImmutableList.of(NumericConstant.valueOf(-1), args.get(0)),
              Function.MUL);
        } else {
          return binExprHelper(args, Function.SUB);
        }
      case "and":
        return new BooleanNaryExpression(BooleanNaryExpression.Function.AND,
            args.stream().map(e -> (BooleanExpression) e).collect(
                Collectors.toList()));
      case "or":
        return new BooleanNaryExpression(BooleanNaryExpression.Function.OR,
            args.stream().map(e -> (BooleanExpression) e).collect(
                Collectors.toList()));
      case "not":
        Preconditions.checkArgument(args.size() == 1);
        return new NotExpression((BooleanExpression) args.get(0));
      default:
        throw new ParserException("Unknown function: " + op);
    }
  }

  private Expression binExprHelper(List<Expression> args, Function fun) {
    Preconditions.checkArgument(args.size() >= 2);
    Expression prev = args.get(0);
    NumericBinaryExpression bin = null;
    for (Expression next : args.subList(1, args.size())) {
      bin = new NumericBinaryExpression(prev, next, fun);
      prev = bin;
    }
    return bin;
  }

  private Variable variable() throws ParserException {
    Token tok = scanToken();
    Variable var = nameToPcpVar.get(tok.content);
    if (var == null) {
      throw new ParserException("Unknown var: " + tok.content);
    }
    return var;
  }

  private Expression constant() throws ParserException {
    Token tok = scanToken();
    NumericConstant constant;
    if (tok.content.contains(".")) {
      Number value = Double.parseDouble(tok.content);
      constant = new NumericConstant(value, ExpressionType.DOUBLE, PositionInfo.DUMMY);
    } else {
      Number value = Integer.parseInt(tok.content);
      constant = new NumericConstant(value, ExpressionType.INT, PositionInfo.DUMMY);
    }
    return constant;
  }

  private Token current;
  private Token next;

  //TODO move this to a class

  private Token peekToken() {
    return next;
  }

  private Token scanToken() throws ParserException {
    Token t = justScan();
    current = next;
    next = t;
    return current;
  }

  private Token justScan() throws ParserException {
    char next = ' ';
    while (next != '\0') {
      next = peek();
      switch (next) {
        case '(':
          advance();
          return new Token(Type.LEFT_PAREN, "(");
        case ')':
          advance();
          return new Token(Type.RIGHT_PAREN, ")");
        case '\0':
          return new Token(Type.EOF, "");
        case ' ':
        case '\n':
        case '\t':
        case '\r':
          advance();
          continue;
        case ':':
          return keyword();
        default:
          if (Character.isDigit(next)) {
            return number();
          } else if (ID_SYMBOLS.contains(next)) {
            return identifier();
          } else {
            throw new ParserException("Not sure what to do with character " + next);
          }
      }
    }
    throw new VerifyException("Not supposed to reach this point");
  }

  private Token keyword() throws ParserException {
    StringBuilder sb = new StringBuilder();
    char next = peek();
    Verify.verify(next == ':');
    sb.append(next); //colon

    advance();
    next = peek();
    if (!ID_SYMBOLS.contains(next)) {
      throw new ParserException("Invalid char following a : -> " + next);
    }
    sb.append(next);
    advance();
    next = peek();

    while (ID_SYMBOLS.contains(next) || Character.isDigit(next)) {
      sb.append(next);
      advance();
      next = peek();
    }

    return new Token(Type.KEYWORD, sb.toString());
  }

  private Token number() throws ParserException {
    StringBuilder sb = new StringBuilder();
    char next = peek();
    while (Character.isDigit(next)) {
      sb.append(next);
      advance();
      next = peek();
    }
    if (next == '.') {
      sb.append('.');
      advance();
      next = peek();

      while (Character.isDigit(next)) {
        sb.append(next);
        advance();
        next = peek();
      }
    }

    return new Token(Type.NUMBER, sb.toString());
  }


  private Token identifier() throws ParserException {
    StringBuilder sb = new StringBuilder();
    char next = peek();

    if (ID_SYMBOLS.contains(next)) {
      sb.append(next);
      advance();
      next = peek();
    }

    while (ID_SYMBOLS.contains(next) || Character.isDigit(next)) {
      sb.append(next);
      advance();
      next = peek();
    }

    return new Token(Type.IDENTIFIER, sb.toString());
  }

  private String peek(int nChars) throws ParserException {
    Preconditions.checkArgument(nChars > 0, "nChars must be positive");
    try {
      reader.mark(nChars + 1);
      char[] buf = new char[nChars];
      int read = reader.read(buf);
      if (read <= 0) {
        return "";
      }
      reader.reset();
      return String.valueOf(buf);
    } catch (IOException e) {
      throw new ParserException(e);
    }
  }

  private char peek() throws ParserException {
    try {
      reader.mark(3);
      int c = reader.read();
      if (c < 0) {
        return '\0';
      }
      reader.reset();
      return (char) c;
    } catch (IOException e) {
      throw new ParserException(e);
    }
  }

  private char advance() throws ParserException {
    try {
      int c = reader.read();
      if (c < 0) {
        return '\0';
      }
      return (char) c;
    } catch (IOException e) {
      throw new ParserException(e);
    }
  }

  private String advance(int size) throws ParserException {
    Preconditions.checkArgument(size > 0, "size must be positive");
    try {
      char[] buf = new char[size];
      int read = reader.read(buf);
      if (read <= size) {
        throw new ParserException("unexpected end of stream");
      }
      return String.valueOf(buf);
    } catch (IOException e) {
      throw new ParserException(e);
    }
  }

  private void expect(Token tok, Type expectedType) throws ParserException {
    if (tok == null || tok.type != expectedType) {
      throw new ParserException("Expected token " + expectedType + " but found " + tok.type);
    }
  }

  static class Token {

    public final Type type;
    public final String content;

    private Token(Type type, String content) {
      this.type = type;
      this.content = content;
    }

    enum Type {
      LEFT_PAREN,
      RIGHT_PAREN,
      IDENTIFIER,
      NUMBER,
      KEYWORD,
      EOF;
    }
  }


  public static class ParserException extends Exception {

    public ParserException(String message) {
      super(message);
    }

    public ParserException(Exception e) {
      super(e);
    }
  }

}
