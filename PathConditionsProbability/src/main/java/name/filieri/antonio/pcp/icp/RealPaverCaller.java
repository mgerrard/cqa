package name.filieri.antonio.pcp.icp;

import com.google.common.collect.Range;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.Options;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.Variable;

public class RealPaverCaller implements Paver {

  public String command;
  public int timeout; //in ms
  public int maxBoxes;

  public RealPaverCaller(Options options) {
    this.command = options.getRealPaverPath();
    this.timeout = options.getRealPaverTimeout();
    this.maxBoxes = options.getRealPaverMaxBoxes();
  }

  /**
   * Precondition - constraint must be a flattened conjunction; no ORs or NOTs
   *
   * @param constraint Conjunction of real and/or integer constraints
   */
  @Override
  public List<Box> takePaving(Constraint constraint) {
    try {
      String input = prepareInput(constraint);
      File rpInput = File.createTempFile("realpaver", ".in");
      File rpOutput = File.createTempFile("realpaver", ".out");
      rpInput.deleteOnExit();
      rpOutput.deleteOnExit();
      Files.write(rpInput.toPath(), input.getBytes());

      ProcessBuilder pb = new ProcessBuilder(command, rpInput.getCanonicalPath());
      pb.redirectOutput(Redirect.to(rpOutput));
      Process p = pb.start();
      p.waitFor();

      Map<String, Variable> nameToVar = constraint.getVariables().stream()
          .collect(Collectors.toMap(Variable::getName, Function.identity()));

      return processOutput(rpOutput, nameToVar);
    } catch (IOException e) {
      throw new RuntimeException("Something went wrong while running RP: ", e);
    } catch (InterruptedException e) {
      throw new RuntimeException("Something went wrong while running RP: ", e);
    }
  }


  private String prepareInput(Constraint constraint) {
    StringBuilder rpInput = new StringBuilder();
    Set<Variable> vars = constraint.getVariables();

    rpInput.append("Time = " + timeout + ";\n");
    rpInput.append("Branch\n");
    rpInput.append("\tprecision = 1.0e-3,\n");
    rpInput.append("\tnumber = " + maxBoxes + ",\n");
    rpInput.append("\tmode = paving;\n\n");

    rpInput.append("Variables\n");
    vars.stream()
        .sorted(Comparator.comparing(v -> v.getName()))
        .forEach(v -> {
          if (!(v instanceof BoundedVariable)) {
            return;
          }
          BoundedVariable bv = ((BoundedVariable) v);
          rpInput.append('\t');
          rpInput.append(bv.getName());
          rpInput.append(" in [");
          rpInput.append(bv.getLowerBound().doubleValue());
          rpInput.append(", ");
          rpInput.append(bv.getUpperBound().doubleValue());
          rpInput.append("],\n");
        });
    rpInput.delete(rpInput.length() - 2, rpInput.length());
    rpInput.append(";\n\n");

    RealPaverVisitor rpv = new RealPaverVisitor();
    constraint.getExpr().accept(rpv);

    rpInput.append("Constraints \n");
    for (String cons : rpv.getTranslatedConstraints()) {
      rpInput.append("\t");
      rpInput.append(cons);
      rpInput.append(",\n");
    }

    rpInput.delete(rpInput.length() - 2, rpInput.length());
    rpInput.append(";\n\n");
    return rpInput.toString();
  }

  private List<Box> processOutput(File rpOutput,
      Map<String, Variable> nameToVar) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(rpOutput));
    String line = reader.readLine();
    List<Box> boxes = new ArrayList<>();

    while (line != null) {
      if (line.contains("OUTER BOX") || line.contains("INNER BOX")) { //solution starts now
        line = reader.readLine();
        Map<Variable, Range<BigRational>> intervals = new HashMap<>();

        while (!line.trim().isEmpty()) {
          String name;
          BigRational hi, lo;
          if (line.contains("=")) {
            String[] groups = line.split("=");
            //          System.out.println(Arrays.toString(groups));
            hi = BigRational.valueOf(groups[1].trim());
            lo = hi;
            name = groups[0].trim();
          } else {
            String[] groups = line.split("\\[|\\]|\\,|in");
            hi = parseRPNumber(groups[3].trim());
            lo = parseRPNumber(groups[2].trim());
            name = groups[0].trim();
          }
          intervals.put(nameToVar.get(name), Range.closed(lo, hi));
          line = reader.readLine();
        }
        boxes.add(new Box(intervals));
      }
      line = reader.readLine();
    }

    return boxes;
  }

  private BigRational parseRPNumber(String number) {
    if (number.contains("e")) {
      return BigRational.valueOf(number);
    } else {
      // BigRational doesn't support parsing numbers like "3.000043"
      boolean isNegative = number.startsWith("-");
      if (isNegative) {
        number = number.substring(1);
      }
      BigDecimal decimal = new BigDecimal(number);
      return BigRational.valueOf(decimal.toEngineeringString()).mul(isNegative ? -1 : 1);
    }
  }
}
