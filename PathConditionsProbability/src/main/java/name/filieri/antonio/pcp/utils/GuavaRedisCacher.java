package name.filieri.antonio.pcp.utils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.Options;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Transaction;

import java.util.Map;


public class GuavaRedisCacher implements Cacher {
    private static final int DEFAULT_CACHE_SIZE = 10000;
    private final Cache<Constraint, Map<Solver, CountResult>> cache;
    private final JedisPool jedisPool;
    private final BiMap<String, Solver> stringToSolver = ImmutableBiMap.of(
            Solver.BARVINOK.toString(), Solver.BARVINOK,
            Solver.QCORAL.toString(), Solver.QCORAL,
            Solver.SHARPSAT.toString(), Solver.SHARPSAT,
            Solver.Z3BLOCKING.toString(), Solver.Z3BLOCKING);


    public GuavaRedisCacher(Options options){
        this(options, DEFAULT_CACHE_SIZE);
    }

    public GuavaRedisCacher(Options options, int size) {
        if (options.isRedisAvailable()) {
            this.jedisPool = new JedisPool(new JedisPoolConfig(), options.getRedisHost(), options.getRedisPort());
        }else{
            this.jedisPool = null;
            System.out.println("Redis connection not available at "+options.getRedisHost()+":"+options.getRedisPort()+". Using Guava cache only.");
        }
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(size)
                .recordStats()
                .build();
    }

    @Override
    public synchronized CountResult getIfPresent(Constraint problem) {
        Map<Solver, CountResult> results = cache.getIfPresent(problem);
        if (results == null) {
            results = loadFromRedis(problem);
        }
        if (results != null) {
            if (results.containsKey(Solver.BARVINOK)) {
                return results.get(Solver.BARVINOK);
            }
            if (results.containsKey(Solver.SHARPSAT)) {
                return results.get(Solver.SHARPSAT);
            }
            if (results.containsKey(Solver.Z3BLOCKING)) {
                return results.get(Solver.Z3BLOCKING);
            }
            if (results.containsKey(Solver.QCORAL)) {
                return results.get(Solver.QCORAL);
            }
            throw new RuntimeException("Unrecognized solver: " + results);
        }
        return null;
    }

    private Map<Solver, CountResult> loadFromRedis(Constraint problem) {
        if(jedisPool==null){
            return null;
        }
        try (Jedis jedis = jedisPool.getResource()) {
            Map<String, String> redisEntries = jedis.hgetAll("" + problem.hashCode()); //TODO: We can use a Long hashcode in the future to reduce collisions
            if (redisEntries != null) {
                Map<Solver, CountResult> results = Maps.newHashMap();
                if(redisEntries.isEmpty()){
                    //No results from Redis
                    return null;
                }
                for (String solver : redisEntries.keySet()) {
                    results.put(stringToSolver.get(solver), CountResultsRedisHelper.fromRedis(redisEntries.get(solver)));
                }
                cache.put(problem, results);
                return ImmutableMap.copyOf(results);
            }
        } catch (Exception e) {
            System.err.println("Redis get threw an exception: "+e.getMessage());
        } //just safety check
        return null;
    }

    @Override
    public synchronized CountResult getIfPresent(Constraint problem, Solver solver) {
        Map<Solver, CountResult> results = cache.getIfPresent(problem);
        if (results == null) {
            results = loadFromRedis(problem);
        }
        if(results!=null){
            return results.get(solver);
        }
        return null;
    }

    @Override
    public synchronized void put(Constraint constraint, CountResult result, Solver solver) {
        putIntoGuava(constraint,result,solver);
        putIntoRedis(constraint,result,solver);
    }

    private void putIntoRedis(Constraint constraint, CountResult result, Solver solver){
        if(jedisPool==null){
            return;
        }
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hset(""+constraint.hashCode(), stringToSolver.inverse().get(solver), CountResultsRedisHelper.toRedis(result));
        }catch(Exception e){
            System.err.println("Redis put threw an exception: "+e.getMessage());
        }
    }

    private void putIntoGuava(Constraint constraint, CountResult result, Solver solver) {
        Map<Solver,CountResult> entry = cache.getIfPresent(constraint);
        if(entry==null){
            entry = Maps.newHashMap();
        }
        entry.put(solver,result);
        cache.put(constraint,entry);
    }

    @Override
    public synchronized void putAll(Map<Constraint, CountResult> results, Cacher.Solver solver) {
        for(Constraint constraint: results.keySet()){
            putIntoGuava(constraint, results.get(constraint), solver);
        }

        if(jedisPool!=null) {
            try (Jedis jedis = jedisPool.getResource()) {
                Transaction t = jedis.multi();
                for (Constraint constraint : results.keySet()) {
                    t.hset("" + constraint.hashCode(), stringToSolver.inverse().get(solver), CountResultsRedisHelper.toRedis(results.get(constraint)));
                }
                t.exec();
            } catch (Exception e) {
                System.err.println("Redis put threw an exception: "+e.getMessage());
            }
        }
    }

    @Override
    public synchronized void invalidateAll() {
        this.cache.invalidateAll();
        //Notice that this does not wipe the Redis db, only the in-memory cache
    }
}
