package name.filieri.antonio.pcp.ast.commands;

public interface CommandVisitor<T> {

	T visit(SetOptionCommand setOptionCommand);

	T visit(AssertCommand assertCommand);

	T visit(DeclareConstantCommand declareConstantCommand);

	T visit(ClearCommand clearCommand);

	T visit(VariableDeclarationCommand variableDeclarationCommand);

	T visit(CountCommand countCommand);

	T visit(DependentVariableDeclarationCommand dependentVariableDeclarationCommand);

  T visit(PavingCommand pavingCommand);

  T visit(ExitCommand exitCommand);

  default T visit(Command cmd) {
    if (cmd instanceof SetOptionCommand) {
      SetOptionCommand setOption = (SetOptionCommand) cmd;
      return visit(setOption);
    } else if (cmd instanceof AssertCommand) {
      AssertCommand assertCommand = (AssertCommand) cmd;
      return visit(assertCommand);
    } else if (cmd instanceof DeclareConstantCommand) {
      DeclareConstantCommand declareConstantCommand = (DeclareConstantCommand) cmd;
      return visit(declareConstantCommand);
    } else if (cmd instanceof ClearCommand) {
      ClearCommand clearCommand = (ClearCommand) cmd;
      return visit(clearCommand);
    } else if (cmd instanceof VariableDeclarationCommand) {
      VariableDeclarationCommand variableDeclarationCommand = (VariableDeclarationCommand) cmd;
      return visit(variableDeclarationCommand);
    } else if (cmd instanceof CountCommand) {
      CountCommand countCommand = (CountCommand) cmd;
      return visit(countCommand);
    } else if (cmd instanceof DependentVariableDeclarationCommand) {
      DependentVariableDeclarationCommand depVDC = (DependentVariableDeclarationCommand) cmd;
      return visit(depVDC);
    } else if (cmd instanceof PavingCommand) {
      PavingCommand pvCmd = (PavingCommand) cmd;
      return visit(pvCmd);
    } else if (cmd instanceof ExitCommand) {
      ExitCommand exCmd = (ExitCommand) cmd;
      return visit(exCmd);
    } else {
      throw new RuntimeException("Not implemented: " + cmd);
    }
  }
}
