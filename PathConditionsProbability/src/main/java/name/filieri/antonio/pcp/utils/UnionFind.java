package name.filieri.antonio.pcp.utils;

//Quick union find. Theory: http://algs4.cs.princeton.edu/15uf/


import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

public class UnionFind<T> {

  private Map<T, T> parent;
  private Map<T, Integer> rank;
  private int count;


  public UnionFind() {
    this.count = 0;
    this.parent = Maps.newHashMap();
    this.rank = Maps.newHashMap();
  }

  public void addNode(T n) {
    if(parent.containsKey(n)){
      return;
    }
    parent.put(n, n);
    rank.put(n, 0);
    this.count++;
  }

  // TODO: Can be better?
  public Set<Set<T>> getClusters() {
    Multimap<T, T> parentToChildren = HashMultimap.create();

    for (T par : parent.keySet()) {
      parentToChildren.put(find(par), par);
    }
    Set<Set<T>> result = Sets.newHashSet();
    for (T p : parentToChildren.keySet()) {
      result.add(Sets.newHashSet(parentToChildren.get(p)));
    }
    return result;
  }

  public T find(T p) {
    Preconditions.checkArgument(parent.containsKey(p));
    while (p != parent.get(p)) {
      parent.put(p, parent.get(parent.get(p)));
      p = parent.get(p);
    }
    return p;
  }

  public int count() {
    return count;
  }

  public boolean connected(T p, T q) {
    Preconditions.checkArgument(parent.containsKey(p), "Node "+p+" not in the set");
    Preconditions.checkArgument(parent.containsKey(p), "Node "+q+" not in the set");
    return find(p).equals(find(q)) || find(p) == find(q);
  }

  public void union(T p, T q) {
    addNode(p);
    addNode(q);
    if(p.equals(q) || p==q){
      return;
    }
    T rootP = find(p);
    T rootQ = find(q);
    if (rootP.equals(rootQ) || rootP==rootQ) {
      return;
    }

    // make root of smaller rank point to root of larger rank
    if (rank.get(rootP) < rank.get(rootQ)) {
      parent.put(rootP, rootQ);
    } else if (rank.get(rootP) > rank.get(rootQ)) {
      parent.put(rootQ, rootP);
    } else {
      parent.put(rootQ, rootP);
      rank.put(rootP, rank.get(rootP) + 1);
    }
    count--;
  }

  public static void main(String[] args) {

    UnionFind<Integer> uf = new UnionFind<Integer>();

    System.out.println(uf.count());
    System.out.println(uf.getClusters());

    IntStream.range(0, 5).forEach(n -> uf.addNode(n));

    uf.union(2, 3);
    uf.union(2, 2);
    uf.union(3, 2);
    uf.union(0, 4);
    uf.union(2, 4);

    System.out.println(uf.count());
    System.out.println(uf.getClusters());


    uf.addNode(5);
    uf.addNode(6);
    uf.addNode(7);

    uf.union(7,2);


    System.out.println(uf.count());
    System.out.println(uf.getClusters());
  }
}