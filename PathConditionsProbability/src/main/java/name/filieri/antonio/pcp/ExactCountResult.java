package name.filieri.antonio.pcp;

import com.google.common.hash.Hashing;
import name.filieri.antonio.jpf.utils.BigRational;

public class ExactCountResult implements CountResult {

  public static final ExactCountResult ZERO = new ExactCountResult(BigRational.ZERO);

  private final BigRational probability;
  private Integer hashCode = null;

  public ExactCountResult(BigRational probability) {
    this.probability = probability;
  }

  public ExactCountResult(double probability) {
    this.probability = BigRational.valueOf(probability);
  }

  @Override
  public BigRational expectedProbability() {
    return probability;
  }

  @Override
  public boolean equals(CountResult cr) {
    if (cr instanceof ExactCountResult) {
      return probability.equals(((ExactCountResult) cr).probability);
    }
    return false;
  }

  @Override
  public int hashcode() {
    if (this.hashCode == null) {
      this.hashCode = Hashing.goodFastHash(512)
          .newHasher()
          .putInt(this.getClass().hashCode())
          .putInt(probability.hashCode())
          .hash().asInt();
    }
    return this.hashCode;
  }

  @Override
  public String toString() {
    return "exact:       " + probability;
  }
}
