package name.filieri.antonio.pcp.ast.expr.bitvector;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BinaryExpression;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;

/**
 * Created by mateus on 21/03/17.
 */
public class BitVectorCompareExpression implements BinaryExpression, BooleanExpression {

  private final PositionInfo posInfo;
  private final Expression left;
  private final Expression right;
  private final Function function;
  private HashCode hashCode;
  public BitVectorCompareExpression(Expression left, Expression right,
      Function function, PositionInfo positionInfo) {
    this.left = left;
    this.right = right;
    this.function = function;
    this.posInfo = positionInfo;
  }

  public Function getFunction() {
    return function;
  }

  @Override
  public Expression getLeft() {
    return left;
  }

  @Override
  public Expression getRight() {
    return right;
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public HashCode getHashCode() {
    if (hashCode == null) {
      Hasher hasher = ASTUtil.getHashFunction()
          .newHasher()
          .putString(function.name(), StandardCharsets.UTF_8);
      hashCode = Hashing.combineOrdered(
          ImmutableList.of(left.getHashCode(), right.getHashCode(), hasher.hash()));
    }
    return hashCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BitVectorCompareExpression that = (BitVectorCompareExpression) o;

    if (!left.equals(that.left)) {
      return false;
    }
    if (!right.equals(that.right)) {
      return false;
    }
    return function == that.function;
  }

  @Override
  public int hashCode() {
   return getHashCode().asInt();
  }

  @Override

  public PositionInfo getPositionInfo() {
    return posInfo;
  }

  @Override
  public BitVectorCompareExpression negate() {
    return new BitVectorCompareExpression(left,right,function.negate(),posInfo);
  }

  public enum Function {
    //BVNCOMP doesn't exist in the standard; we use to simplify negations
    BVULT, BVULE, BVUGT, BVUGE, BVSLT, BVSLE, BVSGT, BVSGE, BVCOMP, BVNCOMP;

    public static Optional<Function> fromSymbol(String symbol) {
      try {
        Function fun = Function.valueOf(symbol);
        return Optional.of(fun);
      } catch (IllegalArgumentException e) {
        return Optional.empty();
      }
    }

    public Function negate() {
      switch (this) {
        case BVULT:
          return BVUGE;
        case BVULE:
          return BVUGT;
        case BVUGT:
          return BVULE;
        case BVUGE:
          return BVULT;
        case BVSLT:
          return BVSGE;
        case BVSLE:
          return BVSGT;
        case BVSGT:
          return BVSLE;
        case BVSGE:
          return BVSLT;
        case BVCOMP:
          return BVNCOMP;
        case BVNCOMP:
          return BVCOMP;
        default:
          throw new RuntimeException("Unknown case: " + this);
      }
    }
  }

  @Override
  public String toString() {
    return getFunction().toString() + "(" + getLeft().toString() + "," + getRight().toString() + ")";
  }
}
