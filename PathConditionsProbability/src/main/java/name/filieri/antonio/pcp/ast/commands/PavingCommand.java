package name.filieri.antonio.pcp.ast.commands;

import com.google.common.hash.HashCode;
import java.nio.charset.StandardCharsets;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;

public class PavingCommand implements Command {

  private final static HashCode hash = ASTUtil.getHashFunction().newHasher()
      .putString("(count)", StandardCharsets.UTF_8)
      .hash();
  private PositionInfo position;

  public PavingCommand(PositionInfo position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PavingCommand that = (PavingCommand) o;
    return true;
  }

  @Override
  public int hashCode() {
    return position != null ? position.hashCode() : 0;
  }

  @Override
  public HashCode getHashCode() {
    return hash;
  }

  @Override
  public PositionInfo getPositionInfo() {
    return position;
  }

  @Override
  public CommandType getCommandType() {
    return CommandType.PAVING;
  }

  @Override
  public <T> T accept(CommandVisitor<T> visitor) {
    return visitor.visit(this);
  }
}
