package name.filieri.antonio.pcp;

import name.filieri.antonio.jpf.utils.BigRational;

public interface CountResult {

  BigRational expectedProbability();

  boolean equals(CountResult cr);
  int hashcode();

}
