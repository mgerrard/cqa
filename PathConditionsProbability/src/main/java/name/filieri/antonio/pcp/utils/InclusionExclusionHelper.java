package name.filieri.antonio.pcp.utils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.ConstraintPartitioner;
import name.filieri.antonio.pcp.CountDispatcher;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.ExactCountResult;
import name.filieri.antonio.pcp.ExpressionResult;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.visitors.ExpressionEvaluator;

public class InclusionExclusionHelper {

  private final List<Constraint> disjuncts;
  private final CountDispatcher dispatcher;

  public InclusionExclusionHelper(List<Constraint> disjuncts, CountDispatcher dispatcher) {
    this.disjuncts = ImmutableList.copyOf(disjuncts);
    this.dispatcher = dispatcher;
  }

  public InclusionExclusionCounts count() {
    InclusionExclusionCounts result = new InclusionExclusionCounts();
    int level = 1;
    Deque<Node> queue = new ArrayDeque<>();
    // Loading level 1, i.e., all the single conjuncts
    IntStream.range(0, disjuncts.size()).forEach(i -> {
      Node node = new Node(level, i, disjuncts.get(i), disjuncts.get(i).getVariables());
      queue.add(node);
    });

    while (!queue.isEmpty()) {
      Node current = queue.pop();
      CountResult count = countConstraint(current.constraint);
      if (current.level % 2 == 1) {
        result.addCounts.add(count);
      } else {
        result.subCounts.add(count);
      }
      //TODO: this has to be refined when using approximate counts because prob=0 may not mean unsat
      current.sat = !count.expectedProbability().isZero();

      if (current.sat && current.positionInTheList < disjuncts.size() - 1) {
        //expand to the next level
        IntStream.range(current.positionInTheList + 1, disjuncts.size()).forEach(i -> {
          BooleanNaryExpression conjunctionExpression = new BooleanNaryExpression(Function.AND,
              Lists.newArrayList(current.constraint.getExpr(), disjuncts.get(i).getExpr()));
          Constraint conjunction = new Constraint(conjunctionExpression);

          Node node = new Node(current.level + 1, i, conjunction, disjuncts.get(i).getVariables());
          queue.add(node);
        });
      }
    }

    return result;
  }

  private CountResult countConstraint(Constraint constraint) {
    if (constraint.getVariables().size() == 0) {
      //no variables, return 1 if constraint is true, 0 otherwise
      ExpressionResult result = constraint.getExpr().accept(new ExpressionEvaluator());
      if (result.type != ExpressionType.BOOLEAN) {
        throw new RuntimeException("Expected a boolean expression");
      }
      if (result.numericResult.intValue() == 0) { //false
        return new ExactCountResult(0);
      } else {
        return new ExactCountResult(1);
      }
    }

    ConstraintPartitioner partitioner = new ConstraintPartitioner();
    partitioner.computeVarClusters(Sets.newHashSet(constraint));
    List<Constraint> independentClauses = partitioner.partition(constraint);

    Multimap<Constraint, Constraint> constraintConstraintMultimap = HashMultimap.create();
    constraintConstraintMultimap.putAll(constraint,independentClauses);

    Map<Constraint, CountResult> resultMap = dispatcher.count(constraintConstraintMultimap);

      List<CountResult> partitionResults = new ArrayList<>();

      for (Constraint partition : independentClauses) {
        partitionResults.add(resultMap.get(partition));
      }
      //CountResult mergedResult = partitioner.mergePartitionResults(partitionResults);


    //TODO: when dispatcher becomes thread-safe, this can become a parallel stream
    //List<CountResult> partitionResults = independentClauses.stream().map(c -> dispatcher.count(c))
    //    .collect(Collectors.toList());

    CountResult mergedresult = partitioner.mergePartitionResults(partitionResults);
    return mergedresult;
  }


  public static class InclusionExclusionCounts {

    List<CountResult> addCounts;
    List<CountResult> subCounts;

    public InclusionExclusionCounts() {
      addCounts = Lists.newArrayList();
      subCounts = Lists.newArrayList();
    }

    public List<CountResult> getAddCounts() {
      return addCounts;
    }

    public List<CountResult> getSubCounts() {
      return subCounts;
    }
  }

  private class Node {

    final int level;
    final int positionInTheList;
    final Constraint constraint;
    final Set<Variable> nodeVariables;

    boolean sat;
    List<Node> children;

    public Node(int level, int positionInTheList, Constraint constraint,
        Set<Variable> nodeVariables) {
      this.level = level;
      this.positionInTheList = positionInTheList;
      this.constraint = constraint;
      this.nodeVariables = nodeVariables;
    }
  }

}
