package name.filieri.antonio.pcp.ast.commands;

import com.google.common.hash.HashCode;
import java.nio.charset.StandardCharsets;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;

public class ExitCommand implements Command {

  public static final HashCode hash = ASTUtil.getHashFunction().newHasher()
      .putString("(exit)", StandardCharsets.UTF_8).hash();
  public final PositionInfo position;

  public ExitCommand(PositionInfo positionInfo) {
    position = positionInfo;
  }

  @Override
  public CommandType getCommandType() {
    return CommandType.EXIT;
  }

  @Override
  public <T> T accept(CommandVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public HashCode getHashCode() {
    return hash;
  }

  @Override
  public PositionInfo getPositionInfo() {
    return position;
  }

  @Override
  public int hashCode() {
    return getHashCode().asInt();
  }
}
