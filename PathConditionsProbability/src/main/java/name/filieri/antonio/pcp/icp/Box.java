package name.filieri.antonio.pcp.icp;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Range;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.expr.Variable;

public class Box {

  private final ImmutableMap<Variable, Range<BigRational>> intervals;

  public Box(Map<Variable, Range<BigRational>> intervals) {
    this.intervals = ImmutableMap.copyOf(intervals);
  }

  public Map<Variable, Range<BigRational>> getIntervals() {
    return intervals;
  }

  public String toPcpString(boolean printFloat) {
    return intervals.entrySet().stream()
        .sorted(Comparator.comparing(e -> e.getKey().getName()))
        .map(entry -> {
          BigRational lo = entry.getValue().lowerEndpoint();
          BigRational hi = entry.getValue().upperEndpoint();
          return "(" + entry.getKey().getName() + " "
              + (printFloat ? lo.doubleValue() : lo.toString()) + " "
              + (printFloat ? hi.doubleValue() : hi.toString()) + ")";
        })
        .collect(Collectors.joining(" "));
  }
}
