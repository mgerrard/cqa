package name.filieri.antonio.pcp.utils;

import com.google.common.base.Preconditions;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.ExactCountResult;
import name.filieri.antonio.pcp.StatisticalCountResult;

import java.util.Arrays;
import java.util.Map;

public class CountResultsRedisHelper {
    private static final String EXACT = "EXACT";
    private static final String STATISTICAL = "STAT";


    public static String toRedis(ExactCountResult exactCount){
        return EXACT+"#"+exactCount.expectedProbability();
    }

    public static String toRedis(StatisticalCountResult statisticalCountResult){
        return STATISTICAL+"#"+statisticalCountResult.expectedProbability()+"#"+statisticalCountResult.getVariance()+"#"+statisticalCountResult.getNumSamples();
    }

    public static String toRedis(CountResult countResult){
        if(countResult instanceof ExactCountResult){
            return toRedis((ExactCountResult) countResult);
        }else if(countResult instanceof StatisticalCountResult){
            return toRedis((StatisticalCountResult) countResult);
        }else {
            throw new RuntimeException("Invalid CountResult type: " + countResult.getClass().toString());
        }
    }

    public static CountResult fromRedis(String resultRepresentation){
        Preconditions.checkNotNull(resultRepresentation);
        Preconditions.checkArgument(resultRepresentation.contains("#"));
        String[] parts = resultRepresentation.split("#");
        if(parts[0].equals(EXACT)){
            BigRational expectedProbability = BigRational.valueOf(parts[1]);
            return new ExactCountResult(expectedProbability);
        }else if(parts[0].equals(STATISTICAL)){
            BigRational expectedProbability = BigRational.valueOf(parts[1]);
            BigRational variance = BigRational.valueOf(parts[2]);
            long numSamples = Long.parseLong(parts[3]);
            return new StatisticalCountResult(expectedProbability,variance,numSamples);
        }else{
            throw new RuntimeException("Malformed Redis result");
        }
    }

}
