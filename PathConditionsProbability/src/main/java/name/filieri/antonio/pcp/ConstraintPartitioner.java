package name.filieri.antonio.pcp;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.utils.UnionFind;
import name.filieri.antonio.pcp.visitors.VariableCollector;

public class ConstraintPartitioner implements Partitioner {

  private final Set<Set<Variable>> varClusters;
  private final Multimap<Constraint, Constraint> sourceToPartitions;

  public ConstraintPartitioner() {
    varClusters = new LinkedHashSet<>();
    sourceToPartitions = LinkedHashMultimap.create();
  }

  @Override
  public void computeVarClusters(Set<Constraint> constraints) {
    if (!varClusters.isEmpty()) {
      System.out.println("[pcp:partitioner] varCluster set is not empty!");
    }

    HashMultimap<Variable, Constraint> constraintsRelatedToAVar = HashMultimap.create();

    // TODO measure cost
    Set<Constraint> explodedConstraints = new LinkedHashSet<>();
    for (Constraint constraint : constraints) {
      BooleanExpression expr = constraint.getExpr();
      if (expr instanceof BooleanNaryExpression) {
        BooleanNaryExpression bexpr = (BooleanNaryExpression) expr;
        if (bexpr.getFunction() == Function.AND) {
          for (BooleanExpression clause : bexpr.getBooleanArgs()) {
            explodedConstraints.add(new Constraint(clause));
          }
        } else {
          System.out.println(
              "[ConstraintPartitioner] Disjunction detected while computing dependencies between variables.");
          explodedConstraints.add(constraint);
        }
      }
    }

    UnionFind<Variable> variableDependency = new UnionFind<>();

    for (Constraint constraint : explodedConstraints) {
      List<Variable> vars = new ArrayList<>(constraint.getVariables());
      for (int i = 0; i < vars.size(); i++) {
        constraintsRelatedToAVar.put(vars.get(i), constraint);
        //Starting from j=i covers the case of a single variable
        for (int j = i; j < vars.size(); j++) {
          variableDependency.union(vars.get(i), vars.get(j));
        }
      }
    }

    Set<Set<Variable>> clusters = variableDependency.getClusters();
    varClusters.addAll(clusters);
  }


  @Override
  public List<Constraint> partition(Constraint constraint) {
    List<Constraint> partitions = new ArrayList<>();
    for (Set<Variable> cluster : varClusters) {
      Optional<Constraint> partitionOption = extractPartition(constraint, cluster);
      if (partitionOption.isPresent()) {
        partitions.add(partitionOption.get());
      } else { // constraint don't contain any of the vars in the cluster
        continue;
      }
    }
    return partitions;
  }

  private Optional<Constraint> extractPartition(Constraint constraint, Set<Variable> cluster) {
    BooleanExpression expr = constraint.getExpr();
    Optional<Constraint> result;
    if (expr instanceof BooleanNaryExpression) {
      BooleanNaryExpression naryExpr = (BooleanNaryExpression) expr;
      Set<BooleanExpression> relatedExprs = new LinkedHashSet<>();
      Multimap<Variable, BooleanExpression> locallyRelatedConstraints = LinkedHashMultimap.create();

      for (BooleanExpression argExpr : naryExpr.getBooleanArgs()) {
        Set<Variable> argVars = VariableCollector.collectVariables(argExpr);
        for (Variable var : argVars) {
          locallyRelatedConstraints.put(var, argExpr);
        }
      }

      for (Variable var : cluster) {
        relatedExprs.addAll(locallyRelatedConstraints.get(var));
      }
      if (relatedExprs.isEmpty()) {
        result = Optional.absent();
      } else {
        BooleanNaryExpression partition = new BooleanNaryExpression(naryExpr.getFunction(),
            ImmutableList.copyOf(relatedExprs));
        result = Optional.of(new Constraint(partition));
      }
    } else { // only one clause
      Set<Variable> exprVars = constraint.getVariables();
      if (Sets.intersection(cluster, exprVars).isEmpty()) {
        result = Optional.absent();
      } else {
        result = Optional.of(constraint);
      }
    }
    return result;
  }

  @Override
  public Multimap<Constraint, Constraint> partition(Collection<Constraint> constraints) {
    for (Constraint constraint : constraints) {
      if (sourceToPartitions.containsKey(constraint)) {
        // do nothing for now?
      } else {
        List<Constraint> partitions = partition(constraint);
        sourceToPartitions.putAll(constraint, partitions);
      }
    }
    return ImmutableMultimap.copyOf(sourceToPartitions);
  }

  /**
   * Use eq. 5 (Handling Conjunction) to merge partial results.
   */
  //TODO: update when integrating ApproxMC to realize how it can be improved (and if it can be improved)
  @Override
  public CountResult mergePartitionResults(Collection<CountResult> partialResults) {
    Preconditions.checkArgument(!partialResults.isEmpty() && !partialResults.contains(null));
    BigRational prob = null;
    double var = 0;
    long nSamples = 0;

    for (CountResult partial : partialResults) {
      BigRational partialProb = partial.expectedProbability();
      double partialVar =
          (partial instanceof StatisticalCountResult) ? ((StatisticalCountResult) partial)
              .getVariance().doubleValue() : 0;
      long partialNSamples =
          (partial instanceof StatisticalCountResult) ? ((StatisticalCountResult) partial)
              .getNumSamples() : 0;

      if (prob == null) {
        prob = partialProb;
        var = partialVar;
        nSamples = partialNSamples;
      } else {
        prob = prob.mul(partialProb);
        var = prob.pow(2).doubleValue() * partialVar
            + partialProb.pow(2).doubleValue() * var
            + var * partialVar;
        nSamples = nSamples + partialNSamples;
      }
    }
    return (var == 0) ? new ExactCountResult(prob) : new StatisticalCountResult(prob, var, nSamples);
  }

  @Override
  public void clear() {
    varClusters.clear();
    sourceToPartitions.clear();
  }
}