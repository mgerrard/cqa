package name.filieri.antonio.pcp.errors;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class StorageErrorListener extends BaseErrorListener {

  public final List<ErrorMessage> messages;

  public StorageErrorListener() {
    this.messages = new ArrayList<>();
  }

  public static class ErrorMessage {

    public final int line;
    public final int pos;
    public final String msg;

    public ErrorMessage(int line, int pos, String msg) {
      this.line = line;
      this.pos = pos;
      this.msg = msg;
    }

    public String toString() {
      String result = "line " + line + ":" + pos + " " + msg;
      return result.replaceAll("\"","''");
    }
  }

  @Override
  public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
      int charPositionInLine, String msg, RecognitionException e) {
    messages.add(new ErrorMessage(line, charPositionInLine, msg));
  }
}
