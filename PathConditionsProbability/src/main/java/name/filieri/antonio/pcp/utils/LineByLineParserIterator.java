package name.filieri.antonio.pcp.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class LineByLineParserIterator implements Iterator<List<Command>> {

  //updating the symbol table is the client's responsibility
  private final SymbolTable symtable;
  private final BufferedReader reader;
  private String next;

  public LineByLineParserIterator(SymbolTable symtable, BufferedReader reader) {
    this.symtable = symtable;
    this.reader = reader;
    try {
      next = reader.readLine();
    } catch (IOException e) {
      next = null;
    }
  }

  @Override
  public boolean hasNext() {
    return next != null;
  }

  @Override
  public List<Command> next() {
    String current = next;
    do {
      try {
        next = reader.readLine();
      } catch (IOException e) {
        next = null;
      }
    } while (next != null && next.trim().isEmpty()); //skip empty lines

    if (current == null) {
      return Collections.emptyList();
    }

    ANTLRInputStream input = new ANTLRInputStream(current);
    InputFormatLexer lexer = new InputFormatLexer(input);
    InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

    ParseTree tree = parser.input();
    return AstBuilder.parseCommands(symtable, tree);
  }
}
