package name.filieri.antonio.pcp.simplifier;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import name.filieri.antonio.pcp.Options;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.simplifier.MiniSMTLIBParser.ParserException;
import name.filieri.antonio.pcp.visitors.SMTLIBTranslator;
import name.filieri.antonio.pcp.visitors.VariableCollector;

public class SMTLIBSimplifier implements Simplifier {

  private static final String wrapperFunction = "toSimplify";
  private final String smtlibSolver;

  public SMTLIBSimplifier(Options options) {
    //TODO find out if other smtlib solvers support simplification
    this.smtlibSolver = options.getZ3Path();
  }

  @Override
  public BooleanExpression simplify(BooleanExpression formula) {

    SMTLIBTranslator smtTranslator = new SMTLIBTranslator();
    formula.accept(smtTranslator);

    try {
      Path inputFile = Files.createTempFile("z3-simplifier", ".smt2");
      Path outputFile = Files.createTempFile("z3-simplified", ".smt2");

      try (BufferedWriter writer = new BufferedWriter(new FileWriter(inputFile.toFile()))) {
        writer.write(smtTranslator.getVariableDeclarations());
        writer.write("\n");
        writer.write(smtTranslator.getTranslatedFunction(wrapperFunction));
        writer.write("(assert " + wrapperFunction + ")\n");
        writer.write("(apply (then simplify (then propagate-values ctx-solver-simplify)))\n");
      }

      System.out.println("[smtlib-simplifier] input: " + inputFile);
      System.out.println("[smtlib-simplifier] output: " + outputFile);

      ProcessBuilder pb = new ProcessBuilder();
      pb.command(smtlibSolver, "-smt2", inputFile.toString());
      pb.redirectOutput(outputFile.toFile());

      Process process = pb.start();

      while (process.isAlive()) {
        try {
          process.waitFor();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      Set<Variable> vars = VariableCollector.collectVariables(formula);
      MiniSMTLIBParser parser = new MiniSMTLIBParser(outputFile.toFile(), vars.stream()
          .collect(Collectors.toMap(Variable::getName, Function.identity())));
      BooleanExpression simplified = parser.parseGoals();

      inputFile.toFile().delete();
      outputFile.toFile().delete();

      System.out.println("[smtlib-simplifier] final constraint: " + simplified);
      return simplified;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("IO error while simplifying:", e);
    } catch (ParserException e) {
      System.out.println(
          "[smt-simplifier] something went wrong while parsing z3 output: " + e.getMessage());
      throw new RuntimeException(e);
    }
  }
}
