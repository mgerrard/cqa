package name.filieri.antonio.pcp.icp;

import java.util.ArrayList;
import java.util.List;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;
import name.filieri.antonio.pcp.visitors.ExpressionBaseVisitor;

public class RealPaverVisitor extends ExpressionBaseVisitor<Void> {

  private final List<String> translatedConstraints;
  private boolean firstConjunction = true;
  private StringBuilder sb;

  /**
   * Precondition: No nested boolean operations, no disjunctions, no bitvectors.
   */

  public RealPaverVisitor() {
    this.translatedConstraints = new ArrayList<>();
  }

  public List<String> getTranslatedConstraints() {
    return translatedConstraints;
  }

  @Override
  public Void emptyResult() {
    return null;
  }

  @Override
  public Void mergeResults(List<Void> childResults) {
    return null;
  }

  @Override
  public Void visit(BooleanConstant booleanConstant) {
    throw new UnsupportedOperationException("RealPaver doesn't support boolean constants");
  }

  @Override
  public Void visit(BooleanNaryExpression booleanNaryExpression) {
    if (booleanNaryExpression.getFunction() == Function.OR) {
      throw new UnsupportedOperationException(
          "RealPaver doesn't support disjunctions");
    }

    if (firstConjunction) {
      firstConjunction = false;
    } else {
      throw new UnsupportedOperationException("Nested conjunctions aren't supported");
    }

    for (BooleanExpression bexpr : booleanNaryExpression.getBooleanArgs()) {
      try {
        sb = new StringBuilder();
        bexpr.accept(this);
        translatedConstraints.add(sb.toString());
      } catch (UnsupportedOperationException e) {
        System.out.println(
            "[realpaver-visitor] Dropping unsupported clause in constraint: " + e.getMessage());
      }
    }

    return emptyResult();
  }

  @Override
  public Void visit(NotExpression notExpression) {
    throw new UnsupportedOperationException("Negation is not allowed.");
  }

  @Override
  public Void visit(NumericBinaryExpression numericBinaryExpression) {
    NumericBinaryExpression.Function op = numericBinaryExpression.getFunction();
    Expression left = numericBinaryExpression.getLeft();
    Expression right = numericBinaryExpression.getRight();

    if (op == NumericBinaryExpression.Function.ATAN2
        || op == NumericBinaryExpression.Function.MOD) {
      throw new UnsupportedOperationException(op + " is not suported by RealPaver");
    } else if (op == NumericBinaryExpression.Function.POW) {
      boolean isNaturalExponent = (right instanceof NumericConstant)
          && ((NumericConstant) right).getType().isInteger();
      if (!isNaturalExponent) {
        throw new UnsupportedOperationException("exponents must be natural constants");
      }
    }

    if (op == NumericBinaryExpression.Function.MAX
        || op == NumericBinaryExpression.Function.MIN) {
      sb.append(numericBinaryExpression.getFunction().toSymbol());
      sb.append('(');
      left.accept(this);
      sb.append(',');
      right.accept(this);
      sb.append(')');
    } else {
      sb.append('(');
      left.accept(this);
      sb.append(numericBinaryExpression.getFunction().toSymbol());
      right.accept(this);
      sb.append(')');
    }
    return emptyResult();
  }

  @Override
  public Void visit(NumericComparisonExpression numericComparisonExpression) {
    numericComparisonExpression.getLeft().accept(this);
    sb.append(toSymbol(numericComparisonExpression.getFunction()));
    numericComparisonExpression.getRight().accept(this);
    return emptyResult();
  }

  private String toSymbol(NumericComparisonExpression.Function function) {
    switch (function) {
      case EQ:
        return "=";
      case GE:
      case GT:
        return ">=";
      case LE:
      case LT:
        return "<=";
      case NE:
        throw new UnsupportedOperationException("RealPaver doesn't support !=");
      default:
        throw new RuntimeException("Unknown case: " + this);
    }
  }

  @Override
  public Void visit(NumericUnaryExpression numericUnaryExpression) {
    NumericUnaryExpression.Function op = numericUnaryExpression.getFunction();
    if (op == NumericUnaryExpression.Function.LOG10) {
      throw new UnsupportedOperationException("RealPaver doesn't support log10");
    }

    sb.append(op.toString().toLowerCase());
    sb.append('(');
    numericUnaryExpression.getArg().accept(this);
    sb.append(')');

    return emptyResult();
  }

  @Override
  public Void visit(NumericConstant numericConstant) {
    sb.append(numericConstant.toString());
    return emptyResult();
  }

  @Override
  public Void visit(StringConstant stringConstant) {
    throw new UnsupportedOperationException("RealPaver doesn't support strings");
  }

  @Override
  public Void visit(NormalVariable normalVariable) {
    sb.append(normalVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(BernoulliVariable bernoulliVariable) {
    sb.append(bernoulliVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(UniformFloatVariable uniformFloatVariable) {
    sb.append(uniformFloatVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(UniformDoubleVariable uniformDoubleVariable) {
    sb.append(uniformDoubleVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(UniformIntegerVariable uniformIntegerVariable) {
    sb.append(uniformIntegerVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(UniformLongVariable uniformLongVariable) {
    sb.append(uniformLongVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(StringVariable stringVariable) {
    throw new UnsupportedOperationException("RealPaver doesn't support strings");
  }

  @Override
  public Void visit(StringBinaryExpression stringBinaryExpression) {
    throw new UnsupportedOperationException("RealPaver doesn't support strings");
  }

  @Override
  public Void visit(DependentVariable dependentVariable) {
    sb.append(dependentVariable.getName());
    return emptyResult();
  }

  @Override
  public Void visit(BitVectorConstant bitVectorConstant) {
    throw new UnsupportedOperationException("RealPaver doesn't support bitvectors");
  }

  @Override
  public Void visit(BitVectorBinaryExpression bitVectorBinaryExpression) {
    throw new UnsupportedOperationException("RealPaver doesn't support bitvectors");
  }

  @Override
  public Void visit(BitVectorExtractExpression bitVectorExtractExpression) {
    throw new UnsupportedOperationException("RealPaver doesn't support bitvectors");
  }

  @Override
  public Void visit(BitVectorCompareExpression bitVectorCompareExpression) {
    throw new UnsupportedOperationException("RealPaver doesn't support bitvectors");
  }

  @Override
  public Void visit(BitVectorUnaryExpression bitVectorUnaryExpression) {
    throw new UnsupportedOperationException("RealPaver doesn't support bitvectors");
  }

  @Override
  public Void visit(BitVectorVariable bitVectorVariable) {
    throw new UnsupportedOperationException("RealPaver doesn't support bitvectors");
  }
}
