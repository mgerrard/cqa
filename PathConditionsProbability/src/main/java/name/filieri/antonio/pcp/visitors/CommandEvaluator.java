package name.filieri.antonio.pcp.visitors;

import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountDispatcher;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.ExactCountResult;
import name.filieri.antonio.pcp.ExpressionResult;
import name.filieri.antonio.pcp.Options;
import name.filieri.antonio.pcp.Partitioner;
import name.filieri.antonio.pcp.StatisticalCountResult;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.ClearCommand;
import name.filieri.antonio.pcp.ast.commands.CommandVisitor;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.ast.commands.DeclareConstantCommand;
import name.filieri.antonio.pcp.ast.commands.DependentVariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.commands.ExitCommand;
import name.filieri.antonio.pcp.ast.commands.PavingCommand;
import name.filieri.antonio.pcp.ast.commands.SetOptionCommand;
import name.filieri.antonio.pcp.ast.commands.VariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.icp.Box;
import name.filieri.antonio.pcp.icp.Paver;
import name.filieri.antonio.pcp.icp.RealPaverCaller;
import name.filieri.antonio.pcp.simplifier.SMTLIBSimplifier;
import name.filieri.antonio.pcp.simplifier.Simplifier;
import name.filieri.antonio.pcp.simplifier.Z3Simplifier;
import name.filieri.antonio.pcp.utils.ExitRequestException;
import name.filieri.antonio.pcp.utils.InclusionExclusionHelper;
import name.filieri.antonio.pcp.utils.InclusionExclusionHelper.InclusionExclusionCounts;
import name.filieri.antonio.pcp.visitors.AstBuilder.ExpressionVisitor;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;

public class CommandEvaluator implements CommandVisitor<Reply> {

  public static class Reply {

    public final boolean error;
    public final CountResult result;
    public final String message;

    public Reply(boolean error, CountResult result, String message) {
      super();
      this.error = error;
      this.result = result;
      this.message = message;
    }

    @Override
    public String toString() {
      return "Reply [error=" + error + ", result=" + result + ", message=" + message + "]";
    }

    public static final Reply OK = new Reply(false, ExactCountResult.ZERO, "ok");
  }

  private final SymbolTable stable;
  private final Set<Constraint> constraints;
  private final Partitioner partitioner;
  private final CountDispatcher dispatcher;
  private final Options options;

  public CommandEvaluator(SymbolTable stable, Partitioner partitioner, CountDispatcher dispatcher,
      Options options) {
    this.constraints = new LinkedHashSet<>();
    this.stable = stable;
    this.partitioner = partitioner;
    this.dispatcher = dispatcher;
    this.options = options;
  }

  @Override
  public Reply visit(SetOptionCommand cmd) {
    String key = cmd.getName();
    ExpressionResult val = cmd.getValue();
    if (val.type == ExpressionType.STRING) {
      options.processOption(key, val.stringResult);
    } else if (val.type == ExpressionType.BOOLEAN) {
      options.processOption(key, val.numericResult.intValue() == 1 ? "true" : "false");
    } else {
      options.processOption(key, val.numericResult.toString());
    }
    return Reply.OK;
  }

  @Override
  public Reply visit(AssertCommand cmd) {
    List<BooleanExpression> clauses = cmd.getExprs();
    BooleanNaryExpression conjunction = new BooleanNaryExpression(Function.AND, clauses,
        cmd.getPositionInfo());
    Constraint constraint = new Constraint(conjunction);
    //Do not resolve DNF here
    constraints.add(constraint);

    return Reply.OK;
  }

  @Override
  public Reply visit(DeclareConstantCommand cmd) {
    String name = cmd.getName();
    Reply reply;
    Optional<Expression> optExpr = stable.query(name);
    if (optExpr.isPresent()) {
        reply = new Reply(true, ExactCountResult.ZERO,
            "Error: trying to redefine constant " + name + " at " + cmd.getPositionInfo());
    } else {
      stable.put(name, cmd.getValue());
      reply = Reply.OK;
    }
    return reply;
  }

  @Override
  public Reply visit(ClearCommand clearCommand) {
    stable.clear();
    partitioner.clear();
    constraints.clear();
    dispatcher.clear(false);
    ExpressionVisitor.depVarCounter = 0;
    return Reply.OK;
  }

  @Override
  public Reply visit(VariableDeclarationCommand cmd) {
    Variable var = cmd.getVar();
    String name = var.getName();
    PositionInfo pos = cmd.getPositionInfo();

    Reply reply = updateGlobalScope(var, name, pos);
    return reply;
  }

  private Reply updateGlobalScope(Variable var, String name, PositionInfo pos) {
    Preconditions.checkState(stable.atGlobalScope());
    Optional<Expression> optExpr = stable.query(name);
    if (optExpr.isPresent()) {
      return new Reply(true, ExactCountResult.ZERO,
            "Error: trying to redefine variable " + name + " at " + pos);
    } else {
      stable.put(var.getName(), var);
      return Reply.OK;
    }
  }

  @Override
  public Reply visit(DependentVariableDeclarationCommand cmd) {
    Variable var = cmd.getVar();
    String name = var.getName();
    PositionInfo pos = cmd.getPositionInfo();

    Reply reply = updateGlobalScope(var, name, pos);
    System.out.println("[pcp:command-eval] dependent variable processed: " + reply);
    return reply;
  }

  @Override
  public Reply visit(PavingCommand pavingCommand) {
    BooleanNaryExpression conjunctionAllAssertions = new BooleanNaryExpression(
        Function.AND,
        constraints.stream()
            .map(Constraint::getExpr)
            .collect(Collectors.toList()));

    Constraint wrappedClauses = new Constraint(conjunctionAllAssertions);
    if (!options.isOverridePreprocessing()) {
      wrappedClauses = simplify(wrappedClauses);
    }

    List<Constraint> disjuncts = wrappedClauses.toDNF();
    StringBuilder sb = new StringBuilder();
    Paver paver = new RealPaverCaller(this.options);
    sb.append("(paving\n");
    int count = 0;
    for (Constraint disjunct : disjuncts) {
      count++;
      List<Box> paving = paver.takePaving(disjunct);
      if (paving.size() == 0) {
        continue;
      }
      sb.append("\t(#");
      sb.append(count);
      sb.append(" (");
      for (Box box : paving) {
        sb.append(box.toPcpString(options.printPavingOutputAsFloat()));
        sb.append(" ");
      }
      sb.delete(sb.length() - 1, sb.length());
      sb.append(")");
      sb.append(")\n");
    }
    sb.append(")\n");
    return new Reply(false, ExactCountResult.ZERO, sb.toString());
  }

  @Override
  public Reply visit(ExitCommand exitCommand) {
    throw new ExitRequestException();
  }

  @Override
  public Reply visit(CountCommand countCommand) {
    //TODO: maybe better to make BooleanNaryExpression accept any collection instead of creating a new list here
    try {
      BooleanNaryExpression conjunctionOfAllTheAssertionsExpression = new BooleanNaryExpression(
          Function.AND,
          constraints.stream().map(Constraint::getExpr).collect(Collectors.toList()));

      Constraint conjunctionOfAllTheAssertions = new Constraint(
          conjunctionOfAllTheAssertionsExpression);
      if (options.isOverridePreprocessing()) {
        return new Reply(false, dispatcher.count(conjunctionOfAllTheAssertions), "ok");
      }
      conjunctionOfAllTheAssertions = simplify(conjunctionOfAllTheAssertions);
      if (!options.useInclusionExclusion()) {
        return new Reply(false, dispatcher.count(conjunctionOfAllTheAssertions), "ok");
      }

      List<Constraint> disjuncts = conjunctionOfAllTheAssertions.toDNF();

      InclusionExclusionHelper inclusionExclusionHelper = new InclusionExclusionHelper(disjuncts,
          dispatcher);
      InclusionExclusionCounts result = inclusionExclusionHelper.count();

      CountResult sumOfPositive = merge(result.getAddCounts());
      CountResult sumOfNegative = merge(result.getSubCounts());

      BigRational prob = sumOfPositive.expectedProbability();
      BigRational var = BigRational.ZERO;
      long nSamples = 0;

      if (sumOfPositive instanceof StatisticalCountResult) {
        var = var.plus(((StatisticalCountResult) sumOfPositive).getVariance());
        nSamples += ((StatisticalCountResult) sumOfPositive).getNumSamples();
      }

      prob = prob.minus(sumOfNegative.expectedProbability());
      Preconditions.checkArgument(prob.isPositive() || prob.isZero());

      if (sumOfNegative instanceof StatisticalCountResult) {
        var = var.plus(((StatisticalCountResult) sumOfNegative).getVariance());
        nSamples += ((StatisticalCountResult) sumOfNegative).getNumSamples();
      }

      CountResult finalResult = (var.isZero()) ? new ExactCountResult(prob)
          : new StatisticalCountResult(prob, var, nSamples);

      return new Reply(false, finalResult, "ok");
    } catch (RuntimeException e) { // TODO create a subclass of RuntimeException for counter errors
      e.printStackTrace();
      return new Reply(true, ExactCountResult.ZERO, e.getMessage());
    }
  }

  private Constraint simplify(Constraint toBeSimplified) {
    // z3 simplification only works with integers at the moment.
    Simplifier simplifier;

    switch (options.getSimplifier()) {
      case NONE:
      case MATHEMATICA:
        return toBeSimplified;
      case Z3:
        simplifier = new Z3Simplifier();
        break;
      case Z3_SMTLIB:
        simplifier = new SMTLIBSimplifier(options);
        break;
      default:
        throw new RuntimeException("Unknown simplifier: " + options.getSimplifier());
    }

    try {
      Constraint simplified = new Constraint(simplifier.simplify(toBeSimplified.getExpr()));
      System.out.println("[simplify] final formula: " + simplified);
      return simplified;
    } catch (Exception e) {
      System.out.println("[simplify] Failure attempting to simplify input using z3. "
          + "Keep in mind that the simplifier only works for integer constraints for now."
          + "\nExact error message is: " + e.getMessage());
      return toBeSimplified;
    }
  }

  /**
   * Use eq. 5 (Handling Disjunction) to merge partial results.
   */

  //TODO: update after integrating ApproxMC. There is something suspicious with the sum of variances:
  // the sum is valid only if the constraints are uncorrelated
  // otherwise need to consider (or bound) the covariance too:
  // https://en.wikipedia.org/wiki/Variance#Sum_of_correlated_variables
  private CountResult merge(Collection<CountResult> results) {
    BigRational prob = BigRational.ZERO;
    BigRational var = BigRational.ZERO;
    long nSamples = 0;

    for (CountResult partial : results) {
      BigRational partialProb = partial.expectedProbability();
      BigRational partialVar =
          (partial instanceof StatisticalCountResult) ? ((StatisticalCountResult) partial)
              .getVariance() : BigRational.ZERO;
      long partialNSamples =
          (partial instanceof StatisticalCountResult) ? ((StatisticalCountResult) partial)
              .getNumSamples() : 0;

      prob = prob.plus(partialProb);
      var = var.plus(partialVar);
      nSamples = nSamples + partialNSamples;
    }
    return (var.isZero()) ? new ExactCountResult(prob)
        : new StatisticalCountResult(prob, var, nSamples);
  }

  public SymbolTable getSymbolTable() {
    return stable;
  }

  public Set<Constraint> getConstraints() {
    return constraints;
  }

  public Partitioner getPartitioner() {
    return partitioner;
  }

  public CountDispatcher getDispatcher() {
    return dispatcher;
  }

  public Options getOptions() {
    return options;
  }

}
