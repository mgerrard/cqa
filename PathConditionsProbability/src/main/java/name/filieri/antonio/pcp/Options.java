package name.filieri.antonio.pcp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Options {

	public Options() {
		this.optionMap = new LinkedHashMap<>();
		// Setting default values
		this.optionMap.put(Key.PARTITIONING, Boolean.TRUE.toString());
		this.optionMap.put(Key.CONVERT_TO_DNF, Boolean.TRUE.toString());
		this.optionMap.put(Key.SEED, new Long(77105613).toString());
		this.optionMap.put(Key.ISCC_PATH, "iscc");
		this.optionMap.put(Key.NON_LINEAR_COUNTER, "qcoral");
    this.optionMap.put(Key.LINEAR_COUNTER, "barvinok");
		this.optionMap.put(Key.REALPAVER_PATH, "realpaver");
    this.optionMap.put(Key.REALPAVER_MAX_BOXES, "10");
    this.optionMap.put(Key.REALPAVER_TIMEOUT, "2000");
    this.optionMap.put(Key.PAVING_PRINT_FLOAT, "true");
		this.optionMap.put(Key.REDIS_HOST, "127.0.0.1");
		this.optionMap.put(Key.REDIS_PORT, "6379");
		this.optionMap.put(Key.QCORAL_SAMPLES_INCREMENT, "2000");
		this.optionMap.put(Key.QCORAL_INITIAL_PARTITION_BUDGET, "50000");
    this.optionMap.put(Key.QCORAL_MAX_SAMPLES, "10000000");
    this.optionMap.put(Key.QCORAL_TARGET_VARIANCE, "1E-8");
    this.optionMap.put(Key.SIMPLIFY, "false");
		this.optionMap.put(Key.Z3_PATH, "z3");
		this.optionMap.put(Key.INCLUSION_EXCLUSION, "true");
  }

  public boolean isOverridePreprocessing() {
    return checkOption(Key.OVERRIDE_PREPROCESSING, "true");
  }

  private static final String DEFAULT_CONFIGURATION_FILENAME = ".pcp";

  private Map<Key, String> optionMap;

  public static void main(String[] args) {
    Options options = Options.loadFromUserHomeOrDefault();
    String isccpath = options.getIsccPath();
    System.out.println(isccpath);
  }

  public SIMPLIFIER getSimplifier() {
    String ret = optionMap.get(Key.SIMPLIFY);
    try {
      if (ret == null || ret.equalsIgnoreCase("false")) {
        return SIMPLIFIER.NONE;
      } else if (ret.equalsIgnoreCase("true")
					|| ret.replaceAll("-","_").equalsIgnoreCase("z3_smtlib")) {
        return SIMPLIFIER.Z3_SMTLIB; //default
      } else if (ret.equalsIgnoreCase("z3")) {
				return SIMPLIFIER.Z3;
			} else {
        return SIMPLIFIER.valueOf(ret.toUpperCase().trim());
      }
    } catch (IllegalArgumentException e) {
      throw new RuntimeException(
          "Unknown simplifier value (valid options are [false,true,none,z3,mathematica] : " + ret);
    }
  }

	public boolean isPartitioningEnabled() {
		return checkOption(Key.PARTITIONING,"true");
	}

	public boolean isDNFConversionEnabled() {
		return checkOption(Key.CONVERT_TO_DNF,"true");
	}

	public String getIsccPath() {
		return this.optionMap.get(Key.ISCC_PATH);
	}

  public boolean useBarvinokForLinear() {
    return checkOption(Key.LINEAR_COUNTER, "barvinok") || checkOption(Key.LINEAR_COUNTER, "iscc");
  }

  public boolean useQCoralForNonlinear() {
		return checkOption(Key.NON_LINEAR_COUNTER,"qcoral");
	}

  public boolean useQCoralForLinear() {
    return checkOption(Key.LINEAR_COUNTER, "qcoral");
  }

  public boolean useSharpSATForNonlinear() {
		return checkOption(Key.NON_LINEAR_COUNTER,"sharpsat");
	}

  public boolean useSharpSATForLinear() {
    return checkOption(Key.LINEAR_COUNTER, "sharpsat");
  }

  public boolean useZ3ForNonlinear() {
		return checkOption(Key.NON_LINEAR_COUNTER,"z3");
	}

  public boolean useZ3ForLinear() {
    return checkOption(Key.LINEAR_COUNTER, "z3");
  }
	
	public long getSeed() {
		return Long.parseLong(this.optionMap.get(Key.SEED));
	}

  public void processOption(Key option, String val) {
    optionMap.put(option, val.trim());
  }

	public int getQCoralSamplesPerIncrement() {
		return Integer.parseInt(this.optionMap.get(Key.QCORAL_SAMPLES_INCREMENT));
	}

	public int getQCoralInitialPartitionBudget() {
		return Integer.parseInt(this.optionMap.get(Key.QCORAL_INITIAL_PARTITION_BUDGET));
	}

	public long getQCoralMaxSamples() {
		return Long.parseLong(this.optionMap.get(Key.QCORAL_MAX_SAMPLES));
	}

	public double getQCoralTargetVariance() {
		return Double.parseDouble(this.optionMap.get(Key.QCORAL_TARGET_VARIANCE));
	}

	public String getSharpSatPath() {
		String ret = optionMap.get(Key.SHARPSAT_PATH);
		if (ret == null) {
			ret = "sharpSAT";
		}
		return ret;
	}
	
	public String getRealPaverPath() {
		String ret = optionMap.get(Key.REALPAVER_PATH);
		if (ret == null) {
			ret = "realpaver";
		}
		return ret;
	}

	public String getZ3Path() {
		String ret = optionMap.get(Key.Z3_PATH);
		if (ret == null) {
			ret = "z3";
		}
		return ret;
	}

  public int getRealPaverMaxBoxes() {
    return Integer.parseInt(this.optionMap.get(Key.REALPAVER_MAX_BOXES));
  }

  public int getRealPaverTimeout() {
    return Integer.parseInt(this.optionMap.get(Key.REALPAVER_TIMEOUT));
  }

  public boolean printPavingOutputAsFloat() {
    return Boolean.parseBoolean(this.optionMap.get(Key.PAVING_PRINT_FLOAT));
  }

	public boolean useInclusionExclusion() {
		return Boolean.parseBoolean(this.optionMap.get(Key.INCLUSION_EXCLUSION));
	}

	public enum Key {
    ISCC_PATH,
    SHARPSAT_PATH,
    Z3_PATH,
    SEED,
    CONVERT_TO_DNF,
    PARTITIONING,
    PAVING_PRINT_FLOAT,
    NON_LINEAR_COUNTER,
    REALPAVER_PATH,
    REALPAVER_TIMEOUT,
    REALPAVER_MAX_BOXES,
    REDIS_HOST,
    QCORAL_SAMPLES_INCREMENT,
    QCORAL_INITIAL_PARTITION_BUDGET,
    QCORAL_MAX_SAMPLES,
    QCORAL_TARGET_VARIANCE, REDIS_PORT,
    OVERRIDE_PREPROCESSING,
    INCLUSION_EXCLUSION,
    SIMPLIFY,
    LINEAR_COUNTER
  }

	private static Optional<Options> cachedOptions = Optional.empty();

	public static Options loadFromUserHomeOrDefault() {
	  if (cachedOptions.isPresent()) {
	    return cachedOptions.get();
    } else {
      Options options = new Options();
      String homeFolder = System.getProperty("user.home");
      File defaultConfiguration = new File(homeFolder, Options.DEFAULT_CONFIGURATION_FILENAME);
      if (defaultConfiguration.exists()) {
        options.loadFromFile(defaultConfiguration);
      }
      cachedOptions = Optional.of(options);
      return options;
    }
	}

	public static void reload() {
	  cachedOptions = Optional.empty();
  }

	public void loadFromFile(String path) {
		this.loadFromFile(new File(path));
	}

	public void loadFromFile(File file) {
		try (FileInputStream in = new FileInputStream(file)) {
			Scanner sc = new Scanner(in);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				int commentPos = line.indexOf('#');
				if (commentPos >= 0) {
					line = line.trim().substring(0, commentPos).trim();
				}
				if (!line.isEmpty()) {
					String[] toks = line.split("=");
					String key = toks[0];
					String val = toks[1];
					processOption(key,val);	
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void processOption(String key, String val) {
		key = key.trim();
		key = key.replace('-', '_').toUpperCase();
		if (key.startsWith(":")) {
			key = key.substring(1);
		}
		
		Key option = Key.valueOf(key);
		optionMap.put(option, val.trim());
	}

  public enum SIMPLIFIER {NONE, Z3, Z3_SMTLIB, MATHEMATICA}
	
	public boolean checkOption(Key key, String val) {
		if (optionMap.containsKey(key)) {
			String storedValue = optionMap.get(key);
			return storedValue.equalsIgnoreCase(val);
		}
		return false;
	}

	public boolean checkIfBarvinokIsAccessible() {
		boolean isAccessible = false;
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			proc = rt.exec(getIsccPath()+" --help");
			proc.waitFor();
			int exitVal = proc.exitValue();
      isAccessible = (exitVal == 0);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return isAccessible;
	}

  public boolean checkIfSharpsatIsAccessible() {
    boolean isAccessible = false;
    Runtime rt = Runtime.getRuntime();
    Process proc;
    try {
      proc = rt.exec(getSharpSatPath());
      proc.waitFor();
      int exitVal = proc.exitValue();
      // Vladmir's fork of sharpsat returns 255 if called without files,
      // and 0 if the file doesn't exists.
      isAccessible = (exitVal == 255);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return isAccessible;
  }

  public String getRedisHost(){
	  return optionMap.get(Key.REDIS_HOST);
  }

  public int getRedisPort(){
  	String ret = optionMap.get(Key.REDIS_PORT);
  	if(ret!=null){
  		return Integer.parseInt(ret);
	}
	return -1;
  }

  public boolean isRedisAvailable(){
  	try{
		JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), getRedisHost(), getRedisPort());
		Jedis jedis = jedisPool.getResource();
		jedis.close();
		return true;
	}catch(Exception e){
  		return false;
	}
  }
}
