package name.filieri.antonio.pcp;

import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.utils.Cacher;
import name.filieri.antonio.pcp.utils.GuavaRedisCacher;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConcurrentPCPServer {

    private static final long DEFAULT_CACHE_SIZE = 10000;
    private static final int DEFAULT_N_THREADS = 8;
    private final ServerSocket serverSocket;
    private final ExecutorService executorService;
    private final Cacher sharedCache;
    private final Options options;

    public ConcurrentPCPServer(int port, int maxThreads) throws IOException {
        this.serverSocket = new ServerSocket(port);
        this.executorService = Executors.newFixedThreadPool(maxThreads);
        this.options = Options.loadFromUserHomeOrDefault();
        this.sharedCache = new GuavaRedisCacher(options);
    }

    public void waitForNewSession() {
        System.out.println("[concurrentPCP] Waiting for connection...");
        try {
            Socket clientSocket = serverSocket.accept();
            System.out.println("[concurrentPCP] Connected! " + clientSocket.toString());
            executorService.submit(new PCPSession(clientSocket, sharedCache, options));
        } catch (IOException e) {
            System.out.println("[concurrentPCP] IO error while submitting new job: " + e);
            e.printStackTrace();
        }
    }

    public static class PCPSession implements Runnable {

        private final Socket clientSocket;
        private final Cacher sharedCache;
        private final Options options;

        public PCPSession(Socket clientSocket, Cacher sharedCache, Options options) {
            this.clientSocket = clientSocket;
            this.sharedCache = sharedCache;
            this.options = options;
        }

        @Override
        public void run() {
            try {
                SymbolTable stable = new SymbolTable();
                Partitioner partitioner = new ConstraintPartitioner();
                CountDispatcher dispatcher = new PCPDispatcher(options, sharedCache);
                CommandEvaluator evaluator = new CommandEvaluator(stable, partitioner, dispatcher,
                        options);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream(), "UTF-8"));
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream());

                String line;
                while ((line = in.readLine()) != null) {
                    System.out.println("[pcpserver] query received: " + line);
                    ANTLRInputStream input = new ANTLRInputStream(line);
                    InputFormatLexer lexer = new InputFormatLexer(input);
                    InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

                    ParseTree tree = parser.input();
                    List<? extends Command> commands = AstBuilder
                            .parseCommands(evaluator.getSymbolTable(), tree);

                    for (Command command : commands) {
                        Reply reply = command.accept(evaluator);
                        if (reply.error) {
                            out.println("(error \"" + reply.message + "\")");
                            out.flush();
                        } else if (command instanceof CountCommand) {
                            CountResult result = reply.result;
                            out.println("(" + result + ")");
                            out.flush();
                        }
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        int port;
        int nThreads;
        if (args.length == 0) {
            port = 9991;
            nThreads = DEFAULT_N_THREADS;
        } else {
            port = Integer.parseInt(args[0]);
            nThreads = args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_N_THREADS;
        }

        ConcurrentPCPServer server = null;
        try {
            server = new ConcurrentPCPServer(port, nThreads);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Concurrent PCP Server started");

        ConcurrentPCPServer finalServer = server;
        Runnable signalHandler = () -> {
            System.out.println("[concurrentPCP] signal received, waiting at most 15s for tasks to end...");
            try {
                finalServer.executorService.awaitTermination(15, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
            }
        };
        Runtime.getRuntime().addShutdownHook(new Thread(signalHandler));

        while (true) {
            server.waitForNewSession();
        }
    }
}
