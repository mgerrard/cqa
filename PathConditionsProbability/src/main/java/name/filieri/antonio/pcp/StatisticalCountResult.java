package name.filieri.antonio.pcp;

import com.google.common.hash.Hashing;
import name.filieri.antonio.jpf.utils.BigRational;

public class StatisticalCountResult implements CountResult {

  private final BigRational probability;
  private final BigRational variance;
  private final long numSamples;
  private Integer hashCode = null;

  public StatisticalCountResult(BigRational probability,
      BigRational variance, long numSamples) {
    this.probability = probability;
    this.variance = variance;
    this.numSamples = numSamples;
  }

  public StatisticalCountResult(BigRational probability,
      double variance, long numSamples) {
    this(probability, BigRational.valueOf(variance), numSamples);
  }

  public StatisticalCountResult(double probability,
      BigRational variance, long numSamples) {
    this(BigRational.valueOf(probability), variance, numSamples);
  }

  public StatisticalCountResult(double probability,
      double variance, long numSamples) {
    this(BigRational.valueOf(probability), BigRational.valueOf(variance), numSamples);
  }


  @Override
  public BigRational expectedProbability() {
    return this.probability;
  }

  public BigRational getVariance() {
    return this.variance;
  }

  public long getNumSamples() {
    return this.numSamples;
  }

  @Override
  public boolean equals(CountResult cr) {
    if (cr instanceof StatisticalCountResult) {
      return probability.equals(((StatisticalCountResult) cr).probability) && variance
          .equals(((StatisticalCountResult) cr).variance)
          && numSamples == ((StatisticalCountResult) cr).numSamples;
    }
    return false;
  }

  @Override
  public int hashcode() {
    if (this.hashCode == null) {
      this.hashCode = Hashing.goodFastHash(512)
          .newHasher()
          .putInt(this.getClass().hashCode())
          .putInt(probability.hashCode())
          .putInt(variance.hashCode())
          .putLong(numSamples)
          .hash().asInt();
    }
    return this.hashCode;
  }

  @Override
  public String toString() {
    return "statistical: " + probability + " [var: " + variance + " , samples: "+numSamples+"]";
  }
}
