package name.filieri.antonio.pcp.visitors;

import com.google.common.collect.Maps;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.Optional;
import name.filieri.antonio.pcp.ast.expr.Expression;

public class SymbolTable {

  private final Deque<Map<String, Expression>> scopeStack;

  public SymbolTable() {
    this.scopeStack = new ArrayDeque<>();
    scopeStack.push(Maps.newHashMap()); //global context
  }

  public void newScope() {
    scopeStack.push(Maps.newHashMap());
  }

  public void dropScope() {
    assert scopeStack.size() >= 2;
    scopeStack.pop();
  }

  public void dropNonGlobalScopes() {
    while (scopeStack.size() > 1) {
      scopeStack.pop();
    }
  }

  public Optional<Expression> query(String literal) {
    return scopeStack.stream()
        .filter(m -> m.containsKey(literal))
        .findFirst()
        .map(m -> m.get(literal));
  }

  public Optional<Expression> put(String literal, Expression expr) {
    return Optional.ofNullable(scopeStack.peekFirst().put(literal, expr));
  }

  public void clear() {
    dropNonGlobalScopes();
    scopeStack.peekLast().clear();
  }

  public boolean atGlobalScope() {
    return scopeStack.size() == 1;
  }
}
