package name.filieri.antonio.pcp.visitors;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FPExpr;
import com.microsoft.z3.IntExpr;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class Z3Translator extends ExpressionBaseVisitor<Expr> {

  private final Context ctx;

  private final BiMap<Variable, Expr> declaredVariables;

  public Z3Translator(Context ctx) {
    this.ctx = ctx;
    this.declaredVariables = HashBiMap.create();
  }

  public BoolExpr translate(BooleanExpression expression) {
    BoolExpr z3Expr = (BoolExpr) expression.accept(this);
    List<BoolExpr> varBounds = declaredVariables.entrySet().stream().map(entry -> {
      NumericBoundedVariable pcpVar = (NumericBoundedVariable) entry.getKey();
      Expr z3Var = entry.getValue();
      return ctx.mkAnd(
          ctx.mkGe((ArithExpr) z3Var, ctx.mkInt(pcpVar.getLowerBound().longValue())),
          ctx.mkLe((ArithExpr) z3Var, ctx.mkInt(pcpVar.getUpperBound().longValue())));
    }).filter(Objects::nonNull).collect(Collectors.toCollection(ArrayList::new));
    varBounds.add(z3Expr);
    return ctx.mkAnd(varBounds.toArray(new BoolExpr[]{}));
  }

  @Override
  public Expr emptyResult() {
    throw new RuntimeException("Not implemented - check trace!");
  }

  @Override
  public Expr mergeResults(List<Expr> childResults) {
    throw new RuntimeException("Not implemented");
  }


  @Override
  public BoolExpr visit(BooleanConstant booleanConstant) {
    return booleanConstant.getValue() ? ctx.mkTrue() : ctx.mkFalse();
  }

  @Override
  public BoolExpr visit(BooleanNaryExpression expr) {
    List<BoolExpr> results = new ArrayList<>();
    for (Expression arg : expr.getArgs()) {
      results.add((BoolExpr) arg.accept(this));
    }

    switch (expr.getFunction()) {
      case AND:
        return ctx.mkAnd(results.toArray(new BoolExpr[]{}));
      case OR:
        return ctx.mkOr(results.toArray(new BoolExpr[]{}));
      default:
        throw new RuntimeException("Missing case: " + expr.getFunction());
    }
  }

  @Override
  public BoolExpr visit(NotExpression expr) {
    Expr arg = expr.getArg().accept(this);
    return ctx.mkNot((BoolExpr) arg);
  }

  @Override
  public Expr visit(NumericBinaryExpression expr) {
    Expr a = expr.getLeft().accept(this);
    Expr b = expr.getRight().accept(this);
    return handleBinOp(expr.getFunction(), a, b);
  }

  @Override
  public Expr visit(NumericComparisonExpression expr) {
    Expr a = expr.getLeft().accept(this);
    Expr b = expr.getRight().accept(this);
    return handleBinCmp(expr.getFunction(), a, b);
  }

  @Override
  public Expr visit(NumericUnaryExpression expr) {
    Expr arg = expr.getArg().accept(this);
    return handleUnary(expr.getFunction(), arg);
  }

  @Override
  public Expr visit(NumericConstant constant) {
    if (!constant.getType().isInteger()) {
      throw new RuntimeException("FP handling not implemented yet!");
    }
    return ctx.mkInt(constant.getValue().longValue());
  }

  @Override
  public Expr visit(StringConstant stringConstant) {
    return emptyResult();
  }

  @Override
  public Expr visit(NormalVariable normalVariable) {
    return emptyResult();
  }

  @Override
  public Expr visit(BernoulliVariable bernoulliVariable) {
    return emptyResult();
  }

  @Override
  public Expr visit(UniformFloatVariable uniformFloatVariable) {
    return emptyResult();
  }

  @Override
  public Expr visit(UniformDoubleVariable uniformDoubleVariable) {
    return emptyResult();
  }

  @Override
  public Expr visit(UniformIntegerVariable intVar) {
    Expr z3var = declaredVariables.get(intVar);
    if (z3var == null) {
      z3var = ctx.mkIntConst(intVar.getName());
      declaredVariables.put(intVar, z3var);
    }
    return z3var;
  }

  @Override
  public Expr visit(UniformLongVariable longVar) {
    Expr z3var = declaredVariables.get(longVar);
    if (z3var == null) {
      z3var = ctx.mkIntConst(longVar.getName());
      declaredVariables.put(longVar, z3var);
    }
    return z3var;
  }

  @Override
  public Expr visit(StringVariable stringVariable) {
    return emptyResult();
  }

  @Override
  public Expr visit(StringBinaryExpression stringBinaryExpression) {
    return emptyResult();
  }

  @Override
  public Expr visit(DependentVariable depVar) {
    Expr z3var = declaredVariables.get(depVar);
    if (z3var == null) {
      z3var = ctx.mkIntConst(depVar.getName());
      declaredVariables.put(depVar, z3var);
    }
    return z3var;
  }

  @Override
  public Expr visit(BitVectorConstant bitVectorConstant) {
    return emptyResult();
  }

  @Override
  public Expr visit(BitVectorBinaryExpression bitVectorBinaryExpression) {
    return emptyResult();
  }

  @Override
  public Expr visit(BitVectorExtractExpression bitVectorExtractExpression) {
    return emptyResult();
  }

  @Override
  public Expr visit(BitVectorCompareExpression bitVectorCompareExpression) {
    return emptyResult();
  }

  @Override
  public Expr visit(BitVectorUnaryExpression bitVectorUnaryExpression) {
    return bitVectorUnaryExpression.getArg().accept(this);
  }

  @Override
  public Expr visit(BitVectorVariable bitVectorVariable) {
    return emptyResult();
  }

  private Expr handleBinOp(Function function, Expr a, Expr b) {
    boolean isFP = a instanceof FPExpr || b instanceof FPExpr;
    if (isFP) {
      throw new RuntimeException("FP handling not implemented yet!");
    }

    switch (function) {
      case ADD:
        return ctx.mkAdd((ArithExpr) a, (ArithExpr) b);
      case SUB:
        return ctx.mkSub((ArithExpr) a, (ArithExpr) b);
      case MUL:
        return ctx.mkMul((ArithExpr) a, (ArithExpr) b);
      case DIV:
        return ctx.mkDiv((ArithExpr) a, (ArithExpr) b);
      case MOD:
        return ctx.mkMod((IntExpr) a, (IntExpr) b);
      case POW:
        return ctx.mkPower((ArithExpr) a, (ArithExpr) b);
      case MAX:
        return ctx.mkITE(ctx.mkGt((ArithExpr) a, (ArithExpr) b), a, b);
      case MIN:
        return ctx.mkITE(ctx.mkLt((ArithExpr) a, (ArithExpr) b), a, b);
      case ATAN2:
        break;
    }

    throw new RuntimeException();
  }

  private Expr handleBinCmp(NumericComparisonExpression.Function function, Expr a, Expr b) {
    boolean isFP = a instanceof FPExpr || b instanceof FPExpr;
    if (isFP) {
      throw new RuntimeException("FP handling not implemented yet!");
    }

    switch (function) {
      case NE:
        return ctx.mkNot(ctx.mkEq(a, b));
      case LE:
        return ctx.mkLe((ArithExpr) a, (ArithExpr) b);
      case LT:
        return ctx.mkLt((ArithExpr) a, (ArithExpr) b);
      case EQ:
        return ctx.mkEq(a, b);
      case GT:
        return ctx.mkGt((ArithExpr) a, (ArithExpr) b);
      case GE:
        return ctx.mkGe((ArithExpr) a, (ArithExpr) b);
    }

    throw new RuntimeException();
  }


  private Expr handleUnary(NumericUnaryExpression.Function function, Expr arg) {
    boolean isFP = arg instanceof FPExpr;
    if (isFP) {
      throw new RuntimeException("FP handling not implemented yet!");
    }

    switch (function) {
      case SIN:
        break;
      case COS:
        break;
      case TAN:
        break;
      case SINH:
        break;
      case COSH:
        break;
      case TANH:
        break;
      case EXP:
        break;
      case SQRT:
        break;
      case LOG10:
        break;
      case LOG:
        break;
    }

    throw new RuntimeException();
  }

  public BiMap<Variable, Expr> getDeclaredVariables() {
    return declaredVariables;
  }
}
