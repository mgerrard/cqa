package name.filieri.antonio.pcp.icp;

import java.util.List;
import name.filieri.antonio.pcp.Constraint;

public interface Paver {

  /**
   * @param constraint Conjunction of real and/or integer constraints
   */
  List<Box> takePaving(Constraint constraint);

}


