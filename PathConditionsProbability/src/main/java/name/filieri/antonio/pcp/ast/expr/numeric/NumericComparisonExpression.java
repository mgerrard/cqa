package name.filieri.antonio.pcp.ast.expr.numeric;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BinaryExpression;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;


public class NumericComparisonExpression implements BinaryExpression, BooleanExpression {

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    NumericComparisonExpression other = (NumericComparisonExpression) obj;

    if (this.hashCode() != other.hashCode()) {
      return false;
    }

    if (left == null) {
      if (other.left != null) {
        return false;
      }
    } else if (!left.equals(other.left)) {
      return false;
    }
    if (operator != other.operator) {
      return false;
    }
    if (right == null) {
      return other.right == null;
    } else {
      return right.equals(other.right);
    }
  }

  private final Expression left;
  private final Expression right;
  private final Function operator;
  private final PositionInfo position;
  private HashCode hash = null;

  public NumericComparisonExpression(Expression left, Expression right, Function operator,
      PositionInfo position) {
    super();
    this.left = left;
    this.right = right;
    this.operator = operator;
    this.position = position;
  }

  public NumericComparisonExpression(Expression left, Expression right, Function operator) {
    this(left, right, operator, PositionInfo.DUMMY);
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  public NumericComparisonExpression negate() {
    return new NumericComparisonExpression(left, right, getFunction().negate(), position);
  }

  @Override
  public HashCode getHashCode() {
    if (hash == null) {
      HashCode hc = ASTUtil.getHashFunction().newHasher()
          .putString(operator.name(), StandardCharsets.UTF_8)
          .hash();
      hash = Hashing.combineOrdered(ImmutableList.of(
          hc, left.getHashCode(), right.getHashCode()));
    }
    return hash;
  }

  @Override
  public PositionInfo getPositionInfo() {
    return position;
  }

  @Override
  public Expression getLeft() {
    return left;
  }

  @Override
  public Expression getRight() {
    return right;
  }

  @Override
  public int hashCode() {
    return getHashCode().asInt();
  }

  public Function getFunction() {
    return operator;
  }

	public enum Function {
		NE, LE, LT, EQ, GT, GE;

		public static Optional<Function> fromSymbol(String symbol) {
			switch(symbol) {
			case "<":
				return Optional.of(LT);
			case ">":
				return Optional.of(GT);
			case "<=":
				return Optional.of(LE);
			case ">=":
				return Optional.of(GE);
			case "=":
				return Optional.of(EQ);
			case "!=":
				return Optional.of(NE);
			default:
        return Optional.empty();
			}
		}

		public String toSymbol() {
			switch (this) {
			case EQ:
				return "=";
			case GE:
				return ">=";
			case GT:
				return ">";
			case LE:
				return "<=";
			case LT:
				return "<";
			case NE:
				return "!=";
			default:
				throw new RuntimeException("Unknown case: " + this);
			}
		}

		public Function negate() {
			switch (this) {
			case EQ:
				return NE;
			case GE:
				return LT;
			case GT:
				return LE;
			case LE:
				return GT;
			case LT:
				return GE;
			case NE:
				return EQ;
			default:
				throw new RuntimeException("Unknown case: " + this);
			}
		}
	}

	@Override
	public String toString() {
		return left.toString() + " " + operator.toSymbol() + " " + right.toString();
	}

}
