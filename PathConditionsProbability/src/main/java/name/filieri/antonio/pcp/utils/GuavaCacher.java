package name.filieri.antonio.pcp.utils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Maps;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;

import java.util.Map;

public class GuavaCacher implements Cacher{
    private static final int DEFAULT_CACHE_SIZE = 10000;
    private final Cache<Constraint, Map<Solver,CountResult>> cache;


    public GuavaCacher(){
        this(DEFAULT_CACHE_SIZE);
    }

    public GuavaCacher(int size){
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(size)
                .recordStats()
                .build();
    }

    @Override
    public synchronized CountResult getIfPresent(Constraint problem) {
        Map<Solver,CountResult> results = cache.getIfPresent(problem);
        if(results!=null){
            if(results.containsKey(Solver.BARVINOK)){
                return results.get(Solver.BARVINOK);
            }
            if(results.containsKey(Solver.SHARPSAT)){
                return results.get(Solver.SHARPSAT);
            }
            if(results.containsKey(Solver.Z3BLOCKING)){
                return results.get(Solver.Z3BLOCKING);
            }
            if(results.containsKey(Solver.QCORAL)){
                return results.get(Solver.QCORAL);
            }
            throw new RuntimeException("Unrecognized solver: "+results);
        }
        return null;
    }

    @Override
    public synchronized CountResult getIfPresent(Constraint problem, Solver solver) {
        Map<Solver,CountResult> results = cache.getIfPresent(problem);
        if(results!=null){
            return results.get(solver);
        }
        return null;
    }

    @Override
    public synchronized void put(Constraint constraint, CountResult result, Solver solver) {
        Map<Solver,CountResult> entry = cache.getIfPresent(constraint);
        if(entry==null){
            entry = Maps.newHashMap();
        }
        entry.put(solver,result);   //Actually needed only if not already in or if the solver is QCoral and
                                    // the number of samples is larger
        cache.put(constraint,entry);
    }

    @Override
    public synchronized void invalidateAll() {
        this.cache.invalidateAll();
    }
}
