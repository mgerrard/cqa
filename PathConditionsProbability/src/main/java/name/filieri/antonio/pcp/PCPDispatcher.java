package name.filieri.antonio.pcp;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory.Kind;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.counters.BarvinokCaller;
import name.filieri.antonio.pcp.counters.ModelCounterCaller;
import name.filieri.antonio.pcp.counters.ModelCounterException;
import name.filieri.antonio.pcp.counters.QCoralCaller;
import name.filieri.antonio.pcp.counters.SharpSatCaller;
import name.filieri.antonio.pcp.counters.Z3BlockingCaller;
import name.filieri.antonio.pcp.utils.Cacher;
import name.filieri.antonio.pcp.utils.Cacher.Solver;
import name.filieri.antonio.pcp.utils.GuavaRedisCacher;

public class PCPDispatcher implements CountDispatcher {

	private static final int DEFAULT_CACHE_SIZE = 10000;
	private final Options options;
	private final Cacher cache;
	private BarvinokCaller bvCaller;
	private QCoralCaller qcoralCaller;
	private SharpSatCaller sharpsatCaller;
	private Z3BlockingCaller z3Caller;

	public PCPDispatcher(Options options, Cacher cache) {
		this.cache = cache;
		this.options = options;
	}

	public PCPDispatcher(Options options) {
		this.cache = new GuavaRedisCacher(options);
		this.options = options;
	}

	@Override
	public CountResult count(Constraint constraint) {
		Multimap<Constraint,Constraint> mmap = LinkedHashMultimap.create();
		mmap.put(constraint,constraint);
		return count(mmap).get(constraint);
	}

	@Override
	public Map<Constraint, CountResult> count(Multimap<Constraint, Constraint> partitionMap) {
		Map<ExpressionCategory.Kind, Multimap<Constraint, Constraint>> categoryMap = classify(partitionMap);
		moveFPConstraintsToNonLinear(categoryMap);
		Map<Constraint, CountResult> partitionResults = new LinkedHashMap<>();

		for (ExpressionCategory.Kind kind : categoryMap.keySet()) {
			Multimap<Constraint, Constraint> categoryConstraints = categoryMap.get(kind);
			Map<Constraint, CountResult> categoryResults;

			switch (kind) {
			case DATASTRUCTURE:
				throw new RuntimeException("Not supported");
			case LINEAR:
				try {
					categoryResults = handleLinear(categoryConstraints);
				} catch (ModelCounterException e) {
					System.err.println("Error while handling linear expression: " + e.getMessage());
					// return empty map
					categoryResults = new HashMap<>();
				}
				break;
			case NONLINEAR:
				try {
					categoryResults = handleNonLinear(categoryConstraints);
				} catch (ModelCounterException e) {
          System.err.println("Error while handling nonlinear expression: " + e.getMessage());
					// return empty map
					categoryResults = new HashMap<>();
				}
				break;
			case STRING:
				categoryResults = handlePureString(categoryConstraints);
				break;
			case STRING_NUMERIC:
				categoryResults = handleMixedStringNumeric(categoryConstraints);
				break;
			case BITVECTOR:
				try {
					categoryResults = handleBitVector(categoryConstraints);
				} catch (ModelCounterException e) {
					System.err.println("Error while handling bv expression: " + e.getMessage());
					// return empty map
					categoryResults = new HashMap<>();
				}
				break;
			default:
				throw new RuntimeException("Not implemented yet: " + kind);
			}
			partitionResults.putAll(categoryResults);
		}
		return partitionResults;
	}

	private void moveFPConstraintsToNonLinear(
			Map<ExpressionCategory.Kind, Multimap<Constraint, Constraint>> categoryMap) {
		Multimap<Constraint, Constraint> linearConstraints = categoryMap.get(Kind.LINEAR);

		if (linearConstraints == null || linearConstraints.isEmpty()) {
			return;
		}
		Multimap<Constraint, Constraint> nonLinearConstraints = categoryMap.get(Kind.NONLINEAR);
		if (nonLinearConstraints == null) {
			nonLinearConstraints = LinkedHashMultimap.create();
			categoryMap.put(Kind.NONLINEAR,nonLinearConstraints);
		}

		for (Entry<Constraint, Constraint> entry : linearConstraints.entries()) {
			Constraint original = entry.getKey();
			Constraint partition = entry.getValue();

			for (Variable v : partition.getVariables()) {
				if (v.getType() != ExpressionType.INT) {
					linearConstraints.remove(original, partition);
					nonLinearConstraints.put(original, partition);
					System.out.println(
							"[pcpDispatcher] found non-integer variable in linear constraint: moving to non-linear...");
				}
			}
		}
	}

	private Map<Constraint, CountResult> handleBitVector(
			Multimap<Constraint, Constraint> consToPartitions) throws ModelCounterException {
		Map<Constraint, CountResult> counts = new HashMap<>();
		Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
		Set<Constraint> uncountedProblems = new LinkedHashSet<>();

		for (Constraint problem : problems) {
			CountResult problemCount = cache.getIfPresent(problem);
			if (problemCount != null) {
//				System.out.println("[pcpdispatcher] cache hit!");
				counts.put(problem, problemCount);
			} else {
				uncountedProblems.add(problem);
			}
		}

		ModelCounterCaller caller;
		Cacher.Solver solver;
    if (options.useSharpSATForNonlinear()) {
			if (sharpsatCaller == null) {
				sharpsatCaller = new SharpSatCaller(options);
			}
			caller = sharpsatCaller ;
			solver = Cacher.Solver.SHARPSAT;
    } else if (options.useZ3ForNonlinear()) {
			if (z3Caller == null) {
				z3Caller = new Z3BlockingCaller(options);
			}
			caller = z3Caller;
			solver= Cacher.Solver.Z3BLOCKING;
		} else {
			throw new RuntimeException("No model counter for non-linear constraints was chosen!");
		}

		if (!uncountedProblems.isEmpty()) {
			Map<Constraint, CountResult> missingCounts = caller.count(uncountedProblems);
			cache.putAll(missingCounts, solver);
			counts.putAll(missingCounts);
		}

		return counts;
	}

	private Map<Constraint, CountResult> handleMixedStringNumeric(
			Multimap<Constraint, Constraint> categoryConstraints) {
		throw new RuntimeException("Not implemented");
	}

	private Map<Constraint, CountResult> handlePureString(Multimap<Constraint, Constraint> consToPartitions) {
		throw new RuntimeException("Not implemented");
	}

	private Map<Constraint, CountResult> handleNonLinear(Multimap<Constraint, Constraint> consToPartitions)
			throws ModelCounterException {
		Map<Constraint, CountResult> counts = new HashMap<>();
		Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
		Set<Constraint> uncountedProblems = new LinkedHashSet<>();

		for (Constraint problem : problems) {
			CountResult problemCount = cache.getIfPresent(problem);
			if (problemCount != null) {
//				System.out.println("[pcpdispatcher] cache hit!");
				counts.put(problem, problemCount);
			} else {
				uncountedProblems.add(problem);
			}
		}

		ModelCounterCaller caller;
		Cacher.Solver solver;

    if (options.useQCoralForNonlinear()) {
			if (qcoralCaller == null) { 
				qcoralCaller = new QCoralCaller(options);
			}
			caller = qcoralCaller;
			solver= Cacher.Solver.QCORAL;
    } else if (options.useSharpSATForNonlinear()) {
			if (sharpsatCaller == null) { 
				sharpsatCaller = new SharpSatCaller(options);
			}
			caller = sharpsatCaller ;
			solver= Cacher.Solver.SHARPSAT;
    } else if (options.useZ3ForNonlinear()) {
			if (z3Caller == null) { 
				z3Caller = new Z3BlockingCaller(options);
			}
			caller = z3Caller;
			solver= Cacher.Solver.Z3BLOCKING;
		} else {
			throw new RuntimeException("No model counter for non-linear constraints was chosen!");
		}

		if (!uncountedProblems.isEmpty()) {
			Map<Constraint, CountResult> missingCounts = caller.count(uncountedProblems);
			cache.putAll(missingCounts, solver);
			counts.putAll(missingCounts);
		}

		return counts;
	}

	private Map<Constraint, CountResult> handleLinear(Multimap<Constraint, Constraint> consToPartitions)
			throws ModelCounterException {
		Map<Constraint, CountResult> counts = new HashMap<>();
		Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
		Set<Constraint> uncountedProblems = new LinkedHashSet<>();

		for (Constraint problem : problems) {
			CountResult problemCount = cache.getIfPresent(problem);
			if (problemCount != null) {
//				System.out.println("[pcpdispatcher] cache hit!");
				counts.put(problem, problemCount);
			} else {
				uncountedProblems.add(problem);
			}
		}

    ModelCounterCaller caller;
    Cacher.Solver solver;

    if (options.useBarvinokForLinear()) {
      if (bvCaller == null) { // initialize barvinok lazily
        bvCaller = new BarvinokCaller(options);
      }
      caller = bvCaller;
      solver = Solver.BARVINOK;
    } else if (options.useQCoralForLinear()) {
      if (qcoralCaller == null) {
        qcoralCaller = new QCoralCaller(options);
      }
      caller = qcoralCaller;
      solver = Cacher.Solver.QCORAL;
    } else if (options.useSharpSATForLinear()) {
      if (sharpsatCaller == null) {
        sharpsatCaller = new SharpSatCaller(options);
      }
      caller = sharpsatCaller;
      solver = Cacher.Solver.SHARPSAT;
    } else if (options.useZ3ForLinear()) {
      if (z3Caller == null) {
        z3Caller = new Z3BlockingCaller(options);
      }
      caller = z3Caller;
      solver = Cacher.Solver.Z3BLOCKING;
    } else {
      throw new RuntimeException("No model counter for linear constraints was chosen!");
		}

		if (!uncountedProblems.isEmpty()) {
      Map<Constraint, CountResult> missingCounts = caller.count(uncountedProblems);
      cache.putAll(missingCounts, solver);
			counts.putAll(missingCounts);
		}

		return counts;
	}

	// group entries by constraint type
	private Map<ExpressionCategory.Kind, Multimap<Constraint, Constraint>> classify(
			Multimap<Constraint, Constraint> partitionMap) {
		Map<ExpressionCategory.Kind, Multimap<Constraint, Constraint>> categoryMap = new HashMap<>();

		for (java.util.Map.Entry<Constraint, Constraint> entry : partitionMap.entries()) {
			Constraint originalConstraint = entry.getKey();
			Constraint partition = entry.getValue();

			Set<ExpressionCategory> pcats = partition.getCategory();
			Preconditions.checkState(pcats.size() > 0);

			// FIXME temporary restriction until I figure out how to handle
			// multiple kinds of constraints at once
      Set<Kind> catKinds = pcats.stream().map(cat -> cat.category).collect(Collectors.toSet());
      if (Sets.intersection(catKinds, ImmutableSet.of(Kind.LINEAR, Kind.NONLINEAR)).size() == 2) {
        pcats = pcats.stream()
            .filter(cat -> cat.category == Kind.NONLINEAR)
            .collect(Collectors.toSet());
      } else if (pcats.size() > 1) {
				throw new RuntimeException(
            "Found constraint with multiple types.\n" + partition + "\n" + pcats);
			}
			for (ExpressionCategory pcat : pcats) {
				if (pcat.isConstant) {
					System.out.println("[pcp:dispatcher] Found constant expression, ignoring it...");
				} else {
					Kind category = pcat.category;
					Multimap<Constraint, Constraint> pcatMap = categoryMap.get(category);
					if (pcatMap == null) {
						pcatMap = LinkedHashMultimap.create();
						categoryMap.put(category, pcatMap);
					}

					pcatMap.put(originalConstraint, partition);
				}
			}
		}
		return categoryMap;
	}

	@Override
	public void clear(boolean clearCache) {
		if (clearCache) {
			cache.invalidateAll();
		}
	}
}