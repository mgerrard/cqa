package name.filieri.antonio.pcp.visitors;

import java.util.ArrayList;
import java.util.List;

import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class NegativeNormalFormVisitor implements ExpressionVisitor<Expression> {

	private boolean negate = false;

	@Override
	public Expression visit(StringBinaryExpression binaryStringExpression) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public Expression visit(BooleanConstant booleanConstant) {
		if (negate) {
			negate = false;
			return booleanConstant.negate();
		} else {
			return booleanConstant;
		}
	}

	@Override
	public Expression visit(BooleanNaryExpression expr) {
		if (negate) {
			negate = false;
			expr = (BooleanNaryExpression) expr.negate();
		}

		List<BooleanExpression> nnfArgs = new ArrayList<>();
		for (BooleanExpression arg : expr.getBooleanArgs()) {
			nnfArgs.add((BooleanExpression) arg.accept(this));
		}
		return new BooleanNaryExpression(expr.getFunction(), nnfArgs, expr.getPositionInfo());
	}

	@Override
	public Expression visit(NotExpression notExpression) {
		if (negate) { // double negation
			negate = false;
		} else {
			negate = true;
		}
		return notExpression.getArg().accept(this);
	}

	@Override
	public Expression visit(NumericBinaryExpression numericBinaryExpression) {
		return numericBinaryExpression;
	}

	@Override
	public Expression visit(NumericComparisonExpression numericComparisonExpression) {
		if (negate) {
			negate = false;
			return numericComparisonExpression.negate();
		} else {
			return numericComparisonExpression;
		}
	}

	@Override
	public Expression visit(NumericConstant numericConstant) {
		return numericConstant;
	}

	@Override
	public Expression visit(NumericUnaryExpression numericUnaryExpression) {
		return numericUnaryExpression;
	}

	@Override
	public Expression visit(NormalVariable normalVariable) {
		return normalVariable;
	}

	@Override
	public Expression visit(StringConstant stringConstant) {
		return stringConstant;
	}

	@Override
	public Expression visit(BernoulliVariable bernoulliVariable) {
		return bernoulliVariable;
	}

	@Override
	public Expression visit(UniformFloatVariable uniformFloatVariable) {
		return uniformFloatVariable;
	}

	@Override
	public Expression visit(UniformDoubleVariable uniformDoubleVariable) {
		return uniformDoubleVariable;
	}

	@Override
	public Expression visit(UniformIntegerVariable uniformIntegerVariable) {
		return uniformIntegerVariable;
	}

	@Override
	public Expression visit(UniformLongVariable uniformLongVariable) {
		return uniformLongVariable;
	}

	@Override
	public Expression visit(StringVariable stringVariable) {
		return stringVariable;
	}

	@Override
	public Expression visit(DependentVariable dependentVariable) {
		return dependentVariable;
	}

	@Override
	public Expression visit(BitVectorConstant bitVectorConstant) {
		return bitVectorConstant;
	}

	@Override
	public Expression visit(BitVectorBinaryExpression bitVectorBinaryExpression) {
		return bitVectorBinaryExpression;
	}

	@Override
	public Expression visit(BitVectorExtractExpression bitVectorExtractExpression) {
		return bitVectorExtractExpression;
	}

	@Override
	public Expression visit(BitVectorCompareExpression bitVectorCompareExpression) {
		if (negate) {
			negate = false;
			bitVectorCompareExpression = bitVectorCompareExpression.negate();
		}
		return bitVectorCompareExpression;
	}

	@Override
	public Expression visit(BitVectorUnaryExpression bitVectorUnaryExpression) {
		return bitVectorUnaryExpression;
	}

	@Override
	public Expression visit(BitVectorVariable bitVectorVariable) {
		return bitVectorVariable;
	}
}