package name.filieri.antonio.pcp.simplifier;

import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;

public interface Simplifier {

  public BooleanExpression simplify(BooleanExpression formula);
}
