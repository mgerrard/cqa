package name.filieri.antonio.pcp.counters;

import com.google.common.collect.ImmutableList;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Global;
import com.microsoft.z3.Solver;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.ExactCountResult;
import name.filieri.antonio.pcp.Options;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.counters.ModelCounterUtils.Z3VarMapping;

public class SharpSatCaller implements ModelCounterCaller {

	private final String sharpSatPath;
	private final Options options;
	private final static int SIGFPE = 136;

	public SharpSatCaller(Options options) {
		this.options = options;
		this.sharpSatPath = options.getSharpSatPath();
	}

	@Override
	public Map<Constraint, CountResult> count(Collection<Constraint> problems) throws ModelCounterException {
		Map<Constraint, CountResult> results = new LinkedHashMap<>();
		for (Constraint problem : problems) {
      CountResult r;
      //TODO this should be higher up (before choosing a counter)
      if (noFreeVars(problem)) {
        r = new ExactCountResult(BigRational.ONE.div(problem.getDomainSize()));
      } else {
        Path cnfFile = prepareCNF(problem);
        BigRational count = runToolAndExtract(cnfFile);
        cnfFile.toFile().delete();
        r = new ExactCountResult(count.div(problem.getDomainSize()));
      }
			results.put(problem, r);
		}
		return results;
	}

	public Path prepareCNF(Constraint problem) {
		Context ctx = new Context();
		Solver solver = ctx.mkSolver();
		Global.setParameter("rewriter.hi_fp_unspecified","true");
		Z3VarMapping varMap = ModelCounterUtils.declareVars(problem.getVariables(), ctx);
		List<BoolExpr> constraintExprs = ModelCounterUtils
				.collectZ3Constraints(problem, ctx, solver, varMap);
		List<BoolExpr> formula = new ArrayList<>();
		formula.addAll(varMap.domain);
		formula.addAll(constraintExprs);

		// bitblast
		Z3Bitblaster bitblaster = new Z3Bitblaster(ctx);
		String dimacs = bitblaster.generateDimacs(formula);

		try {
			Path tmpFile = Files.createTempFile("sharp", ".cnf");
			Files.write(tmpFile, ImmutableList.of(dimacs));
			return tmpFile;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private BigRational runToolAndExtract(Path cnfFile) {
		ProcessBuilder pb = new ProcessBuilder(sharpSatPath, cnfFile.toAbsolutePath().toString());
		try {
			Process process = pb.start();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			String line = null;

			while (process.isAlive()) {
				try {
					process.waitFor();
				} catch (InterruptedException e) {
				}
			}

			// workaround for division by 0 in the projection patch
			if (process.exitValue() == SIGFPE) {
				return BigRational.ONE;
			} else {
				while ((line = reader.readLine()) != null) {
					// uncomment this to see what sharpSAT is printing
          System.out.println("[sharpsat-output] " + line);
					if (line.startsWith("# solutions")) {
						String solutionLine = reader.readLine();
						System.out.println(solutionLine);
						return BigRational.valueOf(solutionLine);
					}
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		throw new RuntimeException("Solution line not found in the output of sharpSAT!");
	}


  private boolean noFreeVars(Constraint problem) {
    Set<Variable> freeVars = new HashSet<>();
    freeVars.addAll(problem.getVariables());
    BooleanExpression constraint = problem.getExpr();

    if (!(constraint instanceof BooleanNaryExpression)) {
      return false;
    }
    if (((BooleanNaryExpression) constraint).getFunction() != Function.AND) {
      return false;
    }

    for (Expression expr : ((BooleanNaryExpression) constraint).getBooleanArgs()) {
      if (expr instanceof NumericComparisonExpression) {
        NumericComparisonExpression cmp = (NumericComparisonExpression) expr;
        if (cmp.getFunction() == NumericComparisonExpression.Function.EQ) {
          Variable var;
          boolean constant;
          if (cmp.getLeft() instanceof Variable) {
            var = (Variable) cmp.getLeft();
            constant = cmp.getRight() instanceof NumericConstant;
          } else if (cmp.getRight() instanceof Variable) {
            var = (Variable) cmp.getRight();
            constant = cmp.getLeft() instanceof NumericConstant;
          } else {
            return false;
          }

          if (constant) {
            freeVars.remove(var);
          }
        }
      }
    }

    return freeVars.isEmpty();
  }
}
