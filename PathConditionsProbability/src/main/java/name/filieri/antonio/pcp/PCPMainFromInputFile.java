package name.filieri.antonio.pcp;

import java.io.IOException;
import java.util.List;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class PCPMainFromInputFile {

  public static void main(String[] args) throws IOException {
    Options options = Options.loadFromUserHomeOrDefault();
    //Scanner sc = new Scanner(new File(args[0]), "UTF-8");
    SymbolTable stable = new SymbolTable();
    Partitioner partitioner = new ConstraintPartitioner();
    CountDispatcher dispatcher = new PCPDispatcher(options);
    CommandEvaluator eval = new CommandEvaluator(stable, partitioner, dispatcher, options);

    ANTLRInputStream input = new ANTLRFileStream(args[0]);
    InputFormatLexer lexer = new InputFormatLexer(input);
    InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

    ParseTree tree = parser.input();
    List<? extends Command> commands = AstBuilder.parseCommands(stable, tree);

    for (Command command : commands) {
      Reply reply = command.accept(eval);
      if (reply.error) {
        System.out.println("(error \"" + reply.message + "\")");
      } else {
        if (command instanceof CountCommand) {
          CountResult result = reply.result;
          System.out.println("(" + result + ")");
        }
      }
    }
  }
}
