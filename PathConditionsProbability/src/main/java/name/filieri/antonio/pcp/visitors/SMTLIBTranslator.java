package name.filieri.antonio.pcp.visitors;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class SMTLIBTranslator extends ExpressionBaseVisitor<Void> {

  private final Set<Variable> declaredVariables;
  private final StringBuilder smtFormulas;

  public SMTLIBTranslator() {
    this.declaredVariables = new HashSet<>();
    this.smtFormulas = new StringBuilder();
  }

  public String getTranslatedFunction(String wrapperName) {
    StringBuilder sb = new StringBuilder();
    sb.append("(define-fun " + wrapperName + " () Bool \n");
    sb.append("  (and");
    for (Variable var : declaredVariables) {
      if (var instanceof BoundedVariable) {
        BoundedVariable bvar = (BoundedVariable) var;
        sb.append("\t(<= " + var.getName() + " " + bvar.getUpperBound() + ")\n");
        sb.append("\t(>= " + var.getName() + " " + bvar.getLowerBound() + ")\n");
      }
    }
    sb.append("\t");
    sb.append(smtFormulas.toString());
    sb.append("))\n");
    return sb.toString();
  }

  public String getVariableDeclarations() {
    StringBuilder sb = new StringBuilder();
    for (Variable var : declaredVariables) {
      sb.append("(declare-const ");
      sb.append(var.getName());
      sb.append(" ");

      switch (var.getType()) {
        case BOOLEAN:
          sb.append("Bool");
          break;
        case INT:
        case LONG:
          sb.append("Int");
          break;
        case FLOAT:
        case DOUBLE:
        case STRING:
        case BV:
        case DATA:
          throw new RuntimeException("Unsupported data type: " + var.getType());
      }
      sb.append(")\n");
    }
    return sb.toString();
  }

  public String getFunction() {
    return smtFormulas.toString();
  }

  @Override
  public Void emptyResult() {
    throw new RuntimeException("Not implemented - check trace!");
  }

  @Override
  public Void mergeResults(List<Void> childResults) {
    throw new RuntimeException("Not implemented - check trace");
  }


  @Override
  public Void visit(BooleanConstant booleanConstant) {
    smtFormulas.append(booleanConstant.getValue());
    return null;
  }

  @Override
  public Void visit(BooleanNaryExpression expr) {
    switch (expr.getFunction()) {
      case AND:
        smtFormulas.append("(and");
        break;
      case OR:
        smtFormulas.append("(or");
        break;
      default:
        throw new RuntimeException("Missing case: " + expr.getFunction());
    }
    for (Expression arg : expr.getArgs()) {
      smtFormulas.append("\n\t");
      arg.accept(this);
    }
    smtFormulas.append(")");
    return null;
  }

  @Override
  public Void visit(NotExpression expr) {
    smtFormulas.append("(not ");
    expr.getArg().accept(this);
    smtFormulas.append(")");
    return null;
  }

  @Override
  public Void visit(NumericBinaryExpression expr) {
    smtFormulas.append('(');
    switch (expr.getFunction()) {
      case ADD:
        smtFormulas.append("+");
        break;
      case SUB:
        smtFormulas.append("-");
        break;
      case MUL:
        smtFormulas.append("*");
        break;
      case DIV:
        smtFormulas.append("/");
        break;
      case MOD:
        smtFormulas.append("%");
        break;
      case POW:
        smtFormulas.append("pow"); //TODO add declarations for the functions below
        break;
      case MAX:
        smtFormulas.append("max");
        break;
      case MIN:
        smtFormulas.append("min");
        break;
      case ATAN2:
        throw new RuntimeException("Unsupported: ATAN2");
    }
    smtFormulas.append(" ");
    expr.getLeft().accept(this);
    smtFormulas.append(" ");
    expr.getRight().accept(this);
    smtFormulas.append(')');
    return null;
  }

  @Override
  public Void visit(NumericComparisonExpression expr) {
    smtFormulas.append('(');
    boolean isNeg = false;
    switch (expr.getFunction()) {
      case NE:
        smtFormulas.append("not (=");
        isNeg = true;
        break;
      case LE:
        smtFormulas.append("<=");
        break;
      case LT:
        smtFormulas.append("<");
        break;
      case EQ:
        smtFormulas.append("=");
        break;
      case GT:
        smtFormulas.append(">");
        break;
      case GE:
        smtFormulas.append(">=");
        break;
    }

    smtFormulas.append(" ");
    expr.getLeft().accept(this);
    smtFormulas.append(" ");
    expr.getRight().accept(this);
    if (isNeg) {
      smtFormulas.append(')');
    }
    smtFormulas.append(')');
    return null;
  }

  @Override
  public Void visit(NumericUnaryExpression expr) {
    throw new RuntimeException("Not implemented - most of the stuff here are transcendental functions");
  }

  @Override
  public Void visit(NumericConstant constant) {
    smtFormulas.append(constant.getValue());
    return null;
  }

  @Override
  public Void visit(StringConstant stringConstant) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(NormalVariable normalVariable) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(BernoulliVariable bernoulliVariable) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(UniformFloatVariable uniformFloatVariable) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(UniformDoubleVariable uniformDoubleVariable) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(UniformIntegerVariable intVar) {
    declaredVariables.add(intVar);
    smtFormulas.append(intVar.getName());
    return null;
  }

  @Override
  public Void visit(UniformLongVariable longVar) {
    declaredVariables.add(longVar);
    smtFormulas.append(longVar.getName());
    return null;
  }

  @Override
  public Void visit(StringVariable stringVariable) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(StringBinaryExpression expr) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(DependentVariable depVar) {
    declaredVariables.add(depVar);
    smtFormulas.append(depVar.getName());
    return null;
  }

  @Override
  public Void visit(BitVectorConstant bitVectorConstant) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(BitVectorBinaryExpression bv) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(BitVectorExtractExpression bv) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(BitVectorCompareExpression bv) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(BitVectorUnaryExpression bv) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Void visit(BitVectorVariable bv) {
    throw new RuntimeException("Not implemented");
  }
}
