package name.filieri.antonio.pcp.simplifier;

import com.google.common.base.Verify;
import com.microsoft.z3.ApplyResult;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Goal;
import com.microsoft.z3.Native;
import com.microsoft.z3.Tactic;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.visitors.Z3ToPCPTranslator;
import name.filieri.antonio.pcp.visitors.Z3Translator;

public class Z3Simplifier implements Simplifier {

  public Z3Simplifier() {
  }

  @Override
  public BooleanExpression simplify(BooleanExpression clauses) {
    Context ctx = new Context();
    Z3Translator z3Translator = new Z3Translator(ctx);

    BoolExpr z3formula = z3Translator.translate(clauses);
    Goal g = ctx.mkGoal(false, false, false);
    g.add(z3formula);
    System.out.println("[z3-simplifier] input formula: \n" + z3formula);

    Tactic t = ctx.then(ctx.mkTactic("simplify"), ctx.mkTactic("propagate-values"),
        ctx.mkTactic("ctx-solver-simplify"));
    ApplyResult ar = t.apply(g);
    Verify.verify(ar.getNumSubgoals() == 1);
    BoolExpr[] simplifiedClauses = ar.getSubgoals()[0].getFormulas();

    Z3ToPCPTranslator pcpTranslator = new Z3ToPCPTranslator(
        z3Translator.getDeclaredVariables().inverse());
    BooleanExpression result = pcpTranslator.translate(simplifiedClauses);

    ctx.close();
    ctx = null;
    Native.resetMemory();
    System.gc();

    return result;
  }
}
