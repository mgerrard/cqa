package name.filieri.antonio.pcp;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory.Kind;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.counters.BarvinokCaller;
import name.filieri.antonio.pcp.counters.ModelCounterCaller;
import name.filieri.antonio.pcp.counters.ModelCounterException;
import name.filieri.antonio.pcp.counters.QCoralCaller;
import name.filieri.antonio.pcp.counters.SharpSatCaller;
import name.filieri.antonio.pcp.counters.Z3BlockingCaller;

public class PCPDispatcherAsync implements CountDispatcher {

  private static final int DEFAULT_CACHE_SIZE = 10000;
  private final Options options;
  private final Cache<Constraint, CountResult> cache;
  private BarvinokCaller bvCaller;
  private QCoralCaller qcoralCaller;
  private SharpSatCaller sharpsatCaller;
  private Z3BlockingCaller z3Caller;

  public PCPDispatcherAsync(Options options) {
    this.cache = CacheBuilder.newBuilder()
        .maximumSize(DEFAULT_CACHE_SIZE)
        .recordStats()
        .build();
    this.options = options;
  }

  @Override
  public CountResult count(Constraint constraint) {
    throw new RuntimeException("Not implemented yet");
  }

  @Override
  public Map<Constraint, CountResult> count(Multimap<Constraint, Constraint> partitionMap) {
    Map<ExpressionCategory, Multimap<Constraint, Constraint>> categoryMap = classify(partitionMap);
    final Map<Constraint, CountResult> partitionResults = new ConcurrentHashMap<>();

    Consumer<Optional<Map<Constraint, CountResult>>> addToPartition = new Consumer<Optional<Map<Constraint, CountResult>>>() {
      @Override
      public void accept(Optional<Map<Constraint, CountResult>> constraintCountResultMap) {
        if (constraintCountResultMap.isPresent()) {
          partitionResults.putAll(constraintCountResultMap.get());
        }
      }
    };

    Set<CompletableFuture> countings = Sets.newHashSet();

    for (ExpressionCategory cat : categoryMap.keySet()) {
      Multimap<Constraint, Constraint> categoryConstraints = categoryMap.get(cat);

      Kind kind = cat.category;
      if (kind == Kind.LINEAR) {
        loop:
        for (Constraint c : categoryConstraints.values()) {
          for (Variable v : c.getVariables()) {
            if (v.getType() != ExpressionType.INT) {
              kind = Kind.NONLINEAR;
              System.out.println(
                  "[pcpDispatcher] found non-integer variable in linear constraint: moving to non-linear...");
              break loop;
            }
          }
        }
      }

      switch (kind) {
        case DATASTRUCTURE:
          throw new RuntimeException("Not supported");
        case LINEAR:
          countings.add(CompletableFuture.supplyAsync(toSupplier(this::handleLinear, categoryConstraints))
              .thenAcceptAsync(addToPartition));
          break;
        case NONLINEAR:
          countings.add(CompletableFuture.supplyAsync(toSupplier(this::handleNonLinear, categoryConstraints))
              .thenAcceptAsync(addToPartition));
          break;
        case STRING:
          countings.add(CompletableFuture.supplyAsync(toSupplier(this::handlePureString, categoryConstraints))
              .thenAcceptAsync(addToPartition));
          break;
        case STRING_NUMERIC:
          countings.add(CompletableFuture.supplyAsync(toSupplier(this::handleMixedStringNumeric, categoryConstraints))
              .thenAcceptAsync(addToPartition));
          break;
        case BITVECTOR:
          countings.add(CompletableFuture.supplyAsync(toSupplier(this::handleBitVector, categoryConstraints))
              .thenAcceptAsync(addToPartition));
          break;
        default:
          throw new RuntimeException("Not implemented yet: " + cat.category);
      }
    }
    for(CompletableFuture cf : countings){
      try {
        cf.get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    }
    return partitionResults;
  }


  public static <T, R> Supplier<R> toSupplier(Function<T, R> fn, T val) {
    return () -> fn.apply(val);
  }

  private Optional<Map<Constraint, CountResult>> handleBitVector(
      Multimap<Constraint, Constraint> consToPartitions) {
    Map<Constraint, CountResult> counts = new HashMap<>();
    Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
    Set<Constraint> uncountedProblems = new LinkedHashSet<>();

    for (Constraint problem : problems) {
      CountResult problemCount = cache.getIfPresent(problem);
      if (problemCount != null) {
//        System.out.println("[pcpdispatcher] cache hit!");
        counts.put(problem, problemCount);
      } else {
        uncountedProblems.add(problem);
      }
    }

    ModelCounterCaller caller;

    //TODO: make first served first returned

    if (options.useSharpSATForNonlinear()) {
      if (sharpsatCaller == null) {
        sharpsatCaller = new SharpSatCaller(options);
      }
      caller = sharpsatCaller;
    } else if (options.useZ3ForNonlinear()) {
      if (z3Caller == null) {
        z3Caller = new Z3BlockingCaller(options);
      }
      caller = z3Caller;
    } else {
      throw new RuntimeException("No model counter for bitvector constraints was chosen!");
    }

    if (!uncountedProblems.isEmpty()) {
      Map<Constraint, CountResult> missingCounts = null;
      try {
        missingCounts = caller.count(uncountedProblems);
      } catch (ModelCounterException e) {
        System.err.println("Error while handling bitvector expression: " + e.getMessage());
        return Optional.empty();
      }
      cache.putAll(missingCounts);
      counts.putAll(missingCounts);
    }

    return Optional.of(counts);
  }

  private Optional<Map<Constraint, CountResult>> handleMixedStringNumeric(
      Multimap<Constraint, Constraint> categoryConstraints) {
    throw new RuntimeException("Not implemented");
  }

  private Optional<Map<Constraint, CountResult>> handlePureString(
      Multimap<Constraint, Constraint> consToPartitions) {
    throw new RuntimeException("Not implemented");
  }

  private Optional<Map<Constraint, CountResult>> handleNonLinear(
      Multimap<Constraint, Constraint> consToPartitions) {
    Map<Constraint, CountResult> counts = new HashMap<>();
    Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
    Set<Constraint> uncountedProblems = new LinkedHashSet<>();

    for (Constraint problem : problems) {
      CountResult problemCount = cache.getIfPresent(problem);
      if (problemCount != null) {
//        System.out.println("[pcpdispatcher] cache hit!");
        counts.put(problem, problemCount);
      } else {
        uncountedProblems.add(problem);
      }
    }

    //TODO: make first come first returned

    ModelCounterCaller caller;

    if (options.useQCoralForNonlinear()) {
      if (qcoralCaller == null) {
        qcoralCaller = new QCoralCaller(options);
      }
      caller = qcoralCaller;
    } else if (options.useSharpSATForNonlinear()) {
      if (sharpsatCaller == null) {
        sharpsatCaller = new SharpSatCaller(options);
      }
      caller = sharpsatCaller;
    } else if (options.useZ3ForNonlinear()) {
      if (z3Caller == null) {
        z3Caller = new Z3BlockingCaller(options);
      }
      caller = z3Caller;
    } else {
      throw new RuntimeException("No model counter for nonlinear constraints was chosen!");
    }

    if (!uncountedProblems.isEmpty()) {
      Map<Constraint, CountResult> missingCounts = null;
      try {
        missingCounts = caller.count(uncountedProblems);
      } catch (ModelCounterException e) {
        System.err.println("Error while handling nonlinear expression: " + e.getMessage());
        return Optional.empty();
      }
      cache.putAll(missingCounts);
      counts.putAll(missingCounts);
    }

    return Optional.of(counts);
  }

  private Optional<Map<Constraint, CountResult>> handleLinear(
      Multimap<Constraint, Constraint> consToPartitions) {
    Map<Constraint, CountResult> counts = new HashMap<>();
    Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
    Set<Constraint> uncountedProblems = new LinkedHashSet<>();

    for (Constraint problem : problems) {
      CountResult problemCount = cache.getIfPresent(problem);
      if (problemCount != null) {
//        System.out.println("[pcpdispatcher] cache hit!");
        counts.put(problem, problemCount);
      } else {
        uncountedProblems.add(problem);
      }
    }

    if (bvCaller == null) { // initialize barvinok lazily
      bvCaller = new BarvinokCaller(options);
    }

    if (!uncountedProblems.isEmpty()) {
      Map<Constraint, CountResult> missingCounts = null;
      try {
        missingCounts = bvCaller.count(uncountedProblems);
      } catch (ModelCounterException e) {
        System.err.println("Error while handling linear expression: " + e.getMessage());
        return Optional.empty();
      }
      cache.putAll(missingCounts);
      counts.putAll(missingCounts);
    }

    return Optional.of(counts);
  }

  // group entries by constraint type
  private Map<ExpressionCategory, Multimap<Constraint, Constraint>> classify(
      Multimap<Constraint, Constraint> partitionMap) {
    Map<ExpressionCategory, Multimap<Constraint, Constraint>> categoryMap = new HashMap<>();

    for (Map.Entry<Constraint, Constraint> entry : partitionMap.entries()) {
      Constraint originalConstraint = entry.getKey();
      Constraint partition = entry.getValue();

      Set<ExpressionCategory> pcats = partition.getCategory();
      Preconditions.checkState(pcats.size() > 0);

      // FIXME temporary restriction until I figure out how to handle
      // multiple kinds of constraints at once
      if (pcats.size() > 1) {
        throw new RuntimeException(
            "Found constraint with multiple types. Still need to figure how to handle this :P");
      }
      for (ExpressionCategory pcat : pcats) {
        if (pcat.isConstant) {
          System.out.println("[pcp:dispatcher] Found constant expression, ignoring it...");
        } else {
          Multimap<Constraint, Constraint> pcatMap = categoryMap.get(pcat);
          if (pcatMap == null) {
            pcatMap = LinkedHashMultimap.create();
            categoryMap.put(pcat, pcatMap);
          }

          pcatMap.put(originalConstraint, partition);
        }
      }
    }
    return categoryMap;
  }

  @Override
  public void clear(boolean clearCache) {
    if (clearCache) {
      cache.invalidateAll();
    }
  }
}