package name.filieri.antonio.pcp.utils;

import java.util.Map;

import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;

public interface Cacher {
    enum Solver {
        BARVINOK("BARVINOK"),
        QCORAL("QCORAL"),
        SHARPSAT("SHARPSAT"),
        Z3BLOCKING("Z3BLOCKING");

        private final String representation;

        Solver(String representation) {
            this.representation = representation;
        }

        @Override
        public String toString() {
            return representation;
        }
    }

    CountResult getIfPresent(Constraint problem);

    CountResult getIfPresent(Constraint problem, Solver solver);

    void put(Constraint constraint, CountResult result, Solver solver);

    default void putAll(Map<Constraint, CountResult> results, Solver solver) {
        for (Constraint constraint : results.keySet()) {
            put(constraint, results.get(constraint), solver);
        }
    }

    void invalidateAll();
}
