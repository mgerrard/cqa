package name.filieri.antonio.pcp;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Multimap;

public interface Partitioner {

	public void computeVarClusters(Set<Constraint> constraints);
	
	public List<Constraint> partition(Constraint constraint);

	public Multimap<Constraint, Constraint> partition(Collection<Constraint> constraints);

	public CountResult mergePartitionResults(Collection<CountResult> partialResults);

	public void clear();
	
}
