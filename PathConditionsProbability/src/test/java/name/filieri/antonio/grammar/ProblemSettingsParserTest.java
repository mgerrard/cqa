package name.filieri.antonio.grammar;



import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.junit.Test;

import org.junit.Assert;
import name.filieri.antonio.jpf.domain.Domain;
import name.filieri.antonio.jpf.domain.ProblemSetting;
import name.filieri.antonio.jpf.domain.UsageProfile;
import name.filieri.antonio.jpf.domain.exceptions.InvalidUsageProfileException;
import name.filieri.antonio.jpf.grammar.ProblemSettingsLexer;
import name.filieri.antonio.jpf.grammar.ProblemSettingsParser;

public class ProblemSettingsParserTest {

	@Test
	public void testUPComplete() throws InvalidUsageProfileException {
		StringBuilder input = new StringBuilder();
		
		input.append("domain{\n");
		input.append("	x_1_SYMINT: -10,9;\n");
		input.append("	y_2_SYMINT: -10,9;\n");
		input.append("};\n\n");

		input.append("usageProfile{\n");
		input.append("	x_1_SYMINT>=-10 && y_2_SYMINT>=-10  : 100/100;\n");
		input.append("};\n");
		
		
		ProblemSettingsLexer psLexer = new ProblemSettingsLexer(new ANTLRInputStream(input.toString()));
		TokenStream psTokenStream = new CommonTokenStream(psLexer);
		ProblemSettingsParser psParser = new ProblemSettingsParser(psTokenStream);
		ProblemSetting output = psParser.problemSettings().ps;

		
		Domain.Builder domainBuilder = new Domain.Builder();
		domainBuilder.addVariable("x_1_SYMINT", -10, 9);
		domainBuilder.addVariable("y_2_SYMINT", -10, 9);
		Domain domain = domainBuilder.build();

		UsageProfile.Builder usageProfileBuilder = new UsageProfile.Builder();
		usageProfileBuilder.addScenario("x_1_SYMINT>=-10 && y_2_SYMINT>=-10", 1);
		UsageProfile usageProfile = usageProfileBuilder.build();
		
		
		ProblemSetting correctResult = new ProblemSetting(domain, usageProfile);
		
		Assert.assertEquals(correctResult.toString(), output.toString());
	}

}
