package name.filieri.antonio.jpf.analysis;

import java.util.Set;

import name.filieri.antonio.jpf.analysis.exceptions.AnalysisException;
import name.filieri.antonio.jpf.analysis.exceptions.EmptyDomainException;
import name.filieri.antonio.jpf.domain.Domain;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.domain.UsageProfile;
import name.filieri.antonio.jpf.domain.exceptions.InvalidUsageProfileException;
import name.filieri.antonio.jpf.latte.LatteException;
import name.filieri.antonio.jpf.omega.exceptions.OmegaException;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.jpf.utils.Configuration;

public class SequentialAnalyzerTest {

	//@Test
	public void testRestriction() throws LatteException, InterruptedException, OmegaException, AnalysisException, EmptyDomainException, InvalidUsageProfileException {
		
		Domain.Builder domainBuilder = new Domain.Builder();
		domainBuilder.addVariable("x", 1, 10);
		Domain domain = domainBuilder.build();
		
		UsageProfile.Builder usageProfileBuilder = new UsageProfile.Builder();
		
		usageProfileBuilder.addScenario("x>=0", BigRational.valueOf("100/100"));
		//usageProfileBuilder.addScenario("x<=5", BigRational.valueOf("5/100"));
		
		UsageProfile usageProfile = usageProfileBuilder.build();
		
		Configuration configuration = new Configuration();
		
		SequentialAnalyzer analyzer = new SequentialAnalyzer(configuration, domain, usageProfile, 1);
		
		
		BigRational probabilityOf = analyzer.analyzeSpfPC("x>9");
		
		System.out.println(probabilityOf+"\t"+probabilityOf.doubleValue());
		
		System.out.println(usageProfile);
		Set<Problem> newDomain = analyzer.excludeFromDomain("x=8");
		System.out.println(newDomain);
		probabilityOf = analyzer.analyzeSpfPC("x>9");
		
		System.out.println(probabilityOf+"\t"+probabilityOf.doubleValue());
	}

}
