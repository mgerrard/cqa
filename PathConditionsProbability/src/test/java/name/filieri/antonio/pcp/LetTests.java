package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.utils.LineByLineParserIterator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.junit.Test;

public class LetTests {

  @Test
  public void testLet() throws IOException {
    BufferedReader br = new BufferedReader(
        new FileReader(new File("src/test/inputs/grammar/let.pcp")));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, null);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    //process defvars
    Command command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);

    AssertCommand assertCmd = (AssertCommand) llpi.next().get(0);
    NumericComparisonExpression bExpr = (NumericComparisonExpression) assertCmd.getExprs().get(0);

    assertEquals(Function.GT, bExpr.getFunction());
    NumericBinaryExpression left = (NumericBinaryExpression) bExpr.getLeft();
    NumericBinaryExpression right = (NumericBinaryExpression) bExpr.getRight();

    assertEquals(NumericBinaryExpression.Function.ADD, left.getFunction());
    assertEquals(NumericBinaryExpression.Function.ADD, right.getFunction());
    assertEquals("x1", ((UniformIntegerVariable) left.getLeft()).getName());
    assertEquals("x2", ((UniformIntegerVariable) left.getRight()).getName());

    assertEquals("x2", ((UniformIntegerVariable) right.getLeft()).getName());
    assertEquals(42, ((NumericConstant) right.getRight()).getValue());
  }

  @Test
  public void testNestedLet() throws IOException {
    BufferedReader br = new BufferedReader(
        new FileReader(new File("src/test/inputs/grammar/nestedLet.pcp")));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, null);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    //process defvars
    Command command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);

    AssertCommand assertCmd = (AssertCommand) llpi.next().get(0);
    NumericComparisonExpression bExpr = (NumericComparisonExpression) assertCmd.getExprs().get(0);

    assertEquals(Function.GT, bExpr.getFunction());
    NumericBinaryExpression left = (NumericBinaryExpression) bExpr.getLeft();
    NumericBinaryExpression right = (NumericBinaryExpression) bExpr.getRight();

    assertEquals(NumericBinaryExpression.Function.ADD, left.getFunction());
    assertEquals(NumericBinaryExpression.Function.ADD, right.getFunction());
    assertEquals("x1", ((UniformIntegerVariable) left.getLeft()).getName());
    assertEquals("x2", ((UniformIntegerVariable) left.getRight()).getName());

    assertEquals("x2", ((UniformIntegerVariable) right.getLeft()).getName());
    assertEquals(42, ((NumericConstant) right.getRight()).getValue());
  }

  @Test
  public void testShadowing() throws IOException {
    BufferedReader br = new BufferedReader(
        new FileReader(new File("src/test/inputs/grammar/shadowLet.pcp")));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, null);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    //process defvars
    Command command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);
    command = llpi.next().get(0);
    eval.visit(command);

    AssertCommand assertCmd = (AssertCommand) llpi.next().get(0);
    NumericComparisonExpression bExpr = (NumericComparisonExpression) assertCmd.getExprs().get(0);

    assertEquals(Function.GT, bExpr.getFunction());
    NumericBinaryExpression left = (NumericBinaryExpression) bExpr.getLeft();
    NumericBinaryExpression right = (NumericBinaryExpression) bExpr.getRight();

    assertEquals(NumericBinaryExpression.Function.ADD, left.getFunction());
    assertEquals(NumericBinaryExpression.Function.ADD, right.getFunction());
    assertEquals("x1", ((UniformIntegerVariable) left.getLeft()).getName());
    assertEquals("x2", ((UniformIntegerVariable) left.getRight()).getName());

    assertEquals("x2", ((UniformIntegerVariable) right.getLeft()).getName());
    assertEquals(42, ((NumericConstant) right.getRight()).getValue());
  }
}
