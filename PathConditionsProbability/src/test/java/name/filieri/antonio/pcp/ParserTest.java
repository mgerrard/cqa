package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.ClearCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.ast.commands.DeclareConstantCommand;
import name.filieri.antonio.pcp.ast.commands.DependentVariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.commands.SetOptionCommand;
import name.filieri.antonio.pcp.ast.commands.VariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;
import name.filieri.antonio.pcp.utils.LineByLineParserIterator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.junit.Test;

public class ParserTest {

  @Test
  public void testParsing() throws IOException {
    BufferedReader br = new BufferedReader(
        new FileReader(new File("src/test/java/name/filieri/antonio/pcp/input.pcp")));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, null);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    Command command = llpi.next().get(0);
    assertTrue(command instanceof SetOptionCommand);
    assertEquals(((SetOptionCommand) command).getName(), ":seed");
    assertEquals(((SetOptionCommand) command).getValue().numericResult, 121314);
    command = llpi.next().get(0);

    assertTrue(command instanceof SetOptionCommand);
    assertEquals(((SetOptionCommand) command).getName(), ":linear-counter");
    assertEquals(((SetOptionCommand) command).getValue().stringResult, "barvinok");
    command = llpi.next().get(0);

    assertTrue(command instanceof SetOptionCommand);
    assertEquals(((SetOptionCommand) command).getName(), ":non-linear-counter");
    assertEquals(((SetOptionCommand) command).getValue().stringResult, "qcoral");
    command = llpi.next().get(0);

    assertTrue(command instanceof SetOptionCommand);
    assertEquals(((SetOptionCommand) command).getName(), ":partitioning");
    assertEquals(((SetOptionCommand) command).getValue().numericResult, 1);
    command = llpi.next().get(0);
    eval.visit(command);

    assertTrue(command instanceof DeclareConstantCommand);
    assertEquals(((DeclareConstantCommand) command).getName(), "pi");
    assertEquals(((DeclareConstantCommand) command).getType(), ExpressionType.FLOAT);
    assertEquals(((DeclareConstantCommand) command).getValue(), NumericConstant.valueOf(3.1415f));
    command = llpi.next().get(0);
    eval.visit(command);

    assertTrue(command instanceof VariableDeclarationCommand);
    Variable x1 = ((VariableDeclarationCommand) command).getVar();
    assertEquals(x1.getType(), ExpressionType.INT);
    assertEquals(x1.getName(), "x1");
    assertEquals(((NumericBoundedVariable) x1).getLowerBound(), -10);
    assertEquals(((NumericBoundedVariable) x1).getUpperBound(), 10);
    command = llpi.next().get(0);
    eval.visit(command);

    assertTrue(command instanceof VariableDeclarationCommand);
    Variable x2 = ((VariableDeclarationCommand) command).getVar();
    assertEquals(x2.getType(), ExpressionType.LONG);
    assertEquals(x2.getName(), "x2");
    assertEquals(((NumericBoundedVariable) x2).getLowerBound(), 13l);
    assertEquals(((NumericBoundedVariable) x2).getUpperBound(), 15l);
    command = llpi.next().get(0);
    eval.visit(command);

    assertTrue(command instanceof VariableDeclarationCommand);
    Variable x3 = ((VariableDeclarationCommand) command).getVar();
    assertEquals(x3.getType(), ExpressionType.FLOAT);
    assertEquals(x3.getName(), "x3");
    assertEquals(((NumericBoundedVariable) x3).getLowerBound(), 3.1415f);
    assertEquals(((NumericBoundedVariable) x3).getUpperBound(), 2 * 3.1415f);
    command = llpi.next().get(0);
    eval.visit(command);

    assertTrue(command instanceof VariableDeclarationCommand);
    Variable x4 = ((VariableDeclarationCommand) command).getVar();
    assertEquals(x4.getType(), ExpressionType.STRING);
    assertEquals(x4.getName(), "x4");
    assertEquals(((StringVariable) x4).getDomain(), "BLABLA");
    llpi.next(); // skip comment
    command = llpi.next().get(0);

    assertTrue(command instanceof AssertCommand);
    List<BooleanExpression> s1 = ((AssertCommand) command).getExprs();
    assertEquals(((NumericComparisonExpression) s1.get(0)).getFunction(),
        NumericComparisonExpression.Function.GT);
    assertEquals(((NumericComparisonExpression) s1.get(0)).getLeft(), x1);
    assertEquals(((NumericComparisonExpression) s1.get(0)).getRight(), x2);
    command = llpi.next().get(0);

    assertTrue(command instanceof AssertCommand);
    List<BooleanExpression> s2 = ((AssertCommand) command).getExprs();
    assertEquals(((NumericComparisonExpression) s2.get(0)).getFunction(),
        NumericComparisonExpression.Function.LE);
    assertEquals(((NumericComparisonExpression) s2.get(0)).getLeft(), x1);
    assertEquals(((NumericComparisonExpression) s2.get(0)).getRight(), x2);
    assertEquals(((NumericComparisonExpression) s2.get(1)).getFunction(),
        NumericComparisonExpression.Function.NE);
    assertEquals(((NumericComparisonExpression) s2.get(1)).getLeft(), x1);
    assertEquals(((NumericComparisonExpression) s2.get(1)).getRight(), NumericConstant.valueOf(0));
    command = llpi.next().get(0);

    assertTrue(command instanceof CountCommand);
    llpi.next(); // skip comment
    command = llpi.next().get(0);

    assertTrue(command instanceof AssertCommand);
    List<BooleanExpression> s3 = ((AssertCommand) command).getExprs();
    assertEquals(((BooleanNaryExpression) s3.get(0)).getFunction(),
        BooleanNaryExpression.Function.OR);
    Expression s3_0 = ((BooleanNaryExpression) s3.get(0)).getArgs().get(0);
    Expression s3_1 = ((BooleanNaryExpression) s3.get(0)).getArgs().get(1);
    assertEquals(((NumericComparisonExpression) s3_0).getFunction(),
        NumericComparisonExpression.Function.LT);
    assertEquals(((NumericComparisonExpression) s3_0).getLeft(), x1);
    assertEquals(((NumericComparisonExpression) s3_0).getRight(),
        new NumericBinaryExpression(x2, NumericConstant.valueOf(5),
            NumericBinaryExpression.Function.ADD, PositionInfo.DUMMY));
    assertEquals(((NumericComparisonExpression) s3_1).getFunction(),
        NumericComparisonExpression.Function.GT);
    assertEquals(((NumericComparisonExpression) s3_1).getLeft(), x2);
    assertEquals(((NumericComparisonExpression) s3_1).getRight(), x3);
    command = llpi.next().get(0);

    assertTrue(command instanceof CountCommand);
    llpi.next(); // skip comment
    command = llpi.next().get(0);
    assertTrue(command instanceof ClearCommand);
  }

  @Test
  public void orWithNestedAnd() throws IOException {
    BufferedReader br = new BufferedReader(
        new FileReader(new File("src/test/inputs/parser/or-with-and")));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, null);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    Command command = llpi.next().get(0);
    assertTrue(command instanceof VariableDeclarationCommand);
    eval.visit(command);

    command = llpi.next().get(0);
    assertTrue(command instanceof DependentVariableDeclarationCommand);
    eval.visit(command);

    command = llpi.next().get(0);
    assertTrue(command instanceof AssertCommand);

    List<BooleanExpression> s1 = ((AssertCommand) command).getExprs();
    assertEquals(((BooleanNaryExpression) s1.get(3)).getFunction(),
        BooleanNaryExpression.Function.OR);
  }

  @Test
  public void simpleOrWithComments() throws IOException {
    BufferedReader br = new BufferedReader(
        new FileReader(new File("src/test/inputs/intersection/addition02cfc.pcp")));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, null);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    while (llpi.hasNext()) {
      List<Command> commands = llpi.next();
      if (commands.isEmpty()) {
        continue;
      }
      Command command = commands.get(0);
      if (command instanceof VariableDeclarationCommand
          || command instanceof DependentVariableDeclarationCommand) {
        eval.visit(command);
      }
    }
    //just checking if it is parsed correctly :)
  }
}