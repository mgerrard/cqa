package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;

import com.google.common.collect.Sets;
import coral.util.Config;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.counters.QCoralCaller;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.SymbolTable;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class PartitioningTests {

  @Test
  public void testJtopasPartitioning() throws IOException {
    SymbolTable symtable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(symtable, null, null, null);
    Constraint cons = null;

    for (String line : Files.readAllLines(Paths.get("src/test/inputs/jtopas.pcp"))) {
      ANTLRInputStream input = new ANTLRInputStream(line);
      InputFormatLexer lexer = new InputFormatLexer(input);
      InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

      ParseTree tree = parser.input();
      List<Command> commands = AstBuilder.parseCommands(symtable, tree);
      commands.forEach(c -> eval.visit(c));

      if (!commands.isEmpty() && commands.get(0) instanceof AssertCommand) {
        cons = new Constraint(
            new BooleanNaryExpression(Function.AND,
                ((AssertCommand) commands.get(0)).getExprs()));
      }
    }
    assert cons != null;

    ConstraintPartitioner part = new ConstraintPartitioner();
    part.computeVarClusters(Sets.newHashSet(cons));
    List<Constraint> partitions = part.partition(cons);
    System.out.println(partitions);
    assertEquals(partitions.size(), 20);
  }

  @Test
  public void ensureRealPaverIsWorkingProperlyWithQCoral() throws Exception {
    UniformDoubleVariable n1 = new UniformDoubleVariable("n", 0, 100);
    NumericConstant c1 = new NumericConstant(5, ExpressionType.DOUBLE);
    NumericUnaryExpression sqrt = new NumericUnaryExpression(NumericUnaryExpression.Function.SQRT,
        n1, PositionInfo.DUMMY);
    NumericComparisonExpression comparison = new NumericComparisonExpression(sqrt, c1,
        NumericComparisonExpression.Function.LE);

    Constraint constraint = new Constraint(comparison);
    QCoralCaller qCaller = new QCoralCaller(Options.loadFromUserHomeOrDefault());

    StatisticalCountResult qResultsWithRealPaver = (StatisticalCountResult) qCaller.count(Sets.newHashSet(constraint)).get(constraint);
    Config.mcUseRealPaver = false;
    StatisticalCountResult qResultsWithoutRealPaver = (StatisticalCountResult) qCaller.count(Sets.newHashSet(constraint))
        .get(constraint);

    BigRational answer = BigRational.valueOf(1).div(4);
    System.out.println("with   :"+qResultsWithRealPaver);
    System.out.println("without:"+qResultsWithoutRealPaver);
    Assert.assertTrue(qResultsWithRealPaver.expectedProbability()
        .sub(answer)
        .compareTo(BigRational.valueOf(0.001)) <= 0);
    Assert.assertTrue(qResultsWithRealPaver.getVariance()
        .sub(qResultsWithoutRealPaver.getVariance())
        .isNegative());
  }

  @After
  public void restoreQCoralOptions() {
    Config.mcUseRealPaver = true;
  }
}
