package name.filieri.antonio.pcp;

import com.google.common.collect.Sets;
import java.util.Map;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.counters.BarvinokCaller;
import name.filieri.antonio.pcp.counters.ModelCounterException;
import name.filieri.antonio.pcp.counters.QCoralCaller;
import name.filieri.antonio.pcp.counters.SharpSatCaller;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

public class LinearTests {

  Options options = Options.loadFromUserHomeOrDefault();

  @Test
  public void linearConstraints() throws ModelCounterException {
    Assume.assumeTrue("Barvinok is not installed correctly!", options.checkIfBarvinokIsAccessible());
    Assume.assumeTrue("SharpSAT is not installed correctly!", options.checkIfSharpsatIsAccessible());

    UniformIntegerVariable n1 = new UniformIntegerVariable("n", 0,10);
    NumericConstant c1 = new NumericConstant(5, ExpressionType.INT);
    NumericComparisonExpression comparison = new NumericComparisonExpression(n1,c1, Function.LE);

    Constraint constraint = new Constraint(comparison);
    BarvinokCaller bCaller = new BarvinokCaller(Options.loadFromUserHomeOrDefault());
    SharpSatCaller sCaller = new SharpSatCaller(Options.loadFromUserHomeOrDefault());
    QCoralCaller qCaller = new QCoralCaller(Options.loadFromUserHomeOrDefault());

    Map<Constraint,CountResult> bResults = bCaller.count(Sets.newHashSet(constraint));
    Map<Constraint,CountResult> sResults = sCaller.count(Sets.newHashSet(constraint));
    Map<Constraint,CountResult> qResults = qCaller.count(Sets.newHashSet(constraint));

    BigRational answer = BigRational.valueOf(6).div(11);
    Assert.assertTrue(bResults.get(constraint).expectedProbability().equals(answer));
    Assert.assertTrue(sResults.get(constraint).expectedProbability().equals(answer));
    Assert.assertTrue(qResults.get(constraint).expectedProbability()
        .sub(answer)
        .compareTo(BigRational.valueOf(0.0001)) <= 0);
  }
}
