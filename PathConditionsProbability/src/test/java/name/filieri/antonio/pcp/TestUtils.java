package name.filieri.antonio.pcp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.utils.LineByLineParserIterator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.SymbolTable;

public class TestUtils {

  public static List<BooleanExpression> parseAndCollectExpressions(File input, Options options)
      throws FileNotFoundException {
    BufferedReader br = new BufferedReader(new FileReader(input));
    SymbolTable stable = new SymbolTable();
    CommandEvaluator eval = new CommandEvaluator(stable, null, null, options);
    LineByLineParserIterator llpi = new LineByLineParserIterator(stable, br);

    List<BooleanExpression> exprs = new ArrayList<>();
    while (llpi.hasNext()) {
      Command command = llpi.next().get(0);
      switch (command.getCommandType()) {
        case SET_OPTION:
        case DECLARE_DEP_VAR:
        case DECLARE_CONST:
        case DECLARE_VAR:
        case DECLARE_EXPR:
          eval.visit(command);
          break;
        case ASSERT:
          AssertCommand assertCmd = (AssertCommand) command;
          exprs.add(assertCmd.getExprs().get(0));
          break;
        case COUNT:
        case CLEAR:
          break;
      }
    }
    return exprs;
  }
}
