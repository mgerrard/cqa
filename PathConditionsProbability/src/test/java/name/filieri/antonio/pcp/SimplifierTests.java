package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.simplifier.SMTLIBSimplifier;
import name.filieri.antonio.pcp.simplifier.Z3Simplifier;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

public class SimplifierTests {

  static boolean z3InPath = false;
  static Options options = Options.loadFromUserHomeOrDefault();

  @BeforeClass
  public static void checkForCommandInPath() {
    Runtime rt = Runtime.getRuntime();
    Process proc;
    try {
      proc = rt.exec(options.getZ3Path() + " --help");
      proc.waitFor();
      int exitVal = proc.exitValue();
      if (exitVal == 0) {
        z3InPath = true;
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }


  @Test
  public void testLinearNativeZ3() throws FileNotFoundException {
    List<BooleanExpression> exprs = TestUtils
        .parseAndCollectExpressions(new File("src/test/inputs/simplifier/simple-linear.pcp"), options);
    BooleanExpression simplified = new Z3Simplifier()
        .simplify(new BooleanNaryExpression(Function.AND, exprs));
    System.out.println(simplified);

    assertTrue(simplified instanceof BooleanNaryExpression);
    assertEquals(((BooleanNaryExpression) simplified).getBooleanArgs().size(), 3);
  }

  @Test
  public void testLinearSMTLIB() throws FileNotFoundException {
    Assume.assumeTrue(z3InPath);
    List<BooleanExpression> exprs = TestUtils
        .parseAndCollectExpressions(new File("src/test/inputs/simplifier/simple-linear.pcp"), options);
    BooleanExpression simplified = new SMTLIBSimplifier(options)
        .simplify(new BooleanNaryExpression(Function.AND, exprs));
    System.out.println(simplified);

    assertTrue(simplified instanceof BooleanNaryExpression);
    assertEquals(((BooleanNaryExpression) simplified).getBooleanArgs().size(), 3);
  }

  @Test
  public void testSimplificationToConstantNativeZ3() throws FileNotFoundException {
    List<BooleanExpression> exprs = TestUtils
        .parseAndCollectExpressions(new File("src/test/inputs/simplifier/alpaca-problem01.pcp"), options);
    BooleanExpression simplified = new Z3Simplifier()
        .simplify(new BooleanNaryExpression(Function.AND, exprs));
    System.out.println(simplified);

    assertTrue(simplified instanceof BooleanNaryExpression);
    assertEquals(((BooleanNaryExpression) simplified).getBooleanArgs().size(), 1);
    assertEquals(((BooleanNaryExpression) simplified).getBooleanArgs().get(0), BooleanConstant.FALSE);
  }

  @Test
  public void testSimplificationToConstantSMTLIB() throws FileNotFoundException {
    List<BooleanExpression> exprs = TestUtils
        .parseAndCollectExpressions(new File("src/test/inputs/simplifier/alpaca-problem01.pcp"), options);
    BooleanExpression simplified = new SMTLIBSimplifier(options)
        .simplify(new BooleanNaryExpression(Function.AND, exprs));
    System.out.println(simplified);

    assertTrue(simplified instanceof BooleanNaryExpression);
    assertEquals(((BooleanNaryExpression) simplified).getBooleanArgs().size(), 1);
    assertEquals(((BooleanNaryExpression) simplified).getBooleanArgs().get(0), BooleanConstant.FALSE);
  }
}
