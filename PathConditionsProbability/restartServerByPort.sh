#!/usr/bin/env bash

PROJECT_ROOT="$(dirname "$(readlink -f "$0")")"
#generated during build (see pom.xml)
DEPS=$(<"${PROJECT_ROOT}/classpath")

CLASSES="${PROJECT_ROOT}/target/classes"
PORT="$1"

#main class
MAIN="name.filieri.antonio.pcp.PCPServer"
LOG_FOLDER="${PROJECT_ROOT}/logs/"
mkdir -p "$LOG_FOLDER"

# #kill process if running
# pkill -f "$MAIN"

# sleep 5

# #bury the knife deeper if still alive
# if pgrep -f "$MAIN"
# then
# 	pkill --signal SIGKILL -f "$MAIN"
# 	sleep 2
# fi

if [[ "$PORT" != +([0-9]) ]]; then
	echo "Usage: restartServer port [old_pid]"
	exit 1
fi


# kill any java process that is listening to the same port
PID="$(ss -lptn sport = :${PORT} | awk 'NR>1{print $6}' | grep java | sed -e 's/.*pid=\(.*\),.*/\1/')"
if [[ ! -z "$PID" ]]; then
	echo "killing $PID"
	kill "$PID"
	sleep 5;

	#check again
	PID="$(ss -lptn sport = :${PORT} | awk 'NR>1{print $6}' | grep java | sed -e 's/.*pid=\(.*\),.*/\1/')"
	if [[ ! -z "$PID" ]]; then
		echo "double-tap $PID"
		kill -9 "$PID"
		sleep 3;
	fi
fi

TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
java -Xmx3G -classpath "${CLASSES}:${DEPS}" "$MAIN" "$PORT" &> "${LOG_FOLDER}/${TIMESTAMP}.${PORT}.log" & disown
sleep 2
#print pid
echo $! 

