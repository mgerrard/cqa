#!/bin/bash

###########
# SETUP

if [ "$#" -lt 2 ]; then
    echo "error: missing arguments" >&2
    echo "  Usage: ./cqa <pse|sse|mc> <foo.c>" >&2
    exit 1
fi

technique=$1
file=$2

if [ ! -f $file ]; then
    echo "error: file '$file' not found" >&2
    exit 1
fi

if ! [ -x "$(command -v alpaca)" ]; then
    echo "error: alpaca is not installed" >&2
    exit 1
fi

echo "* Running CQA (instantiated with $technique) on $file"
echo

###########
# ALPACA

alpaca -s -t 300 --generalize-timeout 900 $file

###########
# QUANTIFY

filebase=$(basename $file .c)
cqa_dir="./logs_alpaca/$filebase/cqa"
if [ ! -d "$cqa_dir" ]; then
    echo "error: cannot find expected directory at: $cqa_dir"
    exit 1
fi

PCP="./PathConditionsProbability/startPCP"

for i in $cqa_dir/*/ ; do
    if [ -f $i/coinciding_interval ]; then
	echo
	echo "****************************************************"
	echo "Interval $i has coinciding bounds; counting them:"
	echo
	count-query $i/coinciding_interval > $i/pcp.query
	$PCP < $i/pcp.query
    else
	echo
	echo "****************************************************"
	echo "Interval $i has noncoinciding bounds; counting them:"
	echo
	if [ $technique = "mc" ]; then
	    interval-size-query $i/noncoinciding_interval > $i/pcp.query
	    $PCP < $i/pcp.query
	else
	    ./instrumentSources.sh $i
            ./runUNCIVL.sh $technique $i/*.c
	fi
    fi
done

